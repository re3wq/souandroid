package com.taiyi.soul;

import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static com.taiyi.soul.utils.EncryptUtil.getDecryptString;
import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void test(){

        try {
//            String str = getEncryptString("5Hqi3o8PUAtQVUj1MhDwkjbA16CWFRYnVcLfyFCBtsG3kaTBqLN","123456abc");
//
//            System.out.println("加密---"+str);

            String str1 = getDecryptString("qNWIBvxOuJ9b4U4YdnLxQbHGm8LkkygB03811b86c0e294cd0763c96e3d1271837ec925175f98b6c028f05c7121d8b6bbf61713f60301b81c660fadbbbf65bc0e2b89f07535682dfa2a95bc12085f0654","123456abc");
            System.out.println("解密---"+str1);
            String str2 = getDecryptString("qNWIBvxOuJ9b4U4YdnLxQbHGm8LkkygB03811b86c0e294cd0763c96e3d1271837ec925175f98b6c028f05c7121d8b6bbf61713f60301b81c660fadbbbf65bc0e2b89f07535682dfa2a95bc12085f0654","123456abc");
            System.out.println("解密---"+str2);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

    }
}