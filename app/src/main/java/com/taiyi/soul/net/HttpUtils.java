package com.taiyi.soul.net;

import com.lzy.okgo.request.GetRequest;
import com.lzy.okgo.request.PostRequest;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.net.callbck.JsonCallbackWrapper;
import com.taiyi.soul.utils.Utils;
import com.lzy.okgo.OkGo;

import java.io.File;
import java.util.Map;


/**
 * Created by NGK on 2018/4/2.
 * app网络请求管理类
 */
public class HttpUtils {

    public static String chain_url = "https://node02.soulswap.io/v1/";//开发节点
//    public static String chain_url = "https://node01.soulswap.io/v1/";//sss
//    public static String chain_url = "http://18.163.19.52:28001/v1/";//开发节点

    public static String language = "zh";



    /**
     * Gets requets.
     *
     * @param <T>      the type parameter
     * @param url      the url
     * @param tag      the tag
     * @param map      the map
     * @param callback the callback
     */
    public static <T> void getRequets(String url, Object tag, Map<String, String> map, JsonCallback<T> callback) {
        String lang = "";

        if (Utils.getSpUtils().getString("current_language", "").equals("")) {
//            lang = HttpUtils.systemLocale.getLanguage();
            lang = language;
        }else {
            lang = Utils.getSpUtils().getString("current_language", "");
        }
//        Log.e("lang===",lang);
        OkGo.<T>get(url)
                .tag(tag)
                .headers("lang", lang)
                .headers("langu", lang)
                .headers("token", Utils.getSpUtils().getString(Constants.TOKEN, ""))
                .params(map)
                .execute(callback);
    }


    /**
     * Post request.
     *
     * @param <T>      the type parameter
     * @param url      the url
     * @param tag      the tag
     * @param map      the map
     * @param callback the callback
     */
    public static <T> void postRequest(String url, Object tag, Map<String, String> map, JsonCallback<T> callback) {

        String lang = "";
        if (Utils.getSpUtils().getString("current_language", "").equals("")) {
//            lang = systemLocale.getLanguage();
            lang = language;
        }else {
            lang = Utils.getSpUtils().getString("current_language", "");
        }
        OkGo.<T>post(url)
                .tag(tag)
                .headers("token", Utils.getSpUtils().getString(Constants.TOKEN, ""))
                .headers("langu", lang)
                .params(map)
                .execute(callback);

    }



    /**
     * Post request.
     *
     * @param <T>      the type parameter
     * @param url      the url
     * @param tag      the tag
     * @param callback the callback
     */
    public static <T> void fileRequest(String url, Object tag, File file, JsonCallback<T> callback) {

        String lang = "";
        if (Utils.getSpUtils().getString("current_language", "").equals("")) {
//            lang = systemLocale.getLanguage();
            lang = language;
        }else {
            lang = Utils.getSpUtils().getString("current_language", "");
        }
        OkGo.<T>post(url)
                .tag(tag)
                .headers("token", Utils.getSpUtils().getString(Constants.TOKEN, ""))
                .headers("langu", lang)
                .params("image",file)
                .execute(callback);

    }

    /**
     * Post request.
     *
     * @param <T>      the type parameter
     * @param url      the url
     * @param tag      the tag
     * @param map      the map
     * @param callback the callback
     */
    public static <T> void putRequest(String url, Object tag, String map, JsonCallback<T> callback) {

        String lang = "";
        if (Utils.getSpUtils().getString("current_language", "").equals("")) {
//            lang = systemLocale.getLanguage();
            lang = language;
        }else {
            lang = Utils.getSpUtils().getString("current_language", "");
        }
        OkGo.<T>put(url)
                .tag(tag)
                .headers("token", Utils.getSpUtils().getString(Constants.TOKEN, ""))
                .headers("langu",lang)
                .upJson(map)
                .execute(callback);

    }

    /**
     * Post request.
     *
     * @param <T>      the type parameter
     * @param url      the url
     * @param tag      the tag
     * @param parms    the parms
     * @param callback the callback
     */
    public static <T> void postRequest(String url, Object tag, String parms, JsonCallback<T> callback) {
        String lang = "";
        if (Utils.getSpUtils().getString("current_language", "").equals("")) {
//            lang = systemLocale.getLanguage();
            lang = language;
        }else {
            lang = Utils.getSpUtils().getString("current_language", "");
        }
        OkGo.<T>post(url)
                .headers("Content-Type", "application/json")
                .tag(tag)
                .headers("token", Utils.getSpUtils().getString(Constants.TOKEN, ""))
                .headers("langu", lang)
                .upJson(parms)
                .execute(callback);

    }




    /**
     * EOS chain get
     * @param url
     * @param tag
     * @param map
     * @param callback
     * @param <T>
     */
    public static <T> void getRequetsChain(String url, Object tag, Map<String, String> map, JsonCallback<T> callback) {
        String httpUrl = url;
        if (!url.startsWith("http")) {
            String httpAddress = Utils.getSpUtils().getString("httpAddress",chain_url);
            httpUrl = httpAddress + url;
        }
        GetRequest<T> request = OkGo.<T>get(httpUrl)
                .tag(tag)
                .params(map);
        request.execute(new JsonCallbackWrapper<>(callback));
    }


    /**
     * EOS chain post
     * @param url
     * @param tag
     * @param map
     * @param callback
     * @param <T>
     */
    public static <T> void postRequestChain(String url, Object tag, Map<String, String> map, JsonCallback<T> callback) {
        String httpUrl = url;
        if (!url.startsWith("http")) {
            String httpAddress = Utils.getSpUtils().getString("httpAddress",chain_url);
            httpUrl = httpAddress + url;
        }
        PostRequest<T> request = OkGo.<T>post(httpUrl)
                .tag(tag)
                .params(map);
        request.execute(new JsonCallbackWrapper<>(callback));
    }


    /**
     * EOS chain post
     * @param url
     * @param tag
     * @param parms
     * @param callback
     * @param <T>
     */
    public static <T> void postRequestChain(String url, Object tag, String parms, JsonCallback<T> callback) {
        String httpUrl = url;
        if (!url.startsWith("http")) {
            String httpAddress = Utils.getSpUtils().getString("httpAddress",chain_url);
            httpUrl = httpAddress + url;
        }
        PostRequest<T> request = OkGo.<T>post(httpUrl)
                .tag(tag)
                .upJson(parms);
        request.execute(new JsonCallbackWrapper<>(callback));
    }



    public static <T> void pushRequest(String url, Object tag, Map<String, String> map, JsonCallback<T> callback) {
        String lang = "";
        if (Utils.getSpUtils().getString("current_language", "").equals("")) {
//            lang = systemLocale.getLanguage();
            lang = language;
        }else {
            lang = Utils.getSpUtils().getString("current_language", "");
        }
        OkGo.<T>post(url)
                .headers("Content-Type", "application/json")
                .tag(tag)
                .headers("token", "1")
                .headers("userid", "1")
                .headers("langu", lang)
                .params(map)
                .execute(callback);

    }

}
