package com.taiyi.soul.net.callbck;

import com.lzy.okgo.model.Progress;
import com.lzy.okgo.model.Response;
import com.lzy.okgo.request.base.Request;

import java.lang.reflect.Type;

public class JsonCallbackWrapper<T> extends JsonCallback<T>{

    private JsonCallback<T> mCallback;
    public JsonCallbackWrapper(JsonCallback<T> callback) {
        mCallback = callback;
    }

    public JsonCallbackWrapper(Type type, JsonCallback<T> callback) {
        super(type);
        mCallback = callback;
    }


    public JsonCallbackWrapper(Class<T> clazz, JsonCallback<T> callback) {
        super(clazz);
        mCallback = callback;
    }

    @Override
    public void onSuccess(Response<T> response) {
        try {
            mCallback.onSuccess(response);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public T convertResponse(okhttp3.Response response) throws Throwable {
        try {
            return mCallback.convertResponse(response);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onStart(Request<T, ? extends Request> request) {
        super.onStart(request);
        try {
            mCallback.onStart(request);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCacheSuccess(Response<T> response) {
        super.onCacheSuccess(response);
        try {
            mCallback.onCacheSuccess(response);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Response<T> response) {
        super.onError(response);
        try {
            mCallback.onError(response);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinish() {
        super.onFinish();
        try {
            mCallback.onFinish();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadProgress(Progress progress) {
        super.uploadProgress(progress);
        try {
            mCallback.uploadProgress(progress);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void downloadProgress(Progress progress) {
        super.downloadProgress(progress);
        try {
            mCallback.downloadProgress(progress);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
