package com.taiyi.soul.moudles.main.home.options.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\20 0020 9:51
 * @Author : yuan
 * @Describe ：期权购买弹出窗口的bean
 */
public class OptionPopupBean {
        @SerializedName("1")
        public List<_$1Bean> _$1;
        @SerializedName("2")
        public List<_$2Bean> _$2;
        @SerializedName("3")
        public List<_$3Bean> _$3;
        public static class _$1Bean {
            public String percentage;
            public String id;
            public String time;
            public String type;
            public String updown_id;
            public int select;
        }
        public static class _$2Bean {
            public int num;
            public String id;
            public String type;
            public String updown_id;
        }
        public static class _$3Bean {
            public String coin_name;
            public String coin_id;
            public String id;
            public int updown_id;
            public String url;
        }
}
