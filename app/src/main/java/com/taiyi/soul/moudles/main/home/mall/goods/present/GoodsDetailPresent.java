package com.taiyi.soul.moudles.main.home.mall.goods.present;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.ResultBean;
import com.taiyi.soul.moudles.main.home.mall.bean.GoodsDetailBean;
import com.taiyi.soul.moudles.main.home.mall.goods.GoAddOrderBean;
import com.taiyi.soul.moudles.main.home.mall.goods.SearchPriceBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class GoodsDetailPresent extends BasePresent<GoodsDetailView> {

    //获取商品详情
    public void getData(Activity activity, String commodityid) {
        Map<String, String> map = new HashMap<>();
        map.put("commodityid", commodityid);
        HttpUtils.postRequest(BaseUrl.MALL_GOODS_DETAILS, this, map, new JsonCallback<BaseResultBean<GoodsDetailBean>>() {
            @Override
            public BaseResultBean<GoodsDetailBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<GoodsDetailBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {

                    Calendar.getInstance().getTimeInMillis();
                    if (null != view)
                        view.onSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<GoodsDetailBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //根据规格查询对应价格
    public void searchPrice(Activity activity, String commodityid, String specproids) {
        Map<String, String> map = new HashMap<>();
        map.put("commodityid", commodityid);  //商品id
        map.put("specproids", specproids);//query	string
        HttpUtils.postRequest(BaseUrl.COMMODITYPRICE, this, map, new JsonCallback<BaseResultBean<SearchPriceBean>>() {
            @Override
            public BaseResultBean<SearchPriceBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<SearchPriceBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onSearchPriceSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<SearchPriceBean>> response) {
                super.onError(response);
            }
        });
    }

    //加入购物车
    public void addCar(Activity activity, String commodityspecid, String num) {
        Map<String, String> map = new HashMap<>();
        map.put("commodityspecid", commodityspecid);
        map.put("number1", num);
        HttpUtils.postRequest(BaseUrl.GOODS_ADDCAR, this, map, new JsonCallback<BaseResultBean<ResultBean>>() {
            @Override
            public BaseResultBean<ResultBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ResultBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onAddCarSuccess(response.body().msg_cn);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ResultBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //准备下单
    public void goAddOrder(Activity activity, String commodityspecid, String num) {
        Map<String, String> map = new HashMap<>();
        map.put("commodityspecid", commodityspecid);
        map.put("num1", num);
        map.put("userid", Utils.getSpUtils().getString(Constants.TOKEN, ""));
//        map.put("type", "1");
        HttpUtils.postRequest(BaseUrl.GOODGOADDORDER, this, map, new JsonCallback<BaseResultBean<GoAddOrderBean>>() {
            @Override
            public BaseResultBean<GoAddOrderBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<GoAddOrderBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onAddOrderSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<GoAddOrderBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }
    //获取购物车是否为空
    public void getCarSizeData(Activity activity, String pagenum) {
        Map<String, String> map = new HashMap<>();
        map.put("pagenum", pagenum);
        HttpUtils.postRequest(BaseUrl.GOODS_CARLIST, this, map, new JsonCallback<BaseResultBean<CarListBean>>() {
            @Override
            public BaseResultBean<CarListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CarListBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onCarSizeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<CarListBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

}
