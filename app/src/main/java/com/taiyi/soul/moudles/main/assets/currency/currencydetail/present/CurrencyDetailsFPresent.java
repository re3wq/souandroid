package com.taiyi.soul.moudles.main.assets.currency.currencydetail.present;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.CurrencyFragmentBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.UsdnOrNgkTransferRecordBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class CurrencyDetailsFPresent extends BasePresent<CurrencyDetailsFView> {

    public void getDatas(Activity activity,String coin_name,int pageNum,String transferType){

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("coinName",coin_name);
            jsonObject.put("pageNum",pageNum);
            jsonObject.put("pageSize",10);
            if (!TextUtils.isEmpty(transferType)) {

                jsonObject.put("transferType",transferType);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.PROFITTRANSFER, this, jsonObject.toString(), new JsonCallback<BaseResultBean<CurrencyFragmentBean>>() {
            @Override
            public BaseResultBean<CurrencyFragmentBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CurrencyFragmentBean>> response) {
                super.onSuccess(response);
                    if(null!=view)
                        if(response.body().status==602 || response.body().status == 3080005){
                            if(Constants.isL==0){
                                Constants.isL=1;
                                ToastUtils.showShortToast(response.body().msg);
//                           //  AppManager.getAppManager().finishAllActivity();
//                            ActivityUtils.next(activity, LoginActivity.class, true);
                                activity.startActivity(new Intent(activity, LoginActivity.class));
                                Utils.getSpUtils().remove(Constants.TOKEN);
                            }

                            view.onFailure();
                        }else {
                            view.getSuccess(response.body().status,response.body().msg,response.body().data);
                        }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<CurrencyFragmentBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getUsdnOrNgkTransferRecord(String coinName,String s,String pageNo){
        Map<String,String>map=new HashMap<>();
        map.put("accountName", Utils.getSpUtils().getString("mainAccount"));
        map.put("TokenSymbol", coinName);//币种名
        map.put("TokenCode",s);//合约名
        map.put("PageIndex",pageNo);//页数
        map.put("PageSize","10");//每页数量
        HttpUtils.getRequets("http://54.199.103.230:38001/account/GetTransferRecord",
                this, map, new JsonCallback<BaseResultBean<UsdnOrNgkTransferRecordBean>>() {
            @Override
            public BaseResultBean<UsdnOrNgkTransferRecordBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<UsdnOrNgkTransferRecordBean>> response) {
                super.onSuccess(response);
                view.getUsdnOrNgkRecordSuccess(response.body().data);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<UsdnOrNgkTransferRecordBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getCoinList() {
        HttpUtils.getRequets(BaseUrl.GET_COIN_LIST, this, null, new JsonCallback<BaseResultBean<List<CoinListBean>>>() {
            @Override
            public BaseResultBean<List<CoinListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<CoinListBean>>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getCoinListSuccess(response.body().data);
                    } else {
                        view.getCoinListFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<CoinListBean>>> response) {
                super.onError(response);
            }
        });
    }
}
