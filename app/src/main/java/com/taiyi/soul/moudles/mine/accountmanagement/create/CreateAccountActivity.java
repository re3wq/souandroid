package com.taiyi.soul.moudles.mine.accountmanagement.create;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.blockchain.cypto.ec.EosPrivateKey;
import com.taiyi.soul.blockchain.util.ChainUtil;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.register.bean.WordBean;
import com.taiyi.soul.moudles.scancode.ScanCodeActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.RegexUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import org.bitcoinj.core.ECKey;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.OnClick;
import me.ljp.permission.PermissionItem;

public class CreateAccountActivity extends BaseActivity<CreateAccountView, CreateAccountPresent> implements CreateAccountView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.inputInviteCodeEt)
    EditText inputInviteCodeEt;
    @BindView(R.id.invitePeopleEt)
    TextView invitePeopleEt;
    @BindView(R.id.accountNameEt)
    EditText accountNameEt;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.cl_title)
    ConstraintLayout cl_title;

    @BindView(R.id.ll_register)
    LinearLayout ll_register;

    @BindView(R.id.view)
    View view;

    @BindView(R.id.tv_desc)
    TextView tv_desc;

    @BindView(R.id.view_desc)
    View view_desc;

    @BindView(R.id.tv_type)
    TextView tv_type;

    ArrayList<String> data = new ArrayList<>();
    public static final int REQUEST_CODE = 0x1001;

    private String from = "";

    private WordBean wordBean;
    private String account = "";
    private String inviteCode;
    private String loginPwd;
    private String loginType;
    private String account1;
    int  clic = 0;

    @Override
    protected int getLayoutId() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        return R.layout.activity_create_account;
    }

    @Override
    public CreateAccountPresent initPresenter() {
        return new CreateAccountPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        from = getIntent().getStringExtra("from");

        loginPwd = getIntent().getStringExtra("loginPwd");
        loginType = getIntent().getStringExtra("loginType");
        account1 = getIntent().getStringExtra("account");
        if (from != null && from.equals("register")) {
            titleTv.setText(getString(R.string.register));
            inviteCode = getIntent().getStringExtra("inviteCode");
            tv_type.setText(getString(R.string.register));
            ll_register.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            tv_desc.setVisibility(View.VISIBLE);
            view_desc.setVisibility(View.VISIBLE);

        } else {
            titleTv.setText(getString(R.string.create_account));
            tv_type.setText(getString(R.string.create));
            //TODO lxq
            titleTv.setVisibility(View.GONE);
            ll_register.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
//            view.setVisibility(View.VISIBLE);

            tv_desc.setVisibility(View.GONE);
            view_desc.setVisibility(View.GONE);
        }

    }

    @Override
    protected void initData() {

        wordBean = generateData();
        List<String> stringList = wordBean.getWords();
        data.clear();

        data.addAll(stringList);
        CommonAdapter mnemonicListAdapter = AdapterManger.getMnemonicListAdapter(this, data);
        recyclerView.setAdapter(mnemonicListAdapter);

    }

    @Override
    public void initEvent() {
        inputInviteCodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 6) {
                    showProgress();
                    presenter.getInvitePerson(s.toString());
                }
            }
        });
        accountNameEt.setFilters(new EditInputFilter[]{new EditInputFilter(_INPUT)});
        accountNameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString())) {
                    String s1 = s.toString();
                    String s2 = s1.substring(s1.length() - 1, s1.length());

//                    if (!s2.matches("^[a-z1-5]+$")) {
//                        if (s1.length() > 1) {
//                            accountNameEt.setText(s1.substring(0, s1.length() - 1));
//                            accountNameEt.setSelection(s1.substring(0, s1.length() - 1).length());
//                        } else {
//                            accountNameEt.setText("");
//                        }
//                    }
//                    if (s2.matches("^[0-9]+$")&&(Integer.parseInt(s2)>5||Integer.parseInt(s2)==0)) {
//                        if (s1.length() > 1) {
//                            accountNameEt.setText(s1.substring(0, s1.length() - 1));
//                            accountNameEt.setSelection(s1.substring(0, s1.length() - 1).length());
//                        } else {
//                            accountNameEt.setText("");
//                        }
//                    }
                }
            }
        });
    }
    public static final String _INPUT = "^[a-z1-5]+$";
    /**
     *   * Created by xugang on 2016/6/22.
     *   * EditView过滤器
     *
     */
    public class EditInputFilter implements InputFilter {
        //
        private String regular;

        public EditInputFilter(String regular) {
            this.regular = regular;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (regular == null)
                return source;
            if (dstart == 0 && dend == 0 && TextUtils.isEmpty(source))
                return null;
            if (dstart == dend) {
                //输入
                StringBuilder builder = new StringBuilder(dest);
                if (builder.append(source).toString().matches(regular)) return source;
                else return "";
            }
            return source;
        }
    }

    /**
     * 生成助记词、公私钥
     *
     * @return
     */
    private WordBean generateData() {

        String genMnemonic = ChainUtil.genMnemonic();
        ECKey genECKey = ChainUtil.genECKey(genMnemonic, "");
        EosPrivateKey eosPrivateKey = new EosPrivateKey(genECKey.getPrivKeyBytes());

        List<String> list = Arrays.asList(genMnemonic.split(" "));
//        List<String> listNew = new ArrayList<String>(new TreeSet<String>(list));
//        if(list.size()!=listNew.size()){
//            generateData();
//        }
        return new WordBean(new ArrayList<>(list), eosPrivateKey.getPublicKey().toString(), eosPrivateKey.toWif());
    }

    @OnClick({R.id.backIv, R.id.scanIv, R.id.randomTv, R.id.refreshTv, R.id.nextStepTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                if(from != null && from.equals("register")){
                    EventBus.getDefault().post("isRegister");
                    ActivityUtils.next(this, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                }
                finish();
                break;
            case R.id.scanIv:
                List<PermissionItem> permissonItems = new ArrayList<PermissionItem>();
                permissonItems.add(new PermissionItem(Manifest.permission.CAMERA, getString(R.string.camer), R.drawable.permission_ic_camera));
                if (Utils.getPermissions(permissonItems, getString(R.string.open_camer_scan))) {
                    Bundle bundle = new Bundle();
                    bundle.putString("from", "transfer");
                    ActivityUtils.next(this, ScanCodeActivity.class, bundle, REQUEST_CODE);
                }
                break;
            case R.id.randomTv:
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.randomTv);
                if(!fastDoubleClick){
                    if(clic==0){
                        clic=1;
                        showProgress();
                        account = RegexUtil.getRandomString(8);
                        presenter.checkAccountUseableData(account);
                    }
                }




                break;
            case R.id.refreshTv:
                wordBean = generateData();
                List<String> stringList = wordBean.getWords();
                data.clear();
                data.addAll(stringList);
                recyclerView.getAdapter().notifyDataSetChanged();
                break;
            case R.id.nextStepTv:
                boolean fastDoubleClick1 = ButtonUtils.isFastDoubleClick(R.id.nextStepTv);
                if(!fastDoubleClick1){
                    String accountName = accountNameEt.getText().toString();
                    if (TextUtils.isEmpty(accountName)) {
                        toast(getResources().getString(R.string.account_input));
                        return;
                    }

                    if (!accountName.matches("^[a-z1-5]+$") || accountName.length() != 8) {
                        toast(getResources().getString(R.string.account_err));
                        return;
                    }
                    showProgress();
                    presenter.checkAccountUseableDataInput(accountName);
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (data != null)
                if (null != data.getExtras()) {
                    String strCode = data.getExtras().getString("id");

                    if(strCode.contains("=")){
                        String[] split = strCode.split("=");
                        if(split.length==1){
                            ToastUtils.showShortToast(getString(R.string.invite_filed_hint));
                        }else {
                            inputInviteCodeEt.setText(split[1]);
                        }

                    }
//                    String substring = strCode.substring(strCode.length() - 6, strCode.length());
//                    Log.i("sdadada","----"+substring);
//                    inputInviteCodeEt.setText(substring);
//                    Log.i("dsadadas", "-------" + strCode);
                }
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(cl_title).init();
    }

    @Override
    public void getInvitePersonSuccess(String invitePeople) {
        hideProgress();
        invitePeopleEt.setText(invitePeople);
    }

    @Override
    public void getInvitePersonFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
        invitePeopleEt.setText("");
    }

    @Override
    public void checkAccountUseable(boolean s) {
        hideProgress();

        if (!s) {
            showProgress();
            account = RegexUtil.getRandomString(8);
            presenter.checkAccountUseableData(account);
        } else {
            clic=0;
//            accountNameEt.setText(account+ Constants.ACCOUNT_SUFFIX);
            accountNameEt.setText(account);
        }
    }

    @Override
    public void checkAccountUseableInput(boolean ss) {
        hideProgress();
        if (!ss) {
            AlertDialogShowUtil.toastMessage(CreateAccountActivity.this,getString(R.string.create_account_username));
        } else {
            Bundle bundle = new Bundle();
            String accountName = accountNameEt.getText().toString();
            if (from != null && from.equals("register")) {

                bundle.putString("from", from);
                bundle.putSerializable("word_bean", wordBean);
                bundle.putString("inviteCode", inviteCode);
                bundle.putString("accountName", accountName + Constants.ACCOUNT_SUFFIX);

            } else {
                String s = inputInviteCodeEt.getText().toString();
                if (TextUtils.isEmpty(s)) {
                    toast("请输入邀请码");
                    return;
                }
                if (s.length() < 6) {
                    toast(getResources().getString(R.string.invite_code_err));
                    return;
                }
                String invitePeople = invitePeopleEt.getText().toString();
                if (TextUtils.isEmpty(invitePeople)) {
                    toast(getResources().getString(R.string.invite_person_err));
                    return;
                }

                bundle.putString("from", from);
                bundle.putSerializable("word_bean", wordBean);
                bundle.putString("inviteCode", s);
                bundle.putString("accountName", accountName + Constants.ACCOUNT_SUFFIX);
            }
            ActivityUtils.next(this, CreateAccountStep2Activity.class, bundle);
            if (from != null && from.equals("register")) {

            } else {
                finish();
            }
        }
    }
}
