package com.taiyi.soul.moudles.main.home.options.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\22 0022 14:06
 * @Author : yuan
 * @Describe ：
 */
public class KOtherBean {
        public int id;
        public String instrument_id;
        public String img_url;
        public String product_id;
        public String last;
        public String last_qty;
        public String best_ask;
        public String ask;
        public String best_ask_size;
        public String best_bid;
        public String bid;
        public String best_bid_size;
        public String open_24h;
        public String high_24h;
        public String low_24h;
        public String base_volume_24h;
        public String quote_volume_24h;
        public String timestamp;
        public String price;
        public String conversion;
}
