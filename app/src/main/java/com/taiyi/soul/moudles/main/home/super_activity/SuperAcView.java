package com.taiyi.soul.moudles.main.home.super_activity;

import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;

public interface SuperAcView {
    void activitySuccess(ActivityFirstBean activityFirstBean);
    void activityFailure(String errorMsg);
}
