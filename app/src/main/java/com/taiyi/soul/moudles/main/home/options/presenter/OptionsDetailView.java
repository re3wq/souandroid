package com.taiyi.soul.moudles.main.home.options.presenter;

import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.RedirectsBean;
import com.taiyi.soul.moudles.main.home.options.bean.KLainBean;
import com.taiyi.soul.moudles.main.home.options.bean.KOtherBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionDetailsListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionPopupBean;

import java.util.List;

public interface OptionsDetailView {
    void getDataSuccess(int status, String msg, OptionDetailsListBean bean);
    void getPopupSuccess(int status, String msg,OptionPopupBean bean);
    void getPopupBuySuccess(int status, String msg);
    void getCurrencyBalanceSuccess(int code, String msg_cn,String balance);//查询币种对应余额
    void getBalanceSuccess(String symbol, String balance);//查询usdn和ngk余额
    void onIssueFeeSuccess(int code, String msg_cn, IssueFeeBean bean);//获取转账地址
    void getKLineSuccess(int code, String msg_cn, KLainBean bean);//k线
    void getKOtherSuccess(int code, String msg_cn, KOtherBean bean);//k线页面其他
    void getPictureDataSuccess(int code, String msg_cn, List<OptionListBean> bean);
    void onCheckPayPasswordSuccess(int code, String msg_cn, String bean);//校验交易密码
    void getRedirectDataSuccess(int code, String msg_cn, RedirectsBean bean);//文案信息
    void onFailure();
}
