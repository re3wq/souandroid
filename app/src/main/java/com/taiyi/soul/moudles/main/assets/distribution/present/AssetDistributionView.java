package com.taiyi.soul.moudles.main.assets.distribution.present;

import com.taiyi.soul.moudles.main.assets.distribution.bean.AssetDistributionBean;


public interface AssetDistributionView {
    void onSuccess(int code, String msg_cn, AssetDistributionBean bean);//资产分布信息
}
