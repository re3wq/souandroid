package com.taiyi.soul.moudles.welcome;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.NodeListBean;
import com.taiyi.soul.bean.TokenListBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.gen.WalletBeanDao;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.MainActivity;
import com.taiyi.soul.moudles.mine.bean.VersionUpdateBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.utils.ScreenUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.CustomVideoView;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class WelcomeActivity extends BaseActivity<WelcomeView, WelcomePresent> implements WelcomeView, View.OnClickListener {

    @BindView(R.id.videoview)
    CustomVideoView videoview;
 @BindView(R.id.welcome_iv)
 ImageView welcome_iv;

    TextView skipTv, nowUpdateTv, forceUpdateTv, updateContentTv;
    private String downloadUrl;
    private Dialog dialog;
    private int isFirstIn = Utils.getSpUtils().getInt("isFirstInApp", 0);
    private MediaMetadataRetriever retriever;
    private int mValue;
    private int start = 0;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_welcome;
    }

    @Override
    public WelcomePresent initPresenter() {
        return new WelcomePresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {


        mString = Utils.getSpUtils().getString("current_language", "");

        if ("".equals(mString)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                //获取系统语言
                locale =getResources().getConfiguration().getLocales().get(0);
                mString = locale.getLanguage();

            }
        }
        initLanguage();
        presenter.getRate("SOU");
        presenter.getRate("USDS");

        dialog = new Dialog(this, R.style.MyDialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        View view = LayoutInflater.from(this).inflate(R.layout.version_update_dialog_layout, null);
        updateContentTv = view.findViewById(R.id.updateContentTv);
        skipTv = view.findViewById(R.id.skipTv);
        skipTv.setOnClickListener(this);
        nowUpdateTv = view.findViewById(R.id.nowUpdateTv);
        nowUpdateTv.setOnClickListener(this);
        forceUpdateTv = view.findViewById(R.id.forceUpdateTv);
        forceUpdateTv.setOnClickListener(this);
        dialog.setContentView(view);
        initVideo();
//        initView();
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                    startA();
//            }
//        },2000);

//        Log.i("sha1===",sHA1(WelcomeActivity.this));
//        presenter.getVer(sHA1(WelcomeActivity.this));
//        initVideo();
    }

    private void initVideo() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int maxWidth = metrics.widthPixels;
        int maxhHeight = metrics.heightPixels;
        int res;
        //one one1 0.46  two 0.5 three1 0.54 three 0.56 four 0.6  five 0.66
        BigDecimal divide1 = new BigDecimal(maxWidth).divide(new BigDecimal(maxhHeight),2,BigDecimal.ROUND_HALF_UP);
        BigDecimal bigDecimal = new BigDecimal("0.50");

        if(divide1.compareTo(bigDecimal)==0){
            res = R.raw.move_two;//0.5
        }else if(divide1.compareTo(bigDecimal)==-1){
            res = R.raw.move_one1;//0.46
        }else {
            res = R.raw.move_three;
        }
        String uriStr = "android.resource://" + getPackageName() + "/" + res;


        retriever = new MediaMetadataRetriever();
        retriever.setDataSource(WelcomeActivity.this, Uri.parse(uriStr));
        videoview.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + res));
        //播放
        videoview.start();

        videoview.requestFocus();
        //循环播放
        videoview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                startA();
            }
        });

        videoview.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

//                Log.d("onError------", "----------------" + what);
                switch (what) {
                    case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                        break;

                    case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                        break;

                    case MediaPlayer.MEDIA_ERROR_UNKNOWN:
//                        Log.d("onError------", "onError() >>>>> MEDIA_ERROR_UNKNOWN");
                        // 在这里处理未知错误
                        return true; // 在这里return true后，未知错误时MediaPlayer不会再弹窗提示“无法播放视频”

                    default:
                        break;

                }
                return false;
            }
        });
        presenter.getNodeData();
        presenter.getTokenData();
    }
    public static String sHA1(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            byte[] cert = info.signatures[0].toByteArray();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(cert);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < publicKey.length; i++) {
                String appendString = Integer.toHexString(0xFF & publicKey[i])
                        .toUpperCase(Locale.US);
                if (appendString.length() == 1)
                    hexString.append("0");
                hexString.append(appendString);
                hexString.append(":");
            }
            String result = hexString.toString();
            return result.substring(0, result.length() - 1);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String mString = "";
    private Locale locale;
    @SuppressLint("NewApi")
    @Override
    protected void initData() {


    }
    private void initLanguage() {
        if (mString.equals("en")) {//英语
            HttpUtils.language = "en";
        } else if (mString.equals("fr")) {//法语
            HttpUtils.language  = "fra";
        } else if (mString.equals("de")) {//德语

            HttpUtils.language = "de";
        } else if (mString.equals("spa")) {//西班牙语

            HttpUtils.language = "spa";
        } else if (mString.equals("pt")) {//葡萄牙语

            HttpUtils.language = "pt";
        } else if (mString.equals("ru")) {//俄语

            HttpUtils.language = "ru";
        } else if (mString.equals("cht")) {//繁体中文

            HttpUtils.language = "cht";
        } else if (mString.equals("zh")) {//\中文
            if(null!=locale){
                if(locale.getCountry().equals("CN")){
                    HttpUtils.language = "zh";
                }else {
                    HttpUtils.language = "cht";
                }
            }else {
                HttpUtils.language = "zh";
            }

        } else if (mString.equals("ja")) {//日语
            HttpUtils.language = "jp";
        } else if (mString.equals("ko")) {//韩语
            HttpUtils.language = "kor";
        }

    }
    @Override
    public void initEvent() {

    }

    /**
     * 初始化
     */
    private void initView() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int maxWidth = metrics.widthPixels;
        int maxhHeight = metrics.heightPixels;
        int res;
        if (ScreenUtils.isAllScreenDevice(this)) {//全屏
            if(maxhHeight>=2244){
                res = R.raw.movie;
            }else {
                res = R.raw.welcomenew;
            }
        } else {//不是全屏
            if(maxhHeight>=2244){
                res = R.raw.movie;
            }else {
                res = R.raw.welcomenew;
            }

        }

        String uriStr = "android.resource://" + getPackageName() + "/" + res;


        retriever = new MediaMetadataRetriever();
        retriever.setDataSource(WelcomeActivity.this, Uri.parse(uriStr));
        String width = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH); //宽
        String height = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT); //高

//        Log.e("wwww====", width + "---" + height);



        //hh/ww = 1.5   x*1.5
        BigDecimal b1 = new BigDecimal(width);  //1080  1:2     2160 1:2
        BigDecimal b2 = new BigDecimal(height);  //2160
        BigDecimal b3 = new BigDecimal(maxWidth);
        double divide = b2.divide(b1, 1, BigDecimal.ROUND_DOWN).doubleValue();//  h : w
        BigDecimal divide2 = new BigDecimal(Double.toString(divide));//宽高比

        ViewGroup.LayoutParams params = videoview.getLayoutParams();

        mValue = divide2.multiply(b3).intValue();
        params.width = maxWidth; // 宽度设置成屏幕宽度，这里根据自己喜好设置
        params.height = mValue; // 利用已知图片的宽高比计算高度

        videoview.setLayoutParams(params);
        videoview.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + res));
        //播放
        videoview.start();

        videoview.requestFocus();
        //循环播放
        videoview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
//                showProgress();
//                presenter.checkVersionUpdate();
//                startA();
            }
        });

        videoview.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

//                Log.d("onError------", "----------------" + what);
                switch (what) {
                    case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                        break;

                    case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                        break;

                    case MediaPlayer.MEDIA_ERROR_UNKNOWN:
//                        Log.d("onError------", "onError() >>>>> MEDIA_ERROR_UNKNOWN");
                        // 在这里处理未知错误
                        return true; // 在这里return true后，未知错误时MediaPlayer不会再弹窗提示“无法播放视频”

                    default:
                        break;

                }
                return false;
            }
        });

        presenter.getNodeData();
        presenter.getTokenData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        retriever.release();
    }

    private void initAccount() {
        //确保 walletBean 有数据
        int loginType = Utils.getSpUtils().getInt("loginType");
        String phoneNumber = Utils.getSpUtils().getString("phoneNumber");
        String email = Utils.getSpUtils().getString("email");
        WalletBean walletBean = null;
        if (loginType == 0) {
            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(phoneNumber)).build().list();
            if (null != list) {
                if (list.size() > 0)
                    walletBean = list.get(list.size() - 1);
            }
//                walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(phoneNumber)).build().unique();
        } else {
            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(email)).build().list();
            if (null != list) {
                if (list.size() > 0)
                    walletBean = list.get(list.size() - 1);
            }
//                walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(email)).build().unique();
        }
        MyApplication.getInstance().setWalletBean(walletBean);
    }



    //    @OnClick({R.id.btn_stadrt, R.id.videoview})
    @OnClick({R.id.videoview})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.videoview:

                break;
        }
    }


    @Override
    public void onNodeSuccess(List<NodeListBean> bean) {

        if (bean != null && bean.size() > 0) {
            String httpAddress = bean.get(0).httpAddress;
            Utils.getSpUtils().put("httpAddress", httpAddress + "/v1/");

//            Log.e("htttt====",httpAddress + "/v1/");
        }

    }

    @Override
    public void onTokenSuccess(List<TokenListBean> bean) {
        if (bean != null && bean.size() > 0) {
            for (int i = 0; i < bean.size(); i++) {
                if (bean.get(i).symbol.equals("SOU")) {
                    String contract = bean.get(i).contract;
                    Constants.NGK_CONTRACT_ADDRESS = contract;
                } else if (bean.get(i).symbol.equals("USDS")) {
                    String contract = bean.get(i).contract;
                    Constants.USDN_CONTRACT_ADDRESS = contract;
                }
            }
//            Constants.tokenListBeans.addAll(bean);
        }
    }

    @Override
    public void checkVersionUpdateSuccess(VersionUpdateBean versionUpdateBean) {
        hideProgress();
    }

    private void startA() {
        if (isFirstIn == 0) {
            MyApplication.getDaoSession().getWalletBeanDao().deleteAll();
            MyApplication.getDaoSession().clear();
            Utils.getSpUtils().clear();
            Utils.getSpUtils().put("isFirstInApp", 1);
            ActivityUtils.next(WelcomeActivity.this, LoginActivity.class, true);
        } else {
            if (Utils.getSpUtils().getBoolean("isLogin") && !TextUtils.isEmpty(Utils.getSpUtils().getString(Constants.TOKEN))) {
                initAccount();
                ActivityUtils.next(WelcomeActivity.this, MainActivity.class, true);
            } else {
                ActivityUtils.next(WelcomeActivity.this, LoginActivity.class, true);
            }
        }

    }

    @Override
    public void checkVersionUpdateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getRateSuccess(String symbol, String rate) {
        if ("SOU".equals(symbol)) {
            Utils.getSpUtils().put(Constants.NGK_TO_USDT_RATE, rate);
        } else {
            Utils.getSpUtils().put(Constants.USDN_TO_USDT_RATE, rate);
        }
    }

    @Override
    public void getRateFailure(String errorMsg) {
        toast(errorMsg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skipTv:
                dialog.dismiss();
//                startA();
                break;
            case R.id.nowUpdateTv:
            case R.id.forceUpdateTv:
                dialog.dismiss();
                openBrowser(this, downloadUrl);
                break;
        }
    }

    public void openBrowser(Context context, String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            ComponentName componentName = intent.resolveActivity(context.getPackageManager());
            // 打印Log   ComponentName到底是什么
            context.startActivity(Intent.createChooser(intent, "请选择浏览器"));
        } else {
            toast("未找到手机中的浏览器");
        }
    }


}