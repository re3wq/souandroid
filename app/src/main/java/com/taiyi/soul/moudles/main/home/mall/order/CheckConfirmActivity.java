package com.taiyi.soul.moudles.main.home.mall.order;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.ShopCarSubmitBean;
import com.taiyi.soul.moudles.main.home.mall.goods.GoAddOrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.AllPriceBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.CheckConfirmListBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.FreightBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.GoodsOrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.present.CheckConfirmPresent;
import com.taiyi.soul.moudles.main.home.mall.order.present.CheckConfirmView;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.shippingaddress.ShippingAddressActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.Utils;
import com.yanzhenjie.recyclerview.SwipeRecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\5\21 0021 15:58
 * @Author : yuan
 * @Describe ：订单-确认
 */
public class CheckConfirmActivity extends BaseActivity<CheckConfirmView, CheckConfirmPresent> implements CheckConfirmView {

    @BindView(R.id.tv_title)
    TextView mTvTitle;

    @BindView(R.id.check_order_recycle)
    SwipeRecyclerView mCheckOrderRecycle;
    @BindView(R.id.check_order_freight)//运费
            TextView mCheckOrderFreight;
    @BindView(R.id.check_order_price)
    TextView mCheckOrderPrice;
    @BindView(R.id.check_order_name)
    TextView mCheckOrderName;
    @BindView(R.id.check_order_phone)
    TextView mCheckOrderPhone;
    @BindView(R.id.check_order_address)
    TextView mCheckOrderAddress;
    @BindView(R.id.check_order_total)
    TextView mCheckOrderTotal;
    @BindView(R.id.check_order_next)
    TextView mCheckOrderNext;
    private GoAddOrderBean mDetails;
    private ShopCarSubmitBean submitData;
    List<CheckConfirmListBean> mConfirmListBeans;
    private Bundle mExtras;
    private String addressId = "";
    private String commodityspecid = "";
    private String name = "";
    private String phone = "";
    private String address = "";
    private String mShopcarid;
    private AllPriceBean mAllPriceBean;
    private Double mTotalPrice;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_confirm;
    }

    @Override
    public CheckConfirmPresent initPresenter() {
        return new CheckConfirmPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mTvTitle.setText(getString(R.string.check_order_title));
        mExtras = getIntent().getExtras();

        //buy_type 判断跳转页面 0是从商品详情购买跳转，1是购物车购买跳转
        if (mExtras.getInt("buy_type") == 0) {
            mDetails = (GoAddOrderBean) mExtras.get("details");
            mConfirmListBeans = new ArrayList<>();
            CheckConfirmListBean bean = new CheckConfirmListBean();
            bean.img = mDetails.product.smallimgurl;
            bean.name = mDetails.product.name;
            bean.price = mDetails.product.hyprice;
            bean.specification = mDetails.product.typename;
            bean.num = mDetails.product.num;
            mConfirmListBeans.add(bean);

            if (null != mDetails.addressParams) {
                name = mDetails.addressParams.surname +mDetails.addressParams.name;
                phone = mDetails.addressParams.tel;
                address = mDetails.addressParams.countryName + mDetails.addressParams.provincename + mDetails.addressParams.cityName + mDetails.addressParams.address;
                addressId = mDetails.addressParams.useraddress_id;
                presenter.getFee(this, mDetails.addressParams.useraddress_id, mDetails.product.commodityspecid);
            }
            mTotalPrice = (Double.parseDouble(mDetails.product.hyprice) * Integer.parseInt(mDetails.product.num));
            mCheckOrderPrice.setText(mTotalPrice + " USDS");
        } else {
            submitData = (ShopCarSubmitBean) getIntent().getExtras().get("submitData");
            mShopcarid = getIntent().getExtras().getString("mShopcarid");
            mConfirmListBeans = new ArrayList<>();
            mTotalPrice = 0.0;
            for (int i = 0; i < submitData.product.size(); i++) {
                CheckConfirmListBean bean = new CheckConfirmListBean();
                bean.img = submitData.product.get(i).smallimgurl;
                bean.name = submitData.product.get(i).name;
                bean.price = submitData.product.get(i).hyprice;
                bean.specification = submitData.product.get(i).typename;
                bean.num = submitData.product.get(i).number + "";
                mConfirmListBeans.add(bean);
                mTotalPrice += (Double.parseDouble(submitData.product.get(i).hyprice) * submitData.product.get(i).number);
                if (i == 0) {
                    commodityspecid = submitData.product.get(i).commodityspecid;
                } else {
                    commodityspecid += "," + submitData.product.get(i).commodityspecid;
                }

            }
            if (null != submitData.addressParams) {
                name = submitData.addressParams.surname+submitData.addressParams.name;
                phone = submitData.addressParams.tel;
                address = submitData.addressParams.countryName + submitData.addressParams.provincename + submitData.addressParams.cityName + submitData.addressParams.address;
                addressId = submitData.addressParams.useraddress_id;
                presenter.getFee(this, submitData.addressParams.useraddress_id, commodityspecid);
            }

            mCheckOrderPrice.setText(mTotalPrice + " USDS");


        }
        mCheckOrderName.setText(name);
        mCheckOrderPhone.setText(phone);
        mCheckOrderAddress.setText(address);
        mCheckOrderRecycle.setLayoutManager(new LinearLayoutManager(CheckConfirmActivity.this));
        CommonAdapter adapter = AdapterManger.getCheckOrderListAdapter(CheckConfirmActivity.this, mCheckOrderRecycle, mConfirmListBeans);
        mCheckOrderRecycle.setAdapter(adapter);
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }

    private String mMainAccount = Utils.getSpUtils().getString("mainAccount");

    @OnClick({R.id.iv_back, R.id.check_order_address_more, R.id.check_order_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.check_order_next:
                if(null==addressId || "".equals(addressId)){
                    AlertDialogShowUtil.toastMessage(CheckConfirmActivity.this, getResources().getString(R.string.sddress_no));
                    return;
                }
                if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                    if (mExtras.getInt("buy_type") == 0) {//详情下单
                        presenter.goodsOrder(this, mDetails.product.commodityspecid, 1 + "", mDetails.product.num, addressId, "");
                    } else {//购物车下单
                        showProgress();
                        presenter.goodsCarOrder(this, mShopcarid, 1 + "", addressId, "");
                    }

                } else {//未导入私钥 ，去设置
                    Dialog dialog = new Dialog(CheckConfirmActivity.this, R.style.MyDialog);
                    View inflate = LayoutInflater.from(CheckConfirmActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                    dialog.setContentView(inflate);
                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putString("accountName", mMainAccount);
                            ActivityUtils.next(CheckConfirmActivity.this, ImportAccountActivity.class, bundle, false);
                        }
                    });
                    dialog.show();
                }

                break;
            case R.id.check_order_address_more:
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.check_order_address_more);
                if (!fastDoubleClick) {
                    Intent intent = new Intent(CheckConfirmActivity.this, ShippingAddressActivity.class);
                    intent.putExtra("type", 1);
                    startActivityForResult(intent, 1001);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            if (null != data) {
                addressId = data.getStringExtra("addressId");
                mCheckOrderName.setText(data.getStringExtra("name"));
                mCheckOrderPhone.setText(data.getStringExtra("phone"));
                mCheckOrderAddress.setText(data.getStringExtra("address"));
                if (mExtras.getInt("buy_type") == 0) {
                    presenter.getFee(this, addressId, mDetails.product.commodityspecid);
                } else {
                    presenter.getFee(this, addressId, commodityspecid);
                }
            }
        }
    }


    @Override
    public void onGetFeeSuccess(FreightBean bean) {
        mCheckOrderFreight.setText(bean.sumyunfei + " USDS");//运费
        if (bean.sumyunfei.equals("0")) {
            mCheckOrderTotal.setText(mTotalPrice + " USDS");
        } else {
            mCheckOrderTotal.setText(mTotalPrice + Double.valueOf(bean.sumyunfei) + " USDS");
        }
    }


    @Override
    public void onGoodsBuySuccess(GoodsOrderBean bean) {
        hideProgress();
        Bundle bundle = new Bundle();
        bundle.putString("allmoney_ngk", bean.allmoney_ngk);
        bundle.putString("allmoney_usdn", bean.allmoney_usdn);
        bundle.putString("NGK_RATIO", bean.NGK_RATIO);
        bundle.putString("orderid", bean.orderid);
        ActivityUtils.next(CheckConfirmActivity.this, CheckPayActivity.class, bundle, true);
    }

    @Override
    public void onGoodsCarBuySuccess(GoodsOrderBean bean) {
        hideProgress();
        EventBus.getDefault().post("order_refresh");
        Bundle bundle = new Bundle();
        bundle.putString("allmoney_ngk", bean.allmoney_ngk);
        bundle.putString("allmoney_usdn", bean.allmoney_usdn);
        bundle.putString("NGK_RATIO", bean.NGK_RATIO);
        bundle.putString("orderid", bean.orderid);
        ActivityUtils.next(CheckConfirmActivity.this, CheckPayActivity.class, bundle, true);
    }

    @Override
    public void onFailure() {
        hideProgress();
    }
}
