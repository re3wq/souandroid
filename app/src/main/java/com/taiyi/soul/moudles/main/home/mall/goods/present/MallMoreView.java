package com.taiyi.soul.moudles.main.home.mall.goods.present;

import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.GoodsDetailBean;
import com.taiyi.soul.moudles.main.home.mall.bean.MallMoreBean;
import com.taiyi.soul.moudles.main.home.mall.goods.GoAddOrderBean;
import com.taiyi.soul.moudles.main.home.mall.goods.SearchPriceBean;

public interface MallMoreView {

    void onSuccess(MallMoreBean bean);
    void onSearchPriceSuccess(SearchPriceBean bean);
    void onGoodsSuccess(GoodsDetailBean bean);
    void onAddCarSuccess(String bean);
    void onAddOrderSuccess(GoAddOrderBean bean);
    void onCarSizeSuccess(CarListBean bean);
    void onFailure();
}
