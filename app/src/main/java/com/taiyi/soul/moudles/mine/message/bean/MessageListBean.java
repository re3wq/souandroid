package com.taiyi.soul.moudles.mine.message.bean;

import java.io.Serializable;
import java.util.List;

public class MessageListBean implements Serializable {


    /**
     * total : 2
     * list : [{"id":3,"userId":15941762687836420,"url":"9","createTime":"2020-07-09 21:21:34","type":1,"isRead":2,"title":"9","abstrac":"9","content":"9","langu":"zh"},{"id":1,"userId":15941762687836420,"url":"6666","createTime":"2020-05-26 14:46:01","type":1,"isRead":1,"title":"端午","abstrac":"我是概述","content":"端午节日快乐","langu":"zh"}]
     */

    private int total;
    private List<ListBean> list;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean implements Serializable {
        /**
         * id : 3
         * userId : 15941762687836420
         * url : 9
         * createTime : 2020-07-09 21:21:34
         * type : 1
         * isRead : 2
         * title : 9
         * abstrac : 9
         * content : 9
         * langu : zh
         */

        private long id;
        private long userId;
        private String url;
        private String createTime;
        private int type;
        private int isRead;
        private String title;
        private String abstrac;
        private String content;
        private String langu;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAbstrac() {
            return abstrac;
        }

        public void setAbstrac(String abstrac) {
            this.abstrac = abstrac;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getLangu() {
            return langu;
        }

        public void setLangu(String langu) {
            this.langu = langu;
        }
    }
}
