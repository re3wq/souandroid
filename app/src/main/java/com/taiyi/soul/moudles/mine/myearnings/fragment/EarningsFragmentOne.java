package com.taiyi.soul.moudles.mine.myearnings.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.container.DefaultHeader;
import com.liaoinstan.springview.widget.SpringView;
import com.othershe.calendarview.bean.DateBean;
import com.othershe.calendarview.listener.OnPagerChangeListener;
import com.othershe.calendarview.listener.OnSingleChooseListener;
import com.othershe.calendarview.weiget.CalendarView;
import com.othershe.calendarview.weiget.WeekView;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.bean.MyEarningsBean;
import com.taiyi.soul.moudles.mine.bean.MyEarningsListBean;
import com.taiyi.soul.moudles.mine.myearnings.MyEarningsPresent;
import com.taiyi.soul.moudles.mine.myearnings.MyEarningsView;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.MyLineChartView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class EarningsFragmentOne extends BaseFragment<MyEarningsView, MyEarningsPresent> implements MyEarningsView, SpringView.OnFreshListener, View.OnClickListener, OnPagerChangeListener, OnSingleChooseListener {
    @BindView(R.id.totalRevenueTv)
    TextView totalRevenueTv;
    @BindView(R.id.thisMonthEarningsTv)
    TextView thisMonthEarningsTv;
    @BindView(R.id.dayGainTv)
    TextView dayGainTv;
    @BindView(R.id.valueTv)
    TextView valueTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.springView)
    SpringView springView;
    @BindView(R.id.lineChart)
    LineChart lineChart;
    @BindView(R.id.lineChartView)
    MyLineChartView lineChartView;
    @BindView(R.id.noDataLl)
    LinearLayout noDataLl;
    @BindView(R.id.noIncomeRecordTv)
    TextView noIncomeRecordTv;
    CalendarView calendarView;
    @BindView(R.id.topIv)
    ImageView topIv;
    @BindView(R.id.bottomIv)
    ImageView bottomIv;
    @BindView(R.id.rlChartView)
    RelativeLayout rlChartView;
    WeekView weekView;
    TextView dateTv, cancelTv, determineTv;
    ImageView leftIv, rightIv;
    List<MyEarningsListBean.ListBean> incomeList = new LinkedList<>();
    private int pageNo = 1;
    private int total;
    private Dialog calendarDialog = null;
    private String selectDate = "";
    List<String> xValues;   //x轴数据集合
    List<Float> yValues;  //y轴数据集合
    @BindView(R.id.lineChart1)
    LineChart lineChart1;
    public static EarningsFragment getInstance(String source) {
        EarningsFragment earningsFragment = new EarningsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("source", source);
        earningsFragment.setArguments(bundle);
        return earningsFragment;
    }
    //折线初始化
    @SuppressLint("NewApi")
    private void initLine(List<MyEarningsBean.WeeksumBean> assetsBean) {
        if (null == assetsBean || 0 == assetsBean.size()) {
            return;
        }
        //得到图例
        Legend legend = lineChart1.getLegend();  //得到图例
        legend.setForm(Legend.LegendForm.EMPTY);  //设置图例形状为圆形
        legend.setOrientation(Legend.LegendOrientation.VERTICAL);  //设置图例条目垂直排列


        //设置X轴数据
        XAxis xAxis = lineChart1.getXAxis();
        xAxis.setValueFormatter(mFormatter);
        xAxis.setTextSize(9f);
        xAxis.setTextColor(getContext().getResources().getColor(R.color.color_5e6164));
        xAxis.setDrawGridLines(false); //是否显示X坐标轴上的刻度竖线，默认是true
        xAxis.setDrawAxisLine(true); //是否绘制坐标轴的线，即含有坐标的那条线，默认是true
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(8,false);
        xAxis.setYOffset(8f);
        lineChart1.setExtraOffsets(0, 0, 0, 12);
        //设置Y轴left
        YAxis axisLeft = lineChart1.getAxisLeft();
        axisLeft.setAxisMinimum(0f);
        axisLeft.setGranularity(1f);
        axisLeft.setGridColor(getContext().getResources().getColor(R.color.black));//设置网格线颜色（横线）
        axisLeft.setGridLineWidth(1f);
        axisLeft.setLabelCount(8,false);
        //设置整个line描述信息
//        Description description = new Description();
//        description.setText("");
//        chartView.setDescription(description);//设置描述信息

        //设置LainView属性
        lineChart1.getDescription().setEnabled(false);  //隐藏描述
        lineChart1.setNoDataText(""); // 设置当 chart 为空时显示的描述文字
        lineChart1.getAxisLeft().setDrawLabels(false);// 设置左边的label不可用
        lineChart1.getAxisLeft().setDrawAxisLine(false); // 设置右边的线不可用
        lineChart1.getAxisRight().setEnabled(false);  // 隐藏右边 的坐标轴
        lineChart1.getAxisRight().setDrawLabels(false);// 设置右边的label不可用
        lineChart1.getAxisRight().setDrawGridLines(false); // 设置右边的线不可用
        lineChart1.getAxisRight().setDrawAxisLine(false); // 设置右边的线不可用
        lineChart1.setScaleEnabled(false);  //禁止缩放
        lineChart1.setTouchEnabled(false);  //禁止触摸
        lineChart1.setDragEnabled(false);
        lineChart1.setDrawGridBackground(false);//禁止网格

        lineChart1.setHighlightPerDragEnabled(false);

        lineChart1.setBackgroundColor(Color.TRANSPARENT);
        //设置图表距离上下左右的距离
//        lineChart1.setExtraOffsets(0, 0, 0, 0);
        //从X轴进入的动画
        lineChart1.animateX(1000);
        // add data
        setData(assetsBean.size(), assetsBean);
        lineChart1.invalidate();
    }

    //折线数据
    private void setData(int count, List<MyEarningsBean.WeeksumBean> assetsBean) {



        //数据二
        ArrayList<Entry> yVals2 = new ArrayList<>();
        float big1 = 0f;
        for (int i = 0; i < count; i++) {
            Drawable drawable = ContextCompat.getDrawable(getContext(), R.mipmap.mine_progressbar_current_iv);
            yVals2.add(new Entry(i, Float.parseFloat(assetsBean.get(i).getMoneysum()), drawable));
            if (Float.parseFloat(assetsBean.get(i).getMoneysum()) > big1) {
                big1 = Float.parseFloat(assetsBean.get(i).getMoneysum());
            }
        }


        //将数据放入折线图
        LineDataSet set2;
        if (lineChart1.getData() != null &&
                lineChart1.getData().getDataSetCount() > 0) {

            set2 = (LineDataSet) lineChart1.getData().getDataSetByIndex(0);
            set2.setValues(yVals2);
            lineChart1.getData().notifyDataChanged();
            lineChart1.notifyDataSetChanged();
        } else {


            set2 = new LineDataSet(yVals2, "");
            set2.setColor(getContext().getResources().getColor(R.color.color_fe2540));
//            set2.setCircleColor(getContext().getResources().getColor(R.color.color_b7957f));
            set2.setLineWidth(2f);
            set2.setCircleRadius(3f);
            set2.setFillAlpha(45);
            set2.setDrawCircleHole(true);

            ArrayList<ILineDataSet> allLinesList = new ArrayList<>();
            allLinesList.add(set2);

            //LineData表示一个LineChart的所有数据(即一个LineChart中所有折线的数据)
            LineData data = new LineData(allLinesList);
            data.setDrawValues(false);//折线图不显示数值
            data.setHighlightEnabled(false);
            initNum();
            if (big1 == 0) {
                lineChart1.getAxisLeft().setAxisMinimum(0f);//设置y轴的最小值
                lineChart1.getAxisLeft().setAxisMaximum(0f);//设置y轴的最小值
            }
            lineChart1.setData(data);
        }
    }

    ValueFormatter mNumFormatter;
    ValueFormatter mFormatter;
    //x周日期
    private void initNum() {

        mNumFormatter = new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                String st = "0";
                String tim1 = "";
                tim1 = st;
                return tim1;
            }
        };


    }
    //x周日期
    private void initLineTwo(List<MyEarningsBean.WeeksumBean> assetsBean) {


        if (null == assetsBean || 0 == assetsBean.size()) {
            return;
        }

        mFormatter = new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                int v = (int) value;
                if (v < assetsBean.size() && v >= 0) {
                    String[] split = assetsBean.get(v).getDatetime().split("-");
                    String st = split[1] + "-" + split[2];
                    String tim1 = "";
                    tim1 = st;
                    return tim1;
                } else {
                    return null;
                }
            }
        };


    }
    @Override
    public MyEarningsPresent initPresenter() {
        return new MyEarningsPresent();
    }


    @Override
    protected void initViews(Bundle savedInstanceState) {
        springView.setFooter(new DefaultFooter(getContext()));
        springView.setHeader(new DefaultHeader(getContext()));
        springView.setGive(SpringView.Give.BOTH);
        springView.setType(SpringView.Type.FOLLOW);
        springView.setListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        CommonAdapter myEarningsListAdapter = AdapterManger.getMyEarningsListAdapter(getContext(), incomeList);
        recyclerView.setAdapter(myEarningsListAdapter);
//        initLineChart(weeksum);
    }




    private boolean isFirst = true;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isFirst) {

            isFirst = false;
        }
    }

    @Override
    protected void initData() {
//        showProgress();
//        initChart(null);
        presenter.getList(pageNo, "", "2");
        presenter.getMyEarnings("2");
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.my_earnings_view_pager_layout;
    }

    @Override
    public void getListSuccess(MyEarningsListBean myEarningsListBean) {
        springView.onFinishFreshAndLoad();
        total = myEarningsListBean.getTotal();
        hideProgress();
        List<MyEarningsListBean.ListBean> list = myEarningsListBean.getList();
        if (pageNo == 1)
            incomeList.clear();
        incomeList.addAll(list);
        recyclerView.getAdapter().notifyDataSetChanged();
        if (incomeList.size() == 0) {
            noDataLl.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            topIv.setVisibility(View.GONE);
            bottomIv.setVisibility(View.GONE);
        } else {
            noDataLl.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            topIv.setVisibility(View.VISIBLE);
            bottomIv.setVisibility(View.VISIBLE);
        }

    }

    @OnClick({R.id.dateIv})
    public void onClicked(View v) {
        switch (v.getId()) {
            case R.id.dateIv:
                if (calendarDialog == null) {
                    calendarDialog = new Dialog(getContext(), R.style.MyDialog);
                    View view = LayoutInflater.from(getContext()).inflate(R.layout.calendar_layout, null);
                    calendarDialog.setContentView(view);
                    WindowManager.LayoutParams layoutParams = calendarDialog.getWindow().getAttributes();
                    layoutParams.gravity = Gravity.BOTTOM;
                    layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    calendarDialog.getWindow().setAttributes(layoutParams);


                    calendarView = view.findViewById(R.id.calendarView);
                    weekView = view.findViewById(R.id.weekView);
                    dateTv = view.findViewById(R.id.dateTv);
                    cancelTv = view.findViewById(R.id.cancelTv);
                    determineTv = view.findViewById(R.id.determineTv);
                    leftIv = view.findViewById(R.id.leftIv);
                    rightIv = view.findViewById(R.id.rightIv);
                    leftIv.setOnClickListener(this);
                    rightIv.setOnClickListener(this);
                    cancelTv.setOnClickListener(this);
                    determineTv.setOnClickListener(this);
                    weekView.setBackgroundColor(getContext().getResources().getColor(R.color.transparent));
                    calendarView.setBackgroundColor(getContext().getResources().getColor(R.color.transparent));
                    calendarView.setBackground(null);
                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day =calendar.get(Calendar.DAY_OF_MONTH);
                    dateTv.setText(year + " / " + (month > 9 ? month : "0" + month));
                    calendarView
                            .setStartEndDate("2020.6", "2022.12")
                            .setInitDate("2020.6")
                            .setSingleDate(year+"."+month+"."+day)
                            .init();
                    calendarView.setOnPagerChangeListener(this);
                    calendarView.setOnSingleChooseListener(this);
                }
                calendarDialog.show();
                break;
        }
    }

    @Override
    public void getListFailure(String errorMsg) {
        springView.onFinishFreshAndLoad();
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void loginInvalid(String msg) {
        springView.onFinishFreshAndLoad();
        hideProgress();
        if(Constants.isL==0) {
            Constants.isL = 1;
            ToastUtils.showShortToast(msg);
            //  AppManager.getAppManager().finishAllActivity();
            startActivity(new Intent(getActivity(), LoginActivity.class));
            Utils.getSpUtils().remove(Constants.TOKEN);
        }
    }

    @Override
    public void getEarningsSuccess(MyEarningsBean myEarningsBean) {
        totalRevenueTv.setText(myEarningsBean.getTotalRevenue() + " SOU");
        thisMonthEarningsTv.setText(myEarningsBean.getMouthsum() + " SOU");
        dayGainTv.setText(myEarningsBean.getDaysum());
        String symbol = Utils.getSpUtils().getString("symbol");
        if(myEarningsBean.getConvert().contains(symbol)){
            valueTv.setText("≈" + myEarningsBean.getConvert());
        }else {
            valueTv.setText("≈" + symbol + myEarningsBean.getConvert());
        }
//        valueTv.setText("≈￥" + myEarningsBean.getConvert());

        List<MyEarningsBean.WeeksumBean> weeksum = myEarningsBean.getWeeksum();
//        initLineChart(weeksum);
//        initChart(weeksum);
        initLineTwo(myEarningsBean.getWeeksum());
        initLine(myEarningsBean.getWeeksum());
    }

    @Override
    public void getEarningsFailure(String errorMsg) {
        toast(errorMsg);
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        showProgress();
        presenter.getList(pageNo, "", "2");
        presenter.getMyEarnings("2");
    }

    @Override
    public void onLoadmore() {
//        if (total == 0 || pageNo == total) {
//            toast("暂无更多");
//            return;
//        }
        pageNo++;
        showProgress();
        presenter.getList(pageNo, "", "2");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.leftIv:
                calendarView.lastMonth();
                break;
            case R.id.rightIv:
                calendarView.nextMonth();
                break;
            case R.id.cancelTv:
                calendarDialog.dismiss();
                break;
            case R.id.determineTv:
                calendarDialog.dismiss();
                pageNo = 1;
                showProgress();
                presenter.getList(pageNo, selectDate, "2");

                break;
        }
    }

    @Override
    public void onPagerChanged(int[] date) {
        int i = date[1];
//        Log.i("currentMonth", "---year---" + date[0] + "---month---" + date[1]);
        if (i < 10)
            dateTv.setText(date[0] + " / 0" + date[1]);
        else
            dateTv.setText(date[0] + " / " + date[1]);
    }

    @Override
    public void onSingleChoose(View view, DateBean date) {
        int[] solar = date.getSolar();
        String month = null, day = null;
        if (solar[1] < 10) {
            month = "0" + solar[1];
        } else {
            month = "" + solar[1];
        }
        if (solar[2] < 10) {
            day = "0" + solar[2];
        } else {
            day = "" + solar[2];
        }
        selectDate = solar[0] + "-" + month + "-" + day;
//        Log.i("dddddddddd", "当前选择--" + selectDate);
    }
}
