package com.taiyi.soul.moudles.mine.securitycenter.settransactionpwd;

import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.View;
import android.widget.TextView;

import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import butterknife.BindView;
import butterknife.OnClick;

public class SetTransactionPwdActivity extends BaseActivity<NormalView, NormalPresenter> {
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.newPwdTv)
    TextView newPwdTv;
    @BindView(R.id.pwdEditText)
    MNPasswordEditText pwdEditText;
    @BindView(R.id.nextStepTv)
    TextView nextStepTv;
    //    @BindView(R.id.confirmPwdEt)
//    PwdEditText pwdEditText;
    @BindView(R.id.top)
    ConstraintLayout top;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_set_transaction_pwd;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.set_transaction_password));
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.nextStepTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.nextStepTv:
                break;
        }
    }
}
