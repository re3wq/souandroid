package com.taiyi.soul.moudles.main.assets.exchange.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondRecordBean;
import com.taiyi.soul.utils.DensityUtil;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class BondRecordAdapter extends BaseRecyclerAdapter<BondRecordBean.ComlistBean> {


    private PullRecyclerView pullRecyclerView;
    private Context context;

    public BondRecordAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_exchange_bond;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final BondRecordBean.ComlistBean data) {

        //num
        //money
        //create_time
        //type(0 赎回债券   1 兑换债券 )
        View views = getView(holder, R.id.views);
        LinearLayout item_view = getView(holder, R.id.item_view);
        TextView bond_type = getView(holder, R.id.bond_type);//兑换类型
        TextView bond_time = getView(holder, R.id.bond_time);//时间
        TextView bond_rate = getView(holder, R.id.bond_rate);//汇率
        TextView bond_add = getView(holder, R.id.bond_add);//债券
        TextView bond_exchange = getView(holder, R.id.bond_exchange);//USDN

        if (position == 0) {
            views.setVisibility(View.GONE);
            item_view.setPadding(DensityUtil.dip2px(context,20),0,DensityUtil.dip2px(context,20), 0);

        } else {
            views.setVisibility(View.VISIBLE);
        }
        if(data.positotals==position){
            item_view.setBackgroundResource(R.mipmap.bond_exchange_bg_bottom);
            item_view.setPadding(DensityUtil.dip2px(context,20),0,DensityUtil.dip2px(context,20),0);
        }else {
            item_view.setBackgroundResource(R.mipmap.bond_exchange_bg_min);

        }
        bond_type.setText(data.order_name);
        bond_time.setText(data.createtime);
        bond_rate.setText(context.getResources().getString(R.string.exchanges_rate2) + data.price);
        bond_exchange.setText(data.money + " " + context.getResources().getString(R.string.USDN));
        bond_add.setText(data.num);


    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }

}

