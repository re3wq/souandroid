package com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.text.InputFilter;
import android.text.TextUtils;
import android.text.method.NumberKeyListener;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.gen.WalletBeanDao;
import com.taiyi.soul.moudles.login.LoginActivity;

import com.taiyi.soul.moudles.login.bean.UserInfoBean;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.EncryptUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.countdowntimer.CountDownTimerUtils;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class UpdateLoginPasswordActivity extends BaseActivity<UpdateLoginPasswordView, UpdateLoginPasswordPresent> implements UpdateLoginPasswordView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.logoIv)
    ImageView logoIv;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.emailAddressEt)
    TextView emailAddressEt;
    @BindView(R.id.verificationCodeEt)
    EditText verificationCodeEt;
    @BindView(R.id.getVerificationCodeTv)
    TextView getVerificationCodeTv;
    @BindView(R.id.ll)
    LinearLayout ll;
    @BindView(R.id.newPwdEt)
    EditText newPwdEt;
    @BindView(R.id.confirmPwdEt)
    EditText confirmPwdEt;
    @BindView(R.id.ll_next)
    LinearLayout llNext;
    @BindView(R.id.nextStepTv)
    TextView nextStepTv;
    @BindView(R.id.top)
    ConstraintLayout top;
    private int flag = 1;
    private int type;
    private String account;
    private String ac_privateKey;
    private String ow_privateKey;
    private int loginType;
    private String userInfo;


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_update_login_password;
    }

    @Override
    public UpdateLoginPasswordPresent initPresenter() {
        return new UpdateLoginPasswordPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        type = getIntent().getExtras().getInt("type");
        loginType = Utils.getSpUtils().getInt("loginType");
        if (loginType == 1) {//邮箱
            account = Utils.getSpUtils().getString("email");
        } else {
            account = Utils.getSpUtils().getString("phoneNumber");

        }
        emailAddressEt.setText(account);
        if (type == 1) {
            titleTv.setText(getString(R.string.update_login_password));
            tv.setText("ENTER THE FOLLOWING INFORMATION TO\nMODIFY THE LOGIN PASSWORD");
            userInfo = Utils.getSpUtils().getString("userInfo");
            newPwdEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        String loginPwd = newPwdEt.getText().toString();
                        if (!TextUtils.isEmpty(loginPwd)) {
                            if (loginPwd.length() < 8) {//位数不足八位
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint1));
//                            toast("登录密码不足8位");
                            } else if (loginPwd.matches("^[0-9]+$")) {//输入的纯数字为弱
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                            } else if (loginPwd.matches("^[a-z]+$")) {//输入的纯小写字母为弱
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                            } else if (loginPwd.matches("^[A-Z]+$")) {//输入的纯大写字母为弱
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                            } else if (loginPwd.matches("^[A-Za-z]+$")) {//输入的大写字母和小写字母为中
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                            } else if (loginPwd.matches("^[a-z0-9]+$")) {//输入的小写字母和数字为中
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                            } else if (loginPwd.matches("^[A-Z0-9]+$")) {//输入的大写字母和数字为中
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                            } else if (loginPwd.matches("^[A-Za-z0-9]+$")) {//输入的大写字母和小写字母和数字为强
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint4));
//                            toast("密码安全系数为高级");
                            } else {//其他视为高强度
                                AlertDialogShowUtil.toastMessage(UpdateLoginPasswordActivity.this, getResources().getString(R.string.password_hint4));
//                            toast("密码安全系数为高级");
                            }

                        }
                    }
                }
            });
        } else {
            titleTv.setText(getString(R.string.set_transaction_password));
            tv.setText("ENTER THE FOLLOWING INFORMATION TO\nMODIFY THE TRANSACTION PASSWORD");
//            newPwdEt.setKeyListener(new DigitsKeyListener(false,false));
            newPwdEt.setKeyListener(new NumberKeyListener() {
                @Override
                protected char[] getAcceptedChars() {
                    return new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
                }

                @Override
                public int getInputType() {
                    // TODO Auto-generated method stub
                    return android.text.InputType.TYPE_CLASS_PHONE;
                }
            });
            newPwdEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
            confirmPwdEt.setKeyListener(new NumberKeyListener() {
                @Override
                protected char[] getAcceptedChars() {
                    return new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
                }

                @Override
                public int getInputType() {
                    // TODO Auto-generated method stub
                    return android.text.InputType.TYPE_CLASS_PHONE;
                }
            });
            confirmPwdEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.getVerificationCodeTv, R.id.nextStepTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.getVerificationCodeTv:
                showProgress();
                if (loginType == 1) {
                    presenter.getVerificationCode(this, account, "");
                } else {
                    String areaCode = Utils.getSpUtils().getString(Constants.AREA_CODE);
                    presenter.getVerificationCode(this, account, areaCode);
                }
                break;
            case R.id.nextStepTv:
                if (flag == 1) {
                    String verificationCode_ = verificationCodeEt.getText().toString();
                    if (TextUtils.isEmpty(verificationCode_)) {
                        toast(getString(R.string.code_input));
                        return;
                    }
                    showProgress();
                    if (loginType == 1) {
                        presenter.checkVerificationCode(this, "", account, verificationCode_);
                    } else {
                        String areaCode = Utils.getSpUtils().getString(Constants.AREA_CODE);
                        presenter.checkVerificationCode(this, areaCode, account, verificationCode_);
                    }
                } else {
                    String newPwd = newPwdEt.getText().toString();
                    String confirmPwd = confirmPwdEt.getText().toString();
                    if (TextUtils.isEmpty(newPwd) || TextUtils.isEmpty(confirmPwd)) {
                        toast(getString(R.string.password_not_blank));
                        return;
                    }
                    if (type == 1) {
                        if (newPwd.length() < 8) {
                            toast(getResources().getString(R.string.password_hint1));
                            return;
                        }
                        if (confirmPwd.length() < 8) {
                            toast(getResources().getString(R.string.password_hint1));
                            return;
                        }
                    } else {//修改交易密码
                        if (newPwd.length() != 6) {
                            toast(getResources().getString(R.string.need_six));
                            return;
                        }
                        if (confirmPwd.length() != 6) {
                            toast(getResources().getString(R.string.need_six));
                            return;
                        }

                    }


                    if (!TextUtils.equals(newPwd, confirmPwd)) {
                        toast(getResources().getString(R.string.is_different));
                        return;
                    }


                    String verificationCode = verificationCodeEt.getText().toString();
                    showProgress();
                    presenter.updatePwd(this, account, newPwd, verificationCode, type == 2);
                }

//
//                if (flag == 1) {
//                    String verificationCode = verificationCodeEt.getText().toString();
//                    if (TextUtils.isEmpty(verificationCode)) {
//                        toast("验证码不能为空");
//                        return;
//                    }
//                    flag = 2;
//                    nextStepTv.setText(getString(R.string.confirm_));
//                    ll.setVisibility(View.GONE);
//                    llNext.setVisibility(View.VISIBLE);
//                } else {
//                    String newPwd = newPwdEt.getText().toString();
//                    String confirmPwd = confirmPwdEt.getText().toString();
//                    if (TextUtils.isEmpty(newPwd) || TextUtils.isEmpty(confirmPwd)) {
//                        toast("密码不能为空");
//                        return;
//                    }
//                    if (!TextUtils.equals(newPwd, confirmPwd)) {
//                        toast("两次密码输入不一致");
//                        return;
//                    }
//
//                    String verificationCode = verificationCodeEt.getText().toString();
//                    showProgress();
//                    presenter.updatePwd(account, newPwd, verificationCode, type == 2);
//                }
                break;
        }
    }

    @Override
    public void getVerificationCodeSuccess() {
        hideProgress();
        CountDownTimerUtils countDownTimerUtils = new CountDownTimerUtils(this, getVerificationCodeTv, 60000, 1000, "#a5a9ac");
        countDownTimerUtils.start();

        getVerificationCodeTv.setFocusable(true);
        getVerificationCodeTv.setFocusableInTouchMode(true);
        getVerificationCodeTv.requestFocus();
    }

    @Override
    public void getVerificationCodeFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void updateSuccess(String msg) {
        hideProgress();
        if (type == 1) {
            //更改數據庫
            WalletBean walletBean = null;
            ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
            int loginType = Utils.getSpUtils().getInt("loginType");
            //通过手机号和钱包类型查询有无账户
            if (loginType == 1) {
                List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(Utils.getSpUtils().getString("email", ""))).build().list();
                if (null!=list) {
                    if(list.size()>0)
                    walletBean = list.get(list.size()-1);
                }
//                walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(Utils.getSpUtils().getString("email", ""))).build().unique();
            } else {
                List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(Utils.getSpUtils().getString("phoneNumber", ""))).build().list();
                if (null!=list) {
                    if(list.size()>0)
                    walletBean = list.get(list.size()-1);
                }
//                walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(Utils.getSpUtils().getString("phoneNumber", ""))).build().unique();
            }
            if (walletBean != null) {

                if (walletBean.getAccount_info() != null) {
                    accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(walletBean.getAccount_info(), AccountInfoBean.class);
                }

                String accountName = Utils.getSpUtils().getString("mainAccount");
                for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
                    if (accountInfoBean.getAccount_name().equals(accountName)) {
                        String loginPwd = Utils.getSpUtils().getString("loginPwd");

                        try {
                            //通过原密码解出原私钥
                            ac_privateKey = EncryptUtil.getDecryptString(accountInfoBean.getAccount_active_private_key(), loginPwd);
                            ow_privateKey = EncryptUtil.getDecryptString(accountInfoBean.getAccount_owner_private_key(), loginPwd);
                            //通过新密码设置新私钥
                            String newPwd = newPwdEt.getText().toString();
                            Utils.getSpUtils().put("loginPwd",newPwdEt.getText().toString());
                            accountInfoBean.setAccount_active_private_key(EncryptUtil.getEncryptString(ac_privateKey, newPwd));
                            accountInfoBean.setAccount_owner_private_key(EncryptUtil.getEncryptString(ow_privateKey, newPwd));

                        } catch (NoSuchAlgorithmException e) {
                        } catch (InvalidKeySpecException e) {
                        }
                        break;
                    }
                }

                walletBean.setAccount_info(new Gson().toJson(accountInfoBeanArrayList));
                MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
                MyApplication.getInstance().setWalletBean(walletBean);

            }

            String newPwd = newPwdEt.getText().toString();
            List<UserInfoBean> list = new Gson().fromJson(userInfo, new TypeToken<List<UserInfoBean>>() {
            }.getType());
            int i = 0;
            for (UserInfoBean infoBean : list) {
                if (infoBean.getAccount().equals(account)) {
                    infoBean.setPwd(newPwd);
                }
            }
            Utils.getSpUtils().put("userInfo", new Gson().toJson(list));

           //  AppManager.getAppManager().finishAllActivity();
            ActivityUtils.next(this, LoginActivity.class, true);
        } else {
            finish();
        }
        toast(msg);

    }

    @Override
    public void updateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void checkSuccess() {
        hideProgress();
//        if (flag == 1) {
//            String verificationCode = verificationCodeEt.getText().toString();
//            if (TextUtils.isEmpty(verificationCode)) {
//                toast("验证码不能为空");
//                return;
//            }
        flag = 2;
        nextStepTv.setText(getString(R.string.confirm_));
        ll.setVisibility(View.GONE);
        llNext.setVisibility(View.VISIBLE);
//        }

    }

    @Override
    public void checkFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }
}
