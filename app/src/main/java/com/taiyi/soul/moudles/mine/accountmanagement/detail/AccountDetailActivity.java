package com.taiyi.soul.moudles.mine.accountmanagement.detail;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Handler;
import android.os.IBinder;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.gen.WalletBeanDao;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.accountmanagement.detail.importmnemonic.ImportMnemonicActivity;
import com.taiyi.soul.moudles.mine.accountmanagement.detail.importprivatekey.ImportPrivateKeyActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AESOperator;
import com.taiyi.soul.utils.CopyUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.EncryptUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AccountDetailActivity extends BaseActivity<AccountDetailView, AccountDetailPresent> implements AccountDetailView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.accountNameTv)
    TextView accountNameTv;
    @BindView(R.id.inviteCodeTv)
    TextView inviteCodeTv;
    @BindView(R.id.registerTimeTv)
    TextView registerTimeTv;
    @BindView(R.id.zwt)
    TextView zwt;
    @BindView(R.id.top)
    ConstraintLayout top;
    private String accountName, inviteCode, createTime;
    int type = 1;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_account_detail;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public AccountDetailPresent initPresenter() {
        return new AccountDetailPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.account_details));
        Bundle bundle = getIntent().getExtras();
        accountName = bundle.getString("accountName");
        inviteCode = bundle.getString("inviteCode");
        createTime = bundle.getString("createTime");

        if(null==inviteCode){
            inviteCodeTv.setVisibility(View.GONE);
            zwt.setVisibility(View.GONE);
        }else {
            zwt.setVisibility(View.VISIBLE);
            inviteCodeTv.setVisibility(View.VISIBLE);
        }
        accountNameTv.setText(accountName);
        inviteCodeTv.setText(inviteCode);
        registerTimeTv.setText(createTime);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }
    //显示密码框
    private void showPassword(View view) {
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInputp(viewById, ev,keyboardPlace)) {
                        hideSoftInputp(view.getWindowToken());
                    }
                }
                return false;
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(AccountDetailActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pay_popup.dismiss();
                            showProgress();
                            presenter.checkPayPassword(s);
                        }
                    },500);

                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom +DensityUtil.dip2px(AccountDetailActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {

                pay_popup.dismiss();


            }
        }
    }
    @OnClick({R.id.backIv, R.id.inviteCodeTv, R.id.importMnemonicTv, R.id.importPrivateKeyTv, R.id.deleteAccountTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.inviteCodeTv:
                toast(getString(R.string.copy_success));
                CopyUtils.CopyToClipboard(this, inviteCode);
                break;
            case R.id.importMnemonicTv:
                type = 1;
//                showPassword(view);
                checkMnemonic();
                break;
            case R.id.importPrivateKeyTv:
                type = 2;
//                showPassword(view);
                checkPrivateKey();
                break;
            case R.id.deleteAccountTv:
                type = 3;
                showPassword(view);

                break;
        }
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg) {
        hideProgress();
        if (code == 0) {
            if (type == 1) {
//                checkMnemonic();
                Bundle bundle = new Bundle();
                bundle.putString("accountName", accountName);
                ActivityUtils.next(this, ImportMnemonicActivity.class, bundle);
            } else if (type == 2) {
//                checkPrivateKey();
                Bundle bundle1 = new Bundle();
                bundle1.putString("accountName", accountName);
                ActivityUtils.next(this, ImportPrivateKeyActivity.class, bundle1);
            } else {
                WalletBean walletBean = null;
                ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
                int loginType = Utils.getSpUtils().getInt("loginType");
                //通过手机号和钱包类型查询有无账户
                if (loginType == 1) {
                    List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(Utils.getSpUtils().getString("email", ""))).build().list();
                    if (null!=list) {
                        if(list.size()>0)
                        walletBean = list.get(list.size()-1);
                    }
//                    walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(Utils.getSpUtils().getString("email", ""))).build().unique();
                }else {
                    List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(Utils.getSpUtils().getString("phoneNumber", ""))).build().list();
                    if (null!=list) {
                        if(list.size()>0)
                        walletBean = list.get(list.size()-1);
                    }
//                    walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(Utils.getSpUtils().getString("phoneNumber", ""))).build().unique();

                }
                if (walletBean != null) {

                    if (walletBean.getAccount_info() != null) {
                        accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(walletBean.getAccount_info(), AccountInfoBean.class);
                    }

                    String accountName = getIntent().getExtras().getString("accountName");
                    for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
                        if (accountInfoBean.getAccount_name().equals(accountName)) {
                            accountInfoBeanArrayList.remove(accountInfoBean);
                            finish();
                            break;
                        }
                    }
                    walletBean.setAccount_info(new Gson().toJson(accountInfoBeanArrayList));
                    MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
                    MyApplication.getInstance().setWalletBean(walletBean);
                    finish();

                }
            }
        } else {
            toast(msg);
        }
    }

    private void checkPrivateKey() {
        ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
        if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
            accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
        }

        String accountName = getIntent().getExtras().getString("accountName");
        for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
            if (accountInfoBean.getAccount_name().equals(accountName)) {
                String account_active_private_key =
                        accountInfoBean.getAccount_active_private_key().equals("") ?
                                accountInfoBean.getAccount_owner_private_key()
                                : accountInfoBean.getAccount_active_private_key();

                String loginPwd = Utils.getSpUtils().getString("loginPwd");
                try {
                    String privateKey = EncryptUtil.getDecryptString(account_active_private_key, loginPwd);

                    if (TextUtils.isEmpty(privateKey)) {
                        toast("助记词导入无法查看私钥");
                    } else {
                        showPassword(titleTv);
//                       Bundle bundle1 = new Bundle();
//                       bundle1.putString("accountName", accountName);
//                       ActivityUtils.next(this, ImportPrivateKeyActivity.class, bundle1);
                    }
                } catch (NoSuchAlgorithmException e) {
                } catch (InvalidKeySpecException e) {
                }

                break;
            }
        }
    }

    private void checkMnemonic() {
        ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
        if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
            accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
        }

        String accountName = getIntent().getExtras().getString("accountName");
        for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
            if (accountInfoBean.getAccount_name().equals(accountName)) {
                String mnemonic = accountInfoBean.getAccount_gen_mnemonic();
                String privateKey = AESOperator.getInstance().decrypt(mnemonic);
                if (TextUtils.isEmpty(privateKey)) {
                    toast("私钥导入无法查看助记词");
                } else {
                    showPassword(titleTv);
//                  Bundle bundle = new Bundle();
//                  bundle.putString("accountName", accountName);
//                  ActivityUtils.next(this, ImportMnemonicActivity.class, bundle);
                }
                break;
            }
        }
    }
}
