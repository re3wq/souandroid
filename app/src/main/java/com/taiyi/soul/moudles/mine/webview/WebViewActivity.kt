package com.taiyi.soul.moudles.mine.webview

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import com.taiyi.soul.R
import com.taiyi.soul.base.BaseActivity
import com.taiyi.soul.moudles.mine.bean.HelpCenterBean
import com.taiyi.soul.moudles.mine.bean.WebviewTextBean
import com.taiyi.soul.utils.Utils
import kotlinx.android.synthetic.main.activity_web_view_acitivity.*
import kotlinx.android.synthetic.main.base_top_layout.*

class WebViewActivity : BaseActivity<WebviewView, WebViewPresent>(), WebviewView {
    var type = 1

    override fun getLayoutId(): Int = R.layout.activity_web_view_acitivity

    override fun initPresenter(): WebViewPresent = WebViewPresent()

    override fun initViews(savedInstanceState: Bundle?) {
//        webView.setBackgroundColor(1) //设置背景色
//        webView.background.alpha = 1 //设置填充透明度（布局中一定要设置background，不然getbackground会是null）

        webView.apply {
//            setBackgroundColor(0)
//            background.alpha = 0
            settings.javaScriptEnabled=true
        }
        webView.webViewClient=object :WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return super.shouldOverrideUrlLoading(view, url)
            }
        }

        type = intent.getIntExtra("type", 1)
        when (type) {
            1 ->
                titleTv.text = getString(R.string.about_us)
            2 ->
                titleTv.text = getString(R.string.operation_guide)
            3 ->
                titleTv.text = getString(R.string.help_center)
            4 ->
                titleTv.text = getString(R.string.register_agreement)
            8 ->
                titleTv.text = getString(R.string.activity_sp_title)
        }
        backIv.setOnClickListener { finish() }
    }

    override fun initImmersionBar() {
        super.initImmersionBar()
        mImmersionBar.titleBar(top).init()
    }

    override fun initData() {
        showProgress()
        if (type != 3 && type != 4)
            presenter.getData(type.toString())
        else {
            if (type == 3) presenter.getHelpCenter("1")
            else
                presenter.getHelpCenter("2")
        }

    }

    override fun initEvent() {

    }

    override fun onSuccess(list: WebviewTextBean?) {

        hideProgress()

//        var get = list?.get(0)
        var url = list?.url
//        Log.e("tag===",url)
//        webView.loadDataWithBaseURL(null, content, "text/html", "utf-8", null)

//        Log.e("url===",url)
        webView.loadUrl(url+"?lang="+ Utils.getSpUtils().getString("current_language", ""))
    }

    override fun onFailure(errorMsg: String?) {
        hideProgress()
        toast(errorMsg)
    }

    override fun getHelpCenterSuccess(helpCenterBean: HelpCenterBean?) {
        hideProgress()
        webView.loadUrl(helpCenterBean?.url+"?lang="+ Utils.getSpUtils().getString("current_language", ""))
    }

}
