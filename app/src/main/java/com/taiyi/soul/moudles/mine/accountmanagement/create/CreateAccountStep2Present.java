package com.taiyi.soul.moudles.mine.accountmanagement.create;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Response;

public class CreateAccountStep2Present extends BasePresent<CreateAccountStep2View> {

    public void createChildAccount(String account,String inviteCode,String publicKey){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("account",account);
            jsonObject.put("invitationCode",inviteCode);
            jsonObject.put("publicKey",publicKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.putRequest(BaseUrl.CREATE_CHILD_ACCOUNT, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.createSuccess(response.body().data);
                    } else {
                        view.createFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }
}
