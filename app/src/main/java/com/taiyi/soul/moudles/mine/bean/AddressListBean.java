package com.taiyi.soul.moudles.mine.bean;

import java.util.List;

public class AddressListBean {

        public int totalpage;
        public int rowcount;
        public List<AddresslistBean> addresslist;

    public int getTotalpage() {
        return totalpage;
    }

    public void setTotalpage(int totalpage) {
        this.totalpage = totalpage;
    }

    public int getRowcount() {
        return rowcount;
    }

    public void setRowcount(int rowcount) {
        this.rowcount = rowcount;
    }

    public List<AddresslistBean> getAddresslist() {
        return addresslist;
    }

    public void setAddresslist(List<AddresslistBean> addresslist) {
        this.addresslist = addresslist;
    }

    public static class AddresslistBean {
            public String createtime;
            public String address;
            public String useraddress_id;
            public String cityid;
            public String type;
            public String provinceid;
            public String userid;
            public String countryid;
            public String issh;
            public String cityName;
            public String provincename;
            public String surname;
            public String name;
            public String tel;
            public String countryName;
            public String delflag;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getUseraddress_id() {
            return useraddress_id;
        }

        public void setUseraddress_id(String useraddress_id) {
            this.useraddress_id = useraddress_id;
        }

        public String getCityid() {
            return cityid;
        }

        public void setCityid(String cityid) {
            this.cityid = cityid;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getProvinceid() {
            return provinceid;
        }

        public void setProvinceid(String provinceid) {
            this.provinceid = provinceid;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getCountryid() {
            return countryid;
        }

        public void setCountryid(String countryid) {
            this.countryid = countryid;
        }

        public String getIssh() {
            return issh;
        }

        public void setIssh(String issh) {
            this.issh = issh;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getProvincename() {
            return provincename;
        }

        public void setProvincename(String provincename) {
            this.provincename = provincename;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }

        public String getDelflag() {
            return delflag;
        }

        public void setDelflag(String delflag) {
            this.delflag = delflag;
        }
    }

    /**
     * totalpage : 1
     * addresslist : [{"createtime":"2020-08-03 15:55:40","address":"啊咯的好像","useraddress_id":1,"cityid":"1000020","type":"1","provinceid":"100002","userid":"15952243829550305","issh":"2","cityName":"南开","provincename":"天津","name":"啦啦啦","tel":"18322125252","delflag":"1"}]
     * rowcount : 1
     */


//    private int totalpage;
//    private int rowcount;
//    private List<AddresslistBean> addresslist;
//
//    public int getTotalpage() {
//        return totalpage;
//    }
//
//    public void setTotalpage(int totalpage) {
//        this.totalpage = totalpage;
//    }
//
//    public int getRowcount() {
//        return rowcount;
//    }
//
//    public void setRowcount(int rowcount) {
//        this.rowcount = rowcount;
//    }
//
//    public List<AddresslistBean> getAddresslist() {
//        return addresslist;
//    }
//
//    public void setAddresslist(List<AddresslistBean> addresslist) {
//        this.addresslist = addresslist;
//    }
//
//    public static class AddresslistBean {
//        /**
//         * createtime : 2020-08-03 15:55:40
//         * address : 啊咯的好像
//         * useraddress_id : 1
//         * cityid : 1000020
//         * type : 1
//         * provinceid : 100002
//         * userid : 15952243829550305
//         * issh : 2
//         * cityName : 南开
//         * provincename : 天津
//         * name : 啦啦啦
//         * tel : 18322125252
//         * delflag : 1
//         */
//
//        private String createtime;
//        private String address;
//        private int useraddress_id;
//        private String cityid;
//        private String type;
//        private String provinceid;
//        private String userid;
//        private String issh;
//        private String cityName;
//        private String provincename;
//        private String name;
//        private String surname;
//        private String tel;
//        private String delflag;
//
//        public String getSurname() {
//            return surname;
//        }
//
//        public void setSurname(String surname) {
//            this.surname = surname;
//        }
//
//        public String getCreatetime() {
//            return createtime;
//        }
//
//        public void setCreatetime(String createtime) {
//            this.createtime = createtime;
//        }
//
//        public String getAddress() {
//            return address;
//        }
//
//        public void setAddress(String address) {
//            this.address = address;
//        }
//
//        public int getUseraddress_id() {
//            return useraddress_id;
//        }
//
//        public void setUseraddress_id(int useraddress_id) {
//            this.useraddress_id = useraddress_id;
//        }
//
//        public String getCityid() {
//            return cityid;
//        }
//
//        public void setCityid(String cityid) {
//            this.cityid = cityid;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        public String getProvinceid() {
//            return provinceid;
//        }
//
//        public void setProvinceid(String provinceid) {
//            this.provinceid = provinceid;
//        }
//
//        public String getUserid() {
//            return userid;
//        }
//
//        public void setUserid(String userid) {
//            this.userid = userid;
//        }
//
//        public String getIssh() {
//            return issh;
//        }
//
//        public void setIssh(String issh) {
//            this.issh = issh;
//        }
//
//        public String getCityName() {
//            return cityName;
//        }
//
//        public void setCityName(String cityName) {
//            this.cityName = cityName;
//        }
//
//        public String getProvincename() {
//            return provincename;
//        }
//
//        public void setProvincename(String provincename) {
//            this.provincename = provincename;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getTel() {
//            return tel;
//        }
//
//        public void setTel(String tel) {
//            this.tel = tel;
//        }
//
//        public String getDelflag() {
//            return delflag;
//        }
//
//        public void setDelflag(String delflag) {
//            this.delflag = delflag;
//        }
//    }
}
