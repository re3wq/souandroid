package com.taiyi.soul.moudles.mine.editinformation;

import android.app.Dialog;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.othershe.calendarview.bean.DateBean;
import com.othershe.calendarview.listener.OnPagerChangeListener;
import com.othershe.calendarview.listener.OnSingleChooseListener;
import com.othershe.calendarview.weiget.CalendarView;
import com.othershe.calendarview.weiget.WeekView;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ToastUtils;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;

public class EditInformationActivity extends BaseActivity<EditInformationView, EditInformationPresent> implements EditInformationView, OnPagerChangeListener, View.OnClickListener, OnSingleChooseListener {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.nameEt)
    EditText nameEt;
    @BindView(R.id.sexTv)
    TextView sexTv;
    @BindView(R.id.birthdayTv)
    TextView birthdayTv;
    @BindView(R.id.addressTv)
    EditText addressTv;
    @BindView(R.id.top)
    ConstraintLayout top;
    Dialog calendarDialog;
    CalendarView calendarView;
    WeekView weekView;
    TextView dateTv, cancelTv, determineTv;
    ImageView leftIv, rightIv;
    private String mSelectDate;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_information;
    }

    @Override
    public EditInformationPresent initPresenter() {
        return new EditInformationPresent();
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.edit_information));
        showProgress();
        presenter.getUserInfo();
        calendarDialog = new Dialog(this, R.style.MyDialog);
        View view = LayoutInflater.from(this).inflate(R.layout.calendar_layout, null);
        calendarDialog.setContentView(view);
        WindowManager.LayoutParams layoutParams = calendarDialog.getWindow().getAttributes();
        layoutParams.gravity = Gravity.BOTTOM;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        calendarDialog.getWindow().setAttributes(layoutParams);


        calendarView = view.findViewById(R.id.calendarView);
        weekView = view.findViewById(R.id.weekView);
        dateTv = view.findViewById(R.id.dateTv);
        cancelTv = view.findViewById(R.id.cancelTv);
        determineTv = view.findViewById(R.id.determineTv);
        leftIv = view.findViewById(R.id.leftIv);
        rightIv = view.findViewById(R.id.rightIv);
        leftIv.setOnClickListener(this);
        rightIv.setOnClickListener(this);
        cancelTv.setOnClickListener(this);
        determineTv.setOnClickListener(this);
        weekView.setBackgroundColor(getResources().getColor(R.color.transparent));
        calendarView.setBackgroundColor(getResources().getColor(R.color.transparent));
        calendarView.setBackground(null);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        dateTv.setText(year + " / " + (month > 9 ? month : "0" + month));
        calendarView
                .setStartEndDate("1950.6", "3050.12")
                .setInitDate("2020.6")
//                .setSingleDate("2021.12.12")
                .init();
        calendarView.setOnSingleChooseListener(this);
        calendarView.setOnPagerChangeListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.sexTv, R.id.birthdayTv, R.id.completeTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.sexTv:
                if (sexTv.getText().toString().equals("") || sexTv.getText().toString().equals(getString(R.string.female))) {
                    sexTv.setText(getString(R.string.male));
                } else {
                    sexTv.setText(getString(R.string.female));
                }
                break;
            case R.id.birthdayTv:
                calendarDialog.show();
                break;
            case R.id.completeTv:
                if (nameEt.getText().toString().equals("")) {
                    AlertDialogShowUtil.toastMessage(this,getString(R.string.toast_input_name));
                    return;
                }
                if (sexTv.getText().toString().equals("")) {
                    return;
                }
                if (birthdayTv.getText().toString().equals("")) {
                    return;
                }
                if (addressTv.getText().toString().equals("")) {
                    AlertDialogShowUtil.toastMessage(this,getString(R.string.toast_input_address));
                    return;
                }
                int sexType = 0;
                if (sexTv.getText().toString().equals(getString(R.string.male))) {
                    sexType = 0;
                } else {
                    sexType = 1;
                }
                showProgress();
                presenter.getChangeInfo(addressTv.getText().toString(), birthdayTv.getText().toString(), nameEt.getText().toString(), sexType);
                break;
        }
    }

    @Override
    public void onPagerChanged(int[] date) {
        int i = date[1];
//        Log.i("currentMonth", "---year---" + date[0] + "---month---" + date[1]);
        if (i < 10)
            dateTv.setText(date[0] + " / 0" + date[1]);
        else
            dateTv.setText(date[0] + " / " + date[1]);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.leftIv:
                calendarView.lastMonth();
                break;
            case R.id.rightIv:
                calendarView.nextMonth();
                break;
            case R.id.cancelTv:
                calendarDialog.dismiss();
                break;
            case R.id.determineTv:
                calendarDialog.dismiss();
//                int[] currentDate = CalendarUtil.getCurrentDate();
                DateBean singleDate = calendarView.getSingleDate();
                int[] solar = singleDate.getSolar();
//                ToastUtils.showLongToast("当前选择--" + solar[0] + "-" + solar[1] + "-" + solar[2]);
                ToastUtils.showLongToast(getString(R.string.current_choose_1) + mSelectDate);
                birthdayTv.setText(mSelectDate);
                break;
        }
    }

    @Override
    public void getSuccess(int status, String msg, EditInformationBean userInfoBean) {
        hideProgress();
        if (null != userInfoBean.nickName) {
            nameEt.setText(userInfoBean.nickName);
        }
        if (null != userInfoBean.birthday) {
            birthdayTv.setText(userInfoBean.birthday);
        }
        if (null != userInfoBean.address) {
            addressTv.setText(userInfoBean.address);
        }
        if (null != userInfoBean.sex) {
            if(userInfoBean.sex.equals("0")){
                sexTv.setText(getString(R.string.male));
            }else {
                sexTv.setText(getString(R.string.female));
            }
        }
    }

    @Override
    public void getChangeInfoSuccess(int status, String msg, String errorMsg) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(this, msg);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (status == 200) {
                    finish();
                }
            }
        },2300);

    }

    @Override
    public void onSingleChoose(View view, DateBean date) {
        int[] solar = date.getSolar();
        String month = null, day = null;
        if (solar[1] < 10) {
            month = "0" + solar[1];
        } else {
            month = "" + solar[1];
        }
        if (solar[2] < 10) {
            day = "0" + solar[2];
        } else {
            day = "" + solar[2];
        }
        mSelectDate = solar[0] + "-" + month + "-" + day;
    }
}
