package com.taiyi.soul.moudles.main.node.cpubandwidth;

import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;

public interface CpuNetView {
    void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean);

    void getDataHttpFail(String errorMsg);

    void getBalanceSuccess(String balance);

    void stakeSuccess();

    void unStackSuccess();

    void onCheckPayPasswordSuccess(int code,String msg);
}
