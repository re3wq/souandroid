package com.taiyi.soul.moudles.mine.about;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.shape.CornerTreatment;
import com.mylhyl.zxing.scanner.encode.QREncode;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.mine.webview.WebViewActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.CopyUtils;

import net.lucode.hackware.magicindicator.buildins.circlenavigator.CircleNavigator;

import butterknife.BindView;
import butterknife.OnClick;

public class AboutUsActivity extends BaseActivity<AboutView, AboutPresent> implements AboutView {

    //
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.user_xy)
    RelativeLayout userXy;
    @BindView(R.id.twitter)
    TextView twitter;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.gw)
    TextView gw;
 @BindView(R.id.small_logo)
 ImageView small_logo;
 @BindView(R.id.iv)
 ImageView iv;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about_us;
    }

    @Override
    public AboutPresent initPresenter() {
        return new AboutPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setCenterTitle(getString(R.string.about_us));
    }

    @Override
    protected void initData() {
        presenter.getInfo(AboutUsActivity.this);
        twitter.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CopyUtils.CopyToClipboard(AboutUsActivity.this, twitter.getText().toString());
                toast(getString(R.string.copy_success));
                return true;
            }
        });
        email.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CopyUtils.CopyToClipboard(AboutUsActivity.this, email.getText().toString());
                toast(getString(R.string.copy_success));
                return true;
            }
        });
        gw.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CopyUtils.CopyToClipboard(AboutUsActivity.this, gw.getText().toString());
                toast(getString(R.string.copy_success));
                return true;
            }
        });
    }

    @Override
    public void initEvent() {

    }


    @OnClick({R.id.iv_back, R.id.user_xy, R.id.twitter, R.id.gw})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.user_xy:
                Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("type", 4);
                startActivity(intent);
                break;
            case R.id.twitter:
                if (!"".equals(twitter.getText().toString())) {
                    Intent intents = new Intent(Intent.ACTION_VIEW, Uri.parse(twitter.getText().toString()));
                    startActivity(intents); //启动浏览器dao
                }
                break;

            case R.id.gw:
                if (!"".equals(gw.getText().toString())) {
                    Intent intentss = new Intent(Intent.ACTION_VIEW, Uri.parse(gw.getText().toString()));
                    startActivity(intentss);
                }
                break;
        }
    }


    @Override
    public void getInfoSuccess(int code, String msg_cn, AboutBean data) {
        if (code == 0) {
            twitter.setText(data.getTwitter_url());
            email.setText(data.getEmail_url());
            gw.setText(data.getWeb_url());
            RequestOptions options = new RequestOptions();
            RequestOptions options2 = new RequestOptions();

            Bitmap bitmap = new QREncode.Builder(this)
//                    .setContents("$invitationUrl?invitationCode=$invitationCode") //二维码内容
                    .setContents(data.getQr_code()) //二维码内容
                    .setMargin(1)
                    .build().encodeAsBitmap();


            options.centerCrop().circleCrop().transform(new RoundedCorners(40));
            options2.centerCrop().circleCrop();
            if (bitmap != null) {
//                iv.setImageBitmap(bitmap);
                Glide.with(this).load(bitmap).apply(options).into(iv);
                Glide.with(this).load(R.mipmap.ic_loco_new).apply(options2).into(small_logo);
            }


        } else {
            AlertDialogShowUtil.toastMessage(AboutUsActivity.this, msg_cn);
        }

    }
}