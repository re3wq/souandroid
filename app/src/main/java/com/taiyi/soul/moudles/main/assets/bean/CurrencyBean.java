package com.taiyi.soul.moudles.main.assets.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\3 0003 17:08
 * @Author : yuan
 * @Describe ：
 */
public class CurrencyBean {

    public int imgs;
    public String names;

    public CurrencyBean(int imgs, String names) {
        this.imgs = imgs;
        this.names = names;
    }
}
