package com.taiyi.soul.moudles.main.home.latestnews;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.main.home.bean.NewsListBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class LatestNewsPresent extends BasePresent<LatestNewsView> {
    public void getData(int pageNo){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNum",pageNo);
            jsonObject.put("pageSize",10);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.GET_NEWS_LIST, this, jsonObject.toString(), new JsonCallback<BaseResultBean<NewsListBean>>() {
            @Override
            public BaseResultBean<NewsListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<NewsListBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                if (response.body().status==200){
                    view.onSuccess(response.body().data);
                }else {
                    view.onFailure(response.body().msg);
                }}
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<NewsListBean>> response) {
                super.onError(response);
            }
        });
    }
    public void updateReadNum(String id){
        Map<String,String> map=new HashMap<>();
        map.put("newId",id);
        HttpUtils.getRequets(BaseUrl.UPDATE_NEWS_READ_NUM, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.updateSuccess();
                    } else {
                        view.updateFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }
}
