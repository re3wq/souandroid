package com.taiyi.soul.moudles.mine.securitycenter.googleverification;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.mine.securitycenter.bean.GoogleSecretBean;
import com.taiyi.soul.utils.CopyUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.countdowntimer.CountDownTimerUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class GoogleVerificationActivity extends BaseActivity<GoogleVerificationView, GoogleVerificationPresent> implements GoogleVerificationView {
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.googleSecretTv)
    TextView googleSecretTv;
    @BindView(R.id.sendCodeTv)
    TextView sendCodeTv;
    @BindView(R.id.updateGoogleVerificationTv)
    TextView updateGoogleVerificationTv;
    @BindView(R.id.googleVerificationCodeEt)
    EditText googleVerificationCodeEt;
    @BindView(R.id.verificationCodeEt)
    EditText verificationCodeEt;
    @BindView(R.id.top)
    ConstraintLayout top;
    private int loginType;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_google_verification;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @OnClick({R.id.backIv, R.id.sendCodeTv, R.id.copySecretTv, R.id.updateGoogleVerificationTv, R.id.pasteGoogleVerificationCodeTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.sendCodeTv:
                if (loginType == 0) {//手机号
                    String phoneNumber = Utils.getSpUtils().getString("phoneNumber");
                    String areaCode = Utils.getSpUtils().getString(Constants.AREA_CODE);
                    showProgress();
                    presenter.getVerificationCode(this, areaCode, phoneNumber);
                } else {
                    String email = Utils.getSpUtils().getString("email");
                    showProgress();
                    presenter.getVerificationCode(this, "", email);
                }
                break;
            case R.id.copySecretTv:
                toast(getString(R.string.copy_success));
                CopyUtils.CopyToClipboard(this, googleSecretTv.getText().toString());
                break;
            case R.id.updateGoogleVerificationTv:
                String verificationCode = verificationCodeEt.getText().toString();
                String googleVerificationCode = googleVerificationCodeEt.getText().toString();
                if (TextUtils.isEmpty(verificationCode)) {
                    toast(getString(R.string.input_code));
                    return;
                }
                if (TextUtils.isEmpty(googleVerificationCode)) {
                    toast(getString(R.string.edit_google_code));
                    return;
                }
                showProgress();

                presenter.bindGoogleVerification(this, googleSecretTv.getText().toString(), verificationCode, googleVerificationCode, String.valueOf(loginType));
                break;
            case R.id.pasteGoogleVerificationCodeTv:
                String clipboardContent = getClipboardContent(this);
                if (TextUtils.isEmpty(clipboardContent)) {
                    toast(getString(R.string.no_clip));
                    return;
                }
                googleVerificationCodeEt.setText(clipboardContent);
                break;
        }
    }


    @Override
    public GoogleVerificationPresent initPresenter() {
        return new GoogleVerificationPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.google_verification));
        loginType = Utils.getSpUtils().getInt("loginType");
        String isOpenGoogle = Utils.getSpUtils().getString(Constants.IS_OPEN_GOOGLE);
        if ("0".equals(isOpenGoogle)) {
            updateGoogleVerificationTv.setText(getString(R.string.bind_google));
        }
    }

    @Override
    protected void initData() {
        showProgress();
        presenter.getGoogleSecret(this);
    }

    @Override
    public void initEvent() {

    }


    /**
     * 获取剪切板上的内容
     */
    @Nullable
    public static String getClipboardContent(Context context) {
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (cm != null) {
            ClipData data = cm.getPrimaryClip();
            if (data != null && data.getItemCount() > 0) {
                ClipData.Item item = data.getItemAt(0);
                if (item != null) {
                    CharSequence sequence = item.coerceToText(context);
                    if (sequence != null) {
                        return sequence.toString();
                    }
                }
            }
        }
        return null;
    }


    @Override
    public void getGoogleSecretSuccess(GoogleSecretBean secret) {
        hideProgress();
        googleSecretTv.setText(secret.getSecret());
    }

    @Override
    public void getGoogleSecretFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getVerificationCodeSuccess() {
        hideProgress();
        CountDownTimerUtils countDownTimerUtils = new CountDownTimerUtils(this, sendCodeTv, 60000, 1000, "#a5a9ac");
        countDownTimerUtils.start();
        sendCodeTv.setFocusable(true);
        sendCodeTv.setFocusableInTouchMode(true);
        sendCodeTv.requestFocus();
    }

    @Override
    public void getInvitePersonFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void bindSuccess() {
        hideProgress();
        String isOpenGoogle = Utils.getSpUtils().getString(Constants.IS_OPEN_GOOGLE);
        if ("0".equals(isOpenGoogle)) {
            Utils.getSpUtils().put(Constants.IS_OPEN_GOOGLE, "1");//0未绑定 1绑定未开启
        }
//        EventBus.getDefault().post("guge_change");
        finish();
    }
}
