package com.taiyi.soul.moudles.main.home.mall.order;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.EosSignDataManger;
import com.taiyi.soul.moudles.main.assets.bean.AssetsBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.mall.order.present.CheckPayPresent;
import com.taiyi.soul.moudles.main.home.mall.order.present.CheckPayView;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;

import org.greenrobot.eventbus.EventBus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\5\21 0021 15:58
 * @Author : yuan
 * @Describe ：订单-支付
 */
public class CheckPayActivity extends BaseActivity<CheckPayView, CheckPayPresent> implements CheckPayView {


    @BindView(R.id.tv_title)
    TextView mTvTitle;

    @BindView(R.id.ngk_checked)
    CheckBox mNgkChecked;
    @BindView(R.id.usdn_checked)
    CheckBox mUsdnChecked;
    @BindView(R.id.check_order_total)
    TextView mCheckOrderTotal;
    @BindView(R.id.balance_usdn)
    TextView balance_usdn;
    @BindView(R.id.balance_ngk)
    TextView balance_ngk;
    @BindView(R.id.iv_ngk)
    ImageView iv_ngk;
    @BindView(R.id.iv_usdn)
    ImageView iv_usdn;

    private int payType = 1;//为选中支付方式  1ngk支付 2usdn支付
    private Bundle mExtras;
    private String allmoney_ngk;
    private String allmoney_usdn;
    private String orderid;
    private String mMainAccount = Utils.getSpUtils().getString("mainAccount");
    private String mNgk_ratio;
    private String mShop_address;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_pay;
    }

    @Override
    public CheckPayPresent initPresenter() {
        return new CheckPayPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        presenter.getBalance(mMainAccount, "SOU");
        presenter.getBalance(mMainAccount, "USDS");
        presenter.getAssets(this);
        presenter.getFindOrderData(this);
    }

    @Override
    protected void initData() {
        mTvTitle.setText(getString(R.string.check_order_title));
        showProgress();

        mExtras = getIntent().getExtras();
        allmoney_ngk = mExtras.getString("allmoney_ngk");
        allmoney_usdn = mExtras.getString("allmoney_usdn");
        mNgk_ratio = mExtras.getString("NGK_RATIO");
        orderid = mExtras.getString("orderid");
        mCheckOrderTotal.setText(allmoney_ngk + " SOU");

        mNgkChecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (payType != 1) {
                    payType = 1;
                    mUsdnChecked.setChecked(false);
                    mNgkChecked.setChecked(true);
                    mCheckOrderTotal.setText(allmoney_ngk + " SOU");
                } else {
                    mNgkChecked.setChecked(true);
                }
            }
        });
        mUsdnChecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (payType != 2) {
                    payType = 2;
                    mUsdnChecked.setChecked(true);
                    mNgkChecked.setChecked(false);
                    mCheckOrderTotal.setText(allmoney_usdn + " USDS");
                } else {
                    mUsdnChecked.setChecked(true);
                }
            }
        });
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.iv_back, R.id.check_order_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.check_order_next:
                if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                    if (!mNgkChecked.isChecked() && !mUsdnChecked.isChecked()) {
                        AlertDialogShowUtil.toastMessage(CheckPayActivity.this, getString(R.string.order_select_pay_hint));
                        return;
                    }
                    buyMemo++;
                    showPassword(view);
                } else {//未导入私钥 ，去设置
                    Dialog dialog = new Dialog(CheckPayActivity.this, R.style.MyDialog);
                    View inflate = LayoutInflater.from(CheckPayActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                    dialog.setContentView(inflate);
                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putString("accountName", mMainAccount);
                            ActivityUtils.next(CheckPayActivity.this, ImportAccountActivity.class, bundle, false);
                        }
                    });
                    dialog.show();
                }
                break;
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    private void showPassword(View view) {
        View v = LayoutInflater.from(CheckPayActivity.this).inflate(R.layout.popup_order_pay, null);
        OrderPopupWindow pay_popup = new OrderPopupWindow(CheckPayActivity.this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        SafeKeyboard safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(CheckPayActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });

        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showProgress();
//                            presenter.checkPayPassword(CheckPayActivity.this, s);
                            getSign(s);
                            pay_popup.dismiss();
                        }
                    }, 500);
                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }

    @Override
    public void onCheckPayPasswordSuccess(String bean) {

    }


    private String coin_name = "";
    private int buyMemo = 0;
    private void getSign(String s) {//获取签名
        String memo = "";
        String price = "";
        String unit = "";
        String mainAccount = Utils.getSpUtils().getString("mainAccount");
        memo = "order_pay"+"_"+buyMemo;
        if (payType == 1) {
            unit = Constants.NGK_CONTRACT_ADDRESS;
            coin_name = "SOU";
//            price = new DecimalFormat("##0.0000").format(new Double(allmoney_ngk)) + " " + coin_name;
            price =new BigDecimal(allmoney_ngk).setScale(4,BigDecimal.ROUND_DOWN).toPlainString() + " " + coin_name;
        } else if (payType == 2) {
            unit = Constants.USDN_CONTRACT_ADDRESS;
            coin_name = "USDS";
//            price = new DecimalFormat("##0.00000000").format(new Double(allmoney_usdn)) + " " + coin_name;
            price =new BigDecimal(allmoney_usdn).setScale(8,BigDecimal.ROUND_DOWN).toPlainString() + " " + coin_name;
        }

        new EosSignDataManger(CheckPayActivity.this, new EosSignDataManger.Callback() {
            @Override
            public void onSignSuccess(String sign_data) {
//                Log.e("sign===", sign_data);
                presenter.getPayOrder(CheckPayActivity.this, orderid, coin_name, sign_data,s);
            }

            @Override
            public void fail() {
            }
        }).pushAction(new Gson().toJson(new TransferEosMessageBean(memo, mShop_address, price
                        , mainAccount)),
                unit,
                mainAccount);
    }


    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        if (symbol.equals("USDS")) {
            balance_usdn.setText("(" + balance + " USDS)");
        } else if (symbol.equals("SOU")) {
            balance_ngk.setText("(" + balance + " SOU)");
        }
    }

    @Override
    public void onIssueFeeSuccess(IssueFeeBean bean) {
        hideProgress();
        mShop_address = bean.shop_address;
    }

    @Override
    public void getPayOrderSuccess(String bean) {
        hideProgress();
        ActivityUtils.next(CheckPayActivity.this, CheckCompletedActivity.class, true);
        AlertDialogShowUtil.toastMessage(this, bean);
        EventBus.getDefault().post("order_refresh");
    }

    @Override
    public void getAssetsSuccess(AssetsBean assetsBean) {
        hideProgress();
        for (int i = 0; i < assetsBean.getPropertelist().size(); i++) {
            if (assetsBean.getPropertelist().get(i).getCoinName().equals("SOU")) {
                Glide.with(this).load(assetsBean.getPropertelist().get(i).getUrl()).into(iv_ngk);
            } else if (assetsBean.getPropertelist().get(i).getCoinName().equals("USDS")) {
                Glide.with(this).load(assetsBean.getPropertelist().get(i).getUrl()).into(iv_usdn);
            }
        }
    }

    @Override
    public void onFailure() {
        hideProgress();
    }
}
