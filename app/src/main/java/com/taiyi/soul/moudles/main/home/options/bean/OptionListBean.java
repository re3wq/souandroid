package com.taiyi.soul.moudles.main.home.options.bean;

import java.io.Serializable;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\19 0019 19:13
 * @Author : yuan
 * @Describe ：
 */
public class OptionListBean implements Serializable {

        public String id;
        public String instrument_id;//币种名称
        public String product_id;
        public String last;//当时交易价
        public String last_qty;
        public String best_ask;
        public String ask;
        public String best_ask_size;
        public String best_bid;
        public String bid;
        public String best_bid_size;
        public String open_24h;
        public String high_24h;
        public String low_24h;
        public String base_volume_24h;//24小时量
        public String quote_volume_24h;
        public String timestamp;
        public String img_url;
        public String price;//美元价格
        // 获取系统当前选中币种 在 币种汇率接口中判断currency_code，获取对应币种税率rate*price
        public String conversion;//涨幅


}
