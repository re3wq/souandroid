package com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd;

public interface UpdateLoginPasswordView {
    void  getVerificationCodeSuccess();

    void getVerificationCodeFailure(String errorMsg);

    void updateSuccess(String msg);

    void updateFailure(String errorMsg);

    void checkSuccess();

    void checkFailure(String errorMsg);
}
