package com.taiyi.soul.moudles.main.deal.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\24 0024 17:51
 * @Author : yuan
 * @Describe ：
 */
public class EntrustThreeBean {

        public int totalpage;
        public int rowcount;
        public List<ComlistBean> comlist;

        public static class ComlistBean {
            public String relation_orderid;
            public int flag;
            public String ifrevoke;
            public int dealnum;
            public String maybemoney;
            public String type;
            public String userid;
            public String surpluscharge;
            public String get_coinname;
            public String price;
            public String postersid;
            public String percentage;
            public String dealcharge;
            public String dealmoney;
            public String postersnum;
            public String createtime;
            public String charge;
            public String orderid;
            public String surplusnum;
            public String ifeat;
            public String applytime;
            public String money;
            public String surplusmoney;
            public String lose_coinname;
            public String ifnum;
            public String account;
            public int ordertype;

    }
}
