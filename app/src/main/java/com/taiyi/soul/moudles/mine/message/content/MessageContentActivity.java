package com.taiyi.soul.moudles.mine.message.content;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.mine.message.bean.MessageListBean;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.SPUtil;
import com.taiyi.soul.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class MessageContentActivity extends BaseActivity<NormalView, NormalPresenter> {
    @BindView(R.id.top)
    ConstraintLayout top;
    @BindView(R.id.dateTv)
    TextView dateTv;
    @BindView(R.id.msgTitleTv)
    TextView msgTitleTv;
    @BindView(R.id.contentTv)
    TextView contentTv;
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.webView)
    WebView webView;

    private MessageListBean.ListBean content;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message_content;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        content = ((MessageListBean.ListBean) bundle.getSerializable("data"));
//        dateTv.setText(content.getCreateTime());
//        msgTitleTv.setText(content.getTitle());
//        contentTv.setText(content.getUrl());
        if (content.getType() == 1) {
            titleTv.setText(getResources().getString(R.string.message_context));
        } else {
            titleTv.setText(getResources().getString(R.string.notice_context));
        }
        webView.setBackgroundColor(R.drawable.bg_212121_15);//设置背景色
        webView.getBackground().setAlpha(3);//设置填充透明度（布局中一定要设置background，不然getbackground会是null）
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                showProgress();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
//                hideProgress();
            }
        });
//        Log.e("tag====",content.getUrl()+"  "+content.getId() +" "+ Utils.getSpUtils().getString("current_language", ""));

        webView.loadUrl(content.getUrl() + "?id=" + content.getId() + "&token=" + Utils.getSpUtils().getString(Constants.TOKEN, "") + "&lang=" + Utils.getSpUtils().getString("current_language", ""));
//        webView.loadUrl(content.getUrl() + "?id=" + content.getId() + "&token=" + Utils.getSpUtils().getString(Constants.TOKEN, "") + "&lang=" + getSetLanguageLocale(this));
    }

    public static String getSetLanguageLocale(Context context) {

        switch (SPUtil.getInstance(context).getSelectLanguage()) {
            case 0: //繁体
                return "zh-rCN";
            case 1:
                return "en";//英语
            case 2:
                return "fr";//法语
            case 3:
                return "de";//德语
            case 4:
                return "es";//西班牙语
            case 5:
                return "pt";//葡萄牙语
            case 6:
                return "ru";//俄语
            case 7:
                return "ar";//阿拉伯语
            case 8://繁体中文
                return "zh-rCN";
//                return new Locale("wyw");
            case 9://简体中文
                return "zh";
            case 10:
                return "ja";//日语
            case 11:
                return "ko";//韩语
//            case 1:
            default:
                return "zh-rCN";//简体中文
        }
    }


    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick(R.id.backIv)
    public void onViewClicked() {
        finish();
    }
}
