package com.taiyi.soul.moudles.main.home.options.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\20 0020 20:53
 * @Author : yuan
 * @Describe ：
 */
public class KLainBean {

        public DepthsBean depths;
        public List<List<String>> lines;

        public static class DepthsBean {
            public List<?> asks;
            public List<?> bids;

        }

}
