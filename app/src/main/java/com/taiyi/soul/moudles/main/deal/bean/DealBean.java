package com.taiyi.soul.moudles.main.deal.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\3 0003 16:35
 * @Author : yuan
 * @Describe ：
 */
public class DealBean {


        public int totalpage;
        public int rowcount;
        public List<ComlistBean> comlist;

        public static class ComlistBean {
            public String createtime;
            public String flag;
            public String charge;
            public String orderid;
            public String ifrevoke;
            public String maybemoney;
            public String surplusnum;
            public String type;
            public String ifeat;
            public String userid;
            public String surpluscharge;
            public String get_coinname;
            public String money;
            public String price;
            public String postersid;
            public String surplusmoney;
            public String lose_coinname;
            public String postersnum;
            public String ifnum;
            public String account;
            public String ordertype;

    }
}
