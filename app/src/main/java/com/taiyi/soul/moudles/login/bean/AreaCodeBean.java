package com.taiyi.soul.moudles.login.bean;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/4/13
 * Time: 3:27 PM
 */
public class AreaCodeBean {
    private String icon;
    private int iconD;
    private String code;

    public AreaCodeBean() {
    }

    public AreaCodeBean(String icon, String code) {
        this.icon = icon;
        this.code = code;
    }

      public AreaCodeBean(int iconD, String code) {
        this.iconD = iconD;
        this.code = code;
    }


    public String getIcon() {
        return icon;
    }

    public int getIconD() {
        return iconD;
    }

    public void setIconD(int iconD) {
        this.iconD = iconD;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
