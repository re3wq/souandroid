package com.taiyi.soul.moudles.mine.feedback;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Response;

public class FeedbackPresent extends BasePresent<FeedbackView> {
    public void getFeedbackClass() {
        JSONObject jsonObject = new JSONObject();

        HttpUtils.postRequest(BaseUrl.GET_FEED_BACK_CLASS, this, jsonObject.toString(), new JsonCallback<BaseResultBean<TypeListBean>>() {
            @Override
            public BaseResultBean<TypeListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<TypeListBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getTypeSuccess(response.body().data);
                    } else {
                        view.getTypeFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<TypeListBean>> response) {
                super.onError(response);
            }
        });
    }

    public void commitFeedbackContent(String content, String type) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("content", content);
            jsonObject.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.putRequest(BaseUrl.FEED_BACK_COMMIT, this, jsonObject.toString(), new JsonCallback<BaseResultBean<Integer>>() {
            @Override
            public BaseResultBean<Integer> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<Integer>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.commitSuccess(response.body().msg);
                    } else {
                        view.commitFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<Integer>> response) {
                super.onError(response);
            }
        });
    }
}
