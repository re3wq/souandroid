package com.taiyi.soul.moudles.main.home.mall.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\5\15 0015 17:32
 * @Author : yuan
 * @Describe ：
 */
public class DemoOptionsBean {
        public String img;
        public String name;
        public String unit;
        public String hour_num;
        public String num;
        public String price;
        public String extent;
        public int type;
        public DemoOptionsBean(String img, String name, String unit, String hour_num, String num, String price, String extent, int type) {
                this.img = img;
                this.name = name;
                this.unit = unit;
                this.hour_num = hour_num;
                this.num = num;
                this.price = price;
                this.extent = extent;
                this.type = type;
        }

        public DemoOptionsBean(String img) {
                this.img = img;
        }
}
