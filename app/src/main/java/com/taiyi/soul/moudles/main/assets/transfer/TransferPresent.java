package com.taiyi.soul.moudles.main.assets.transfer;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.TransferBalanceBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class TransferPresent extends BasePresent<TransferView> {
    public void getCoinList() {
        HttpUtils.getRequets(BaseUrl.GET_COIN_LIST, this, null, new JsonCallback<BaseResultBean<List<CoinListBean>>>() {
            @Override
            public BaseResultBean<List<CoinListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<CoinListBean>>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getCoinListSuccess(response.body().data);
                    } else {
                        view.getCoinListFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<CoinListBean>>> response) {
                super.onError(response);
            }
        });
    }
    public void getAccountStatus(View viewButton) {
        HttpUtils.getRequets(BaseUrl.account_status, this, null, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    view.getAccountStatusSuccess(response.body().status,response.body().msg,response.body().data,viewButton);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    public void getCurrencyBalance(Activity activity,String coinName){
        Map<String,String>map=new HashMap<>();
        map.put("coinName",coinName);
        HttpUtils.getRequets(BaseUrl.GET_CURRENCY_BALANCE, this, map, new JsonCallback<BaseResultBean<TransferBalanceBean>>() {
            @Override
            public BaseResultBean<TransferBalanceBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<TransferBalanceBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getBalanceSuccess(response.body().data);
                    } else if (response.body().status == 602||response.body().status == 3080005) {//登录失效
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            activity.startActivity(new Intent(activity, LoginActivity.class));
                            Utils.getSpUtils().remove(Constants.TOKEN);}
                    } else {
                        view.getCoinListFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<TransferBalanceBean>> response) {
                super.onError(response);
            }
        });
    }

    public void transfer(Activity activity,String amount,String coinName,String free,String memo,String payPass,String toAddress){
        String passwordS = "";

        Map maps = new HashMap();
        maps.put("payPass", payPass);
        maps.put("time", new Date().getTime());

        String toAddressS = "";
        Map mapsS = new HashMap();
        mapsS.put("toAddress", toAddress);
        mapsS.put("time", new Date().getTime());
        try {
            passwordS = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
            toAddressS = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(mapsS));
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("amount",amount);
            jsonObject.put("coinName",coinName);
            jsonObject.put("free",free);
            jsonObject.put("memo",memo);
            jsonObject.put("payPass",passwordS);
            jsonObject.put("toAddress",toAddressS);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.TRANSFER, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.transferSuccess(response.body().msg);
                    } else if (response.body().status == 602) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else {
                        view.transferFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }


    /**
     * 获取余额
     */
    public void getBalance(String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        }else if (symbol.equals("USDS"))  {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        } else if(symbol.equals("HASH")){
            hashMap.put("code", Constants.HASH_CONTRACT_ADDRESS);
        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account", Utils.getSpUtils().getString("mainAccount"));

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (null!=view) {
                            if (response.body().size() > 0) {
                                view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                            } else {
                                view.getBalanceSuccess(symbol, "0.0000");
                            }
                        }}

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        view.getBalanceSuccess(symbol, "0.0000");
                    }

                });

    }

    /**
     * 转账账户是否存在
     * @param
     * @param eosAccountName
     */
    public void checkAccountUseableDataInput(View viewButton, String eosAccountName) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", eosAccountName);

        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_CHIAN_ACCOUNT_INFO, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<BlockChainAccountInfoBean>() {
                    @Override
                    public BlockChainAccountInfoBean convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                        super.onSuccess(response);
                        if (null!=view)
                            view.checkAccountUseableInput(viewButton,false);
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                        super.onError(response);
                        if (null!=view)
                            view.checkAccountUseableInput(viewButton,true);
                    }
                });

    }

    //验证交易密码
    public void checkPayPassword(Activity activity, String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);


                if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showLongToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }

                }else if (response.body().code == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showLongToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }

                } else {
                    if (null != view)
                        view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn, response.body().msg_cn);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);

            }
        });
    }
}
