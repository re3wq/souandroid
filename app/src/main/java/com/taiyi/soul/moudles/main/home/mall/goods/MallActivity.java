package com.taiyi.soul.moudles.main.home.mall.goods;

import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.stx.xhb.pagemenulibrary.PageMenuLayout;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.MallFirstBean;

import com.taiyi.soul.moudles.main.home.mall.goods.adapter.MallAdapter;
import com.taiyi.soul.moudles.main.home.mall.goods.present.MallPresent;
import com.taiyi.soul.moudles.main.home.mall.goods.present.MallView;
import com.taiyi.soul.moudles.main.home.mall.order.OrderActivity;
import com.zhouwei.mzbanner.MZBannerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MallActivity extends BaseActivity<MallView, MallPresent> implements MallView, AdapterManger.GoodsHotItemClick {


    @BindView(R.id.close)
    TextView mClose;
    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.img_car)
    ImageView mImgCar;
    @BindView(R.id.order_iv)
    ImageView orderIv;
    @BindView(R.id.mall_banner)
    MZBannerView mMallBanner;

    @BindView(R.id.mall_discount)
    RecyclerView mMallDiscount;
    @BindView(R.id.topicRecyclerView)
    RecyclerView topicRecyclerView;


    @BindView(R.id.pagemenu)
    PageMenuLayout<MallFirstBean.CatelistBean.ComlistBean> mPageMenuLayout;
    @BindView(R.id.ll_top_dot)
    LinearLayout ll_top_dot;

    @BindView(R.id.mall_recycle)
    RecyclerView mall_recycle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_mall;
    }

    @Override
    public MallPresent initPresenter() {
        return new MallPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mTvTitle.setText(getString(R.string.mall_title));
        mImgCar.setVisibility(View.VISIBLE);
        orderIv.setVisibility(View.VISIBLE);

//        Glide.with(MallActivity.this).load(R.mipmap.mall_car_nom).into(mImgCar);
//        initDiscount();
        showProgress();

    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.getData(this);
        presenter.getCarSizeData(this, 1 + "");
    }

    @Override
    public void onFailure() {
        hideProgress();
    }

    @Override
    public void onSuccess(MallFirstBean bean) {
        hideProgress();
        List<MallFirstBean.CarousellistBean> carousellist = bean.getCarousellist();
        List<MallFirstBean.CatelistBean> catelist = bean.getCatelist();

        for (int i = 0; i < catelist.size(); i++) {
            if(catelist.get(i).getComlist().size()==0){
                catelist.remove(i);
            }
        }
//
        LinearLayoutManager manager = new LinearLayoutManager(MallActivity.this);
        manager.setOrientation(RecyclerView.VERTICAL);
        mall_recycle.setLayoutManager(manager);
        mall_recycle.setAdapter(new MallAdapter(MallActivity.this, carousellist, catelist));

    }

    @Override
    public void onCarSizeSuccess(CarListBean bean) {
        hideProgress();
         if(bean.cartList.size()==0){
             Glide.with(MallActivity.this).load(R.mipmap.mall_car_nom).into(mImgCar);
         }else {
             Glide.with(MallActivity.this).load(R.mipmap.mall_car_have_data).into(mImgCar);
         }
    }

    private List<ImageView> mDotView = new ArrayList<>();

    private int maxWidth;
    int mWidth;
    int mHeight;
    private int mValue;

    @Override
    public void onItemClick(int position, String commodityid) {
        Bundle bundle = new Bundle();
        bundle.putString("commodityid", commodityid);
        ActivityUtils.next(MallActivity.this, GoodsDetailActivity.class, bundle, false);
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }


    @OnClick({R.id.iv_back, R.id.img_car, R.id.mall_hot_more, R.id.mall_discount_more, R.id.order_iv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.img_car:
                ActivityUtils.next(MallActivity.this, ShoppingCartActivity.class, false);
                break;
            case R.id.mall_hot_more:
            case R.id.mall_discount_more:

                ActivityUtils.next(MallActivity.this, MallMoreActivity.class, false);
                break;
            case R.id.order_iv:
                Bundle bundle = new Bundle();
                bundle.putInt("type",0);
                ActivityUtils.next(this, OrderActivity.class,bundle);
//                ActivityUtils.next(this, OrderActivity.class);
                break;
        }
    }
}
