package com.taiyi.soul.moudles.main.node.purchasehashrate;

import com.google.gson.Gson;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.main.node.bean.ComputerPurchaseRecordBean;
import com.taiyi.soul.moudles.main.node.bean.NodeBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class PurchaseHashratePresent extends BasePresent<PurchaseHashrateView> {

    public void purchaseComputer(String money, String num, String payPass, String sign) {
        String passwordS = "";
        Map maps = new HashMap();
        maps.put("payPass", payPass);
        maps.put("time", new Date().getTime());
        try {
            passwordS = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("money", money);
            jsonObject.put("num", num);
            jsonObject.put("payPass", passwordS);
            jsonObject.put("sign", sign);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.PURCHASE_COMPUTER, this, jsonObject.toString(), new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if(null!=view){
                    if (response.body().status == 200) {
                        view.purchaseSuccess(response.body().msg);
                    } else {
                        view.purchaseFailure(response.body().msg);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);

            }
        });
    }

    public void purchaseRecord(int pageNum) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNum", pageNum);
            jsonObject.put("pageSize", 10);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.COMPUTER_PURCHASE_RECORD, this, jsonObject.toString(), new JsonCallback<BaseResultBean<ComputerPurchaseRecordBean>>() {
            @Override
            public BaseResultBean<ComputerPurchaseRecordBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ComputerPurchaseRecordBean>> response) {
                super.onSuccess(response);
                if(null!=view)
                if (response.body().status == 200) {
                    view.getPurchaseRecordSuccess(response.body().data);
                } else {
                    view.getPurchaseRecordFailure(response.body().msg);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ComputerPurchaseRecordBean>> response) {
                super.onError(response);
            }
        });
    }


    /**
     * 获取余额
     */
    public void getBalance(String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account", Utils.getSpUtils().getString("mainAccount"));

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(okhttp3.Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size() > 0) {
                            view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        } else {
                            if(null!=view)
                            view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if(null!=view)
                        view.getBalanceSuccess(symbol, "0.0000");
                    }

                });

    }


    //获取节点数据
    public void getNodeData() {
        HttpUtils.getRequets(BaseUrl.GET_NODE_DATA, this, null, new JsonCallback<BaseResultBean<NodeBean>>() {
            @Override
            public BaseResultBean<NodeBean> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<NodeBean>> response) {
                super.onSuccess(response);
                if (response.body().status == 200) {
                    if(null!=view)
                    view.getNodeDataSuccess(response.body().data);
                } else {
                    if(null!=view)
                    view.getNodeDataFailure(response.body().msg);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<NodeBean>> response) {
                super.onError(response);
            }
        });
    }


    public void getAccountInfoData(String mainAccount) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", mainAccount);
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_CHIAN_ACCOUNT_INFO, this, new Gson().toJson(hashMap), new JsonCallback<BlockChainAccountInfoBean>() {
            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                if (null != view)
                    view.getBlockchainAccountInfoDataHttp(response.body());
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                super.onError(response);
                if (null != view)
                    view.getDataHttpFail(response.code());
            }
        });
    }
}
