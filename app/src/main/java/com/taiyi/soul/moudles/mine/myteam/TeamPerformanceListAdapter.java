package com.taiyi.soul.moudles.mine.myteam;

import android.content.Context;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.moudles.mine.bean.MyTeamPerformanceListBean;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class TeamPerformanceListAdapter extends BaseRecyclerAdapter<MyTeamPerformanceListBean.ListBean>  {


    private PullRecyclerView pullRecyclerView;
    private Context context;

    public TeamPerformanceListAdapter(Context context) {
        this.context = context;
      }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.mine_my_team_item_layout;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final MyTeamPerformanceListBean.ListBean data) {

        TextView dateTv = getView(holder,R.id.dateTv);
        TextView numTv = getView(holder,R.id.numTv);
        TextView performanceTv = getView(holder,R.id.performanceTv);

        String replace = data.getDate().replace("-", "/");
        dateTv.setText(replace);
        numTv.setText(data.getNum() + "");
        performanceTv.setText(data.getAchievement());

    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }
}

