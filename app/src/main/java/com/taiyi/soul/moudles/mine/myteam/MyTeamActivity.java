package com.taiyi.soul.moudles.mine.myteam;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.container.DefaultHeader;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.bean.MyTeamBean;
import com.taiyi.soul.moudles.mine.bean.MyTeamPerformanceListBean;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.MyListView;
import com.taiyi.soul.view.mytreeview.bean.FileBean;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MyTeamActivity extends BaseActivity<MyTeamView, MyTeamPresent> implements MyTeamView, SpringView.OnFreshListener {

    @BindView(R.id.backIv)
    ImageView backIv;
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.teamTv)
    TextView teamTv;
    @BindView(R.id.directPushNumTv)
    TextView directPushNumTv;
    @BindView(R.id.currentLevelTv)
    TextView currentLevelTv;
    @BindView(R.id.todayAddPeopleNumTv)
    TextView todayAddPeopleNumTv;
    @BindView(R.id.todayAddPerformanceTv)
    TextView todayAddPerformanceTv;
    @BindView(R.id.teamTotalPeopleTv)
    TextView teamTotalPeopleTv;
    @BindView(R.id.totalPerformanceTv)
    TextView totalPerformanceTv;
    @BindView(R.id.performanceTv)
    TextView performanceTv;
    @BindView(R.id.accountTv)
    TextView accountTv;
    @BindView(R.id.rightTitleTv)
    TextView rightTitleTv;
    @BindView(R.id.search_text)
    TextView search_text;
    @BindView(R.id.show_all)
    TextView show_all;
    @BindView(R.id.nodata)
    LinearLayout nodata;
    @BindView(R.id.nodata_two)
    LinearLayout nodata_two;
    @BindView(R.id.teamLl)
    LinearLayout teamLl;
    @BindView(R.id.performanceLl)
    LinearLayout performanceLl;
    @BindView(R.id.top)
    ConstraintLayout top;
    @BindView(R.id.recyclerView)
    PullRecyclerView recyclerView;
    //    @BindView(R.id.recyclerView)
//    RecyclerView recyclerView;
    @BindView(R.id.springView)
    SpringView springView;
    @BindView(R.id.lv)
    MyListView lv;
    @BindView(R.id.avatarIv)
    ImageView avatarIv;

    private String payPwd;
    private int pageNo = 1;
    private int stop = 3;
    private List<MyTeamPerformanceListBean.ListBean> performanceList = new LinkedList<>();
    List<FileBean> accountlist = new ArrayList<>();
    private TeamPerformanceListAdapter mPerformanceListAdapter;
    private String lang;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_team;
    }

    @Override
    public MyTeamPresent initPresenter() {
        return new MyTeamPresent();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.my_team));
        teamTv.setSelected(true);
        payPwd = getIntent().getExtras().getString("payPwd");
        lang = Utils.getSpUtils().getString("current_language", "");
        lv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    private List<FileBean> mFileListLast = new ArrayList<>();
    private TeamTreeListViewAdapter<FileBean> mAdapter;


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    protected void initData() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setOnPullLoadMoreListener(null);
        recyclerView.setIsRefreshEnabled(false);
        recyclerView.setIsLoadMoreEnabled(false);
//        recyclerView.setEmptyView(LayoutInflater.from(this).inflate(R.layout.layout_empty,null));
        mPerformanceListAdapter = new TeamPerformanceListAdapter(this);
        mPerformanceListAdapter.setPullRecyclerView(recyclerView);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mPerformanceListAdapter);
        recyclerView.refreshWithPull();
        recyclerView.setPullLoadMoreCompleted();


        showProgress();
        presenter.getMyTeamData(payPwd);
        //搜索 默认""  账户名限制12 .ngk结尾   邀请码 6
        presenter.getTeam("", "", stop);
    }

    @Override
    public void initEvent() {
        springView.setFooter(new DefaultFooter(this));
        springView.setHeader(new DefaultHeader(this));
        springView.setEnable(false);
        springView.setGive(SpringView.Give.BOTH);
        springView.setType(SpringView.Type.FOLLOW);
        springView.setListener(null);
    }


    @OnClick({R.id.backIv, R.id.rightTitleTv, R.id.teamTv, R.id.performanceTv, R.id.show_all, R.id.search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightTitleTv:
                break;
            case R.id.teamTv:
                springView.setListener(null);
                springView.setEnable(false);
                teamTv.setSelected(true);
                performanceTv.setSelected(false);
                teamLl.setVisibility(View.VISIBLE);
                performanceLl.setVisibility(View.GONE);
                break;
            case R.id.performanceTv:
                springView.setEnable(true);
                springView.setListener(this);
                teamTv.setSelected(false);
                performanceTv.setSelected(true);
                showProgress();
                presenter.getPerformance(pageNo);
                teamLl.setVisibility(View.GONE);
                performanceLl.setVisibility(View.VISIBLE);
                break;
            case R.id.show_all:
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.show_all, 2000);
                if (!fastDoubleClick) {
                    if (stop == 3) {
                        stop = -1;
                        show_all.setText("");
                        show_all.setText(getResources().getString(R.string.expand_nom));
                    } else {
                        stop = 3;
                        show_all.setText(getResources().getString(R.string.expand_all));
                    }
                    showProgress();
                    sumNumNew = 0;
                    presenter.getTeam("", "", stop);

                }

                break;
            case R.id.search:
                if (search_text.getText().toString().equals("")) {
                    presenter.getTeam("", "", stop);
                } else {
                    if (search_text.getText().toString().length() == 12 || search_text.getText().toString().length() == 6) {
                        if (search_text.getText().toString().length() == 12 && search_text.getText().toString().endsWith(".sou")) {
                            presenter.getTeam(search_text.getText().toString(), "", stop);
                        } else if (search_text.getText().toString().length() == 6) {
                            presenter.getTeam("", search_text.getText().toString(), stop);
                        }
                    } else {
                        return;
                    }

                }
                break;

        }
    }

    private String replace;

    @Override
    public void onSuccess(MyTeamBean myTeamBean) {
        hideProgress();
        RequestOptions options = new RequestOptions();
        options.centerCrop().placeholder(R.mipmap.mine_avatar_iv).error(R.mipmap.mine_avatar_iv);
        Glide.with(this).load(myTeamBean.getHeaderImg()).apply(options).into(avatarIv);
        if (null == myTeamBean) {
            accountTv.setText("");
            directPushNumTv.setText(getString(R.string.direct_push_number) + 0);

            if (lang.equals("zh") || lang.equals("TW")) {
                currentLevelTv.setText(getResources().getString(R.string.no));
            } else {
                currentLevelTv.setText(getString(R.string.current_level) + getResources().getString(R.string.no));
            }

            todayAddPeopleNumTv.setText(getString(R.string.today_add_number) + 0);
            todayAddPerformanceTv.setText(getString(R.string.today_add_money) + 0);
            teamTotalPeopleTv.setText(getString(R.string.team_total_number) + 0);
            totalPerformanceTv.setText(getString(R.string.total_performance) + 0);
        } else {
            String mainAccount = myTeamBean.getMainAccount();
            int loginType = Utils.getSpUtils().getInt("loginType");
            if (loginType == 0) {//手机号
//                if (!TextUtils.isEmpty(mainAccount)) {
//                    if (mainAccount.length() >= 12) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 7, mainAccount.length() - 3, "****").toString();
//                    } else if (mainAccount.length() >= 11) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 7, mainAccount.length() - 3, "****").toString();
//                    } else if (mainAccount.length() >= 10) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 6, mainAccount.length() - 2, "****").toString();
//                    } else if (mainAccount.length() >= 9) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 5, mainAccount.length() - 1, "****").toString();
//                    } else if (mainAccount.length() >= 8) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 4, mainAccount.length(), "****").toString();
//                    } else if (mainAccount.length() >= 7) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 4, mainAccount.length() - 1, "***").toString();
//                    } else if (mainAccount.length() >= 6) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 4, mainAccount.length() - 2, "**").toString();
//                    } else if (mainAccount.length() >= 5) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 3, mainAccount.length() - 1, "**").toString();
//                    } else if (mainAccount.length() >= 4) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 2, mainAccount.length(), "**").toString();
//                    } else if (mainAccount.length() >= 3) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 2, mainAccount.length() - 1, "*").toString();
//                    } else if (mainAccount.length() >= 2) {
//                        replace = new StringBuffer(mainAccount).replace(mainAccount.length() - 1, mainAccount.length(), "*").toString();
//                    } else if (mainAccount.length() >= 1) {
//                        replace = "*";
//                    }
//                    accountTv.setText(replace);
//                }
                accountTv.setText(mainAccount);
            } else {

//                String[] split = mainAccount.split("@");
//                if (split[0].length() >= 12) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 8, split[0].length() - 4, "****").toString();
//                } else if (split[0].length() >= 11) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 7, split[0].length() - 3, "****").toString();
//                }else if (split[0].length() >= 10) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 6, split[0].length() - 2, "****").toString();
//                }else if (split[0].length() >= 9) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 5, split[0].length()-1, "****").toString();
//                }else if (split[0].length() >= 8) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 4, split[0].length(), "****").toString();
//                }else if (split[0].length() >= 7) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 4, split[0].length()-1, "***").toString();
//                }else if (split[0].length() >= 6) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 4, split[0].length()-2, "**").toString();
//                }else if (split[0].length() >= 5) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 3, split[0].length()-1, "**").toString();
//                }else if (split[0].length() >= 4) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 2, split[0].length(), "**").toString();
//                }else if (split[0].length() >= 3) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 2, split[0].length()-1, "*").toString();
//                }else if (split[0].length() >= 2) {
//                    replace = new StringBuffer(split[0]).replace(split[0].length() - 1, split[0].length(), "*").toString();
//                }else if (split[0].length() >= 1) {
//                    replace = "*";
//                }
//                accountTv.setText(replace+"@"+split[1]);
                accountTv.setText(mainAccount);
            }
//            accountTv.setText(myTeamBean.getMainAccount().replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2"));
            directPushNumTv.setText(getString(R.string.direct_push_number) + myTeamBean.getDirectPushNum());
            if (myTeamBean.getCommunityLevel().equals("0")) {
                if (lang.equals("zh") || lang.equals("TW")) {
                    currentLevelTv.setText(getResources().getString(R.string.no));
                } else {
                    currentLevelTv.setText(getString(R.string.current_level) + getResources().getString(R.string.no));
                }
            } else {
                if (lang.equals("zh") || lang.equals("TW")) {
                    currentLevelTv.setText("VIP" + myTeamBean.getCommunityLevel());
                } else {
                    currentLevelTv.setText(getString(R.string.current_level) + "VIP" + myTeamBean.getCommunityLevel());
                }
            }
            todayAddPeopleNumTv.setText(getString(R.string.today_add_number) + myTeamBean.getTodayTotalPeople());
            todayAddPerformanceTv.setText(getString(R.string.today_add_money) + myTeamBean.getTodayAddAchievement());
            teamTotalPeopleTv.setText(getString(R.string.team_total_number) + myTeamBean.getTotalPeople());

            DecimalFormat format = new DecimalFormat("0.0000");
            String format1 = format.format(Double.parseDouble(myTeamBean.getTotalAchievement()));

            totalPerformanceTv.setText(getString(R.string.total_performance) + NumUtils.subZeroAndDot(format1));
        }

    }

    @Override
    public void onFailure(String errorMsg) {
        hideProgress();
        recyclerView.setPullLoadMoreCompleted();
        toast(errorMsg);
    }

    @Override
    public void loginInvalid() {
        //  AppManager.getAppManager().finishAllActivity();
        if (Constants.isL == 0) {
            Constants.isL = 1;
            //  AppManager.getAppManager().finishAllActivity();
            ActivityUtils.next(this, LoginActivity.class, true);
            Utils.getSpUtils().remove(Constants.TOKEN);
        }
    }

    //业绩列表数据
    @Override
    public void getPerformanceSuccess(MyTeamPerformanceListBean myTeamPerformanceListBean) {
        hideProgress();
        springView.onFinishFreshAndLoad();
        List<MyTeamPerformanceListBean.ListBean> list = myTeamPerformanceListBean.getList();

        if (pageNo == 1) {
            performanceList.clear();
            performanceList.addAll(list);
            mPerformanceListAdapter.setDatas(list);
        } else {
            performanceList.addAll(list);
            mPerformanceListAdapter.addDatas(list);
        }
        recyclerView.setPullLoadMoreCompleted();
//             performanceList.clear();
//            performanceList.addAll(list);
//            CommonAdapter myTeamPerformanceListAdapter = AdapterManger.getMyTeamPerformanceListAdapter(this, performanceList);
//            recyclerView.setAdapter(myTeamPerformanceListAdapter);
//            nodata_two.setVisibility(View.GONE);
        if (performanceList.size() > 0) {
            nodata_two.setVisibility(View.GONE);
        } else {
            nodata_two.setVisibility(View.VISIBLE);
        }


    }


    //获取团队列表
    @Override
    public void getTeamSuccess(int status, String msg, List<Object> date) {

        if (status == 200) {
            if (date.size() > 0) {
                nodata.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
                mFileListLast.clear();
                accountlist.clear();
                try {
                    JSONArray arr = new JSONArray(date);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject temp = arr.getJSONObject(i);
                        Long id = temp.getLong("id");
                        Long parentId = temp.getLong("parentId");
                        String account = temp.getString("account");
                        int sum = temp.getInt("sum");

//                        Log.e("one===", id + "   " + parentId);
                        //01  0
                        //02   0
                        accountlist.add(new FileBean(id + "", "0", account, sum + ""));

                        JSONArray list = temp.getJSONArray("list");
                        sumNum = 1;
                        if (list.length() > 0) {
                            getNodes(list);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Utils.getSpUtils().put("NsumNumNew", sumNumNew);


//                Log.e("nummm===",sumNumNew+"");
                initListView();

            } else {
                nodata.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
            }
            hideProgress();
//
        } else if (status == 602) {
            hideProgress();
            if (Constants.isL == 0) {
                Constants.isL = 1;
                AlertDialogShowUtil.toastMessage(this, msg);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        } else {
            hideProgress();
            AlertDialogShowUtil.toastMessage(this, msg);
        }

    }

    private int sumNum;
    private int sumNumNew = 0;

    public void getNodes(JSONArray date) {
        sumNum++;
        try {
            for (int i = 0; i < date.length(); i++) {
                JSONObject temp = date.getJSONObject(i);
                Long id = temp.getLong("id");
                Long parentId = temp.getLong("parentId");
                String account = temp.getString("account");
                int sum = temp.getInt("sum");
                accountlist.add(new FileBean(id + "", parentId + "", account, sum + ""));
                JSONArray list = temp.getJSONArray("list");
                if (list.length() > 0) {
                    getNodes(list);
                }

            }
            if (sumNum > sumNumNew) {
                sumNumNew = sumNum;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void initListView() {
        try {
            mAdapter = new TeamTreeListViewAdapter<>(this, "", "1", lv, mFileListLast, 0);
            lv.setAdapter(mAdapter);
            mAdapter.setOnTreeNodeClickListener(new TeamTreeListViewAdapter.OnTreeNodeClickListener() {
                @Override
                public void onNodeClick(Node node, int position) {

                }
            });
            mAdapter.setOnCopyClickListener(new TeamTreeListViewAdapter.OnCopyClickListener() {
                @Override
                public void onCopyClick(String value) {

                }
            });
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        List<FileBean> list = new ArrayList<>();
//        list.addAll(accountlist);
        for (int i = 0; i < accountlist.size(); i++) {
            list.add(new FileBean(accountlist.get(i).getUserid(), accountlist.get(i).getFatherid(), accountlist.get(i).getUsername(), accountlist.get(i).getNum()));
        }
        initList(list);
        try {
            mAdapter.refresh(mFileListLast, 0);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void initList(List<FileBean> list) {
        if (list == null || list.size() <= 0) return;
        for (FileBean item : list) {
            mFileListLast.add(item);
            if (item.getXiaList() != null && item.getXiaList().size() > 0) {
                initList(item.getXiaList());
            }
        }
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        showProgress();
        presenter.getPerformance(pageNo);
    }

    @Override
    public void onLoadmore() {
        pageNo += 1;
        showProgress();
        presenter.getPerformance(pageNo);
    }

//    @Override
//    public void onLoadMore() {
//        pageNo += 1;
//        showProgress();
//        presenter.getPerformance(pageNo);
//    }

}
