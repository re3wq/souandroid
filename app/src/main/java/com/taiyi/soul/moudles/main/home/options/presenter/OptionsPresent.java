package com.taiyi.soul.moudles.main.home.options.presenter;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class OptionsPresent extends BasePresent<OptionsView> {
    public void getData(Activity activity, String term, String reversed) {
        Map map = new HashMap();
        map.put("term", term);//1成交量2名称3涨幅
        map.put("reversed", reversed);//1 true 0false
        map.put("coin", Utils.getSpUtils().getString("cur_coin_name"));

        HttpUtils.getRequets(BaseUrl.MAKETALL, this, map, new JsonCallback<BaseResultBean<List<OptionListBean>>>() {
            @Override
            public BaseResultBean<List<OptionListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<OptionListBean>>> response) {
                super.onSuccess(response);
                if (response.body().status == 602) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else {
                    if(null!=view){
                        view.getDataSuccess(response.body().status, response.body().msg, response.body().data);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<OptionListBean>>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }
}
