package com.taiyi.soul.moudles.main.home;

import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;
import com.taiyi.soul.moudles.main.home.bean.HomeBean;
import com.taiyi.soul.moudles.main.home.bean.OptionIsOptenBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.mine.bean.CurrencyListBean;

import java.util.List;

public interface HomeView {
    void onSuccess(HomeBean homeBean);
    void onFailure(String errorMsg);
    void updateSuccess();
    void updateFailure(String errorMsg);
    void activitySuccess(ActivityFirstBean activityFirstBean);
    void activityFailure(int code, String errorMsg);
    void getCoinSuccess(int status, String msg, List<CurrencyListBean> data);
    void getOptionOption(int status, String msg_cn, OptionIsOptenBean data);
    void getDataSuccess(int status, String msg, List<OptionListBean> data);
}
