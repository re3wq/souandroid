package com.taiyi.soul.moudles.main.node.hashmarket.transactionrecord;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import android.view.View;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.taiyi.soul.R;

import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.node.hashmarket.fragment.HashOneFragment;
import com.taiyi.soul.moudles.main.node.hashmarket.fragment.HashThreeFragment;
import com.taiyi.soul.moudles.main.node.hashmarket.fragment.HashTwoFragment;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.OnClick;

public class TransactionRecordActivity extends BaseActivity<NormalView, NormalPresenter> {

    @BindView(R.id.titleTv)
    TextView mTvTitle;

    @BindView(R.id.entrust_tab)
    SlidingTabLayout mEntrustTab;
    @BindView(R.id.entrust_viewpager)
    ViewPager mEntrustViewpager;


    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private String[] mTitles;


    @BindView(R.id.top)
    ConstraintLayout top;


    @Override
    protected int getLayoutId() {
//        return R.layout.activity_transaction_record;
        return R.layout.activity_transaction_record_new;

    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        mTvTitle.setText(getString(R.string.transaction_record));
        mTitles = new String[]{getResources().getString(R.string.deal_entrust_one),
                getResources().getString(R.string.deal_entrust_two),getResources().getString(R.string.deal_entrust_three)};
        mFragments.add(new HashOneFragment());//
        mFragments.add(new HashTwoFragment());//
        mFragments.add(new HashThreeFragment());//

        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mEntrustViewpager.setAdapter(mAdapter);

        mEntrustTab.setIsBackGround(true);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.node_hash_market_sel_iv3);
        mEntrustTab.setDrawBitMap(bitmap, 126);
        mEntrustTab.setViewPager(mEntrustViewpager);
        mEntrustViewpager.setCurrentItem(0);

        mEntrustTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mAdapter.notifyDataSetChanged();
                EventBus.getDefault().post("refresh_anim");
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

//        CommonAdapter transactionRecordListAdapter = AdapterManger.getTransactionRecordListAdapter(this, data);

    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
