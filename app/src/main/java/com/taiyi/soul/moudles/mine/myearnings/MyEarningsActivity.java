package com.taiyi.soul.moudles.mine.myearnings;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import android.view.View;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.mine.myearnings.fragment.EarningsFragment;
import com.taiyi.soul.moudles.mine.myearnings.fragment.EarningsFragmentOne;
import com.taiyi.soul.moudles.mine.myearnings.fragment.EarningsFragmentThree;
import com.taiyi.soul.moudles.mine.myearnings.fragment.EarningsFragmentTwo;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.view.HorizontalCanScrollViewPager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class MyEarningsActivity extends BaseActivity<NormalView, NormalPresenter> {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.top)
    ConstraintLayout top;
    @BindView(R.id.slidingTabLayout)
    SlidingTabLayout slidingTabLayout;
    @BindView(R.id.viewPager)
    HorizontalCanScrollViewPager viewPager;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private String[] mTitles;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_earnings;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }


    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.my_earnings));
        mTitles = new String[]{getResources().getString(R.string.static_income),
                getResources().getString(R.string.earning),
                getResources().getString(R.string.community_revenue),
                getResources().getString(R.string.global_shareholders)};

        mFragments.add(new EarningsFragment());
        mFragments.add(new EarningsFragmentOne());
        mFragments.add(new EarningsFragmentTwo());
        mFragments.add(new EarningsFragmentThree());

        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myPagerAdapter);

        slidingTabLayout.setIsBackGround(true);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.node_hash_market_sel_iv2);
        slidingTabLayout.setDrawBitMap(bitmap, 126);
        slidingTabLayout.setViewPager(viewPager);
//        viewPager.setCurrentItem(0);
//        slidingTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
//            @Override
//            public void onTabSelect(int position) {
//                EventBus.getDefault().post(new DataRefreshBean("refresh_data", position + ""));
//            }
//
//            @Override
//            public void onTabReselect(int position) {
//
//            }
//        });


    }

    @Override
    protected void initData() {
//        showProgress();
//        presenter.getList(pageNo, "");
//        presenter.getMyEarnings("1");
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }


}
