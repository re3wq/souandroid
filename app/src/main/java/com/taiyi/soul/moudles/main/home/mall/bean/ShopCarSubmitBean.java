package com.taiyi.soul.moudles.main.home.mall.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\1 0001 15:53
 * @Author : yuan
 * @Describe ：准备下单返回数据
 */
public class ShopCarSubmitBean implements Serializable {

        public AddressParamsBean addressParams;
        public int account;
        public List<ProductBean> product;

        public static class AddressParamsBean implements Serializable{
            public String createtime;
            public String address;
            public String useraddress_id;
            public String cityid;
            public String type;
            public String provinceid;
            public String userid;
            public String countryid;
            public String issh;
            public String cityName;
            public String provincename;
            public String surname;
            public String name;
            public String tel;
            public String countryName;
            public String delflag;
        }

        public static class ProductBean implements Serializable{
            public String flag;
            public String commodityid;
            public String salenum;
            public String type;
            public String commodityspecid;
            public String userid;
            public String classificationid;
            public int number;
            public String imgurl;
            public String price;
            public String visitgoodsnum;
            public String businessuserid;
            public String cashPrice;
            public String stock;
            public String hybvprice;
            public String delflag;
            public String recommendflag;
            public String createtime;
            public String yunfei;
            public String bvprice;
            public String shopcarid;
            public String name;
            public String hyprice;
            public String typename;
            public String smallimgurl;
        }



//    public AddressParamsBean addressParams;
//    public String account;
//    public List<ProductBean> product;
//
//    public static class AddressParamsBean implements Serializable{
//        public String createtime;
//        public String address;
//        public String useraddress_id;
//        public String cityid;
//        public String type;
//        public String provinceid;
//        public String userid;
//        public String issh;
//        public String areaid;
//        public String cityName;
//        public String provincename;
//        public String areaName;
//        public String name;
//        public String tel;
//        public String delflag;
//    }
//
//    public static class ProductBean implements Serializable{
//        public String flag;
//        public String commodityid;
//        public String salenum;
//        public String type;
//        public String commodityspecid;
//        public String userid;
//        public String classificationid;
//        public int number;
//        public String imgurl;
//        public String price;
//        public String visitgoodsnum;
//        public String businessuserid;
//        public String cashPrice;
//        public String stock;
//        public String hybvprice;
//        public String delflag;
//        public String recommendflag;
//        public String createtime;
//        public String yunfei;
//        public String bvprice;
//        public String shopcarid;
//        public String name;
//        public String hyprice;
//        public String typename;
//        public String smallimgurl;
//    }
}
