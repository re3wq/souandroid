package com.taiyi.soul.moudles.main.deal.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.animation.AnimationUtils;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.deal.adapter.EntrustThreeAdapter;
import com.taiyi.soul.moudles.main.deal.bean.EntrustOneBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustThreeBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustTwoBean;
import com.taiyi.soul.moudles.main.deal.present.EntrustPresent;
import com.taiyi.soul.moudles.main.deal.present.EntrustView;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import butterknife.BindView;


/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\8 0008 11:21
 * @Author : yuan
 * @Describe ：集市交易-我的委托-历史委托
 */
public class EntrustThreeFragment extends BaseFragment<EntrustView, EntrustPresent> implements EntrustView, PullRecyclerView.PullLoadMoreListener {


    @BindView(R.id.pull_recycle)
    PullRecyclerView mPayRecycle;
    private EntrustThreeAdapter mFragmentAdapter;
    private int pageNo = 1;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_entrust_three;
    }

    @Override
    public EntrustPresent initPresenter() {
        return new EntrustPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if (EventBus.getDefault().isRegistered(false)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void initData() {

        mPayRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        mPayRecycle.setOnPullLoadMoreListener(this);
        mPayRecycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));

        mFragmentAdapter = new EntrustThreeAdapter(getContext());
        mFragmentAdapter.setPullRecyclerView(mPayRecycle);
        mPayRecycle.setItemAnimator(new DefaultItemAnimator());
        mFragmentAdapter.notifyDataSetChanged();
        mPayRecycle.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_anim));
        mPayRecycle.scheduleLayoutAnimation();

        mPayRecycle.setAdapter(mFragmentAdapter);
        mPayRecycle.refreshWithPull();


    }

    @Override
    public void initEvent() {

    }

    @Subscribe
    public void event(String event) {

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
//        Log.e("fragment==", "hidden");
        if (!hidden) {
            presenter.getThreeData(getActivity(), "1", pageNo + "", mPayRecycle);
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();

    }

    /************************************Refresh and load more****************************************************/

    @Override
    public void onRefresh() {
        pageNo = 1;
        presenter.getThreeData(getActivity(), "1", pageNo + "", mPayRecycle);

    }

    @Override
    public void onLoadMore() {
        pageNo++;
        presenter.getThreeData(getActivity(), "1", pageNo + "", mPayRecycle);

    }

    /************************************Call Interface callback****************************************************/

    @Override
    public void onSuccess(int code, EntrustOneBean bean) {

    }

    @Override
    public void onTwoSuccess(int code, EntrustTwoBean bean) {

    }

    @Override
    public void onThreeSuccess(int code, EntrustThreeBean bean) {//获取当前委托数据
        if (pageNo == 1) {
            mFragmentAdapter.setDatas(bean.comlist);
        } else {
            mFragmentAdapter.addDatas(bean.comlist);
        }
        mPayRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onFailure() {
        hideProgress();
    }

    @Override
    public void onCancelSuccess(int code, String data) {

    }
}
