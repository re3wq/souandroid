package com.taiyi.soul.moudles.main.node.cpubandwidth;

import android.content.Context;

import com.google.gson.Gson;
import com.lzy.okgo.model.Response;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.blockchain.PushDataManger;
import com.taiyi.soul.moudles.main.node.cpubandwidth.bean.StakeBean;
import com.taiyi.soul.moudles.main.node.cpubandwidth.bean.UnstakeBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.taiyi.soul.base.Constants.ACTIONDELEGATEBW;
import static com.taiyi.soul.base.Constants.ACTION_UNDELEGATEBW;
import static com.taiyi.soul.base.Constants.DELEGATEBWCONTRACT;

public class CupNetPresent extends BasePresent<CpuNetView> {

    private Context context;

    public CupNetPresent(Context context) {
        this.context = context;
    }

    public void getAccountInfoData() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", Utils.getSpUtils().getString("mainAccount"));
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_CHIAN_ACCOUNT_INFO, this, new Gson().toJson(hashMap), new JsonCallback<BlockChainAccountInfoBean>() {
            @Override
            public void onSuccess(Response<BlockChainAccountInfoBean> response) {
                if (null!=view) {
                    view.getBlockchainAccountInfoDataHttp(response.body());
                }
            }

            @Override
            public void onError(Response<BlockChainAccountInfoBean> response) {
                super.onError(response);
                if (null!=view) {
                    view.getDataHttpFail(context.getString(R.string.socket_exception));
                }
            }
        });
    }


    /**
     * 获取NGK余额
     */
    public void getBalance() {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);

        hashMap.put("account", Utils.getSpUtils().getString("mainAccount"));

        hashMap.put("symbol", "SOU");

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(okhttp3.Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (null!=view) {
                            if (response.body().size() > 0) {
                                view.getBalanceSuccess(response.body().get(0).split(" ")[0]);
                            } else {
                                view.getBalanceSuccess("0.0000");
                            }
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        view.getBalanceSuccess("0.0000");
                    }

                });

    }



    /**
     * 抵押
     */
    public void addStake(String from,String to,String cpu_amount,String net_amount) {


        StakeBean stakeBean = new StakeBean();
        stakeBean.setFrom(from);
        stakeBean.setReceiver(to);
        stakeBean.setStake_cpu_quantity(new DecimalFormat("##0.0000").format(new Double(cpu_amount)) + " SOU");
        stakeBean.setStake_net_quantity(new DecimalFormat("##0.0000").format(new Double(net_amount)) + " SOU");
//        if (identity_type == 0) {
//            stakeBean.setTransfer(false);
//            //Log.i("wang", "自己抵押");
//        }else {
//            if(status_type == 0){
//                stakeBean.setTransfer(false);//租借
//            }else {
//                stakeBean.setTransfer(true);//过户
//            }
//
//        }

        new PushDataManger(context, new PushDataManger.Callback() {
            @Override
            public void getResult(String result) {
                if (null!=view) {
                    if (result.contains("transaction_id")) {
                        view.stakeSuccess();
                    } else {
                        view.getDataHttpFail(context.getResources().getString(R.string.di_faile));
                    }
                }
            }

            @Override
            public void fail() {
                if (null!=view) {
                    view.getDataHttpFail(context.getResources().getString(R.string.di_faile));
                }
            }


        }).pushAction(
                DELEGATEBWCONTRACT,
                ACTIONDELEGATEBW,
                new Gson().toJson(stakeBean),
                from);

    }


    /**
     * 赎回
     */
    public void unStake(String from,String to,String cpu_amount,String net_amount) {

        UnstakeBean unstakeBean = new UnstakeBean();
        unstakeBean.setFrom(from);
        unstakeBean.setReceiver(to);

        unstakeBean.setUnstake_cpu_quantity(new DecimalFormat("##0.0000").format(new Double(cpu_amount)) + " SOU");
        unstakeBean.setUnstake_net_quantity(new DecimalFormat("##0.0000").format(new Double(net_amount)) + " SOU");



        new PushDataManger(context, new PushDataManger.Callback() {
            @Override
            public void getResult(String result) {
                if (null!=view) {
                    if (result.contains("transaction_id")) {
                        view.unStackSuccess();
                    } else {
                        view.getDataHttpFail(context.getResources().getString(R.string.shu_faile));
                    }
                }
            }

            @Override
            public void fail() {
                if (null!=view) {
                    view.getDataHttpFail(context.getResources().getString(R.string.shu_faile));
                }
            }


        }).pushAction(
                DELEGATEBWCONTRACT,
                ACTION_UNDELEGATEBW,
                new Gson().toJson(unstakeBean),
                from);

    }

    //验证交易密码
    public void checkPayPassword(String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

}
