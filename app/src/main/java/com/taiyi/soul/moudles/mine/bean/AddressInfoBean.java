package com.taiyi.soul.moudles.mine.bean;

public class AddressInfoBean {

    /**
     * createtime : 2020-08-03 15:55:40
     * address : 啊咯的好像
     * useraddress_id : 1
     * cityid : 1000020
     * type : 1
     * provinceid : 100002
     * userid : 15952243829550305
     * issh : 2
     * cityName : 南开
     * provincename : 天津
     * name : 啦啦啦
     * tel : 18322125252
     * delflag : 1
     */

    private String createtime;
    private String address;
    private int useraddress_id;
    private String cityid;
    private String type;
    private String provinceid;
    private String userid;
    private String issh;
    private String cityName;
    private String provincename;
    private String surname;
    private String name;
    private String tel;
    private String delflag;
    private String countryName;
    private String countryid;

    public String getCountryid() {
        return countryid;
    }

    public void setCountryid(String countryid) {
        this.countryid = countryid;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getUseraddress_id() {
        return useraddress_id;
    }

    public void setUseraddress_id(int useraddress_id) {
        this.useraddress_id = useraddress_id;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProvinceid() {
        return provinceid;
    }

    public void setProvinceid(String provinceid) {
        this.provinceid = provinceid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getIssh() {
        return issh;
    }

    public void setIssh(String issh) {
        this.issh = issh;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvincename() {
        return provincename;
    }

    public void setProvincename(String provincename) {
        this.provincename = provincename;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDelflag() {
        return delflag;
    }

    public void setDelflag(String delflag) {
        this.delflag = delflag;
    }
}
