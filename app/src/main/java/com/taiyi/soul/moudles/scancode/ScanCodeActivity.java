package com.taiyi.soul.moudles.scancode;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.FilesUtils;
import com.taiyi.soul.utils.ShowDialog;
import com.taiyi.soul.utils.Utils;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.mylhyl.zxing.scanner.OnScannerCompletionListener;
import com.mylhyl.zxing.scanner.ScannerView;
import com.mylhyl.zxing.scanner.decode.QRDecode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import me.ljp.permission.PermissionItem;

public class ScanCodeActivity extends BaseActivity<NormalView, NormalPresenter> implements NormalView {

    @BindView(R.id.scanner_view)
    ScannerView mScannerView;
    @BindView(R.id.iv_back)
    ImageView mIvBack;
    @BindView(R.id.ll)
    RelativeLayout mLl;
    @BindView(R.id.tv_right)
    TextView mTvRight;
    private static final int CHOOSE_PICTURE = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scan;
    }


    @Override
    protected void initViews(Bundle savedInstanceState) {


        mScannerView.setMediaResId(R.raw.beep);//设置扫描成功的声音
        mScannerView.setLaserFrameBoundColor(getResources().getColor(R.color.theme_color));
        mScannerView.setLaserColor(getResources().getColor(R.color.theme_color));
//        mScannerView.toggleLight(true);//切换闪光灯
        mScannerView.setDrawText(getString(R.string.scan_info),true);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mScannerView.setOnScannerCompletionListener(new OnScannerCompletionListener() {
            @Override
            public void onScannerCompletion(Result result, ParsedResult parsedResult, Bitmap bitmap) {
                pareCode(parsedResult.toString());
            }

        });
        mTvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<PermissionItem> permissonItems = new ArrayList<PermissionItem>();
                permissonItems.add(new PermissionItem(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.WRITE_STORAGE), R.drawable.permission_card1));
                permissonItems.add(new PermissionItem(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.READ_STORAGE), R.drawable.permission_card1));
                if(Utils.getPermissions(permissonItems , getString(R.string.SCAN_EXTERNAL_STORAGE))){
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, CHOOSE_PICTURE);
                }
            }
        });
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void onResume() {
        mScannerView.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mScannerView.onPause();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ScanCodeActivity.RESULT_OK && requestCode == CHOOSE_PICTURE) {
            //获取uri拿bitmap要做压缩处理，防止oom
            ShowDialog.showDialog(this, getString(R.string.recognition), true, null);
            Uri originalUri = null;
            File file = null;
            if (null != data) {
                originalUri = data.getData();
                file = FilesUtils.getFileFromMediaUri(ScanCodeActivity.this, originalUri);
            }
            Bitmap photoBmp = null;
            try {
                photoBmp = FilesUtils.getBitmapFormUri(ScanCodeActivity.this, Uri.fromFile(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
            int degree = FilesUtils.getBitmapDegree(file.getAbsolutePath());
            //把图片旋转为正的方向
            Bitmap newbitmap = FilesUtils.rotateBitmapByDegree(photoBmp, degree);
            QRDecode.decodeQR(newbitmap, new OnScannerCompletionListener() {
                @Override
                public void onScannerCompletion(Result result, ParsedResult parsedResult, Bitmap bitmap) {
                    ShowDialog.dissmiss();
                    if (parsedResult != null) {
                        pareCode(parsedResult.toString());

                    } else {
                        toast(getString(R.string.scanner_error_toast));
                    }
                }
            });
        }
    }

    public void pareCode(String data) {

        Bundle bundle = new Bundle();

        if (getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from").equals("import_account")){
            bundle.putString("key_word", data);
            ActivityUtils.goBackWithResult(ScanCodeActivity.this, 100, bundle);
        }else {
            bundle.putString("id", data);
            ActivityUtils.goBackWithResult(ScanCodeActivity.this, 300, bundle);
        }



    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarColor(R.color.transparent).titleBar(mLl).statusBarDarkFont(false, 0f).init();

    }


}
