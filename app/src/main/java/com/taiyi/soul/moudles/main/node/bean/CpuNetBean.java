package com.taiyi.soul.moudles.main.node.bean;

public class CpuNetBean {
    private String name;
    private int resId;
    private String speed;

    public CpuNetBean(String name, int resId, String speed) {
        this.name = name;
        this.resId = resId;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
