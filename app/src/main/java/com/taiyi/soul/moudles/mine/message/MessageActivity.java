package com.taiyi.soul.moudles.mine.message;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.mine.message.bean.MessageBean;
import com.taiyi.soul.moudles.mine.message.list.MessageListActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class MessageActivity extends BaseActivity<MessageView, MessagePresent> implements MessageView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.rightTitleTv)
    TextView rightTitleTv;
    @BindView(R.id.messageTimeTv)
    TextView messageTimeTv;
    @BindView(R.id.contentTv)
    TextView messageContentTv;
    @BindView(R.id.isOpenIv)
    ImageView messageIsOpenIv;
    @BindView(R.id.noticeTimeTv)
    TextView noticeTimeTv;
    @BindView(R.id.noticeContentTv)
    TextView noticeContentTv;
    @BindView(R.id.noticeIsOpenIv)
    ImageView noticeIsOpenIv;
    @BindView(R.id.top)
    ConstraintLayout top;

   @BindView(R.id.messageCl)
    ConstraintLayout messageCl;
  @BindView(R.id.noticeCl)
    ConstraintLayout noticeCl;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message;
    }

    @Override
    public MessagePresent initPresenter() {
        return new MessagePresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.message));
        rightTitleTv.setText(getString(R.string.all_read));
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgress();
        presenter.getMessage(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.rightTitleTv, R.id.messageCl, R.id.noticeCl})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightTitleTv:
                showProgress();
                presenter.allRead(this, "0");
                break;
            case R.id.messageCl:
                Bundle bundle = new Bundle();
                bundle.putInt("type", 1);
                ActivityUtils.next(this, MessageListActivity.class, bundle);
                break;
            case R.id.noticeCl:
                Bundle bundle_ = new Bundle();
                bundle_.putInt("type", 2);
                ActivityUtils.next(this, MessageListActivity.class, bundle_);
                break;
        }
    }

    @Override
    public void getMessageSuccess(MessageBean messageBean) {
        hideProgress();
        if (messageBean.getSystemnotice() != null) {//系统消息
            MessageBean.SystemnoticeBean systemnotice = messageBean.getSystemnotice();
            messageTimeTv.setText(systemnotice.getCreateTime());
            messageContentTv.setText(systemnotice.getContent());
            if (messageBean.isSystemn()) {//未读
                messageIsOpenIv.setVisibility(View.VISIBLE);
            } else {
                messageIsOpenIv.setVisibility(View.GONE);
            }

        } else {
            messageCl.setOnClickListener(null);
            messageContentTv.setText(getResources().getString(R.string.system_message_no));
            messageTimeTv.setVisibility(View.INVISIBLE);
        }

        if (messageBean.getPublicnotice() != null) {//系统公告
            MessageBean.PublicnoticeBean publicnoticeBean = messageBean.getPublicnotice();
            noticeTimeTv.setText(publicnoticeBean.getCreateTime());
            noticeContentTv.setText(publicnoticeBean.getContent());
            if (messageBean.isPublicn()) {//未读
                noticeIsOpenIv.setVisibility(View.VISIBLE);
            } else {
                noticeIsOpenIv.setVisibility(View.GONE);
            }

        } else {
            noticeCl.setOnClickListener(null);
            noticeContentTv.setText(getResources().getString(R.string.system_message_no));
            noticeTimeTv.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void getMessageFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void onSuccess() {
//        hideProgress();
        presenter.getMessage(this);
    }

    @Override
    public void onFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }
}
