package com.taiyi.soul.moudles.main.home.mall.goods.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.home.mall.bean.MallMoreBean;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class MallFragmentAdapter  extends BaseRecyclerAdapter<MallMoreBean.ComlistBean> {

    private PullRecyclerView pullRecyclerView;
    private Context context;
    private MallItemClick mItemClick;

    //期权列表item点击事件
    public interface MallItemClick {
        void onItemClick(int position);
    }

    public MallFragmentAdapter(Context context,MallItemClick mItemClick) {
        this.context = context;
        this.mItemClick = mItemClick;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_mall_more;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final MallMoreBean.ComlistBean data) {
        ImageView mall_goods_img = getView(holder,R.id.mall_goods_img);
        TextView mall_goods_name = getView(holder,R.id.mall_goods_name);
        TextView mall_goods_price = getView(holder,R.id.mall_goods_price);
        RelativeLayout add_car = getView(holder,R.id.add_car);


        Glide.with(context).load(data.smallimgurl).apply(RequestOptions.bitmapTransform(new RoundedCorners(35)).override(300, 300)).into(mall_goods_img);
        mall_goods_name.setText(data.name);
        mall_goods_price.setText(data.hyprice+context.getResources().getString(R.string.USDN));
        add_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mItemClick.onItemClick(position);
            }
        });
    }


    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }

}

