package com.taiyi.soul.moudles.mine.setup.currencydisplay;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.mine.bean.CurrencyListBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class CurrencyDisplayPresent extends BasePresent<CurrencyDisplayView> {

    public void getCurrencyList(){
        HttpUtils.getRequets(BaseUrl.GET_CURRENCY_PRICE_LIST, this, null, new JsonCallback<BaseResultBean<List<CurrencyListBean>>>() {
            @Override
            public BaseResultBean<List<CurrencyListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<CurrencyListBean>>> response) {
                super.onSuccess(response);
                if (null!=view) {
                if (response.body().status==200){
                    view.getCurrencyListSuccess(response.body().data);
                }else {
                    view.getCurrencyListFailure(response.body().msg);
                }
            }}

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<CurrencyListBean>>> response) {
                super.onError(response);
            }
        });
    }

    public void switchCurrency(String id){
        Map<String,String>map=new HashMap<>();
        map.put("curId",id);
        HttpUtils.getRequets(BaseUrl.SWITCH_CURRENCY, this, map, new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.switchSuccess();
                    } else {
                        view.switchFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

}
