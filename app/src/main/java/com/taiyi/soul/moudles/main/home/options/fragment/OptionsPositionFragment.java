package com.taiyi.soul.moudles.main.home.options.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.RedirectsBean;
import com.taiyi.soul.moudles.main.home.options.adapter.OptionsDetailsFragmentAdapter;
import com.taiyi.soul.moudles.main.home.options.bean.KLainBean;
import com.taiyi.soul.moudles.main.home.options.bean.KOtherBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionDetailsListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionPopupBean;
import com.taiyi.soul.moudles.main.home.options.presenter.OptionsDetailPresent;
import com.taiyi.soul.moudles.main.home.options.presenter.OptionsDetailView;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\6\2 0002 13:55
 * @Author : yuan
 * @Describe ：klain-持仓明细
 */
public class OptionsPositionFragment extends BaseFragment<OptionsDetailView, OptionsDetailPresent> implements OptionsDetailView, View.OnClickListener, PullRecyclerView.PullLoadMoreListener {


    @BindView(R.id.recycle)
    PullRecyclerView recycle;

    private boolean node_hidden;
    private OptionsDetailsFragmentAdapter mFragmentAdapter;

     private String id;
     private String price = "0";
    private List<OptionDetailsListBean.ComlistBean> mComlist;
    private Timer mTimer;

    public OptionsPositionFragment(String id) {
        this.id = id;
    }

  public  void optionsGetPrice(String price) {
        this.price = price;
        if(null!=mComlist){
            for (int i = 0; i < mComlist.size(); i++) {
                mComlist.get(i).price_now = price;
            }
            if(null!=mFragmentAdapter){
                mFragmentAdapter.notifyDataSetChanged();
            }
        }else {
            presenter.getData(getActivity(),pageNo+"","0",id);
        }
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_options;
    }


    @Override
    public OptionsDetailPresent initPresenter() {
        return new OptionsDetailPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if(!EventBus.getDefault().isRegistered(true))
        EventBus.getDefault().post(this);
    }




    public void reData(String id){
      presenter.getData(getActivity(),1+"","0",id);
    }
    Handler handler = new Handler();
    private TimerTask timerTask = new TimerTask() {
        public void run() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < mFragmentAdapter.getDatas().size(); i++) {
                        mFragmentAdapter.getDatas().get(i).spendtime++;
                        if(mFragmentAdapter.getDatas().get(i).spendtime==mFragmentAdapter.getDatas().get(i).time){
                            mFragmentAdapter.getDatas().remove(i);

                        }
                    }
                    mFragmentAdapter.notifyDataSetChanged();
                }
            });

        }
    };

    @Override
    protected void initData() {


        presenter.getData(getActivity(),pageNo+"","0",id);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 1);
        recycle.setLayoutManager(manager);
        //实例化PullRecyclerView相关信息
        recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        recycle.setIsLoadMoreEnabled(true);
        recycle.setOnPullLoadMoreListener(this);
        recycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new OptionsDetailsFragmentAdapter(getContext());
        mFragmentAdapter.setPullRecyclerView(recycle);
        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setAdapter(mFragmentAdapter);
        recycle.refreshWithPull();
        recycle.setPullLoadMoreCompleted();
    }


    @Override
    public void initEvent() {


    }


    @Override
    public void onResume() {
        super.onResume();
            //TODO 刷新数据
            presenter.getData(getActivity(),pageNo+"","0",id);
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        node_hidden = hidden;

        if (!hidden) {
            //TODO 刷新数据
        }else {
            mTimer.cancel();
        }
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        }

    }

    /************************************Call Interface callback****************************************************/
    private int pageNo = 1;
    int start = 0;
    //获取列表
    @Override
    public void getDataSuccess(int status, String msg, OptionDetailsListBean bean) {
        if (status == 0) {
            if (pageNo == 1) {
//                Log.e("pppp===",pageNo+"");
                if(null!=mComlist){
                    mComlist.clear();
                    mFragmentAdapter.clearDatas();
                }

                for (int i = 0; i <  bean.comlist.size(); i++) {
                    bean.comlist.get(i).price_now = price;
                }
                mComlist = bean.comlist;
                mFragmentAdapter.setDatas(bean.comlist);
            } else {
//                Log.e("aaaa===",pageNo+"");
                for (int i = 0; i < bean.comlist.size(); i++) {
                    bean.comlist.get(i).price_now = price;
                }
                mComlist.addAll(bean.comlist);
                mFragmentAdapter.addDatas(bean.comlist);
            }
            if(mComlist.size()>0){
                if(start==0){
                    start=1;
                    mTimer = new Timer();
//                    //启动Timer定时器，并在每1s 更新一次进度
                    mTimer.schedule(timerTask, 1000, 1000);
                }
            }


        } else {
            AlertDialogShowUtil.toastMessage(getContext(), msg);
        }
        recycle.setPullLoadMoreCompleted();
    }

    @Override
    public void getPopupSuccess(int status, String msg, OptionPopupBean bean) {

    }

    @Override
    public void getPopupBuySuccess(int status, String msg) {

    }

    @Override
    public void getCurrencyBalanceSuccess(int code, String msg_cn, String balance) {

    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {

    }

    @Override
    public void onIssueFeeSuccess(int code, String msg_cn, IssueFeeBean bean) {

    }

    @Override
    public void getKLineSuccess(int code, String msg_cn, KLainBean bean) {

    }

    @Override
    public void getKOtherSuccess(int code, String msg_cn, KOtherBean bean) {

    }

    @Override
    public void getPictureDataSuccess(int code, String msg_cn, List<OptionListBean> bean) {

    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg_cn, String bean) {

    }

    @Override
    public void getRedirectDataSuccess(int code, String msg_cn, RedirectsBean bean) {

    }

    @Override
    public void onFailure() {

    }

    /************************************refresh or load more****************************************************/
    @Override
    public void onRefresh() {
        pageNo=1;
        presenter.getData(getActivity(),pageNo+"","0",id);
    }

    @Override
    public void onLoadMore() {
         pageNo++;
        presenter.getData(getActivity(),pageNo+"","0",id);

    }
}
