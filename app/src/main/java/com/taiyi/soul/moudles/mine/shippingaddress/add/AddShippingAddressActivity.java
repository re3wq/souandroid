package com.taiyi.soul.moudles.mine.shippingaddress.add;

import android.app.Dialog;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.assets.view.CurrencyPopupWindow;
import com.taiyi.soul.moudles.mine.bean.AddressInfoBean;
import com.taiyi.soul.moudles.mine.bean.AreaBean;
import com.taiyi.soul.utils.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AddShippingAddressActivity extends BaseActivity<AddShippingAddressView, AddShippingAddressPresent> implements AddShippingAddressView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.rightTitleTv)
    TextView rightTitleTv;
    @BindView(R.id.nameEt)
    EditText nameEt;
    @BindView(R.id.surnameEt)
    EditText surnameEt;
    @BindView(R.id.phoneNumberEt)
    EditText phoneNumberEt;
    @BindView(R.id.streetAddressEt)
    EditText streetAddressEt;
    @BindView(R.id.countryTv)
    TextView countryTv;
    @BindView(R.id.countryIv)
    ImageView countryIv;
    @BindView(R.id.stateTv)
    TextView stateTv;
    @BindView(R.id.stateIv)
    ImageView stateIv;
    @BindView(R.id.cityTv)
    TextView cityTv;
    @BindView(R.id.cityIv)
    ImageView cityIv;
    @BindView(R.id.defaultIv)
    ImageView defaultIv;
    @BindView(R.id.top)
    ConstraintLayout top;
    private CurrencyPopupWindow currencyPopupWindow, countryPopupWindow, cityPopupWindow;
    private RecyclerView countryRecyclerView, popupWindowRecyclerView, cityRecyclerView;
    private List<String> list = new ArrayList<>();
    private List<AreaBean> countryList = new ArrayList<>();
    private List<AreaBean> stateList = new ArrayList<>();
    private List<AreaBean> cityList = new ArrayList<>();
    int type = 0;
    private String countryId, provinceId, cityId;
    private String isDefault = "1";
    private String addressId;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_shipping_address;
    }

    @Override
    public AddShippingAddressPresent initPresenter() {
        return new AddShippingAddressPresent();
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {


        if (getIntent().getExtras() != null) {
            titleTv.setText(getString(R.string.edit_shipping_address));
            addressId = getIntent().getExtras().getString("addressId");
            rightTitleTv.setText(getString(R.string.delete));
            showProgress();
            presenter.getAddressInfo(this, addressId);
        } else {
            titleTv.setText(getString(R.string.add_shipping_address));
        }

        View view = LayoutInflater.from(this).inflate(R.layout.address_province_pop_layout, null);
        View view1 = LayoutInflater.from(this).inflate(R.layout.address_country_pop_layout, null);
        View view2 = LayoutInflater.from(this).inflate(R.layout.address_province_pop_layout, null);

        countryPopupWindow = new CurrencyPopupWindow(this, view1);
        countryPopupWindow.setWidth((getResources().getDisplayMetrics().widthPixels - DensityUtil.dip2px(this, 30)));
        countryPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        currencyPopupWindow = new CurrencyPopupWindow(this, view);
        currencyPopupWindow.setWidth((getResources().getDisplayMetrics().widthPixels - DensityUtil.dip2px(this, 30)) / 2);
        currencyPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//
        cityPopupWindow = new CurrencyPopupWindow(this, view2);
        cityPopupWindow.setWidth((getResources().getDisplayMetrics().widthPixels - DensityUtil.dip2px(this, 30)) / 2);
        cityPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        countryRecyclerView = view1.findViewById(R.id.recyclerView);
        popupWindowRecyclerView = view.findViewById(R.id.recyclerView);
        cityRecyclerView = view2.findViewById(R.id.recyclerView);

        countryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        popupWindowRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        cityRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        CommonAdapter popupListAdapter = AdapterManger.getPopupListAdapter(this, countryList);//国家
        countryRecyclerView.setAdapter(popupListAdapter);
        popupListAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                type = 1;//根据国家ID获取省列表
                countryTv.setText(countryList.get(position).getArea_name());
                stateTv.setText("州");
                cityTv.setText("市");
                countryId = countryList.get(position).getArea_id();
                presenter.getList(countryId);
//                if (TextUtils.isEmpty(addressId)) {
//                    presenter.getList(countryId,0);
//                }else {
//                    presenter.getList(countryId,1);
//                }
                countryPopupWindow.dismiss();
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });

        CommonAdapter stateListAdapter = AdapterManger.getPopupListAdapter(this, stateList);//省
        popupWindowRecyclerView.setAdapter(stateListAdapter);
        stateListAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                type = 2;//根据省ID获取市列表
                stateTv.setText(stateList.get(position).getArea_name());
                cityTv.setText("市");
                provinceId = stateList.get(position).getArea_id();
                presenter.getList(provinceId);
//                if (TextUtils.isEmpty(addressId)) {
//                    presenter.getList(provinceId,0);
//                }else {
//                    presenter.getList(provinceId,1);
//                }
                currencyPopupWindow.dismiss();
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });

        CommonAdapter cityListAdapter = AdapterManger.getPopupListAdapter(this, cityList);//市
        cityRecyclerView.setAdapter(cityListAdapter);
        cityListAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                cityTv.setText(cityList.get(position).getArea_name());
                cityId = cityList.get(position).getArea_id();
                cityPopupWindow.dismiss();
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });


    }

    @Override
    protected void initData() {
        presenter.getList("1");
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.rightTitleTv, R.id.countryRl, R.id.stateTv, R.id.cityTv, R.id.defaultIv, R.id.confirmTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightTitleTv:
                Dialog dialog = new Dialog(AddShippingAddressActivity.this, R.style.MyDialog);
                View inflate = LayoutInflater.from(AddShippingAddressActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                TextView hint_text = inflate.findViewById(R.id.hint_text);
                hint_text.setText(getResources().getString(R.string.delete_address));
                dialog.setContentView(inflate);
                inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        showProgress();
                        presenter.deleteAddress(AddShippingAddressActivity.this, addressId);
                    }
                });
                dialog.show();

                break;
            case R.id.countryRl:
                countryPopupWindow.showAsDropDown(view, 0, DensityUtil.dip2px(this, -56));
                break;
            case R.id.stateTv:
                currencyPopupWindow.showAsDropDown(view, 0, DensityUtil.dip2px(this, -74));
                break;
            case R.id.cityTv:
                cityPopupWindow.showAsDropDown(view, 0, DensityUtil.dip2px(this, -74));
                break;
            case R.id.defaultIv:
                if (isDefault.equals("1")) {
                    isDefault = "0";
                    defaultIv.setImageResource(R.mipmap.mine_safety_center_close_iv);
                } else {
                    isDefault = "1";
                    defaultIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
                }
                break;
            case R.id.confirmTv:
                String name = nameEt.getText().toString();
                String surname = surnameEt.getText().toString();
                String phoneNumber = phoneNumberEt.getText().toString();
                String streetAddress = streetAddressEt.getText().toString();
                if (TextUtils.isEmpty(name)) {
                    toast(getString(R.string.edit_last_name));
                    return;
                }
                if (TextUtils.isEmpty(surname)) {
                    toast(getString(R.string.edit_first_name));
                    return;
                }
                if (TextUtils.isEmpty(phoneNumber)) {
                    toast(getString(R.string.phone_input));
                    return;
                }
                if (TextUtils.isEmpty(streetAddress)) {
                    toast(getString(R.string.edit_address));
                    return;
                }
                if (streetAddress.toLowerCase().contains("script")) {
                    toast(getString(R.string.edit_address_illegal));
                    return;
                }

                showProgress();
                if (TextUtils.isEmpty(addressId)) {
                    presenter.addAddress(this, name.trim(), surname.trim(), phoneNumber, countryId, provinceId, cityId, streetAddress, isDefault);
                } else {
                    presenter.editAddress(this, name.trim(), surname.trim(), phoneNumber, countryId, provinceId, cityId, streetAddress, isDefault, addressId);
                }
                break;
        }
    }

    @Override
    public void onListSuccess(List<AreaBean> list) {
        if (type == 0) {
            countryList.clear();
            countryList.addAll(list);
            countryRecyclerView.getAdapter().notifyDataSetChanged();
        } else if (type == 1) {
            stateList.clear();
            stateList.addAll(list);
            popupWindowRecyclerView.getAdapter().notifyDataSetChanged();

            if (!TextUtils.isEmpty(addressId)) {
                type = 2;
                presenter.getList(provinceId);
            }
        } else {
            cityList.clear();
            cityList.addAll(list);
            cityRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onListFailure(String errorMsg) {

    }

    @Override
    public void addSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void addFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getAddressInfoSuccess(AddressInfoBean addressInfoBean) {
        hideProgress();
        phoneNumberEt.setText(addressInfoBean.getTel());
        streetAddressEt.setText(addressInfoBean.getAddress());
        nameEt.setText(addressInfoBean.getName());
        surnameEt.setText(addressInfoBean.getSurname());
        countryTv.setText(addressInfoBean.getCountryName());
        stateTv.setText(addressInfoBean.getProvincename());
        cityTv.setText(addressInfoBean.getCityName());
        isDefault = addressInfoBean.getType();
        countryId = addressInfoBean.getCountryid();
        provinceId = addressInfoBean.getProvinceid();
        cityId = addressInfoBean.getCityid();
        if ("0".equals(isDefault)) {//非默认
            defaultIv.setImageResource(R.mipmap.mine_safety_center_close_iv);
        }
        type = 1;
        //获取省份列表
        presenter.getList(countryId);
    }

    @Override
    public void getAddressInfoFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void deleteSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void deleteFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }
}
