package com.taiyi.soul.moudles.mine.setup.currencydisplay;

import com.taiyi.soul.moudles.mine.bean.CurrencyListBean;

import java.util.List;

public interface CurrencyDisplayView {
    void getCurrencyListSuccess(List<CurrencyListBean>currencyListBeanList);
    void getCurrencyListFailure(String errorMsg);
    void switchSuccess();
    void switchFailure(String errorMsg);
}
