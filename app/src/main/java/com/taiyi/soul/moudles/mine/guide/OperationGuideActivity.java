package com.taiyi.soul.moudles.mine.guide;

import android.os.Bundle;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

public class OperationGuideActivity extends BaseActivity<NormalView, NormalPresenter> {



    @Override
    protected int getLayoutId() {
        return R.layout.activity_operation_guide;
    }

    @Override
    public NormalPresenter initPresenter() {
        return null;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }
}
