package com.taiyi.soul.moudles.mine.accountmanagement.create;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateAccountStep3Activity extends BaseActivity<NormalView, NormalPresenter> {

    @BindView(R.id.titleTv)
    TextView titleTv;

    @BindView(R.id.cl_title)
    ConstraintLayout cl_title;

    @BindView(R.id.tv_type)
    TextView tv_type;

    @BindView(R.id.determineTv)
    TextView determineTv;

    private String from = "";


    @Override
    protected int getLayoutId() {
        return R.layout.activity_create_account_step3;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        from = getIntent().getStringExtra("from");

        if (from != null && from.equals("register")) {
            titleTv.setText(getString(R.string.account_register));
            tv_type.setText(getString(R.string.register));
            determineTv.setText(getString(R.string.go_login));

        } else {
            titleTv.setText(getString(R.string.create_account));
            //TODO lxq
            titleTv.setVisibility(View.GONE);
            tv_type.setText(getString(R.string.create));
            determineTv.setText(getString(R.string.determine));
        }

    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.determineTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
            case R.id.determineTv:
                if (from != null && from.equals("register")) {
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(this, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                } else {
                    finish();
                }
                break;
        }
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(cl_title).init();
    }
}
