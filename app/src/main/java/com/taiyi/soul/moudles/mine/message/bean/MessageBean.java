package com.taiyi.soul.moudles.mine.message.bean;

public class MessageBean {

    /**
     * systemnotice : {"id":3,"userId":15941762687836420,"url":"9","createTime":"2020-07-09 21:21:34","type":1,"isRead":1,"title":"9","abstrac":"9","content":"9","langu":"zh"}
     * publicnotice : {"id":4,"userId":15941762687836420,"url":"8","createTime":"2020-07-09 21:21:49","type":2,"isRead":1,"title":"8","abstrac":"8","content":"8","langu":"zh"}
     */

    private SystemnoticeBean systemnotice;
    private PublicnoticeBean publicnotice;
    private boolean systemn;
    private boolean publicn;

    public boolean isSystemn() {
        return systemn;
    }

    public void setSystemn(boolean systemn) {
        this.systemn = systemn;
    }

    public boolean isPublicn() {
        return publicn;
    }

    public void setPublicn(boolean publicn) {
        this.publicn = publicn;
    }

    public SystemnoticeBean getSystemnotice() {
        return systemnotice;
    }

    public void setSystemnotice(SystemnoticeBean systemnotice) {
        this.systemnotice = systemnotice;
    }

    public PublicnoticeBean getPublicnotice() {
        return publicnotice;
    }

    public void setPublicnotice(PublicnoticeBean publicnotice) {
        this.publicnotice = publicnotice;
    }

    public static class SystemnoticeBean {
        /**
         * id : 3
         * userId : 15941762687836420
         * url : 9
         * createTime : 2020-07-09 21:21:34
         * type : 1
         * isRead : 1
         * title : 9
         * abstrac : 9
         * content : 9
         * langu : zh
         */

        private long id;
        private long userId;
        private String url;
        private String createTime;
        private int type;
        private int isRead;
        private String title;
        private String abstrac;
        private String content;
        private String langu;

        public long getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAbstrac() {
            return abstrac;
        }

        public void setAbstrac(String abstrac) {
            this.abstrac = abstrac;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getLangu() {
            return langu;
        }

        public void setLangu(String langu) {
            this.langu = langu;
        }
    }

    public static class PublicnoticeBean {
        /**
         * id : 4
         * userId : 15941762687836420
         * url : 8
         * createTime : 2020-07-09 21:21:49
         * type : 2
         * isRead : 1
         * title : 8
         * abstrac : 8
         * content : 8
         * langu : zh
         */

        private long id;
        private long userId;
        private String url;
        private String createTime;
        private int type;
        private int isRead;
        private String title;
        private String abstrac;
        private String content;
        private String langu;

        public long getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAbstrac() {
            return abstrac;
        }

        public void setAbstrac(String abstrac) {
            this.abstrac = abstrac;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getLangu() {
            return langu;
        }

        public void setLangu(String langu) {
            this.langu = langu;
        }
    }
}
