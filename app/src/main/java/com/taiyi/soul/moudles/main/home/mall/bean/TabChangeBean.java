package com.taiyi.soul.moudles.main.home.mall.bean;

public class TabChangeBean {

    public int tab_position;
    public int sort;

    public TabChangeBean(int tab_position, int sort) {
        this.tab_position = tab_position;
        this.sort = sort;
    }
}
