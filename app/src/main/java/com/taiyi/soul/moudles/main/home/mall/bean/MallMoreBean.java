package com.taiyi.soul.moudles.main.home.mall.bean;

import java.util.List;

public class MallMoreBean {


        public int totalpage;
        public int rowcount;
        public String pagenum;
        public List<ComlistBean> comlist;

        public static class ComlistBean {
            public String flag;
            public String commodityid;
            public int salenum;
            public String type;
            public String brandnamezz;
            public String brandname;
            public String commodityspecid;
            public int zhuanstock;
            public String zhuanprice;
            public int zhuanyunfei;
            public int visitgoodsnum;
            public String price;
            public String businessuserid;
            public int brandid;
            public String sanfurl;
            public String logo;
            public String lableidname;
            public String cashPrice;
            public int stock;
            public String delflag;
            public String hybvprice;
            public String classificationidname;
            public String recommendflag;
            public int stockzong;
            public String createtime;
            public String introimg;
            public String sanfprice;
            public String bvprice;
            public String sanftype;
            public String youxiaoday;
            public String comimgurl;
            public String detailimgurl;
            public String videourl;
            public String name;
            public String businessname;
            public String qiangurl;
            public String hyprice;
            public String categoryname;
            public String combz;
            public String smallimgurl;

    }
}
