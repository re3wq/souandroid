package com.taiyi.soul.moudles.main.assets.distribution.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\5 0005 11:10
 * @Author : yuan
 * @Describe ：
 */
public class PieBean {

    public List<PieDataBean> beanList;



    public static class PieDataBean {
        public float percentages;
        public Integer colors;
        public String unit;

        public PieDataBean(Float percentages, Integer colors, String unit) {
            this.percentages = percentages;
            this.colors = colors;
            this.unit = unit;
        }
    }


}
