package com.taiyi.soul.moudles.main.assets.exchange;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.liaoinstan.springview.R;
import com.liaoinstan.springview.container.BaseHeader;

import androidx.core.content.ContextCompat;

/**
 * Created by Administrator on 2016/3/21.
 */
public class MyHeader extends BaseHeader {
    private Context context;
    private int rotationSrc;
    private int arrowSrc;

    private long freshTime;

    private final int ROTATE_ANIM_DURATION = 180;
    private RotateAnimation mRotateUpAnim;
    private RotateAnimation mRotateDownAnim;

    private TextView headerTitle;
    private TextView headerTime;
    private ImageView headerArrow;
    private ProgressBar headerProgressbar;
//    private TextView default_header_time1;
    private String bottom_refresh;
    private String open_refresh;
    private String refreshing1;
    private String just_refresh;
    private String minutes_refresh;
    private String hour_refresh;
    private String day_refresh;
    private String last_refresh;

    public MyHeader(Context context, String bottom_refresh, String open_refresh, String refreshing1, String just_refresh
            , String minutes_refresh, String hour_refresh, String day_refresh, String last_refresh) {
        this(context, R.drawable.progress_small, R.drawable.arrow);
        this.bottom_refresh = bottom_refresh;
        this.open_refresh = open_refresh;
        this.refreshing1 = refreshing1;
        this.just_refresh = just_refresh;
        this.minutes_refresh = minutes_refresh;
        this.hour_refresh = hour_refresh;
        this.day_refresh = day_refresh;
        this.last_refresh = last_refresh;
    }

    public MyHeader(Context context, int rotationSrc, int arrowSrc) {
        this.context = context;
        this.rotationSrc = rotationSrc;
        this.arrowSrc = arrowSrc;

        mRotateUpAnim = new RotateAnimation(0.0f, -180.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        mRotateUpAnim.setDuration(ROTATE_ANIM_DURATION);
        mRotateUpAnim.setFillAfter(true);
        mRotateDownAnim = new RotateAnimation(-180.0f, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        mRotateDownAnim.setDuration(ROTATE_ANIM_DURATION);
        mRotateDownAnim.setFillAfter(true);
    }

    @Override
    public View getView(LayoutInflater inflater, ViewGroup viewGroup) {
        View view = inflater.inflate(R.layout.default_header, viewGroup, true);
        headerTitle = (TextView) view.findViewById(R.id.default_header_title);
        headerTime = (TextView) view.findViewById(R.id.default_header_time);
//        default_header_time1 = (TextView) view.findViewById(R.id.default_header_time1);
        headerArrow = (ImageView) view.findViewById(R.id.default_header_arrow);
        headerProgressbar = (ProgressBar) view.findViewById(R.id.default_header_progressbar);
        headerProgressbar.setIndeterminateDrawable(ContextCompat.getDrawable(context, rotationSrc));
        headerArrow.setImageResource(arrowSrc);
        headerProgressbar.setVisibility(View.INVISIBLE);
        return view;
    }

    @Override
    public void onPreDrag(View rootView) {
        if (freshTime == 0) {
            freshTime = System.currentTimeMillis();
        } else {
            int m = (int) ((System.currentTimeMillis() - freshTime) / 1000 / 60);
            if (m >= 1 && m < 60) {
                headerTime.setText(m + minutes_refresh);
            } else if (m >= 60) {
                int h = m / 60;
                headerTime.setText(h + hour_refresh);
            } else if (m > 60 * 24) {
                int d = m / (60 * 24);
                headerTime.setText(d + day_refresh);
            } else if (m == 0) {
//                default_header_time1.setText(last_refresh);
                headerTime.setText(just_refresh);
            }
        }
    }

    @Override
    public void onDropAnim(View rootView, int dy) {
    }

    @Override
    public void onLimitDes(View rootView, boolean upORdown) {
        if (!upORdown) {
            headerTitle.setText(open_refresh);
            if (headerArrow.getVisibility() == View.VISIBLE)
                headerArrow.startAnimation(mRotateUpAnim);
        } else {
            headerTitle.setText(bottom_refresh);
            if (headerArrow.getVisibility() == View.VISIBLE)
                headerArrow.startAnimation(mRotateDownAnim);
        }
    }

    @Override
    public void onStartAnim() {
        freshTime = System.currentTimeMillis();
        headerTitle.setText(refreshing1);
        headerArrow.setVisibility(View.INVISIBLE);
        headerArrow.clearAnimation();
        headerProgressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFinishAnim() {
        headerArrow.setVisibility(View.VISIBLE);
        headerProgressbar.setVisibility(View.INVISIBLE);
    }
}