package com.taiyi.soul.moudles.main.assets.exchange.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\10 0010 16:44
 * @Author : yuan
 * @Describe ：兑换记录
 */
public class ExchangeRecordBean {

        public int totalpage;
        public int rowcount;
        public List<ComlistBean> comlist;

        public static class ComlistBean {
            public String to_coin_name;// 上 前币种名固定为1+to_coin_name
            public String createtime;//时间
            public String charge;
            public String orderid;
            public String num;
            public String from_coin_id;
            public String pay_money;// 下 前币种名固定为pay_money+to_coin_name
            public String type;
            public String maybe_money;//下 后币种 maybe_money+from_coin_name
            public String userid;
            public String from_coin_name;
            public String money;
            public String to_coin_id;
            public String price;//上 后币种 price+from_coin_name
            public int id;
            public String from_url;
            public String to_url;
        }

}
