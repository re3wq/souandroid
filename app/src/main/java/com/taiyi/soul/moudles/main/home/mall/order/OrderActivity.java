package com.taiyi.soul.moudles.main.home.mall.order;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.order.fragment.OrderDeliverFragment;
import com.taiyi.soul.moudles.main.home.mall.order.fragment.OrderFinishFragment;
import com.taiyi.soul.moudles.main.home.mall.order.fragment.OrderPaymentFragment;
import com.taiyi.soul.moudles.main.home.mall.order.fragment.OrderReceivedFragment;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class OrderActivity extends BaseActivity<NormalView, NormalPresenter> {


    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tabLayout)
    SlidingTabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    private String[] mTitles;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_order;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mTvTitle.setText(getString(R.string.order_title));
        int type = getIntent().getExtras().getInt("type",0);
        mTitles = new String[]{this.getResources().getString(R.string.order_pending_payment),
                this.getResources().getString(R.string.order_to_be_delivered),
                this.getResources().getString(R.string.order_to_be_received),
                this.getResources().getString(R.string.order_to_be_finish)
        };

        mFragments.add(new OrderPaymentFragment());//待付款
        mFragments.add(new OrderDeliverFragment());//待发货
        mFragments.add(new OrderReceivedFragment());//待收货
        mFragments.add(new OrderFinishFragment());//已完成


        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mTabLayout.post(new Runnable() {
            @Override
            public void run() {
                int mTabWidth = mTabLayout.getWidth();
                BigDecimal b1 = new BigDecimal(Double.toString(mTabWidth));
                BigDecimal b2 = new BigDecimal(Double.toString(4));
                int width = b1.divide(b2, 0, BigDecimal.ROUND_HALF_UP).intValue();
//                mTabLayout.setTextSelectBg(R.mipmap.node_hash_market_sel_iv, width);
//                mTabLayout.setTextUnselectBg(R.mipmap.node_hash_market_unsel_iv);
                mTabLayout.setIsBackGround(true);
                Bitmap bitmap = BitmapFactory.decodeResource(OrderActivity.this.getResources(), R.mipmap.node_hash_market_sel_iv);
                mTabLayout.setDrawBitMap(bitmap, 126);
                mTabLayout.setViewPager(mViewPager);
//                Log.e("type=====",type+"");
                mViewPager.setCurrentItem(type);
            }
        });

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
