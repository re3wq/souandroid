package com.taiyi.soul.moudles.main.assets.bean;

import java.io.Serializable;

public class CurrencyListBean implements Serializable {

    /**
     * coinid : 16
     * coinName : NGK
     * convert : £0.0000
     * url : http://114.115.131.170:8888/group1/M00/00/03/wKgAp18VaPmAEmlYAAAghxWeqxE899.png
     * datetime : null
     * moneyandfor : 0.0000
     * mfcon : £0.0000
     * money : 0.0000
     */

    private String coinid;
    private String coinName;
    private String convert;//可用
    private String url;
    private Object datetime;
    private String moneyandfor;
    private String mfcon;
    private String money;
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCoinid() {
        return coinid;
    }

    public void setCoinid(String coinid) {
        this.coinid = coinid;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public String getConvert() {
        return convert;
    }

    public void setConvert(String convert) {
        this.convert = convert;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getDatetime() {
        return datetime;
    }

    public void setDatetime(Object datetime) {
        this.datetime = datetime;
    }

    public String getMoneyandfor() {
        return moneyandfor;
    }

    public void setMoneyandfor(String moneyandfor) {
        this.moneyandfor = moneyandfor;
    }

    public String getMfcon() {
        return mfcon;
    }

    public void setMfcon(String mfcon) {
        this.mfcon = mfcon;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
