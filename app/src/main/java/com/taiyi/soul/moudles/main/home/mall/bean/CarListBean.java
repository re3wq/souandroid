package com.taiyi.soul.moudles.main.home.mall.bean;

import java.util.List;

public class CarListBean {
        public int totalpage;
        public int rowcount;
        public List<CartListBean> cartList;
        public static class CartListBean {
            public String flag;
            public String commodityid;
            public int salenum;
            public String type;
            public String commodityspecid;
            public String userid;
            public int classificationid;
            public int number;
            public String imgurl;
            public String price;
            public int visitgoodsnum;
            public String businessuserid;
            public String cashPrice;
            public int stock;
            public String hybvprice;
            public String delflag;
            public String recommendflag;
            public String createtime;
            public String yunfei;
            public String bvprice;
            public String shopcarid;
            public String name;
            public String hyprice;
            public String typename;
            public String smallimgurl;
            public boolean isChecked;
        }

}
