package com.taiyi.soul.moudles.main;

import com.taiyi.soul.moudles.mine.bean.VersionUpdateBean;

public interface MainView {
    void checkVersionUpdateSuccess(VersionUpdateBean versionUpdateBean);
    void checkVersionUpdateFailure(int status, String errorMsg);
}
