package com.taiyi.soul.moudles.mine.editinformation;

public interface EditInformationView {
    void getSuccess(int status, String msg, EditInformationBean userInfoBean);
    void getChangeInfoSuccess(int status, String msg, String errorMsg);

}
