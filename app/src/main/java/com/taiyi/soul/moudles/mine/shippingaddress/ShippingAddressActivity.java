package com.taiyi.soul.moudles.mine.shippingaddress;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.container.DefaultHeader;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.mine.bean.AddressListBean;
import com.taiyi.soul.moudles.mine.shippingaddress.add.AddShippingAddressActivity;


import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ShippingAddressActivity extends BaseActivity<ShippingAddressView, ShippingAddressPresent> implements ShippingAddressView, SpringView.OnFreshListener {
    @BindView(R.id.top)
    ConstraintLayout top;
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.noDataLl)
    LinearLayout noDataLl;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.springView)
    SpringView springView;
    private int pageNo = 1;
    List<AddressListBean.AddresslistBean> addressList = new LinkedList<>();


    @Override
    protected int getLayoutId() {
        return R.layout.activity_shipping_address;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).statusBarDarkFont(false, 0.0f).init();
    }

    @Override
    public ShippingAddressPresent initPresenter() {
        return new ShippingAddressPresent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgress();
        presenter.getAddressList(this,pageNo);
    }

    @Override
    public void initViews(Bundle savedInstanceState) {

        int type = getIntent().getIntExtra("type", 0);
        titleTv.setText(getString(R.string.shipping_address));
        springView.setFooter(new DefaultFooter(this));
        springView.setHeader(new DefaultHeader(this));
        springView.setGive(SpringView.Give.BOTH);
        springView.setType(SpringView.Type.FOLLOW);
        springView.setListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        CommonAdapter shippingAddressListAdapter = AdapterManger.getShippingAddressListAdapter(this, this, addressList,type);
        recyclerView.setAdapter(shippingAddressListAdapter);
    }


    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.newShippingAddressTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.newShippingAddressTv:
                ActivityUtils.next(this, AddShippingAddressActivity.class);
                break;
        }
    }


    @Override
    public void onSuccess(AddressListBean addressListBean) {
        hideProgress();
        springView.onFinishFreshAndLoad();
        List<AddressListBean.AddresslistBean> addresslist = addressListBean.getAddresslist();
        if (null != addresslist) {
            if (pageNo == 1) {
                addressList.clear();
                if (addresslist.size() == 0) {
                    noDataLl.setVisibility(View.VISIBLE);
                } else {
                    noDataLl.setVisibility(View.GONE);
                }
            }
            addressList.addAll(addresslist);
            recyclerView.getAdapter().notifyDataSetChanged();
        } else {
            if (pageNo == 1) {
                noDataLl.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onFailure(String errorMsg) {
        hideProgress();
        springView.onFinishFreshAndLoad();
        toast(errorMsg);
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        showProgress();
        presenter.getAddressList(this,pageNo);
    }

    @Override
    public void onLoadmore() {
        pageNo = pageNo + 1;
        showProgress();
        presenter.getAddressList(this,pageNo);
    }
}
