package com.taiyi.soul.moudles.mine.bean;

import androidx.annotation.IdRes;

public class DataBean {
    @IdRes
    public int resId;
    public String name;
    public boolean isSelect;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public DataBean(int resId, String name) {
        this.resId = resId;
        this.name = name;
    }

    public int getResId() {
        return resId;
    }

    public String getName() {
        return name;
    }
}
