package com.taiyi.soul.moudles.mine.shippingaddress;

import com.taiyi.soul.moudles.mine.bean.AddressListBean;

public interface ShippingAddressView {
    void onSuccess(AddressListBean addressListBean);
    void onFailure(String errorMsg);
}
