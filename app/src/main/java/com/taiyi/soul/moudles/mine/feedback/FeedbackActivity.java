package com.taiyi.soul.moudles.mine.feedback;

import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class FeedbackActivity extends BaseActivity<FeedbackView, FeedbackPresent>implements FeedbackView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.rightTitleTv)
    TextView rightTitleTv;
    @BindView(R.id.todaySendTv)
    TextView todaySendTv;
    @BindView(R.id.typeOneTv)
    TextView typeOneTv;
    @BindView(R.id.typeTwoTv)
    TextView typeTwoTv;
    @BindView(R.id.typeThreeTv)
    TextView typeThreeTv;
    @BindView(R.id.contentEt)
    EditText contentEt;
    @BindView(R.id.typeOneIv)
    ImageView typeOneIv;
    @BindView(R.id.typeTwoIv)
    ImageView typeTwoIv;
    @BindView(R.id.typeThreeIv)
    ImageView typeThreeIv;
    @BindView(R.id.llOne)
    LinearLayout llOne;
    @BindView(R.id.llTwo)
    LinearLayout llTwo;
    @BindView(R.id.llThree)
    LinearLayout llThree;
    @BindView(R.id.top)
    ConstraintLayout top;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_feedback;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }


    @Override
    public FeedbackPresent initPresenter() {
        return new FeedbackPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.feedback));
    }

    @Override
    protected void initData() {
        showProgress();
presenter.getFeedbackClass();
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.rightTitleTv,R.id.llOne,R.id.llTwo,R.id.llThree,R.id.commitTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightTitleTv:
                break;
            case R.id.llOne:
                llOne.setSelected(!llOne.isSelected());
                typeOneTv.setSelected(llOne.isSelected());
                if (llOne.isSelected()) {
                    typeOneIv.setVisibility(View.VISIBLE);
                    typeTwoTv.setSelected(false);
                    typeThreeTv.setSelected(false);
                    llTwo.setSelected(false);
                    llThree.setSelected(false);
                    typeTwoIv.setVisibility(View.GONE);
                    typeThreeIv.setVisibility(View.GONE);
                }else {
                    typeOneIv.setVisibility(View.GONE);
                }
                break;
            case R.id.llTwo:
                llTwo.setSelected(!llTwo.isSelected());
                typeTwoTv.setSelected(llTwo.isSelected());
                if (llTwo.isSelected()) {
                    typeTwoIv.setVisibility(View.VISIBLE);
                    typeOneTv.setSelected(false);
                    typeThreeTv.setSelected(false);
                    llOne.setSelected(false);
                    llThree.setSelected(false);
                    typeOneIv.setVisibility(View.GONE);
                    typeThreeIv.setVisibility(View.GONE);
                }else {
                    typeTwoIv.setVisibility(View.GONE);
                }
                break;
            case R.id.llThree:
                llThree.setSelected(!llThree.isSelected());
                typeThreeTv.setSelected(llThree.isSelected());
                if (llThree.isSelected()) {
                    typeOneTv.setSelected(false);
                    typeTwoTv.setSelected(false);
                    llOne.setSelected(false);
                    llTwo.setSelected(false);
                    typeOneIv.setVisibility(View.GONE);
                    typeTwoIv.setVisibility(View.GONE);
                    typeThreeIv.setVisibility(View.VISIBLE);
                }else {
                    typeThreeIv.setVisibility(View.GONE);
                }
                break;
            case R.id.commitTv:
                if(llOne.isSelected() || llTwo.isSelected() || llThree.isSelected()){
                    String content = contentEt.getText().toString();
                    if (TextUtils.isEmpty(content)) {
                        toast(getString(R.string.enter_feedback));
                        return;
                    }
                    if (content.toLowerCase().contains("script")) {
                        toast(getString(R.string.edit_address_illegal));
                        return;
                    }
                    showProgress();
                    presenter.commitFeedbackContent(content,getString(R.string.abnormal_function));
                }else {
                    toast(getString(R.string.select_feedback));
                }

                break;
        }
    }

    @Override
    public void getTypeSuccess(TypeListBean typeListBean) {
        hideProgress();
        todaySendTv.setText(getString(R.string.today_send)+typeListBean.getSent()+"/"+typeListBean.getSum());
        List<TypeListBean.ListBean> typeListBeanList = typeListBean.getList();
        if (typeListBeanList!=null&&typeListBeanList.size()==3) {
            typeOneTv.setText(typeListBeanList.get(0).getType());
            typeTwoTv.setText(typeListBeanList.get(1).getType());
            typeThreeTv.setText(typeListBeanList.get(2).getType());
        }
    }

    @Override
    public void getTypeFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void commitSuccess(String msg) {
        hideProgress();
        toast(msg);
        finish();
    }

    @Override
    public void commitFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);

    }
}
