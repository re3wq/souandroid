package com.taiyi.soul.moudles.main.home.options.presenter;

import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;

import java.util.List;

public interface OptionsView {
    void getDataSuccess(int status, String msg, List<OptionListBean> data);
    void onFailure();
}
