package com.taiyi.soul.moudles.main.deal;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.deal.fragment.DealPayFragment;
import com.taiyi.soul.moudles.main.deal.fragment.DealSellFragment;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\8 0008 11:43
 * @Author : yuan
 * @Describe ：集市交易
 */
public class DealFragment extends BaseFragment<NormalView, NormalPresenter> implements NormalView, View.OnClickListener {


    @BindView(R.id.deal_tab)
    SlidingTabLayout mDealTab;
    @BindView(R.id.deal_viewpager)
    ViewPager mDealViewpager;


    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private String[] mTitles;
    private float mTabWidth;
    int j = 0;
    private String mMainAccount;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_deal;
    }


    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        mMainAccount = Utils.getSpUtils().getString("mainAccount");
    }


    @Override
    protected void initData() {
        mTitles = new String[]{getResources().getString(R.string.deal_pay),
                getResources().getString(R.string.deal_sell)};

        DealPayFragment dealPayFragment = new DealPayFragment();
        DealSellFragment dealSellFragment = new DealSellFragment();
        if(!dealPayFragment.isAdded()){
            mFragments.add(new DealPayFragment());//买单
        }
        if(!dealSellFragment.isAdded()){
            mFragments.add(new DealSellFragment());//卖单
        }


        mAdapter = new MyPagerAdapter(getChildFragmentManager());
        mDealViewpager.setAdapter(mAdapter);


        mDealTab.setIsBackGround(true);
        Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.mipmap.node_hash_market_sel_iv);
        mDealTab.setDrawBitMap(bitmap,126);
        mDealTab.setViewPager(mDealViewpager);
        mDealViewpager.setCurrentItem(0);

        mDealTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mAdapter.notifyDataSetChanged();
                EventBus.getDefault().post("refresh_anim");
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

    }


    @Override
    public void initEvent() {


    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if (!hidden) {
            //TODO 刷新数据
            EventBus.getDefault().post("refresh_anim");
            mMainAccount = Utils.getSpUtils().getString("mainAccount");

        }
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        }
    }


    @OnClick({R.id.deal_entrust, R.id.deal_issue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.deal_entrust:
                getContext().startActivity(new Intent(getContext(), MainEntrustActivity.class));
                break;
            case R.id.deal_issue:
                if(AccountPrivateUtils.isHavePrivateKey()){//有私钥
                    Bundle bundle = new Bundle();
                    bundle.putInt("issue_type", mDealViewpager.getCurrentItem());
//                bundle.putSerializable("issueFeeBean",mIssueFeeBean);
                    ActivityUtils.next(getActivity(), IssueDealActivity.class, bundle, false);
                }else {//未导入私钥 ，去设置
                    Dialog dialog = new Dialog(getContext(), R.style.MyDialog);
                    View inflate = LayoutInflater.from(getContext()).inflate(R.layout.dialog_import_sub_wallet, null);
                    dialog.setContentView(inflate);
                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putString("accountName", mMainAccount);
                            ActivityUtils.next(getActivity(), ImportAccountActivity.class, bundle, false);
                        }
                    });
                    dialog.show();
                }
                break;

        }
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
