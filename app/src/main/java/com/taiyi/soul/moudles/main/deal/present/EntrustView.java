package com.taiyi.soul.moudles.main.deal.present;

import com.taiyi.soul.moudles.main.deal.bean.EntrustOneBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustThreeBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustTwoBean;

public interface EntrustView {
    void onSuccess(int code, EntrustOneBean bean);
    void onTwoSuccess(int code, EntrustTwoBean bean);
    void onThreeSuccess(int code, EntrustThreeBean bean);
    void onFailure();
    void onCancelSuccess(int code, String data);
}
