package com.taiyi.soul.moudles.main.deal.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\24 0024 17:51
 * @Author : yuan
 * @Describe ：
 */
public class EntrustTwoBean {

        public int totalpage;
        public int rowcount;
        public List<ComlistBean> comlist;

        public static class ComlistBean {
            public String createtime;
            public String charge;
            public String create_time;
            public String orderid;
            public String num;
            public int source;
            public boolean type;
            public String allmoney;
            public String coin_name;
            public String money;
            public String user_id;
            public String price;
            public String id;
            public int state;
            public String postersnum;
            public String hash;
            public String account;
            public String ifcharge;

    }
}
