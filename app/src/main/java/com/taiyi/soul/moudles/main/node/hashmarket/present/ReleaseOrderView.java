package com.taiyi.soul.moudles.main.node.hashmarket.present;

import com.taiyi.soul.moudles.main.deal.bean.IssueDealTotalBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.node.bean.HashBalanceBean;

public interface ReleaseOrderView {
    void onSuccess(int code, String msg_cn, IssueDealTotalBean bean);
    void onCheckPayPasswordSuccess(int code, String msg_cn);
    void entryOrdersSuccess(int code, String msg_cn);
    void getBalanceSuccess(String symbol, String balance);
    void onIssueFeeSuccess(IssueFeeBean bean);
    void onHashBalanceSuccess(int code, String msg_cn, HashBalanceBean data);
}
