package com.taiyi.soul.moudles.mine.bean;

public class InviteInfoBean {
    /**
     * invitationUrl : www.1
     * invitationCode : MhxZmW
     */

    private String invitationUrl;
    private String invitationCode;
    private String updown;

    public String getUpdown() {
        return updown;
    }

    public void setUpdown(String updown) {
        this.updown = updown;
    }

    public String getInvitationUrl() {
        return invitationUrl;
    }

    public void setInvitationUrl(String invitationUrl) {
        this.invitationUrl = invitationUrl;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }
}
