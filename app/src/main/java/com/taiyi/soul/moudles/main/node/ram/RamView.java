package com.taiyi.soul.moudles.main.node.ram;

import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;

public interface RamView {
    void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean);
    void getDataHttpFail(String errorMsg);

    void getRamPriceSuccess(String price);

    void getBalanceSuccess(String balance);

    void buySuccess();
    void sellSuccess();

    void onCheckPayPasswordSuccess(int code,String msg);

}
