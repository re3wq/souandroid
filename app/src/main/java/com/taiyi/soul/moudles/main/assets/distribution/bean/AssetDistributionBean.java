package com.taiyi.soul.moudles.main.assets.distribution.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\14 0014 17:20
 * @Author : yuan
 * @Describe ：
 */
public class AssetDistributionBean {


        public String totalAssets;
        public String color;
        public List<PercentBean> percent;

        public static class PercentBean {
            public String coinName;
            public String percentage;
            public String color;
            public String moneysum;
        }

}
