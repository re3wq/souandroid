package com.taiyi.soul.moudles.main.assets.currency;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import androidx.recyclerview.widget.LinearLayoutManager;


import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.CurrencyDetailsActivity;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.Utils;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class CurrencyActivity extends BaseActivity<CurrencyView, CurrencyPresent> implements CurrencyView,  AdapterManger.CurrencyItemClick {

    @BindView(R.id.titleTv)
    TextView titleTv;
//    @BindView(R.id.recyclerView)
//    RecyclerView recyclerView;

    @BindView(R.id.pull_recycle)
    PullRecyclerView pull_recycle;
    @BindView(R.id.top)
    ConstraintLayout top;
    private List<CurrencyListBean> list = new ArrayList<>();
    private CurrencyListAdapter currencyListAdapter;

    public static double hashprice=0;
    public void getHashPrice(){
        String url_hashprice="http://18.167.53.241:8088/hash.txt";
        HttpUtils.getRequets(url_hashprice, this, null, new JsonCallback<String>()  {
            @Override
            public String convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                super.onSuccess(response);
                try{ //response.body().get(0).split(" ")[0]
                    hashprice=Double.parseDouble(response.body().toString());
                }catch(Exception e){
                    hashprice=0;
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
            }
        });

    }
    @Override
    protected int getLayoutId() {
        return R.layout.activity_currency;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getRate("SOU");
        presenter.getRate("USDS");
        getHashPrice();//获取hash价格
        presenter.getCurrencyList(CurrencyActivity.this);
    }


    @Override
    public CurrencyPresent initPresenter() {
        return new CurrencyPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.currency));
        presenter.getCurrencyList(CurrencyActivity.this);

//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        showProgress();

//        CommonAdapter currencyListAdapter = AdapterManger.getCurrencyListAdapter(this, list,this);
//        recyclerView.setAdapter(currencyListAdapter);
    }

    @Override
    protected void initData() {
        pull_recycle.setLayoutManager(new LinearLayoutManager(CurrencyActivity.this));
//        pull_recycle.setOnPullLoadMoreListener(this);
        pull_recycle.setEmptyView(LayoutInflater.from(CurrencyActivity.this).inflate(R.layout.layout_empty, null));
        currencyListAdapter = new CurrencyListAdapter(CurrencyActivity.this);
        currencyListAdapter.setPullRecyclerView(pull_recycle);
        pull_recycle.scheduleLayoutAnimation();
        pull_recycle.setIsRefreshEnabled(false);
        pull_recycle.setIsLoadMoreEnabled(false);
        pull_recycle.setAdapter(currencyListAdapter);
        pull_recycle.refreshWithPull();
        pull_recycle.setPullLoadMoreCompleted();
        currencyListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<CurrencyListBean>() {
            @Override
            public void onItemClick(View covertView, int position, CurrencyListBean data) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("currencyBean", list.get(position));
                ActivityUtils.next(CurrencyActivity.this, CurrencyDetailsActivity.class, bundle);
            }
        });
    }

    @Override
    public void initEvent() {

    }

    @OnClick(R.id.backIv)
    public void onViewClicked() {
        finish();
    }


    @Override
    public void getListSuccess(List<CurrencyListBean> mList) {

        list.clear();
        for (CurrencyListBean currencyListBean : mList) {
            if (!"TetherUSDT".equals(currencyListBean.getCoinName())) {
                list.add(currencyListBean);
            }
        }
        currencyListAdapter.setDatas(list);
        pull_recycle.setPullLoadMoreCompleted();
//        recyclerView.getAdapter().notifyDataSetChanged();
        presenter.getBalance("SOU");
    }

    @Override
    public void onFailure(String errorMsg) {

    }

    private String balance_ngk = "0.0000";
    private String balance_usdn = "0.0000";
    private String balance_hash = "0.0000";
    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        hideProgress();
        if ("SOU".equals(symbol)) {
            balance_ngk = balance;
            presenter.getBalance("USDS");
        } else if("USDS".equals(symbol)){
            balance_usdn = balance;
            presenter.getBalance("HASH");
        }else if("HASH".equals(symbol)){
            balance_hash = balance;
            presenter.getSelectCoinRate();
        }
    }


    @Override
    public void getSelectCoinRateSuccess(List<SelectCoinRateBean> mList) {
        hideProgress();
        String selectCoin = Utils.getSpUtils().getString(Constants.SELECT_COIN);
        String symbol = Utils.getSpUtils().getString("symbol");
        String ngk_to_usdt = Utils.getSpUtils().getString(Constants.NGK_TO_USDT_RATE);
        String usdn_to_usdt = Utils.getSpUtils().getString(Constants.USDN_TO_USDT_RATE);
        for (SelectCoinRateBean selectCoinRateBean : mList) {
            if (selectCoinRateBean.getCurrency_code().equals(selectCoin)) {
                double rate = selectCoinRateBean.getRate();

                for (CurrencyListBean currencyListBean : list) {
                    if (currencyListBean.getCoinName().equals("SOU")) {
//                        currencyListBean.setPrice(ngk_to_usdt);
                        currencyListBean.setMoneyandfor(balance_ngk);
                        currencyListBean.setMfcon((new BigDecimal(ngk_to_usdt).multiply(new BigDecimal(balance_ngk)).multiply(new BigDecimal(rate)).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
                    } else if (currencyListBean.getCoinName().equals("USDS")) {
//                        currencyListBean.setPrice(usdn_to_usdt);
                        currencyListBean.setMoneyandfor(new BigDecimal(balance_usdn).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
                        currencyListBean.setMfcon(new BigDecimal(usdn_to_usdt).multiply(new BigDecimal(balance_usdn)).multiply(new BigDecimal(rate)).toPlainString());
                    }else if (currencyListBean.getCoinName().equals("HASH")) {
//                        currencyListBean.setPrice(usdn_to_usdt);
                        rate=hashprice;
                        currencyListBean.setMoneyandfor(new BigDecimal(balance_hash).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
                        currencyListBean.setMfcon(new BigDecimal(usdn_to_usdt).multiply(new BigDecimal(balance_hash)).multiply(new BigDecimal(rate)).toPlainString());
                    }
                }
                currencyListAdapter.notifyDataSetChanged();
                pull_recycle.setPullLoadMoreCompleted();
                break;
            }
        }
    }

    @Override
    public void getSelectCoinRateFailure(String errorMsg) {
        hideProgress();
    }



    @Override
    public void onItemClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("currencyBean", list.get(position));
        ActivityUtils.next(this, CurrencyDetailsActivity.class, bundle);
    }

    @Override
    public void getRateSuccess(String symbol, String rate) {
        if ("SOU".equals(symbol)) {
            Utils.getSpUtils().put(Constants.NGK_TO_USDT_RATE, rate);
        } else {
            Utils.getSpUtils().put(Constants.USDN_TO_USDT_RATE, rate);
        }
    }

    @Override
    public void getRateFailure(String errorMsg) {
        toast(errorMsg);
    }

}
