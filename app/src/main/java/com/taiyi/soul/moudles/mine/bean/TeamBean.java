package com.taiyi.soul.moudles.mine.bean;

import java.util.List;

public class TeamBean {

    /**
     * account : nobqeoqa.ngk
     * sum : 1
     * list : [{"account":"3ffrgoip.ngk","sum":0,"list":[]}]
     */

    private String account;
    private int sum;
    private List<TeamBean> list;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public List<TeamBean> getList() {
        return list;
    }

    public void setList(List<TeamBean> list) {
        this.list = list;
    }

}
