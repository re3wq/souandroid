package com.taiyi.soul.moudles.mine.myteam;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.Utils;

import java.util.List;


public class TeamTreeListViewAdapter<T> extends TeamTreeViewAdapter<T> {


    private OnCopyClickListener onCopyClickListener;
    private String main_account;

    private String from;

    public OnCopyClickListener getOnCopyClickListener() {
        return onCopyClickListener;
    }

    public void setOnCopyClickListener(OnCopyClickListener onCopyClickListener) {
        this.onCopyClickListener = onCopyClickListener;
    }

    public TeamTreeListViewAdapter(Context context, String main_account, String from, ListView listView, List<T> datas, int defaultNodeLevel)
            throws IllegalAccessException {
        super(context, listView, datas, defaultNodeLevel);
        this.main_account = main_account;
        this.from = from;
    }

    @Override
    public View getConvertView(Node node, final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
//        if (convertView == null) {
        convertView = mInflater.inflate(R.layout.item_team_tree, parent, false);
//            convertView = mInflater.inflate(R.layout.item_account_tree, parent, false);
        holder = new ViewHolder();
        holder.iv = convertView.findViewById(R.id.iv);
        holder.tv_name = convertView.findViewById(R.id.tv_name);
        holder.tv_phone = convertView.findViewById(R.id.tv_phone);
        holder.iv_selected = convertView.findViewById(R.id.iv_selected);

        int nsumNumNew = Utils.getSpUtils().getInt("NsumNumNew", 1);
        RelativeLayout re_item = convertView.findViewById(R.id.re_item);
//        if(nsumNumNew>17){
//            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext, nsumNumNew * 70f-(DensityUtil.dip2px(mContext, 75))), DensityUtil.dip2px(mContext, 40f)));
//        }else if(nsumNumNew>18) {
//            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext, 4 * 170f), DensityUtil.dip2px(mContext, 40f)));
//        }else if(nsumNumNew>14) {
//            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext, 4 * 150f), DensityUtil.dip2px(mContext, 40f)));
//        }else if(nsumNumNew>11) {
//            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext, 4 * 130f), DensityUtil.dip2px(mContext, 40f)));
//        }else if(nsumNumNew>8) {
//            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext, 4 * 110f), DensityUtil.dip2px(mContext, 40f)));
//        }else if(nsumNumNew>5) { // i+3  i+3+3   i+3+3+3
//            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext, 4 * 90f), DensityUtil.dip2px(mContext, 40f)));
//        }else{
//            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext, 4 * 80f), DensityUtil.dip2px(mContext, 40f)));
//        }

        if(nsumNumNew>5){
            int dev = dev(nsumNumNew-5);
//            Log.e("nnn===",dev+"");
            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext,  (dev*20+90)*4), DensityUtil.dip2px(mContext, 40f)));
        }else{
            re_item.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(mContext, 4 * 80f), DensityUtil.dip2px(mContext, 40f)));
        }

      //  (nsumNumNew-5)/3  0   90 +0
      //  (nsumNumNew-5)/3   1   90 +20
      //  (nsumNumNew-5)/3   2   90 +20*2
      //  (nsumNumNew-5)/3   3   90 +20*3
      //  (nsumNumNew-5)/3   4   90 +20*4
      //  (nsumNumNew-5)/3   5   90 +20*5
//            holder.tv_code = convertView.findViewById(R.id.tv_code);
//            holder.iv_copy = convertView.findViewById(R.id.iv_copy);
//            holder.iv_selected = convertView.findViewById(R.id.iv_selected);
//            holder.iv_p_level = convertView.findViewById(R.id.iv_p_level);
//            holder.iv_z_level = convertView.findViewById(R.id.iv_z_level);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }

        //图标
        if (!node.isRootNode()) {
//            if (node.getIcon() == -1) {
            holder.iv.setVisibility(View.GONE);

        } else {
            if (node.getXiaList().size() > 0) {
                holder.iv.setVisibility(View.VISIBLE);
                holder.iv.setImageResource(node.getIcon());
            } else {
                holder.iv.setVisibility(View.VISIBLE);
                holder.iv.setImageResource(R.mipmap.team_dot);
            }

        }


        holder.tv_name.setText(node.getUsername());


//        if(node.getNum().equals("0")){
//            holder.iv_selected.setVisibility(View.GONE);
//            holder.tv_phone.setText("");
//        }else {
        holder.iv_selected.setVisibility(View.VISIBLE);
        holder.tv_phone.setText(node.getNum());
//        }


//        holder.tv_code.setText(node.getUnion_id());

        holder.iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expendOrCollapseNode(position);
            }
        });

//        holder.iv_copy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onCopyClickListener.onCopyClick(node.getUnion_id());
//
//            }
//        });

        return convertView;
    }
    int num = 0;
    public int dev(Integer st){
        int  s = st/3;
        num++;
        if(s>3){
            dev(s);
        }
        return s;
    }
    private class ViewHolder {
        ImageView iv;
        ImageView iv_selected;
        TextView tv_name;
        TextView tv_phone;
//        TextView tv_code;
//        ImageView iv_copy;
//        ImageView iv_selected;
//        ImageView iv_p_level;
//        ImageView iv_z_level;

    }


    /**
     * TreeListView的icon对外点击接口
     */
    public interface OnCopyClickListener {
        void onCopyClick(String value);
    }
}
