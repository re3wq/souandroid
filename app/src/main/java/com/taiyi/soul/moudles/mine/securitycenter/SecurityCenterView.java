package com.taiyi.soul.moudles.mine.securitycenter;

public interface SecurityCenterView {
    void onSuccess();
    void onFailure(String errorMsg);
    void onCheckPayPasswordSuccess(int code,String msg);
}
