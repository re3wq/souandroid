package com.taiyi.soul.moudles.mine.invite

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View.*
import androidx.core.content.FileProvider
import com.mylhyl.zxing.scanner.encode.QREncode
import com.taiyi.soul.R
import com.taiyi.soul.base.BaseActivity
import com.taiyi.soul.moudles.mine.bean.InviteInfoBean
import com.taiyi.soul.utils.*
import kotlinx.android.synthetic.main.activity_invite.*
import kotlinx.android.synthetic.main.base_top_layout.*
import me.ljp.permission.PermissionItem
import java.io.File
import java.io.FileNotFoundException
import java.util.*

class InviteActivity : BaseActivity<InviteView, InvitePresent>(), InviteView {
    lateinit var invitationUrl: String
    lateinit var invitationCode: String
    lateinit var updown: String

    override fun getLayoutId(): Int {
        return R.layout.activity_invite
    }

    override fun initPresenter(): InvitePresent {
        return InvitePresent()
    }

    override fun initImmersionBar() {
        super.initImmersionBar()
        mImmersionBar.titleBar(top).init()
    }

    override fun initViews(savedInstanceState: Bundle?) {
        titleTv.text = getString(R.string.invite_)
//        rightTitleTv.text = "分享"
        rightIv.visibility = VISIBLE
        rightIv.maxWidth = DensityUtil.dip2px(this, 18.0f)
        rightIv.maxHeight = DensityUtil.dip2px(this, 18.0f)
        rightIv.setImageResource(R.mipmap.asset_invite_img)
    }

    override fun initData() {
        showProgress()
        presenter.getInviteInfo(this)
    }

    override fun initEvent() {
        backIv.setOnClickListener {
            finish()
        }
        rightIv.setOnClickListener {
            var bitmap = ViewToImageUtils.loadBitmapFromView(cl)
            val permissonItems: MutableList<PermissionItem> = ArrayList()
            permissonItems.add(PermissionItem(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.WRITE_STORAGE), R.drawable.permission_ic_storage))
            if (Utils.getPermissions(permissonItems, getString(R.string.SCAN_EXTERNAL_STORAGE))) {
                var saveFile = ViewToImageUtils.saveFile(bitmap)
                if (saveFile != null) {
                    // 其次把文件插入到系统图库
                    try {
                        MediaStore.Images.Media.insertImage(contentResolver,
                                saveFile.absolutePath, saveFile.name, null);
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace();
                    }
                    // 最后通知图库更新
                    sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(saveFile.absolutePath)));
//                    toast("保存成功")
                    shareSingleImage(saveFile)
                } else {
                    toast(getString(R.string.save_fail))
                }
            }
        }
        copyInviteCodeIv.setOnClickListener {
            toast(getString(R.string.copy_success))
            CopyUtils.CopyToClipboard(this, invitationCode)
        }
        copyLinkIv.setOnClickListener {
            toast(getString(R.string.copy_success))
            CopyUtils.CopyToClipboard(this, invitationUrl)
        }
//        qrCodeIv.setOnLongClickListener {
//            var bitmap = ViewToImageUtils.loadBitmapFromView(cl)
//            val permissonItems: MutableList<PermissionItem> = ArrayList()
//            permissonItems.add(PermissionItem(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.WRITE_STORAGE), R.drawable.permission_ic_storage))
//            if (Utils.getPermissions(permissonItems, getString(R.string.SCAN_EXTERNAL_STORAGE))) {
//                var saveFile = ViewToImageUtils.saveFile(bitmap)
//                if (saveFile != null) {
//                    // 其次把文件插入到系统图库
//                    try {
//                        MediaStore.Images.Media.insertImage(contentResolver,
//                                saveFile.absolutePath, saveFile.name, null);
//                    } catch (e: FileNotFoundException) {
//                        e.printStackTrace();
//                    }
//                    // 最后通知图库更新
//                    sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(saveFile.absolutePath)));
//                    toast(getString(R.string.save_success))
//                } else {
//                    toast(getString(R.string.save_fail))
//                }
//            }
//
//            false
//        }
    }

    private fun shareSingleImage(file: File) { //由文件得到uri
        val imageUri = Uri.fromFile(file)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
        shareIntent.type = "image/*"
        if (file.isFile && file.exists()) {
            var uri: Uri? = null
            uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                FileProvider.getUriForFile(this, ShareUtils.AUTHORITY, file)
            } else {
                Uri.fromFile(file)
            }
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        }
        shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(Intent.createChooser(shareIntent, "分享到"))
    }


    override fun onFailure(errorMsg: String?) {
        hideProgress()
        toast(errorMsg)
    }

    override fun getInviteInfoSuccess(inviteInfoBean: InviteInfoBean?) {
        hideProgress()
        invitationUrl = inviteInfoBean?.invitationUrl.toString()
        invitationCode = inviteInfoBean?.invitationCode.toString()
        updown = inviteInfoBean?.updown.toString()

        val bitmap: Bitmap? = QREncode.Builder(this)
                .setContents("$invitationUrl?invitationCode=$invitationCode") //二维码内容
                .setMargin(1)
                .build().encodeAsBitmap()

//        val bitmap: Bitmap? = QREncode.Builder(this)
//                .setContents(updown) //二维码内容
//                .setMargin(1)
//                .build().encodeAsBitmap()
        if (bitmap != null) {
            iv.setImageBitmap(bitmap)
//            qrCodeIv1.setImageBitmap(bitmap)
        }
        inviteCodeTv.text = inviteInfoBean?.invitationCode
//        inviteCodeTv1.text = inviteInfoBean?.invitationCode
        linkTv.text = inviteInfoBean?.invitationUrl


        if (inviteInfoBean != null) {
            if("".equals(inviteInfoBean.invitationCode)){
                copyInviteCodeIv.visibility = GONE
                copyInviteCodeIv.setOnKeyListener(null)
//                inviteCodeTv1.visibility = GONE
                inviteCodeTv.text = getString(R.string.invite_no)
//                inviteCodeTv_LL.visibility = GONE
                inviteCodeTv.visibility = VISIBLE
                AlertDialogShowUtil.toastMessage(this,getString(R.string.invite_hint))
            }else{
//                inviteCodeTv_LL.visibility = VISIBLE
//                inviteCodeTv.visibility = GONE
//                inviteCodeTv_1.text = invitationCode.substring(0, 1)
//                inviteCodeTv_2.text = invitationCode.substring(1, 2)
//                inviteCodeTv_3.text = invitationCode.substring(2, 3)
//                inviteCodeTv_4.text = invitationCode.substring(3, 4)
//                inviteCodeTv_5.text = invitationCode.substring(4, 5)
//                inviteCodeTv_6.text = invitationCode.substring(5, 6)
            }
        }
    }

}
