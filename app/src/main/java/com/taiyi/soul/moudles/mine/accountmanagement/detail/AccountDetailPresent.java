package com.taiyi.soul.moudles.mine.accountmanagement.detail;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.RsaUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class AccountDetailPresent extends BasePresent<AccountDetailView> {
    //验证交易密码
    public void checkPayPassword(String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }
}
