package com.taiyi.soul.moudles.main.home;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;
import com.taiyi.soul.moudles.main.home.bean.HomeBean;
import com.taiyi.soul.moudles.main.home.bean.OptionIsOptenBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.mine.bean.CurrencyListBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class HomePresent extends BasePresent<HomeView> {

    public void getData(){
        HttpUtils.getRequets(BaseUrl.FIRST_NEWS, this, null, new JsonCallback<BaseResultBean<HomeBean>>() {
            @Override
            public BaseResultBean<HomeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<HomeBean>> response) {
                super.onSuccess(response);
                if(null!=view){
                    if (response.body().status==200){
                        view.onSuccess(response.body().data);
                    }else {
                        view.onFailure(response.body().msg);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<HomeBean>> response) {
                super.onError(response);
            }
        });
    }

    public void updateReadNum(String id){
        Map<String,String>map=new HashMap<>();
        map.put("newId",id);
        HttpUtils.getRequets(BaseUrl.UPDATE_NEWS_READ_NUM, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if(null!=view) {
                    if (response.body().status == 200) {
                        view.updateSuccess();
                    } else {
                        view.updateFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    public void getFirstData(){
        HttpUtils.postRequest(BaseUrl.GET_ACTIVITY_FIRST_DATA, this, (String) null, new JsonCallback<BaseResultBean<ActivityFirstBean>>() {
            @Override
            public BaseResultBean<ActivityFirstBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ActivityFirstBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().code==0){
                        view.activitySuccess(response.body().data);
                    }else {
                        view.activityFailure(response.body().code,response.body().msg_cn);
                    }
                }}
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ActivityFirstBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getCurrencyList() {
        HttpUtils.getRequets(BaseUrl.GET_CURRENCY_PRICE_LIST, this, null, new JsonCallback<BaseResultBean<List<CurrencyListBean>>>() {
            @Override
            public BaseResultBean<List<CurrencyListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<CurrencyListBean>>> response) {
                super.onSuccess(response);
                    if(null!=view)
                        view.getCoinSuccess(response.body().status, response.body().msg, response.body().data);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<CurrencyListBean>>> response) {
                super.onError(response);
            }
        });
    }
 public void getOptionOpen() {
        HttpUtils.getRequets(BaseUrl.IFFUPDOWNOPEN, this, null, new JsonCallback<BaseResultBean<OptionIsOptenBean>>() {
            @Override
            public BaseResultBean<OptionIsOptenBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<OptionIsOptenBean>> response) {
                super.onSuccess(response);
                    if(null!=view)
                        view.getOptionOption(response.body().code,response.body().msg_cn, response.body().data);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<OptionIsOptenBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getOptionData(Activity activity) {
        Map map = new HashMap();
        map.put("term", "1");//1成交量2名称3涨幅
        map.put("reversed", "1");//1 true 0false
        map.put("coin", Utils.getSpUtils().getString("cur_coin_name"));

        HttpUtils.getRequets(BaseUrl.MAKETALL, this, map, new JsonCallback<BaseResultBean<List<OptionListBean>>>() {
            @Override
            public BaseResultBean<List<OptionListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<OptionListBean>>> response) {
                super.onSuccess(response);
                if (response.body().status == 602) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg);
                    //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure(response.body().msg);
                } else {
                    if(null!=view){
                        view.getDataSuccess(response.body().status, response.body().msg, response.body().data);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<OptionListBean>>> response) {
                super.onError(response);
//                if (null != view)
//                    view.onFailure();
            }
        });
    }
}
