package com.taiyi.soul.moudles.main.home.mall.order.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.home.mall.order.CheckPayActivity;
import com.taiyi.soul.moudles.main.home.mall.order.OrderDetailActivity;
import com.taiyi.soul.moudles.main.home.mall.order.adapter.OrderPaymentAdapter;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderListPay;
import com.taiyi.soul.moudles.main.home.mall.order.present.OrderPresent;
import com.taiyi.soul.moudles.main.home.mall.order.present.OrderView;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.shippingaddress.ShippingAddressActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\5\22 0022 10:23
 * @Author : yuan
 * @Describe ：待支付
 */
public class OrderPaymentFragment extends BaseFragment<OrderView, OrderPresent> implements OrderView,  OrderPaymentAdapter.OrderItemButtonClick, PullRecyclerView.PullLoadMoreListener {
    @BindView(R.id.order_recycle)
    PullRecyclerView mOrderRecycle;
    private OrderPaymentAdapter mFragmentAdapter;

    private int pagerNo = 1;
    private List<OrderBean.OrderlistBean> mOrderlist;
    private String mOrdercommodityid;
    private String allmoney_ngk;
    private String allmoney_usdn;
    private String mNGK_ratio;
    private String orderid;


    @Override
    public OrderPresent initPresenter() {
        return new OrderPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if (EventBus.getDefault().isRegistered(false)) {
            EventBus.getDefault().register(this);
        }
    }


        @Subscribe
    public void even(String event) {
        if (event.equals("order_refresh")) {
            showProgress();
            presenter.getOrderList(getActivity(), 1 + "", 0 + "");
        }
    }

    @Override
    protected void initData() {
        mOrderRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        mOrderRecycle.setOnPullLoadMoreListener(this);
        mOrderRecycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new OrderPaymentAdapter(getContext(), this);
        mFragmentAdapter.setPullRecyclerView(mOrderRecycle);
        mOrderRecycle.setItemAnimator(new DefaultItemAnimator());
        mOrderRecycle.setAdapter(mFragmentAdapter);
        mOrderRecycle.refreshWithPull();

    }

    @Override
    public void initEvent() {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_order;
    }



    /************************************edit order****************************************************/
    @Override
    public void onCancelOrderClick(int position) {//取消订单
//        Log.e("cancle_po",position+"");
        View view = LayoutInflater.from(getContext()).inflate(R.layout.popup_cancel, null);
        OrderPopupWindow orderPopupWindow = new OrderPopupWindow(getContext(), view);

        ((TextView) view.findViewById(R.id.consider)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderPopupWindow.dismiss();
            }
        });
        ((TextView) view.findViewById(R.id.sure)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderPopupWindow.dismiss();
                showProgress();
                presenter.getCancelOrder(getActivity(), mOrderlist.get(position).ordercommodityid);

            }
        });
        orderPopupWindow.show(getView(), getActivity().getWindow(), 0);
    }

    @Override
    public void onPayClick(int position) {//支付
        showProgress();
        presenter.getZhiFuLie(getActivity(), mOrderlist.get(position).ordercommodityid);
        orderid = mOrderlist.get(position).orderid;
    }

    @Override
    public void onChangeAddressClick(int position) {//修改地址

        mOrdercommodityid = mOrderlist.get(position).ordercommodityid;
        Intent intent = new Intent(getContext(), ShippingAddressActivity.class);
        intent.putExtra("type", 1);
        startActivityForResult(intent, 1001);
    }
    @Override
    public void onItemClick(int position) {//待付款
        Bundle bundle = new Bundle();
        bundle.putInt("orderType", 1);
//        Log.e("posss===",position+"");
        bundle.putString("ordercommodityid", mOrderlist.get(position).ordercommodityid);
        ActivityUtils.next(getActivity(), OrderDetailActivity.class, bundle, false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            if(null != data){
                String addressId = data.getStringExtra("addressId");
                showProgress();
                presenter.getEditOrderAddress(getActivity(), mOrdercommodityid, addressId);
            }

        }
    }


    /************************************Call Interface callback****************************************************/

    @Override
    public void onGetListSuccess(OrderBean bean) {
        hideProgress();

        if (pagerNo == 1) {
            mFragmentAdapter.clearDatas();
            mOrderlist = bean.orderlist;
            mFragmentAdapter.setDatas(bean.orderlist);
        } else {
            mOrderlist.addAll(bean.orderlist);
            mFragmentAdapter.addDatas(bean.orderlist);
        }
        mOrderRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onCancelOrderSuccess(String bean) {
        hideProgress();
        View view = LayoutInflater.from(getContext()).inflate(R.layout.popup_cancel_finish, null);
        OrderPopupWindow cancel_popu = new OrderPopupWindow(getContext(), view);
        cancel_popu.show(getView(), getActivity().getWindow(), 0);
        android.os.Handler hander = new android.os.Handler();
        hander.postDelayed(new Runnable() {
            @Override
            public void run() {
                cancel_popu.dismiss();
                mOrderRecycle.refreshWithPull();
            }
        }, 1500);

    }

    @Override
    public void onEditAddressSuccess(String bean) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(getContext(), bean);
    }

    @Override
    public void onRemindDeliverySuccess(String bean) {

    }

    @Override
    public void onZhiFuLieSuccess(OrderListPay bean) {
        hideProgress();
        Bundle bundle = new Bundle();
        bundle.putString("allmoney_ngk", bean.allmoney_ngk);
        bundle.putString("allmoney_usdn", bean.allmoney_usdn);
        bundle.putString("NGK_RATIO", bean.ngk_ratio);
        bundle.putString("orderid", orderid);
        ActivityUtils.next(getActivity(), CheckPayActivity.class, bundle, false);
    }

    @Override
    public void onFailure() {
        hideProgress();
        mOrderRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onTakeDeliverySuccess(String bean) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (null != presenter) {
                presenter.getOrderList(getActivity(), pagerNo + "", 0 + "");
            }
        }
    }

    @Override
    public void onRefresh() {
        pagerNo = 1;
        presenter.getOrderList(getActivity(), pagerNo + "", 0 + "");
    }

    @Override
    public void onLoadMore() {
        pagerNo++;
        presenter.getOrderList(getActivity(), pagerNo + "", 0 + "");
    }
}
