package com.taiyi.soul.moudles.main.assets.collectmoney;

import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.CollectMoneyAddressBean;

import java.util.List;

public interface CollectMoneyView {
    void getCoinListSuccess(List<CoinListBean> coinListBeanList);
    void getCoinListFailure(String errorMsg);
    void getAddressSuccess(CollectMoneyAddressBean address);
    void getAddressFailure(String errorMsg);
}
