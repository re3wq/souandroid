package com.taiyi.soul.moudles.mine.invite;

import android.app.Activity;
import android.content.Intent;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.bean.InviteInfoBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class InvitePresent extends BasePresent<InviteView> {
    public void getInviteInfo(Activity activity){
        Map<String,String>map=new HashMap<>();
        map.put("type","1");
        HttpUtils.getRequets(BaseUrl.GET_INVITE_INFO, this, map, new JsonCallback<BaseResultBean<InviteInfoBean>>() {
            @Override
            public BaseResultBean<InviteInfoBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<InviteInfoBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getInviteInfoSuccess(response.body().data);
                    } else if (response.body().status == 602) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            activity.startActivity(new Intent(activity, LoginActivity.class));
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else if (response.body().status == 3080005) {//登录失效
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
//                   //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }

                        Utils.getSpUtils().remove(Constants.TOKEN);
                    } else {
                        view.onFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<InviteInfoBean>> response) {
                super.onError(response);
            }
        });
    }
}
