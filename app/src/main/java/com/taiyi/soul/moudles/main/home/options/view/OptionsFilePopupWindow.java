package com.taiyi.soul.moudles.main.home.options.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.taiyi.soul.R;
import com.taiyi.soul.utils.DensityUtil;

public class OptionsFilePopupWindow extends PopupWindow {

    private Context context;
    private View mView;

    private OnPictureItemClick onItemClick;
    public OptionsFilePopupWindow(Context context,OnPictureItemClick onItemClick) {
        this.context = context;
        this.onItemClick = onItemClick;
        init();
    }



    private void init() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.popup_options_file, null);


        RadioGroup rg_main = mView.findViewById(R.id.rg_main);
        RadioGroup rg_pair = mView.findViewById(R.id.rg_pair);
        RadioButton ma = mView.findViewById(R.id.ma);
        RadioButton ema = mView.findViewById(R.id.ema);
        RadioButton boll = mView.findViewById(R.id.boll);
        RadioButton macd = mView.findViewById(R.id.macd);
        RadioButton kdj = mView.findViewById(R.id.kdj);
        rg_main.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == ma.getId()){
                    onItemClick.onPictureMainClick(0);
                }else if(checkedId == boll.getId()){
                    onItemClick.onPictureMainClick(2);
                }else if(checkedId == ema.getId()){
                    onItemClick.onPictureMainClick(1);
                }


            }
        });
        rg_pair.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == macd.getId()){
                    onItemClick.onPicturePairHide(0);
                }else if(checkedId == kdj.getId()){
                    onItemClick.onPicturePairHide(1);
                }


            }
        });
        // 设置外部可点击
        this.setOutsideTouchable(false);

        /* 设置弹出窗口特征 */
        // 设置视图
        this.setContentView(this.mView);
        // 设置弹出窗体的宽和高
        this.setHeight(DensityUtil.dip2px(context,67));
        this.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);


        // 设置弹出窗体可点击
        this.setFocusable(true);

        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0x00000000);
        // 设置弹出窗体的背景
        this.setBackgroundDrawable(dw);

        // 设置弹出窗体显示时的动画，从底部向上弹出
        this.setAnimationStyle(R.style.shop_popup_window_anim);

        this.setOnDismissListener(new OnDismissListener() {

            // 在dismiss中恢复透明度
            public void onDismiss() {

            }
        });


    }

    public interface OnPictureItemClick{
        void  onPictureMainClick(int position);
        void  onPicturePairHide(int position);
    }
}
