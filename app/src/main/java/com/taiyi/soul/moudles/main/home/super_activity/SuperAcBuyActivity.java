package com.taiyi.soul.moudles.main.home.super_activity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.EosSignDataManger;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taiyi.soul.utils.Utils.getContext;

public class SuperAcBuyActivity extends BaseActivity<SuperAcBuyView, SuperAcBuyPresent> implements SuperAcBuyView, PullRecyclerView.PullLoadMoreListener {


    @BindView(R.id.titleTv)
    TextView mTitleTv;

    @BindView(R.id.computingPowerPriceTv)//ngk价格
            TextView mComputingPowerPriceTv;
    @BindView(R.id.quantityHash)//已购数量单位
            TextView mQuantityHash;
    @BindView(R.id.amountTv)//剩余额度
            NumberRollingView mAmountTv;
    @BindView(R.id.inputQuantityEt)//输入购买数量
            EditText mInputQuantityEt;
    @BindView(R.id.t)//购买数量单位
            TextView mT;
    @BindView(R.id.usdn)//可用余额单位
            TextView mUsdn;
    @BindView(R.id.balanceTv)//USDN可用余额
            NumberRollingView mBalanceTv;
    @BindView(R.id.totalPaymentTv)//支付合计金额
            TextView mTotalPaymentTv;
    @BindView(R.id.tt)//支付合计单位
            TextView mTt;
    @BindView(R.id.buyTv)//购买按钮
            TextView mBuyTv;
    @BindView(R.id.recyclerView)//记录列表
            PullRecyclerView mRecyclerView;


    @BindView(R.id.empty_data)
    LinearLayout empty_data;
    Handler handler = new Handler();
    Runnable runnable;
    @BindView(R.id.key_scroll)
    LinearLayout keyScroll;
    @BindView(R.id.keyboardPlace)
    LinearLayout keyboardPlace;
    @BindView(R.id.key_main)
    RelativeLayout keyMain;
    private SuperAcBuyNumBean mSuperAcBuyNumBean;
    private String mAirdrop_address;
    private int PageNum = 1;
    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    private SuperAcBuyAdapter mFragmentAdapter;
    private String mIfopen;
    private String mSurplus_money;
    private String ngkPrice;
    private int pay = 0;
    private SafeKeyboard safeKeyboard1;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_super_ac_buy;
    }

    @Override
    public SuperAcBuyPresent initPresenter() {
        return new SuperAcBuyPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        String ngkPrice = getIntent().getExtras().getString("ngkPrice");
        String surplus_money = getIntent().getExtras().getString("surplus_money");
//        mAmountTv.setText(surplus_money);
//        mComputingPowerPriceTv.setText(ngkPrice);
//        mTitleTv.setText(getString(R.string.activity_buy_title));
        mTitleTv.setText(getString(R.string.activity_title_new));
        presenter.getFirstData();
//        mIfopen = getIntent().getExtras().getString("mIfopen");
//        if(!mIfopen.equals("2")){
//            mBuyTv.setText(getResources().getString(R.string.air_buy_button));
//            mBuyTv.setOnClickListener(null);
//            mBuyTv.setTextColor(getResources().getColor(R.color.color_898e93));
//        }else {
//            mBuyTv.setText(getResources().getString(R.string.buy));
//        }
        presenter.getBalance("USDS");
        presenter.getFindOrderData(SuperAcBuyActivity.this);
        presenter.getBuyAirList(SuperAcBuyActivity.this, PageNum + "");

        mRecyclerView.setLayoutManager(new LinearLayoutManager(SuperAcBuyActivity.this));
        mRecyclerView.setOnPullLoadMoreListener(this);
        mFragmentAdapter = new SuperAcBuyAdapter(SuperAcBuyActivity.this);
        mFragmentAdapter.setPullRecyclerView(mRecyclerView);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mFragmentAdapter);
        mRecyclerView.refreshWithPull();
        mRecyclerView.setPullLoadMoreCompleted();

        mInputQuantityEt.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(mInputQuantityEt);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                safeKeyboard1 = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, keyMain, keyScroll, list, true);
            }
        }, 500);
        mInputQuantityEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (runnable != null) {
                    handler.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (!mInputQuantityEt.getText().toString().equals("")) {
                            if (safeKeyboard1 != null && safeKeyboard1.isShow()) {
                                safeKeyboard1.hideKeyboard();
                            }
                            showProgress();
                            presenter.getTotalPrice(SuperAcBuyActivity.this, mInputQuantityEt.getText().toString());
                        }
                    }
                };
                handler.postDelayed(runnable, 1000);
            }
        });


    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
            View v = getCurrentFocus();

            if (isShouldHideInput(v, ev,keyboardPlace)){
                hideSoftInput(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (event.getY() > top && event.getY() < bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() -keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                if (null != safeKeyboard1 && safeKeyboard1.isShow()) {
                    return true;
                } else {
                    return false;
                }

            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard1 != null && safeKeyboard1.stillNeedOptManually(false)) {
                safeKeyboard1.hideKeyboard();
            }
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }


    @OnClick({R.id.backIv, R.id.buyTv, R.id.rightIv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.buyTv:

                if (mInputQuantityEt.getText().toString().equals("")) {
                    AlertDialogShowUtil.toastMessage(SuperAcBuyActivity.this, getResources().getString(R.string.air_buy_hint));
                    return;
                }
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.buyTv);
                if (!fastDoubleClick) {
                    if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                        buyMemo++;
                        if (pay == 1) {
                            showPassword(view);
                        } else {
                            toast(getString(R.string.activity_hint_pf));
                        }

                    } else {//未导入私钥 ，去设置
                        Dialog dialog = new Dialog(SuperAcBuyActivity.this, R.style.MyDialog);
                        View inflate = LayoutInflater.from(SuperAcBuyActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                        dialog.setContentView(inflate);
                        inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Bundle bundle = new Bundle();
                                bundle.putString("accountName", mainAccount);
                                ActivityUtils.next(SuperAcBuyActivity.this, ImportAccountActivity.class, bundle, false);
                            }
                        });
                        dialog.show();
                    }
                }


//                getSign();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    private void showPassword(View view) { //显示密码框
        View v = LayoutInflater.from(SuperAcBuyActivity.this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(SuperAcBuyActivity.this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);

        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInputpop(viewById, ev,keyboardPlace)) {
                        hideSoftInputpopu(view.getWindowToken(), 1);
                    }
                }
                return false;
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(SuperAcBuyActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showProgress();
//                            presenter.checkPayPassword(SuperAcBuyActivity.this,s);
                            getSign(s);
                            pay_popup.dismiss();
                        }
                    }, 500);
                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputpop(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (safeKeyboard != null && safeKeyboard.isShow()) {
                if (event.getY() > top && event.getY() < bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            } else {
                if (event.getY() > top && event.getY() < (bottom + DensityUtil.dip2px(getContext(), 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    private void hideSoftInputpopu(IBinder token, int i) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {
                pay_popup.dismiss();
            }
        }
    }

    private int buyMemo = 0;

    private void getSign(String s) {//获取签名
        String memo = "";
        String price = "";
        String unit = "";
//        memo = "air_buy" + "_" + buyMemo;
        memo = "Buy ngk" + "_" + buyMemo;
        if (null == mSuperAcBuyNumBean.coin_name) {
            AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.socket_time_out));
            return;
        }
        if (mSuperAcBuyNumBean.coin_name.equals("SOU")) {
            unit = Constants.NGK_CONTRACT_ADDRESS;
            price = new BigDecimal(mSuperAcBuyNumBean.pay_money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString() + " " + mSuperAcBuyNumBean.coin_name;
//            price = new DecimalFormat("##0.0000").format(new Double(mSuperAcBuyNumBean.pay_money)) + " " + mSuperAcBuyNumBean.coin_name;
        } else if (mSuperAcBuyNumBean.coin_name.equals("USDS")) {
            unit = Constants.USDN_CONTRACT_ADDRESS;
            price = new BigDecimal(mSuperAcBuyNumBean.pay_money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString() + " " + mSuperAcBuyNumBean.coin_name;
//            price = new DecimalFormat("##0.00000000").format(new Double(mSuperAcBuyNumBean.pay_money)) + " " + mSuperAcBuyNumBean.coin_name;
        }

        new EosSignDataManger(SuperAcBuyActivity.this, new EosSignDataManger.Callback() {
            @Override
            public void onSignSuccess(String sign_data) {
//                Log.e("sign===",sign_data);

                presenter.getBuyAir(SuperAcBuyActivity.this, mInputQuantityEt.getText().toString(), sign_data, mComputingPowerPriceTv.getText().toString(), sign_data + mComputingPowerPriceTv.getText().toString(), s);
            }

            @Override
            public void fail() {
            }
        }).pushAction(new Gson().toJson(new TransferEosMessageBean(memo, mAirdrop_address, price
                        , mainAccount)),
                unit,
                mainAccount);
    }

    //计算价格
    @Override
    public void getTotalPriceSuccess(int code, String msg_cn, SuperAcBuyNumBean bean) {
        hideProgress();
        pay = 1;
        if (code == 0) {
            mSuperAcBuyNumBean = bean;
            mTotalPaymentTv.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.pay_money).setScale(4).toPlainString()));
            mComputingPowerPriceTv.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.ngk_price).setScale(4).toPlainString()));
        } else {
            mSuperAcBuyNumBean = bean;
            mTotalPaymentTv.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.pay_money).setScale(4).toPlainString()));
            mComputingPowerPriceTv.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.ngk_price).setScale(4).toPlainString()));
            AlertDialogShowUtil.toastMessage(SuperAcBuyActivity.this, msg_cn);
        }

    }

    @Override
    public void getBuyAirSuccess(int code, String bean) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(SuperAcBuyActivity.this, bean);
        if (code == 0) {
            presenter.getBuyAirList(SuperAcBuyActivity.this, 1 + "");
            presenter.getBalance("USDS");
            presenter.getFirstData();
            mInputQuantityEt.setText("");
            mTotalPaymentTv.setText("0.0000");
        }
    }

    //获取余额
    @Override
    public void getBalanceSuccess(String symbol, String balance) {

        if ("USDS".equals(symbol)) {
            String s = new BigDecimal(balance).setScale(4, BigDecimal.ROUND_DOWN).toString();
            mBalanceTv.setText(s);
        }
    }

    @Override
    public void getBuyAirListSuccess(AirListBean bean) {
        hideProgress();
        if (PageNum == 1 && bean.comlist.size() == 0) {
            empty_data.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            return;
        } else {
            empty_data.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
        if (PageNum == 1) {
            mFragmentAdapter.setDatas(bean.comlist);
        } else {
            mFragmentAdapter.addDatas(bean.comlist);
        }
        mRecyclerView.setPullLoadMoreCompleted();
    }

    @Override
    public void onIssueFeeSuccess(IssueFeeBean bean) {
        mAirdrop_address = bean.airdrop_address;
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String bean) {
        hideProgress();


    }

    @Override
    public void activitySuccess(ActivityFirstBean activityFirstBean) {
        hideProgress();
        ngkPrice = activityFirstBean.getNgk_price();
        mSurplus_money = activityFirstBean.getUser_surplus_money();
        mIfopen = activityFirstBean.getIfopen();

        mAmountTv.setText(mSurplus_money);
        mComputingPowerPriceTv.setText(ngkPrice);
//        mIfopen = getIntent().getExtras().getString("mIfopen");
        if (!mIfopen.equals("2")) {
            mBuyTv.setText(getResources().getString(R.string.air_buy_button));
            mBuyTv.setOnClickListener(null);
            mBuyTv.setTextColor(getResources().getColor(R.color.color_898e93));
        } else {
            mBuyTv.setText(getResources().getString(R.string.buy));
        }
    }

    @Override
    public void activityFailure(String errorMsg) {

    }

    @Override
    public void onRefresh() {
        PageNum = 1;
        presenter.getBuyAirList(SuperAcBuyActivity.this, PageNum + "");
    }

    @Override
    public void onLoadMore() {
        PageNum++;
        presenter.getBuyAirList(SuperAcBuyActivity.this, PageNum + "");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
