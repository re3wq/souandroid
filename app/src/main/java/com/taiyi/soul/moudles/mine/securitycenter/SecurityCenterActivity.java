package com.taiyi.soul.moudles.mine.securitycenter;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arcsoft.face.ActiveFileInfo;
import com.arcsoft.face.ErrorInfo;
import com.arcsoft.face.FaceEngine;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.face.RegisterAndRecognizeActivity;
import com.taiyi.soul.moudles.login.bean.UserInfoBean;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.securitycenter.googleverification.GoogleVerificationActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateemail.BindEmailActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber.BindPhoneNumberActivity;
import com.taiyi.soul.utils.FingerprintUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SecurityCenterActivity extends BaseActivity<SecurityCenterView, SecurityCenterPresent> implements SecurityCenterView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.phoneNumberTv)
    TextView phoneNumberTv;
    @BindView(R.id.emailTv)
    TextView emailTv;
    @BindView(R.id.levelTv)
    TextView levelTv;
    @BindView(R.id.bindPhoneNumberTv)
    TextView bindPhoneNumberTv;
    @BindView(R.id.bindEmailTv)
    TextView bindEmailTv;
    @BindView(R.id.isOpenGoogleIv)
    ImageView isOpenGoogleIv;
    @BindView(R.id.starThreeIv)
    ImageView starThreeIv;
    @BindView(R.id.starFourIv)
    ImageView starFourIv;
    @BindView(R.id.starFiveIv)
    ImageView starFiveIv;
    @BindView(R.id.isOpenFingerprintIv)
    ImageView isOpenFingerprintIv;
    @BindView(R.id.isOpenFaceIv)
    ImageView isOpenFaceIv;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.top)
    ConstraintLayout top;
    private String isOpenGoogle;
    private int securityLevel = 2;
    private int type = 1;
    private int faceType = 1;
    private String replace;
    private String userInfo;
    private List<UserInfoBean> list;
    private String mainAccount;
    private boolean startFace;
    private boolean startFinger;
    private SafeKeyboard safeKeyboard;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_security_center;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public SecurityCenterPresent initPresenter() {
        return new SecurityCenterPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.security_center));
//        EventBus.getDefault().register(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    //显示密码框
    private void showPassword(View view) {
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        OrderPopupWindow pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);

        TextView forgot_password = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(SecurityCenterActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pay_popup.dismiss();
                            showProgress();
                            presenter.checkPayPassword(s);
                        }
                    }, 500);

                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }


    @Override
    protected void onResume() {
        super.onResume();
        isOpenGoogle = Utils.getSpUtils().getString(Constants.IS_OPEN_GOOGLE);
        userInfo = Utils.getSpUtils().getString("userInfo");
        list = new Gson().fromJson(userInfo, new TypeToken<List<UserInfoBean>>() {
        }.getType());
        mainAccount = Utils.getSpUtils().getString("face_account");


        for (UserInfoBean infoBean : list) {
            if (infoBean.getAccount().equals(mainAccount)) {
                startFace = infoBean.isStartFace();
                startFinger = infoBean.isStartFinger();
            }
        }
//        Log.e("start==", mainAccount + "   " + startFinger + "   " + startFace);
        if ("0".equals(isOpenGoogle) || "1".equals(isOpenGoogle)) {//未开启谷歌验证
            isOpenGoogleIv.setImageResource(R.mipmap.mine_safety_center_close_iv);
        } else {
            isOpenGoogleIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
        }


        String email = Utils.getSpUtils().getString("email");
        String phoneNumber = Utils.getSpUtils().getString("phoneNumber");
        if (!TextUtils.isEmpty(email)) {
            emailTv.setText(getString(R.string.update_email));
            bindEmailTv.setText(email);
        }
        if (!TextUtils.isEmpty(phoneNumber)) {
            phoneNumberTv.setText(getString(R.string.update_phonenumber));
            bindPhoneNumberTv.setText(phoneNumber);
        }

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(phoneNumber)) {
            securityLevel = securityLevel + 1;
        }
        if ("2".equals(isOpenGoogle)) {//开启谷歌验证
            securityLevel = securityLevel + 1;
        }

//        boolean isOpenFingerprint = Utils.getSpUtils().getBoolean(Constants.IS_FINGEREPRINT);//是否开启指纹
//        boolean face = Utils.getSpUtils().getBoolean(Constants.FACE);//是否开启人脸识别
//        if (isOpenFingerprint) {
//            isOpenFingerprintIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
//        }

        if (startFinger == true) {
            isOpenFingerprintIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
        }

        if (startFace == true) {
            isOpenFaceIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
        }

//        if (face) {
//            isOpenFaceIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
//        }

        if (startFinger == true || startFace == true) {
            securityLevel = securityLevel + 1;
        }
//        if (isOpenFingerprint || face) {
//            securityLevel = securityLevel + 1;
//        }
        if (securityLevel == 3) {
            levelTv.setText("3/5");
            progressBar.setProgress(60);
            starThreeIv.setImageResource(R.mipmap.star_sel_iv);
        } else if (securityLevel == 4) {
            levelTv.setText("4/5");
            progressBar.setProgress(80);
            starThreeIv.setImageResource(R.mipmap.star_sel_iv);
            starFourIv.setImageResource(R.mipmap.star_sel_iv);
        } else if (securityLevel == 5) {
            levelTv.setText("5/5");
            progressBar.setProgress(100);
            starThreeIv.setImageResource(R.mipmap.star_sel_iv);
            starFourIv.setImageResource(R.mipmap.star_sel_iv);
            starFiveIv.setImageResource(R.mipmap.star_sel_iv);
        }else {
            levelTv.setText("2/5");
            progressBar.setProgress(40);
        }

    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.bindPhoneNumberRl, R.id.isOpenFingerprintIv, R.id.isOpenFaceIv, R.id.isOpenGoogleIv, R.id.bindEmailRl, R.id.updateTransactionPwdRl, R.id.updateLoginPwdRl, R.id.fingerprintRl, R.id.googleVerificationRl})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.bindPhoneNumberRl:
                String phoneNumber = Utils.getSpUtils().getString("phoneNumber");
                Bundle bundle1 = new Bundle();
                bundle1.putInt("type", TextUtils.isEmpty(phoneNumber) ? 0 : 1);
                ActivityUtils.next(this, BindPhoneNumberActivity.class, bundle1);
                break;
            case R.id.bindEmailRl:
                String email = Utils.getSpUtils().getString("email");
                Bundle bundle3 = new Bundle();
                bundle3.putInt("type", TextUtils.isEmpty(email) ? 0 : 1);
                ActivityUtils.next(this, BindEmailActivity.class, bundle3);
                break;
            case R.id.updateTransactionPwdRl:
//                ActivityUtils.next(this, SetTransactionPwdActivity.class);
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(this, UpdateLoginPasswordActivity.class, bundle2);
                break;
            case R.id.updateLoginPwdRl:
                Bundle bundle = new Bundle();
                bundle.putInt("type", 1);
                ActivityUtils.next(this, UpdateLoginPasswordActivity.class, bundle);
                break;
            case R.id.googleVerificationRl:
                ActivityUtils.next(this, GoogleVerificationActivity.class);
                break;
            case R.id.fingerprintRl:
//                ActivityUtils.next(this, FingerprintEnterActivity.class);
                break;
            case R.id.isOpenGoogleIv://谷歌验证
                showProgress();
                presenter.openOrCloseGoogle();
                break;
            case R.id.isOpenFaceIv://人脸识别
//                boolean IS_FINGEREPRINT= Utils.getSpUtils().getBoolean(Constants.IS_FINGEREPRINT);//是否开启指纹
                for (UserInfoBean infoBean : list) {
                    if (infoBean.getAccount().equals(mainAccount)) {
                        startFinger = infoBean.isStartFinger();
                    }
                }
                if (startFinger == true) {
                    toast(getString(R.string.not_face));
                    return;
                }
                type = 2;
                showPassword(view);


//                ActivityUtils.next(this, RegisterAndRecognizeActivity.class);
//                AlertDialogShowUtil.toastMessage(this, "暂未开放");

                break;
            case R.id.isOpenFingerprintIv://指纹录入
//                boolean isRegisterFace = Utils.getSpUtils().getBoolean("isRegisterFace", false);//是否注册人脸
                for (UserInfoBean infoBean : list) {
                    if (infoBean.getAccount().equals(mainAccount)) {
                        startFace = infoBean.isStartFace();
                    }
                }
                if (startFace == true) {
                    toast(getString(R.string.not_finger));
                    return;
                }
                type = 1;
                showPassword(view);
                break;
        }
    }

    private static final String[] NEEDED_PERMISSIONS = new String[]{
            Manifest.permission.READ_PHONE_STATE
    };
    private static final int ACTION_REQUEST_PERMISSIONS = 0x001;
    private FaceEngine faceEngine = new FaceEngine();

    /**
     * 激活引擎
     *
     * @param view
     */
    public void activeEngine(final View view) {
        if (!checkPermissions(NEEDED_PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, NEEDED_PERMISSIONS, ACTION_REQUEST_PERMISSIONS);
            return;
        }
        if (view != null) {
            view.setClickable(false);
        }
        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                int activeCode = faceEngine.active(SecurityCenterActivity.this, Constants.APP_ID, Constants.SDK_KEY);
                emitter.onNext(activeCode);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Integer activeCode) {
                        if (activeCode == ErrorInfo.MOK) {//成功
                            Intent intent = new Intent(SecurityCenterActivity.this, RegisterAndRecognizeActivity.class);
                            intent.putExtra("page_type", 1);
                            startActivity(intent);
                        } else if (activeCode == ErrorInfo.MERR_ASF_ALREADY_ACTIVATED) {//已激活
                            Intent intent = new Intent(SecurityCenterActivity.this, RegisterAndRecognizeActivity.class);
                            intent.putExtra("page_type", 1);
                            startActivity(intent);
                        } else {
//                            Log.d("1111555", getString(R.string.fail) + "---" + activeCode + "");
                            toast(getString(R.string.fail) + activeCode);
                        }

                        if (view != null) {
                            view.setClickable(true);
                        }
                        ActiveFileInfo activeFileInfo = new ActiveFileInfo();
                        int res = faceEngine.getActiveFileInfo(SecurityCenterActivity.this, activeFileInfo);
                        if (res == ErrorInfo.MOK) {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        toast(getString(R.string.fail));
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private boolean checkPermissions(String[] neededPermissions) {
        if (neededPermissions == null || neededPermissions.length == 0) {
            return true;
        }
        boolean allGranted = true;
        for (String neededPermission : neededPermissions) {
            allGranted &= ContextCompat.checkSelfPermission(this, neededPermission) == PackageManager.PERMISSION_GRANTED;
        }
        return allGranted;
    }


    @Override
    public void onSuccess() {
        hideProgress();
        if ("1".equals(isOpenGoogle)) {
            isOpenGoogle = "2";
            Utils.getSpUtils().put(Constants.IS_OPEN_GOOGLE, "2");//0未绑定 1绑定未开启
            isOpenGoogleIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
        } else {
            isOpenGoogle = "1";
            Utils.getSpUtils().put(Constants.IS_OPEN_GOOGLE, "1");//0未绑定 1绑定未开启
            isOpenGoogleIv.setImageResource(R.mipmap.mine_safety_center_close_iv);
        }
    }

    @Override
    public void onFailure(String errorMsg) {
        Log.d("1111555", "-*-*-" + errorMsg);
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg) {
        Log.d("1111555", "-+-+-" + code + "---" + msg);
        hideProgress();
        if (code == 0) {
            for (UserInfoBean infoBean : list) {
                if (infoBean.getAccount().equals(mainAccount)) {
                    startFace = infoBean.isStartFace();
                    startFinger = infoBean.isStartFinger();
                }
            }
            //保存账号是否开启人脸
            if (type == 1) {
                if (FingerprintUtil.isSupport(this)) {
//                    boolean isOpenFingerprint = Utils.getSpUtils().getBoolean(Constants.IS_FINGEREPRINT);//是否开启指纹
                    if (startFinger == true) {
                        Utils.getSpUtils().put(Constants.IS_FINGEREPRINT, false);
                        isOpenFingerprintIv.setImageResource(R.mipmap.mine_safety_center_close_iv);
                        for (UserInfoBean infoBean : list) {
                            if (infoBean.getAccount().equals(mainAccount)) {
                                infoBean.setStartFinger(false);
                            }
                        }

                    } else {
                        Utils.getSpUtils().put(Constants.IS_FINGEREPRINT, true);
                        isOpenFingerprintIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
                        Utils.getSpUtils().put(Constants.FACE, false);//打开指纹，关闭人脸
                        isOpenFaceIv.setImageResource(R.mipmap.mine_safety_center_close_iv);
                        for (UserInfoBean infoBean : list) {
                            if (infoBean.getAccount().equals(mainAccount)) {
                                infoBean.setStartFinger(true);
                            }
                        }
                    }
                }

            } else if (type == 2) {
//                boolean FACE= Utils.getSpUtils().getBoolean(Constants.FACE);//是否开启人脸
                if (startFace == false) {
                    activeEngine(null);
                } else {
                    isOpenFaceIv.setImageResource(R.mipmap.mine_safety_center_close_iv);
                    Utils.getSpUtils().put("isRegisterFace", false);//是否注册人脸
                    for (UserInfoBean infoBean : list) {
                        if (infoBean.getAccount().equals(mainAccount)) {
                            infoBean.setStartFace(false);
                        }
                    }

                }
            }

            Utils.getSpUtils().put("userInfo", new Gson().toJson(list));
        } else {
            toast(msg);
        }
    }


//    @Subscribe
//    public void even(String event){
//        if("faceRegisterSuccess".equals(event)){
//                isOpenFaceIv.setImageResource(R.mipmap.mine_safety_center_open_iv);
//                Utils.getSpUtils().put("face_cuccess",1000);
//                userInfoBean.setStartFace(true);
//                Utils.getSpUtils().put("userInfo", new Gson().toJson(list));
//        }
//    }
}
