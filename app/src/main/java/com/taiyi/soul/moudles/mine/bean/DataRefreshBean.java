package com.taiyi.soul.moudles.mine.bean;

public class DataRefreshBean {
    String flag;
    String source;

    public DataRefreshBean(String flag, String source) {
        this.flag = flag;
        this.source = source;
    }

    public String getFlag() {
        return flag;
    }

    public String getSource() {
        return source;
    }
}
