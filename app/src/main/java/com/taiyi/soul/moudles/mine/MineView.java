package com.taiyi.soul.moudles.mine;

import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;
import com.taiyi.soul.moudles.mine.bean.UserInfoBean;

import java.util.List;

public interface MineView {
    void getUserInfoSuccess(UserInfoBean userInfoBean);
    void getUserInfoFailure(String errorMsg);
    void getUserAccountSuccess(String accountStr);
    void getBalanceSuccess(String symbol,String balance);
    void onCheckPayPasswordSuccess(int code,String msg);
    void getRateSuccess(String rate);
    void getRateFailure(String errorMsg);
    void getSelectCoinRateSuccess(List<SelectCoinRateBean>list);
    void getSelectCoinRateFailure(String errorMsg);
    void updateSuccess();
    void updateFailure(String errorMsg);
}
