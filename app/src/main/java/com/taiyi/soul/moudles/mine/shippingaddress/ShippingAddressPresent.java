package com.taiyi.soul.moudles.mine.shippingaddress;

import android.app.Activity;
import android.content.Intent;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.bean.AddressListBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class ShippingAddressPresent extends BasePresent<ShippingAddressView> {
    public void getAddressList(Activity activity,int page){
        Map<String,String>map=new HashMap<>();
        HttpUtils.postRequest(BaseUrl.GET_ADDRESS_LIST+"?pagenum="+page, this, map, new JsonCallback<BaseResultBean<AddressListBean>>() {
            @Override
            public BaseResultBean<AddressListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<AddressListBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().code == 0) {
                        view.onSuccess(response.body().data);
                    } else if (response.body().code == -1) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            activity.startActivity(new Intent(activity, LoginActivity.class));
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else {
                        view.onFailure(response.body().msg_cn);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<AddressListBean>> response) {
                super.onError(response);
            }
        });
    }
}
