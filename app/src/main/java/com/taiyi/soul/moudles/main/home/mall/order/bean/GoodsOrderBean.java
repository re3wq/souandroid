package com.taiyi.soul.moudles.main.home.mall.order.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\8\8 0008 13:55
 * @Author : yuan
 * @Describe ：
 */
public class GoodsOrderBean {

        public String return_type;
        public String createtime;
        public String isevaluate;
        public String expresspricez;
        public String flag;
        public String useraddress_id;
        public String orderid;
        public String remark;
        public String commodityid;
        public String commodityspecid;
        public String userid;
        public String sumyunfei;
        public String addressid;
        public String number;
        public String isshou;
        public String businessuserid;
        public String expressprice;
        public String paytype;
        public String ordertype;
        public String allmoney_ngk;
        public String allmoney_usdn;
        public String NGK_RATIO;
}
