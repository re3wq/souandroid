package com.taiyi.soul.moudles.main.home.mall.order.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\8\10 0010 10:54
 * @Author : yuan
 * @Describe ：
 */
public class OrderDetailsBean {

        public OrderBean order;

        public static class OrderBean {
            public String return_type;
            public String type;
            public String userid;
            public String classificationid;
            public String paycreatetime;
            public String provinceid;
            public String number;
            public String imgurl;
            public String zhuanyunfei;
            public String price;
            public String visitgoodsnum;
            public String xcday;
            public String stock;
            public String cashPrice;
            public String delflag;
            public String sumprice;
            public String ifex;
            public String cityid;
            public String countryid;
            public String addressid;
            public String payment_type;
            public String comcreatetime;
            public String name;
            public String addname;
            public String hyprice;
            public String countryName;
            public String combz;
            public String ordertype;
            public String isevaluate;
            public String flag;
            public String useraddress_id;
            public String remark;
            public String commodityid;
            public String salenum;
            public String commodityspecid;
            public String issh;
            public String zhuanprice;
            public String cityName;
            public String provincename;
            public String surname;
            public String businessuserid;
            public String shou_type;
            public String ordercommodityid;
            public String hybvprice;
            public String recommendflag;
            public String createtime;
            public String address;
            public String orderid;
            public String yunfei;
            public String addtel;
            public String typename;
            public String smallimgurl;
            public String ngk_ratio;
            public String paycoin;
    }
}
