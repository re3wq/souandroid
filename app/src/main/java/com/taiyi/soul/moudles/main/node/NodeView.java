package com.taiyi.soul.moudles.main.node;

import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.node.bean.NodeBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;

public interface NodeView {
    void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean);
    void getDataHttpFail(String errorMsg);

    void getComputerPriceSuccess(String price);
    void getComputerPriceFailure(String errorMsg);

    void getNodeDataSuccess(NodeBean nodeBean);
    void getNodeDataFailure(String errorMsg);

    void onIssueFeeSuccess(IssueFeeBean issueFeeBean);
    void getBalanceSuccess(String symbol,String balance);

    void getRateSuccess(String rate);
    void getRateFailure(String errorMsg);
}
