package com.taiyi.soul.moudles.register.bean;

import java.util.List;

public class EmptyBean {


    public int status;
    public String msg;
    public List<DataBean> data;

    public static class DataBean {
        public long id;
        public long parentId;
        public String account;
        public int sum;
        public List<ListBeanX> list;

        public static class ListBeanX {
            public long id;
            public long parentId;
            public String account;
            public int sum;
            public List<ListBean> list;

            public static class ListBean {
                public long id;
                public long parentId;
                public String account;
                public int sum;
                public List<?> list;
            }
        }
    }
}
