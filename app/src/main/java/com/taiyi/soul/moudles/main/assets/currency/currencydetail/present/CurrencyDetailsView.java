package com.taiyi.soul.moudles.main.assets.currency.currencydetail.present;

import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.CurrencyDetailsBean;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;

import java.util.List;

public interface CurrencyDetailsView {
    void getSuccess(int status, String msg, List<CurrencyDetailsBean> assetsBean);
    void getFailure(String errorMsg);
    void getSelectCoinRateSuccess(List<SelectCoinRateBean>list);
    void getSelectCoinRateFailure(String errorMsg);
    void getBalanceSuccess(String symbol,String balance);
    void getListSuccess(List<CurrencyListBean> list);
    void onFailure(String errorMsg);
    void getCoinListSuccess(List<CoinListBean> data);
}
