package com.taiyi.soul.moudles.main.home.options.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.home.options.bean.OptionDetailsListBean;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：期权
 */
public class OptionsDetailsFragmentAdaptert extends BaseRecyclerAdapter<OptionDetailsListBean.ComlistBean> {

    private PullRecyclerView pullRecyclerView;
    private Context context;

    public OptionsDetailsFragmentAdaptert(Context context) {
        this.context = context;
    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }


    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_options_position_two;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, OptionDetailsListBean.ComlistBean data) {

        ImageView mItemCoinImg = getView(holder, R.id.item_coin_img);
        TextView mItemCoinName = getView(holder, R.id.item_coin_name);
        TextView item_end_time = getView(holder, R.id.item_end_time);
        TextView mItemOrientation = getView(holder, R.id.item_orientation);
        TextView mItemBuyPrice = getView(holder, R.id.item_buy_price);
        TextView mItemCurrencyPrice = getView(holder, R.id.item_currency_price);
        TextView mItemMoney = getView(holder, R.id.item_money);
        TextView mItemProfit = getView(holder, R.id.item_profit);
        TextView money_unit = getView(holder, R.id.money_unit);
        TextView item_type = getView(holder, R.id.item_type);


        Glide.with(context).load(data.url).into(mItemCoinImg);
        mItemCoinName.setText(data.coin_name);
        money_unit.setText(data.coin_name);
        //持平时色值：#D8DADD
        //亏损时色值：#CC0628
        //盈利时色值：#04A387

        if (null == data.sign) {
            if (new BigDecimal(data.endprice).compareTo(new BigDecimal(data.buyprice)) == 1) {// 1大于 0等于  -1小于
                if (data.flag == 0) {//买涨
                    item_type.setText(context.getResources().getString(R.string.yingli) + " " + data.get_num + data.coin_name);
                    item_type.setTextColor(context.getResources().getColor(R.color.color_04a387));
                    mItemProfit.setText(data.get_num + "(" + new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2).toPlainString() + "%)");//汇率;
                    mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_03a387));
                } else {
                    item_type.setText(context.getResources().getString(R.string.kuisun) + " " + data.get_num + data.coin_name);
                    item_type.setTextColor(context.getResources().getColor(R.color.color_cc0628));
                    mItemProfit.setText(data.get_num + "(" + new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2).toPlainString() + "%)");//汇率;
                    mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_cc0628));
                }
            } else if (new BigDecimal(data.endprice).compareTo(new BigDecimal(data.buyprice)) == 0) {// 1大于 0等于  -1小于
                item_type.setText(context.getResources().getString(R.string.chiping) + " " + 0 + data.coin_name);
                item_type.setTextColor(context.getResources().getColor(R.color.color_d8dadd));
                mItemProfit.setText(0 + "(" + new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2).toPlainString() + "%)");//汇率;
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_d8dadd));
            } else if (new BigDecimal(data.endprice).compareTo(new BigDecimal(data.buyprice)) == -1) {// 1大于 0等于  -1小于
                if (data.flag == 1) {//买跌
                    item_type.setText(context.getResources().getString(R.string.yingli) + " " + data.get_num + data.coin_name);
                    item_type.setTextColor(context.getResources().getColor(R.color.color_04a387));
                    mItemProfit.setText(data.get_num + "(" + new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2).toPlainString() + "%)");//汇率;
                    mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_03a387));
                } else {
                    item_type.setText(context.getResources().getString(R.string.kuisun) + " " + data.get_num + data.coin_name);
                    item_type.setTextColor(context.getResources().getColor(R.color.color_cc0628));
                    mItemProfit.setText(data.get_num + "(" + new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2).toPlainString() + "%)");//汇率;
                    mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_cc0628));
                }
            }
        } else {
            if (data.sign == 0) {
//盈利
//            item_type.setText("盈利 "+data.get_num+data.coin_name);
                item_type.setText(context.getResources().getString(R.string.yingli) + " " + data.get_num + data.coin_name);
                item_type.setTextColor(context.getResources().getColor(R.color.color_04a387));
                String big = new BigDecimal(data.charge).multiply(new BigDecimal("100").setScale(2)).toPlainString();
                mItemProfit.setText(data.get_num + "(" + new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2).toPlainString() + "%)");//汇率;
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_03a387));
            } else if (data.sign == 1) {
//平
//            item_type.setText("持平 "+0+data.coin_name);
                item_type.setText(context.getResources().getString(R.string.chiping) + " " + 0 + data.coin_name);
                item_type.setTextColor(context.getResources().getColor(R.color.color_d8dadd));
                mItemProfit.setText(0 + "(" + new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2).toPlainString() + "%)");//汇率;
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_d8dadd));
            } else if (data.sign == 2) {
//亏损
                item_type.setText(context.getString(R.string.kuisun) + data.num + data.coin_name);
//                item_type.setText(context.getResources().getString(R.string.kuisun)+" "+data.get_num+data.coin_name);
                item_type.setTextColor(context.getResources().getColor(R.color.color_cc0628));
                mItemProfit.setText(data.get_num + "(" + new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2).toPlainString() + "%)");//汇率;
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_cc0628));
            }
        }

        if (data.flag == 0) {
            mItemOrientation.setText(context.getResources().getString(R.string.options_buy_rise));
            mItemOrientation.setTextColor(context.getResources().getColor(R.color.color_03a387));

        } else {
            mItemOrientation.setText(context.getResources().getString(R.string.options_buy_desc));
            mItemOrientation.setTextColor(context.getResources().getColor(R.color.color_cc0628));
        }

        item_end_time.setText(data.endtime2);
        mItemBuyPrice.setText(data.buyprice);//买入价
        mItemCurrencyPrice.setText(data.endprice);
        mItemMoney.setText(data.num);//金额

    }

    //期权列表item点击事件
    public interface OptionsItemClick {
        void onItemClick(int position);
    }
}



