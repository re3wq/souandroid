package com.taiyi.soul.moudles.mine.myteam;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.mine.bean.MyTeamBean;
import com.taiyi.soul.moudles.mine.bean.MyTeamPerformanceListBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class MyTeamPresent extends BasePresent<MyTeamView> {
    public void getMyTeamData(String payPwd) {
        String password= "";
        Map maps = new HashMap();
        maps.put("payPass", payPwd);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("payPass", password);
        HttpUtils.getRequets(BaseUrl.MY_TEAM, this, map, new JsonCallback<BaseResultBean<MyTeamBean>>() {
            @Override
            public BaseResultBean<MyTeamBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<MyTeamBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.onSuccess(response.body().data);
                    } else if (response.body().status == 602) {
                        ToastUtils.showShortToast(response.body().msg);
                        view.loginInvalid();
                    } else {
                        view.onFailure(response.body().msg);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<MyTeamBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getPerformance(int pageNo) {//获取业绩列表
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNum", pageNo);
            jsonObject.put("pageSize", 10);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpUtils.postRequest(BaseUrl.MY_TEAM_PERFORMANCE, this, jsonObject.toString(), new JsonCallback<BaseResultBean<MyTeamPerformanceListBean>>() {
            @Override
            public BaseResultBean<MyTeamPerformanceListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<MyTeamPerformanceListBean>> response) {
                super.onSuccess(response);
                if (null!=view){
                    if (response.body().status == 200) {
                        if (null != view)
                            view.getPerformanceSuccess(response.body().data);
                    } else if (response.body().status == 602) {
                        ToastUtils.showShortToast(response.body().msg);
                        view.loginInvalid();
                    } else {
                        view.onFailure(response.body().msg);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<MyTeamPerformanceListBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getTeam(String account,String invitationCode,int stop) {

        JSONObject jsonObject = new JSONObject();//EmptyBean
        try {
            jsonObject.put("account", account);
            jsonObject.put("invitationCode", invitationCode);
            jsonObject.put("stop", stop);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.MY_TEAM_TEAM_LIST, this, jsonObject.toString(), new JsonCallback<BaseResultBean<List<Object>>>() {
            @Override
            public BaseResultBean<List<Object>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<Object>>> response) {
                super.onSuccess(response);
                if (null!=view){
                    if (response.body().status == 200) {
                        if (view != null)
                            view.getTeamSuccess(response.body().status, response.body().msg, response.body().data);
                    } else if (response.body().status == 602) {
                        ToastUtils.showShortToast(response.body().msg);
                        view.loginInvalid();
                    } else {
                        view.onFailure(response.body().msg);
                    }

                }


            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<Object>>> response) {
                super.onError(response);
            }
        });
    }
}
