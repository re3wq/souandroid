package com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.Response;

public class BindPhoneNumberPresent extends BasePresent<BindPhoneNumberView> {

    public void getVerificationCode(String areaCode,String account) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areaCode",areaCode);
            jsonObject.put("mainAccount", account);
            jsonObject.put("type", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.REGISTER_GET_VERIFICATION_CODE, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getVerificationCodeSuccess();
                    } else {
                        view.getInvitePersonFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }



public void bindPhoneNumberOrEmail(String areaCode,String mainAccount,String verifyCode,String payPass){
    JSONObject jsonObject = new JSONObject();
    try {
        jsonObject.put("payPass",payPass);
        jsonObject.put("areaCode",areaCode);
        jsonObject.put("mainAccount",mainAccount);
        jsonObject.put("type",1);
        jsonObject.put("verifyCode",verifyCode);
    } catch (JSONException e) {
        e.printStackTrace();
    }
    HttpUtils.postRequest(BaseUrl.BING_PHONE_NUMBER_OR_EMAIL, this, jsonObject.toString(), new JsonCallback<BaseResultBean<String>>() {
        @Override
        public BaseResultBean<String> convertResponse(Response response) throws Throwable {
            return super.convertResponse(response);
        }

        @Override
        public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
            super.onSuccess(response);
            if (null != view) {
                if (response.body().status == 200) {
                    view.updateSuccess(response.body().msg);
                } else {
                    view.updateFailure(response.body().msg);
                }
            }
        }

        @Override
        public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
            super.onError(response);
        }
    });
}


    public void getAreaCode(){
        HttpUtils.getRequets(BaseUrl.GET_AREA_CODE, this, null, new JsonCallback<BaseResultBean<List<AreaBean>>>() {
            @Override
            public BaseResultBean<List<AreaBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onSuccess(response);
                if (null!=view) {
                if (response.body().status==200){
                    view.getAreaCodeSuccess(response.body().data);
                }
            }}

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onError(response);
            }
        });
    }
}
