package com.taiyi.soul.moudles.mine.message;

import com.taiyi.soul.moudles.mine.message.bean.MessageBean;

public interface MessageView {
    void getMessageSuccess(MessageBean messageBean);

    void getMessageFailure(String errorMsg);

//    void updateStateSuccess();

    void onSuccess();
    void onFailure(String errorMsg);
}
