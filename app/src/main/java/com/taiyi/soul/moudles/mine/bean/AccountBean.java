package com.taiyi.soul.moudles.mine.bean;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/7/3
 * Time: 3:50 PM
 */
public class AccountBean {

    private String Account;
    private String Permission;


    public AccountBean() {
    }

    public AccountBean(String account, String permission) {
        this.Account = account;
        this.Permission = permission;
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String account) {
        this.Account = account;
    }

    public String getPermission() {
        return Permission;
    }

    public void setPermission(String permission) {
        this.Permission = permission;
    }

}
