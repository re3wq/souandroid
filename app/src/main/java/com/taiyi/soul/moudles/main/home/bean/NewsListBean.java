package com.taiyi.soul.moudles.main.home.bean;

import java.util.List;

public class NewsListBean {

    /**
     * pageVO : {"total":7,"list":[{"id":6,"title":"新闻资讯6","outline":"end2","conent":"end2","url":"www.123465","createTime":"2020-07-06 15:23:29","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":5,"title":"新闻资讯5","outline":"end1","conent":"end1","url":"www.987","createTime":"2020-07-06 15:23:18","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":0,"langu":"zh","num":0},{"id":4,"title":"新闻资讯4","outline":"banner","conent":"banner2","url":"www.999","createTime":"2020-07-06 15:22:10","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":3,"title":"新闻资讯3","outline":"banner","conent":"banner1","url":"www.777","createTime":"2020-07-06 15:21:31","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":0,"langu":"zh","num":0},{"id":2,"title":"新闻资讯2","outline":"list1","conent":"列表2","url":"www.456","createTime":"2020-07-06 15:20:41","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":1,"title":"新闻资讯1","outline":"list","conent":"列表1","url":"www.123","createTime":"2020-07-06 15:17:51","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":0,"langu":"zh","num":2},{"id":7,"title":"新闻资讯7","outline":"7","conent":"7","url":"www.1111","createTime":"1970-01-01 00:00:00","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0}]}
     * bannerNews : [{"id":6,"title":"新闻资讯6","outline":"end2","conent":"end2","url":"www.123465","createTime":"2020-07-06 15:23:29","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":4,"title":"新闻资讯4","outline":"banner","conent":"banner2","url":"www.999","createTime":"2020-07-06 15:22:10","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":2,"title":"新闻资讯2","outline":"list1","conent":"列表2","url":"www.456","createTime":"2020-07-06 15:20:41","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":7,"title":"新闻资讯7","outline":"7","conent":"7","url":"www.1111","createTime":"1970-01-01 00:00:00","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0}]
     */

    private PageVOBean pageVO;
    private List<BannerNewsBean> bannerNews;

    public PageVOBean getPageVO() {
        return pageVO;
    }

    public void setPageVO(PageVOBean pageVO) {
        this.pageVO = pageVO;
    }

    public List<BannerNewsBean> getBannerNews() {
        return bannerNews;
    }

    public void setBannerNews(List<BannerNewsBean> bannerNews) {
        this.bannerNews = bannerNews;
    }

    public static class PageVOBean {
        /**
         * total : 7
         * list : [{"id":6,"title":"新闻资讯6","outline":"end2","conent":"end2","url":"www.123465","createTime":"2020-07-06 15:23:29","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":5,"title":"新闻资讯5","outline":"end1","conent":"end1","url":"www.987","createTime":"2020-07-06 15:23:18","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":0,"langu":"zh","num":0},{"id":4,"title":"新闻资讯4","outline":"banner","conent":"banner2","url":"www.999","createTime":"2020-07-06 15:22:10","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":3,"title":"新闻资讯3","outline":"banner","conent":"banner1","url":"www.777","createTime":"2020-07-06 15:21:31","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":0,"langu":"zh","num":0},{"id":2,"title":"新闻资讯2","outline":"list1","conent":"列表2","url":"www.456","createTime":"2020-07-06 15:20:41","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0},{"id":1,"title":"新闻资讯1","outline":"list","conent":"列表1","url":"www.123","createTime":"2020-07-06 15:17:51","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":0,"langu":"zh","num":2},{"id":7,"title":"新闻资讯7","outline":"7","conent":"7","url":"www.1111","createTime":"1970-01-01 00:00:00","pictureTop":"1","pictureDetails":"2","pictureList":"3","isShow":1,"langu":"zh","num":0}]
         */

        private int total;
        private List<ListBean> list;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : 6
             * title : 新闻资讯6
             * outline : end2
             * conent : end2
             * url : www.123465
             * createTime : 2020-07-06 15:23:29
             * pictureTop : 1
             * pictureDetails : 2
             * pictureList : 3
             * isShow : 1
             * langu : zh
             * num : 0
             */

            private int id;
            private String title;
            private String outline;
            private String conent;
            private String url;
            private String createTime;
            private String pictureTop;
            private String pictureDetails;
            private String pictureList;
            private int isShow;
            private String langu;
            private int num;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getOutline() {
                return outline;
            }

            public void setOutline(String outline) {
                this.outline = outline;
            }

            public String getConent() {
                return conent;
            }

            public void setConent(String conent) {
                this.conent = conent;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getPictureTop() {
                return pictureTop;
            }

            public void setPictureTop(String pictureTop) {
                this.pictureTop = pictureTop;
            }

            public String getPictureDetails() {
                return pictureDetails;
            }

            public void setPictureDetails(String pictureDetails) {
                this.pictureDetails = pictureDetails;
            }

            public String getPictureList() {
                return pictureList;
            }

            public void setPictureList(String pictureList) {
                this.pictureList = pictureList;
            }

            public int getIsShow() {
                return isShow;
            }

            public void setIsShow(int isShow) {
                this.isShow = isShow;
            }

            public String getLangu() {
                return langu;
            }

            public void setLangu(String langu) {
                this.langu = langu;
            }

            public int getNum() {
                return num;
            }

            public void setNum(int num) {
                this.num = num;
            }
        }
    }

    public static class BannerNewsBean {
        /**
         * id : 6
         * title : 新闻资讯6
         * outline : end2
         * conent : end2
         * url : www.123465
         * createTime : 2020-07-06 15:23:29
         * pictureTop : 1
         * pictureDetails : 2
         * pictureList : 3
         * isShow : 1
         * langu : zh
         * num : 0
         */

        private int id;
        private String title;
        private String outline;
        private String conent;
        private String url;
        private String createTime;
        private String pictureTop;
        private String pictureDetails;
        private String pictureList;
        private int isShow;
        private String langu;
        private int num;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getOutline() {
            return outline;
        }

        public void setOutline(String outline) {
            this.outline = outline;
        }

        public String getConent() {
            return conent;
        }

        public void setConent(String conent) {
            this.conent = conent;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getPictureTop() {
            return pictureTop;
        }

        public void setPictureTop(String pictureTop) {
            this.pictureTop = pictureTop;
        }

        public String getPictureDetails() {
            return pictureDetails;
        }

        public void setPictureDetails(String pictureDetails) {
            this.pictureDetails = pictureDetails;
        }

        public String getPictureList() {
            return pictureList;
        }

        public void setPictureList(String pictureList) {
            this.pictureList = pictureList;
        }

        public int getIsShow() {
            return isShow;
        }

        public void setIsShow(int isShow) {
            this.isShow = isShow;
        }

        public String getLangu() {
            return langu;
        }

        public void setLangu(String langu) {
            this.langu = langu;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }
}
