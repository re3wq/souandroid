package com.taiyi.soul.moudles.main.home.mall.order;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.DemoOrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderDetailsBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderListPay;
import com.taiyi.soul.moudles.main.home.mall.order.present.OrderDetailsPresent;
import com.taiyi.soul.moudles.main.home.mall.order.present.OrderDetailsView;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.shippingaddress.ShippingAddressActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;

import org.greenrobot.eventbus.EventBus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class OrderDetailActivity extends BaseActivity<OrderDetailsView, OrderDetailsPresent> implements OrderDetailsView {


    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.order_state)
    TextView mOrderState;
    @BindView(R.id.order_name)
    TextView mOrderName;
    @BindView(R.id.order_phone)
    TextView mOrderPhone;
    @BindView(R.id.order_address)
    TextView mOrderAddress;
    @BindView(R.id.goods_recycle)
    RecyclerView mGoodsRecycle;
    @BindView(R.id.goods_change_address)
    TextView mGoodsChangeAddress;
    @BindView(R.id.goods_cancel_order)
    TextView mGoodsCancelOrder;
    @BindView(R.id.goods_pay)
    TextView mGoodsPay;
    @BindView(R.id.goods_remind_shipment)
    TextView mGoodsRemindShipment;
    @BindView(R.id.goods_confirm_receipt)
    TextView mGoodsConfirmReceipt;
    @BindView(R.id.goods_confirm_finish)
    TextView mGoodsConfirmFinish;
    @BindView(R.id.order_item)
    LinearLayout mOrderItem;
    @BindView(R.id.order_price)
    TextView mOrderPrice;
    @BindView(R.id.order_freight)
    TextView mOrderFreight;
    @BindView(R.id.order_total)
    TextView mOrderTotal;
    @BindView(R.id.order_num)
    TextView mOrderNum;
    @BindView(R.id.order_time)
    TextView mOrderTime;
    @BindView(R.id.order_pay_type)
    TextView order_pay_type;
    @BindView(R.id.ll_order_pay_time)
    LinearLayout ll_order_pay_time;
    @BindView(R.id.ll_pay_type)
    LinearLayout ll_pay_type;
    @BindView(R.id.order_pay_time)
    TextView mOrderPayTime;
    private String mOrdercommodityid;
    private int mOrderType;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_order_detail;
    }

    @Override
    public OrderDetailsPresent initPresenter() {
        return new OrderDetailsPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        Bundle bundle = getIntent().getExtras();
        mOrderType = bundle.getInt("orderType");
        mOrdercommodityid = bundle.getString("ordercommodityid");

        showProgress();
        presenter.getOrderDetails(this, mOrdercommodityid);

        mTvTitle.setText(getString(R.string.order_detail_title));
        if (mOrderType == 1) {//待付款
            mOrderState.setText(getString(R.string.order_details_type_payment));
            mGoodsPay.setVisibility(View.VISIBLE);
            mGoodsChangeAddress.setVisibility(View.VISIBLE);
            mGoodsCancelOrder.setVisibility(View.VISIBLE);
            ll_pay_type.setVisibility(View.GONE);
        } else if (mOrderType == 2) {//待发货
            mOrderState.setText(getString(R.string.order_details_type_delivered));
            mGoodsRemindShipment.setVisibility(View.VISIBLE);
        } else if (mOrderType == 3) {//待收货
            mOrderState.setText(getString(R.string.order_details_type_received));
            mGoodsConfirmReceipt.setVisibility(View.VISIBLE);
        } else if (mOrderType == 4) {
            mOrderState.setText(getString(R.string.order_to_be_finish));
            mGoodsConfirmFinish.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void initEvent() {

    }


    @OnClick({R.id.iv_back, R.id.goods_change_address, R.id.goods_cancel_order, R.id.goods_pay, R.id.goods_remind_shipment, R.id.goods_confirm_receipt, R.id.goods_confirm_finish})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.goods_change_address:
                Intent intent = new Intent(OrderDetailActivity.this, ShippingAddressActivity.class);
                intent.putExtra("type", 1);
                startActivityForResult(intent, 1001);
                break;
            case R.id.goods_cancel_order:
                View view = LayoutInflater.from(OrderDetailActivity.this).inflate(R.layout.popup_cancel, null);
                OrderPopupWindow orderPopupWindow = new OrderPopupWindow(OrderDetailActivity.this, view);

                ((TextView) view.findViewById(R.id.consider)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderPopupWindow.dismiss();
                    }
                });
                ((TextView) view.findViewById(R.id.sure)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderPopupWindow.dismiss();
                        presenter.getCancelOrder(OrderDetailActivity.this, mOrdercommodityid);
                    }
                });
                orderPopupWindow.show(v, OrderDetailActivity.this.getWindow(), 0);
                break;
            case R.id.goods_pay:
                presenter.getZhiFuLie(this, mOrdercommodityid);
            case R.id.goods_remind_shipment:
                presenter.getRemindDelivery(OrderDetailActivity.this, mOrdercommodityid);
            case R.id.goods_confirm_receipt:
                showProgress();
                presenter.getTakeDeliveryOrder(OrderDetailActivity.this, mOrdercommodityid);
            case R.id.goods_confirm_finish:
//                Toast.makeText(OrderDetailActivity.this,"123",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            if (null != data) {
                String addressId = data.getStringExtra("addressId");
                presenter.getEditOrderAddress(OrderDetailActivity.this, mOrdercommodityid, addressId);
            }
        }
    }


    @Override
    public void onGetOrderDetailsSuccess(OrderDetailsBean bean) {
  hideProgress();
        mOrderName.setText(bean.order.surname + bean.order.addname);//联系人
        mOrderPhone.setText(bean.order.addtel);//联系人电话
        mOrderAddress.setText(bean.order.countryName + bean.order.provincename + bean.order.cityName + bean.order.address);//地址
        mOrderPrice.setText(bean.order.sumprice + " USDS");//商品总价
        mOrderFreight.setText(bean.order.yunfei);//运费

        mOrderNum.setText(bean.order.ordercommodityid);//订单编号
        mOrderTime.setText(bean.order.createtime);//下单时间


        BigDecimal sumprice = new BigDecimal(bean.order.sumprice);
        BigDecimal yunfei = new BigDecimal(bean.order.yunfei);
        BigDecimal ngk_ratio = new BigDecimal(bean.order.ngk_ratio);
        BigDecimal bigDecimal = sumprice.add(yunfei);

        if(mOrderType==1){
            ll_order_pay_time.setVisibility(View.GONE);//支付方式
            mOrderPayTime.setVisibility(View.GONE);//支付时间
            mOrderTotal.setText(bigDecimal + " USDS");//合计价.setText(bigDecimal1+" USDS");
        }else {
            ll_order_pay_time.setVisibility(View.VISIBLE);
            order_pay_type.setText(bean.order.paycoin);
            mOrderPayTime.setVisibility(View.VISIBLE);
            mOrderPayTime.setText(bean.order.paycreatetime);//支付时间
            if (bean.order.paycoin.equals("SOU")) {
                BigDecimal bigDecimal1 = bigDecimal.multiply(ngk_ratio).setScale(4, BigDecimal.ROUND_UP);
                mOrderTotal.setText(bigDecimal1 + " SOU");//合计价.setText(bigDecimal1+" USDS");
            } else {
                mOrderTotal.setText(bigDecimal + " USDS");
            }
        }


        List<DemoOrderBean.GoodBean> beans = new ArrayList<>();
        DemoOrderBean.GoodBean goods = new DemoOrderBean.GoodBean();
        goods.img = bean.order.smallimgurl;
        goods.name = bean.order.name;
        goods.price = bean.order.hyprice;
        goods.num = bean.order.number;
        goods.specification = bean.order.typename;
        beans.add(goods);

        mGoodsRecycle.setLayoutManager(new LinearLayoutManager(OrderDetailActivity.this));
        CommonAdapter dealSellAdapter = AdapterManger.getOrderGoodsAdapter(OrderDetailActivity.this, beans, null);
        mGoodsRecycle.setAdapter(dealSellAdapter);
    }

    @Override
    public void onCancelOrderSuccess(String bean) {//取消订单
        hideProgress();
        View view = LayoutInflater.from(OrderDetailActivity.this).inflate(R.layout.popup_cancel_finish, null);
        OrderPopupWindow cancel_popu = new OrderPopupWindow(OrderDetailActivity.this, view);
        cancel_popu.show(view, OrderDetailActivity.this.getWindow(), 0);
        android.os.Handler hander = new android.os.Handler();
        hander.postDelayed(new Runnable() {
            @Override
            public void run() {
                cancel_popu.dismiss();
                finish();
                EventBus.getDefault().post("order_refresh");
            }
        }, 1500);
    }

    @Override
    public void onTakeDeliverySuccess(String bean) {//确认收货
        hideProgress();
        AlertDialogShowUtil.toastMessage(OrderDetailActivity.this, bean);
        finish();
        EventBus.getDefault().post("order_refresh");
    }

    @Override
    public void onEditAddressSuccess(String bean) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(OrderDetailActivity.this, bean);
    }

    @Override
    public void onRemindDeliverySuccess(String bean) {
        hideProgress();
    }

    @Override
    public void onZhiFuLieSuccess(OrderListPay bean) {
        hideProgress();
        Bundle bundle = new Bundle();
        bundle.putString("allmoney_ngk", bean.allmoney_ngk);
        bundle.putString("allmoney_usdn", bean.allmoney_usdn);
        bundle.putString("NGK_RATIO", bean.ngk_ratio);
        bundle.putString("orderid", bean.orderid);
        ActivityUtils.next(this, CheckPayActivity.class, bundle, true);
    }

    @Override
    public void onFailure() {
        hideProgress();
    }

}
