package com.taiyi.soul.moudles.mine.securitycenter.googleverification;

import com.taiyi.soul.moudles.mine.securitycenter.bean.GoogleSecretBean;

public interface GoogleVerificationView {
    void getGoogleSecretSuccess(GoogleSecretBean secret);
    void getGoogleSecretFailure(String errorMsg);
    void getVerificationCodeSuccess();
    void getInvitePersonFailure(String errorMsg);
    void bindSuccess();

}
