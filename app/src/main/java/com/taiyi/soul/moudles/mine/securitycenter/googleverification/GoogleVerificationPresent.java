package com.taiyi.soul.moudles.mine.securitycenter.googleverification;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.securitycenter.bean.GoogleSecretBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Response;

public class GoogleVerificationPresent extends BasePresent<GoogleVerificationView> {
    public void getGoogleSecret(Activity activity) {
        HttpUtils.postRequest(BaseUrl.GET_GOOGLE_SECRET, this, "", new JsonCallback<BaseResultBean<GoogleSecretBean>>() {
            @Override
            public BaseResultBean<GoogleSecretBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<GoogleSecretBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().code == 0) {
                        view.getGoogleSecretSuccess(response.body().data);
                    } else if (response.body().status == 602) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else {
                        view.getGoogleSecretFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<GoogleSecretBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getVerificationCode(Activity activity, String areaCode, String account) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areaCode", areaCode);
            jsonObject.put("mainAccount", account);
            jsonObject.put("type", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.REGISTER_GET_VERIFICATION_CODE, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                if (response.body().status == 200) {
                    view.getVerificationCodeSuccess();
                } else if (response.body().status == 602) {//登录失效
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                } else {
                    view.getInvitePersonFailure(response.body().msg);
                }
            }}

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

    public void bindGoogleVerification(Activity activity, String secret, String smscode, String newcode, String type) {
        int loginType = Utils.getSpUtils().getInt("loginType");
        String account="";
        if (loginType==0){//手机号
            account=Utils.getSpUtils().getString("phoneNumber");
        }else {
            account=Utils.getSpUtils().getString("email");
        }
        HttpUtils.postRequest(BaseUrl.BIND_GOOGLE_VERIFICATION + "?secret=" + secret + "&smscode=" + smscode +"&account="+account+ "&newcode=" + newcode + "&type=" + type, this, "", new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().code == 0) {
                        view.bindSuccess();
                    } else if (response.body().status == 602) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else {
                        view.getGoogleSecretFailure(response.body().msg_cn);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }


}
