package com.taiyi.soul.moudles.main.home.options.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.home.options.bean.OptionDetailsListBean;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import androidx.core.widget.ContentLoadingProgressBar;

import java.math.BigDecimal;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：期权
 */
public class OptionsDetailsFragmentAdapter extends BaseRecyclerAdapter<OptionDetailsListBean.ComlistBean> {


    private PullRecyclerView pullRecyclerView;
    private Context context;

    public OptionsDetailsFragmentAdapter(Context context) {
        this.context = context;
    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }


    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_options_position;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, OptionDetailsListBean.ComlistBean data) {
        ImageView mItemCoinImg = getView(holder, R.id.item_coin_img);
        TextView mItemCoinName = getView(holder, R.id.item_coin_name);
        TextView  mItemOrientation= getView(holder, R.id.item_orientation);
        TextView mItemBuyPrice = getView(holder, R.id.item_buy_price);
        TextView mItemCurrencyPrice = getView(holder, R.id.item_currency_price);
        TextView mItemMoney = getView(holder, R.id.item_money);
        TextView mItemProfit = getView(holder, R.id.item_profit);
        ContentLoadingProgressBar mItemProgress = getView(holder, R.id.item_progress);
        TextView mItemCurrencyTime= getView(holder, R.id.item_currency_time);
        TextView mItemAllTime= getView(holder, R.id.item_all_time);
        TextView money_unit = getView(holder, R.id.money_unit);



        Glide.with(context).load(data.url).into(mItemCoinImg);
        mItemCoinName.setText(data.coin_name);
        money_unit.setText(data.coin_name);
        BigDecimal bigDecimal = new BigDecimal(data.buyprice);
        BigDecimal bigDecimal1 = new BigDecimal(data.price_now);
//        Log.e("ttt===",bigDecimal+"    "+bigDecimal1);
        if (data.flag == 0) {
            mItemOrientation.setText(context.getResources().getString(R.string.options_buy_rise));
            mItemOrientation.setTextColor(context.getResources().getColor(R.color.color_03a387));

            if (bigDecimal.compareTo(bigDecimal1)==-1) {//小于 买入价 小于 当前价
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_03a387));
            }else if(bigDecimal.compareTo(bigDecimal1)==0){//等于 买入价 等于 当前价
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.white));
            }else if(bigDecimal.compareTo(bigDecimal1)==1){//大于 买入价 大于 当前价
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_cc0628));
            }

        } else {
            mItemOrientation.setText(context.getResources().getString(R.string.options_buy_desc));
            mItemOrientation.setTextColor(context.getResources().getColor(R.color.color_cc0628));
            mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_cc0628));

            if (bigDecimal.compareTo(bigDecimal1)==-1) {//小于 买入价 小于 当前价
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_cc0628));
            }else if(bigDecimal.compareTo(bigDecimal1)==0){//等于 买入价 等于 当前价
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.white));
            }else if(bigDecimal.compareTo(bigDecimal1)==1){//大于 买入价 大于 当前价
                mItemCurrencyPrice.setTextColor(context.getResources().getColor(R.color.color_03a387));
            }

        }
        mItemAllTime.setText("/ "+data.time+"s");
        mItemCurrencyTime.setText(data.spendtime+"s");
        mItemProgress.setProgress(data.spendtime);
        mItemProgress.setMax(data.time);

        mItemBuyPrice.setText(data.buyprice);//买入价
        mItemCurrencyPrice.setText(data.price_now);//当前价
        mItemMoney.setText(data.num);//金额
        mItemProfit.setText(data.get_num+"("+new BigDecimal(data.charge).multiply(new BigDecimal("100")).setScale(2)+"%)");//汇率;
    }

    //期权列表item点击事件
    public interface OptionsItemClick {
        void onItemClick(int position);
    }
}



