package com.taiyi.soul.moudles.main.node.bean;

import java.util.List;

public class NodeBean {

    /**
     * list : [{"createTime":"07-01-2020","price":1},{"createTime":"06-30-2020","price":3},{"createTime":"06-29-2020","price":2},{"createTime":"06-28-2020","price":1},{"createTime":"06-27-2020","price":1.5},{"createTime":"06-26-2020","price":2}]
     * level : 0
     * hashrate : 0
     */

    private int level;
    private String hashrate;
    private List<ListBean> list;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getHashrate() {
        return hashrate;
    }

    public void setHashrate(String hashrate) {
        this.hashrate = hashrate;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * createTime : 07-01-2020
         * price : 1
         */

        private String createTime;
        private String price;

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
}
