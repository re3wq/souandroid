package com.taiyi.soul.moudles.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.assets.AssetsFragment;
import com.taiyi.soul.moudles.main.deal.DealFragment;
import com.taiyi.soul.moudles.main.home.HomeFragment;
import com.taiyi.soul.moudles.main.node.NodeFragment;
import com.taiyi.soul.moudles.mine.MineFragment;
import com.taiyi.soul.moudles.mine.bean.VersionUpdateBean;
import com.taiyi.soul.utils.ToastUtils;

import butterknife.BindView;

public class MainActivity extends BaseActivity<MainView, MainPresent> implements MainView, View.OnClickListener {

    private long exitTime = 0;

    @BindView(R.id.rg_bottom_menu)
    RadioGroup mRgBottomMenu;

 @BindView(R.id.iv_home1)
 RadioButton iv_home1;

 @BindView(R.id.iv_assets1)
 RadioButton iv_assets1;

 @BindView(R.id.iv_node1)
 RadioButton iv_node1;

 @BindView(R.id.iv_transaction1)
 RadioButton iv_transaction1;

 @BindView(R.id.iv_mine1)
 RadioButton iv_mine1;


    //页面的集合以及下标
    private Fragment[] mFragments1;
    private int currentindex;

    TextView skipTv, nowUpdateTv, forceUpdateTv, updateContentTv;
    private String downloadUrl;
    private Dialog dialog;
    private int startact_bind = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        initRadioButton();
        initFragment1();
        dialog = new Dialog(this, R.style.MyDialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        View view = LayoutInflater.from(this).inflate(R.layout.version_update_dialog_layout, null);
        updateContentTv = view.findViewById(R.id.updateContentTv);
        skipTv = view.findViewById(R.id.skipTv);
        skipTv.setOnClickListener(this);
        nowUpdateTv = view.findViewById(R.id.nowUpdateTv);
        nowUpdateTv.setOnClickListener(this);
        forceUpdateTv = view.findViewById(R.id.forceUpdateTv);
        forceUpdateTv.setOnClickListener(this);
        dialog.setContentView(view);


    }

    private int  size = 80;
    private void initRadioButton() {
        //定义底部标签图片大小和位置
        Drawable drawable_news = getResources().getDrawable(R.drawable.first_home_drawable);
        //当这个图片被绘制时，给他绑定一个矩形 ltrb规定这个矩形
        drawable_news.setBounds(0, 0, size, size);
        //设置图片在文字的哪个方向
        iv_home1.setCompoundDrawables(null, drawable_news, null, null);

        //定义底部标签图片大小和位置
        Drawable drawable_news1= getResources().getDrawable(R.drawable.first_node_drawable);
        //当这个图片被绘制时，给他绑定一个矩形 ltrb规定这个矩形
        drawable_news1.setBounds(0, 0, size, size);
        //设置图片在文字的哪个方向
        iv_assets1.setCompoundDrawables(null, drawable_news1, null, null);

        //定义底部标签图片大小和位置
        Drawable drawable_news2 = getResources().getDrawable(R.drawable.first_assets_drawable);
        //当这个图片被绘制时，给他绑定一个矩形 ltrb规定这个矩形
        drawable_news2.setBounds(0, 0, size, size);
        //设置图片在文字的哪个方向
        iv_node1.setCompoundDrawables(null, drawable_news2, null, null);


        //定义底部标签图片大小和位置
        Drawable drawable_news3 = getResources().getDrawable(R.drawable.first_transaction_drawable);
        //当这个图片被绘制时，给他绑定一个矩形 ltrb规定这个矩形
        drawable_news3.setBounds(0, 0, size, size);
        //设置图片在文字的哪个方向
        iv_transaction1.setCompoundDrawables(null, drawable_news3, null, null);


        //定义底部标签图片大小和位置
        Drawable drawable_news4 = getResources().getDrawable(R.drawable.first_mine_drawable);
        //当这个图片被绘制时，给他绑定一个矩形 ltrb规定这个矩形
        drawable_news4.setBounds(0, 0, size, size);
        //设置图片在文字的哪个方向
        iv_mine1.setCompoundDrawables(null, drawable_news4, null, null);

    }

    private void initFragment1() {


        mFragments1 = new Fragment[]{
                //首页/出借/发现/我的
                new HomeFragment(),
                new AssetsFragment(),
                new NodeFragment(),
                new DealFragment(),
                new MineFragment()
        };
        setIndexSelected(1);
        if(null!= getIntent().getExtras()){
            startact_bind = getIntent().getExtras().getInt("startact_bind");
            mRgBottomMenu.check(mRgBottomMenu.getChildAt(startact_bind).getId());
        }
        //添加首页
        setIndexSelected(startact_bind);
        //RadioGroup选中事件监听 改变fragment
        mRgBottomMenu.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.iv_home1:
                        setIndexSelected(0);
                        break;
                    case R.id.iv_assets1:
                        setIndexSelected(1);
                        break;
                    case R.id.iv_node1:
                        setIndexSelected(2);
                        break;
                    case R.id.iv_transaction1:
                        setIndexSelected(3);
                        break;
                    case R.id.iv_mine1:
                        setIndexSelected(4);
                        break;
                }
            }
        });

    }


    //设置Fragment页面
    private void setIndexSelected(int index) {

        if (currentindex == index) {
            return;
        }
        //开启事务
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //隐藏当前Fragment
        ft.hide(mFragments1[currentindex]);
        //判断Fragment是否已经添加
        if (!mFragments1[index].isAdded()) {
            ft.add(R.id.framelayout, mFragments1[index]).show(mFragments1[index]);
        } else {
            //显示新的Fragment
            ft.show(mFragments1[index]);
        }
        ft.commit();
        currentindex = index;
    }

    @Override
    protected void initData() {
        presenter.checkVersionUpdate();
    }

    @Override
    public void initEvent() {

    }


    @Override
    public MainPresent initPresenter() {
        return new MainPresent();
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();

    }


    /**
     * 二次退出确认 结束软件
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO 按两次返回键退出应用程序
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // 判断间隔时间 大于2秒就退出应用
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                String msg1 = getString(R.string.drop_two_to_home);
                ToastUtils.showLongToast(msg1);
//                AlertDialogShowUtil.toastMessage(MainActivity.this,msg1);
                // 计算两次返回键按下的时间差
                exitTime = System.currentTimeMillis();
            } else {
                // 返回桌面操作
                Intent home = new Intent(Intent.ACTION_MAIN);
                home.addCategory(Intent.CATEGORY_HOME);
                startActivity(home);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    @Override
    public void checkVersionUpdateSuccess(VersionUpdateBean versionUpdateBean) {
        int versionCode = 0;
        String versionName = "0.0";
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
             versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        downloadUrl = versionUpdateBean.getUpUrl();

//        if (Integer.parseInt(versionUpdateBean.getVersionNum()) > versionCode) {//有新版本
        if (!versionUpdateBean.getVersionNum().equals(versionName)) {//有新版本
            dialog.show();
            updateContentTv.setText(versionUpdateBean.getContent());
            if (versionUpdateBean.isIsForceUp()) {//是否强制更新
                skipTv.setVisibility(View.GONE);
                nowUpdateTv.setVisibility(View.GONE);
                forceUpdateTv.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void checkVersionUpdateFailure(int status, String errorMsg) {
        toast(errorMsg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skipTv:
                dialog.dismiss();
                break;
            case R.id.nowUpdateTv:
            case R.id.forceUpdateTv:
                dialog.dismiss();
                openBrowser(this, downloadUrl);
                break;
        }
    }

    public void openBrowser(Context context, String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//        intent.setAction(Intent.ACTION_VIEW);
//        intent.setData( Uri.parse(url));
//        intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
        startActivity(intent);
        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
//        if (intent.resolveActivity(context.getPackageManager()) != null) {
//            ComponentName componentName = intent.resolveActivity(context.getPackageManager());
//            // 打印Log   ComponentName到底是什么
//            context.startActivity(Intent.createChooser(intent, "请选择浏览器"));
//        } else {
//            toast("未找到手机中的浏览器");
//        }
    }
}
