package com.taiyi.soul.moudles.main.node.ram;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.BigDecimalUtil;
import com.taiyi.soul.utils.BytesUtlis;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.view.EditTextJudgeNumberWatcher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class RamActivity extends BaseActivity<RamView, RamPresent> implements RamView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.priceTv)
    TextView priceTv;
    @BindView(R.id.speedTv)
    TextView speedTv;
    @BindView(R.id.ramProgressBar)
    ProgressBar ramProgressBar;
    @BindView(R.id.buyInTv)
    TextView buyInTv;
    @BindView(R.id.sellTv)
    TextView sellTv;
    @BindView(R.id.quantityEt)
    EditText quantityEt;
    @BindView(R.id.balanceTv)
    TextView balanceTv;
    @BindView(R.id.valueTv)
    TextView valueTv;
    @BindView(R.id.progressBar)
    SeekBar progressBar;
    @BindView(R.id.receiptAccountNameEt)
    EditText receiptAccountNameEt;
    @BindView(R.id.supportTv)
    TextView supportTv;
    @BindView(R.id.buyInOrSellTv)
    TextView buyInOrSellTv;
    @BindView(R.id.quantity)
    TextView quantity;
    @BindView(R.id.top)
    ConstraintLayout top;



    private String type = "0";//0---buy,1---sell
    private String ram_price = "0.00";
    private String balance = "0.0000";
    private String ram = "0.00 KB";

    private String account = Utils.getSpUtils().getString("mainAccount");
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_ram;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public RamPresent initPresenter() {
        return new RamPresent(this);
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.ram));
        buyInTv.setSelected(true);
        receiptAccountNameEt.setEnabled(false);
        receiptAccountNameEt.setFocusable(false);
        receiptAccountNameEt.setText(account);
//        quantityEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        quantityEt.addTextChangedListener(new EditTextJudgeNumberWatcher(quantityEt,2));

    }

    @Override
    protected void initData() {

        showProgress();
        presenter.getRamPrice();
        presenter.getAccountInfoData();
        presenter.getBalance();

    }

    @Override
    public void initEvent() {

        quantityEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s) && !s.toString().startsWith(".")){
                    if (type.equals("0")){
                        String kb = BigDecimalUtil.divide(new BigDecimal(quantityEt.getText().toString()), new BigDecimal(ram_price),4).toPlainString();
                        valueTv.setText("≈" + kb + " KB");


                        int progress = BigDecimalUtil.multiply(BigDecimalUtil.divide(new BigDecimal(quantityEt.getText().toString()), new BigDecimal(balance)), new BigDecimal("100"),0).intValue();
                        progressBar.setProgress(progress);


                    }else {
                        String ngk = BigDecimalUtil.multiply(new BigDecimal(quantityEt.getText().toString()), new BigDecimal(ram_price),4).toPlainString();
                        valueTv.setText("≈" + ngk + " SOU");

                        BigDecimal decimal = new BigDecimal(quantityEt.getText().toString());
                        BigDecimal multiply = new BigDecimal(quantityEt.getText().toString()).divide(new BigDecimal(1024));
                        if(ram.split(" ")[1].equals("TB")){
                            decimal = multiply.divide(new BigDecimal(1024)).divide(new BigDecimal(1024));
                        }else if(ram.split(" ")[1].equals("GB")){
                            decimal = multiply.divide(new BigDecimal(1024));
                        }else if(ram.split(" ")[1].equals("MB")){
                            decimal =multiply;
                        }else if(ram.split(" ")[1].equals("KB")){
                            decimal = new BigDecimal(quantityEt.getText().toString());
                        }
                        int progress = BigDecimalUtil.multiply(BigDecimalUtil.divide(decimal, new BigDecimal(ram.split(" ")[0])), new BigDecimal("100"),0).intValue();
//                        int progress = BigDecimalUtil.multiply(BigDecimalUtil.divide(new BigDecimal(quantityEt.getText().toString()), new BigDecimal(ram.split(" ")[0])), new BigDecimal("100"),0).intValue();
                        progressBar.setProgress(progress);

                    }
                }else {
                    if (type.equals("0")){
                        valueTv.setText("≈0.0000 KB");
                    }else {
                        valueTv.setText("≈0.0000 SOU");
                    }
                    progressBar.setProgress(0);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        progressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int progress1 = 0;

                if (type.equals("0")){
                    if (!TextUtils.isEmpty(quantityEt.getText())){
                        progress1 = BigDecimalUtil.multiply(BigDecimalUtil.divide(new BigDecimal(quantityEt.getText().toString()), new BigDecimal(ram_price)), new BigDecimal("100"),0).intValue();
                    }
                    String ngk_count = BigDecimalUtil.multiply(new BigDecimal(balance), BigDecimalUtil.divide(new BigDecimal(progress),new BigDecimal("100")),4).toPlainString();

                    if(fromUser && progress != progress1) {
                        quantityEt.setText(ngk_count);
                    }
                }else {
                    if (!TextUtils.isEmpty(quantityEt.getText())) {
                        progress1 = BigDecimalUtil.multiply(BigDecimalUtil.divide(new BigDecimal(quantityEt.getText().toString()), new BigDecimal(ram.split(" ")[0])), new BigDecimal("100"), 0).intValue();

                    }
                    String ram_count = BigDecimalUtil.multiply(new BigDecimal(ram.split(" ")[0]), BigDecimalUtil.divide(new BigDecimal(progress),new BigDecimal("100")),4).toPlainString();
                    if(fromUser && progress != progress1) {
                        BigDecimal decimal = new BigDecimal(ram_count);
                        BigDecimal multiply = new BigDecimal(ram_count).multiply(new BigDecimal(1024));
                        if(ram.split(" ")[1].equals("TB")){
                            decimal = multiply.multiply(new BigDecimal(1024)).multiply(new BigDecimal(1024));
                        }else if(ram.split(" ")[1].equals("GB")){
                            decimal = multiply.multiply(new BigDecimal(1024));
                        }else if(ram.split(" ")[1].equals("MB")){
                            decimal =multiply;
                        }else if(ram.split(" ")[1].equals("KB")){
                            decimal = new BigDecimal(ram_count);
                        }
//                        quantityEt.setText(ram_count);
                        quantityEt.setText(decimal.toPlainString());
                        Log.e("onProgressChanged",decimal.toString()+"progress="+progress);
                    }

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.e("onStopTrackingTouch", "seekBar="+seekBar);
            }
        });

    }

    @OnClick({R.id.backIv, R.id.buyInTv, R.id.sellTv, R.id.buyInOrSellTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.buyInTv:
                type = "0";
                buyInTv.setSelected(true);
                sellTv.setSelected(false);
                balanceTv.setText(getString(R.string.balance) + " " + this.balance + " SOU");

                quantityEt.setText("");
                quantity.setText(getString(R.string.purchase_quantity));
                quantityEt.setHint(getString(R.string.input_ngk_quantity));
                buyInOrSellTv.setText(getString(R.string.buy_in));
                break;
            case R.id.sellTv:
                type = "1";
                sellTv.setSelected(true);
                buyInTv.setSelected(false);
                balanceTv.setText(getString(R.string.balance) + " " + this.ram);
                quantityEt.setText("");
                quantity.setText(getString(R.string.quantity_sold));
                quantityEt.setHint(getString(R.string.input_ram_quantity));
                buyInOrSellTv.setText(getString(R.string.sell));

                break;
            case R.id.buyInOrSellTv:

                String amount = quantityEt.getText().toString();
                if (TextUtils.isEmpty(amount)){
                    if (type.equals("0")) {
                        toast(getString(R.string.please_enter_the_amount));
                    }else {
                        toast(getString(R.string.edit_cache));
                    }
                    return;
                }

                if (Double.parseDouble(amount)<=0){
                    toast(getString(R.string.edit_money));
                    return;
                }




                if(AccountPrivateUtils.isHavePrivateKey()){//有私钥
                    if (type.equals("0")) {
                        if (BigDecimalUtil.greaterThan(new BigDecimal(amount), new BigDecimal(balance))) {
                            toast(getString(R.string.toast_balance_lacking));
                            return;
                        }
                    }else {
                        BigDecimal decimal = new BigDecimal("0");
                        BigDecimal multiply = new BigDecimal(ram.split(" ")[0]).multiply(new BigDecimal(1024));
                        if(ram.split(" ")[1].equals("TB")){
                            decimal = multiply.multiply(new BigDecimal(1024)).multiply(new BigDecimal(1024));
                        }else if(ram.split(" ")[1].equals("GB")){
                            decimal = multiply.multiply(new BigDecimal(1024));
                        }else if(ram.split(" ")[1].equals("MB")){
                            decimal =multiply;
                        }else if(ram.split(" ")[1].equals("KB")){
                             decimal = new BigDecimal(ram.split(" ")[0]);
                        }
//                        if (BigDecimalUtil.greaterThan(new BigDecimal(amount), new BigDecimal(ram.split(" ")[0]))) {
                        if (BigDecimalUtil.greaterThan(new BigDecimal(amount), decimal)) {
                            toast(getString(R.string.not_enough_storage));
                            return;
                        }

                    }

                    showPassword(view);
//                    showProgress();
//
//                    if (type.equals("0")) {
//                        presenter.buyRam(account,amount);
//
//                    }else {
//                        presenter.sellRam(account,amount);
//                    }
                }else {//未导入私钥 ，去设置
                    Dialog dialog = new Dialog(RamActivity.this, R.style.MyDialog);
                    View inflate = LayoutInflater.from(RamActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                    dialog.setContentView(inflate);
                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putString("accountName", account);
                            ActivityUtils.next(RamActivity.this, ImportAccountActivity.class, bundle, false);
                        }
                    });
                    dialog.show();
                }

//                showPassword(view);
                break;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    private String passWord="";
    //显示密码框
    private void showPassword(View view) {
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInputp(viewById, ev,keyboardPlace)) {
                        hideSoftInputp(view.getWindowToken());
                    }
                }
                return false;
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            passWord=s;
                            pay_popup.dismiss();
                            showProgress();
                            presenter.checkPayPassword(s);
                        }
                    },500);

                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom +DensityUtil.dip2px(RamActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {

                pay_popup.dismiss();


            }
        }
    }

    @Override
    public void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean) {

        if (blockChainAccountInfoBean != null) {

            BigDecimal memory_details = BigDecimalUtil.minus(new BigDecimal(blockChainAccountInfoBean.getRam_quota()), new BigDecimal(blockChainAccountInfoBean.getRam_usage()), 0);
            String memory = BytesUtlis.getPrintSize(new BigDecimal(memory_details.toPlainString())) + " / " + BytesUtlis.getPrintSize(new BigDecimal(blockChainAccountInfoBean.getRam_quota()));
            int progress = BigDecimalUtil.multiply(BigDecimalUtil.divide(memory_details, new BigDecimal(blockChainAccountInfoBean.getRam_quota())), new BigDecimal("100")).intValue();
            setAnimation(ramProgressBar, progress);


            //可以卖出的ram
            this.ram = BytesUtlis.getPrintSize(BigDecimalUtil.minus(memory_details, new BigDecimal("102400"), 0));
            if (Double.parseDouble(this.ram .split(" ")[0]) < 0){
                this.ram = "0.00 KB";
            }
            speedTv.setText(memory);
        }
        hideProgress();

    }

    @Override
    public void getDataHttpFail(String errorMsg) {

        toast(errorMsg);
        hideProgress();

    }

    @Override
    public void getRamPriceSuccess(String price) {

        ram_price = price.split(" ")[0];
        priceTv.setText(getString(R.string.mall_more_price) +":"+ price);

        presenter.getAccountInfoData();
        presenter.getBalance();
        if(type.equals("0")){
            balanceTv.setText(getString(R.string.balance) + " " + this.balance + " SOU");
        }else {
            balanceTv.setText(getString(R.string.balance) + " " + this.ram);
        }
        hideProgress();
    }

    @Override
    public void getBalanceSuccess(String balance) {
        this.balance = balance;
        if(type.equals("0")){
            balanceTv.setText(getString(R.string.balance) + " " + this.balance + " SOU");
        }else {
            balanceTv.setText(getString(R.string.balance) + " " + this.ram);
        }
        hideProgress();
    }

    @Override
    public void buySuccess() {
        toast(getString(R.string.successful_purchase));
        presenter.getAccountInfoData();
        presenter.getBalance();
        presenter.getRamPrice();
        quantityEt.setText("");
//        receiptAccountNameEt.setText("");
    }

    @Override
    public void sellSuccess() {
        toast(getString(R.string.sold_successfully));
        presenter.getAccountInfoData();
        presenter.getBalance();
        presenter.getRamPrice();
        quantityEt.setText("");
//        receiptAccountNameEt.setText("");
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg) {
        hideProgress();
        if (code==0){
            if(AccountPrivateUtils.isHavePrivateKey()){//有私钥
                String amount = quantityEt.getText().toString();
                if (type.equals("0")) {
                    if (BigDecimalUtil.greaterThan(new BigDecimal(amount), new BigDecimal(balance))) {
                        toast(getString(R.string.insufficient_balance));
                        return;
                    }
                }else {
//                    if (BigDecimalUtil.greaterThan(new BigDecimal(amount),  new BigDecimal(balance))) { //new BigDecimal(ram.split(" ")[0]))
//                        toast(getString(R.string.not_enough_storage));
//                        return;
//                    }

                }

                showProgress();

                if (type.equals("0")) {
                    presenter.buyRam(account,amount);

                }else {
                    presenter.sellRam(account,amount);
                }
            }else {//未导入私钥 ，去设置
                Dialog dialog = new Dialog(RamActivity.this, R.style.MyDialog);
                View inflate = LayoutInflater.from(RamActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                dialog.setContentView(inflate);
                inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Bundle bundle = new Bundle();
                        bundle.putString("accountName", account);
                        ActivityUtils.next(RamActivity.this, ImportAccountActivity.class, bundle, false);
                    }
                });
                dialog.show();
            }
        }else {
            toast(msg);
        }
    }


    //进度条动画
    private void setAnimation(final ProgressBar view, final int mProgressBar) {
        ValueAnimator animator = ValueAnimator.ofInt(0, mProgressBar).setDuration(500);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Log.e("setAnimation","addUpdateListener"+valueAnimator.getAnimatedValue());
                view.setProgress((int) valueAnimator.getAnimatedValue());
            }
        });
        animator.start();
    }

}
