package com.taiyi.soul.moudles.main.node.ram.bean;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/7/6
 * Time: 1:39 PM
 */
public class BuyRamBean {

    /**
     * payer : aaa
     * receiver : aaa
     * quant : 2.0000 NGK
     */

    private String payer;
    private String receiver;
    private String quant;

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getQuant() {
        return quant;
    }

    public void setQuant(String quant) {
        this.quant = quant;
    }
}
