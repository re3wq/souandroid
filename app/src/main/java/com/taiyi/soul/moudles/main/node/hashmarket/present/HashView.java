package com.taiyi.soul.moudles.main.node.hashmarket.present;

import com.taiyi.soul.moudles.main.deal.bean.EntrustOneBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustThreeBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustTwoBean;

public interface HashView {
    void onSuccess(int code, String msg_cn, EntrustOneBean bean);
    void onTwoSuccess(int code, String msg_cn, EntrustTwoBean bean);
    void onThreeSuccess(int code, String msg_cn, EntrustThreeBean bean);

    void onCancelSuccess(int code, String data);
}
