package com.taiyi.soul.moudles.main.assets.bean;

public class CoinListBean {

    /**
     * id : 1
     * coinName : ETH
     * url : http://api.mccarthy.top:5000/USDS@2x.png
     * minOut : 30
     * maxOut : 1000
     * noftOut : 0
     * quotaOut : 0
     * decimals : 1000000000000000000
     * unit : null
     * affirm : 6
     * gasPrice : 5
     * contractAddress :
     * enable : 0
     * dex : 1
     * backImgUrl : null
     * introduce : null
     * issueDate : null
     * circulationTotal : null
     * officialAddress : null
     * whitePaper : null
     * blockQuery : null
     * price : null
     */

    private int id;
    private String coinName;
    private String url;
    private String minOut;
    private String maxOut;
    private String noftOut;
    private String quotaOut;
    private long decimals;
    private Object unit;
    private int affirm;
    private String gasPrice;
    private String contractAddress;
    private int enable;
    private int dex;
    private String backImgUrl;
    private String introduce;
    private String issueDate;
    private String circulationTotal;
    private String officialAddress;
    private String whitePaper;
    private String blockQuery;
    private String price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMinOut() {
        return minOut;
    }

    public void setMinOut(String minOut) {
        this.minOut = minOut;
    }

    public String getMaxOut() {
        return maxOut;
    }

    public void setMaxOut(String maxOut) {
        this.maxOut = maxOut;
    }

    public String getNoftOut() {
        return noftOut;
    }

    public void setNoftOut(String noftOut) {
        this.noftOut = noftOut;
    }

    public String getQuotaOut() {
        return quotaOut;
    }

    public void setQuotaOut(String quotaOut) {
        this.quotaOut = quotaOut;
    }

    public long getDecimals() {
        return decimals;
    }

    public void setDecimals(long decimals) {
        this.decimals = decimals;
    }

    public Object getUnit() {
        return unit;
    }

    public void setUnit(Object unit) {
        this.unit = unit;
    }

    public int getAffirm() {
        return affirm;
    }

    public void setAffirm(int affirm) {
        this.affirm = affirm;
    }

    public String getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(String gasPrice) {
        this.gasPrice = gasPrice;
    }

    public String getContractAddress() {
        return contractAddress;
    }

    public void setContractAddress(String contractAddress) {
        this.contractAddress = contractAddress;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public String getBackImgUrl() {
        return backImgUrl;
    }

    public void setBackImgUrl(String backImgUrl) {
        this.backImgUrl = backImgUrl;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getCirculationTotal() {
        return circulationTotal;
    }

    public void setCirculationTotal(String circulationTotal) {
        this.circulationTotal = circulationTotal;
    }

    public String getOfficialAddress() {
        return officialAddress;
    }

    public void setOfficialAddress(String officialAddress) {
        this.officialAddress = officialAddress;
    }

    public String getWhitePaper() {
        return whitePaper;
    }

    public void setWhitePaper(String whitePaper) {
        this.whitePaper = whitePaper;
    }

    public String getBlockQuery() {
        return blockQuery;
    }

    public void setBlockQuery(String blockQuery) {
        this.blockQuery = blockQuery;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}
