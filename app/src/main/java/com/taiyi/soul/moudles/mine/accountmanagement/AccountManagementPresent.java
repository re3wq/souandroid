package com.taiyi.soul.moudles.mine.accountmanagement;

import android.app.Activity;
import android.content.Intent;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.bean.AccountListBean;
import com.taiyi.soul.moudles.mine.bean.SwitchAccountBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class AccountManagementPresent extends BasePresent<AccountManagementView> {
    public void getAccountList(Activity activity){
        HttpUtils.getRequets(BaseUrl.GET_ACCOUNT_MANAGER_LIST, this, null, new JsonCallback<BaseResultBean<List<AccountListBean>>>() {
            @Override
            public BaseResultBean<List<AccountListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<AccountListBean>>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getAccountListSuccess(response.body().data);
                    } else if (response.body().status == 602) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showLongToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                           activity.startActivity(new Intent(activity, LoginActivity.class));
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    }  else if (response.body().status == 3080005) {//登录失效
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
//                   //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }

                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }else {
                        view.getAccountListFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<AccountListBean>>> response) {
                super.onError(response);
            }
        });
    }
    public void switchAccount(Activity activity,String id){
        Map<String,String>map=new HashMap<>();
        map.put("childId",id);
        HttpUtils.getRequets(BaseUrl.SWITCH_ACCOUNT, this, map, new JsonCallback<BaseResultBean<SwitchAccountBean>>() {
            @Override
            public BaseResultBean<SwitchAccountBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<SwitchAccountBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.switchSuccess(response.body().data);
                    } else if (response.body().status == 602) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showLongToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            activity.startActivity(new Intent(activity, LoginActivity.class));
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else if (response.body().status == 3080005) {//登录失效
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
//                   //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }

                        Utils.getSpUtils().remove(Constants.TOKEN);
                    } else {
                        view.getAccountListFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<SwitchAccountBean>> response) {
                super.onError(response);
            }
        });
    }
}
