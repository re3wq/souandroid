package com.taiyi.soul.moudles.mine.setup.signout;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import org.json.JSONObject;

import okhttp3.Response;

public class SignOutPresent extends BasePresent<SignOutView> {
    public void signOut(){
        JSONObject jsonObject = new JSONObject();
        HttpUtils.postRequest(BaseUrl.SIGN_OUT, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status==200){
                        view.signOutSuccess();
                    }else {
                        view.signOutFailure(response.body().msg);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }
}
