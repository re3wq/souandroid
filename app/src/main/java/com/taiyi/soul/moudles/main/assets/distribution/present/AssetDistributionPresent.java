package com.taiyi.soul.moudles.main.assets.distribution.present;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.main.assets.distribution.bean.AssetDistributionBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import java.util.HashMap;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：兑换相关接口
 */
public class AssetDistributionPresent extends BasePresent<AssetDistributionView> {
    //获取页面信息
    public void getData() {

        HttpUtils.getRequets(BaseUrl.PRECENT, this, new HashMap<>(), new JsonCallback<BaseResultBean<AssetDistributionBean>>() {
            @Override
            public BaseResultBean<AssetDistributionBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<AssetDistributionBean>> response) {
                super.onSuccess(response);
                if(null!=view)
                    view.onSuccess(response.body().status, response.body().msg_cn, response.body().data);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<AssetDistributionBean>> response) {
                super.onError(response);
            }
        });
    }
}
