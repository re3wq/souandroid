package com.taiyi.soul.moudles.main.node.hashmarket.present;

import com.taiyi.soul.moudles.main.deal.bean.DealBean;
import com.taiyi.soul.moudles.main.deal.bean.EatInfoBean;
import com.taiyi.soul.moudles.main.deal.bean.EatNumberBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.node.bean.HashBalanceBean;

public interface HashMarketView {
    void onDealSuccess(DealBean bean);
    void onIssueFeeSuccess(IssueFeeBean bean);
    void onEatNumSuccess(int code, String msg_cn, EatNumberBean bean);
    void onEatSuccess(int code, String bean);
    void onEatInfoSuccess(int code, String msg_cn, EatInfoBean bean);
    void getBalanceSuccess(String symbol,String balance);
    void onHashBalanceSuccess(int code, String msg_cn, HashBalanceBean data);
    void onFailure();
    void onIsFaOrder(int code, String msg_cn, String bean);
}
