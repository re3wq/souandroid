package com.taiyi.soul.moudles.main.home.super_activity;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import okhttp3.Response;

public class SuperAcPresent extends BasePresent<SuperAcView> {
    public void getFirstData(){
        HttpUtils.postRequest(BaseUrl.GET_ACTIVITY_FIRST_DATA, this, (String) null, new JsonCallback<BaseResultBean<ActivityFirstBean>>() {
            @Override
            public BaseResultBean<ActivityFirstBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ActivityFirstBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                if (response.body().code==0){
                    view.activitySuccess(response.body().data);
                }else {
                    view.activityFailure(response.body().msg_cn);
                }
            }}
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ActivityFirstBean>> response) {
                super.onError(response);
            }
        });
    }
}
