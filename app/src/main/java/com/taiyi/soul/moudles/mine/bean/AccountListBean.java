package com.taiyi.soul.moudles.mine.bean;

public class AccountListBean {

    /**
     * id : 15936718677458444
     * dadId : 19
     * parentId : 15932291773871480
     * invitationCode : PHhUmC
     * account : kh3ahgjj.ngk
     * publickey : NGK6dybAZ4sjSyeDMt4XNyh6jmFSP8k6atm63Zzf2n8sS2sMVpFCs
     * createTime : 2020-07-02 14:37:48
     * type : 0
     * headerImg : http://114.115.131.170:8888/group1/M00/00/00/wKgAp15fd66AbHKSAAiJ135U-tQ949.png
     * parentS : 15932291773871480-null
     */

    private long id;
    private int dadId;
    private long parentId;
    private String invitationCode;
    private String account;
    private String publickey;
    private String createTime;
    private int type;
    private String headerImg;
    private String parentS;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDadId() {
        return dadId;
    }

    public void setDadId(int dadId) {
        this.dadId = dadId;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPublickey() {
        return publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }

    public String getParentS() {
        return parentS;
    }

    public void setParentS(String parentS) {
        this.parentS = parentS;
    }
}
