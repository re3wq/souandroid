package com.taiyi.soul.moudles.mine;

import androidx.fragment.app.FragmentActivity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;

import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;
import com.taiyi.soul.moudles.mine.bean.UserInfoBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class MinePresent extends BasePresent<MineView> {

    public void getUserInfo(FragmentActivity activity) {
        HttpUtils.getRequets(BaseUrl.GET_USER_INFO, this, null, new JsonCallback<BaseResultBean<UserInfoBean>>() {
            @Override
            public BaseResultBean<UserInfoBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<UserInfoBean>> response) {
                super.onSuccess(response);
                if(null!=view){
                if (response.body().status == 200) {
                    view.getUserInfoSuccess(response.body().data);
                } else if (response.body().status == 602) {//登录失效
                    if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg);
//                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }

                    Utils.getSpUtils().remove(Constants.TOKEN);
                } else if (response.body().status == 3080005) {//登录失效
                    if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg);
//                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }

                    Utils.getSpUtils().remove(Constants.TOKEN);
                } else {
                    view.getUserInfoFailure(response.body().msg);
                }}
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<UserInfoBean>> response) {
                super.onError(response);
            }
        });
    }


    //获取当前登录账号
    public void getCurrentAccount() {
        HttpUtils.getRequets(BaseUrl.GET_CURRENT_ACCOUNT, this, null, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if(null!=view)
                if (response.body().status == 200) {
                    view.getUserAccountSuccess(response.body().data);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    /**
     * 获取余额
     */
    public void getBalance(String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account", Utils.getSpUtils().getString("mainAccount"));

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if(null!=view){
                        if (response.body().size() > 0) {
                            view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        } else {
                            view.getBalanceSuccess(symbol, "0.0000");
                        }}
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if (null!=view) {
                            view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                });

    }

    //验证交易密码
    public void checkPayPassword(String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if(null!=view)
                view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }


    public void getRate() {
        String selectCoin = Utils.getSpUtils().getString(Constants.SELECT_COIN);
        Map<String, String> map = new HashMap<>();
        map.put("coinNames", "USDS");
        map.put("coinNamee", selectCoin);
        HttpUtils.getRequets(BaseUrl.COIN_TO_COIN_RATE, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if(null!=view){
                if (response.body().status == 200) {
                    view.getRateSuccess(response.body().data);
                } else {
                    view.getRateFailure(response.body().msg);
                }}
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    public void getSelectCoinRate() {
        HttpUtils.getRequets(BaseUrl.GET_SELECT_COIN_RATE, this, null, new JsonCallback<BaseResultBean<List<SelectCoinRateBean>>>() {
            @Override
            public BaseResultBean<List<SelectCoinRateBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<SelectCoinRateBean>>> response) {
                super.onSuccess(response);
                if(null!=view){
                if (response.body().status == 200) {
                    view.getSelectCoinRateSuccess(response.body().data);
                } else {
                    view.getSelectCoinRateFailure(response.body().msg);
                }}
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<SelectCoinRateBean>>> response) {
                super.onError(response);
            }
        });
    }


    public void updateAvatar(File file){
        HttpUtils.fileRequest(BaseUrl.UPDATE_AVATAR, this, file, new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if(null!=view){
                if (response.body().status==200){
                    view.updateSuccess();
                    ToastUtils.showShortToast("上传成功");
                }else {
                    view.updateFailure(response.body().msg);
                    ToastUtils.showShortToast(response.body().msg);
                }
            }}

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }


}
