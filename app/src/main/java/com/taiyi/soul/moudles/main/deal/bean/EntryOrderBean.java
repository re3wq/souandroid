package com.taiyi.soul.moudles.main.deal.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\3 0003 15:25
 * @Author : yuan
 * @Describe ：挂单
 */
public class EntryOrderBean {

        public String createtime;
        public int flag;
        public double charge;
        public String orderid;
        public double maybemoney;
        public String surplusnum;
        public String type;
        public String userid;
        public double surpluscharge;
        public String get_coinname;
        public String money;
        public String price;
        public String surplusmoney;
        public int postersid;
        public String lose_coinname;
        public String postersnum;
        public String ordertype;

}
