package com.taiyi.soul.moudles.mine.feedback;

import java.util.List;

public class TypeListBean {

    /**
     * sent : 0
     * sum : 5
     * list : [{"id":1,"type":"功能异常","langu":"zh"},{"id":2,"type":"优化意见","langu":"zh"},{"id":3,"type":"其他反馈","langu":"zh"}]
     */

    private int sent;
    private int sum;
    private List<ListBean> list;

    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 1
         * type : 功能异常
         * langu : zh
         */

        private int id;
        private String type;
        private String langu;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLangu() {
            return langu;
        }

        public void setLangu(String langu) {
            this.langu = langu;
        }
    }
}
