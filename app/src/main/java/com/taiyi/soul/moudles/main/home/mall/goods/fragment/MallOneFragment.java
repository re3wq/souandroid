package com.taiyi.soul.moudles.main.home.mall.goods.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.GoodsDetailBean;
import com.taiyi.soul.moudles.main.home.mall.bean.MallMoreBean;
import com.taiyi.soul.moudles.main.home.mall.bean.TabChangeBean;
import com.taiyi.soul.moudles.main.home.mall.goods.GoAddOrderBean;
import com.taiyi.soul.moudles.main.home.mall.goods.GoodsDetailActivity;
import com.taiyi.soul.moudles.main.home.mall.goods.SearchPriceBean;
import com.taiyi.soul.moudles.main.home.mall.goods.adapter.MallFragmentAdapter;
import com.taiyi.soul.moudles.main.home.mall.goods.adapter.SpecificationsAdapter;
import com.taiyi.soul.moudles.main.home.mall.goods.present.MallMorePresent;
import com.taiyi.soul.moudles.main.home.mall.goods.present.MallMoreView;
import com.taiyi.soul.moudles.main.home.mall.order.CheckConfirmActivity;
import com.taiyi.soul.moudles.main.home.mall.view.SpecificationsPopupWindow;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\6\17 0017 10:34
 * @Author : yuan
 * @Describe ：商城-综合
 */
public class MallOneFragment extends BaseFragment<MallMoreView, MallMorePresent> implements MallMoreView, PullRecyclerView.PullLoadMoreListener, MallFragmentAdapter.MallItemClick {

    @BindView(R.id.mall_more_pull)
    PullRecyclerView mall_more_pull;

    private int sort = 0;
    private int pageNo = 1;
    private MallFragmentAdapter mFragmentAdapter;
    private List<MallMoreBean.ComlistBean> mMallMoreBean;
    private GoodsDetailBean mGoodsDetailBean;
    private String mCommodityid;
    private SearchPriceBean mSearchPriceBean;
    private TextView mSpecification_price;
    private TextView mSpecification_stock;
    private SpecificationsPopupWindow mPopupWindow;

    @Override
    public MallMorePresent initPresenter() {
        return new MallMorePresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
    }

    @Subscribe
    public void even(TabChangeBean bean) {
        if (bean.tab_position == 0) {
            if (bean.sort != sort) {
                sort = bean.sort;
                showProgress();
                presenter.getData(getActivity(), pageNo + "", "", "", "", "", "", "", "", "");
            }
        }
    }

    @Override
    protected void initData() {
        mall_more_pull.setLayoutManager(new GridLayoutManager(getContext(), 2));
        mall_more_pull.setOnPullLoadMoreListener(this);
        mFragmentAdapter = new MallFragmentAdapter(getContext(), this);
        mFragmentAdapter.setPullRecyclerView(mall_more_pull);
        mall_more_pull.setItemAnimator(new DefaultItemAnimator());
        mall_more_pull.setAdapter(mFragmentAdapter);
        mall_more_pull.refreshWithPull();
    }

    @Override
    public void initEvent() {


    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_mall;
    }

    /************************************Call Interface callback****************************************************/

    @Override
    public void onSuccess(MallMoreBean bean) {
         hideProgress();
        if (pageNo == 1) {
            mMallMoreBean = bean.comlist;
            mFragmentAdapter.setDatas(bean.comlist);
        } else {
            mMallMoreBean.addAll(bean.comlist);
            mFragmentAdapter.addDatas(bean.comlist);
        }
        mall_more_pull.setPullLoadMoreCompleted();

        mFragmentAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<MallMoreBean.ComlistBean>() {
            @Override
            public void onItemClick(View covertView, int position, MallMoreBean.ComlistBean data) {
                Bundle bundle = new Bundle();
                bundle.putString("commodityid", data.commodityid);
                ActivityUtils.next(getActivity(), GoodsDetailActivity.class, bundle, false);
            }
        });
    }

    @Override
    public void onSearchPriceSuccess(SearchPriceBean bean) {
        hideProgress();
        mSearchPriceBean = bean;
        if (null != mSpecification_price) {
            mSpecification_price.setText(bean.commodityspec.hyprice + " USDN");
            mSpecification_stock.setText(getString(R.string.goods_details_stock) + " " + bean.commodityspec.stock);
        }
        initPopupWindow(mGoodsDetailBean);
    }

    private String mProd = "";

    @Override
    public void onGoodsSuccess(GoodsDetailBean bean) {
        hideProgress();
        mGoodsDetailBean = bean;
        mProd = "";
        for (int i = 0; i < mGoodsDetailBean.productSpecList.size(); i++) {
            for (int i1 = 0; i1 < mGoodsDetailBean.productSpecList.get(i).proList.size(); i1++) {
                if (i1 == 0) {
                    mGoodsDetailBean.productSpecList.get(i).proList.get(i1).isChecked = true;
                    if (mProd.equals("")) {
                        mProd = mGoodsDetailBean.productSpecList.get(i).proList.get(i1).specproid;
                    } else {
                        mProd += "," + mGoodsDetailBean.productSpecList.get(i).proList.get(i1).specproid;
                    }
                } else {
                    mGoodsDetailBean.productSpecList.get(i).proList.get(i1).isChecked = false;
                }
            }
        }

        presenter.searchPrice(getActivity(), mCommodityid, mProd);
    }

    @Override
    public void onAddCarSuccess(String bean) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.add_car_goods));
        EventBus.getDefault().post("car_change");
    }

    @Override
    public void onAddOrderSuccess(GoAddOrderBean bean) {
        hideProgress();
        Bundle bundle = new Bundle();
        bundle.putSerializable("details", bean);
        bundle.putInt("buy_type", 0);
        ActivityUtils.next(getActivity(), CheckConfirmActivity.class, bundle, false);
    }

    @Override
    public void onCarSizeSuccess(CarListBean bean) {

    }

    @Override
    public void onFailure() {
        hideProgress();
    }

    /************************************Refresh or LoadMore****************************************************/

    @Override
    public void onRefresh() {
        pageNo = 1;
        presenter.getData(getActivity(), pageNo + "", "", "", "", "", "", "", "", "");

    }

    @Override
    public void onLoadMore() {
        pageNo++;
        presenter.getData(getActivity(), pageNo + "", "", "", "", "", "", "", "", "");

    }

    /************************************other method****************************************************/

    int goods_number = 1;

    //item 点击事件
    @Override
    public void onItemClick(int position) {

        mCommodityid = mMallMoreBean.get(position).commodityid;
        showProgress();
        presenter.getGoodsData(getActivity(), mMallMoreBean.get(position).commodityid);
    }

    //规格弹窗
    private void initPopupWindow(GoodsDetailBean goodsDetailBean) {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.popup_specifications, null);

        ImageView specification_img = inflate.findViewById(R.id.specification_img);//商品图片
        TextView specification_num = inflate.findViewById(R.id.specification_num);//数量
        TextView specification_name = inflate.findViewById(R.id.specification_name);//商品名
        TextView sure_buy_now = inflate.findViewById(R.id.sure_buy_now);
        LinearLayout ll_button = inflate.findViewById(R.id.ll_button);

        ll_button.setVisibility(View.GONE);
        sure_buy_now.setVisibility(View.VISIBLE);
        //商品价格
        mSpecification_price = inflate.findViewById(R.id.specification_price);
        //库存
        mSpecification_stock = inflate.findViewById(R.id.specification_stock);
        RecyclerView specification_recycle = inflate.findViewById(R.id.specification_recycle);//商品规格列表
        goods_number = 1;

        SpecificationsAdapter adapter = new SpecificationsAdapter(getContext(), goodsDetailBean.productSpecList);
        specification_recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        specification_recycle.setAdapter(adapter);

        Glide.with(this).load(goodsDetailBean.product.smallimgurl).into(specification_img);
        specification_name.setText(goodsDetailBean.product.name);
        mSpecification_price.setText(goodsDetailBean.product.hyprice + " USDN");
//        mSpecification_stock.setText(getString(R.string.goods_details_stock) + " " + mSearchPriceBean.commodityspec.stock);

        inflate.findViewById(R.id.specification_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goods_number++;
                specification_num.setText(goods_number + "");
            }
        });
        inflate.findViewById(R.id.specification_minus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goods_number == 1) {
                    specification_num.setText(goods_number + "");
                    return;
                }
                goods_number--;
                specification_num.setText(goods_number + "");
            }
        });


//        //加入购物车
//        inflate.findViewById(R.id.specification_buy).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                goAddOrder();
//
//            }
//        });
        //加入购物车
        inflate.findViewById(R.id.sure_buy_now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCar();
            }
        });
        mPopupWindow = new SpecificationsPopupWindow(getContext(), inflate);
        mPopupWindow.show(getView(), getActivity().getWindow(), 0);
    }

    //立即购买
    private void goAddOrder() {
        presenter.goAddOrder(getActivity(), mSearchPriceBean.commodityspec.commodityspecid, goods_number + "");
        mPopupWindow.dismiss();
    }

    //加入购物车
    private void addCar() {
        presenter.addCar(getActivity(), mSearchPriceBean.commodityspec.commodityspecid, goods_number + "");
        mPopupWindow.dismiss();
    }

}
