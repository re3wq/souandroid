package com.taiyi.soul.moudles.main.home.mall.order.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.home.mall.order.OrderDetailActivity;
import com.taiyi.soul.moudles.main.home.mall.order.adapter.OrderFinishAdapter;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderListPay;
import com.taiyi.soul.moudles.main.home.mall.order.present.OrderPresent;
import com.taiyi.soul.moudles.main.home.mall.order.present.OrderView;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\5\25 0022 10:29
 * @Author : yuan
 * @Describe ：已完成
 */
public class OrderFinishFragment extends BaseFragment<OrderView, OrderPresent> implements OrderView, AdapterManger.OrderItemClick, PullRecyclerView.PullLoadMoreListener, OrderFinishAdapter.OrderItemButtonClick {
    @BindView(R.id.order_recycle)
    PullRecyclerView mOrderRecycle;
    private OrderFinishAdapter mFragmentAdapter;

    private int pagerNo=1;
    private List<OrderBean.OrderlistBean> mOrderlist;
    @Override
    public OrderPresent initPresenter() {
        return new OrderPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
//        List<DemoOrderBean> beanList = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            DemoOrderBean bean = new DemoOrderBean();
//            bean.orderType = 4;
//            bean.orderNum = "1";
//            bean.orderPrice = "360.00 USDN";
//            bean.goods = new ArrayList<>();
//
//            DemoOrderBean.GoodBean goods = new DemoOrderBean.GoodBean();
//            goods.img = R.mipmap.aa_discount_demo+"";
//            goods.name = "ReFa進口美容儀";
//            goods.price = "360.00 USDN";
//            goods.num = "1";
//            goods.specification = "13英寸";
//            bean.goods.add(goods);
//
//            beanList.add(bean);
//        }
        mOrderRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        mOrderRecycle.setOnPullLoadMoreListener(this);
        mOrderRecycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty,null));
        mFragmentAdapter = new OrderFinishAdapter(getContext(),this);
        mFragmentAdapter.setPullRecyclerView(mOrderRecycle);
        mOrderRecycle.setItemAnimator(new DefaultItemAnimator());
        mOrderRecycle.setAdapter(mFragmentAdapter);
        mOrderRecycle.refreshWithPull();


//        CommonAdapter dealSellAdapter = AdapterManger.getOrderDeliverAdapter(getContext(), beanList, this,this);
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_order;
    }

    @Override
    public void onItemClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("orderType",4);
        bundle.putString("ordercommodityid", mOrderlist.get(position).ordercommodityid);
        ActivityUtils.next(getActivity(), OrderDetailActivity.class,bundle, false);
    }

    /************************************Call Interface callback****************************************************/


    @Override
    public void onGetListSuccess(OrderBean bean) {
        hideProgress();
        if(pagerNo==1){
            mOrderlist = bean.orderlist;
            mFragmentAdapter.setDatas(bean.orderlist);
        }else {
            mOrderlist.addAll(bean.orderlist);
            mFragmentAdapter.addDatas(bean.orderlist);
        }
        mOrderRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onCancelOrderSuccess(String bean) {

    }

    @Override
    public void onEditAddressSuccess(String bean) {

    }

    @Override
    public void onRemindDeliverySuccess(String bean) {

    }

    @Override
    public void onZhiFuLieSuccess(OrderListPay bean) {

    }

    @Override
    public void onFailure() {
        hideProgress();
        mOrderRecycle.setPullLoadMoreCompleted();

    }

    @Override
    public void onTakeDeliverySuccess(String bean) {

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if(null!=presenter)
            presenter.getOrderList(getActivity(),pagerNo+"",3+"");
        }
    }

    @Override
    public void onRefresh() {
        pagerNo = 1;
        presenter.getOrderList(getActivity(),pagerNo+"",3+"");
    }

    @Override
    public void onLoadMore() {
        pagerNo++;
        presenter.getOrderList(getActivity(),pagerNo+"",3+"");
    }
}
