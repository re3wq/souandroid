package com.taiyi.soul.moudles.main.assets.exchange.present;

import android.app.Activity;
import android.content.Intent;

import com.alibaba.fastjson.JSONObject;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.exchange.bean.CurrencyBalanceBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRateBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRecordBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.MD5Util;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：兑换相关接口
 */
public class ExchangePresent extends BasePresent<ExchangeView> {


    /**
     * 获取币种列表
     *
     * @param from
     */
    public void getData(String from) {
        Map<String, String> map = new HashMap<>();
        map.put("from",from);
        HttpUtils.postRequest(BaseUrl.EXCHANGELIST, this, map, new JsonCallback<BaseResultBean<ExchangeBean>>() {
            @Override
            public BaseResultBean<ExchangeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ExchangeBean>> response) {
                super.onSuccess(response);
                if(null!=view)
                    view.onSuccess(response.body().code,response.body().msg_cn,response.body().data);

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ExchangeBean>> response) {
                super.onError(response);
            }
        });
    }

    //获取兑换数量
    public void getExchangeOrderData(String exchangeid,String num) {
        Map<String, String> map = new HashMap<>();
        map.put("exchangeid",exchangeid);
        map.put("num1",num);
        HttpUtils.postRequest(BaseUrl.EXCHANGEORDER, this, map, new JsonCallback<BaseResultBean<ExchangeRateBean>>() {
            @Override
            public BaseResultBean<ExchangeRateBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ExchangeRateBean>> response) {
                super.onSuccess(response);

                if(null!=view)
                    view.onGetRateSuccess(response.body().code,response.body().msg_cn,response.body().data);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ExchangeRateBean>> response) {
                super.onError(response);
            }
        });
    }

    //兑换
    public void getExchangeData(String exchangeid,String num,String json,String pri,String sign,String pass) {
        String md5Str = MD5Util.getMD5Str(sign+"exchange", MD5Util.MD5_LOWER_CASE);
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("exchangeid",exchangeid);
        map.put("num1",num);
        map.put("json",json);
        map.put("buyprice",pri);
        map.put("sign",md5Str);
        map.put("paypass",password);
        HttpUtils.postRequest(BaseUrl.EXCHANG, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if(null!=view)
                    view.onGetExchangerSuccess(response.body().code,response.body().msg_cn,response.body().data);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    //获取兑换记录
    public void getExchangerRecordData(String PageNum,String PageSize) {
        Map<String, String> map = new HashMap<>();
        map.put("PageNum",PageNum);
        map.put("PageSize",PageSize);
        HttpUtils.postRequest(BaseUrl.EXCHANGRERECORD, this, map, new JsonCallback<BaseResultBean<ExchangeRecordBean>>() {
            @Override
            public BaseResultBean<ExchangeRecordBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ExchangeRecordBean>> response) {
                super.onSuccess(response);
                if(null!=view)
                    view.onGetExchangerRecordSuccess(response.body().code,response.body().msg_cn,response.body().data);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ExchangeRecordBean>> response) {
                super.onError(response);
            }
        });
    }

    //获取挂单信息（手续费、数量、价格等）
    public void getFindOrderData(Activity activity) {
        HttpUtils.postRequest(BaseUrl.DEAFINDORDER, this, "", new JsonCallback<BaseResultBean<IssueFeeBean>>() {
            @Override
            public BaseResultBean<IssueFeeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if(null!=view)
                    view.onIssueFeeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onError(response);
            }
        });
    }

    //验证交易密码
    public void checkPayPassword(String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if(null!=view)
                    view.onCheckPayPasswordSuccess(response.body().code,response.body().msg_cn, response.body().msg_cn);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    //查询币种可用余额
    public void getCurrencyBalance(String coinName){
        Map<String,String>map=new HashMap<>();
        map.put("coinName",coinName);
        HttpUtils.getRequets(BaseUrl.GET_CURRENCY_BALANCE, this, map, new JsonCallback<BaseResultBean<CurrencyBalanceBean>>() {
            @Override
            public BaseResultBean<CurrencyBalanceBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CurrencyBalanceBean>> response) {
                super.onSuccess(response);
                if(null!=view)
//                    if (response.body().status==200){
                    view.getCurrencyBalanceSuccess(response.body().status,response.body().msg,response.body().data);

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<CurrencyBalanceBean>> response) {
                super.onError(response);
            }
        });
    }

    /**
     * 获取余额(usdn/ngk)
     */
    public void getBalance(String account,String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        }else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }

        //        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account",
                account);
        hashMap.put("symbol", symbol);
        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size()>0){
                            if(null!=view)
                                view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        }else {
                            if(null!=view)
                                view.getBalanceSuccess(symbol,"0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if(null!=view)
                            view.getBalanceSuccess(symbol,"0.0000");
                    }

                });

    }
}
