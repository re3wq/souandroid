package com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber;

import com.taiyi.soul.bean.AreaBean;

import java.util.List;

public interface BindPhoneNumberView {

    void getVerificationCodeSuccess();
    void getInvitePersonFailure(String errorMsg);
    void updateSuccess(String msg);
    void updateFailure(String errorMsg);
    void getAreaCodeSuccess(List<AreaBean>list);
}
