package com.taiyi.soul.moudles.mine.message.list

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.liaoinstan.springview.container.DefaultFooter
import com.liaoinstan.springview.container.DefaultHeader
import com.liaoinstan.springview.widget.SpringView
import com.taiyi.soul.R
import com.taiyi.soul.adapter.AdapterManger
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter
import com.taiyi.soul.app.ActivityUtils
import com.taiyi.soul.base.BaseActivity
import com.taiyi.soul.moudles.mine.message.bean.MessageListBean
import com.taiyi.soul.moudles.mine.message.content.MessageContentActivity
import kotlinx.android.synthetic.main.activity_message_list.*
import kotlinx.android.synthetic.main.base_top_layout.*

class MessageListActivity : BaseActivity<MessageListView, MessageListPresent>(), MessageListView, SpringView.OnFreshListener, MultiItemTypeAdapter.OnItemClickListener {

    var type: Int = 0
    var data = mutableListOf<MessageListBean.ListBean>()

    var pageNo = 1
    var lookType: Int = 1
    override fun getLayoutId(): Int = R.layout.activity_message_list

    override fun initPresenter(): MessageListPresent = MessageListPresent()

    override fun initImmersionBar() {
        super.initImmersionBar()
        mImmersionBar.titleBar(top).init()
    }

    override fun initViews(savedInstanceState: Bundle?) {
        type = intent.extras.getInt("type")
        when (type) {
            1 ->
//                titleTv.text = "系统消息"
                titleTv.text = resources.getString(R.string.system_message)
            else ->
                titleTv.text = resources.getString(R.string.system_notice)
//                titleTv.text = "系统公告"
        }
        rightTitleTv.text=getString(R.string.all_read)
        springView.header=DefaultHeader(this)
        springView.footer=DefaultFooter(this)
        springView.setGive(SpringView.Give.BOTH)
        springView.type = SpringView.Type.FOLLOW
        springView.setListener(this)
        msgRecyclerView.layoutManager = LinearLayoutManager(this)
        var messageListAdapter = AdapterManger.getMessageListAdapter(this, data)
        msgRecyclerView.adapter = messageListAdapter
        messageListAdapter.setOnItemClickListener(this)
    }

    override fun initData() {

    }

    override fun onResume() {
        super.onResume()
        pageNo=1
        showProgress()
        presenter.getMessageList(type, pageNo)
    }

    override fun initEvent() {
        backIv.setOnClickListener {
            finish()
        }
        rightTitleTv.setOnClickListener {
            showProgress()
            presenter.allRead(this,type.toString())
        }
    }

    override fun onSuccess() {
        presenter.getMessageList(type, pageNo)
    }

    override fun updateStateSuccess() {
        presenter.getMessageList(type,pageNo)
    }

    override fun onFailure(errorMsg: String?) {
       hideProgress()
        toast(errorMsg)
    }

    override fun getMessageListSuccess(listBean: MessageListBean?) {
        springView.onFinishFreshAndLoad()
        hideProgress()
        var list = listBean?.list
        if (pageNo == 1) {
        data.clear()
     if (list==null||list.size==0){
         noDataLl.visibility=VISIBLE
     }else{
         noDataLl.visibility= GONE
     }
        }
        for(item in list!!){
            data.add(item)
        }
        msgRecyclerView.adapter?.notifyDataSetChanged()
    }


    override fun getMessageListFailure(errorMsg: String?) {
        springView.onFinishFreshAndLoad()
        hideProgress()
        toast(errorMsg)
    }

    override fun onLoadmore() {
        pageNo++
        showProgress()
        presenter.getMessageList(type,pageNo)
    }

    override fun onRefresh() {
        pageNo=1
        showProgress()
        presenter.getMessageList(type,pageNo)
    }

    override fun onItemLongClick(view: View?, holder: RecyclerView.ViewHolder?, position: Int): Boolean =false

    override fun onItemClick(view: View?, holder: RecyclerView.ViewHolder?, position: Int) {
        when (type) {
            1 ->
               lookType = 1
            else ->
                lookType =2
        }
        presenter.updateMessageState(data[position].id, lookType.toString())
        var bundle=Bundle()
        bundle.putSerializable("data", data[position])
        ActivityUtils.next(this,MessageContentActivity::class.java,bundle)
    }
}
