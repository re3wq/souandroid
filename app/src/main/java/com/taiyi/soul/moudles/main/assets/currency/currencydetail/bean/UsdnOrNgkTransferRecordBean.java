package com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean;

import java.util.List;

public class UsdnOrNgkTransferRecordBean {

    /**
     * pagerInfo : {"totalRowCount":1,"pageSize":10,"startIndex":0,"pageIndex":1,"hasPrev":false,"hasNext":false,"isFirst":true,"isLast":true,"totalPageCount":1}
     * data : [{"from":"lkjhgfd1.ngk","to":"axiflsrz.ngk","quantity":"0.0001 NGK","feeQuantity":null,"memo":null,"actionAccount":"ngk.token","actName":"transfer","blockNum":11767171,"status":"executed","blockTime":"2020-07-22T07:55:53.5","transactionId":"1FBA1C32F8DBFC382534F9C351C8ECE12FBCEB8DE2EB7A60A2BB20D2145872A4"}]
     */

    private PagerInfoBean pagerInfo;
    private List<DataBean> data;

    public PagerInfoBean getPagerInfo() {
        return pagerInfo;
    }

    public void setPagerInfo(PagerInfoBean pagerInfo) {
        this.pagerInfo = pagerInfo;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class PagerInfoBean {
        /**
         * totalRowCount : 1
         * pageSize : 10
         * startIndex : 0
         * pageIndex : 1
         * hasPrev : false
         * hasNext : false
         * isFirst : true
         * isLast : true
         * totalPageCount : 1
         */

        private int totalRowCount;
        private int pageSize;
        private int startIndex;
        private int pageIndex;
        private boolean hasPrev;
        private boolean hasNext;
        private boolean isFirst;
        private boolean isLast;
        private int totalPageCount;

        public int getTotalRowCount() {
            return totalRowCount;
        }

        public void setTotalRowCount(int totalRowCount) {
            this.totalRowCount = totalRowCount;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getStartIndex() {
            return startIndex;
        }

        public void setStartIndex(int startIndex) {
            this.startIndex = startIndex;
        }

        public int getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }

        public boolean isHasPrev() {
            return hasPrev;
        }

        public void setHasPrev(boolean hasPrev) {
            this.hasPrev = hasPrev;
        }

        public boolean isHasNext() {
            return hasNext;
        }

        public void setHasNext(boolean hasNext) {
            this.hasNext = hasNext;
        }

        public boolean isIsFirst() {
            return isFirst;
        }

        public void setIsFirst(boolean isFirst) {
            this.isFirst = isFirst;
        }

        public boolean isIsLast() {
            return isLast;
        }

        public void setIsLast(boolean isLast) {
            this.isLast = isLast;
        }

        public int getTotalPageCount() {
            return totalPageCount;
        }

        public void setTotalPageCount(int totalPageCount) {
            this.totalPageCount = totalPageCount;
        }
    }

    public static class DataBean {
        /**
         * from : lkjhgfd1.ngk
         * to : axiflsrz.ngk
         * quantity : 0.0001 NGK
         * feeQuantity : null
         * memo : null
         * actionAccount : ngk.token
         * actName : transfer
         * blockNum : 11767171
         * status : executed
         * blockTime : 2020-07-22T07:55:53.5
         * transactionId : 1FBA1C32F8DBFC382534F9C351C8ECE12FBCEB8DE2EB7A60A2BB20D2145872A4
         */

        private String from;
        private String to;
        private String quantity;
        private Object feeQuantity;
        private String memo;
        private String actionAccount;
        private String actName;
        private int blockNum;
        private String status;
        private String blockTime;
        private String transactionId;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public Object getFeeQuantity() {
            return feeQuantity;
        }

        public void setFeeQuantity(Object feeQuantity) {
            this.feeQuantity = feeQuantity;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getActionAccount() {
            return actionAccount;
        }

        public void setActionAccount(String actionAccount) {
            this.actionAccount = actionAccount;
        }

        public String getActName() {
            return actName;
        }

        public void setActName(String actName) {
            this.actName = actName;
        }

        public int getBlockNum() {
            return blockNum;
        }

        public void setBlockNum(int blockNum) {
            this.blockNum = blockNum;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getBlockTime() {
            return blockTime;
        }

        public void setBlockTime(String blockTime) {
            this.blockTime = blockTime;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }
    }
}
