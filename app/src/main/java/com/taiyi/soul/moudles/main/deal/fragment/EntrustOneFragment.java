package com.taiyi.soul.moudles.main.deal.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.deal.adapter.EntrustOneAdapter;
import com.taiyi.soul.moudles.main.deal.bean.EntrustOneBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustThreeBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustTwoBean;
import com.taiyi.soul.moudles.main.deal.present.EntrustPresent;
import com.taiyi.soul.moudles.main.deal.present.EntrustView;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Android Studio.
 * @Date ： 2020\7\8 0008 11:21
 * @Author : yuan
 * @Describe ：集市交易-我的委托-当前委托
 */
public class EntrustOneFragment extends BaseFragment<EntrustView, EntrustPresent> implements EntrustView, PullRecyclerView.PullLoadMoreListener, EntrustOneAdapter.ItemClick {


    @BindView(R.id.pull_recycle)
    PullRecyclerView mPayRecycle;

    private boolean node_hidden;

    private EntrustOneAdapter mFragmentAdapter;
    private int pageNo = 1;

    private List<EntrustOneBean.ComlistBean> comlist;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_entrust_one;
    }


    @Override
    public EntrustPresent initPresenter() {
        return new EntrustPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if(EventBus.getDefault().isRegistered(false)){
            EventBus.getDefault().register(this);
        }

    }

    @Override
    protected void initData() {

        mPayRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        mPayRecycle.setOnPullLoadMoreListener(this);
        mPayRecycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty,null));
        mFragmentAdapter = new EntrustOneAdapter(getContext(),this);
        mFragmentAdapter.setPullRecyclerView(mPayRecycle);
        mPayRecycle.setItemAnimator(new DefaultItemAnimator());
        mFragmentAdapter.notifyDataSetChanged();
        mPayRecycle.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getContext(),R.anim.layout_anim));
        mPayRecycle.scheduleLayoutAnimation();
        mPayRecycle.setAdapter(mFragmentAdapter);
        mPayRecycle.refreshWithPull();


    }


    @Override
    public void initEvent() {

    }

    @Subscribe
   public void event(String event){

   }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
//        Log.e("fragment==","hidden");
        node_hidden = hidden;
        if (!hidden) {
            presenter.getData(getActivity(),"0",pageNo+"",mPayRecycle);
        }
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();

    }

    /************************************item Cancel button click event****************************************************/

    @Override
    public void onItemClick(int position) {
        Dialog dialog = new Dialog(getContext(), R.style.MyDialog);
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.dialog_cancel_order, null);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                String postersid =comlist.get(position).postersid;

                showProgress();
                presenter.cancelOrder(getActivity(),postersid);
            }
        });
        dialog.show();

    }
    /************************************Refresh and load more****************************************************/

    @Override
    public void onRefresh() {
        pageNo = 1;
        presenter.getData(getActivity(),"0",pageNo+"",mPayRecycle);
    }

    @Override
    public void onLoadMore() {
        pageNo ++;
        presenter.getData(getActivity(),"0",pageNo+"",mPayRecycle);
    }

    /************************************Call Interface callback****************************************************/

    @Override
    public void onSuccess(int code, EntrustOneBean bean) {//获取当前委托数据
        if(pageNo==1){
            comlist = bean.comlist;
            mFragmentAdapter.setDatas(bean.comlist);
        }else {
            comlist.addAll(bean.comlist);
            mFragmentAdapter.addDatas(bean.comlist);
        }
        mPayRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onTwoSuccess(int code, EntrustTwoBean bean) {

    }

    @Override
    public void onThreeSuccess(int code, EntrustThreeBean bean) {

    }

    @Override
    public void onFailure() {
        hideProgress();
    }

    @Override
    public void onCancelSuccess(int code, String data) {//取消委托结果
        hideProgress();
        AlertDialogShowUtil.toastMessage(getContext(),data);
        mPayRecycle.refreshWithPull();
        EventBus.getDefault().post("issue_refresh");
    }

}
