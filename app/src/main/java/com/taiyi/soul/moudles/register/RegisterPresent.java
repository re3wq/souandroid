package com.taiyi.soul.moudles.register;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.moudles.register.bean.RegisterBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class RegisterPresent extends BasePresent<RegisterView> {

    public void getVerificationCode(String areaCode,String account) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areaCode",areaCode);
            jsonObject.put("mainAccount", account);
            jsonObject.put("type", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.REGISTER_GET_VERIFICATION_CODE, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getVerificationCodeSuccess();
                    } else {
                        view.getInvitePersonFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }


    public void getInvitePerson(String inviteCode) {
        HttpUtils.getRequets(BaseUrl.INVITE_PERSON_FROM_INVITE_CODE + inviteCode, this, null, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getInvitePersonSuccess(response.body().data);
                    } else {
                        view.getInvitePersonFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    public void register(String areaCode,String account,String password,String payPass,String verifyCode,String invitationCode) {
        String passwordS = "";
        Map maps = new HashMap();
        maps.put("passWold", password);
        maps.put("payPass", payPass);
        maps.put("time", new Date().getTime());
        try {
            passwordS = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areaCode",areaCode);
            jsonObject.put("deviceId", Utils.getUniquePsuedoID());
            jsonObject.put("mainAccount", account);
            jsonObject.put("passWold", passwordS);
            jsonObject.put("payPass", passwordS);
            jsonObject.put("type", 1);
            jsonObject.put("verifyCode", verifyCode);
            jsonObject.put("invitationCode", invitationCode);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.putRequest(BaseUrl.REGISTER, this, jsonObject.toString(), new JsonCallback<BaseResultBean<RegisterBean>>() {
            @Override
            public BaseResultBean<RegisterBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<RegisterBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.registerSuccess(response.body().data);
                    } else {
                        view.registerFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<RegisterBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getAreaCode(){
        HttpUtils.getRequets(BaseUrl.GET_AREA_CODE, this, null, new JsonCallback<BaseResultBean<List<AreaBean>>>() {
            @Override
            public BaseResultBean<List<AreaBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getAreaCodeSuccess(response.body().data);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onError(response);
            }
        });
    }



    public void verificationMainAccountOnly(String account){
        Map<String,String>map=new HashMap<>();
        map.put("mainAccount",account);
        HttpUtils.getRequets(BaseUrl.MAIN_ACCOUNT_ONLY, this, map, new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status != 200) {
                        view.mainAccountOnlyFailure(response.body().msg);
                    } else {
                        view.mainAccountOnlySuccess();
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

}
