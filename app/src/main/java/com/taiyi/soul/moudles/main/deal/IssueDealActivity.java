package com.taiyi.soul.moudles.main.deal;

import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.EosSignDataManger;
import com.taiyi.soul.moudles.main.deal.bean.IssueDealTotalBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.deal.present.IssueDealPresent;
import com.taiyi.soul.moudles.main.deal.present.IssueDealView;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.view.EditTextJudgeNumberWatcher;

import org.greenrobot.eventbus.EventBus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\8 0008 11:43
 * @Author : yuan
 * @Describe ：发布委托买单委托卖单
 */
public class IssueDealActivity extends BaseActivity<IssueDealView, IssueDealPresent> implements IssueDealView {

    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.issue_sure)
    TextView mIssueSure;
    @BindView(R.id.tv_issue_num)
    TextView mTvIssueNum;
    @BindView(R.id.deal_once_price)
    TextView mDealOncePrice;
    @BindView(R.id.deal_price)
    TextView mDealPrice;
    @BindView(R.id.account_balance)
    TextView mAccountBalance;
    @BindView(R.id.service_charge)
    TextView mServiceCharge;
    @BindView(R.id.service_charge_price)
    TextView mServiceChargePrice;
    @BindView(R.id.total_price)
    TextView mTotalPrice;
    @BindView(R.id.price)
    EditText mPrice;
    @BindView(R.id.num)
    EditText mNum;
    @BindView(R.id.account_unit)
    TextView mAccountUnit;
    @BindView(R.id.charge_unit)
    TextView mChargeUnit;
    @BindView(R.id.total_unit)
    TextView mTotalUnit;
    @BindView(R.id.suggest_price)
    TextView suggest_price;
    @BindView(R.id.total_type)
    TextView total_type;
    @BindView(R.id.key_main)
    RelativeLayout key_main;
    @BindView(R.id.key_scroll)
    LinearLayout key_scroll;
    @BindView(R.id.keyboardPlace)
    LinearLayout keyboardPlace;
    private int mIssue_type;
    private String mAllMoney;
    private IssueFeeBean mBean;
    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    int type = 0;
    Handler handler = new Handler();
    Runnable runnable;
    private int mItemclick = 0;//判断是否可进行下一步
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;
    private SafeKeyboard safeKeyboardP;
    private String button_hint;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_issue_deal;
    }

    @Override
    public IssueDealPresent initPresenter() {
        return new IssueDealPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        mPrice.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        mNum.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(mPrice);
        list.add(mNum);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
            }
        }, 500);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
            View v = getCurrentFocus();

            if (isShouldHideInput(v, ev)) {
                hideSoftInput(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (event.getY() > top && event.getY() < bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                if (null != safeKeyboard && safeKeyboard.isShow()) {
                    return true;
                } else {
                    return false;
                }

            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            }
        }
    }

    @Override
    protected void initData() {

        mIssue_type = getIntent().getExtras().getInt("issue_type");
        presenter.getFindOrderData(IssueDealActivity.this);

        //根据mIssue_type判断是买单还是买单
        if (0 == mIssue_type) {//buy
            presenter.getBalance(mainAccount, "USDS");
            mTvTitle.setText(getResources().getString(R.string.issue_buy_title));
            mIssueSure.setText(getResources().getString(R.string.issue_button_buy));
            mTvIssueNum.setText(getResources().getString(R.string.issue_num_title));
            mAccountUnit.setText("USDS");
            mChargeUnit.setText("USDS");
            mTotalUnit.setText("USDS");
            total_type.setText(getResources().getString(R.string.issue_amount_totalz));
        } else {//sell
            presenter.getBalance(mainAccount, "SOU");
            mTvTitle.setText(getResources().getString(R.string.issue_sell_title));
            mIssueSure.setText(getResources().getString(R.string.issue_button_sell));
            mTvIssueNum.setText(getResources().getString(R.string.issue_num_title2));
            mAccountUnit.setText("SOU");
            mChargeUnit.setText("USDS");
            mTotalUnit.setText("USDS");
            total_type.setText(getResources().getString(R.string.issue_amount_total));

        }

        //监听价格和数量的改变（获取对应的价格）
        mPrice.addTextChangedListener(new NumberWatcher(mPrice, 4));
        mNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mItemclick = 1;
                if (runnable != null) {
                    handler.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (s.toString().equals("")) {
                            return;
                        }

                        if (!mPrice.getText().toString().equals("")) {
                            if (0 == mIssue_type) {//buy
                                presenter.getData("0", mNum.getText().toString(), mPrice.getText().toString());
                            } else {
                                presenter.getData("1", mNum.getText().toString(), mPrice.getText().toString());
                            }
                        }
//                        }

                    }
                };
                handler.postDelayed(runnable, 800);

            }
        });

    }


    @Override
    public void initEvent() {

    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }


    @OnClick({R.id.iv_back, R.id.issue_sure})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.issue_sure://确认委托

                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.issue_sure);
                if (!fastDoubleClick) {

                    if (mItemclick == 0) {
                        if (mPrice.getText().toString().equals("")) {
                            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, getString(R.string.toast_ngk_pricr));
                            return;
                        }
                        if (mNum.getText().toString().equals("")) {
                            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, getString(R.string.toast_ngk_num));
                            return;
                        }
                        if (Double.parseDouble(mPrice.getText().toString()) <= 0) {
                            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, getString(R.string.toast_ngk_pricr));
                            return;
                        }
                        if (Integer.parseInt(mNum.getText().toString()) <= 0) {
                            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, getString(R.string.toast_ngk_num));
                            return;
                        }
                        if (mDealPrice.getText().toString().equals("")) {
                            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, getString(R.string.socket_time_out));
                            return;
                        }
                        if (mTotalPrice.getText().toString().equals("")) {
                            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, getString(R.string.socket_time_out));
                            return;
                        }

                        if (0 == mIssue_type) {//buy
                            double s = Double.parseDouble(mDealPrice.getText().toString());
                            if (!"".equals(mAccountBalance.getText().toString())) {
                                double s1 = Double.parseDouble(mAccountBalance.getText().toString());
                                if (s > s1) {
                                    AlertDialogShowUtil.toastMessage(IssueDealActivity.this, getString(R.string.toast_balance_lacking));
                                } else {
                                    buyMemo++;
                                    showPassword(view);
                                }
                            }


                        } else {//sell
                            double s = Double.parseDouble(mNum.getText().toString());

                            if (!"".equals(mAccountBalance.getText().toString())) {
                                double s1 = Double.parseDouble(mAccountBalance.getText().toString());
                                if (s > s1) {
                                    AlertDialogShowUtil.toastMessage(IssueDealActivity.this, getString(R.string.toast_balance_lacking));
                                } else {
                                    buyMemo2++;
                                    showPassword(view);
                                }
                            }


                        }

                    } else if(mItemclick==2){
                        AlertDialogShowUtil.toastMessage(IssueDealActivity.this, button_hint);

                    }

                }


                break;
        }
    }

    /************************************Other methods****************************************************/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    private void showPassword(View view) { //显示密码框
        View v = LayoutInflater.from(IssueDealActivity.this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(IssueDealActivity.this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboardP = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);


        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
                    if (isShouldHideInputpop(viewById, ev, keyboardPlace)) {
                        hideSoftInputpopu(v.getWindowToken(), 1);
                    }
                }
                return false;
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(IssueDealActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showProgress();
                            getSign(s);
//                            presenter.checkPayPassword(s);
                            pay_popup.dismiss();
                        }
                    }, 500);
                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputpop(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (safeKeyboardP != null && safeKeyboardP.isShow()) {
                if (event.getY() > top && event.getY() < bottom || event.getY() > (getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight())) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            } else {
                if (event.getY() > top && event.getY() < (bottom + DensityUtil.dip2px(IssueDealActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    private void hideSoftInputpopu(IBinder token, int i) {
        if (token != null) {
            if (safeKeyboardP != null && safeKeyboardP.stillNeedOptManually(false)) {
                safeKeyboardP.hideKeyboard();
            } else {
                pay_popup.dismiss();
            }
        }
    }

    public class NumberWatcher extends EditTextJudgeNumberWatcher {//设置edittext输入到小数点后四位


        public NumberWatcher(EditText editText, int precision) {
            super(editText, precision);

        }

        @Override
        public void afterTextChanged(Editable s) {
            super.afterTextChanged(s);
            mItemclick = 1;
            if (runnable != null) {
                handler.removeCallbacks(runnable);
            }
            runnable = new Runnable() {
                @Override
                public void run() {
                    //结束后进行操作
                    if (s.toString().equals("")) {
                        return;
                    }
                    if (!mNum.getText().toString().equals("")) {
                        if (0 == mIssue_type) {//buy
                            presenter.getData("0", mNum.getText().toString(), mPrice.getText().toString());
                        } else {
                            presenter.getData("1", mNum.getText().toString(), mPrice.getText().toString());
                        }
                    }
                }
            };
            handler.postDelayed(runnable, 800);


        }
    }

    /************************************Call Interface callback****************************************************/


    @Override
    public void onSuccess(int code, String msg_cn, IssueDealTotalBean bean) {//根据金额数量获取交易总额、手续费以及到手金额
        hideProgress();
        mItemclick = code;
        button_hint = msg_cn;
        if (code == 0) {
            if (0 == mIssue_type) {//buy
                mDealPrice.setText(NumUtils.subZeroAndDot(bean.money));
                mServiceChargePrice.setText(NumUtils.subZeroAndDot(bean.charge));
                mAllMoney = bean.allmoney;
                mTotalPrice.setText(NumUtils.subZeroAndDot(bean.allmoney));
            } else {//sell
                mDealPrice.setText(NumUtils.subZeroAndDot(bean.money));
                mServiceChargePrice.setText(NumUtils.subZeroAndDot(bean.charge));
                mTotalPrice.setText(bean.allmoney);
                mAllMoney = bean.num;
            }
        } else if (code == 2) {
            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, msg_cn);
            if (0 == mIssue_type) {//buy
                mDealPrice.setText(NumUtils.subZeroAndDot(bean.money));
                mServiceChargePrice.setText(bean.charge);
                mTotalPrice.setText(NumUtils.subZeroAndDot(bean.allmoney));
                mAllMoney = bean.allmoney;
            } else {//sell
                mDealPrice.setText(NumUtils.subZeroAndDot(bean.money));
                mServiceChargePrice.setText(NumUtils.subZeroAndDot(bean.charge));
                mTotalPrice.setText(NumUtils.subZeroAndDot(bean.allmoney));
                mAllMoney = bean.num;
            }
        } else {
            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, msg_cn);

        }


    }


    @Override
    public void onCheckPayPasswordSuccess(int code, String bean) {//校验支付密码是否正确
//        if (code == 0) {//成功，获取链签名
//            getSign();
//        } else {
//            hideProgress();
//            AlertDialogShowUtil.toastMessage(IssueDealActivity.this, bean);
//        }
    }

    private int buyMemo = 0;
    private int buyMemo2 = 0;

    private void getSign(String s) {
        String memo = "";
        String price = "";
        String unit = "";
        String mainAccount = Utils.getSpUtils().getString("mainAccount");
        if (0 == mIssue_type) {//buy
            type = 0;
//            memo = "order_buy" + "_" + buyMemo;
            memo = "trading post" + "_" + buyMemo;
            unit = Constants.USDN_CONTRACT_ADDRESS;
            price = new BigDecimal(mAllMoney).setScale(8, BigDecimal.ROUND_DOWN).toPlainString() + " USDS";
        } else {//sell
            type = 1;
//            memo = "order_sell" + "_" + buyMemo2;
            memo = "expenses" + "_" + buyMemo2;
            unit = Constants.NGK_CONTRACT_ADDRESS;
            price = new BigDecimal(mAllMoney).setScale(4, BigDecimal.ROUND_DOWN).toPlainString() + " SOU";
        }

        new EosSignDataManger(IssueDealActivity.this, new EosSignDataManger.Callback() {
            @Override
            public void onSignSuccess(String sign_data) {
//                Log.e("sgin==",sign_data);
                presenter.entryOrders(type + "", mPrice.getText().toString(), mNum.getText().toString(), sign_data, s);
            }

            @Override
            public void fail() {
                hideProgress();
            }
        }).pushAction(new Gson().toJson(new TransferEosMessageBean(memo, mBean.ngk_collect_address, price
                        , mainAccount)),
                unit,
                mainAccount);
    }


    @Override
    public void entryOrdersSuccess(int code, String bean) {//委托结果
        hideProgress();
        AlertDialogShowUtil.toastMessage(IssueDealActivity.this, bean);
        if (code == 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post("issue_refresh");
                    finish();
                }
            }, 2050);
        }

    }


    @Override
    public void getBalanceSuccess(String symbol, String balance) {//获取账户余额
        hideProgress();
//        mAccountBalance.setText("1000000");
        mAccountBalance.setText(new BigDecimal(NumUtils.subZeroAndDot(balance)).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
    }


    @Override
    public void onIssueFeeSuccess(IssueFeeBean bean) {//获取手续费、交易最小金额等相关数据
        hideProgress();
        mBean = bean;
        if (null == mBean) {
            return;
        }
        if (0 == mIssue_type) {//buy
            double ngk_charge = Double.parseDouble(mBean.ngk_charge_buy);
            mServiceCharge.setText("(" + NumUtils.subZeroAndDot((ngk_charge * 100) + "") + "%)");
            suggest_price.setText(NumUtils.subZeroAndDot(bean.ngk_recommend_price_buy) + " ");
            mDealOncePrice.setText(" " + bean.ngk_min_ngk_buy + " " + getString(R.string.USDN));//单次交易最小金额
        } else {//sell
            double ngk_charge = Double.parseDouble(mBean.ngk_charge_sell);
            mServiceCharge.setText("(" + NumUtils.subZeroAndDot((ngk_charge * 100) + "") + "%)");
            suggest_price.setText(NumUtils.subZeroAndDot(bean.ngk_recommend_price_sell) + " ");
            mDealOncePrice.setText(" " + NumUtils.subZeroAndDot(bean.ngk_min_ngk_sell) + " " + getString(R.string.USDN));//单次交易最小金额
        }
    }
}
