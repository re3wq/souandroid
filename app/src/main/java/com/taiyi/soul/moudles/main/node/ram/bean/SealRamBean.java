package com.taiyi.soul.moudles.main.node.ram.bean;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/7/6
 * Time: 1:50 PM
 */
public class SealRamBean {

    /**
     * account : aaa
     * bytes : 100
     */

    private String account;
    private int bytes;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getBytes() {
        return bytes;
    }

    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

}
