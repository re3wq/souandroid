package com.taiyi.soul.moudles.mine.feedback;

public interface FeedbackView {

     void getTypeSuccess(TypeListBean typeListBean);

     void getTypeFailure(String errorMsg);

     void commitSuccess(String msg);

     void commitFailure(String errorMsg);

}
