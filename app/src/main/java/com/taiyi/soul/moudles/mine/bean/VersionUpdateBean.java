package com.taiyi.soul.moudles.mine.bean;

public class VersionUpdateBean {

    /**
     * id : 10
     * versionNum : 200
     * isForceUp : false
     * createTime : 2019-11-07 13:21:50
     * upTime : 2019-11-07 05:14:15
     * upUrl : http://www.baidu.com
     * deviceType : true
     * enabled : true
     */

    private int id;
    private String versionNum;
    private boolean isForceUp;
    private String createTime;
    private String upTime;
    private String upUrl;
    private String content;
    private boolean deviceType;
    private boolean enabled;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersionNum() {
        return versionNum;
    }

    public void setVersionNum(String versionNum) {
        this.versionNum = versionNum;
    }

    public boolean isIsForceUp() {
        return isForceUp;
    }

    public void setIsForceUp(boolean isForceUp) {
        this.isForceUp = isForceUp;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpTime() {
        return upTime;
    }

    public void setUpTime(String upTime) {
        this.upTime = upTime;
    }

    public String getUpUrl() {
        return upUrl;
    }

    public void setUpUrl(String upUrl) {
        this.upUrl = upUrl;
    }

    public boolean isDeviceType() {
        return deviceType;
    }

    public void setDeviceType(boolean deviceType) {
        this.deviceType = deviceType;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
