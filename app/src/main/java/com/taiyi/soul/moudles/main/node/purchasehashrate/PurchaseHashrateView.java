package com.taiyi.soul.moudles.main.node.purchasehashrate;

import com.taiyi.soul.moudles.main.node.bean.ComputerPurchaseRecordBean;
import com.taiyi.soul.moudles.main.node.bean.NodeBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;

public interface PurchaseHashrateView {
    void purchaseSuccess(String msg);
    void purchaseFailure(String errorMsg);

    void getPurchaseRecordSuccess(ComputerPurchaseRecordBean list);
    void getPurchaseRecordFailure(String errorMsg);
    void getBalanceSuccess(String symbol,String balance);

    void getNodeDataSuccess(NodeBean nodeBean);
    void getNodeDataFailure(String errorMsg);

    void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean);
    void getDataHttpFail(int code);
}
