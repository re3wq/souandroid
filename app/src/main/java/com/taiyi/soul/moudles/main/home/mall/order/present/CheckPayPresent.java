package com.taiyi.soul.moudles.main.home.mall.order.present;

import android.app.Activity;

import com.alibaba.fastjson.JSONObject;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.bean.AssetsBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.MD5Util;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：支付订单
 */
public class CheckPayPresent extends BasePresent<CheckPayView> {


    //验证交易密码
    public void checkPayPassword(Activity activity, String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onCheckPayPasswordSuccess(response.body().msg_cn);
                } else if (response.body().code == -1) {
                    if (null != view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}

                } else if (response.body().code == 3080005) {
                    if (null != view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}

                } else {
                    if (null != view)
                        view.onFailure();
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }


            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //获取挂单信息（手续费、数量、价格、币种地址等）
    public void getFindOrderData(Activity activity) {
        HttpUtils.postRequest(BaseUrl.DEAFINDORDER, this, "", new JsonCallback<BaseResultBean<IssueFeeBean>>() {
            @Override
            public BaseResultBean<IssueFeeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onIssueFeeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if (null != view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                }else if (response.body().code == 3080005) {
                    if (null != view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                }else {
                    if (null != view)
                        view.onFailure();
                    AlertDialogShowUtil.toastMessage(activity,response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //获取usdn、ngk余额
    public void getBalance(String account, String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account",
                account);

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size() > 0) {
                            if (null != view)
                                view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        } else {
                            if (null != view)
                                view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if (null != view)
                            view.getBalanceSuccess(symbol, "0.0000");
                    }

                });

    }

   //确认支付
    public void getPayOrder(Activity activity, String orderid,String paycoin, String json,String pass) {
        String md5Str = MD5Util.getMD5Str(json + orderid + "shop", MD5Util.MD5_LOWER_CASE);
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("orderid", orderid);
        map.put("paycoin", paycoin);//币种(USDN，NGK)
        map.put("json", json);//签名
        map.put("sign", md5Str);//签名
        map.put("paypass", password);//签名
        HttpUtils.postRequest(BaseUrl.PAYORDER, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.getPayOrderSuccess(response.body().msg_cn);
                } else if (response.body().code == -1) {
                    if (null != view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}

                } else if (response.body().code == 3080005) {
                    if (null != view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}

                } else {
                    if (null != view)
                        view.onFailure();
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //获取币种图片
    public void getAssets(Activity activity){
        HttpUtils.getRequets(BaseUrl.ASSETS_FIRST, this, null, new JsonCallback<BaseResultBean<AssetsBean>>() {
            @Override
            public BaseResultBean<AssetsBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<AssetsBean>> response) {
                super.onSuccess(response);
                if (response.body().status==200){
                    if(null!=view)
                    view.getAssetsSuccess(response.body().data);
                }else if (response.body().status == 602 || response.body().status == 3080005) {//登录失效
                    if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);

                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                }else {
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<AssetsBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

}
