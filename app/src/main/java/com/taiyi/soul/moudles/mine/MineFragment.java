package com.taiyi.soul.moudles.mine;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.about.AboutUsActivity;
import com.taiyi.soul.moudles.mine.accountmanagement.AccountManagementActivity;
import com.taiyi.soul.moudles.mine.bean.DataBean;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;
import com.taiyi.soul.moudles.mine.bean.UserInfoBean;
import com.taiyi.soul.moudles.mine.editinformation.EditInformationActivity;
import com.taiyi.soul.moudles.mine.feedback.FeedbackActivity;
import com.taiyi.soul.moudles.mine.invite.InviteActivity;
import com.taiyi.soul.moudles.mine.message.MessageActivity;
import com.taiyi.soul.moudles.mine.myearnings.MyEarningsActivity;
import com.taiyi.soul.moudles.mine.myteam.MyTeamActivity;
import com.taiyi.soul.moudles.mine.securitycenter.SecurityCenterActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.moudles.mine.setup.SetUpActivity;
import com.taiyi.soul.moudles.mine.webview.WebViewActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;
import com.taiyi.soul.utils.numberrolling.NumberRollingViewT;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.OnClick;
import me.ljp.permission.PermissionItem;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

import static android.app.Activity.RESULT_OK;

//
public class MineFragment extends BaseFragment<MineView, MinePresent> implements MultiItemTypeAdapter.OnItemClickListener, MineView, View.OnClickListener {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.messageIv)
    ImageView messageIv;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.levelIv)
    TextView levelIv;
    @BindView(R.id.mine_avatars)
    ImageView avatarIv;
    @BindView(R.id.topIndTv)
    TextView topIndTv;
    @BindView(R.id.V1)
    TextView V1;
    @BindView(R.id.V2)
    TextView V2;
    @BindView(R.id.totalAssetsTypeTv)
    TextView totalAssetsTypeTv;
//    @BindView(R.id.totalAssetsTv)
//    CountTwoView totalAssetsTv;
//    @BindView(R.id.ngkAssetsTv)
//    CountView ngkAssetsTv;
//    @BindView(R.id.usdnAssetsTv)
//    CountView usdnAssetsTv;

    @BindView(R.id.usdnAssetsTv1)
    NumberRollingView usdnAssetsTv1;
    @BindView(R.id.ngkAssetsTv1)
    NumberRollingView ngkAssetsTv1;
    @BindView(R.id.totalAssetsTv1)
    NumberRollingViewT totalAssetsTv1;
    @BindView(R.id.phoneNumberTv)
    TextView phoneNumberTv;
    @BindView(R.id.levelOneCl)
    ConstraintLayout levelOneCl;
    @BindView(R.id.levelSixCl)
    ConstraintLayout levelSixCl;
    private List<DataBean> dataBeanList = new ArrayList<>();
    private Timer timer;


    String balance_ngk = "0";
    String balance_usdn = "0";
    private int communityLevel;
    private boolean mHidden;
    //    private MyHandler handler;
    Dialog mDialog;
    int type = 1;
    File endSelectFile;
    //-------------------------------------------------拍照------------------------------------------------------------
    public static final int CAMERA_INTENT_REQUEST = 0x2005;//照相机
    //头像
    private File mCurrentPhotoFile;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;
    int ngkbalance = 0;
    int usdnbalance = 0;

    @Override
    public MinePresent initPresenter() {
        return new MinePresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
//        EventBus.getDefault().register(this);
//        Log.e("HY====","进入了我的页面initViews方法");
        initDialog();

        String symbol = Utils.getSpUtils().getString("symbol");
        totalAssetsTypeTv.setText(getString(R.string.totalAssets) + "(" + symbol + ")");
//        handler = new MyHandler();
        dataBeanList.add(new DataBean(R.mipmap.mine_my_team_iv, getString(R.string.my_team)));
        dataBeanList.add(new DataBean(R.mipmap.mine_account_management_iv, getString(R.string.account_management)));
        dataBeanList.add(new DataBean(R.mipmap.mine_invite_to_join_iv, getString(R.string.invite_to_join)));
        dataBeanList.add(new DataBean(R.mipmap.mine_my_earnings_iv, getString(R.string.my_earnings)));
        dataBeanList.add(new DataBean(R.mipmap.mine_security_center_iv, getString(R.string.security_center)));
//        dataBeanList.add(new DataBean(R.mipmap.mine_shipping_address_iv, getString(R.string.shipping_address)));
        dataBeanList.add(new DataBean(R.mipmap.mine_feedback_iv, getString(R.string.feedback)));
        dataBeanList.add(new DataBean(R.mipmap.mine_help_center_iv, getString(R.string.help_center)));
        dataBeanList.add(new DataBean(R.mipmap.mine_smart_customer_service_iv, getString(R.string.smart_customer_service)));
//        dataBeanList.add(new DataBean(R.mipmap.mine_operation_guide_iv, getString(R.string.operation_guide)));
        dataBeanList.add(new DataBean(R.mipmap.mine_about_us_iv, getString(R.string.about_us)));

        timer = new Timer();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        CommonAdapter mineListAdapter = AdapterManger.getMineListAdapter(getContext(), dataBeanList);
        recyclerView.setAdapter(mineListAdapter);
        mineListAdapter.setOnItemClickListener(this);
//        initAnimator();
        initPhotoDir1();
    }

    private void initDialog() {
        View selectView = LayoutInflater.from(getContext()).inflate(R.layout.way_select_dialog_layout, null);
        selectView.findViewById(R.id.openAlbumTv).setOnClickListener(this);
        selectView.findViewById(R.id.takePictureTv).setOnClickListener(this);
        selectView.findViewById(R.id.cancelTv).setOnClickListener(this);
        mDialog = new Dialog(getContext(), R.style.MyDialog);
        mDialog.setContentView(selectView);
        WindowManager.LayoutParams attributes = mDialog.getWindow().getAttributes();
        attributes.gravity = Gravity.BOTTOM;
        attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
        attributes.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mDialog.getWindow().setAttributes(attributes);
    }

//    @Subscribe
//    public void refreshData(String refresh) {
//        if ("switchSuccess".equals(refresh)) {
//            String symbol = Utils.getSpUtils().getString("symbol");
//            totalAssetsTypeTv.setText(getString(R.string.totalAssets) + "(" + symbol + ")");
//            presenter.getBalance("BKO");
//        }
//    }


    private int i = 0;


    public int dp2px(float dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, getResources().getDisplayMetrics());
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        mHidden = hidden;
        if (!hidden) {
            i = 0;
            mProgress = 0.0f;
            timer = new Timer();
            String symbol = Utils.getSpUtils().getString("symbol");
            totalAssetsTypeTv.setText(getString(R.string.totalAssets) + "(" + symbol + ")");
            presenter.getUserInfo(getActivity());
            presenter.getBalance("SOU");

        }
    }


    float mProgress = 0.0f;

    private void initAnimator(float progress) {
        mProgress = progress;
//        Log.i("asdasddas", "---进度为----" + mProgress);
        progressBar.post(new Runnable() {
            @Override
            public void run() {
                float v = progressBar.getMeasuredWidth() * progress;
                float v1 = v - dp2px(20);
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(iv, "translationX", 0, v1).setDuration(500);
                objectAnimator.start();
            }
        });
        topIndTv.post(new Runnable() {
            @Override
            public void run() {
                float v = progressBar.getMeasuredWidth() * progress;
                float v1 = v - dp2px(9);
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(topIndTv, "translationX", 0, v1).setDuration(500);
                objectAnimator.start();
            }
        });

        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, (int) (progress * 100)).setDuration(500);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                progressBar.setProgress((Integer) animation.getAnimatedValue());
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (progress == 1.0f) {
                    levelIv.setVisibility(View.GONE);
                    topIndTv.setVisibility(View.GONE);
                    levelSixCl.setVisibility(View.VISIBLE);
                    V2.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        valueAnimator.start();
    }

    @OnClick({R.id.setupIv, R.id.messageIv, R.id.phoneNumberTv, R.id.mine_avatars})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.setupIv:
                ActivityUtils.next(getActivity(), SetUpActivity.class);
                break;
            case R.id.messageIv:
                ActivityUtils.next(getActivity(), MessageActivity.class);
                break;
            case R.id.phoneNumberTv:
                ActivityUtils.next(getActivity(), EditInformationActivity.class);
                break;
            case R.id.mine_avatars:
                mDialog.show();
                break;
        }
    }

    private String passWord = "";

    @Override
    public void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    //显示密码框
    private void showPassword() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(getContext(), v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgot_password = v.findViewById(R.id.forgot_password);
        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInput(viewById, ev, keyboardPlace)) {
                        hideSoftInput(v.getWindowToken());
                    }
                }
                return false;
            }
        });
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(getActivity(), UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            passWord = s;
                            pay_popup.dismiss();
//                            showProgress();
                            presenter.checkPayPassword(s);
                        }
                    }, 500);

                }
            }
        });
        pay_popup.show(getView(), getActivity().getWindow(), 1);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (safeKeyboard != null && safeKeyboard.isShow()) {
                if (event.getY() > top && event.getY() < bottom || event.getY() > getActivity().getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            } else {
                if (event.getY() > top && event.getY() < bottom) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {
                pay_popup.dismiss();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (!mHidden) {
//        Log.e("main====", "onResume---------");

        ngkbalance = 0;
        usdnbalance = 0;
        String symbol = Utils.getSpUtils().getString("symbol");
        totalAssetsTypeTv.setText(getString(R.string.totalAssets) + "(" + symbol + ")");
        presenter.getUserInfo(getActivity());
        presenter.getBalance("SOU");
        presenter.getBalance("USDS");

//        }
    }

    public static final int PHOTO_REQUEST_GALLERY = 0x1001;

    // 激活系统图库，选择一张图片
    private void picture() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_REQUEST_GALLERY && resultCode == RESULT_OK) {//相册获取图片
            Uri uri = data.getData();
            String path = getPath(uri);
            List<String> list = new ArrayList<>();
            list.add(path);
            File file = new File(path);
//            Log.i("dadas", "压缩前-----" + file.length());

            Luban.with(getContext()).load(file)
                    .setTargetDir(PHOTO_DIR.getPath()).setCompressListener(new OnCompressListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onSuccess(File mFile) {
//                    showProgress();
                    presenter.updateAvatar(mFile);
                    endSelectFile = mFile;
//                    Log.i("dadas", "压缩后-----" + mFile.length());
                }

                @Override
                public void onError(Throwable e) {

                }
            }).launch();
        } else if (requestCode == CAMERA_INTENT_REQUEST && resultCode == RESULT_OK) {
            String path = mCurrentPhotoFile.getPath();
//            Log.i("dadas", "压缩前-----" + mCurrentPhotoFile.length());
            Luban.with(getContext()).load(path)
                    .setTargetDir(PHOTO_DIR.getPath()).setCompressListener(new OnCompressListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onSuccess(File mFile) {
//                    showProgress();
                    presenter.updateAvatar(mFile);
                    endSelectFile = mFile;
//                    Log.i("dadas", "压缩后-----" + mFile.length());
                }

                @Override
                public void onError(Throwable e) {

                }
            }).launch();
        }
    }

    private File PHOTO_DIR = null;

    /*
     * 获取图片存储位置
     * */
    private void initPhotoDir() {
        // 初始化图片保存路径
        File fileRoot = Environment.getExternalStorageDirectory();
        File dirFile = new File(fileRoot.getAbsolutePath() + File.separator + "SOU" + File.separator);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        String photo_dir = dirFile.getPath();
        if (TextUtils.isEmpty(photo_dir)) {
            ToastUtils.showShortToast(getString(R.string.NOT_STORAGE));
            return;
        } else {
//            PHOTO_DIR = new File(photo_dir);
            doTakePhoto();
        }
    }

    /*
     * 获取图片存储位置
     * */
    private void initPhotoDir1() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //需要在子线程中处理的逻辑
                // 初始化图片保存路径
                File fileRoot = Environment.getExternalStorageDirectory();
                File dirFile = new File(fileRoot.getAbsolutePath() + File.separator + "SOU" + File.separator);
                if (!dirFile.exists()) {
                    dirFile.mkdirs();
                }
                String photo_dir = dirFile.getPath();
                if (TextUtils.isEmpty(photo_dir)) {
                    ToastUtils.showShortToast(getString(R.string.NOT_STORAGE));
                    return;
                } else {
                    PHOTO_DIR = new File(photo_dir);
//            doTakePhoto();
                }
            }
        }).start();


    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return "";
    }

    @Override
    protected void initData() {
//        showProgress();
        //        presenter.getCurrentAccount();
        //        presenter.getBalance("USDS");


//        presenter.getUserInfo(getActivity());
//        presenter.getBalance("BKO");

    }

    @Override
    public void initEvent() {

    }


    @Override
    protected int getContentViewLayoutID() {
        return R.layout.mine_fragment_layout;
    }

    //    @Override
//    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
//        Intent intent = new Intent(getContext(), WebViewActivity.class);
//        if (position == 0) {//我的团队
//            showPassword();
//        } else if (position == 1) {//账号管理
//            ActivityUtils.next(getActivity(), AccountManagementActivity.class);
//        } else if (position == 2) {//邀请加入
//            ActivityUtils.next(getActivity(), InviteActivity.class);
//        } else if (position == 3) {//我的收益
//            ActivityUtils.next(getActivity(), MyEarningsActivity.class);
//        } else if (position == 4) {//安全中心
//            ActivityUtils.next(getActivity(), SecurityCenterActivity.class);
//        } else if (position == 5) {//收货地址
////            ActivityUtils.next(getActivity(), ShippingAddressActivity.class);
//            toast(getString(R.string.not_open));
//            //                AlertDialogShowUtil.toastMessage(getContext(), "暂未开放");
//
//        } else if (position == 6) {//意见反馈
//            ActivityUtils.next(getActivity(), FeedbackActivity.class);
//        } else if (position == 7) {//帮助中心
//            intent.putExtra("type", 3);
//            startActivity(intent);
//        } else if (position == 8) {//智能客服
//            toast(getResources().getString(R.string.not_open));
//            //                AlertDialogShowUtil.toastMessage(getContext(), "暂未开放");
//
//        }
////        else if (position == 9) {//操作指南
////            intent.putExtra("type", 2);
////            startActivity(intent);
////        }
//
//        else if (position == dataBeanList.size() - 1) {//关于我们
//            ActivityUtils.next(getActivity(), AboutUsActivity.class);
////            intent.putExtra("type", 1);
////            startActivity(intent);
//        }
//    }
    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
        Intent intent = new Intent(getContext(), WebViewActivity.class);
        if (position == 0) {//我的团队
            showPassword();
        } else if (position == 1) {//账号管理
            ActivityUtils.next(getActivity(), AccountManagementActivity.class);
        } else if (position == 2) {//邀请加入
            ActivityUtils.next(getActivity(), InviteActivity.class);
        } else if (position == 3) {//我的收益
            ActivityUtils.next(getActivity(), MyEarningsActivity.class);
        } else if (position == 4) {//安全中心
            ActivityUtils.next(getActivity(), SecurityCenterActivity.class);
        } else if (position == 5) {//意见反馈
            ActivityUtils.next(getActivity(), FeedbackActivity.class);
        } else if (position == 6) {//帮助中心
            intent.putExtra("type", 3);
            startActivity(intent);
        } else if (position == 7) {//智能客服
            toast(getResources().getString(R.string.not_open));
            //                AlertDialogShowUtil.toastMessage(getContext(), "暂未开放");

        }
//        else if (position == 8) {//操作指南
//            toast(getResources().getString(R.string.not_open));
//            //                AlertDialogShowUtil.toastMessage(getContext(), "暂未开放");
//
//        }
//        else if (position == 9) {//操作指南
//            intent.putExtra("type", 2);
//            startActivity(intent);
//        }

        else if (position == dataBeanList.size() - 1) {//关于我们
            ActivityUtils.next(getActivity(), AboutUsActivity.class);
//            intent.putExtra("type", 1);
//            startActivity(intent);
        }
    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }

    private String replace;

    @Override
    public void getUserInfoSuccess(UserInfoBean userInfoBean) {
        hideProgress();
        boolean read = userInfoBean.isRead();
        if (read) {
            messageIv.setImageResource(R.mipmap.new_news_iv);
        } else {
            messageIv.setImageResource(R.mipmap.message_no_iv);
        }
        ObjectAnimator.ofFloat(iv, "translationX", 0, 0).setDuration(10).start();
        ObjectAnimator.ofFloat(topIndTv, "translationX", 0, 0).setDuration(10).start();
        RequestOptions options = new RequestOptions();
        options.centerCrop().placeholder(R.mipmap.mine_avatar_iv).error(R.mipmap.mine_avatar_iv);
        Glide.with(getContext()).load(userInfoBean.getHeaderImg()).apply(options).into(avatarIv);
        int loginType = Utils.getSpUtils().getInt("loginType");
        if (loginType == 0) {//手机号
            String phoneNumber = Utils.getSpUtils().getString("phoneNumber");
//            }
            phoneNumberTv.setText(phoneNumber);
        } else {
            String email = Utils.getSpUtils().getString("email");
            phoneNumberTv.setText(email);
        }

        communityLevel = userInfoBean.getCommunityLevel();
        levelIv.setText("VIP" + communityLevel);
        if (communityLevel == 0) {
            topIndTv.setVisibility(View.GONE);
            iv.setVisibility(View.GONE);
            levelOneCl.setVisibility(View.GONE);
            levelSixCl.setVisibility(View.GONE);
            levelIv.setVisibility(View.INVISIBLE);
            progressBar.setProgress(0);
        } else if (communityLevel == 1) {
            V1.setVisibility(View.INVISIBLE);
            levelOneCl.setVisibility(View.VISIBLE);
            topIndTv.setText("" + communityLevel);
            levelSixCl.setVisibility(View.GONE);
            levelIv.setVisibility(View.VISIBLE);
//            levelIv.setVisibility(View.GONE);
            topIndTv.setVisibility(View.GONE);
            progressBar.setProgress(0);
        } else if (communityLevel == 2) {
            levelIv.setVisibility(View.VISIBLE);
            topIndTv.setVisibility(View.VISIBLE);
            iv.setVisibility(View.VISIBLE);
            levelOneCl.setVisibility(View.GONE);
            levelSixCl.setVisibility(View.GONE);
            V1.setVisibility(View.VISIBLE);
            topIndTv.setText("" + communityLevel);
            initAnimator(0.2f);
        } else if (communityLevel == 3) {
            levelIv.setVisibility(View.VISIBLE);
            topIndTv.setVisibility(View.VISIBLE);
            iv.setVisibility(View.VISIBLE);
            V1.setVisibility(View.VISIBLE);
            topIndTv.setText("" + communityLevel);
            levelOneCl.setVisibility(View.GONE);
            levelSixCl.setVisibility(View.GONE);
            initAnimator(0.4f);
        } else if (communityLevel == 4) {
            levelIv.setVisibility(View.VISIBLE);
            topIndTv.setVisibility(View.VISIBLE);
            iv.setVisibility(View.VISIBLE);
            V1.setVisibility(View.VISIBLE);
            topIndTv.setText("" + communityLevel);
            levelOneCl.setVisibility(View.GONE);
            levelSixCl.setVisibility(View.GONE);
            initAnimator(0.6f);
        } else if (communityLevel == 5) {
            levelIv.setVisibility(View.VISIBLE);
            topIndTv.setVisibility(View.VISIBLE);
            iv.setVisibility(View.VISIBLE);
            V1.setVisibility(View.VISIBLE);
            topIndTv.setText("" + communityLevel);
            levelSixCl.setVisibility(View.GONE);
            levelOneCl.setVisibility(View.GONE);
            initAnimator(0.8f);
        } else if (communityLevel == 6) {
            levelIv.setVisibility(View.VISIBLE);
            topIndTv.setVisibility(View.VISIBLE);
            iv.setVisibility(View.VISIBLE);
            V1.setVisibility(View.VISIBLE);
            levelSixCl.setVisibility(View.GONE);
            topIndTv.setText("" + communityLevel);
            levelOneCl.setVisibility(View.GONE);
            initAnimator(1.0f);
        }
    }

    @Override
    public void getUserInfoFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getUserAccountSuccess(String accountStr) {
        hideProgress();
    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        hideProgress();

        if (symbol.equals("SOU")) {
            ngkbalance = 1;
            balance_ngk = balance;
        }
        if (symbol.equals("USDS")) {
            usdnbalance = 1;
            balance_usdn = balance;
        }
        if (ngkbalance == 1 && usdnbalance == 1) {
            presenter.getSelectCoinRate();
        }
//        if (symbol.equals("BKO")) {
//            balance_ngk = balance;
//
//            if (!balance_ngk.equals("0"))
////                ngkAssetsTv.setMoney(Float.parseFloat(new BigDecimal(balance_ngk).setScale(5,BigDecimal.ROUND_DOWN).toPlainString()));
////                ngkAssetsTv1.setContent(new BigDecimal(balance_ngk).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
//                presenter.getBalance("USDS");
//        } else {
//            balance_usdn = balance;
//            if (!balance_usdn.equals("0")) {
//                Log.e("balance_usdn", balance_usdn);
////                usdnAssetsTv1.setContent(new BigDecimal(balance_usdn).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
////                usdnAssetsTv.setMoney(Float.parseFloat(new BigDecimal(balance_usdn).setScale(5,BigDecimal.ROUND_DOWN).toPlainString()));
//                presenter.getSelectCoinRate();
//            }
//        }
////        if (!balance_ngk.equals("0") && !balance_usdn.equals("0")){
//            ngkAssetsTv.setMoney(Float.parseFloat(balance_ngk));
//            usdnAssetsTv.setMoney(Float.parseFloat(balance_usdn));
//        }
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg) {
        hideProgress();
        if (code == 0) {
            Bundle bundle = new Bundle();
            bundle.putString("payPwd", passWord);
            ActivityUtils.next(getActivity(), MyTeamActivity.class, bundle);
        } else {
            AlertDialogShowUtil.toastMessage(getActivity(), msg);
        }
    }

    @Override
    public void getRateSuccess(String rate) {
        hideProgress();
//        float totalAssets = Float.parseFloat(balance_ngk) * Float.parseFloat(balance_usdn) * Float.parseFloat(rate);
//        totalAssetsTv.setMoney(totalAssets);
    }

    @Override
    public void getRateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getSelectCoinRateSuccess(List<SelectCoinRateBean> list) {
        hideProgress();
        String selectCoin = Utils.getSpUtils().getString(Constants.SELECT_COIN);
        for (SelectCoinRateBean selectCoinRateBean : list) {
            if (selectCoinRateBean.getCurrency_code().equals(selectCoin)) {
                double rate = selectCoinRateBean.getRate();


                String ngkRate = Utils.getSpUtils().getString(Constants.NGK_TO_USDT_RATE);
                String usdnRate = Utils.getSpUtils().getString(Constants.USDN_TO_USDT_RATE);
//                if (!balance_ngk.equals("0.0000") && !balance_usdn.equals("0.0000")) {
//                    double totalAssets = (Float.parseFloat(ngkRate) * Float.parseFloat(balance_ngk) + Float.parseFloat(usdnRate) * Float.parseFloat(balance_usdn)) * rate;
//
//                    Log.i("dadasdas", "----" + totalAssets);
//                    totalAssetsTv.setMoney(Float.parseFloat(String.valueOf(totalAssets)));

                if (!ngkRate.equals("") && !usdnRate.equals("")) {
                    String totalAssets = new BigDecimal(ngkRate).multiply(new BigDecimal(balance_ngk)).add((new BigDecimal(usdnRate).multiply(new BigDecimal(balance_usdn)).multiply(new BigDecimal(rate)))).toPlainString();
                    totalAssetsTv1.setContent(new BigDecimal(totalAssets).setScale(2, BigDecimal.ROUND_DOWN).toPlainString());
                    ngkAssetsTv1.setContent(new BigDecimal(balance_ngk).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
                    usdnAssetsTv1.setContent(new BigDecimal(balance_usdn).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
                }


//                } else {
//
//
//                        String totalAssets = new BigDecimal(ngkRate).multiply(new BigDecimal(balance_ngk)).add((new BigDecimal(usdnRate).multiply(new BigDecimal(balance_usdn)).multiply(new BigDecimal(rate)))).toPlainString();
//
//                    totalAssetsTv1.setContent(totalAssets);
//                    ngkAssetsTv1.setContent(new BigDecimal(balance_ngk).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
//                    usdnAssetsTv1.setContent(new BigDecimal(balance_usdn).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
//

//                }

                break;
            }
        }
    }

    @Override
    public void getSelectCoinRateFailure(String errorMsg) {
        hideProgress();
    }

    @Override
    public void updateSuccess() {
        hideProgress();
        presenter.getUserInfo(getActivity());
//        Glide.with(getContext()).load(endSelectFile).into(avatarIv);
    }

    @Override
    public void updateFailure(String errorMsg) {
        hideProgress();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.openAlbumTv:
                mDialog.dismiss();
                type = 1;
                List<PermissionItem> permissonItems = new ArrayList<PermissionItem>();
                permissonItems.add(new PermissionItem(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.SCAN_EXTERNAL_STORAGE), R.drawable.permission_ic_storage));
                if (Utils.getPermissions(permissonItems, getString(R.string.SCAN_EXTERNAL_STORAGE))) {
                    picture();
                }
                break;
            case R.id.takePictureTv:
                mDialog.dismiss();
//                String mFileName = System.currentTimeMillis() + ".jpg";
//                mCurrentPhotoFile = new File(Environment.getExternalStorageDirectory(),
//                        mFileName);
                type = 2;
                List<PermissionItem> permissonItems1 = new ArrayList<PermissionItem>();
                permissonItems1.add(new PermissionItem(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.SCAN_EXTERNAL_STORAGE), R.drawable.permission_ic_storage));
                if (Utils.getPermissions(permissonItems1, getString(R.string.SCAN_EXTERNAL_STORAGE))) {
                    initPhotoDir();
                }
                break;
            case R.id.cancelTv:
                mDialog.dismiss();
                break;
        }
    }


    /**
     * 拍照获取图片
     */
    protected void doTakePhoto() {
        try {
            String mFileName = System.currentTimeMillis() + ".jpg";

            mCurrentPhotoFile = new File(Environment.getExternalStorageDirectory(),
                    mFileName);
            List<PermissionItem> permissonItems = new ArrayList<PermissionItem>();
            permissonItems.add(new PermissionItem(Manifest.permission.CAMERA, getString(R.string.camer), R.drawable.permission_ic_camera));
            if (Utils.getPermissions(permissonItems, getString(R.string.open_camer_scan))) {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                // 判断存储卡是否可以用，可用进行存储
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {//provider   fileprovider
                    Uri photoURI = FileProvider.getUriForFile(getActivity().getApplicationContext(), getActivity().getApplicationContext().getPackageName() + ".fileprovider", mCurrentPhotoFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            photoURI);
                }
                startActivityForResult(intent, CAMERA_INTENT_REQUEST);
            }

        } catch (Exception e) {
            ToastUtils.showLongToast(getString(R.string.NOT_CAMERA));
        }
    }


}
