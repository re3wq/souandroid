package com.taiyi.soul.moudles.main.assets.exchange.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRecordBean;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class ExchangeRecordAdapter extends BaseRecyclerAdapter<ExchangeRecordBean.ComlistBean>  {


    private PullRecyclerView pullRecyclerView;
    private Context context;

    public ExchangeRecordAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_exchange;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final ExchangeRecordBean.ComlistBean data) {


        TextView record_rates_details = getView(holder,R.id.record_rates_details);
        TextView record_rates = getView(holder,R.id.record_rates);
        ImageView record_img_one = getView(holder,R.id.record_img_one);
        ImageView record_img_two = getView(holder,R.id.record_img_two);
        TextView record_time = getView(holder,R.id.record_time);
        TextView fee_type = getView(holder,R.id.fee_type);
        TextView exchanges_miner_fee = getView(holder,R.id.exchanges_miner_fee);
        record_time.setText(data.createtime);

//        if (data.from_coin_name.equals("USDK") || data.from_coin_name.equals("BKO")) {
//            fee_type.setText(context.getString(R.string.exchanges_miner_fee));
//        }
        String num = "0";
        String money = "0";
        String charge = "0";
        if(data.to_coin_name.equals("BTC")){
            money = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
        }else  if(data.to_coin_name.equals("ETH")){
            money = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
        }else {
            money = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
        }
        if(data.from_coin_name.equals("BTC")){
            num = new BigDecimal(data.num).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
            charge = new BigDecimal(data.charge).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
        }else  if(data.from_coin_name.equals("ETH")){
            num = new BigDecimal(data.num).setScale(6,BigDecimal.ROUND_DOWN).toPlainString();
            charge = new BigDecimal(data.charge).setScale(6,BigDecimal.ROUND_DOWN).toPlainString();
        }else {
            num = new BigDecimal(data.num).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
            charge = new BigDecimal(data.charge).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
        }
        record_rates.setText(1+data.from_coin_name +" = "+data.price+data.to_coin_name);
        record_rates_details.setText(num+data.from_coin_name +" = "+money+data.to_coin_name);
        exchanges_miner_fee.setText(charge+data.from_coin_name);
        Glide.with(context).load(data.from_url).into(record_img_one);
        Glide.with(context).load(data.to_url).into(record_img_two);

    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }
    //买卖单点击
    public interface DealItemClick {
        void onDeal(int position);
    }
}

