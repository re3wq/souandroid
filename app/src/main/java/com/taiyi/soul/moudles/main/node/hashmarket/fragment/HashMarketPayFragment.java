package com.taiyi.soul.moudles.main.node.hashmarket.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.deal.adapter.DealPayAdapter;
import com.taiyi.soul.moudles.main.deal.bean.DealBean;
import com.taiyi.soul.moudles.main.deal.bean.DealPayBean;
import com.taiyi.soul.moudles.main.deal.bean.EatInfoBean;
import com.taiyi.soul.moudles.main.deal.bean.EatNumberBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.deal.view.DealPopupWindow;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.main.node.bean.HashBalanceBean;
import com.taiyi.soul.moudles.main.node.hashmarket.adapter.HashMarketPayAdapter;
import com.taiyi.soul.moudles.main.node.hashmarket.present.HashMarketPresent;
import com.taiyi.soul.moudles.main.node.hashmarket.present.HashMarketView;
import com.taiyi.soul.moudles.main.node.hashmarket.release.ReleaseOrderActivity;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\8 0008 11:38
 * @Author : yuan
 * @Describe ：算力市场买单列表
 */
public class HashMarketPayFragment extends BaseFragment<HashMarketView, HashMarketPresent> implements HashMarketView, DealPayAdapter.DealItemClick, PullRecyclerView.PullLoadMoreListener, HashMarketPayAdapter.DealItemClick {


    @BindView(R.id.pay_recycle)
    PullRecyclerView mPayRecycle;

    private boolean node_hidden;
    private List<DealPayBean> mBeanList;
    private HashMarketPayAdapter mFragmentAdapter;
    private int pageNo;


    private List<DealBean.ComlistBean> mComlist = new ArrayList<>();
    private static int eat_buy_min_num = 0;
    private TextView mPopup_all_price;
    private String mPay_coin_name;
    private String mPay_price;
    private static String hash_collect_address;
    //    private EatNumberBean mEatNumberBean;
    private int mEatPosition;
    private EditText mPopup_num;
    private EatInfoBean mInfoBean;
    private int mItemclick;//判断是否可以点击下一步（弹出输入密码框）

    int j = 0;
    private String mMainAccount;
    private TextView mDeal_once_price;
    private String mBalance;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;
    private DealPopupWindow dealPopupWindow;


    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_hash_market;
    }

    @Override
    public HashMarketPresent initPresenter() {
        return new HashMarketPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        mMainAccount = Utils.getSpUtils().getString("mainAccount");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    @Subscribe
    public void even(String event) {
//        Log.e("fragment==", event);
//        if ("refresh_anim".equals(event)) {
//            mFragmentAdapter.notifyDataSetChanged();
//            mPayRecycle.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_anim));
//            mPayRecycle.scheduleLayoutAnimation();
//        }
        if ("hash_issue_refresh".equals(event)) {
            mPayRecycle.refreshWithPull();
        }
    }

    @Override
    protected void initData() {

        presenter.getFindOrderData(getActivity());

        mPayRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        mPayRecycle.setOnPullLoadMoreListener(this);
        mPayRecycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new HashMarketPayAdapter(getContext(), this);
        mFragmentAdapter.setPullRecyclerView(mPayRecycle);
        mPayRecycle.setItemAnimator(new DefaultItemAnimator());
        mPayRecycle.setIsShowNoMore(true);

        mPayRecycle.setAdapter(mFragmentAdapter);
        mPayRecycle.refreshWithPull();
        mPayRecycle.setPullLoadMoreCompleted();


    }

    @Override
    public void initEvent() {


    }

    @Subscribe
    public void event(String event) {
        if ("issue_refresh".equals(event)) {
            mPayRecycle.refreshWithPull();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
//        Log.e("fragment==", "hidden");
        node_hidden = hidden;
        if (!hidden) {
            showProgress();
            mMainAccount = Utils.getSpUtils().getString("mainAccount");
            presenter.getFindOrderData(getActivity());
            presenter.getData(getActivity(), "0", pageNo + "", mPayRecycle);
            mMainAccount = Utils.getSpUtils().getString("mainAccount");
            ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
            if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
                accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
            }


            for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
                if (accountInfoBean.getAccount_name().equals(mMainAccount)) {
                    j += 1;
                }
            }
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();
    }

    @Override
    public void onDeal(int position) {

        mEatPosition = position;

        if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
            initDeal(0);
        } else {//未导入私钥 ，去设置
            Dialog dialog = new Dialog(getContext(), R.style.MyDialog);
            View inflate = LayoutInflater.from(getContext()).inflate(R.layout.dialog_import_sub_wallet, null);
            dialog.setContentView(inflate);
            inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putString("accountName", mMainAccount);
                    ActivityUtils.next(getActivity(), ImportAccountActivity.class, bundle, false);
                }
            });
            dialog.show();
        }
    }

    /************************************Other methods****************************************************/

    Handler handler = new Handler();
    Runnable runnable;

    private void initDeal(int i) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.popup_deal, null);
        dealPopupWindow = new DealPopupWindow(getContext(), view);
        ((TextView) view.findViewById(R.id.deal_popup_title)).setText(getString(R.string.deal_popu_buy_title));
        mPopup_num = view.findViewById(R.id.popup_num);
        TextView unit = view.findViewById(R.id.unit);
        RelativeLayout key_main = view.findViewById(R.id.key_main);
        LinearLayout key_scroll = view.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = view.findViewById(R.id.keyboardPlace);

        mPopup_num.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        List<EditText> list = new ArrayList<>();
        list.add(mPopup_num);

        unit.setText("Hash");
        mPopup_num.setHint(getResources().getString(R.string.release_num_hint));
        TextView popup_price = view.findViewById(R.id.popup_price);
        mDeal_once_price = view.findViewById(R.id.deal_once_price);
        TextView deal_once_name = view.findViewById(R.id.deal_once_name);
        mPopup_all_price = view.findViewById(R.id.popup_all_price);

        deal_once_name.setText(getResources().getString(R.string.popu_balance));
        presenter.getUserHashBalanceData(getActivity());


//        if (i == 0) {//从交易点击弹出
            mPopup_num.setText(mComlist.get(mEatPosition).surplusnum + "");
            presenter.getEatNumber(getActivity(), mComlist.get(mEatPosition).postersid, mPopup_num.getText().toString());
//        } else {//输入密码后数量不足，修改
//            mPopup_num.setText(mInfoBean.surplusnum + "");
//            presenter.getEatNumber(getActivity(), mComlist.get(mEatPosition).postersid, mPopup_num.getText().toString());
//
//        }

        ((TextView) view.findViewById(R.id.deal_popup_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealPopupWindow.dismiss();
            }
        });
        ((TextView) view.findViewById(R.id.deal_popup_sure)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick();
                if (!fastDoubleClick) {
                    if (mPopup_num.getText().toString().equals("")) {
                        AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.release_num_hint));
                        return;
                    }
                    if (mItemclick != 1) {
                        return;
                    }

                        if (Integer.parseInt(mPopup_num.getText().toString()) > 0) {
                            dealPopupWindow.dismiss();
                            showPassword();
                        } else {
                            AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.release_num_hint));
                            return;
                        }

                }
            }
        });


        if (mComlist.size() > 0) {
            popup_price.setText(getString(R.string.deal_popup_price_two).replace("XXX", NumUtils.subZeroAndDot(mComlist.get(mEatPosition).price)));
//            popup_price.setText(NumUtils.subZeroAndDot(mComlist.get(mEatPosition).price));
//            if (mComlist.get(mEatPosition).surplusnum <= eat_buy_min_num) {
//
//                mPopup_num.setKeyListener(null);
//                showProgress();
//                presenter.getEatNumber(getActivity(), mComlist.get(mEatPosition).postersid, mPopup_num.getText().toString());
//                dealPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
//                    @Override
//                    public boolean onTouch(View vi, MotionEvent ev) {
//                        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
//
//                            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
//
//
//                            if (isShouldHideInputp(mPopup_num, ev,keyboardPlace)) {
//                                hideSoftInputp(view.getWindowToken());
//                            }
//                        }
//                        return false;
//                    }
//                });
//            } else {
            safeKeyboard = ShowKeyboard.initKeyBoard(getContext(), keyboardPlace, key_main, key_scroll, list, true);
            dealPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View vi, MotionEvent ev) {
                    if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                        // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                        if (isShouldHideInput(mPopup_num, ev, keyboardPlace)) {
                            hideSoftInput(view.getWindowToken(), 1);
                        }
                    }
                    return false;
                }
            });
            mPopup_num.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    mItemclick = 0;
                    if (runnable != null) {
                        handler.removeCallbacks(runnable);
                    }
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (!mPopup_num.getText().toString().equals("")) {
                                presenter.getEatNumber(getActivity(), mComlist.get(mEatPosition).postersid, mPopup_num.getText().toString());
                            }
                        }
                    };
                    handler.postDelayed(runnable, 800);
                }
            });
//            }
        }


        if(!dealPopupWindow.isShowing()){
            dealPopupWindow.show(getView(), getActivity().getWindow(), 1);
        }
    }


    //显示密码框
    private void showPassword() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(getContext(), v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getContext(), keyboardPlace, key_main, key_scroll, list, true);

        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInput(mPopup_num, ev, keyboardPlace)) {
                        hideSoftInput(v.getWindowToken(), 2);
                    }
                }
                return false;
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(getActivity(), UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showProgress();
                            presenter.eatOrder(getActivity(), mComlist.get(mEatPosition).postersid, mPopup_num.getText().toString(), "", s);
                            pay_popup.dismiss();
                        }
                    }, 500);
                }
            }
        });
        pay_popup.show(getView(), getActivity().getWindow(), 1);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (safeKeyboard != null && safeKeyboard.isShow()) {
                if (event.getY() > top && event.getY() < bottom || event.getY() > getActivity().getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            } else {
                if (event.getY() > top && event.getY() < (bottom + DensityUtil.dip2px(getContext(), 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    private void hideSoftInput(IBinder token, int i) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {
                if (i == 1) {
                    dealPopupWindow.dismiss();
                } else {
                    pay_popup.dismiss();
                }

            }
        }
    }

    /************************************Refresh and load more****************************************************/

    @Override
    public void onRefresh() {
        pageNo = 1;
        presenter.getData(getActivity(), "0", pageNo + "", mPayRecycle);
    }

    @Override
    public void onLoadMore() {
        pageNo++;
        presenter.getData(getActivity(), "0", pageNo + "", mPayRecycle);
    }

    /************************************Call Interface callback****************************************************/

    @Override
    public void onDealSuccess(DealBean bean) {
        hideProgress();
        if (pageNo == 1) {
            mComlist = bean.comlist;
            mFragmentAdapter.setDatas(bean.comlist);
        } else {
            mComlist.addAll(bean.comlist);
            mFragmentAdapter.addDatas(bean.comlist);
        }
        mPayRecycle.setPullLoadMoreCompleted();

        if (null == bean.comlist || bean.comlist.size() == 0) {
            if (pageNo != 1) {
                pageNo--;
            }
        }
    }

    @Override
    public void onIssueFeeSuccess(IssueFeeBean bean) {
        hideProgress();
        eat_buy_min_num = Integer.parseInt(bean.hash_eat_buy_mixnum);
        hash_collect_address = bean.hash_collect_address;
    }


    @Override
    public void onEatNumSuccess(int code, String msg_cn, EatNumberBean bean) {
        hideProgress();
        eatsuccess = 0;
        if (code == 0) {
            mItemclick = 1;
//            mEatNumberBean = bean;
            mPay_coin_name = bean.pay_coin_name;
            mPay_price = bean.pay_price;
            mPopup_all_price.setText(NumUtils.subZeroAndDot(bean.have_price));
        } else if (code == 2) {
            presenter.eatOrderInfo(getActivity(), mComlist.get(mEatPosition).postersid);
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }  else if (code == 1){
            mItemclick = 0;
            if(dealPopupWindow.isShowing()){
                dealPopupWindow.dismiss();
            }
            mComlist.remove(mEatPosition);
            mFragmentAdapter.removePos(mEatPosition);
            mFragmentAdapter.notifyDataSetChanged();
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        } else{
            mItemclick = 0;
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }

    }
    private int eatsuccess = 0;
    @Override
    public void onEatSuccess(int code, String bean) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(getContext(),bean);
        if(code==0 ||code==2||code==1){
            eatsuccess = 1;
            presenter.eatOrderInfo(getActivity(), mComlist.get(mEatPosition).postersid);
        }

    }

    @Override
    public void onEatInfoSuccess(int code, String msg_cn, EatInfoBean bean) {
        if (code == 0) {
            mInfoBean = bean;
            if (Integer.parseInt(bean.surplusnum) != 0) {
                for (int i = 0; i < mComlist.size(); i++) {
                    if (mEatPosition == i) {
                        mComlist.get(mEatPosition).surplusnum = bean.surplusnum;
                    }
                }
                mFragmentAdapter.notifyDataSetChanged();
                if( eatsuccess !=1){
                    //                    initDeal(1);
                    if(null!=mPopup_num){
                        mPopup_num.setText(mComlist.get(mEatPosition).surplusnum + "");
                        presenter.getEatNumber(getActivity(), mComlist.get(mEatPosition).postersid, mPopup_num.getText().toString());
                    }
                }
            }
        } else if (code == 1){
            mComlist.remove(mEatPosition);
            mFragmentAdapter.removePos(mEatPosition);
            mFragmentAdapter.notifyDataSetChanged();
        }else {
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }

    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {

    }

    @Override
    public void onHashBalanceSuccess(int code, String msg_cn, HashBalanceBean data) {
        hideProgress();
        if (code == 0) {
            mBalance = data.num + "";
            mDeal_once_price.setText(NumUtils.subZeroAndDot(mBalance) + "Hash");
//            availableTv.setText(getResources().getString(R.string.available) + " " + data.num + " Hash");
        } else if (code == -1) {
            if (Constants.isL == 0) {
                Constants.isL = 1;
                ToastUtils.showShortToast(msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(getActivity(), LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        } else if (code == 3080005) {
            if (Constants.isL == 0) {
                Constants.isL = 1;
                ToastUtils.showShortToast(getString(R.string.account_status));
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(getActivity(), LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        } else {
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }
    }

    @Override
    public void onFailure() {
        hideProgress();
        mPayRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onIsFaOrder(int code, String msg_cn, String bean) {
        if (code == 0) {
            if (bean.equals("1")) {
                if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                    Bundle bundle = new Bundle();
                    bundle.putInt("hash_market_type", 0);
                    ActivityUtils.next(getActivity(), ReleaseOrderActivity.class, bundle);
                } else {//未导入私钥 ，去设置
                    Dialog dialog = new Dialog(getContext(), R.style.MyDialog);
                    View inflate = LayoutInflater.from(getContext()).inflate(R.layout.dialog_import_sub_wallet, null);
                    dialog.setContentView(inflate);
                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putString("accountName", mMainAccount);
                            ActivityUtils.next(getActivity(), ImportAccountActivity.class, bundle, false);
                        }
                    });
                    dialog.show();
                }
            } else {
                AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.hash_is_fa_order));
            }
        } else {
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }
    }

    @OnClick({R.id.releaseTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.releaseTv:
                presenter.getIsOrder();
//                if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
//                    Bundle bundle = new Bundle();
//                    bundle.putInt("hash_market_type", 0);
//                    ActivityUtils.next(getActivity(), ReleaseOrderActivity.class, bundle);
//                } else {//未导入私钥 ，去设置
//                    Dialog dialog = new Dialog(getContext(), R.style.MyDialog);
//                    View inflate = LayoutInflater.from(getContext()).inflate(R.layout.dialog_import_sub_wallet, null);
//                    dialog.setContentView(inflate);
//                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
//                    inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                            Bundle bundle = new Bundle();
//                            bundle.putString("accountName", mMainAccount);
//                            ActivityUtils.next(getActivity(), ImportAccountActivity.class, bundle, false);
//                        }
//                    });
//                    dialog.show();
//                }
                break;
        }
    }
}
