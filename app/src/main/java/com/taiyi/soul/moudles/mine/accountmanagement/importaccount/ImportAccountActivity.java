package com.taiyi.soul.moudles.mine.accountmanagement.importaccount;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.blockchain.cypto.ec.EosPrivateKey;
import com.taiyi.soul.blockchain.util.ChainUtil;
import com.taiyi.soul.gen.WalletBeanDao;
import com.taiyi.soul.moudles.mine.bean.AccountListBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.moudles.scancode.ScanCodeActivity;
import com.taiyi.soul.utils.AESOperator;
import com.taiyi.soul.utils.EncryptUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.mytreeview.bean.FileBean;

import org.bitcoinj.core.ECKey;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.ljp.permission.PermissionItem;

public class ImportAccountActivity extends BaseActivity<ImportAccountView, ImportAccountPresent> implements ImportAccountView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.rightIv)
    ImageView rightIv;
    @BindView(R.id.accountNameEt)
    EditText accountNameEt;
    @BindView(R.id.mnemonicTv)
    TextView mnemonicTv;
    @BindView(R.id.privateKeyTv)
    TextView privateKeyTv;
    @BindView(R.id.et)
    EditText et_key;
    @BindView(R.id.top)
    ConstraintLayout top;


    //0---助记词，1---私钥
    private int type = 0;


    private String mAccount_private_key = null;
    private String mAccount_public_key = null;

    private EosPrivateKey mPrivateKey;

    private WalletBean walletBean = new WalletBean();

    private ArrayList<FileBean> account_size ;

    private ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
    private int isHaveKey;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_import_account;
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }


    @Override
    public ImportAccountPresent initPresenter() {
        return new ImportAccountPresent(this);
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.import_account));
        rightIv.setImageResource(R.mipmap.mine_create_account_scan_iv);
        rightIv.setVisibility(View.VISIBLE);
        mnemonicTv.setSelected(true);



        account_size = (ArrayList<FileBean>) getIntent().getSerializableExtra("accounts");
        isHaveKey = getIntent().getExtras().getInt("isHaveKey", 0);
        if (null==account_size || account_size.size() == 0){
            presenter.getAccountList(ImportAccountActivity.this);
        }

        int loginType = Utils.getSpUtils().getInt("loginType");
        //通过手机号和钱包类型查询有无账户
        if (loginType==1) {
            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(Utils.getSpUtils().getString("email", ""))).build().list();
            if (null!=list) {
                if(list.size()>0)
                walletBean = list.get(list.size()-1);
            }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(Utils.getSpUtils().getString("email", ""))).build().unique();
        }else {
            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(Utils.getSpUtils().getString("phoneNumber", ""))).build().list();
            if (null!=list) {
                if(list.size()>0)
                walletBean = list.get(list.size()-1);
            }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(Utils.getSpUtils().getString("phoneNumber", ""))).build().unique();

        }

    }


    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.rightIv, R.id.mnemonicTv, R.id.privateKeyTv, R.id.importTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightIv:
                List<PermissionItem> permissonItems = new ArrayList<PermissionItem>();
                permissonItems.add(new PermissionItem(Manifest.permission.CAMERA, getString(R.string.camer), R.drawable.permission_ic_camera));
                if (Utils.getPermissions(permissonItems, getString(R.string.open_camer_scan))) {
                    Bundle bundle = new Bundle();
                    bundle.putString("from", "import_account");
                    ActivityUtils.next(ImportAccountActivity.this, ScanCodeActivity.class, bundle, 100);
                }
                break;
            case R.id.mnemonicTv:
                et_key.setText("");
                et_key.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1000)});
                type = 0;
                mnemonicTv.setSelected(true);
                privateKeyTv.setSelected(false);
                break;
            case R.id.privateKeyTv:
                et_key.setText("");
                et_key.setFilters(new InputFilter[]{new InputFilter.LengthFilter(51)});
                type = 1;
                mnemonicTv.setSelected(false);
                privateKeyTv.setSelected(true);
                break;
            case R.id.importTv:
                if (TextUtils.isEmpty(accountNameEt.getText().toString())){
                    toast("请输入账户名");
                    return;
                }

                if (TextUtils.isEmpty(et_key.getText().toString())){
                    if (type == 0) {//助记词
                        toast("请输入助记词");
                    }else {
                        toast("请输入私钥");
                    }
                    return;
                }
                String s1 = accountNameEt.getText().toString();
                if (s1.length()!=8) {
                    toast("賬戶名格式不正確");
                    return;
                }

                int s = 0;//无可用账户
                for (int i = 0; i < account_size.size(); i++) {
                    if ((s1+ Constants.ACCOUNT_SUFFIX).equals(account_size.get(i).getUsername())){
                        s++;
                    }

                }

                if ( s == 0){
                    toast("未找到該账户信息");
                    return;
                }
                privateKeyImport();
                break;
        }
    }



    private void privateKeyImport(){

        if (type == 0){//助记词

            ECKey genECKey = ChainUtil.genECKey(et_key.getText().toString(), "");
            mPrivateKey = new EosPrivateKey(genECKey.getPrivKeyBytes());
            mAccount_private_key = mPrivateKey.toWif();

        }else {//私钥
            mAccount_private_key = et_key.getText().toString();
            try {
                mPrivateKey = new EosPrivateKey(mAccount_private_key);
            } catch (Exception e) {
                e.printStackTrace();
                toast(getString(R.string.private_format_err));
                return;
            }

        }

        mAccount_public_key = mPrivateKey.getPublicKey().toString();
        showProgress();
        presenter.getAccountInfoData(accountNameEt.getText().toString()+Constants.ACCOUNT_SUFFIX);
//        presenter.getAccounts(mAccount_public_key);

    }

    @Override
    public void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean) {

        String chainAccountOwnerKey = null;
        String chainAccountActiveKey = null;
        for (int i = 0; i < blockChainAccountInfoBean.getPermissions().size(); i++) {
            if (blockChainAccountInfoBean.getPermissions().get(i).getPerm_name().equals("owner")) {
                chainAccountOwnerKey = blockChainAccountInfoBean.getPermissions().get(i).getRequired_auth().getKeys().get(0).getKey();
            } else {
                chainAccountActiveKey = blockChainAccountInfoBean.getPermissions().get(i).getRequired_auth().getKeys().get(0).getKey();
            }
        }


        if (chainAccountOwnerKey.equals(mAccount_public_key) || chainAccountActiveKey.equals(mAccount_public_key)){

            ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
            if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
                accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
            }


            for (int i = 0; i < accountInfoBeanArrayList.size(); i++){
                if (accountInfoBeanArrayList.get(i).getAccount_name().equals(accountNameEt.getText().toString())){
                    accountInfoBeanArrayList.remove(accountInfoBeanArrayList.get(i));
                    break;
                }
            }


            AccountInfoBean accountInfoBean = new AccountInfoBean();

            accountInfoBean.setAccount_name(accountNameEt.getText().toString()+Constants.ACCOUNT_SUFFIX);

            if (mAccount_public_key.equals(chainAccountOwnerKey)) {

                try {
                    accountInfoBean.setAccount_owner_private_key(EncryptUtil.getEncryptString(mAccount_private_key, Utils.getSpUtils().getString("loginPwd","")));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                }
                accountInfoBean.setAccount_owner_public_key(mAccount_public_key);
            }

            //判断传过来的是ownerKey，还是activeKey
            if (mAccount_public_key.equals(chainAccountActiveKey)) {

                try {
                    accountInfoBean.setAccount_active_private_key(EncryptUtil.getEncryptString(mAccount_private_key, Utils.getSpUtils().getString("loginPwd","")));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                }
                accountInfoBean.setAccount_active_public_key(mAccount_public_key);

            }


            if (type == 0){
                accountInfoBean.setAccount_gen_mnemonic(AESOperator.getInstance().encrypt(et_key.getText().toString()));
            }


            if (walletBean != null) {
                accountInfoBeanArrayList.add(accountInfoBean);
                walletBean.setAccount_info(new Gson().toJson(accountInfoBeanArrayList));
                MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
                MyApplication.getInstance().setWalletBean(walletBean);
//                Log.i("dasdsadasdasd","--------"+walletBean.toString());
                toast(getString(R.string.imported_successfully));
                finish();
            }

        }else {

            if (type == 0){
                toast(getString(R.string.incorrect_mnemonic));
            }else {
                toast(getString(R.string.incorrect_private_key));
            }
        }

        hideProgress();


    }

    @Override
    public void getDataHttpFail(String msg) {
        hideProgress();
        toast(msg);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == 100) {

            if (data.getStringExtra("key_word") != null) {
                et_key.setText(data.getStringExtra("key_word"));
            }
        }
    }



    @Override
    public void getAccountHttp(ArrayList<String> account_names) {

//        hideProgress();
//
//        if (account_names != null && account_names.size()>0){
//
//            int s = 0;
//            for (int i = 0; i < account_names.size(); i++){
//                if (account_names.get(i).equals(accountNameEt.getText().toString())){
//                    s++;
//                }
//            }
//
//            if (s != 0){
//
//                showProgress();
//                presenter.getAccountInfoData(accountNameEt.getText().toString());
//
//            }else {
//                toast("私钥有误");
//            }
//
//
//        }else {
//            //通过公钥没有查到对应的账户
//            toast("私钥有误");
//        }

    }

    @Override
    public void getAccountListSuccess(List<AccountListBean> list) {

        if (list != null && list.size()>0) {
            account_size = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                FileBean fileBean = new FileBean(list.get(i).getId() + "", list.get(i).getParentId() + "", list.get(i).getAccount(), list.get(i).getInvitationCode(), list.get(i).getCreateTime());
                account_size.add(fileBean);
            }

        }

        hideProgress();
    }

}
