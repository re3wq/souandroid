package com.taiyi.soul.moudles.main.assets.currency.currencydetail.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.adapter.CurrencyDetailsTwoAdapter;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.CurrencyFragmentBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.UsdnOrNgkTransferRecordBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.present.CurrencyDetailsFPresent;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.present.CurrencyDetailsFView;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\19 0019 14:34
 * @Author : yuan
 * @Describe ：币种详情下方列表-全部
 */
public class CurrencyDetailsTwoFragment extends BaseFragment<CurrencyDetailsFView, CurrencyDetailsFPresent> implements CurrencyDetailsFView, PullRecyclerView.PullLoadMoreListener {


    @BindView(R.id.recyclerView)
    PullRecyclerView recyclerView;
    @BindView(R.id.noDataLl)
    LinearLayout noDataLl;
    private CurrencyDetailsTwoAdapter mFragmentAdapter;
    private int pageNo = 1;
    private String coinName;
    private String coinUrl;

    public CurrencyDetailsTwoFragment(String coinName,String coinUrl) {
        this.coinName = coinName;
        this.coinUrl=coinUrl;
    }
    public CurrencyDetailsTwoFragment() {

    }


    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_currency;
    }

    @Subscribe
    public void refresh(String msg){
        if ("transferSuccess".equals(msg)){
            recyclerView.refreshWithPull();
        }
    }
    @Override
    public CurrencyDetailsFPresent initPresenter() {
        return new CurrencyDetailsFPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if(EventBus.getDefault().isRegistered(false))
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void even(String event) {
//        Log.e("fragment==", event);
        if ("refresh_anim".equals(event)) {
            mFragmentAdapter.notifyDataSetChanged();
            recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_anim));
            recyclerView.scheduleLayoutAnimation();
        }
    }

    @Override
    protected void initData() {
        //实例化PullRecyclerView相关信息
//        presenter.getFindOrderData(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setOnPullLoadMoreListener(this);
        recyclerView.setIsRefreshEnabled(true);
        recyclerView.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new CurrencyDetailsTwoAdapter(getContext(),coinUrl);
        mFragmentAdapter.setPullRecyclerView(recyclerView);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mFragmentAdapter);
//        recyclerView.refreshWithPull();
//        recyclerView.setPullLoadMoreCompleted();
        pageNo=1;
        presenter.getDatas(getActivity(), coinName, pageNo, "0");



    }


    @Override
    public void initEvent() {


    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
//        Log.e("fragment==", "hidden");
        if (!hidden) {
            presenter.getDatas(getActivity(), coinName, pageNo, "0");


        }
    }

    /************************************Other methods****************************************************/


    /************************************Refresh and load more****************************************************/

    @Override
    public void onRefresh() {
        pageNo = 1;
        presenter.getDatas(getActivity(), coinName, pageNo, "0");
    }

    @Override
    public void onLoadMore() {
        pageNo++;
        presenter.getDatas(getActivity(), coinName, pageNo, "0");
    }


    /************************************Call Interface callback****************************************************/
    @Override
    public void getSuccess(int status, String msg, CurrencyFragmentBean bean) {
        if(status==200){
            if (pageNo == 1) {
                mFragmentAdapter.setDatas(bean.list);
            } else {
                mFragmentAdapter.addDatas(bean.list);
            }
        }else {
            noDataLl.setVisibility(View.VISIBLE);
        }
        recyclerView.setPullLoadMoreCompleted();
    }

    @Override
    public void getUsdnOrNgkRecordSuccess(UsdnOrNgkTransferRecordBean usdnOrNgkTransferRecordBean) {

    }

    @Override
    public void onFailure() {
        hideProgress();
        noDataLl.setVisibility(View.VISIBLE);
    }

    @Override
    public void getCoinListSuccess(List<CoinListBean> coinListBeanList) {

    }

    @Override
    public void getCoinListFailure(String errorMsg) {

    }
}
