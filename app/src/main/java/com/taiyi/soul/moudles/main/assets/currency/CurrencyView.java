package com.taiyi.soul.moudles.main.assets.currency;

import com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;

import java.util.List;

public interface CurrencyView {
    void getListSuccess(List<CurrencyListBean> list);
    void onFailure(String errorMsg);
    void getBalanceSuccess(String symbol,String balance);
    void getSelectCoinRateSuccess(List<SelectCoinRateBean>list);
    void getSelectCoinRateFailure(String errorMsg);
    void getRateSuccess(String symbol,String rate);
    void getRateFailure(String errorMsg);
}
