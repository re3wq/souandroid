package com.taiyi.soul.moudles.main.assets.currency;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class CurrencyListAdapter extends BaseRecyclerAdapter<CurrencyListBean>  {


    private PullRecyclerView pullRecyclerView;
    private Context context;
    public CurrencyListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.assets_currency_item_layout;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final CurrencyListBean data) {
        TextView coinNameTv = getView(holder,R.id.coinNameTv);
        TextView quantityTv = getView(holder,R.id.quantityTv);
        TextView valueTv = getView(holder,R.id.valueTv);
        TextView coinRateTv = getView(holder,R.id.coinRateTv);
        ImageView coinIv = getView(holder,R.id.coinIv);
        coinNameTv.setText(data.getCoinName());
        String quantity = "";
        if(data.getCoinName().equals("BTC")){
            quantity = new BigDecimal(data.getMoneyandfor()).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
        }else  if(data.getCoinName().equals("ETH")){
            quantity = new BigDecimal(data.getMoneyandfor()).setScale(6,BigDecimal.ROUND_DOWN).toPlainString();
        }else {
            quantity = new BigDecimal(data.getMoneyandfor()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
        }
        quantity = NumUtils.subZeroAndDot(quantity);
        quantityTv.setText(quantity);
        String coin_symbol = Utils.getSpUtils().getString("coin_symbol");

        String replace = data.getMfcon().replace(coin_symbol, "");
        String s = new BigDecimal(replace).setScale(2,BigDecimal.ROUND_HALF_UP).toPlainString();

        valueTv.setText("≈"+ coin_symbol + s);
        coinRateTv.setText("≈"+ coin_symbol +new BigDecimal(data.getPrice()).setScale(2,BigDecimal.ROUND_HALF_UP).toPlainString());

        Glide.with(MyApplication.getInstance()).load(data.getUrl()).into(coinIv);

    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }

}

