package com.taiyi.soul.moudles.mine.about;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import okhttp3.Response;

public class AboutPresent extends BasePresent<AboutView> {
    public void getInfo(Activity activity){
        HttpUtils.postRequest(BaseUrl.ABOUTUS, this, "", new JsonCallback<BaseResultBean<AboutBean>>() {
            @Override
            public BaseResultBean<AboutBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<AboutBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                if (response.body().code == 602) {//登录失效
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    } else {
                        view.getInfoSuccess(response.body().code,response.body().msg_cn,response.body().data);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<AboutBean>> response) {
                super.onError(response);
            }
        });
    }
}
