package com.taiyi.soul.moudles.main.assets.currency.currencydetail.present;

import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.CurrencyFragmentBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.UsdnOrNgkTransferRecordBean;

import java.util.List;

public interface CurrencyDetailsFView {
    void getSuccess(int status, String msg, CurrencyFragmentBean assetsBean);

    void getUsdnOrNgkRecordSuccess(UsdnOrNgkTransferRecordBean usdnOrNgkTransferRecordBean);

    void onFailure();

    void getCoinListSuccess(List<CoinListBean> coinListBeanList);
    void getCoinListFailure(String errorMsg);

}
