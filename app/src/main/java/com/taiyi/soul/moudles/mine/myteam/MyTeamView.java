package com.taiyi.soul.moudles.mine.myteam;

import com.taiyi.soul.moudles.mine.bean.MyTeamBean;
import com.taiyi.soul.moudles.mine.bean.MyTeamPerformanceListBean;

import java.util.List;

public interface MyTeamView {
    void onSuccess(MyTeamBean myTeamBean);
    void onFailure(String errorMsg);
    void loginInvalid();
    void getPerformanceSuccess(MyTeamPerformanceListBean myTeamPerformanceListBean);
    void getTeamSuccess(int status, String msg, List<Object> date);

}
