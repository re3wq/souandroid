package com.taiyi.soul.moudles.main.home.mall.bean;

import java.util.List;

public class MallFirstBean {

    private List<CarousellistBean> carousellist;
    private List<CatelistBean> catelist;

    public List<CarousellistBean> getCarousellist() {
        return carousellist;
    }

    public void setCarousellist(List<CarousellistBean> carousellist) {
        this.carousellist = carousellist;
    }

    public List<CatelistBean> getCatelist() {
        return catelist;
    }

    public void setCatelist(List<CatelistBean> catelist) {
        this.catelist = catelist;
    }

    public static class CarousellistBean {
        /**
         * imgurl : http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200510/dc8eed3ef2c840e8a9b133c9c7c6101c.jpg
         * carousel_id : 11
         * carousel_type : 1
         * apptype : 3
         * sort : 1
         * hrefid : 9
         */

        private String imgurl;
        private int carousel_id;
        private int carousel_type;
        private String apptype;
        private int sort;
        private String hrefid;

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public int getCarousel_id() {
            return carousel_id;
        }

        public void setCarousel_id(int carousel_id) {
            this.carousel_id = carousel_id;
        }

        public int getCarousel_type() {
            return carousel_type;
        }

        public void setCarousel_type(int carousel_type) {
            this.carousel_type = carousel_type;
        }

        public String getApptype() {
            return apptype;
        }

        public void setApptype(String apptype) {
            this.apptype = apptype;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public String getHrefid() {
            return hrefid;
        }

        public void setHrefid(String hrefid) {
            this.hrefid = hrefid;
        }
    }

    public static class CatelistBean {
        /**
         * ifindex : 1
         * introimg : http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200426/0c01962d437f4f65a498dc808c10b28d.jpg
         * createtime : 2020-03-27 10:27:10
         * ifremen : 1
         * categoryname : 发现好货
         * introname : 品质好物
         * comlist : [{"flag":"2","commodityid":"1a8711b7043e457885b58eb8fd5333b3","salenum":1,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"301bc67d14fd40b4afe1bd1ef40570b0","zhuanstock":99,"zhuanprice":152,"zhuanyunfei":0,"visitgoodsnum":0,"price":152,"businessuserid":"90","brandid":112,"sanfurl":"https://detail.tmall.com/item.htm?spm=a220m.1000858.1000725.1.44016418vD0Ab0&id=594571242517&user_id=2549194080&cat_id=2&is_b=1&rn=d10ee08fa964c520a93d0242e139936a\r\n\r\n","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":98,"stock":99,"delflag":"1","hybvprice":27,"classificationidname":"净水壶","recommendflag":"2","stockzong":99,"createtime":"2020-05-12 11:43:09","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"231","bvprice":54,"sanftype":"1","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/e939f86a0ba3426baf30405c9bc3f033.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/a9f3cd25980346f1b191d753d06fa9f9.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/9b865c2346614a27b96b735bca74b858.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/abcbb8eacb3a4d99a8ef9512614a59a2.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/fb5547a4085c48a484190818e89bbb07.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/6c0e0f023e234097b58ba97e51458fe1.jpg","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/51b1fa19c56a47f3b503aae640ad5e65.jpg\" alt=\"\" />","videourl":"","name":"TCL超强净化水壶TJ-HUF101A\r\n","businessname":"极致之选","qiangurl":"","hyprice":125,"categoryname":"家用电器","combz":"\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/a01fa12f23e54defac0cce2327bc9bf0.jpg"},{"flag":"2","commodityid":"24c3da1af6304cca8a936fe8bfbda14b","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"dc4c031a21584f988dd8d96981871981","zhuanstock":100,"zhuanprice":138,"zhuanyunfei":0,"visitgoodsnum":0,"price":138,"businessuserid":"90","brandid":112,"sanfurl":"https://item.jd.com/64313639111.html\r\n\r\n","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":90,"stock":100,"delflag":"1","hybvprice":24,"classificationidname":"电热水壶","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:41:19","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"186","bvprice":48,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/4dace6aa9696443cbeb7300641334eea.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/32e4e0d1b5bc440b9a9875d085ccc1fe.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/41af6d7a938947e1bdc6df167837f933.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/f016133c0fba40f4904abf565e97a8ee.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/a079ef05e00145ba8f24c2e4d9ef0274.JPG","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/1033a07009f8467d838d4bfd347b9b28.jpg\" alt=\"\" />","videourl":"","name":"TCL金刚电热水壶TA-KB183A\r\n","businessname":"极致之选","qiangurl":"","hyprice":114,"categoryname":"家用电器","combz":"TCL金刚电热水壶TA-KB183ATCL金刚电热水壶TA-KB183ATCL金刚电热水TCL金刚电热水壶TA-KB183ATCL金刚电热水壶TA-KB183A壶TA-KB183A","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/0f200ef220464ab9abcd003a79757981.jpg"},{"flag":"2","commodityid":"c2c7739d92c049cfb7c2d0b343ac1fdf","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"497616c640a64b3687c8938480322f86","zhuanstock":100,"zhuanprice":207,"zhuanyunfei":0,"visitgoodsnum":0,"price":207,"businessuserid":"90","brandid":112,"sanfurl":"http://www.liyinka.com/goods.php?id=2160","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":130,"stock":100,"delflag":"1","hybvprice":39,"classificationidname":"锅具","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:39:02","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"290","bvprice":77,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/8858364e59e040b7b52ed56e14f74007.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/4ab02e93914144549f4857ab2d466a2f.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/f1444336fc554872898f242194350bb2.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/be7e54ce051347b48200b1cc7e57075a.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/8b628640f51c44b8b0b17ad4e27ce4cb.JPG","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/6c0b9ed7a84a4c40b74b112c92a1be6d.jpg\" alt=\"\" />","videourl":"","name":"TCL私房养生炖盅ZJ102A\r\n","businessname":"极致之选","qiangurl":"","hyprice":169,"categoryname":"家用电器","combz":"TCL私房养生炖盅ZJ102A\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/bccf8b313e884772a4c71973dfc0e07c.jpg"},{"flag":"2","commodityid":"56b5b3bde367414bb064167b86fbea5f","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"9c857d679bd04d15948ea3815b6bd9c4","zhuanstock":100,"zhuanprice":175,"zhuanyunfei":0,"visitgoodsnum":0,"price":175,"businessuserid":"90","brandid":112,"sanfurl":"https://www.hhlme.com/index.php/item.html?item_id=5598","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":145,"stock":100,"delflag":"1","hybvprice":15,"classificationidname":"电饭煲","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:34:33","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"188","bvprice":30,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/859925c1ab0344bc9502a85505828f10.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/bf619042a6724565a2541509eb4630cd.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/eee9f8d3b2674672beb2d42f1d9547f8.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/769ac6cd5fcb4b20819b6a2210cd8051.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/7c5cd64e7cb94fbc987ce04e2fa4be90.JPG","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/fccb6d3464eb44358cdcea745ab6e6b2.jpg\" alt=\"\" />","videourl":"","name":"TCL古秘养生煲TH-MJ351A\r\n","businessname":"极致之选","qiangurl":"","hyprice":160,"categoryname":"家用电器","combz":"TCL古秘养生煲TH-MJ351A\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/fd30f31a8d824bf489b0c3a9a4f294a0.jpg"},{"flag":"2","commodityid":"dc4a36ab14124402abd1152085611d24","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"150e010b8fcb4740a4b17ea010e04050","zhuanstock":100,"zhuanprice":176,"zhuanyunfei":0,"visitgoodsnum":0,"price":176,"businessuserid":"90","brandid":112,"sanfurl":"https://detail.tmall.com/item.htm?spm=a1z10.3-b.w4011-11428530234.22.66ea26a5NMQyUR&id=606527509282&rn=8a0cb61f22332373e39e04c308fda67b&abbucket=9\r\n\r\n","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":136,"stock":100,"delflag":"1","hybvprice":20,"classificationidname":"酵素机","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:31:56","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"318","bvprice":40,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/2c4f1023b3a846b9a5831e4118793572.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/afc2f4fd195f4a5d8a3785fd47de29d2.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/f0cb67f6ca6c4f2b80096c3301fa23fe.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/e32574cd0145427fa0e8000f20217d46.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/ef6bc99e20af4021a3142317dc719295.jpg","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/e3883b2d10bc43399fa1de2de57541ce.jpg\" alt=\"\" />","videourl":"","name":"TCL家酿酵素机TU-D15H7\r\n","businessname":"极致之选","qiangurl":"","hyprice":156,"categoryname":"家用电器","combz":"TCL家酿酵素机TU-D15H7\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/6410575c9bb741c297880abb45ddabca.jpg"},{"flag":"2","commodityid":"b5f93ef46fd84b9d988edbd464ab1f39","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"306b40dafeea45cfa9c7bb8f3a4db41c","zhuanstock":100,"zhuanprice":108,"zhuanyunfei":0,"visitgoodsnum":0,"price":108,"businessuserid":"90","brandid":112,"sanfurl":"http://www.dada360.com/116878.html\r\n","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":73,"stock":100,"delflag":"1","hybvprice":18,"classificationidname":"酸奶机","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:28:33","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"218","bvprice":35,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/6bb85642fdde46a1ac76041a635bf24c.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/e9f8c2f9e4b14fe495ad8b63d3b5c676.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/d13700a3a77b4c7f862f0bf75f6ae278.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/5cf4e042d7c840a09295cd98f9bae559.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/433e6a79cb4344b1837b8399ea621702.jpg","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/aa75bad6494a419695543c22fb4c2e40.jpg\" alt=\"\" />","videourl":"","name":"TCL清醇酸奶机TU-D15H5\r\n","businessname":"极致之选","qiangurl":"","hyprice":91,"categoryname":"家用电器","combz":"TCL清醇酸奶机TU-D15H5\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/26327774ee1e4b0da1290612771e07e1.jpg"},{"flag":"2","commodityid":"74e32bdda1fe41a68ae65ed3542cc81f","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"68f4221559eb4d3ab72e69db8c14b0a6","zhuanstock":100,"zhuanprice":118,"zhuanyunfei":0,"visitgoodsnum":0,"price":118,"businessuserid":"90","brandid":112,"sanfurl":"http://www.dada360.com/116874.html\r\n\r\n","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":77,"stock":100,"delflag":"1","hybvprice":20,"classificationidname":"电风扇","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:25:19","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"226","bvprice":41,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/eabec879163e4c5593dd1adf3258c692.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/3ed25676be984b3d925481b2f7505390.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/2fa81c86f1d14d379e523cccb9f0da89.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/f2bf27bf86534526ab5169995ac0f940.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/905996f4d6c1435bacb758db11cf18f4.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/8ad76894030c438a9ef125ebfd01a7f5.jpg","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/7f3810edef43405587d588377b05b2bb.jpg\" alt=\"\" />","videourl":"","name":"TCL鸿运静音电风扇TS-T201A \r\n","businessname":"极致之选","qiangurl":"","hyprice":97,"categoryname":"家用电器","combz":"TCL鸿运静音电风扇TS-T201A \r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/0d03924b90014c7693f94a0ae8f21616.jpg"},{"flag":"2","commodityid":"2a7cff2798bc46bdb9375a181feee349","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"95e79f5e67e24ceebddd88301bfa0a3c","zhuanstock":100,"zhuanprice":304,"zhuanyunfei":0,"visitgoodsnum":0,"price":304,"businessuserid":"90","brandid":112,"sanfurl":"https://item.jd.com/62796459933.html\r\n","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":260,"stock":100,"delflag":"1","hybvprice":22,"classificationidname":"空气加湿器","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:22:15","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"988","bvprice":44,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/8c18bf64a0e442a88b9e3ae3cc48e5b3.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/1101d47f8098455abc9469e8c9065681.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/ebeaa0dbc85d49a2b1e2554c56c74a22.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/31eca4515de5485ca212960eddef98f5.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/dd17328aa2a44a72a796654442fa70b4.jpg","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/3d2d64057fe7417ea2e4ab7dbf6c0742.jpg\" alt=\"\" />","videourl":"","name":"TCL智能空气净化加湿器TE-C65F1\r\n","businessname":"极致之选","qiangurl":"","hyprice":282,"categoryname":"家用电器","combz":"TCL智能空气净化加湿器TE-C65F1\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/fc7354951b554ca585cbc8f4511a4f06.jpg"},{"flag":"2","commodityid":"2631bc861a164fc6853e79303244cd75","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"373a352eeaee4319b2380f6c07c092fa","zhuanstock":100,"zhuanprice":350,"zhuanyunfei":0,"visitgoodsnum":0,"price":350,"businessuserid":"90","brandid":112,"sanfurl":"https://item.jd.com/64032932164.html\r\n\r\n","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":292,"stock":100,"delflag":"1","hybvprice":29,"classificationidname":"除螨仪","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:17:52","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"568","bvprice":58,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/5d924fc4d3084bbf97b87b92603d8aa2.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/6d7bff8ca18c4eeca7f1ee16b17e2935.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/80fc7668a1c54c9e8bc0ca78c5321d9c.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/166a5761a6a1446e98d2438668882b0d.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/3176eb1fa99748ceb8d86f9dc31cb5ef.jpg","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/42ff0003243e4058ba50639c7c259114.jpg\" alt=\"\" />","videourl":"","name":"TCL锐芯除螨仪TCM-XPC30\r\n","businessname":"极致之选","qiangurl":"","hyprice":321,"categoryname":"家用电器","combz":"TCL锐芯除螨仪TCM-XPC30\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/6043e0aa4d934d96bf5d84d229227e9d.jpg"},{"flag":"2","commodityid":"78159f8269e941d98a7fa7c52e9c533f","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"4bd4f5773e564f3bb8ba790b422f7bef","zhuanstock":100,"zhuanprice":525,"zhuanyunfei":0,"visitgoodsnum":0,"price":525,"businessuserid":"90","brandid":112,"sanfurl":"https://item.jd.com/63358264292.html\r\n","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":478,"stock":100,"delflag":"1","hybvprice":23,"classificationidname":"扫地机器人","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:15:56","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"1599","bvprice":47,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/68979a47ef82434b8385a8fd4e72d2ce.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/2830f48ccbab464aa5bc599a947fa60d.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/097440aa48e74d0494a659347b96f920.jpg","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/ba32301cad274ff99d19f34ba01cc03c.jpg\" alt=\"\" />","videourl":"","name":"TCL全自动智能吸尘器TXC-25JK\r\n","businessname":"极致之选","qiangurl":"","hyprice":501,"categoryname":"家用电器","combz":"TCL全自动智能吸尘器TXC-25JK\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/c9abd44995ea42b695f6e59f6c20721f.jpg"},{"flag":"2","commodityid":"3a4bf36e4a064e23a1c8206fe650b590","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"7242d295fee64c12a17aca15a642bb35","zhuanstock":100,"zhuanprice":324,"zhuanyunfei":0,"visitgoodsnum":0,"price":324,"businessuserid":"90","brandid":112,"sanfurl":"https://www.hhlme.com/index.php/item.html?item_id=5621","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":284,"stock":100,"delflag":"1","hybvprice":20,"classificationidname":"烤箱","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:10:51","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"338","bvprice":40,"sanftype":"1","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/9f5b6ade83a04b52bd003d5e2b663eb8.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/3d6fcaa784ef48b2bb3ef19d70751718.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/3679161e0785467d8c2af53d12e9575e.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/5d020ff2e9d64b28abb68476c99ec2d8.JPG,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/b24cbb17dff24aae9ed29a5bb7aa8463.JPG","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/e2484ff2aa864fd985baaa266c3f6e9d.jpg\" alt=\"\" />","videourl":"","name":"TCL食尚·早餐吧TKX-J05051A\r\n","businessname":"极致之选","qiangurl":"","hyprice":304,"categoryname":"家用电器","combz":"TCL食尚·早餐吧TKX-J05051A\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/b764647fec8448398bfdcb33f222ca1b.jpg"},{"flag":"2","commodityid":"f8ed7f0a68a64ffab4c7bb501018e59d","salenum":0,"type":"1","brandnamezz":"TCL","brandname":"TCL","commodityspecid":"9fbe50f5d8a84fe6b83a3f706d956243","zhuanstock":100,"zhuanprice":206,"zhuanyunfei":0,"visitgoodsnum":0,"price":206,"businessuserid":"90","brandid":112,"sanfurl":"http://www.epin.com/goods.php?id=249","logo":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg","lableidname":"新品上架","cashPrice":155,"stock":100,"delflag":"1","hybvprice":26,"classificationidname":"烤箱","recommendflag":"2","stockzong":100,"createtime":"2020-05-12 11:07:16","introimg":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg","sanfprice":"229","bvprice":51,"sanftype":"2","youxiaoday":"14","comimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/a6a8ff3710fe4b61b726765484b457c0.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/7f28b419423e4b979e1f47d8addebbb0.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/aea773c62ea2464faec1c786fce2f3c1.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/be2faa274dbd4d7a8605477d52e87d72.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/611ec72d2fb3423f9788c813bfdf83bd.jpg","detailimgurl":"<img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/297a37d05487413db8ee5c5f481a0ef5.jpg\" alt=\"\" /><img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/a3ded631e67a4cc38559a429c954919f.jpg\" alt=\"\" /><img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/8ff0452032244cd6a1813a3395720e32.jpg\" alt=\"\" /><img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/70bc7de2b33a447fa827a51c852d7780.jpg\" alt=\"\" /><img src=\"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/84cfdcb40e774fb69a1d44303054a0e1.jpg\" alt=\"\" />","videourl":"","name":"TCL炫魅电烤箱TKX-J06A1\r\n","businessname":"极致之选","qiangurl":"","hyprice":181,"categoryname":"家用电器","combz":"TCL炫魅电烤箱TKX-J06A1\r\n","smallimgurl":"http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/71b45a11c50742ef8a7ec06cac5b7542.jpg"}]
         * sort : 1
         * type : 1
         * categoryid : 1
         */

        private String ifindex;
        private String introimg;
        private String createtime;
        private String ifremen;
        private String categoryname;
        private String introname;
        private String sort;
        private String type;
        private String categoryid;
        private List<ComlistBean> comlist;

        public String getIfindex() {
            return ifindex;
        }

        public void setIfindex(String ifindex) {
            this.ifindex = ifindex;
        }

        public String getIntroimg() {
            return introimg;
        }

        public void setIntroimg(String introimg) {
            this.introimg = introimg;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getIfremen() {
            return ifremen;
        }

        public void setIfremen(String ifremen) {
            this.ifremen = ifremen;
        }

        public String getCategoryname() {
            return categoryname;
        }

        public void setCategoryname(String categoryname) {
            this.categoryname = categoryname;
        }

        public String getIntroname() {
            return introname;
        }

        public void setIntroname(String introname) {
            this.introname = introname;
        }

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCategoryid() {
            return categoryid;
        }

        public void setCategoryid(String categoryid) {
            this.categoryid = categoryid;
        }

        public List<ComlistBean> getComlist() {
            return comlist;
        }

        public void setComlist(List<ComlistBean> comlist) {
            this.comlist = comlist;
        }

        public static class ComlistBean {
            /**
             * flag : 2
             * commodityid : 1a8711b7043e457885b58eb8fd5333b3
             * salenum : 1
             * type : 1
             * brandnamezz : TCL
             * brandname : TCL
             * commodityspecid : 301bc67d14fd40b4afe1bd1ef40570b0
             * zhuanstock : 99
             * zhuanprice : 152
             * zhuanyunfei : 0
             * visitgoodsnum : 0
             * price : 152
             * businessuserid : 90
             * brandid : 112
             * sanfurl : https://detail.tmall.com/item.htm?spm=a220m.1000858.1000725.1.44016418vD0Ab0&id=594571242517&user_id=2549194080&cat_id=2&is_b=1&rn=d10ee08fa964c520a93d0242e139936a
             * logo : http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/ef8c7252dd5b4ddd9bc626d98975dc55.jpg
             * lableidname : 新品上架
             * cashPrice : 98
             * stock : 99
             * delflag : 1
             * hybvprice : 27
             * classificationidname : 净水壶
             * recommendflag : 2
             * stockzong : 99
             * createtime : 2020-05-12 11:43:09
             * introimg : http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200508/8e940f0c232c4feb8cc9ec32e7c82119.jpg
             * sanfprice : 231
             * bvprice : 54
             * sanftype : 1
             * youxiaoday : 14
             * comimgurl : http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/e939f86a0ba3426baf30405c9bc3f033.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/a9f3cd25980346f1b191d753d06fa9f9.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/9b865c2346614a27b96b735bca74b858.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/abcbb8eacb3a4d99a8ef9512614a59a2.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/fb5547a4085c48a484190818e89bbb07.jpg,http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/6c0e0f023e234097b58ba97e51458fe1.jpg
             * detailimgurl : <img src="http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/20200512/51b1fa19c56a47f3b503aae640ad5e65.jpg" alt="" />
             * videourl :
             * name : TCL超强净化水壶TJ-HUF101A
             * businessname : 极致之选
             * qiangurl :
             * hyprice : 125
             * categoryname : 家用电器
             * combz :
             * smallimgurl : http://wlshop-app.oss-cn-beijing.aliyuncs.com/upload/carousel/20200512/a01fa12f23e54defac0cce2327bc9bf0.jpg
             */

            private String flag;
            private String commodityid;
            private String salenum;
            private String type;
            private String brandnamezz;
            private String brandname;
            private String commodityspecid;
            private String zhuanstock;
            private String zhuanprice;
            private String zhuanyunfei;
            private String visitgoodsnum;
            private String price;
            private String businessuserid;
            private int brandid;
            private String sanfurl;
            private String logo;
            private String lableidname;
            private String cashPrice;
            private int stock;
            private String delflag;
            private String hybvprice;
            private String classificationidname;
            private String recommendflag;
            private String stockzong;
            private String createtime;
            private String introimg;
            private String sanfprice;
            private String bvprice;
            private String sanftype;
            private String youxiaoday;
            private String comimgurl;
            private String detailimgurl;
            private String videourl;
            private String name;
            private String businessname;
            private String qiangurl;
            private String hyprice;
            private String categoryname;
            private String combz;
            private String smallimgurl;

            public String getFlag() {
                return flag;
            }

            public void setFlag(String flag) {
                this.flag = flag;
            }

            public String getCommodityid() {
                return commodityid;
            }

            public void setCommodityid(String commodityid) {
                this.commodityid = commodityid;
            }

            public String getSalenum() {
                return salenum;
            }

            public void setSalenum(String salenum) {
                this.salenum = salenum;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getBrandnamezz() {
                return brandnamezz;
            }

            public void setBrandnamezz(String brandnamezz) {
                this.brandnamezz = brandnamezz;
            }

            public String getBrandname() {
                return brandname;
            }

            public void setBrandname(String brandname) {
                this.brandname = brandname;
            }

            public String getCommodityspecid() {
                return commodityspecid;
            }

            public void setCommodityspecid(String commodityspecid) {
                this.commodityspecid = commodityspecid;
            }

            public String getZhuanstock() {
                return zhuanstock;
            }

            public void setZhuanstock(String zhuanstock) {
                this.zhuanstock = zhuanstock;
            }

            public String getZhuanprice() {
                return zhuanprice;
            }

            public void setZhuanprice(String zhuanprice) {
                this.zhuanprice = zhuanprice;
            }

            public String getZhuanyunfei() {
                return zhuanyunfei;
            }

            public void setZhuanyunfei(String zhuanyunfei) {
                this.zhuanyunfei = zhuanyunfei;
            }

            public String getVisitgoodsnum() {
                return visitgoodsnum;
            }

            public void setVisitgoodsnum(String visitgoodsnum) {
                this.visitgoodsnum = visitgoodsnum;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getBusinessuserid() {
                return businessuserid;
            }

            public void setBusinessuserid(String businessuserid) {
                this.businessuserid = businessuserid;
            }

            public int getBrandid() {
                return brandid;
            }

            public void setBrandid(int brandid) {
                this.brandid = brandid;
            }

            public String getSanfurl() {
                return sanfurl;
            }

            public void setSanfurl(String sanfurl) {
                this.sanfurl = sanfurl;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getLableidname() {
                return lableidname;
            }

            public void setLableidname(String lableidname) {
                this.lableidname = lableidname;
            }

            public String getCashPrice() {
                return cashPrice;
            }

            public void setCashPrice(String cashPrice) {
                this.cashPrice = cashPrice;
            }

            public int getStock() {
                return stock;
            }

            public void setStock(int stock) {
                this.stock = stock;
            }

            public String getDelflag() {
                return delflag;
            }

            public void setDelflag(String delflag) {
                this.delflag = delflag;
            }

            public String getHybvprice() {
                return hybvprice;
            }

            public void setHybvprice(String hybvprice) {
                this.hybvprice = hybvprice;
            }

            public String getClassificationidname() {
                return classificationidname;
            }

            public void setClassificationidname(String classificationidname) {
                this.classificationidname = classificationidname;
            }

            public String getRecommendflag() {
                return recommendflag;
            }

            public void setRecommendflag(String recommendflag) {
                this.recommendflag = recommendflag;
            }

            public String getStockzong() {
                return stockzong;
            }

            public void setStockzong(String stockzong) {
                this.stockzong = stockzong;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getIntroimg() {
                return introimg;
            }

            public void setIntroimg(String introimg) {
                this.introimg = introimg;
            }

            public String getSanfprice() {
                return sanfprice;
            }

            public void setSanfprice(String sanfprice) {
                this.sanfprice = sanfprice;
            }

            public String getBvprice() {
                return bvprice;
            }

            public void setBvprice(String bvprice) {
                this.bvprice = bvprice;
            }

            public String getSanftype() {
                return sanftype;
            }

            public void setSanftype(String sanftype) {
                this.sanftype = sanftype;
            }

            public String getYouxiaoday() {
                return youxiaoday;
            }

            public void setYouxiaoday(String youxiaoday) {
                this.youxiaoday = youxiaoday;
            }

            public String getComimgurl() {
                return comimgurl;
            }

            public void setComimgurl(String comimgurl) {
                this.comimgurl = comimgurl;
            }

            public String getDetailimgurl() {
                return detailimgurl;
            }

            public void setDetailimgurl(String detailimgurl) {
                this.detailimgurl = detailimgurl;
            }

            public String getVideourl() {
                return videourl;
            }

            public void setVideourl(String videourl) {
                this.videourl = videourl;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getBusinessname() {
                return businessname;
            }

            public void setBusinessname(String businessname) {
                this.businessname = businessname;
            }

            public String getQiangurl() {
                return qiangurl;
            }

            public void setQiangurl(String qiangurl) {
                this.qiangurl = qiangurl;
            }

            public String getHyprice() {
                return hyprice;
            }

            public void setHyprice(String hyprice) {
                this.hyprice = hyprice;
            }

            public String getCategoryname() {
                return categoryname;
            }

            public void setCategoryname(String categoryname) {
                this.categoryname = categoryname;
            }

            public String getCombz() {
                return combz;
            }

            public void setCombz(String combz) {
                this.combz = combz;
            }

            public String getSmallimgurl() {
                return smallimgurl;
            }

            public void setSmallimgurl(String smallimgurl) {
                this.smallimgurl = smallimgurl;
            }
        }
    }
}
