package com.taiyi.soul.moudles.main.assets.exchange;

import android.app.Dialog;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.liaoinstan.springview.widget.SpringView;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.EosSignDataManger;
import com.taiyi.soul.moudles.main.assets.exchange.adapter.BondRecordAdapter;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondExchangeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondFeeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondRecordBean;
import com.taiyi.soul.moudles.main.assets.exchange.present.BondPresent;
import com.taiyi.soul.moudles.main.assets.exchange.present.BondView;
import com.taiyi.soul.moudles.main.assets.view.BondPopupWindow;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.RedirectsBean;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\10 0010 15:13
 * @Author : yuan
 * @Describe ：债券兑换页面
 */
public class BondExchangeActivity extends BaseActivity<BondView, BondPresent> implements BondView{


    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_right_text)
    TextView mTvRightText;
    @BindView(R.id.bond_exchange)
    TextView mBondExchange;
//    @BindView(R.id.bond_recycle)
//    PullRecyclerView mBondRecycle;
 @BindView(R.id.bond_recycle)
    PullRecyclerView mBondRecycle;

    @BindView(R.id.bond_price)
    TextView mBondPrice;//当前价格
    @BindView(R.id.bond_refresh)
    ImageView mBondRefresh;//可兑换债券刷新
    @BindView(R.id.bond_redeemable)
    TextView mBondRedeemable;//可兑换债券
    @BindView(R.id.bond_text)
    TextView bond_text;//可兑换债券
    @BindView(R.id.bond_exchange_rate)
    TextView mBondExchangeRate;//兑换比例
    @BindView(R.id.bond_next_time)
    TextView mBondNextTime;//计价时间
    @BindView(R.id.bond_balance)
    TextView mBondBalance;// 余额
    @BindView(R.id.bond_tit)
    TextView bond_tit;
    @BindView(R.id.bond_existing)
    TextView mBondExisting;//现有债券
    @BindView(R.id.bond_redemption)
    TextView mBondRedemption;//已赎回债券
    @BindView(R.id.bond_equity_index)
    TextView mBondEquityIndex;//股权指数
    @BindView(R.id.bond_reward)
    TextView mBondReward;//奖励
    @BindView(R.id.bond_reward1)
    TextView bond_reward1;//剩余可赎回
    @BindView(R.id.spring_view)
    SpringView spring_view;//整体页面刷新

    @BindView(R.id.rl_rate)
    RelativeLayout rl_rate;
    @BindView(R.id.ll_time)
    LinearLayout ll_time;

   @BindView(R.id.empty_view)
    LinearLayout empty_view;


   private int idChangeClick = 0;

    private int pageNo = 1;
    private BondRecordAdapter mAdapter;
    private EditText mPopup_num;
    private TextView mPopup_price;
    private BondFeeBean mBondFeeBean;
    private IssueFeeBean mFeeBean;
    private String numberTradingPeriods;
    DecimalFormat mFormat = new DecimalFormat("0.0000");
    private String mDataUrl;
    private OrderPopupWindow pay_popup;
    private SafeKeyboard safeKeyboard;
    private BondPopupWindow bondPopupWindow;
    private List<BondRecordBean.ComlistBean> comlist;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_bond_exchange;
    }

    @Override
    public BondPresent initPresenter() {
        return new BondPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mTvTitle.setText(getResources().getString(R.string.exchanges_bond_title));
        mTvRightText.setVisibility(View.VISIBLE);
        coin_symbol = Utils.getSpUtils().getString("coin_symbol");
        mTvRightText.setText(getResources().getString(R.string.exchanges_bond_explanation));


        String bottom_refresh = getString(R.string.bottom_refresh);
        String open_refresh = getString(R.string.open_refresh);
        String refreshing1 = getString(R.string.refreshing1);
        String just_refresh = getString(R.string.strInfo5_);
        String minutes_refresh = getString(R.string.strInfo6_);
        String hour_refresh = getString(R.string.strInfo7_);
        String day_refresh = getString(R.string.strInfo8_);
        String last_refresh = getString(R.string.strInfo11_);

        spring_view.setHeader(new MyHeader(this, bottom_refresh, open_refresh, refreshing1, just_refresh
                , minutes_refresh, hour_refresh, day_refresh, last_refresh));
        spring_view.setFooter(new MyFooter(this));

        spring_view.setListener(new SpringView.OnFreshListener() {
            @Override
            public void onRefresh() {
                pageNo=1;
                showProgress();
                presenter.getData(BondExchangeActivity.this);
                presenter.getRecordData(BondExchangeActivity.this, pageNo + "");
                presenter.getFindOrderData(BondExchangeActivity.this);
                presenter.getBalance(mainAccount, "USDS");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        spring_view.onFinishFreshAndLoad();
                    }
                }, 1000);
            }

            @Override
            public void onLoadmore() {
                pageNo++;
                presenter.getRecordData(BondExchangeActivity.this, pageNo + "");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        spring_view.onFinishFreshAndLoad();
                    }
                }, 1000);
            }

        });
        presenter.getData(BondExchangeActivity.this);
        presenter.getRecordData(BondExchangeActivity.this, pageNo + "");
        presenter.getFindOrderData(this);
        presenter.getBalance(mainAccount, "USDS");
        presenter.getRedirectData(this);

        mBondRecycle.setLayoutManager(new LinearLayoutManager(this));

//        mBondRecycle.setOnPullLoadMoreListener(this);
        mBondRecycle.setRefreshing(false);
        mBondRecycle.setIsRefreshEnabled(false);
//        mBondRecycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty_new, null));
        mAdapter = new BondRecordAdapter(this);
        mAdapter.setPullRecyclerView(mBondRecycle);
        mBondRecycle.setItemAnimator(new DefaultItemAnimator());
        mBondRecycle.setAdapter(mAdapter);
        mBondRecycle.refreshWithPull();



    }

    @Override
    public void initEvent() {

    }


    @OnClick({R.id.iv_back, R.id.tv_right_text, R.id.bond_refresh})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_right_text:
                initIndex();
                break;
            case R.id.bond_refresh:
//                showProgress();
                presenter.getData(BondExchangeActivity.this);
                break;
        }
    }

    private void initIndex() {
        //说明
        Dialog dialog = new Dialog(BondExchangeActivity.this, R.style.MyDialog);
        View view = LayoutInflater.from(BondExchangeActivity.this).inflate(R.layout.popup_options_index, null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);
        TextView dialog_title = view.findViewById(R.id.dialog_title);
        dialog_title.setText(getResources().getString(R.string.bound_exchange_text));
        WebView mDialog_web = view.findViewById(R.id.dialog_web);
        mDialog_web.setBackgroundColor(0);//设置背景色
        mDialog_web.getBackground().setAlpha(0);//设置填充透明度（布局中一定要设置background，不然getbackground会是null）
        mDialog_web.getSettings().setJavaScriptEnabled(true);// 这行代码一定加上否则效果不会出现
        mDialog_web.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }
        });
        if (null != mDataUrl) {
            mDialog_web.loadDataWithBaseURL(null, mDataUrl, "text/html", "utf-8", null);
        }
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    private void showPassword(View view) { //显示密码框
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInputp(viewById, ev,keyboardPlace)) {
                        hideSoftInputp(view.getWindowToken());
                    }
                }
                return false;
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(BondExchangeActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            showProgress();
//                            presenter.checkPayPassword(BondExchangeActivity.this,s);
                            commit(s);
                            pay_popup.dismiss();
                        }
                    }, 500);
                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }
    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom +DensityUtil.dip2px(BondExchangeActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {

                pay_popup.dismiss();


            }
        }
    }
    private String coin_symbol;
    private void commit(String s) {
        if(null==mFeeBean.bond_address){
            AlertDialogShowUtil.toastMessage(BondExchangeActivity.this,getString(R.string.socket_time_out));
            return;
        }
        if(null==mBondFeeBean.pay_coin_name){
            AlertDialogShowUtil.toastMessage(BondExchangeActivity.this,getString(R.string.socket_time_out));
            return;
        }
        showProgress();
        String memo = "";
        String price = "";
        String unit = "";
        String mainAccount = Utils.getSpUtils().getString("mainAccount");

//        memo = "bond_exchange" + "_" + buyMemo;
        memo = "Destroyed." + "_" + buyMemo;

        if (mBondFeeBean.pay_coin_name.equals("SOU")) {
            unit = Constants.NGK_CONTRACT_ADDRESS;

            price = new BigDecimal(mBondFeeBean.pay_money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString() + " " + mBondFeeBean.pay_coin_name;
//                price = new DecimalFormat("##0.0000").format(new Double(mBondFeeBean.pay_money)).toString() + " " + mBondFeeBean.pay_coin_name;

        } else if (mBondFeeBean.pay_coin_name.equals("USDS")) {
            unit = Constants.USDN_CONTRACT_ADDRESS;
            price = new BigDecimal(mBondFeeBean.pay_money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString() + " " + mBondFeeBean.pay_coin_name;
//                price = new DecimalFormat("##0.00000000").format(new Double(mBondFeeBean.pay_money)).toString() + " " + mBondFeeBean.pay_coin_name;

        }

//            price = new DecimalFormat("##0.0000").format(new Double(mBondFeeBean.pay_money)).toString() +" "+mBondFeeBean.pay_coin_name;

        new EosSignDataManger(BondExchangeActivity.this, new EosSignDataManger.Callback() {
            @Override
            public void onSignSuccess(String sign_data) {
                String replace = "";
                if(mBondPrice.getText().toString().contains(coin_symbol)){
                    replace = mBondPrice.getText().toString().replace(coin_symbol, "");
                }
                presenter.getSendData(BondExchangeActivity.this, numberTradingPeriods, mPopup_num.getText().toString(), sign_data, replace, sign_data + replace, s);
            }

            @Override
            public void fail() {
                hideProgress();
            }
        }).pushAction(new Gson().toJson(new TransferEosMessageBean(memo, mFeeBean.bond_address, price
                        , mainAccount)),
                unit,
                mainAccount);
    }

    /************************************Call Interface callback****************************************************/
    Handler handler = new Handler();
    Runnable runnable;
    private int startNext = 1;
    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    private int buyMemo = 0;

    @Override
    public void onSuccess(int code, String msg_cn, BondExchangeBean bean) {//获取债券页面信息
        //  余额为账户余额，点击刷新按钮刷新该接口所有数据
        // surplus_num可兑换债券剩余数量（type==0取）
        // type  0可点击兑换，1不可点击
        // bond_num;//现有债券
        // reward_num;//分配奖励
        // stock_right_num;//股权指数
        // bond_num_redeem;//赎回债券
        // num;//可兑换债券剩余数量（type==1取）
        //bond_usdn_price;//兑换规律
        //nexttime;//下次计价时间
        hideProgress();
        if (code == 0) {
            if (bean.type.equals("0")) {
                bond_tit.setText(getString(R.string.exchanges_bond_available));
                numberTradingPeriods = bean.number;
                mBondRedeemable.setText(bean.surplus_num);
                mBondExchange.setTextColor(getResources().getColor(R.color.white));
                mBondRedeemable.setVisibility(View.VISIBLE);
                bond_text.setVisibility(View.GONE);
                rl_rate.setVisibility(View.VISIBLE);
                ll_time.setVisibility(View.VISIBLE);
                mBondExchange.setOnClickListener(new View.OnClickListener() {



                    @Override
                    public void onClick(View vie) {

                        if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                            View views = LayoutInflater.from(BondExchangeActivity.this).inflate(R.layout.popup_bond, null);
                            bondPopupWindow = new BondPopupWindow(BondExchangeActivity.this, views);
                            mPopup_num = views.findViewById(R.id.popup_num);
                            mPopup_price = views.findViewById(R.id.popup_price);
                            RelativeLayout key_main = views.findViewById(R.id.key_main);
                            LinearLayout key_scroll = views.findViewById(R.id.key_scroll);
                            LinearLayout keyboardPlace = views.findViewById(R.id.keyboardPlace);
                            mPopup_num.setInputType(InputType.TYPE_CLASS_NUMBER);
                            List<EditText> lists = new ArrayList<>();
                            lists.add(mPopup_num);
                             safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, lists, true);
                            bondPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View vi, MotionEvent ev) {
                                    if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                                        // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                                        if (isShouldHideInput(mPopup_num, ev,keyboardPlace)) {
                                            hideSoftInput(views.getWindowToken());
                                        }
                                    }
                                    return false;
                                }
                            });
                            mPopup_num.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    startNext =1;
                                    if (runnable != null) {
                                        handler.removeCallbacks(runnable);
                                    }
                                    runnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            if (!mPopup_num.getText().toString().equals("")) {
                                                showProgress();
                                                presenter.getFeeData(BondExchangeActivity.this, numberTradingPeriods, mPopup_num.getText().toString());
//                                            presenter.getEatNumber(mComlist.get(mEatPosition).postersid, mPopup_num.getText().toString());
                                            }
                                        }
                                    };
                                    handler.postDelayed(runnable, 800);

                                }

                            });
                            ((TextView) views.findViewById(R.id.bond_popup_cancel)).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    bondPopupWindow.dismiss();
                                }
                            });
                            ((TextView) views.findViewById(R.id.bond_popup_sure)).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (startNext == 0) {
                                        if (mPopup_num.getText().toString().equals("")) {
                                            AlertDialogShowUtil.toastMessage(BondExchangeActivity.this, getString(R.string.toast_ngk_num));
                                            return;
                                        } else {
                                            if (Integer.parseInt(mPopup_num.getText().toString()) > 0) {
                                                buyMemo++;
                                                bondPopupWindow.dismiss();
                                                showPassword(vie);
                                            } else {
                                                AlertDialogShowUtil.toastMessage(BondExchangeActivity.this, getString(R.string.toast_ngk_bond));
                                                return;
                                            }
                                        }
                                    }
                                }
                            });


                            bondPopupWindow.show(vie, getWindow(), 1);
                        } else {//未导入私钥 ，去设置
                            Dialog dialog = new Dialog(BondExchangeActivity.this, R.style.MyDialog);
                            View inflate = LayoutInflater.from(BondExchangeActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                            dialog.setContentView(inflate);
                            inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("accountName", mainAccount);
                                    ActivityUtils.next(BondExchangeActivity.this, ImportAccountActivity.class, bundle, false);
                                }
                            });
                            dialog.show();
                        }
                    }
                });

            } else if (bean.type.equals("1")) {
                mBondRedeemable.setText(bean.num);
                bond_tit.setText(getString(R.string.exchanges_bond_available2));
                mBondRedeemable.setVisibility(View.VISIBLE);
                bond_text.setVisibility(View.GONE);
                rl_rate.setVisibility(View.VISIBLE);
                ll_time.setVisibility(View.VISIBLE);
                mBondExchange.setTextColor(getResources().getColor(R.color.color_898e93));
                mBondExchange.setOnClickListener(null);
            } else if (bean.type.equals("2")) {
                bond_tit.setText(getString(R.string.exchanges_bond_available));
                rl_rate.setVisibility(View.INVISIBLE);
                ll_time.setVisibility(View.INVISIBLE);
                mBondRefresh.setVisibility(View.GONE);
                mBondRedeemable.setVisibility(View.GONE);
                bond_text.setVisibility(View.VISIBLE);
                mBondExchange.setTextColor(getResources().getColor(R.color.color_898e93));
                mBondExchange.setOnClickListener(null);
            }
            mBondPrice.setText(coin_symbol+bean.usdn_rate);
            mBondExchangeRate.setText(bean.bond_usdn_price);
            mBondNextTime.setText(bean.nexttime);

            mBondExisting.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.bond_num).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
            mBondRedemption.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.bond_num_redeem).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
//            mBondEquityIndex.setText(NumUtils.subZeroAndDot(mFormat.format(Double.parseDouble(bean.stock_right_num))));
            if (null != bean.stock_right_num) {
                mBondEquityIndex.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.stock_right_num).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
            } else {
                mBondEquityIndex.setText("0");
            }
            mBondReward.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.reward_num).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
            bond_reward1.setText(NumUtils.subZeroAndDot(new BigDecimal(bean.alluser_num).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));

        } else {
            AlertDialogShowUtil.toastMessage(this, msg_cn);
        }
    }
    private boolean isShouldHideInput(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() -keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom +DensityUtil.dip2px(BondExchangeActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {

                bondPopupWindow.dismiss();


            }
        }
    }
    @Override
    public void onFeeDataSuccess(int code, String msg_cn, BondFeeBean bean) {//获取兑换数量
        hideProgress();
        startNext = code;
        if (code == 0) {
            mBondFeeBean = bean;
            mPopup_price.setText(bean.pay_money);
            mBondPrice.setText(coin_symbol+bean.bond_usdn_price);
        } else if (code == 2) {
            if(null!=bean.pay_money){
                mPopup_price.setText(bean.pay_money);
                mBondPrice.setText(coin_symbol+bean.bond_usdn_price);
            }
//            mPopup_price.setText(bean.pay_money);
//            mBondPrice.setText(bean.bond_usdn_price);
            AlertDialogShowUtil.toastMessage(this, msg_cn);
        } else {
            AlertDialogShowUtil.toastMessage(this, msg_cn);
        }
    }

    @Override
    public void onSendSuccess(int code, String msg_cn, String bean) {//兑换债券
        hideProgress();
        if (code == 0) {
            showProgress();
            presenter.getData(BondExchangeActivity.this);
            presenter.getRecordData(BondExchangeActivity.this, 1 + "");
            AlertDialogShowUtil.toastMessage(this, msg_cn);
            presenter.getBalance(mainAccount, "USDS");
        } else {
            AlertDialogShowUtil.toastMessage(this, msg_cn);
        }
    }

    @Override
    public void onRecordSuccess(int code, String msg_cn, BondRecordBean bean) {//兑换记录
        hideProgress();
        if (pageNo == 1) {
            comlist = bean.comlist;
            mAdapter.setDatas(bean.comlist);
            for (int i = 0; i < bean.comlist.size(); i++) {
                comlist.get(i).positotals = bean.comlist.size()-1;
            }
        } else {
            comlist.addAll(bean.comlist);
            for (int i = 0; i <comlist.size(); i++) {
                comlist.get(i).positotals = comlist.size()-1;
            }
            mAdapter.addDatas(bean.comlist);
        }

        if(comlist.size()==0){
            empty_view.setVisibility(View.VISIBLE);
        }else {
            empty_view.setVisibility(View.GONE);
        }
        Utils.getSpUtils().put("ishavedata",bean.comlist.size());
        mBondRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {//获取账户余额
        mBondBalance.setText(NumUtils.subZeroAndDot(new BigDecimal(balance).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()) + getResources().getString(R.string.USDN));
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg_cn, String bean) {//检验交易密码
        hideProgress();
        if (code == 0) {//成功，获取链签名

        } else {
            AlertDialogShowUtil.toastMessage(BondExchangeActivity.this, bean);
        }
    }

    @Override
    public void onIssueFeeSuccess(IssueFeeBean bean) {//获取转账地址
        hideProgress();
        mFeeBean = bean;
    }

    @Override
    public void getRedirectDataSuccess(int code, String msg_cn, RedirectsBean bean) {
        hideProgress();
        mDataUrl = bean.content;
    }

    @Override
    public void onFailure(String msg_cn) {
        hideProgress();
    }

//
//    @Override
//    public void onRefresh() {
////        pageNo=1;
////        showProgress();
////        presenter.getData(BondExchangeActivity.this);
////        presenter.getRecordData(BondExchangeActivity.this, pageNo + "");
////        presenter.getFindOrderData(BondExchangeActivity.this);
////        presenter.getBalance(mainAccount, "USDK");
//    }
//
//    @Override
//    public void onLoadMore() {
//        pageNo++;
//        presenter.getRecordData(BondExchangeActivity.this, pageNo + "");
//    }
}
