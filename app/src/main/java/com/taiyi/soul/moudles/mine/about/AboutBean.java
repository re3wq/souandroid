package com.taiyi.soul.moudles.mine.about;

class AboutBean {

    /**
     * code : 0
     * msg : Success !!
     * msg_cn : Success
     * data : {"email_url":"Tech@ngk.io","web_url":"https://ngk.org","twitter_url":"https://twitter.ngk.io"}
     */

    /**
     * email_url : Tech@ngk.io
     * web_url : https://ngk.org
     * twitter_url : https://twitter.ngk.io
     */

    private String email_url;
    private String web_url;
    private String twitter_url;
    private String qr_code;

    public String getEmail_url() {
        return email_url;
    }

    public void setEmail_url(String email_url) {
        this.email_url = email_url;
    }

    public String getWeb_url() {
        return web_url;
    }

    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }

    public String getTwitter_url() {
        return twitter_url;
    }

    public void setTwitter_url(String twitter_url) {
        this.twitter_url = twitter_url;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }
}
