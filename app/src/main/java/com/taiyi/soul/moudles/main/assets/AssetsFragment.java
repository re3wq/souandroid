package com.taiyi.soul.moudles.main.assets;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.main.assets.bean.AssetsBean;
import com.taiyi.soul.moudles.main.assets.collectmoney.CollectMoneyActivity;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.CurrencyDetailsActivity;
import com.taiyi.soul.moudles.main.assets.distribution.AssetDistributionActivity;
import com.taiyi.soul.moudles.main.assets.exchange.ExchangeActivity;
import com.taiyi.soul.moudles.main.assets.transfer.TransferActivity;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.LocalManageUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AssetsFragment extends BaseFragment<AssetsView, AssetsPresent> implements MultiItemTypeAdapter.OnItemClickListener, AssetsView {

    @BindView(R.id.top)
    RelativeLayout top;
    @BindView(R.id.availableAssetsTv)
    NumberRollingView availableAssetsTv;
    @BindView(R.id.accountTotalAssetsTv)
    NumberRollingView accountTotalAssetsTv;
    @BindView(R.id.freezeAssetsTv)
    NumberRollingView freezeAssetsTv;
    @BindView(R.id.collectMoneyTv)
    TextView collectMoneyTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.rotationIv)
    ImageView rotationIv;
    @BindView(R.id.iivv)
    ImageView iivv;
    @BindView(R.id.transparentIv)
    ImageView transparentIv;
    @BindView(R.id.ll)
    LinearLayout ll;

    List<AssetsBean.PropertelistBean> list = new LinkedList<>();
    private List<AssetsBean.PropertelistBean> propertelist;
    private boolean mHidden;
    private String balance_ngk = "0.0000";
    private String balance_usdn = "0.0000";
    private String balance_hash = "0.0000";
    private double srate=0;
    private AssetsBean assetsBeanDates;
    public static double hashprice=0;
    private List<SelectCoinRateBean> scrList;

    public void getHashPrice(){
        String url_hashprice="http://18.167.53.241:8088/hash.txt";
        HttpUtils.getRequets(url_hashprice, this, null, new JsonCallback<String>()  {
            @Override
            public String convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                super.onSuccess(response);
                try{ //response.body().get(0).split(" ")[0]
                    hashprice=Double.parseDouble(response.body());
                    //当前语言
                    Log.e("语言==", hashprice+"----"+ Utils.getSpUtils().getString("cur_coin_name"));

//                    for (SelectCoinRateBean selectCoinRateBean : scrList) {
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("AUD")){
//                            if(selectCoinRateBean.getCode().equals("AU")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" AUD srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("USD")){
//                            if(selectCoinRateBean.getCode().equals("US")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" USD srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("HKD")){
//                            if(selectCoinRateBean.getCode().equals("HK")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" HKD srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("GBP")){
//                            if(selectCoinRateBean.getCode().equals("GB")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" GBP srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("CAD")){
//                            if(selectCoinRateBean.getCode().equals("CA")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" CAD srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("JPY")){
//                            if(selectCoinRateBean.getCode().equals("JP")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" JPY srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("CNY")){
//                            if(selectCoinRateBean.getCode().equals("CN")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" CNY srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("KRW")){
//                            if(selectCoinRateBean.getCode().equals("KR")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" KRW srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("SGD")){
//                            if(selectCoinRateBean.getCode().equals("SG")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" SGD srate==",  srate+"--");
//                            }
//                        }
//                        if( Utils.getSpUtils().getString("cur_coin_name").equals("EUR")){
//                            if(selectCoinRateBean.getCode().equals("EU")){
//                                srate=selectCoinRateBean.getRate();
//                                Log.e(" EUR srate==",  srate+"--");
//                            }
//                        }
//
//
//                    }
//                    hashprice=hashprice*srate;
//                    if(scrList!=null){
//                        Log.e("scrList==",scrList.get(0).getCode());
//                    }
                    hashprice=hashprice*Double.valueOf(propertelist.get(1).getPrice());
                    Log.e("hashprice==",hashprice+"--"+propertelist.get(0).getPrice());
                }catch(Exception e){
                    hashprice=0;
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
            }
        });
    }
    @Override
    public AssetsPresent initPresenter() {
        return new AssetsPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }
    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void initData() {
//        showProgress();
        //TODO lxq

        presenter.getAssets(getActivity());


        presenter.getRate("SOU");
        presenter.getRate("USDS");

//        presenter.getRate("USDT");

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mHidden)
        {
            presenter.getAssets(getActivity());}
        getHashPrice();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        mHidden = hidden;
        if (!hidden) {

            presenter.getAssets(getActivity());
        }
    }

    private void initAnimation() {
//        animation.setLayerType(View.LAYER_TYPE_HARDWARE,null);
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.assets_rotation_anim);
        animation.setFillAfter(true);
        transparentIv.startAnimation(animation);
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.assets_fragment_layout;
    }


    @OnClick({R.id.rightIv, R.id.availableAssetsCl, R.id.freezeAssetsCl, R.id.collectMoneyTv, R.id.transferTv, R.id.exchangeTv, R.id.debtTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rightIv://右上角按钮 资产分布
                ActivityUtils.next(getActivity(), AssetDistributionActivity.class, false);
                break;
            case R.id.availableAssetsCl://可用资产
                break;
            case R.id.freezeAssetsCl://冻结资产
                break;
            case R.id.collectMoneyTv://收款
                ActivityUtils.next(getActivity(), CollectMoneyActivity.class);
                break;
            case R.id.transferTv://转账
                ActivityUtils.next(getActivity(), TransferActivity.class);
                break;
            case R.id.exchangeTv://兑换
//                toast(getResources().getString(R.string.not_open));
                ActivityUtils.next(getActivity(), ExchangeActivity.class, false);
                break;
            case R.id.debtTv://债权
//                ActivityUtils.next(getActivity(), BondExchangeActivity.class, false);
                break;
        }
    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("list", (Serializable) propertelist);
//        ActivityUtils.next(getActivity(), CurrencyActivity.class, bundle);
        Bundle bundle = new Bundle();
        bundle.putSerializable("currencyBean", propertelist.get(position));
        ActivityUtils.next(getActivity(), CurrencyDetailsActivity.class, bundle);
    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }

    @Override
    public void getAssetsSuccess(AssetsBean assetsBean) {

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                initAnimation();
//            }
//        }).start();

        assetsBeanDates = assetsBean;
        accountTotalAssetsTv.setContent(new BigDecimal(assetsBean.getTotalAssets()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString());

//        availableAssetsTv.setMoney(new BigDecimal(assetsBean.getTotalMoenys()));
        availableAssetsTv.setContent(new BigDecimal(assetsBean.getTotalMoenys()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString());
//        freezeAssetsTv.setMoney(Float.parseFloat(assetsBean.getTotalFrozenMoney()));
        freezeAssetsTv.setContent(new BigDecimal(assetsBean.getTotalFrozenMoney()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString());
        propertelist = assetsBean.getPropertelist();
        list.clear();
//        if (propertelist.size() > 3) {
//            for (int i = 0; i < 3; i++) {
//                list.add(propertelist.get(i));
//            }
//        } else {
        list.addAll(propertelist);
//        }
        for (AssetsBean.PropertelistBean propertelistBean : list) {
            if (propertelistBean.getCoinName().equals("SOU")) {
//                showProgress();
                presenter.getBalance("SOU");
            }
        }
        for (AssetsBean.PropertelistBean propertelistBean : list) {
            if (propertelistBean.getCoinName().equals("TRC20USDT")) {
//                showProgress();
                list.remove(propertelistBean);
            }
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        CommonAdapter currencyListAdapter = AdapterManger.getAssetsCurrencyListAdapter(getContext(), list);
        recyclerView.setAdapter(currencyListAdapter);
        currencyListAdapter.setOnItemClickListener(this);
        //TODO lxq
        getHashPrice();
//        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void getAssetsFailure(String errorMsg) {

    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        int i = 0;
        if ("SOU".equals(symbol)) {
            balance_ngk = balance;
//            for (AssetsBean.PropertelistBean propertelistBean : list) {
//                if (propertelistBean.getCoinName().equals("USDS")) {
//                    i = i + 1;
//                    break;
//                }
//            }
//            if (i == 1)
            presenter.getBalance("USDS");
//            else
//                presenter.getSelectCoinRate();
        }

        else  if ("USDS".equals(symbol)){
            balance_usdn = balance;
            presenter.getBalance("HASH");
        } else if ("HASH".equals(symbol)){
            balance_hash = balance;
            //TODO lxq
            presenter.getSelectCoinRate();
        }

        //累加到总资产和自由
        for(int index=0;index<assetsBeanDates.getPropertelist().size();index++){
            if(assetsBeanDates.getPropertelist().get(index).getCoinName().equals(symbol)){
                if(symbol.equals("HASH")){
                    assetsBeanDates.getPropertelist().get(index).setPrice(Double.toString(hashprice));
                }
                assetsBeanDates.getPropertelist().get(index).setMfcon(balance);
                double price=Double.parseDouble(assetsBeanDates.getPropertelist().get(index).getPrice());
                Double money=price*Double.parseDouble(balance);
                assetsBeanDates.getPropertelist().get(index).setMoney(money.toString());
            }
        }
        //汇总
        double total=0;
        for(int index=0;index<assetsBeanDates.getPropertelist().size();index++) {
            total+=Double.parseDouble(assetsBeanDates.getPropertelist().get(index).getMoney());
        }
        assetsBeanDates.setTotalAssets(Double.valueOf(total).toString());
        assetsBeanDates.setTotalMoenys(Double.valueOf(total).toString());
        accountTotalAssetsTv.setContent(new BigDecimal(assetsBeanDates.getTotalAssets()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString());
//        availableAssetsTv.setMoney(new BigDecimal(assetsBean.getTotalMoenys()));
        availableAssetsTv.setContent(new BigDecimal(assetsBeanDates.getTotalMoenys()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString());


    }

    @Override
    public void getSelectCoinRateSuccess(List<SelectCoinRateBean> mList) {
        scrList=mList;
        hideProgress();
        String selectCoin = Utils.getSpUtils().getString(Constants.SELECT_COIN);
        String symbol = Utils.getSpUtils().getString("coin_symbol");
        String ngk_to_usdt = Utils.getSpUtils().getString(Constants.NGK_TO_USDT_RATE);
        String usdn_to_usdt = Utils.getSpUtils().getString(Constants.USDN_TO_USDT_RATE);
        for (SelectCoinRateBean selectCoinRateBean : mList) {
            if (selectCoinRateBean.getCurrency_code().equals(selectCoin)) {
                double rate = selectCoinRateBean.getRate();

                for (AssetsBean.PropertelistBean currencyListBean : list) {
                    if (currencyListBean.getCoinName().equals("SOU")) {
//                        currencyListBean.setPrice(ngk_to_usdt);
                        currencyListBean.setMoneyandfor(balance_ngk);
                        if (!balance_ngk.equals("0.0000")) {
                            currencyListBean.setMfcon(new BigDecimal(ngk_to_usdt).multiply(new BigDecimal(balance_ngk)).multiply(new BigDecimal(rate)).setScale(4, BigDecimal.ROUND_DOWN).toString());
                        }
                    } else if (currencyListBean.getCoinName().equals("USDS")) {
//                        currencyListBean.setPrice(usdn_to_usdt);
                        currencyListBean.setMoneyandfor(new BigDecimal(balance_usdn).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
                        if (!balance_usdn.equals("0.0000")) {
                            currencyListBean.setMfcon(new BigDecimal(usdn_to_usdt).multiply(new BigDecimal(balance_usdn)).multiply(new BigDecimal(rate)).setScale(4, BigDecimal.ROUND_DOWN).toString());
//                            currencyListBean.setMfcon(symbol + decimalFormat.format(Float.parseFloat(usdn_to_usdt) * Float.parseFloat(balance_usdn) * rate));
                        }
                    }
                    else if (currencyListBean.getCoinName().equals("HASH")) {
//                        currencyListBean.setPrice(usdn_to_usdt);
                        //TODO lxq
                        rate=hashprice;
//                        hashprice=hashprice*rate;
                        currencyListBean.setMoneyandfor(new BigDecimal(balance_hash).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
                        if (!balance_hash.equals("0.0000")) {
                            currencyListBean.setMfcon(new BigDecimal(usdn_to_usdt).multiply(new BigDecimal(balance_hash)).multiply(new BigDecimal(rate)).setScale(4, BigDecimal.ROUND_DOWN).toString());
//                            currencyListBean.setMfcon(symbol + decimalFormat.format(Float.parseFloat(usdn_to_usdt) * Float.parseFloat(balance_usdn) * rate));
                        }
                    }
                }
                    recyclerView.getAdapter().notifyDataSetChanged();

                break;
            }
        }

    }

    @Override
    public void getSelectCoinRateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getRateSuccess(String symbol, String rate) {
        if ("SOU".equals(symbol)) {
            Utils.getSpUtils().put(Constants.NGK_TO_USDT_RATE, rate);
        } else {
            Utils.getSpUtils().put(Constants.USDN_TO_USDT_RATE, rate);
        }
    }

    @Override
    public void getRateFailure(String errorMsg) {
        toast(errorMsg);
    }


}
