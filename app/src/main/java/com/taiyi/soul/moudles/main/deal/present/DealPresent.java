package com.taiyi.soul.moudles.main.deal.present;

import android.app.Activity;
import android.content.Intent;

import com.alibaba.fastjson.JSONObject;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.deal.bean.DealBean;
import com.taiyi.soul.moudles.main.deal.bean.EatInfoBean;
import com.taiyi.soul.moudles.main.deal.bean.EatNumberBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.MD5Util;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：集市交易相关接口
 */
public class DealPresent extends BasePresent<DealView> {


    /**
     * 集市交易列表
     *
     * @param downType
     * @param PageNum
     */
    public void getData(Activity activity, String downType, String PageNum, PullRecyclerView pullRecyclerView) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "1");//0:算力市场，1：集市交易
        map.put("downType", downType);//0：买，1：卖
        map.put("PageNum", PageNum);//页数
        map.put("PageSize", "10");//条数
        HttpUtils.postRequest(BaseUrl.DEALLIST, this, map, new JsonCallback<BaseResultBean<DealBean>>() {
            @Override
            public BaseResultBean<DealBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<DealBean>> response) {
                super.onSuccess(response);

                if (response.body().code == 0) {
                    if (null != view)
                        view.onDealSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if (Constants.isL == 0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                } else {
                    pullRecyclerView.setPullLoadMoreCompleted();
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<DealBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }


    //获取挂单信息（手续费、数量、价格、币种地址等）
    public void getFindOrderData(Activity activity) {
        HttpUtils.postRequest(BaseUrl.DEAFINDORDER, this, "", new JsonCallback<BaseResultBean<IssueFeeBean>>() {
            @Override
            public BaseResultBean<IssueFeeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onIssueFeeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if (Constants.isL == 0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }


    //验证交易密码
    public void checkPayPassword(Activity activity, String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn, response.body().msg_cn);
                } else if (response.body().code == -1) {
                    if (Constants.isL == 0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //获取吃单数量
    public void getEatNumber(Activity activity, String postersid, String num) {
        Map<String, String> map = new HashMap<>();
        map.put("postersid", postersid);
        map.put("num1", num);
        HttpUtils.postRequest(BaseUrl.CALCULATION, this, map, new JsonCallback<BaseResultBean<EatNumberBean>>() {
            @Override
            public BaseResultBean<EatNumberBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EatNumberBean>> response) {
                super.onSuccess(response);
                if (response.body().code == -1) {
                    if (Constants.isL == 0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.onEatNumSuccess(response.body().code, response.body().msg_cn, response.body().data);
                }


            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EatNumberBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //吃单
    public void eatOrder(Activity activity, String postersid, String num, String json, String pass) {
        String md5Str = MD5Util.getMD5Str(json + postersid + "eat", MD5Util.MD5_LOWER_CASE);
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("postersid", postersid);
        map.put("num1", num);
        map.put("json", json);
        map.put("sign", md5Str);
        map.put("paypass", password);
        HttpUtils.postRequest(BaseUrl.CLICKBUY, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
              if (response.body().code == -1) {
                    if (Constants.isL == 0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.onEatSuccess(response.body().code, response.body().msg_cn);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //获取订单信息
    public void eatOrderInfo(Activity activity, String postersid) {
        Map<String, String> map = new HashMap<>();
        map.put("postersid", postersid);
        HttpUtils.postRequest(BaseUrl.ORDERINFO, this, map, new JsonCallback<BaseResultBean<EatInfoBean>>() {
            @Override
            public BaseResultBean<EatInfoBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EatInfoBean>> response) {
                super.onSuccess(response);
             if (response.body().code == -1) {
                    if (Constants.isL == 0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.onEatInfoSuccess(response.body().code, response.body().msg_cn, response.body().data);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EatInfoBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    /**
     * 获取余额
     */
    public void getBalance(String account, String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }

        //        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account",
                account);

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size() > 0) {
                            if (null != view)
                                view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        } else {
                            if (null != view)
                                view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if (null != view)
                            view.getBalanceSuccess(symbol, "0.0000");
                    }

                });

    }

}
