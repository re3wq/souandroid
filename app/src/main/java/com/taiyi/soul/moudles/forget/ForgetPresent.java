package com.taiyi.soul.moudles.forget;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.RsaUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class ForgetPresent extends BasePresent<ForgetView> {
    public void updatePwd(String areaCode,String account ,String loginPwd, String verifyCode) {
        String password= "";
        Map maps = new HashMap();
        maps.put("pass", loginPwd);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areaCode",areaCode);
            jsonObject.put("editType","2");
            jsonObject.put("mainAccount", account);
            jsonObject.put("pass", password);
            jsonObject.put("passType", false);
            jsonObject.put("type", 1);
            jsonObject.put("verifyCode", verifyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.FORGET_PASSWORD, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null!=view) {

                if (response.body().status == 200) {
                 view.updateSuccess();
                } else {
                  view.updateFailure(response.body().msg);
                }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getAreaCode(){
        HttpUtils.getRequets(BaseUrl.GET_AREA_CODE, this, null, new JsonCallback<BaseResultBean<List<AreaBean>>>() {
            @Override
            public BaseResultBean<List<AreaBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onSuccess(response);
                if (null!=view) {
                if (response.body().status==200){
                    view.getAreaCodeSuccess(response.body().data);
                }}
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onError(response);
            }
        });
    }


    public void getVerificationCode(String areaCode,String account) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areaCode",areaCode);
            jsonObject.put("mainAccount", account);
            jsonObject.put("type", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.REGISTER_GET_VERIFICATION_CODE, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getVerificationCodeSuccess();
                    } else {
                        view.getInvitePersonFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

}
