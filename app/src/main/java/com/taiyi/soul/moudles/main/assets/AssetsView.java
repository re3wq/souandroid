package com.taiyi.soul.moudles.main.assets;

import com.taiyi.soul.moudles.main.assets.bean.AssetsBean;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;

import java.util.List;

public interface AssetsView {
    void getAssetsSuccess(AssetsBean assetsBean);
    void getAssetsFailure(String errorMsg);
    void getBalanceSuccess(String symbol,String balance);
    void getSelectCoinRateSuccess(List<SelectCoinRateBean> list);
    void getSelectCoinRateFailure(String errorMsg);
    void getRateSuccess(String symbol,String rate);
    void getRateFailure(String errorMsg);
}
