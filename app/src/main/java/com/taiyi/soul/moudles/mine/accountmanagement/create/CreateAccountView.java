package com.taiyi.soul.moudles.mine.accountmanagement.create;

public interface CreateAccountView {
    void getInvitePersonSuccess(String invitePeople);

    void getInvitePersonFailure(String errorMsg);

    void checkAccountUseable(boolean s);
    void checkAccountUseableInput(boolean s);
}
