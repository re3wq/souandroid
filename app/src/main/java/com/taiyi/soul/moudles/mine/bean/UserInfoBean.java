package com.taiyi.soul.moudles.mine.bean;

public class UserInfoBean {

    /**
     * mainAccount : 18302223400
     * totalAssets : 0
     * headerImg : http://114.115.131.170:8888/group1/M00/00/00/wKgAp15fd66AbHKSAAiJ135U-tQ949.png
     * communityLevel : 0
     */

    private String mainAccount;
    private String totalAssets;
    private String headerImg;
    private boolean isRead;
    private int communityLevel;

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(String totalAssets) {
        this.totalAssets = totalAssets;
    }

    public String getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }

    public int getCommunityLevel() {
        return communityLevel;
    }

    public void setCommunityLevel(int communityLevel) {
        this.communityLevel = communityLevel;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
