package com.taiyi.soul.moudles.main.assets.collectmoney;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mylhyl.zxing.scanner.encode.QREncode;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.CollectMoneyAddressBean;
import com.taiyi.soul.moudles.main.assets.view.CurrencyPopupWindow;
import com.taiyi.soul.utils.CopyUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CollectMoneyActivity extends BaseActivity<CollectMoneyView, CollectMoneyPresent> implements CollectMoneyView, MultiItemTypeAdapter.OnItemClickListener, View.OnClickListener {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.coinIv)
    ImageView coinIv;
    @BindView(R.id.coinNameTv)
    TextView coinNameTv;
    //    @BindView(R.id.memoTv)
//    TextView memoTv;
//    @BindView(R.id.copyTv)
//    TextView copyTv;
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.qrCodeIv)
    ImageView qrCodeIv;
    @BindView(R.id.selectCoinIv)
    ImageView selectCoinIv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerViewD;
    @BindView(R.id.addressTv)
    TextView addressTv;
    @BindView(R.id.top)
    ConstraintLayout top;

    @BindView(R.id.describeTv)
    TextView describeTv;
    @BindView(R.id.dateTv)
    TextView dateTv;
    @BindView(R.id.quantityTv)
    TextView quantityTv;
    @BindView(R.id.addressTv1)
    TextView addressTv1;
    @BindView(R.id.whitePagerTv)
    TextView whitePagerTv;
    @BindView(R.id.queryTv)
    TextView queryTv;
    List<CoinListBean> list = new ArrayList<>();
    List<CoinListBean> allList = new ArrayList<>();
    private CurrencyPopupWindow currencyPopupWindow;
    private int usdt_position=0;
    RecyclerView recyclerView;
    private int oldPosition = -1;//防止同一Position多次触发
    CoinListBean tetherUSDTBean,tetherUSDTBean2;
    private Dialog usdtDialog;
    private TextView wayOneTv, wayTwoTv,wayThreeTv;
    private String url;
    private String memo="";
    private String fromCoinName;
//    @BindView(R.id.transfer_eos_hint)
//    TextView transfer_eos_hint;

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_collect_money;
    }

    @Override
    public CollectMoneyPresent initPresenter() {
        return new CollectMoneyPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.collect_money));
        initDialog();
        if (getIntent().getExtras() != null) {
            fromCoinName = getIntent().getExtras()
                    .getString("fromCoinName");
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewD.setLayoutManager(linearLayoutManager);
        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        CommonAdapter currencyDescribeAdapter = AdapterManger.getCurrencyDescribeAdapter(this, allList);
        recyclerViewD.setAdapter(currencyDescribeAdapter);
        pagerSnapHelper.attachToRecyclerView(recyclerViewD);
        recyclerViewD.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int position = 0;
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                //获取当前选中的itemView
                View view = pagerSnapHelper.findSnapView(layoutManager);
                if (view != null) {
                    //获取itemView的position
                    position = layoutManager.getPosition(view);
                }
                //newState == RecyclerView.SCROLL_STATE_IDLE 当滚动停止时触发防止在滚动过程中不停触发
                if (newState == RecyclerView.SCROLL_STATE_IDLE && oldPosition != position) {
//                    Log.i("currentPosition", "------" + position);
                    CoinListBean coinListBean = allList.get(position);
//                    CoinListBean coinListBean = list.get(position);
                    String coinName = coinListBean.getCoinName();
                    if (coinName.equals("TetherUSDT")){
                        if (position>oldPosition) {//向左滑
                            coinListBean=allList.get(position+1);
                            coinName = coinListBean.getCoinName();
                        }else {
                            coinListBean=allList.get(position-1);
                            coinName = coinListBean.getCoinName();
                        }
                    }
                    oldPosition = position;
                    String url = coinListBean.getUrl();
                    coinNameTv.setText(coinName);

                    Glide.with(CollectMoneyActivity.this).load(coinListBean.getUrl()).into(coinIv);
                    Glide.with(CollectMoneyActivity.this).load(coinListBean.getBackImgUrl()).into(selectCoinIv);
                    if (coinName.equals("USDT")){
//                        ToastUtils.showShortToast(coinName);
                        usdtDialog.show();
                    }

                    if (coinListBean.getCoinName().equals("SOU")||coinListBean.getCoinName().equals("USDS")||coinListBean.getCoinName().equals("HASH")){
//                        memoTv.setVisibility(View.GONE);
//                        copyTv.setVisibility(View.GONE);

                        String mainAccount = Utils.getSpUtils().getString("mainAccount");
                        addressTv.setText(mainAccount);
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("address",mainAccount);
                            jsonObject.put("symbol",coinName);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Bitmap bitmap = new QREncode.Builder(CollectMoneyActivity.this)
                                .setContents(jsonObject.toString())//二维码内容
                                .setMargin(1)
                                .build().encodeAsBitmap();
                        qrCodeIv.setImageBitmap(bitmap);

                    }else {
                        presenter.getCollectMoneyAddress(coinName);
                    }


                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        View inflate = LayoutInflater.from(this).inflate(R.layout.coin_list_layout, null);
        recyclerView = inflate.findViewById(R.id.recyclerView);
        CommonAdapter coinListAdapter = AdapterManger.getCoinListAdapter(this, list);
        recyclerView.setAdapter(coinListAdapter);
        coinListAdapter.setOnItemClickListener(this);
        currencyPopupWindow = new CurrencyPopupWindow(this, inflate);
        currencyPopupWindow.setWidth(getResources().getDisplayMetrics().widthPixels - DensityUtil.dip2px(this, 24));
        currencyPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

    }

    private void initDialog() {
        if (usdtDialog == null) {
            usdtDialog = new Dialog(this, R.style.MyDialog);
            View view = LayoutInflater.from(this).inflate(R.layout.usdt_layout_dialog, null);
            usdtDialog.setContentView(view);
            wayOneTv = view.findViewById(R.id.wayOneTv);
            wayTwoTv = view.findViewById(R.id.wayTwoTv);
            wayThreeTv = view.findViewById(R.id.wayThreeTv);
            wayOneTv.setSelected(true);
            view.findViewById(R.id.closeIv).setOnClickListener(this);
            wayOneTv.setOnClickListener(this);
            wayTwoTv.setOnClickListener(this);
            wayThreeTv.setOnClickListener(this);
        }
    }

    @Override
    protected void initData() {
        showProgress();
        presenter.getCoinList();
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.coinCl, R.id.leftIv, R.id.rightAddIv, R.id.copyAddressTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.coinCl:
                currencyPopupWindow.showAsDropDown(view, 0, 20);
                break;
            case R.id.leftIv:
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerViewD.getLayoutManager()).findFirstVisibleItemPosition();
                if (firstVisibleItemPosition == 0) {
                    return;
                }
//                recyclerViewD.smoothScrollToPosition(firstVisibleItemPosition - 1);
                CoinListBean coinListBean = allList.get(firstVisibleItemPosition - 1);
                String coinName = coinListBean.getCoinName();
                if (coinName.equals("TetherUSDT")){
                    recyclerViewD.smoothScrollToPosition(firstVisibleItemPosition - 2);
                }else {
                    recyclerViewD.smoothScrollToPosition(firstVisibleItemPosition - 1);
                }
                if (coinName.equals("USDT")){
//                        ToastUtils.showShortToast(coinName);
                    usdtDialog.show();
                }
                break;
            case R.id.rightAddIv:
                int firstVisibleItemPositionLast = ((LinearLayoutManager) recyclerViewD.getLayoutManager()).findFirstVisibleItemPosition();
                if (firstVisibleItemPositionLast == allList.size() - 1) {
                    return;
                }

                CoinListBean coinListBean1 = allList.get(firstVisibleItemPositionLast + 1);
                String coinName1 = coinListBean1.getCoinName();
                if (coinName1.equals("TetherUSDT")){
                    recyclerViewD.smoothScrollToPosition(firstVisibleItemPositionLast + 2);
                }else {
                    recyclerViewD.smoothScrollToPosition(firstVisibleItemPositionLast + 1);
                }
                if (coinName1.equals("USDT")){
//                        ToastUtils.showShortToast(coinName);
                    usdtDialog.show();
                }
                break;
            case R.id.copyAddressTv:
                CopyUtils.CopyToClipboard(this, addressTv.getText().toString());
                toast(getString(R.string.copy_success));
                break;

        }
    }

    @Override
    public void getCoinListSuccess(List<CoinListBean> coinListBeanList) {
        hideProgress();

        if (coinListBeanList != null) {
            allList.clear();
            allList.addAll(coinListBeanList);
            for (int i = 0; i < allList.size(); i++) {
                if ("TetherUSDT".equals(allList.get(i).getCoinName())) {
                    allList.remove(i);
                }

            }
            for (int i = 0; i < allList.size(); i++) {
                if ("TRC20USDT".equals(allList.get(i).getCoinName())) {
                    allList.remove(i);
                }
            }
            for (int i = 0; i < allList.size(); i++) {
                if ("BTC".equals(allList.get(i).getCoinName())) {
                    allList.remove(i);
                }
            }

            list.clear();
            for (CoinListBean coinListBean : coinListBeanList) {
                if ("TetherUSDT".equals(coinListBean.getCoinName())) {
                    tetherUSDTBean = coinListBean;
                }else if ("TRC20USDT".equals(coinListBean.getCoinName())) {
                    tetherUSDTBean2 = coinListBean;
                }else if ("BTC".equals(coinListBean.getCoinName())) {

                } else {
                    list.add(coinListBean);
                }
            }
//            list.addAll(coinListBeanList);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerViewD.getAdapter().notifyDataSetChanged();
            if (fromCoinName==null) {
                String coinName = list.get(0).getCoinName();
                url = list.get(0).getUrl();
                coinNameTv.setText(coinName);
                Glide.with(this).load(list.get(0).getUrl()).into(coinIv);
                Glide.with(this).load(list.get(0).getBackImgUrl()).into(selectCoinIv);
                describeTv.setText(coinListBeanList.get(0).getIntroduce());
                dateTv.setText(MyApplication.getInstance().getString(R.string.collect_describe_1)+"\n" + list.get(0).getIssueDate());
                quantityTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_2)+"\n" + list.get(0).getCirculationTotal());
                addressTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_3)+"\n" + list.get(0).getOfficialAddress());
                whitePagerTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_4)+"\n" + list.get(0).getWhitePaper());
                queryTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_5)+"\n" + list.get(0).getBlockQuery());

                if (coinName.equals("SOU")||coinName.equals("USDS")||coinName.equals("HASH")){
//                    memoTv.setVisibility(View.GONE);
//                    copyTv.setVisibility(View.GONE);

                    String mainAccount = Utils.getSpUtils().getString("mainAccount");
                    addressTv.setText(mainAccount);
//

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("address",mainAccount);
                        jsonObject.put("symbol",coinName);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Bitmap bitmap = new QREncode.Builder(CollectMoneyActivity.this)
                            .setContents(jsonObject.toString())//二维码内容
                            .setMargin(1)
                            .build().encodeAsBitmap();
                    qrCodeIv.setImageBitmap(bitmap);
                }else {
                    presenter.getCollectMoneyAddress(coinName);
                }

            }else {
                for (int i=0;i<list.size();i++) {
                    CoinListBean coinListBean = list.get(i);
                    if (fromCoinName.equals(coinListBean.getCoinName())) {
                        String coinName =coinListBean.getCoinName();
                        url = coinListBean.getUrl();
                        coinNameTv.setText(coinName);
                        recyclerViewD.scrollToPosition(i);

                        Glide.with(this).load(coinListBean.getUrl()).into(coinIv);
                        Glide.with(this).load(coinListBean.getBackImgUrl()).into(selectCoinIv);

                        if (coinListBean.getCoinName().equals("SOU")||coinListBean.getCoinName().equals("USDS")||coinName.equals("HASH")){
//                            memoTv.setVisibility(View.GONE);
//                            copyTv.setVisibility(View.GONE);
                            String mainAccount = Utils.getSpUtils().getString("mainAccount");
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("address",mainAccount);
                                jsonObject.put("symbol",coinName);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Bitmap bitmap = new QREncode.Builder(this)
                                    .setContents(jsonObject.toString())//二维码内容
                                    .setMargin(1)
                                    .build().encodeAsBitmap();
                            qrCodeIv.setImageBitmap(bitmap);
                            addressTv.setText(mainAccount);
                        }else {
                            if ("USDT".equals(fromCoinName)){
                                usdtDialog.show();
                            }
                            presenter.getCollectMoneyAddress(coinName);
                        }
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void getCoinListFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getAddressSuccess(CollectMoneyAddressBean address) {
        try {
            String s1 = RsaUtil.decryptByRAKey(address.getAddress());
            url = s1;
        } catch (Exception e) {
            e.printStackTrace();
        }
//        url=address.getAddress();
        memo = address.getMemo();
        String s = coinNameTv.getText().toString();

        toBitmap();
    }

    @Override
    public void getAddressFailure(String errorMsg) {
        toast(errorMsg);
    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
        currencyPopupWindow.dismiss();
        String coinName = list.get(position).getCoinName();
//        url = list.get(position).getUrl();
        coinNameTv.setText(coinName);
        recyclerViewD.scrollToPosition(position);
        Glide.with(this).load(list.get(position).getUrl()).into(coinIv);
        Glide.with(this).load(list.get(position).getBackImgUrl()).into(selectCoinIv);
//
        describeTv.setText(list.get(position).getIntroduce());
        dateTv.setText(MyApplication.getInstance().getString(R.string.collect_describe_1)+"\n" +list.get(position).getIssueDate());
        quantityTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_2)+"\n" + list.get(position).getCirculationTotal());
        addressTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_3)+"\n" + list.get(position).getOfficialAddress());
        whitePagerTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_4)+"\n" + list.get(position).getWhitePaper());
        queryTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_5)+"\n" +list.get(position).getBlockQuery());

        if (coinName.equals("USDT")) {
            usdtDialog.show();
        }else if (coinName.equals("USDS")||coinName.equals("SOU")||coinName.equals("HASH")) {
            String mainAccount = Utils.getSpUtils().getString("mainAccount");
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("address",mainAccount);
                jsonObject.put("symbol",coinName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Bitmap bitmap = new QREncode.Builder(this)
                    .setContents(jsonObject.toString())//二维码内容
                    .setMargin(1)
                    .build().encodeAsBitmap();
            qrCodeIv.setImageBitmap(bitmap);
            addressTv.setText(mainAccount);


        }else {

            presenter.getCollectMoneyAddress(coinName);
        }
//        else {
////            toBitmap();
//
//        }

    }

    private void toBitmap() {


        Bitmap bitmap = new QREncode.Builder(this)
                .setContents(url)//二维码内容
                .setMargin(1)
                .build().encodeAsBitmap();
        qrCodeIv.setImageBitmap(bitmap);

        addressTv.setText(url);
    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wayOneTv:
                wayOneTv.setSelected(true);
                wayTwoTv.setSelected(false);
                wayThreeTv.setSelected(false);
                usdtDialog.dismiss();
                presenter.getCollectMoneyAddress(tetherUSDTBean.getCoinName());
                break;
            case R.id.wayTwoTv:

                wayOneTv.setSelected(false);
                wayThreeTv.setSelected(false);
                wayTwoTv.setSelected(true);
                usdtDialog.dismiss();
                presenter.getCollectMoneyAddress("USDT");
//                url = tetherUSDTBean.getUrl();
//                toBitmap();

                break;
            case R.id.wayThreeTv:

                describeTv.setText(tetherUSDTBean2.getIntroduce());
                dateTv.setText(MyApplication.getInstance().getString(R.string.collect_describe_1)+"\n" +tetherUSDTBean2.getIssueDate());
                quantityTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_2)+"\n" + tetherUSDTBean2.getCirculationTotal());
                addressTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_3)+"\n" + tetherUSDTBean2.getOfficialAddress());
                whitePagerTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_4)+"\n" + tetherUSDTBean2.getWhitePaper());
                queryTv.setText(MyApplication.getInstance().getResources().getString(R.string.collect_describe_5)+"\n" +tetherUSDTBean2.getBlockQuery());

                wayOneTv.setSelected(false);
                wayTwoTv.setSelected(false);
                wayThreeTv.setSelected(true);
                usdtDialog.dismiss();
                presenter.getCollectMoneyAddress(tetherUSDTBean2.getCoinName());

//                toBitmap();


                break;
            case R.id.closeIv:
                usdtDialog.dismiss();
                toBitmap();
                break;
        }
    }
}
