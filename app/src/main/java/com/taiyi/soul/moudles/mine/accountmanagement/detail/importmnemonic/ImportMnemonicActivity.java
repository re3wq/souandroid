package com.taiyi.soul.moudles.mine.accountmanagement.detail.importmnemonic;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.AESOperator;
import com.taiyi.soul.utils.CopyUtils;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.zxing.ZXingUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ImportMnemonicActivity extends BaseActivity<NormalView, NormalPresenter> {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.qrCodeIv)
    ImageView qrCodeIv;
    @BindView(R.id.accountNameTv)
    TextView accountNameTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.top)
    ConstraintLayout top;
    private String privateKey;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_import_mnemonic;
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText("Export   Mnemonic");
        ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
        if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
            accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
        }

        String accountName = getIntent().getExtras().getString("accountName");
        accountNameTv.setText(accountName);
        for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
            if (accountInfoBean.getAccount_name().equals(accountName)) {
                String mnemonic = accountInfoBean.getAccount_gen_mnemonic();
                privateKey = AESOperator.getInstance().decrypt(mnemonic);
                Bitmap qrImage = ZXingUtils.createQRImage(privateKey, 148, 148);
                qrCodeIv.setImageBitmap(qrImage);
//                Log.i("dasdasd", "----" + privateKey);
                List<String> list = Arrays.asList(privateKey.split(" "));
                CommonAdapter mnemonicListAdapter = AdapterManger.getMnemonicListAdapter(this, list);
                recyclerView.setAdapter(mnemonicListAdapter);
                break;
            }
        }

    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.copyTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.copyTv:
                toast(getString(R.string.copy_success));
                CopyUtils.CopyToClipboard(this, privateKey);
                break;
        }
    }
}
