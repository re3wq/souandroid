package com.taiyi.soul.moudles.main.deal.present;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.deal.bean.EntrustOneBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustThreeBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustTwoBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：集市交易相关接口
 */
public class EntrustPresent extends BasePresent<EntrustView> {


    /**
     * 集市交易列表
     *
     * @param ordertype
     * @param PageNum
     */
    public void getData(Activity activity, String ordertype, String PageNum, PullRecyclerView pullRecyclerView) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "1");//0:算力市场，1：集市交易
        map.put("ordertype", ordertype);//
        map.put("PageNum", PageNum);//
        map.put("PageSize", "10");//
        HttpUtils.postRequest(BaseUrl.ORDERLIST, this, map, new JsonCallback<BaseResultBean<EntrustOneBean>>() {
            @Override
            public BaseResultBean<EntrustOneBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EntrustOneBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onSuccess(response.body().code, response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EntrustOneBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    public void getTwoData(Activity activity, String ordertype, String PageNum, PullRecyclerView pullRecyclerView) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "1");//0:算力市场，1：集市交易
        map.put("ordertype", ordertype);//
        map.put("PageNum", PageNum);//
        map.put("PageSize", "10");//
        HttpUtils.postRequest(BaseUrl.ORDERLIST, this, map, new JsonCallback<BaseResultBean<EntrustTwoBean>>() {
            @Override
            public BaseResultBean<EntrustTwoBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EntrustTwoBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onTwoSuccess(response.body().code, response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.onFailure();
                }


            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EntrustTwoBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    public void getThreeData(Activity activity, String ordertype, String PageNum, PullRecyclerView pullRecyclerView) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "1");//0:算力市场，1：集市交易
        map.put("ordertype", ordertype);//
        map.put("PageNum", PageNum);//
        map.put("PageSize", "10");//
        HttpUtils.postRequest(BaseUrl.ORDERLIST, this, map, new JsonCallback<BaseResultBean<EntrustThreeBean>>() {
            @Override
            public BaseResultBean<EntrustThreeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EntrustThreeBean>> response) {
                super.onSuccess(response);


                if (response.body().code == 0) {
                    if (null != view)
                        view.onThreeSuccess(response.body().code, response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EntrustThreeBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }


    public void cancelOrder(Activity activity,String postersid) {
        Map<String, String> map = new HashMap<>();
        map.put("postersid", postersid);
        HttpUtils.postRequest(BaseUrl.REVOKEORDER, this, map, new JsonCallback<BaseResultBean<EntrustOneBean>>() {
            @Override
            public BaseResultBean<EntrustOneBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EntrustOneBean>> response) {
                super.onSuccess(response);


                if (response.body().code == 0) {
                    if (null != view)
                        view.onCancelSuccess(response.body().code, response.body().msg_cn);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EntrustOneBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }
}
