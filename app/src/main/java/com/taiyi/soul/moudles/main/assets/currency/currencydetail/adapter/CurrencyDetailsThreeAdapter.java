package com.taiyi.soul.moudles.main.assets.currency.currencydetail.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.CurrencyFragmentBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.view.CircleProgressView;
import com.taiyi.soul.utils.Utils;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class CurrencyDetailsThreeAdapter extends BaseRecyclerAdapter<CurrencyFragmentBean.ListBean> {


    private PullRecyclerView pullRecyclerView;
    private Context context;
    private String coinUrl;

    //    private ItemClick dealItemClick;
    public CurrencyDetailsThreeAdapter(Context context, String coinUrl) {
        this.context = context;
        this.coinUrl = coinUrl;
//        this.dealItemClick = dealItemClick;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.assets_currency_details_item_layout;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final CurrencyFragmentBean.ListBean data) {


        ImageView currency_coin = getView(holder, R.id.currency_coin);
        ImageView progress_img = getView(holder, R.id.progress_img);
        TextView currency_price = getView(holder, R.id.currency_price);
        TextView currency_date = getView(holder, R.id.currency_date);
        TextView currency_address = getView(holder, R.id.currency_address);
        TextView progress_text = getView(holder, R.id.progress_text);
        CircleProgressView mCircleProgressView = getView(holder, R.id.progress);
        Glide.with(context).load(coinUrl).into(currency_coin);
        currency_date.setText(data.create_time);
        if (!TextUtils.isEmpty(data.source)) {
            String quantity = "";
            switch (data.source) {
                case "1"://静态收益

                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.static_income));
                    break;
                case "2"://动态收益
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.dynamic_income));
                    break;
                case "3"://帮扶收益
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.help_income));
                    break;
                case "4"://社区收益
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.community_revenue));
                    break;
                case "5"://平级奖励
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.peer_reward));
                    break;
                case "6"://环球收益
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.global_revenue));
                    break;
                case "7"://购买算力
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.buy_computing_power));
                    break;
                case "8"://算力市场-买单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.moneyall).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.moneyall).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.moneyall).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.market_buy));
                    break;
                case "9"://算力市场-卖单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.market_sell));
                    break;
                case "10"://集市交易-买单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.moneyall).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.moneyall).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.moneyall).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.transaction_buy));
                    break;
                case "11"://集市交易-卖单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.num).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.num).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.num).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.transaction_sell));
                    break;
                case "12"://取消算力市场-买单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.market_buy_cancel));
                    break;
                case "13"://取消算力市场-卖单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.market_sell_cancel));
                    break;
                case "14"://取消集市交易-买单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.transaction_buy_cancel));
                    break;
                case "15"://取消集市交易-卖单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.transaction_sell_cancel));
                    break;
                case "16"://算力市场-吃买单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.market_buy_eat));
                    break;
                case "17"://算力市场-吃卖单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.market_sell_eat));
                    break;
                case "18"://集市交易-吃买单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.transaction_buy_eat));
                    break;
                case "19"://集市交易-吃卖单
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.transaction_sell_eat));
                    break;

                case "20": //兑换

                    if (new BigDecimal(data.num).compareTo(new BigDecimal(data.money)) == 1) {
                        if (data.coin_name.equals("BTC")) {
                            quantity = new BigDecimal(data.num).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                        } else if (data.coin_name.equals("ETH")) {
                            quantity = new BigDecimal(data.num).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                        } else {
                            quantity = new BigDecimal(data.num).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                        }
                        currency_price.setText("-" + quantity);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    } else {
                        if (data.coin_name.equals("BTC")) {
                            quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                        } else if (data.coin_name.equals("ETH")) {
                            quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                        } else {
                            quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                        }
                        currency_price.setText("-" + quantity);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
//                    if (data.coin_name.equals("USDK") || data.coin_name.equals("BKO")) {
//                        currency_address.setText(context.getResources().getString(R.string.transfer_address) + data.addr);
//                    } else
                    currency_address.setText(context.getString(R.string.exchange));
                    break;
                case "21"://债券
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.bond));
                    break;
                case "22"://转账
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.amount).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.amount).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.amount).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.transfer_type.equals("1")) {//转出
                        currency_price.setText("-" + quantity);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));

                        if (TextUtils.isEmpty(data.memo)) {
                            currency_address.setText(context.getResources().getString(R.string.transfer1) + data.to_address);
                        } else {
                            currency_address.setText(context.getResources().getString(R.string.transfer1) + data.to_address + "\n" + "MEMO: " + data.memo);
                        }
                    } else {
                        currency_price.setText("+" + quantity);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));

                        if (TextUtils.isEmpty(data.memo)) {
                            currency_address.setText(context.getResources().getString(R.string.transfer1) + data.out_address);
                        } else {
                            currency_address.setText(context.getResources().getString(R.string.transfer1) + data.out_address + "\n" + "MEMO: " + data.memo);
                        }
                    }
//                    currency_price.setText("-" + decimalFormat.format(data.money));
//                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
////                    currency_address.setText("转账");
//                    if (TextUtils.isEmpty(data.memo)) {
//                        currency_address.setText(context.getResources().getString(R.string.transfer_address) + data.addr);
//                    } else {
//                        currency_address.setText(context.getResources().getString(R.string.transfer_address) + data.addr + "\n" + "MEMO: " + data.memo);
//                    }
                    break;
                case "23"://二元期权
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
//                    currency_address.setText("二元期权");
                    currency_address.setText(context.getResources().getString(R.string.home_main_model));
                    break;
                case "24"://转账地址
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
//                    currency_address.setText("转账地址");
                    currency_address.setText(context.getString(R.string.background_recharge));
                    break;
                case "25"://空投
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    currency_price.setText("-" + quantity);
                    currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.airdrop));
                    break;
                case "26"://
                    String quantity26 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity26 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity26 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity26 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                        currency_price.setText("-" + quantity26);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    currency_address.setText(context.getString(R.string.add_usdn));
                    break;
            }


//            currency_price.setText("-" + data.money);
//            currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
//            if (TextUtils.isEmpty(data.memo)) {
//                currency_address.setText(context.getResources().getString(R.string.transfer_address) + data.addr);
//            }else {
//                currency_address.setText(context.getResources().getString(R.string.transfer_address) + data.addr+"\n"+"MEMO: "+data.memo);
//            }
            if(null!= data.state) {

                if (data.state.equals("0")) {
                    mCircleProgressView.setProgress(100);
                    mCircleProgressView.setVisibility(View.GONE);
                    progress_img.setVisibility(View.VISIBLE);
                } else if (data.state.equals("1")) {
                    progress_img.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.mipmap.transfer_threee_iv).into(progress_img);
                    progress_text.setText(context.getResources().getString(R.string.currency_type_one));
                    progress_text.setVisibility(View.GONE);
                    mCircleProgressView.setVisibility(View.GONE);
//                progress_text.setText(context.getResources().getString(R.string.currency_type_one));
//                mCircleProgressView.setVisibility(View.GONE);
                } else if (data.state.equals("2")) {
                    progress_img.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.mipmap.transfer_refuse_iv).into(progress_img);
                    progress_text.setText(context.getResources().getString(R.string.currency_type_two));
                    progress_text.setVisibility(View.GONE);
                    mCircleProgressView.setVisibility(View.GONE);
                }
            }
        } else {
            currency_price.setText("-" + data.amount);
            currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
            if (TextUtils.isEmpty(data.memo)) {
                currency_address.setText(context.getResources().getString(R.string.transfer1) + data.to_address);
            } else {
                currency_address.setText(context.getResources().getString(R.string.transfer1) + data.to_address + "\n" + "MEMO: " + data.memo);
            }

        }
        if(null!=data.staus) {
            if (data.staus.equals("1")) {
                mCircleProgressView.setVisibility(View.VISIBLE);

                mCircleProgressView.setRadius(35);//设置半径
                mCircleProgressView.setStokewidth(8);//设置环宽
                mCircleProgressView.setTextSize(1);//设置文字进度大小
                mCircleProgressView.setSpeed(0);////设置动画速度，这里的数值是每次进度加一所用时间，所以数值越小动画速度越快
                //设置颜色（环的颜色，进度条的颜色，文字进度的字体颜色）
                mCircleProgressView.setColor(context.getResources().getColor(R.color.color_394048), context.getResources().getColor(R.color.color_b7957f), context.getResources().getColor(R.color.transparent));


                int currency_affirm = Utils.getSpUtils().getInt("currency_affirm");
                if(null == data.confirmations){
                    data.confirmations = "0";
                }
                if (currency_affirm > Integer.parseInt(data.confirmations)) {
                    if (null == data.confirmations || data.confirmations.equals("0")) {
                        mCircleProgressView.setVisibility(View.VISIBLE);
                        mCircleProgressView.setProgress(0);
                        progress_text.setText("0");
                        progress_img.setVisibility(View.GONE);
                    } else {
                        mCircleProgressView.setVisibility(View.VISIBLE);
                        mCircleProgressView.setProgress(100 / 6 * Integer.parseInt(data.confirmations));
                        progress_text.setText(data.confirmations);
                        progress_img.setVisibility(View.GONE);
                    }
                }else {
                    mCircleProgressView.setProgress(100);//设置进度
                    mCircleProgressView.setVisibility(View.GONE);
                    progress_img.setVisibility(View.VISIBLE);
                }

//        else if (data.confirmations.equals("6")) {
//            mCircleProgressView.setProgress(100);
////            progress_text.setBackground(context.getResources().getDrawable(R.mipmap.default_address_iv));
//            mCircleProgressView.setVisibility(View.GONE);
//            progress_text.setText("");
//            progress_text.setBackground(context.getResources().getDrawable(R.mipmap.transfer_complete_iv));
//        } else {
//            mCircleProgressView.setProgress(100);//设置进度
//            mCircleProgressView.setVisibility(View.GONE);
//            progress_text.setText("");
//            progress_text.setBackground(context.getResources().getDrawable(R.mipmap.transfer_complete_iv));
//        }
            } else if (data.staus.equals("0")) {
//                progress_text.setText(context.getResources().getString(R.string.currency_type_one));
//                mCircleProgressView.setVisibility(View.GONE);
                progress_img.setVisibility(View.VISIBLE);
                Glide.with(context).load(R.mipmap.transfer_threee_iv).into(progress_img);
                progress_text.setText(context.getResources().getString(R.string.currency_type_one));
                progress_text.setVisibility(View.GONE);
                mCircleProgressView.setVisibility(View.GONE);
            } else if (data.staus.equals("2")) {
                progress_img.setVisibility(View.VISIBLE);
                Glide.with(context).load(R.mipmap.transfer_refuse_iv).into(progress_img);
                progress_text.setText(context.getResources().getString(R.string.currency_type_two));
                progress_text.setVisibility(View.GONE);
                mCircleProgressView.setVisibility(View.GONE);
            }
        }
//        currency_address.setText(context.getResources().getString(R.string.transfer_address) + data.toAddress);


    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }

    //买卖单点击
    public interface ItemClick {
        void onDeal(int position);
    }
}

