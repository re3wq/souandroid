package com.taiyi.soul.moudles.main.node.purchasehashrate;

import android.app.Dialog;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.container.DefaultHeader;
import com.liaoinstan.springview.widget.SpringView;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.EosSignDataManger;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.main.node.bean.ComputerPurchaseRecordBean;
import com.taiyi.soul.moudles.main.node.bean.NodeBean;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.view.CountView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class PurchaseHashrateActivity extends BaseActivity<PurchaseHashrateView, PurchaseHashratePresent> implements PurchaseHashrateView, SpringView.OnFreshListener {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.computingPowerPriceTv)
    TextView computingPowerPriceTv;
    @BindView(R.id.amountTv)
    CountView amountTv;
    @BindView(R.id.inputQuantityEt)
    EditText inputQuantityEt;
//    @BindView(R.id.balanceTv)
//    CountView balanceTv;

    @BindView(R.id.balanceTv)
    TextView balanceTv;
    @BindView(R.id.totalPaymentTv)
    TextView totalPaymentTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.top)
    ConstraintLayout top;
    @BindView(R.id.noDataLl)
    LinearLayout noDataLl;

    @BindView(R.id.key_main)
    RelativeLayout key_main;
    @BindView(R.id.key_scroll)
    LinearLayout key_scroll;
    @BindView(R.id.keyboardPlace)
    LinearLayout keyboardPlace;
    List<ComputerPurchaseRecordBean.ListBean> list = new LinkedList<>();
    private String hash_collect_address;
    private String lastPrice;
    private String totalPrice;
    private int pageNo = 1;
    private int haveAccount;
    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    private SafeKeyboard safeKeyboard;
    private SafeKeyboard safeKeyboard1;
    private OrderPopupWindow pay_popup;
    @BindView(R.id.spring_view)
    SpringView springView;
    private CommonAdapter purchaseComputingPower;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_purchase_hashrate;
    }

    @Override
    public PurchaseHashratePresent initPresenter() {
        return new PurchaseHashratePresent();
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.buy_computing_power));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        springView.setHeader(new DefaultHeader(this));
        springView.setFooter(new DefaultFooter(this));
        springView.setGive(SpringView.Give.BOTH);
        springView.setType(SpringView.Type.FOLLOW);
        springView.setListener(this);


        purchaseComputingPower = AdapterManger.getPurchaseComputingPower(this, list);
        recyclerView.setAdapter(purchaseComputingPower);
        hash_collect_address = getIntent().getExtras().getString("hash_collect_address");
        lastPrice = getIntent().getExtras().getString("lastPrice");
        computingPowerPriceTv.setText(lastPrice);

        inputQuantityEt.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(inputQuantityEt);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                safeKeyboard1 = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
            }
        }, 500);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
            View v = getCurrentFocus();

            if (isShouldHideInput(v, ev)) {
                hideSoftInput(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
//            if (event.getY() > top ) {

            if (event.getY() > top && event.getY() < bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                if (null != safeKeyboard1 && safeKeyboard1.isShow()) {
                    return true;
                } else {
                    return false;
                }

            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard1 != null && safeKeyboard1.stillNeedOptManually(false)) {
                safeKeyboard1.hideKeyboard();
            }

        }
    }

    @Override
    protected void initData() {
        presenter.purchaseRecord(pageNo);
        presenter.getBalance("USDS");
        presenter.getNodeData();
//        presenter.getAccountInfoData(mainAccount);
    }

    @Override
    public void initEvent() {
        inputQuantityEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString())) {
                    if (!s.toString().startsWith("0")) {
                        String s1 = s.toString();
                        totalPrice = new BigDecimal(lastPrice).multiply(new BigDecimal(s1)).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
//                        totalPrice = Double.parseDouble(lastPrice) * Double.parseDouble(s1);
//                        totalPaymentTv.setText(new BigDecimal(totalPrice).setScale(4,BigDecimal.ROUND_DOWN).toPlainString());
                        totalPaymentTv.setText(totalPrice);
                    } else {
                        inputQuantityEt.setText("");
                        toast(getString(R.string.wrong_format));
                    }
                }
            }
        });
    }

    public int buy_memo = 0;

    @OnClick({R.id.backIv, R.id.buyTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.buyTv:
                if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                    if (safeKeyboard.isShow()) {
                        safeKeyboard.hideKeyboard();
                    }
                }

                buy_memo++;
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.buyTv);

                if (!fastDoubleClick) {
                    if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                        String quantity = inputQuantityEt.getText().toString();
                        if (TextUtils.isEmpty(quantity)) {
                            toast(getString(R.string.toast_hash_num));
                            return;
                        }
                        if (quantity.endsWith(".") || quantity.startsWith(".")) {
                            toast(getString(R.string.incorrect_format));
                            return;
                        }


                        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay_purchase, null);
                        pay_popup = new OrderPopupWindow(this, v);
                        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
                        TextView popup_buy_price = v.findViewById(R.id.popup_buy_price);
                        TextView popup_wallet_balance = v.findViewById(R.id.popup_wallet_balance);
                        TextView forgot_password = v.findViewById(R.id.forgot_password);
                        popup_wallet_balance.setText(balanceTv.getText().toString());
                        popup_buy_price.setText(totalPaymentTv.getText().toString());

                        RelativeLayout key_main = v.findViewById(R.id.key_main);
                        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
                        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

                        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
                        List<EditText> list = new ArrayList<>();
                        list.add(viewById);
                        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
                        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View vi, MotionEvent ev) {
                                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
                                    if (isShouldHideInputp(viewById, ev, keyboardPlace)) {
                                        hideSoftInputp(view.getWindowToken());
                                    }
                                }
                                return false;
                            }
                        });
                        forgot_password.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle2 = new Bundle();
                                bundle2.putInt("type", 2);
                                ActivityUtils.next(PurchaseHashrateActivity.this, UpdateLoginPasswordActivity.class, bundle2);
                            }
                        });
                        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
                            @Override
                            public void onTextChange(String s, boolean b) {
                                if (s.length() == 6) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            pay_popup.dismiss();

                                            showProgress();
                                            new EosSignDataManger(PurchaseHashrateActivity.this, new EosSignDataManger.Callback() {
                                                @Override
                                                public void onSignSuccess(String sign_data) {
                                                    presenter.purchaseComputer(new BigDecimal(totalPrice).setScale(4, BigDecimal.ROUND_DOWN).toPlainString(), inputQuantityEt.getText().toString(), s, sign_data);
                                                }

                                                @Override
                                                public void fail() {
                                                    hideProgress();
                                                    ToastUtils.showLongToast(getString(R.string.fail));
                                                }
                                            })
                                                    .pushAction(new Gson().toJson(
                                                            new TransferEosMessageBean("buy_hash" + "_" + buy_memo,
                                                                    hash_collect_address,
                                                                    new BigDecimal(totalPrice).setScale(8, BigDecimal.ROUND_DOWN).toPlainString() + " USDS",
                                                                    mainAccount)),
                                                            Constants.USDN_CONTRACT_ADDRESS,
                                                            mainAccount);
                                        }
                                    }, 500);

                                }
                            }
                        });
                        pay_popup.show(view, getWindow(), 1);

                    } else {//未导入私钥 ，去设置
                        String mMainAccount = Utils.getSpUtils().getString("mainAccount");
                        Dialog dialog = new Dialog(this, R.style.MyDialog);
                        View inflate = LayoutInflater.from(this).inflate(R.layout.dialog_import_sub_wallet, null);
                        dialog.setContentView(inflate);
                        inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Bundle bundle = new Bundle();
                                bundle.putString("accountName", mMainAccount);
                                ActivityUtils.next(PurchaseHashrateActivity.this, ImportAccountActivity.class, bundle, false);
                            }
                        });
                        dialog.show();
                    }
                }


                break;
        }
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (safeKeyboard != null && safeKeyboard.isShow()) {
                if (event.getY() > top && event.getY() < bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            } else {
                if (event.getY() > top && event.getY() < (bottom + DensityUtil.dip2px(PurchaseHashrateActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {
                pay_popup.dismiss();
            }
        }
    }

    @Override
    public void purchaseSuccess(String msg) {
        hideProgress();
//        toast(getString(R.string.buy_success));
        AlertDialogShowUtil.toastMessage(PurchaseHashrateActivity.this, msg);
        inputQuantityEt.setText("");
        totalPaymentTv.setText("0.0000");
        presenter.purchaseRecord(1);
        presenter.getNodeData();
        presenter.getBalance("USDS");
    }

    @Override
    public void purchaseFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getPurchaseRecordSuccess(ComputerPurchaseRecordBean mList) {
        hideProgress();
        springView.onFinishFreshAndLoad();
        if (pageNo == 1) {
            list.clear();
            if (mList.getList() == null || mList.getList().size() == 0) {
                noDataLl.setVisibility(View.VISIBLE);
            } else {
                noDataLl.setVisibility(View.GONE);
            }

        }
        list.addAll(mList.getList());
        purchaseComputingPower.notifyDataSetChanged();
    }

    @Override
    public void getPurchaseRecordFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        if ("USDS".equals(symbol)) {
//            balanceTv.setMoney(Float.parseFloat(new BigDecimal(balance).setScale(5,BigDecimal.ROUND_DOWN).toPlainString()));
            balanceTv.setText(new BigDecimal(balance).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        }
    }

    @Override
    public void getNodeDataSuccess(NodeBean nodeBean) {
        hideProgress();
        amountTv.setMoney(Float.parseFloat(nodeBean.getHashrate()));
    }

    @Override
    public void getNodeDataFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean) {
        hideProgress();
        haveAccount = 1;
    }

    @Override
    public void getDataHttpFail(int code) {
        hideProgress();
        if (code == 500) {
            haveAccount = 0;
        }
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        presenter.purchaseRecord(pageNo);
    }

    @Override
    public void onLoadmore() {
        pageNo++;
        presenter.purchaseRecord(pageNo);
    }
}
