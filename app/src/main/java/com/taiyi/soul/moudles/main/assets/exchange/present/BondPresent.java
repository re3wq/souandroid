package com.taiyi.soul.moudles.main.assets.exchange.present;

import android.app.Activity;

import com.alibaba.fastjson.JSONObject;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondExchangeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondFeeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondRecordBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.RedirectsBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.MD5Util;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：兑换相关接口
 */
public class BondPresent extends BasePresent<BondView> {


    //获取债券页面信息
    public void getData(Activity activity) {

        HttpUtils.postRequest(BaseUrl.BONDSYS, this, "", new JsonCallback<BaseResultBean<BondExchangeBean>>() {
            @Override
            public BaseResultBean<BondExchangeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<BondExchangeBean>> response) {
                super.onSuccess(response);

                if (response.body().code == 0) {
                    if (view != null)
                        view.onSuccess(response.body().code, response.body().msg_cn, response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showLongToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                } else {
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<BondExchangeBean>> response) {
                super.onError(response);

            }
        });
    }

    //计算债券花费
    public void getFeeData(Activity activity, String type, String num) {
        Map<String, String> map = new HashMap<>();
        map.put("type", type);//交易期数
        map.put("num1", num);//交易数量
        HttpUtils.postRequest(BaseUrl.BONDSYSCALCULATION, this, map, new JsonCallback<BaseResultBean<BondFeeBean>>() {
            @Override
            public BaseResultBean<BondFeeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<BondFeeBean>> response) {
                super.onSuccess(response);

             if (response.body().code == -1) {
                 if(Constants.isL==0) {
                     Constants.isL = 1;
                     ToastUtils.showLongToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                     ActivityUtils.next(activity, LoginActivity.class, true);
                     Utils.getSpUtils().remove(Constants.TOKEN);
                 }
                } else {
                    if (view != null)
                        view.onFeeDataSuccess(response.body().code, response.body().msg_cn, response.body().data);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<BondFeeBean>> response) {
                super.onError(response);

            }
        });
    }


    //债券
    public void getSendData(Activity activity, String type, String num, String json, String pri, String sign, String paypa) {

        String md5Str = MD5Util.getMD5Str(sign + "bond", MD5Util.MD5_LOWER_CASE);
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", paypa);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("type", type);//交易期数
        map.put("num1", num);
        map.put("json", json);
        map.put("buyprice", pri);
        map.put("sign", md5Str);
        map.put("paypass", password);
        HttpUtils.postRequest(BaseUrl.BONDSEND, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showLongToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                } else {
                    if (view != null)
                        view.onSendSuccess(response.body().code, response.body().msg_cn, response.body().data);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);

            }
        });
    }

    //债券记录
    public void getRecordData(Activity activity, String PageNum) {
        Map<String, String> map = new HashMap<>();
        map.put("PageNum", PageNum);
        map.put("PageSize", "10");
        HttpUtils.postRequest(BaseUrl.BONDRECORD, this, map, new JsonCallback<BaseResultBean<BondRecordBean>>() {
            @Override
            public BaseResultBean<BondRecordBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<BondRecordBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onRecordSuccess(response.body().code, response.body().msg_cn, response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showLongToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                } else {
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<BondRecordBean>> response) {
                super.onError(response);

            }
        });
    }

    /**
     * 获取余额
     */
    public void getBalance(String account, String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account",
                account);

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size() > 0) {
                            if (null != view)
                                view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        } else {
                            if (null != view)
                                view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if (null != view)
                            view.getBalanceSuccess(symbol, "0.0000");
                    }

                });

    }

    //验证交易密码
    public void checkPayPassword(Activity activity, String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn, response.body().msg_cn);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showLongToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                } else if (response.body().code ==3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                } else {
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);

            }
        });
    }

    //获取挂单信息（手续费、数量、价格等）
    public void getFindOrderData(Activity activity) {
        HttpUtils.postRequest(BaseUrl.DEAFINDORDER, this, "", new JsonCallback<BaseResultBean<IssueFeeBean>>() {
            @Override
            public BaseResultBean<IssueFeeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onIssueFeeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showLongToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                } else {
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onError(response);

            }
        });
    }

    //获取文案
    public void getRedirectData(Activity activity) {
        Map map = new HashMap();
        map.put("type", "5");
        HttpUtils.getRequets(BaseUrl.FINDREDIRECT, this, map, new JsonCallback<BaseResultBean<RedirectsBean>>() {
            @Override
            public BaseResultBean<RedirectsBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<RedirectsBean>> response) {
                super.onSuccess(response);
                if (response.body().status == 602) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure(response.body().msg_cn);
                } else {
                    if (null != view) {
                        view.getRedirectDataSuccess(response.body().status, response.body().msg, response.body().data);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<RedirectsBean>> response) {
                super.onError(response);

            }
        });
    }

}
