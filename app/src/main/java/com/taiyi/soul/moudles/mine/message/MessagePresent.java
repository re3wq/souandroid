package com.taiyi.soul.moudles.mine.message;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.message.bean.MessageBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class MessagePresent extends BasePresent<MessageView> {
    public void getMessage(Activity activity) {
        HttpUtils.getRequets(BaseUrl.GET_FIRST_MESSAGE, this, null, new JsonCallback<BaseResultBean<MessageBean>>() {
            @Override
            public BaseResultBean<MessageBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<MessageBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getMessageSuccess(response.body().data);
                    } else if (response.body().status == 602) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else if (response.body().status == 3080005) {//登录失效
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
//                   //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }

                        Utils.getSpUtils().remove(Constants.TOKEN);
                    } else {
                        view.getMessageFailure(response.body().msg);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<MessageBean>> response) {
                super.onError(response);
            }
        });
    }

    public void updateMessageState(int id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("isRead", 2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.UPDATE_MESSAGE_STATE, this, jsonObject.toString(), new JsonCallback<BaseResultBean<Integer>>() {
            @Override
            public BaseResultBean<Integer> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<Integer>> response) {
                super.onSuccess(response);
                if (response.body().status == 200) {
//                    view.updateStateSuccess();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<Integer>> response) {
                super.onError(response);
            }
        });
    }

    public void allRead(Activity activity, String type) {
        Map<String, String> map = new HashMap<>();
        map.put("type", type);
        HttpUtils.postRequest(BaseUrl.MESSAGE_ALL_READ + "?type=" + type, this, map, new JsonCallback<BaseResultBean<Integer>>() {
            @Override
            public BaseResultBean<Integer> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<Integer>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.onSuccess();
                    } else if (response.body().status == 602) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    }  else if (response.body().status == 3080005) {//登录失效
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
//                   //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }

                    }else {
                        view.onFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<Integer>> response) {
                super.onError(response);
            }
        });
    }
}
