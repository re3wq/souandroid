package com.taiyi.soul.moudles.main.home.mall.bean;

import com.taiyi.soul.moudles.main.home.mall.goods.EditCarBean;

public class EditCarNumBean {
        public int type;
        public int position;
        public int code;
        public String msg;
        public EditCarBean mCarBean;

    public EditCarNumBean(int type, int position, int code, String msg, EditCarBean mCarBean) {
        this.type = type;
        this.position = position;
        this.msg = msg;
        this.code = code;
        this.mCarBean = mCarBean;
    }
    public EditCarNumBean(int type, int position, int code, String msg) {
        this.type = type;
        this.position = position;
        this.msg = msg;
        this.code = code;

    }
}
