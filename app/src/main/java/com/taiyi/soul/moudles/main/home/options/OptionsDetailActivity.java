package com.taiyi.soul.moudles.main.home.options;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flyco.tablayout.SlidingTabLayout;
import com.github.fujianlian.klinechart.DataHelper;
import com.github.fujianlian.klinechart.KLineChartAdapter;
import com.github.fujianlian.klinechart.KLineChartView;
import com.github.fujianlian.klinechart.KLineEntity;
import com.github.fujianlian.klinechart.draw.Status;
import com.github.fujianlian.klinechart.formatter.DateFormatter;
import com.google.gson.Gson;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.EosSignDataManger;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.RedirectsBean;
import com.taiyi.soul.moudles.main.home.mall.bean.DemoOptionsBean;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.main.home.options.bean.KLainBean;
import com.taiyi.soul.moudles.main.home.options.bean.KOtherBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionDetailsListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionPopupBean;
import com.taiyi.soul.moudles.main.home.options.fragment.OptionsHistoryFragment;
import com.taiyi.soul.moudles.main.home.options.fragment.OptionsPositionFragment;
import com.taiyi.soul.moudles.main.home.options.presenter.OptionsDetailPresent;
import com.taiyi.soul.moudles.main.home.options.presenter.OptionsDetailView;
import com.taiyi.soul.moudles.main.home.options.view.OptionsCoinPopupWindow;
import com.taiyi.soul.moudles.main.home.options.view.OptionsFilePopupWindow;
import com.taiyi.soul.moudles.main.home.options.view.OptionsImgPopupWindow;
import com.taiyi.soul.moudles.main.home.options.view.OptionsMorePopupWindow;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import butterknife.BindView;
import butterknife.OnClick;

import static android.view.Gravity.CENTER;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\6\1 0003 13:58
 * @Author : yuan
 * @Describe ：期权详情
 */
public class OptionsDetailActivity extends BaseActivity<OptionsDetailView, OptionsDetailPresent> implements OptionsDetailView, OptionsImgPopupWindow.OnItemClick, OptionsMorePopupWindow.OnTimeItemClick, OptionsFilePopupWindow.OnPictureItemClick, AdapterManger.OnOptionsTimeClick, OptionsCoinPopupWindow.OnCoinItemClick {


    @BindView(R.id.iv_back)
    ImageView mIvBack;
    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.kLineChartView)
    KLineChartView mKLineChartView;

    @BindView(R.id.options_minute)
    TextView mOptionsMinute;
    @BindView(R.id.options_one_hour)
    TextView mOptionsOneHour;
    @BindView(R.id.options_four_hour)
    TextView mOptionsFourHour;
    @BindView(R.id.options_one_day)
    TextView mOptionsOneDay;
    @BindView(R.id.options_more)
    TextView mOptionsMore;
    @BindView(R.id.options_depth_map)
    TextView mOptionsDepthMap;
    @BindView(R.id.options_more_img)
    ImageView mOptionsMoreImg;
    @BindView(R.id.option_file_img)
    ImageView mOptionsFileImg;
    @BindView(R.id.img_right)
    ImageView img_right;
    @BindView(R.id.options_file)
    RelativeLayout mOptionsFile;
    @BindView(R.id.options_tab)
    SlidingTabLayout mOptionsTab;
    @BindView(R.id.options_pager)
    ViewPager mOptionsPager;
    @BindView(R.id.option_realtime_price)
    TextView mOptionRealtimePrice;
    @BindView(R.id.option_rate)
    TextView mOptionRate;
    @BindView(R.id.option_float)
    TextView mOptionFloat;
    @BindView(R.id.option_high_price)
    TextView mOptionHighPrice;
    @BindView(R.id.option_low_price)
    TextView mOptionLowPrice;
    @BindView(R.id.option_hour_price)
    TextView mOptionHourPrice;
    @BindView(R.id.option_button1)
    TextView option_button1;
    @BindView(R.id.rl_coin_img)
    RelativeLayout rl_coin_img;


    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private String[] mTitles;

    private List<KLineEntity> datas;
    private KLineChartAdapter mChartAdapter = new KLineChartAdapter();

    private List<TextView> mViews = new ArrayList<>();
    private View views;
    private ArrayList<DemoOptionsBean> mBeanList;
    private ArrayList<String> mTimesList;
    private String mId;
    private OptionPopupBean mPopupBeans;
    private String mTime_id, mNumber_id, mCoin_id;
    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    private TextView mBi_name;
    private TextView mPopup_balance;
    private CommonAdapter mOptionsTimeAdapter;
    private Dialog mDialogOptions;
    private IssueFeeBean mIssueFeeBean;
    private String mName;
    private ImageView mBi_img;

    private String mPopupbalance = "0.0000";
    private OptionsPositionFragment mOptionsPositionFragment;
    private List<List<String>> mLines;


    private long sendTime = 0L;
    private int startanm = 0;
    private static final long HEART_BEAT_RATE = 1000;//每隔1秒进行一次对长连接的心跳检测
    private static final String WEBSOCKET_HOST_AND_PORT = BaseUrl.GETKLINE;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            update();
            handler.postDelayed(this, 5000);
        }
    };
    private List<OptionListBean> mOptionListBeans;
    private OptionsHistoryFragment mOptionsHistoryFragment;
    private int mSignNumber;
    private int mType;
    private OptionsCoinPopupWindow optionsCoinPopupWindow;
    private WebView mDialog_web;
    private String mDataUrl;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;


    private void update() {
        presenter.getKData(this, mName);
        presenter.getKLainData(OptionsDetailActivity.this, mId, times);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_options_detail;
    }

    @Override
    public OptionsDetailPresent initPresenter() {
        return new OptionsDetailPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }

    @Override
    protected void initData() {

        Bundle extras = getIntent().getExtras();
        mId = extras.getString("id");
        mName = extras.getString("name");
        getPageData();
        update();
        getLocalityList();
        initTabLayout();
    }

    private void getLocalityList() {

        mTimesList = new ArrayList<>();

        //k线标题栏点击更多的数据
        mTimesList.add(getResources().getString(R.string.optins_1m));
        mTimesList.add(getResources().getString(R.string.optins_3m));
        mTimesList.add(getResources().getString(R.string.optins_5m));
        mTimesList.add(getResources().getString(R.string.optins_30m));
        mTimesList.add(getResources().getString(R.string.optins_2h));
        mTimesList.add(getResources().getString(R.string.optins_6h));
        mTimesList.add(getResources().getString(R.string.optins_12h));
//        mTimesList.add(getResources().getString(R.string.optins_3d));
        mTimesList.add(getResources().getString(R.string.optins_week));

        //k线标题栏数据列表
        mViews.add(mOptionsMinute);
        mViews.add(mOptionsOneHour);
        mViews.add(mOptionsFourHour);
        mViews.add(mOptionsOneDay);
        mViews.add(mOptionsMore);
        mViews.add(mOptionsDepthMap);


    }

    private String times = "90";
    Timer timer = new Timer();

    private void getPageData() {
        showProgress();
        presenter.getPictureData(this);
        presenter.getPopuData(activity, mId);
        presenter.getFindOrderData(this);

        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 5000);
        presenter.getRedirectData(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
        ShowKeyboard.releaseKeyboard();
//        timer.cancel();
    }

    private void initTabLayout() {
        //0  1


        mTitles = new String[]{getResources().getString(R.string.optins_position),
                getResources().getString(R.string.optins_history)};
        mOptionsPositionFragment = new OptionsPositionFragment(mId);
        mFragments.add(mOptionsPositionFragment);//持仓明细
        mOptionsHistoryFragment = new OptionsHistoryFragment(mId);
        mFragments.add(mOptionsHistoryFragment);//历史明细

        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mOptionsPager.setAdapter(mAdapter);

        mOptionsTab.setIsBackGround(true);
        Bitmap bitmap = BitmapFactory.decodeResource(OptionsDetailActivity.this.getResources(), R.mipmap.node_hash_market_sel_iv);
        mOptionsTab.setDrawBitMap(bitmap, 126);
        mOptionsTab.setViewPager(mOptionsPager);
        mOptionsPager.setCurrentItem(0);
    }

    @Override
    public void initEvent() {

    }


    @OnClick({R.id.iv_back, R.id.options_index, R.id.options_bi_more, R.id.img_right, R.id.options_minute, R.id.options_one_hour, R.id.options_four_hour,
            R.id.options_one_day, R.id.options_more, R.id.options_depth_map, R.id.options_file, R.id.option_button1, R.id.option_button2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.options_index:
                initIndex();
                break;
            case R.id.options_bi_more://右上币种选择
            case R.id.img_right:
//                options_bi_more
                OptionsImgPopupWindow popupWindow = new OptionsImgPopupWindow(this, mBeanList, this);
                popupWindow.showAsDropDown(rl_coin_img, 0, DensityUtil.dip2px(this, -35));
                break;
            case R.id.options_minute://15分钟
                times = "90";
                KTimeType = 0;
                for (int i = 0; i < mViews.size(); i++) {
                    if (i == 0) {
                        mViews.get(0).setTextColor(getResources().getColor(R.color.color_136ee5));
                    } else {
                        mViews.get(i).setTextColor(getResources().getColor(R.color.color_819bbd));
                    }
                }

                mOptionsMoreImg.setImageResource(R.mipmap.options_kline_subscript_nom);
                mOptionsFileImg.setImageResource(R.mipmap.options_kline_file);

                presenter.getKLainData(this, mId, times);
                break;
            case R.id.options_one_hour://一小时
                KTimeType = 1;
                times = "360";
                for (int i = 0; i < mViews.size(); i++) {
                    if (i == 1) {
                        mViews.get(1).setTextColor(getResources().getColor(R.color.color_136ee5));
                    } else {
                        mViews.get(i).setTextColor(getResources().getColor(R.color.color_819bbd));
                    }
                }
                mOptionsMoreImg.setImageResource(R.mipmap.options_kline_subscript_nom);
                mOptionsFileImg.setImageResource(R.mipmap.options_kline_file);

                presenter.getKLainData(this, mId, times);
//                mOptionsFileImg.setBackgroundResource(R.mipmap.options_kline_file_selected);
                break;
            case R.id.options_four_hour://四小时
                KTimeType = 1;
                times = "1440";
                for (int i = 0; i < mViews.size(); i++) {
                    if (i == 2) {
                        mViews.get(2).setTextColor(getResources().getColor(R.color.color_136ee5));
                    } else {
                        mViews.get(i).setTextColor(getResources().getColor(R.color.color_819bbd));
                    }
                }
                mOptionsMoreImg.setImageResource(R.mipmap.options_kline_subscript_nom);
                mOptionsFileImg.setImageResource(R.mipmap.options_kline_file);

                presenter.getKLainData(this, mId, times);
                break;
            case R.id.options_one_day://1天
                KTimeType = 2;
                times = "8640";
                for (int i = 0; i < mViews.size(); i++) {
                    if (i == 3) {
                        mViews.get(3).setTextColor(getResources().getColor(R.color.color_136ee5));
                    } else {
                        mViews.get(i).setTextColor(getResources().getColor(R.color.color_819bbd));
                    }
                }
                mOptionsMoreImg.setImageResource(R.mipmap.options_kline_subscript_nom);
                mOptionsFileImg.setImageResource(R.mipmap.options_kline_file);

                presenter.getKLainData(this, mId, times);
                break;
            case R.id.options_more://klain-更多
                for (int i = 0; i < mViews.size(); i++) {
                    if (i == 4) {
                        mViews.get(4).setTextColor(getResources().getColor(R.color.color_136ee5));
                    } else {
                        mViews.get(i).setTextColor(getResources().getColor(R.color.color_819bbd));
                    }
                }
                mOptionsMoreImg.setImageResource(R.mipmap.options_kline_subscript_selected);
                mOptionsFileImg.setImageResource(R.mipmap.options_kline_file);

                OptionsMorePopupWindow popupWindowT = new OptionsMorePopupWindow(this, mTimesList, this);
                popupWindowT.showAsDropDown(view, 0, 8);

                break;
            case R.id.options_depth_map:
                for (int i = 0; i < mViews.size(); i++) {
                    if (i == 5) {
                        mViews.get(5).setTextColor(getResources().getColor(R.color.color_136ee5));
                    } else {
                        mViews.get(i).setTextColor(getResources().getColor(R.color.color_819bbd));
                    }
                }
                mOptionsMoreImg.setImageResource(R.mipmap.options_kline_subscript_nom);
                mOptionsFileImg.setImageResource(R.mipmap.options_kline_file);
                break;
            case R.id.options_file://
                for (int i = 0; i < mViews.size(); i++) {
                    mViews.get(i).setTextColor(getResources().getColor(R.color.color_819bbd));
                }
                mOptionsMoreImg.setImageResource(R.mipmap.options_kline_subscript_nom);
                mOptionsFileImg.setImageResource(R.mipmap.options_kline_file_selected);

                OptionsFilePopupWindow popupWindowF = new OptionsFilePopupWindow(this, this);
                popupWindowF.showAsDropDown(view, 0, 8);
                break;
            case R.id.option_button1://买涨

                if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                    mType = 0;
                    initPopup(view);
                } else {//未导入私钥 ，去设置
                    Dialog dialog = new Dialog(this, R.style.MyDialog);
                    View inflate = LayoutInflater.from(this).inflate(R.layout.dialog_import_sub_wallet, null);
                    dialog.setContentView(inflate);
                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putString("accountName", mainAccount);
                            ActivityUtils.next(OptionsDetailActivity.this, ImportAccountActivity.class, bundle, false);
                        }
                    });
                    dialog.show();
                }

                break;
            case R.id.option_button2://买跌
                if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                    mType = 1;
                    initPopup(view);
                } else {//未导入私钥 ，去设置
                    Dialog dialog = new Dialog(this, R.style.MyDialog);
                    View inflate = LayoutInflater.from(this).inflate(R.layout.dialog_import_sub_wallet, null);
                    dialog.setContentView(inflate);
                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Bundle bundle = new Bundle();
                            bundle.putString("accountName", mainAccount);
                            ActivityUtils.next(OptionsDetailActivity.this, ImportAccountActivity.class, bundle, false);
                        }
                    });
                    dialog.show();
                }


                break;
        }
    }

    private void initIndex() {
        //说明
        Dialog dialog = new Dialog(OptionsDetailActivity.this, R.style.MyDialog);
        View view = LayoutInflater.from(OptionsDetailActivity.this).inflate(R.layout.popup_options_index, null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);
        TextView dialog_title = view.findViewById(R.id.dialog_title);
        dialog_title.setText(getResources().getString(R.string.options_index_title));
        mDialog_web = view.findViewById(R.id.dialog_web);
        mDialog_web.setBackgroundColor(0);//设置背景色
        mDialog_web.getBackground().setAlpha(0);//设置填充透明度（布局中一定要设置background，不然getbackground会是null）
        mDialog_web.getSettings().setJavaScriptEnabled(true);// 这行代码一定加上否则效果不会出现
        mDialog_web.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }
        });
        if (null != mDataUrl) {
            mDialog_web.loadDataWithBaseURL(null, mDataUrl, "text/html", "utf-8", null);
        }

        dialog.show();
    }


    private void showPassword(View view) { //显示密码框
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);

        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);

        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
                    if (isShouldHideInputp(viewById, ev,keyboardPlace)) {
                        hideSoftInputp(view.getWindowToken());
                    }
                }
                return false;
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(OptionsDetailActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (mBi_name.getText().equals("SOU") || mBi_name.getText().equals("USDS")) {
                                getSign(mType, s);
                            } else
                                presenter.getBuyData(OptionsDetailActivity.this, mTime_id, mNumber_id, "", mCoin_id, mType + "", mId, s);
                            pay_popup.dismiss();
                        }
                    }, 500);
                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }
    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom +DensityUtil.dip2px(OptionsDetailActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {

                pay_popup.dismiss();


            }
        }
    }
    private int buyMemo = 0;

    @SuppressLint("ResourceType")
    private void initPopup(View view) {
        if (null != mPopupBeans) {

            for (int i = 0; i < mPopupBeans._$1.size(); i++) {
                if (i == 1) {
                    mPopupBeans._$1.get(i).select = 2;
                } else
                    mPopupBeans._$1.get(i).select = 1;
            }

            mDialogOptions = new Dialog(OptionsDetailActivity.this, R.style.MyDialog);

            View inflate = LayoutInflater.from(OptionsDetailActivity.this).inflate(R.layout.popup_options_buy, null);
            RecyclerView popup_times = inflate.findViewById(R.id.popup_times);
            RadioGroup popup_radio_group = inflate.findViewById(R.id.popup_radio_group);
            mBi_img = inflate.findViewById(R.id.bi_img);
            mBi_name = inflate.findViewById(R.id.bi_name);
            TextView bi_change = inflate.findViewById(R.id.bi_change);
            TextView submit = inflate.findViewById(R.id.submit);
            mPopup_balance = inflate.findViewById(R.id.popup_balance);
            mDialogOptions.setContentView(inflate);
            mDialogOptions.setCanceledOnTouchOutside(true);
            mPopup_balance.setText(mPopupbalance);
            LinearLayoutManager manager = new LinearLayoutManager(OptionsDetailActivity.this);
            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
            popup_times.setLayoutManager(manager);
            mOptionsTimeAdapter = AdapterManger.getOptionsTimeAdapter(mDialogOptions.getContext(), mPopupBeans._$1, this);
            popup_times.setAdapter(mOptionsTimeAdapter);


            //投资数量
            for (int i = 0; i < mPopupBeans._$2.size(); i++) {
                RadioButton radioButton = new RadioButton(this);
                radioButton.setId(i);
                RadioGroup.LayoutParams lp = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                lp.gravity = CENTER;
                radioButton.setLayoutParams(lp);
                radioButton.setBackgroundResource(R.drawable.bg_options_popup_selected);
                radioButton.setButtonDrawable(null);  //设置RadioButton的样式
                radioButton.setGravity(CENTER);
                //设置文字距离四周的距离
                radioButton.setPadding(8, 0, 8, 0);
                radioButton.setText(mPopupBeans._$2.get(i).num + "");    //设置文字
                radioButton.setTextColor(getResources().getColorStateList(R.drawable.color_options_popup_selected));
                radioButton.setTextSize(9);
                popup_radio_group.addView(radioButton);

            }

            popup_radio_group.check(0);
            mSignNumber = mPopupBeans._$2.get(0).num;
            popup_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    mNumber_id = mPopupBeans._$2.get(checkedId).id;
                    mSignNumber = mPopupBeans._$2.get(checkedId).num;
                }
            });

            //币种余额
            Glide.with(OptionsDetailActivity.this).load(mPopupBeans._$3.get(0).url).into(mBi_img);
            mBi_name.setText(mPopupBeans._$3.get(0).coin_name);
            bi_change.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPopupBeans._$3.size() > 1) {
                        initBiPopup(v);
                    }
                }
            });


            //提交
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.submit);
                    if(!fastDoubleClick){
                        if (null != mPopup_balance.getText().toString() && !"".equals(mPopup_balance.getText().toString())) {
                            if (new BigDecimal(mPopup_balance.getText().toString()).compareTo(new BigDecimal(mSignNumber)) >= 0) {
                                mDialogOptions.cancel();
                                buyMemo++;
                                showPassword(view);
                            } else {
                                AlertDialogShowUtil.toastMessage(OptionsDetailActivity.this, getString(R.string.toast_balance_lacking));
                            }
                        }
                    }
                }
            });
            mDialogOptions.show();
        }

    }

    private void getSign(int type, String s) {//获取签名
        String memo = "";
        String price = "";
        String unit = "";
        String mainAccount = Utils.getSpUtils().getString("mainAccount");
        if (null == mIssueFeeBean) {
            return;
        }
//        memo = "options_buy" + "_" + buyMemo;
        memo = "Contract trading" + "_" + buyMemo;
        if (mBi_name.getText().toString().equals("SOU")) {
            unit = Constants.NGK_CONTRACT_ADDRESS;
//            price = new DecimalFormat("##0.0000").format(new Double(mSignNumber)) + " " + mBi_name.getText().toString();
            price = new BigDecimal(mSignNumber).setScale(4, BigDecimal.ROUND_DOWN).toPlainString() + " " + mBi_name.getText().toString();

        } else {
            unit = Constants.USDN_CONTRACT_ADDRESS;
//            price = new DecimalFormat("##0.00000000").format(new Double(mSignNumber)) + " " + mBi_name.getText().toString();
            price = new BigDecimal(mSignNumber).setScale(8, BigDecimal.ROUND_DOWN).toPlainString() + " " + mBi_name.getText().toString();

        }
//        price = new DecimalFormat("##0.0000").format(new Double(mPopup_balance.getText().toString())) + " " + mBi_name.getText().toString();

        new EosSignDataManger(this, new EosSignDataManger.Callback() {
            @Override
            public void onSignSuccess(String sign_data) {
//                Log.e("sign===", sign_data);
                presenter.getBuyData(OptionsDetailActivity.this, mTime_id, mNumber_id, sign_data, mCoin_id, type + "", mId, s);
            }

            @Override
            public void fail() {

            }
        }).pushAction(new Gson().toJson(new TransferEosMessageBean(memo, mIssueFeeBean.up_down_address, price
                        , mainAccount)),
                unit,
                mainAccount);
    }

    //币种窗口
    private void initBiPopup(View v) {

        optionsCoinPopupWindow = new OptionsCoinPopupWindow(this, mPopupBeans._$3, this);
        optionsCoinPopupWindow.showAsDropDown(v, DensityUtil.dip2px(this, -15), DensityUtil.dip2px(this, -5));
    }


    //右上角图片列表item点击事件
    @Override
    public void onItemClick(int position) {
        //点击选择币种后重置该页面所有数据

        mTvTitle.setText(mOptionListBeans.get(position).instrument_id);
        Glide.with(this).load(mOptionListBeans.get(position).img_url).into(img_right);
        Collections.swap(mBeanList, 0, position);
        Collections.swap(mOptionListBeans, 0, position);

        showProgress();
        mName = mOptionListBeans.get(0).instrument_id;
        presenter.getPopuData(activity, mOptionListBeans.get(0).id);
        presenter.getKData(this, mOptionListBeans.get(0).instrument_id);
        mOptionsPositionFragment.reData(mOptionListBeans.get(0).id);
        mOptionsHistoryFragment.reData(mOptionListBeans.get(0).id);

        for (int i = 0; i < mViews.size(); i++) {
            if (i == 0) {
                mViews.get(0).setTextColor(getResources().getColor(R.color.color_136ee5));
            } else {
                mViews.get(i).setTextColor(getResources().getColor(R.color.color_819bbd));
            }
        }
        mOptionsMoreImg.setImageResource(R.mipmap.options_kline_subscript_nom);
        mOptionsFileImg.setImageResource(R.mipmap.options_kline_file);
        presenter.getKLainData(this, mId, "90");


    }

    @Override
    public void onHide(boolean isHide) {

    }

    private int KTimeType = 0; //0分钟  1 小时  2天  3周

    @Override
    public void onTimeItemClick(int position) {
        if (position == 0) {
            KTimeType = 0;
            times = "6";
            presenter.getKLainData(this, mId, times);
        } else if (position == 1) {
            KTimeType = 0;
            times = "18";
            presenter.getKLainData(this, mId, times);
        } else if (position == 2) {
            KTimeType = 0;
            times = "30";
            presenter.getKLainData(this, mId, times);
        } else if (position == 3) {
            KTimeType = 0;
            times = "180";
            presenter.getKLainData(this, mId, times);
        } else if (position == 4) {
            KTimeType = 1;
            times = "720";
            presenter.getKLainData(this, mId, times);
        } else if (position == 5) {
            KTimeType = 1;
            times = "2160";
            presenter.getKLainData(this, mId, times);
        } else if (position == 6) {
            KTimeType = 1;
            times = "4320";
            presenter.getKLainData(this, mId, times);
        } else if (position == 7) {
            KTimeType = 3;
            times = "60480";
            presenter.getKLainData(this, mId, times);
        }


    }

    @Override
    public void onTimeHide(boolean isHide) {

    }

    @Override
    public void onPictureMainClick(int position) {
        if (position == 0) {
            mKLineChartView.hideSelectData();
            mKLineChartView.changeMainDrawType(Status.MA);
        } else if (position == 1) {
            mKLineChartView.hideSelectData();
            mKLineChartView.changeMainDrawType(Status.NONE);
        } else if (position == 2) {
            mKLineChartView.hideSelectData();
            mKLineChartView.changeMainDrawType(Status.BOLL);
        }
    }

    @Override
    public void onPicturePairHide(int position) {
        if (position == 0) {
            mKLineChartView.hideSelectData();
            mKLineChartView.setChildDraw(0);

        } else if (position == 1) {
            mKLineChartView.hideSelectData();
            mKLineChartView.setChildDraw(1);
        }
    }

    /************************************Call Interface callback****************************************************/
    @Override
    public void getDataSuccess(int status, String msg, OptionDetailsListBean bean) {
        hideProgress();
    }

    //窗口数据
    @Override
    public void getPopupSuccess(int status, String msg, OptionPopupBean bean) {
        hideProgress();
        if (status == 0) {
            mPopupBeans = bean;
            mTime_id = bean._$1.get(1).id;
            mNumber_id = bean._$2.get(0).id;
            mCoin_id = bean._$3.get(0).id;

            if (mPopupBeans._$3.get(0).coin_name.equals("SOU") || mPopupBeans._$3.get(0).coin_name.equals("USDS")) {
//                showProgress();
                presenter.getBalance(OptionsDetailActivity.this, mainAccount, mPopupBeans._$3.get(0).coin_name.toString());
            } else {
//                showProgress();
                presenter.getCurrencyBalance(OptionsDetailActivity.this, mPopupBeans._$3.get(0).coin_name);
            }
        } else {
            AlertDialogShowUtil.toastMessage(this, msg);
        }
    }

    //购买
    @Override
    public void getPopupBuySuccess(int status, String msg) {
        option_button1.setOnClickListener(null);
        hideProgress();
        mDialogOptions.dismiss();
        if (status == 0) {
//            EventBus.getDefault().post("options_success");//提交成功刷新历史和成交明细
            mOptionsPositionFragment.reData(mId);
            presenter.getPopuData(activity, mId);
        }
        AlertDialogShowUtil.toastMessage(this, msg);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                option_button1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideProgress();
                        if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                            mType = 0;
                            initPopup(option_button1);
                        } else {//未导入私钥 ，去设置
                            Dialog dialog = new Dialog(OptionsDetailActivity.this, R.style.MyDialog);
                            View inflate = LayoutInflater.from(OptionsDetailActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                            dialog.setContentView(inflate);
                            inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("accountName", mainAccount);
                                    ActivityUtils.next(OptionsDetailActivity.this, ImportAccountActivity.class, bundle, false);
                                }
                            });
                            dialog.show();
                        }

                    }
                });
            }
        }, 2000);


    }

    //获取余额
    @Override
    public void getCurrencyBalanceSuccess(int code, String msg_cn, String balance) {
        hideProgress();
//        double v = Double.parseDouble(balance);
        mPopupbalance = NumUtils.subZeroAndDot(new BigDecimal(balance).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        if (null != mPopup_balance) {
            mPopup_balance.setText(mPopupbalance);
        }
    }

    //获取余额（USDT与NGK）
    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        hideProgress();
//        double v = Double.parseDouble(balance);
        mPopupbalance = NumUtils.subZeroAndDot(new BigDecimal(balance).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
//        mPopupbalance = NumUtils.subZeroAndDot(format.format(v));
        if (null != mPopup_balance) {
            mPopup_balance.setText(mPopupbalance);
        }
    }

    @Override
    public void onIssueFeeSuccess(int code, String msg_cn, IssueFeeBean bean) {
        hideProgress();
        mIssueFeeBean = bean;
    }

    @Override
    public void getKLineSuccess(int code, String msg_cn, KLainBean bean) {
        hideProgress();
        if(code==200){
            if (null != bean) {
                datas = new ArrayList<>();
                for (int i = 0; i < bean.lines.size(); i++) {
                    //0分钟  1 小时  2天  3周
                    String substring;
                    if (KTimeType == 0) {
                        String[] ts = bean.lines.get(i).get(0).split("T");
                        substring = ts[1].substring(0, 8);
                    } else if (KTimeType == 1) {
                        String[] ts = bean.lines.get(i).get(0).split("T");
//                    substring = ts[1].substring(0, 2) + ":00";
                        substring = ts[0].substring(ts[0].length() - 5, ts[0].length()) + " " + ts[1].substring(0, 2) + ":00";
                    } else {
                        substring = bean.lines.get(i).get(0).substring(0, 10);
                    }

                    KLineEntity kLineEntity = new KLineEntity();
                    kLineEntity.Close = Float.parseFloat(bean.lines.get(i).get(4));
                    kLineEntity.Date = substring;
                    kLineEntity.High = Float.parseFloat(bean.lines.get(i).get(2));
                    kLineEntity.Low = Float.parseFloat(bean.lines.get(i).get(3));
                    kLineEntity.Open = Float.parseFloat(bean.lines.get(i).get(1));
                    kLineEntity.Volume = Float.parseFloat(bean.lines.get(i).get(5));
                    datas.add(kLineEntity);

                    if (i == 0) {
                        mOptionRealtimePrice.setText(new BigDecimal(bean.lines.get(i).get(4)).setScale(4).toPlainString() + "");//实时价格
                        mOptionsPositionFragment.optionsGetPrice(new BigDecimal(bean.lines.get(i).get(4)).setScale(4).toPlainString());
                    }
                }
                Collections.reverse(datas);

                mKLineChartView.setAdapter(mChartAdapter);
                mKLineChartView.setDateTimeFormatter(new DateFormatter());
                mKLineChartView.setGridRows(4);
                mKLineChartView.setGridColumns(4);

                mKLineChartView.justShowLoading();

                //k线动画设置
                new Thread(new Runnable() {
                    @Override
                    public void run() {
//                datas = DataRequest.getALL(OptionsDetailActivity.this).subList(0, 500);
                        DataHelper.calculate(datas);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mChartAdapter.addFooterData(datas);
                                mChartAdapter.notifyDataSetChanged();
                                if (startanm == 0) {
                                    mKLineChartView.setAnimationDuration(2000);
                                    mKLineChartView.startAnimation();
                                    startanm = 1;
                                }
                                mKLineChartView.refreshEnd();
                            }
                        });
                    }
                }).start();


            } else {

            }
        }


    }

    @Override
    public void getKOtherSuccess(int code, String msg_cn, KOtherBean bean) {
        hideProgress();
        if (null != bean) {
            double price = Double.parseDouble(bean.price);
            double high_24h = Double.parseDouble(bean.high_24h);
            double low_24h = Double.parseDouble(bean.low_24h);
            double base_volume_24h = Double.parseDouble(bean.base_volume_24h);

//            mOptionRate.setText("≈" + NumUtils.subZeroAndDot(new BigDecimal(price).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()) + " " + Utils.getSpUtils().getString("cur_coin_symbol"));//约等值
            mOptionRate.setText("≈" + Utils.getSpUtils().getString("coin_symbol")+NumUtils.subZeroAndDot(new BigDecimal(price).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()) );//约等值


            if (bean.conversion.contains("-")) {//浮动
                mOptionFloat.setText(bean.conversion + "%");
                mOptionFloat.setTextColor(getResources().getColor(R.color.color_cco628));
                mOptionRealtimePrice.setTextColor(getResources().getColor(R.color.color_cco628));
            } else if (bean.conversion.equals("0")) {
                mOptionFloat.setText(bean.conversion + "%");
                mOptionFloat.setTextColor(getResources().getColor(R.color.color_d8dadd));
                mOptionRealtimePrice.setTextColor(getResources().getColor(R.color.color_d8dadd));
            } else {
                mOptionFloat.setText("+" + bean.conversion + "%");
                mOptionFloat.setTextColor(getResources().getColor(R.color.color_04a387));
                mOptionRealtimePrice.setTextColor(getResources().getColor(R.color.color_04a387));
            }
            mOptionHighPrice.setText(NumUtils.subZeroAndDot(new BigDecimal(high_24h).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
            mOptionLowPrice.setText(NumUtils.subZeroAndDot(new BigDecimal(low_24h).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
            mOptionHourPrice.setText(NumUtils.subZeroAndDot(new BigDecimal(base_volume_24h).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
        }

    }

    @Override
    public void getPictureDataSuccess(int code, String msg_cn, List<OptionListBean> bean) {
        hideProgress();
        mOptionListBeans = bean;
        mBeanList = new ArrayList<>();
        for (int i = 0; i < bean.size(); i++) {
            if(mOptionListBeans.get(i).instrument_id.equals("USDS")||mOptionListBeans.get(i).instrument_id.equals("SOU")){
            }else {
                mBeanList.add(new DemoOptionsBean(bean.get(i).img_url));
            }

        }
        for (int i = 0; i < mBeanList.size(); i++) {
            if (mOptionListBeans.get(i).instrument_id.equals(mName)) {
                mTvTitle.setText(mName);
                Glide.with(this).load(mOptionListBeans.get(i).img_url).into(img_right);
                Collections.swap(mBeanList, 0, i);
                Collections.swap(mOptionListBeans, 0, i);
            }
        }

    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg_cn, String bean) {
        hideProgress();
        if (code == 0) {


        } else if (code == -1) {  if(Constants.isL==0){
            Constants.isL=1;
            ToastUtils.showShortToast(msg_cn);
           //  AppManager.getAppManager().finishAllActivity();
            ActivityUtils.next(activity, LoginActivity.class, true);
            Utils.getSpUtils().remove(Constants.TOKEN);}
        } else if (code == 3080005) {  if(Constants.isL==0){
            Constants.isL=1;
            ToastUtils.showShortToast(activity.getString(R.string.account_status));
           //  AppManager.getAppManager().finishAllActivity();
            ActivityUtils.next(activity, LoginActivity.class, true);
            Utils.getSpUtils().remove(Constants.TOKEN);}
        } else {
            AlertDialogShowUtil.toastMessage(activity, msg_cn);
        }
    }

    @Override
    public void getRedirectDataSuccess(int code, String msg_cn, RedirectsBean bean) {
        hideProgress();
        if (code == 200) {
            if(null!=bean){
                mDataUrl = bean.content;
            }
        }
    }

    @Override
    public void onFailure() {
        hideProgress();
    }

    /************************************item click****************************************************/
    //时间选择
    @Override
    public void onTimeSelect(int position, String id) {
        mTime_id = id;
        for (int i = 0; i < mPopupBeans._$1.size(); i++) {
            if (i == position) {
                mPopupBeans._$1.get(i).select = 2;
            } else {
                mPopupBeans._$1.get(i).select = 1;
            }
        }
        mOptionsTimeAdapter.notifyDataSetChanged();
    }

    //币种选择
    @Override
    public void onBiSelect(int position, String coinName, String id, String url) {
        optionsCoinPopupWindow.dismiss();
        mCoin_id = id;
        mBi_name.setText(coinName);
        Glide.with(this).load(url).into(mBi_img);
        if (mBi_name.getText().equals("SOU") || mBi_name.getText().equals("USDS")) {
            presenter.getBalance(OptionsDetailActivity.this, mainAccount, mBi_name.getText().toString());
        } else {
            presenter.getCurrencyBalance(OptionsDetailActivity.this, mBi_name.getText().toString());
        }
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
