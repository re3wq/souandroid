package com.taiyi.soul.moudles.mine.accountmanagement.create;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.bean.MnemonicBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.gen.WalletBeanDao;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.moudles.register.bean.WordBean;
import com.taiyi.soul.utils.AESOperator;
import com.taiyi.soul.utils.EncryptUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.Utils;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateAccountStep2Activity extends BaseActivity<CreateAccountStep2View, CreateAccountStep2Present> implements MultiItemTypeAdapter.OnItemClickListener, CreateAccountStep2View {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.selectRecyclerView)
    RecyclerView selectRecyclerView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.cl_title)
    ConstraintLayout cl_title;

    @BindView(R.id.tv_type)
    TextView tv_type;


    private ArrayList<String> selectList;
    private ArrayList<MnemonicBean> mnemonicBeanArrayList = new ArrayList<>();//存儲重新排序後的數組
    private ArrayList<String> sortList = new ArrayList<>();//存儲正確順序數組
    private ArrayList<String> userSelList = new ArrayList<>();//用戶點擊選擇的數組

    private WordBean wordBean;

    private String from = "";
    private String inviteCode;
    private String loginPwd;
    private String accountName;
    private String loginType;
    private String account;
    private int clicks = 1;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_create_account_step2;
    }

    @Override
    public CreateAccountStep2Present initPresenter() {
        return new CreateAccountStep2Present();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        from = getIntent().getStringExtra("from");
        inviteCode = getIntent().getStringExtra("inviteCode");
        accountName = getIntent().getStringExtra("accountName");
        loginPwd = Utils.getSpUtils().getString("loginPwd");
        loginType = Utils.getSpUtils().getInt("loginType") + "";
        if ("1".equals(loginType)) {
            account = Utils.getSpUtils().getString("email");
        } else {
            account = Utils.getSpUtils().getString("phoneNumber");
        }
//        account = getIntent().getStringExtra("account");

        if (from != null && from.equals("register")) {
            titleTv.setText(getString(R.string.register));
            tv_type.setText(getString(R.string.register));

        } else {
            titleTv.setText(getString(R.string.create_account));
            //TODO lxq
            titleTv.setVisibility(View.GONE);
            tv_type.setText(getString(R.string.create));
        }

        wordBean = (WordBean) getIntent().getSerializableExtra("word_bean");
        selectList = wordBean.getWords();
        sortList.clear();
        sortList.addAll(selectList);
        Collections.shuffle(selectList);
        for (String s : selectList) {
            mnemonicBeanArrayList.add(new MnemonicBean(s));
        }
        CommonAdapter mnemonicClickListAdapter = AdapterManger.getMnemonicClickListAdapter(this, mnemonicBeanArrayList);
        recyclerView.setAdapter(mnemonicClickListAdapter);
        mnemonicClickListAdapter.setOnItemClickListener(this);
        CommonAdapter mnemonicListAdapter = AdapterManger.getMnemonicListAdapter(this, userSelList);
        selectRecyclerView.setAdapter(mnemonicListAdapter);
        mnemonicListAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                String s = userSelList.get(position);
                for (MnemonicBean mnemonicBean : mnemonicBeanArrayList) {
                    if (mnemonicBean.getName().equals(s)) {
                        mnemonicBean.setSelect(false);
                        recyclerView.getAdapter().notifyDataSetChanged();
                        break;
                    }
                }
                userSelList.remove(s);
                selectRecyclerView.getAdapter().notifyDataSetChanged();

            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });

    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.verificationTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.verificationTv:
                int count = 0;
                if (userSelList.size() < sortList.size()) {
                    toast(getString(R.string.select_terms));
                    return;
                }
                for (int i = 0; i < sortList.size(); i++) {
                    if (!userSelList.get(i).equals(sortList.get(i))) {
                        ++count;
                        break;
                    }
                }
                if (count > 0) {
                    toast(getString(R.string.terms_err));
                    return;
                }
                showProgress();
                if(clicks==1){
                    clicks=0;
                    presenter.createChildAccount(accountName, inviteCode, wordBean.getPublic_key());
                }

//
                break;
        }
    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
        boolean select = mnemonicBeanArrayList.get(position).isSelect();
        mnemonicBeanArrayList.get(position).setSelect(!select);
        recyclerView.getAdapter().notifyDataSetChanged();
        if (!select) {//選中
            userSelList.add(mnemonicBeanArrayList.get(position).getName());
        } else {
            userSelList.remove(mnemonicBeanArrayList.get(position).getName());
        }
        selectRecyclerView.getAdapter().notifyDataSetChanged();

    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(cl_title).init();
    }

    @Override
    public void createSuccess(EmptyBean emptyBean) {

        hideProgress();
        createAccountInfo();
        Bundle bundle = new Bundle();
        clicks=1;
        bundle.putString("from", from);
        ActivityUtils.next(this, CreateAccountStep3Activity.class, bundle);
        if (from != null && from.equals("register")){

        }else {
            finish();
        }
    }

    private void createAccountInfo() {
        ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
        if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
            accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
        }
        AccountInfoBean accountInfoBean = new AccountInfoBean();
        accountInfoBean.setAccount_name(accountName.trim());
        try {
            accountInfoBean.setAccount_active_private_key(EncryptUtil.getEncryptString(wordBean.getPrivate_key(), loginPwd));
            accountInfoBean.setAccount_owner_private_key(EncryptUtil.getEncryptString(wordBean.getPrivate_key(), loginPwd));
            accountInfoBean.setAccount_gen_mnemonic(AESOperator.getInstance().encrypt(wordBean.getMnemonic(sortList)));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        accountInfoBean.setAccount_active_public_key(wordBean.getPublic_key());
        accountInfoBean.setAccount_owner_public_key(wordBean.getPublic_key());
//        Log.i("dasdasdasdasad","----"+accountInfoBean.toString());

        WalletBean walletBean = null;

        //通过手机号和钱包类型查询有无账户
        if (loginType.equals("1")) {
            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(account)).build().list();
            if (null!=list) {
                if(list.size()>0)
                walletBean = list.get(list.size()-1);
            }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_ema    il.eq(account)).build().unique();
        } else {
            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(account)).build().list();
            if (null!=list) {
                if(list.size()>0)
                walletBean = list.get(list.size()-1);
            }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(account)).build().unique();
        }
        if (walletBean != null) {
            if (accountInfoBeanArrayList.size() == 0) {
                if (from != null && from.equals("register")) {
                    walletBean.setWallet_main_account(accountInfoBean.getAccount_name());
                    Utils.getSpUtils().put("mainAccount", accountInfoBean.getAccount_name());
                }
            }
            accountInfoBeanArrayList.add(accountInfoBean);
            walletBean.setAccount_info(new Gson().toJson(accountInfoBeanArrayList));
            MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
            MyApplication.getInstance().setWalletBean(walletBean);
        }
    }

    @Override
    public void createFailure(String errorMsg) {
        clicks=1;
        hideProgress();
        toast(errorMsg);
    }
}
