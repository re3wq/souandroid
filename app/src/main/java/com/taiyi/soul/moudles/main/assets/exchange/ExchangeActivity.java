package com.taiyi.soul.moudles.main.assets.exchange;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.EosSignDataManger;
import com.taiyi.soul.blockchain.util.StringUtils;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.bean.CurrencyBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.CurrencyBalanceBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRateBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRecordBean;
import com.taiyi.soul.moudles.main.assets.exchange.present.ExchangePresent;
import com.taiyi.soul.moudles.main.assets.exchange.present.ExchangeView;
import com.taiyi.soul.moudles.main.assets.view.CurrencyPopupWindow;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.OnClick;

import static com.taiyi.soul.utils.Utils.getContext;

public class ExchangeActivity extends BaseActivity<ExchangeView, ExchangePresent> implements ExchangeView, AdapterManger.OnExchangeItemClick {


    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_right_text)
    TextView mTvRightText;
    @BindView(R.id.exchange_unit)
    TextView mExchangeUnit;
    @BindView(R.id.exchange_currency_rate)
    TextView exchange_currency_rate;
    @BindView(R.id.exchange)
    LinearLayout exchange;
    @BindView(R.id.coin_img)
    ImageView mCoinImg;
    @BindView(R.id.coin_name)
    TextView mCoinName;
    @BindView(R.id.coin_img_two)
    ImageView mCoinImgTwo;
    @BindView(R.id.coin_name_two)
    TextView mCoinNameTwo;
    @BindView(R.id.change_balance)
    TextView mChangeBalance;//余额
    @BindView(R.id.change_balance_unit)
    TextView change_balance_unit;//余额单位
    @BindView(R.id.change_out_num)
    EditText mChangeOutNum;//兑出数量
    @BindView(R.id.change_int_num)
    TextView mChangeIntNum;//兑入数量
    @BindView(R.id.rate_other)
    TextView rate_other;//旷工费
    @BindView(R.id.rate)
    TextView rate;
    //兑入兑出数量前的图片
    @BindView(R.id.exchange_in_img)
    ImageView exchange_in_img;
    @BindView(R.id.exchange_out_img)
    ImageView exchange_out_img;
//  @BindView(R.id.fee_type)
//    TextView fee_type;
@BindView(R.id.key_main)
RelativeLayout key_main;
    @BindView(R.id.key_scroll)
    LinearLayout key_scroll;
    @BindView(R.id.keyboardPlace)
    LinearLayout keyboardPlace;
    private View mView1;
    private CurrencyPopupWindow mCurrencyPopupWindow;
    private ImageView mCurrency_img;
    private TextView mCurrency_name;
    private RecyclerView mCurrency_recycle;
    private List<CurrencyBean> mList, mList2;
    private ExchangeBean mBean1;
    private ExchangeBean mBean2;
    private String mList_img1;
    private String mList_name1 = "";
    private String mList_img2;
    private String mList_name2 = "";
    Handler handler = new Handler();
    Runnable runnable;
    private String mChange_id;
    private ExchangeRateBean mExchangeRateBean;
    private String mExchange_address;
    private int mStartCode;
    private int getList = 0;
    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    private String mStartMsg;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exchange;
    }

    @Override
    public ExchangePresent initPresenter() {
        return new ExchangePresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        presenter.getFindOrderData(this);
        presenter.getData("");
        mTvTitle.setText(getResources().getString(R.string.exchanges_title));
        mTvRightText.setVisibility(View.VISIBLE);
        mTvRightText.setText(getResources().getString(R.string.exchanges_title_record));

        mChangeOutNum.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        mChangeOutNum.setInputType(EditorInfo.TYPE_NUMBER_FLAG_DECIMAL);
        List<EditText> lists = new ArrayList<>();
        lists.add(mChangeOutNum);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, lists, true);
            }
        },500);

        initCurrencyList();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
            View v = getCurrentFocus();

            if (isShouldHideInput(v, ev)) {
                hideSoftInput(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

      /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (event.getY() > top) {
//                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                if (null!=safeKeyboard&&safeKeyboard.isShow()) {
                    return true;
                } else {
                    return false;
                }

            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        mChangeOutNum.setText("");
        mChangeIntNum.setText("0.0000");
        if(null!=mList_name1&&!"".equals(mList_name1)){
            if (mList_name1.equals("SOU") || mList_name1.equals("USDS")) {
                presenter.getBalance(mainAccount, mList_name1);
            } else {
                presenter.getCurrencyBalance(mList_name1);
            }
        }
    }

    private void initCurrencyList() {
        mView1 = LayoutInflater.from(ExchangeActivity.this).inflate(R.layout.popup_currency_right, null);
        mCurrencyPopupWindow = new CurrencyPopupWindow(ExchangeActivity.this, mView1);
        mCurrency_img = mView1.findViewById(R.id.currency_img);
        mCurrency_name = mView1.findViewById(R.id.currency_name);
        mCurrency_recycle = mView1.findViewById(R.id.currency_recycle);
        mChangeOutNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (runnable != null) {
                    handler.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {

                        if (!mChangeOutNum.getText().toString().equals("") && Double.parseDouble(mChangeOutNum.getText().toString()) > 0) {
                            showProgress();
                            presenter.getExchangeOrderData(mChange_id, mChangeOutNum.getText().toString());

                        } else {
                            mChangeIntNum.setText("0");
                            rate_other.setText("0" + mList_name1);
                        }
                    }
                };
                handler.postDelayed(runnable, 1000);
            }
        });

    }

    public class DecimalDigitsInputFilter implements InputFilter {

        private final int decimalDigits;
        Pattern mPattern;
        private static final String POINTER = ".";


        private static final String ZERO = "0";
        /**
         * Constructor.
         *
         * @param decimalDigits maximum decimal digits
         */
        public DecimalDigitsInputFilter(int decimalDigits) {
            this.decimalDigits = decimalDigits;
            mPattern = Pattern.compile("([0-9]|\\.)*");
        }

        @Override
        public CharSequence filter(CharSequence source,
                                   int start,
                                   int end,
                                   Spanned dest,
                                   int dstart,
                                   int dend) {

            String destText = dest.toString();
            int dotPos = -1;
            int len = dest.length();
            for (int i = 0; i < len; i++) {
                char c = dest.charAt(i);
                if (c == '.' || c == ',') {
                    dotPos = i;
                    break;
                }
            }
            if (dotPos >= 0) {

                // protects against many dots
                if (source.equals(".") || source.equals(",")) {
                    return "";
                }
                // if the text is entered before the dot
                if (dend <= dotPos) {
                    return null;
                }
                if (len - dotPos > decimalDigits) {
                    return "";
                }
            }
            Matcher matcher = mPattern.matcher(source);
            if (!matcher.matches()) {
                return "";
            } else {
                if ((POINTER.equals(source.toString())) && StringUtils.isEmpty(destText)) {  //首位不能输入小数点
                    return "";
                } else if (!POINTER.equals(source.toString()) && ZERO.equals(destText)) { //如果首位输入0，接下来只能输入小数点
                    return "";
                }
            }
            return null;
        }

    }

    @Override
    public void initEvent() {

    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }


    /**
     * @param view
     */
    @OnClick({R.id.iv_back, R.id.tv_right_text, R.id.exchange_more_two, R.id.exchange_more_one, R.id.exchange_all, R.id.exchange_rate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_right_text://兑换记录
                ActivityUtils.next(ExchangeActivity.this, ExchangeRecordActivity.class, false);
                break;
            case R.id.exchange_more_two:
                if(null==mBean2){
                    return;
                }
                Glide.with(ExchangeActivity.this).load(mList_img2).into(mCurrency_img);
                mCurrency_name.setText(mList_name2);
                mCurrency_recycle.setLayoutManager(new LinearLayoutManager(ExchangeActivity.this));

                CommonAdapter adapter = AdapterManger.getExchangeToListAdapter(ExchangeActivity.this, mBean2.cowList, this);
                mCurrency_recycle.setAdapter(adapter);
                mCurrencyPopupWindow.showAsDropDown(view, 0, DensityUtil.dip2px(ExchangeActivity.this, -86));
                break;
            case R.id.exchange_more_one:
                if(null==mBean1){
                    return;
                }
                Glide.with(ExchangeActivity.this).load(mList_img1).into(mCurrency_img);
                mCurrency_name.setText(mList_name1);
                mCurrency_recycle.setLayoutManager(new LinearLayoutManager(ExchangeActivity.this));

                CommonAdapter adapter2 = AdapterManger.getExchangeCurrencyListAdapter(ExchangeActivity.this, mBean1.cowList, this);
                mCurrency_recycle.setAdapter(adapter2);
                mCurrencyPopupWindow.showAsDropDown(exchange, DensityUtil.dip2px(ExchangeActivity.this, -30), DensityUtil.dip2px(ExchangeActivity.this, -86));
                break;
            case R.id.exchange_all:
                if (mList_name1.equals("BTC")) {//8
                    mChangeOutNum.setText(new BigDecimal(mChangeBalance.getText().toString()).setScale(8,BigDecimal.ROUND_DOWN).toPlainString());
                } else if (mList_name1.equals("ETH")) {//6
                    mChangeOutNum.setText(new BigDecimal(mChangeBalance.getText().toString()).setScale(6,BigDecimal.ROUND_DOWN).toPlainString());
                } else {//4
                    mChangeOutNum.setText(new BigDecimal(mChangeBalance.getText().toString()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString());
                }
                break;
            case R.id.exchange_rate:
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.exchange_rate);
                if(!fastDoubleClick){
                    if ("".equals(mChangeOutNum.getText().toString())) {
                        return;
                    }
                    if (new BigDecimal(mChangeOutNum.getText().toString()).compareTo(new BigDecimal(0))== 0) {
                        AlertDialogShowUtil.toastMessage(this, getString(R.string.toast_exchanges_two));
                        return;
                    }
                    if (mStartCode == 2) {
                        AlertDialogShowUtil.toastMessage(this, mStartMsg);
                        return;
                    }
                    if (mStartCode == 0 || mStartCode == 3) {
                        if (mChangeOutNum.getText().toString().equals("")) {
                            AlertDialogShowUtil.toastMessage(this, getString(R.string.toast_exchanges));
                            return;
                        }

                        if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                            buyMemo++;
                            showPassword(view);
                        } else {//未导入私钥 ，去设置
                            Dialog dialog = new Dialog(ExchangeActivity.this, R.style.MyDialog);
                            View inflate = LayoutInflater.from(ExchangeActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                            dialog.setContentView(inflate);
                            inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("accountName", mainAccount);
                                    ActivityUtils.next(ExchangeActivity.this, ImportAccountActivity.class, bundle, false);
                                }
                            });
                            dialog.show();
                        }
                    }
                }

                break;
        }
    }


    /************************************coin list select change****************************************************/

    @Override
    public void onCoinItemClick(int position) {
        getList = 1;
        presenter.getData(mBean1.cowList.get(position).from_coin_name);
        Glide.with(ExchangeActivity.this).load(mBean1.cowList.get(position).url).into(mCoinImg);
        Glide.with(ExchangeActivity.this).load(mBean1.cowList.get(position).url).into(exchange_out_img);

        mCoinName.setText(mBean1.cowList.get(position).from_coin_name);
        mList_img1 = mBean1.cowList.get(position).url;
        mList_name1 = mBean1.cowList.get(position).from_coin_name;

        mCurrencyPopupWindow.dismiss();

        mChangeOutNum.setText("");
        mChangeIntNum.setText("");
        rate_other.setText("");
        mExchangeUnit.setText(mList_name2 + "/" + mList_name1);
        change_balance_unit.setText(mList_name1);


    }

    @Override
    public void onCoinClick(int position) {
        if(position>=mBean2.cowList.size()){
            return;
        }
        Glide.with(ExchangeActivity.this).load(mBean2.cowList.get(position).url).into(mCoinImgTwo);
        Glide.with(ExchangeActivity.this).load(mBean2.cowList.get(position).url).into(exchange_in_img);
        mCoinNameTwo.setText(mBean2.cowList.get(position).to_coin_name);
        mList_img2 = mBean2.cowList.get(position).url;//当前兑换的币种图片
        mList_name2 = mBean2.cowList.get(position).to_coin_name;//当前兑换的币种名称
        mChange_id = mBean2.cowList.get(position).id;//当前兑换的id
        rate.setText("≈" + mBean2.cowList.get(position).usdn_rate);
        mCurrencyPopupWindow.dismiss();
        exchange_currency_rate.setText(mBean2.cowList.get(position).exchange_rate);

        if (!mChangeOutNum.getText().toString().equals("")) {
            showProgress();
            presenter.getExchangeOrderData(mChange_id, mChangeOutNum.getText().toString());
        }
        mChangeOutNum.setText("");
        mChangeIntNum.setText("");
        rate_other.setText("");
        mExchangeUnit.setText(mList_name2 + "/" + mList_name1);
        if (mList_name1.equals("BTC")) {
            mChangeOutNum.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(8)});
        } else if (mList_name1.equals("ETH")) {
            mChangeOutNum.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(6)});
        } else {
            mChangeOutNum.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(4)});
        }
    }

    /************************************Other methods****************************************************/
    private int buyMemo = 0;
    private void getSign(String s) {//获取签名
        String memo = "";
        String price = "";
        String unit = "";

//        memo = "exchange_buy"+"_"+buyMemo;
        memo = "usdn order"+"_"+buyMemo;
        if (mExchangeRateBean.pay_coin_name.equals("SOU")) {
            unit = Constants.NGK_CONTRACT_ADDRESS;
            price =new BigDecimal(mExchangeRateBean.pay_money_all).setScale(4,BigDecimal.ROUND_DOWN).toPlainString() + " " + mExchangeRateBean.pay_coin_name;
//            price = new DecimalFormat("##0.0000").format(new Double(mExchangeRateBean.pay_money_all)) + " " + mExchangeRateBean.pay_coin_name;

        } else {
            unit = Constants.USDN_CONTRACT_ADDRESS;
            price = new BigDecimal(mExchangeRateBean.pay_money_all).setScale(8,BigDecimal.ROUND_DOWN).toPlainString()+ " " + mExchangeRateBean.pay_coin_name;
//            price = new DecimalFormat("##0.00000000").format(new Double(mExchangeRateBean.pay_money_all)) + " " + mExchangeRateBean.pay_coin_name;

        }

//        price = new DecimalFormat("##0.0000").format(new Double(mExchangeRateBean.pay_money_all)) + " " + mExchangeRateBean.pay_coin_name;

        new EosSignDataManger(this, new EosSignDataManger.Callback() {
            @Override
            public void onSignSuccess(String sign_data) {
                presenter.getExchangeData(mChange_id, mChangeOutNum.getText().toString(), sign_data,exchange_currency_rate.getText().toString(),sign_data+exchange_currency_rate.getText().toString(),s);
            }

            @Override
            public void fail() {

            }
        }).pushAction(new Gson().toJson(new TransferEosMessageBean(memo, mExchange_address, price
                        , mainAccount)),
                unit,
                mainAccount);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    private void showPassword(View view) { //显示密码框
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
         safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInputp(viewById, ev,keyboardPlace)) {
                        hideSoftInputp(view.getWindowToken());
                    }
                }
                return false;
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(ExchangeActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showProgress();
                            if(null==mList_name1){
                                AlertDialogShowUtil.toastMessage(getContext(),getString(R.string.socket_time_out));
                                return;
                            }
//                            if (mExchangeRateBean.pay_coin_name.equals("BKO") || mExchangeRateBean.pay_coin_name.equals("USDK")) {
                            if (mList_name1.equals("SOU") || mList_name1.equals("USDS")) {
                                getSign(s);
                            } else {
                                presenter.getExchangeData(mChange_id, mChangeOutNum.getText().toString(), "",exchange_currency_rate.getText().toString(),exchange_currency_rate.getText().toString(),s);
                            }
//                            showProgress();
//                            presenter.checkPayPassword(s);
                            pay_popup.dismiss();
                        }
                    }, 500);
                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }
    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom +DensityUtil.dip2px(ExchangeActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {

                pay_popup.dismiss();


            }
        }
    }
    /************************************Call Interface callback****************************************************/

    @Override
    public void onSuccess(int code, String msg_cn, ExchangeBean bean) {//获取币种列表
        hideProgress();
        if (code == 0) {
            if (getList == 0) {
                //获取币种列表
                mBean1 = bean;
                if (bean.cowList.size() > 0) {
                    getList = 1;
                    String from_coin_name = bean.cowList.get(0).from_coin_name;
                    presenter.getData(from_coin_name);

                    mList_img1 = bean.cowList.get(0).url;
                    mList_name1 = bean.cowList.get(0).from_coin_name;
                    Glide.with(ExchangeActivity.this).load(bean.cowList.get(0).url).into(mCoinImg);
                    mCoinName.setText(bean.cowList.get(0).from_coin_name);
                    Glide.with(ExchangeActivity.this).load(bean.cowList.get(0).url).into(exchange_out_img);
                }
//                if (mList_name1.equals("USDK") || mList_name1.equals("BKO")) {
//                    fee_type.setText(getResources().getString(R.string.exchanges_miner_fee));
//                }else {
//                    fee_type.setText(getResources().getString(R.string.entrust_chargr));
//                }
            } else {
                //获取币种对应的可兑换的列表
                mBean2 = bean;
                getList = 1;
                if (bean.cowList.size() > 0) {

                    mList_img2 = bean.cowList.get(0).url;
                    mList_name2 = bean.cowList.get(0).to_coin_name;
                    Glide.with(ExchangeActivity.this).load(bean.cowList.get(0).url).into(mCoinImgTwo);
                    mCoinNameTwo.setText(bean.cowList.get(0).to_coin_name);
                    mChange_id = bean.cowList.get(0).id;
                    exchange_currency_rate.setText(bean.cowList.get(0).exchange_rate);
                    mExchangeUnit.setText(mList_name2 + "/" + mList_name1);
                    rate.setText("≈" + bean.cowList.get(0).usdn_rate);

                    Glide.with(ExchangeActivity.this).load(bean.cowList.get(0).url).into(exchange_in_img);

                }
                if (mList_name1.equals("SOU") || mList_name1.equals("USDS")) {
                    presenter.getBalance(mainAccount, mList_name1);
                } else {
                    presenter.getCurrencyBalance(mList_name1);
                }
            }
            if (mList_name1.equals("BTC")) {
                mChangeOutNum.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(8)});
            } else if (mList_name1.equals("ETH")) {
                mChangeOutNum.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(6)});
            } else {
                mChangeOutNum.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(4)});
            }
        } else if (code == -1) {
            if(Constants.isL==0){
                Constants.isL=1;

                ToastUtils.showShortToast(msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }

        } else if (code == 3080005) {
            if(Constants.isL==0){
                Constants.isL=1;

                ToastUtils.showShortToast(getString(R.string.account_status));
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }

        } else {
            AlertDialogShowUtil.toastMessage(activity, msg_cn);
        }


    }

    @Override
    public void onGetRateSuccess(int code, String msg_cn, ExchangeRateBean bean) {//获取兑换数量
        mStartCode = code;
        mStartMsg = msg_cn;
        mExchangeRateBean = bean;
        hideProgress();
        if (code == 0) {
            if (mList_name2.equals("BTC")) {
                mChangeIntNum.setText(new BigDecimal(bean.have_money).setScale(8, BigDecimal.ROUND_DOWN).toString());
            } else if (mList_name2.equals("ETH")) {
                mChangeIntNum.setText(new BigDecimal(bean.have_money).setScale(6, BigDecimal.ROUND_DOWN).toString());
            } else {
                mChangeIntNum.setText(new BigDecimal(bean.have_money).setScale(4, BigDecimal.ROUND_DOWN).toString());
            }
                rate_other.setText(bean.pay_charge+ " " + mList_name1);

            exchange_currency_rate.setText(bean.exchange_rate);
        } else if (code == 2 || code == 3) {
            if (mList_name2.equals("BTC")) {
                mChangeIntNum.setText(new BigDecimal(bean.have_money).setScale(8, BigDecimal.ROUND_DOWN).toString());
            } else if (mList_name2.equals("ETH")) {
                mChangeIntNum.setText(new BigDecimal(bean.have_money).setScale(6, BigDecimal.ROUND_DOWN).toString());
            } else {
                mChangeIntNum.setText(new BigDecimal(bean.have_money).setScale(4, BigDecimal.ROUND_DOWN).toString());
            }
            exchange_currency_rate.setText(bean.exchange_rate);
            rate_other.setText(bean.pay_charge+ " " + mList_name1);
            AlertDialogShowUtil.toastMessage(this, msg_cn);
        } else if (code == -1) {
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }

        }else if (code == 3080005) {
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(getString(R.string.account_status));
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }

        }
        else {
            AlertDialogShowUtil.toastMessage(this, msg_cn);
        }

    }

    @Override
    public void onGetExchangerSuccess(int code, String msg_cn, String bean) {//开始兑换
        hideProgress();
        if (code == 0) {
            ActivityUtils.next(ExchangeActivity.this, ExchangeSuccessActivity.class);
        } else if (code == -1) {
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        } else if (code == 3080005) {
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(getString(R.string.account_status));
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        } else {
            AlertDialogShowUtil.toastMessage(activity, msg_cn);
        }

    }

    @Override
    public void onGetExchangerRecordSuccess(int code, String msg_cn, ExchangeRecordBean bean) {//兑换记录

    }

    @Override
    public void onIssueFeeSuccess(IssueFeeBean bean) {
        hideProgress();
        mExchange_address = bean.exchange_address;
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg_cn, String bean) {
        hideProgress();
        if (code == 0) {
            showProgress();

        } else if (code == -1) {
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        } else if (code == 3080005) {
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(getString(R.string.account_status));
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        } else {
            AlertDialogShowUtil.toastMessage(activity, msg_cn);
        }


    }

    @Override
    public void getCurrencyBalanceSuccess(int code, String msg_cn, CurrencyBalanceBean bal) {
        hideProgress();

        if (code == 200) {
            String balance = bal.balance;
//            DecimalFormat format = new DecimalFormat("0.0000");
            String[] s = balance.split(" ");
//            String format1 = format.format(Float.parseFloat(s[0]));
            if (mList_name1.equals("BTC")) {//8
                mChangeBalance.setText(new BigDecimal(s[0]).setScale(8,BigDecimal.ROUND_DOWN).toPlainString());
            } else if (mList_name1.equals("ETH")) {//6
                mChangeBalance.setText(new BigDecimal(s[0]).setScale(6,BigDecimal.ROUND_DOWN).toPlainString());
            } else {//4
                mChangeBalance.setText(new BigDecimal(s[0]).setScale(4,BigDecimal.ROUND_DOWN).toPlainString());
            }

        } else if (code == 602 || code==3080005) {
            if(Constants.isL==0){
                Constants.isL=1;
                toast(msg_cn);
//                AlertDialogShowUtil.toastMessage(activity, msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(activity, LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        } else {
            AlertDialogShowUtil.toastMessage(activity, msg_cn);
        }

    }

    String balanceS;

    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        hideProgress();
        DecimalFormat format = new DecimalFormat("0.0000");
        if (symbol.equals("USDS")) {
            balanceS = balance.substring(0, balance.length() - 4);
        } else {
            balanceS = balance.substring(0, balance.length() - 3);
        }
//        String format1 = format.format(Double.parseDouble(balanceS));
        mChangeBalance.setText(NumUtils.subZeroAndDot(new BigDecimal(balanceS).setScale(4,BigDecimal.ROUND_DOWN).toPlainString()));
    }
}
