package com.taiyi.soul.moudles.mine.invite;

import com.taiyi.soul.moudles.mine.bean.InviteInfoBean;

public interface InviteView {
    void getInviteInfoSuccess(InviteInfoBean inviteInfoBean);
    void onFailure(String errorMsg);
}
