package com.taiyi.soul.moudles.main.home.mall.order.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.home.mall.order.OrderDetailActivity;
import com.taiyi.soul.moudles.main.home.mall.order.adapter.OrderDeliverAdapter;
import com.taiyi.soul.moudles.main.home.mall.order.adapter.OrderReceivedAdapter;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderListPay;
import com.taiyi.soul.moudles.main.home.mall.order.present.OrderPresent;
import com.taiyi.soul.moudles.main.home.mall.order.present.OrderView;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\5\22 0022 10:24
 * @Author : yuan
 * @Describe ：待收货
 */
public class OrderReceivedFragment extends BaseFragment<OrderView, OrderPresent> implements OrderView, AdapterManger.OrderItemClick, OrderDeliverAdapter.OrderItemButtonClick, PullRecyclerView.PullLoadMoreListener, OrderReceivedAdapter.OrderItemButtonClick {
    @BindView(R.id.order_recycle)
    PullRecyclerView mOrderRecycle;
    private OrderReceivedAdapter mFragmentAdapter;

    private int pagerNo = 1;
    private List<OrderBean.OrderlistBean> mOrderlist;

    @Override
    public OrderPresent initPresenter() {
        return new OrderPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mOrderRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        mOrderRecycle.setOnPullLoadMoreListener(this);
        mOrderRecycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new OrderReceivedAdapter(getContext(), this);
        mFragmentAdapter.setPullRecyclerView(mOrderRecycle);
        mOrderRecycle.setItemAnimator(new DefaultItemAnimator());
        mOrderRecycle.setAdapter(mFragmentAdapter);
        mOrderRecycle.refreshWithPull();
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_order;
    }

    @Override
    public void onItemClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("orderType", 3);
        bundle.putString("ordercommodityid", mOrderlist.get(position).ordercommodityid);
        ActivityUtils.next(getActivity(), OrderDetailActivity.class, bundle, false);
    }

    /************************************edit order****************************************************/

    @Override
    public void onRemindShipmentClick(int position) {//提醒发货

    }

    @Override
    public void onCancelOrderClick(int position) {//取消订单

    }

    @Override
    public void onConfirmReceiptClick(int position) {//确认收货
        showProgress();
        presenter.getTakeDeliveryOrder(getActivity(), mOrderlist.get(position).ordercommodityid);
    }

    @Override
    public void onChangeAddressClick(int position) {//更换地址

    }

    @Override
    public void onPayClick(int position) {//支付

    }

    /************************************Call Interface callback****************************************************/

    @Override
    public void onGetListSuccess(OrderBean bean) {
        hideProgress();
        if (pagerNo == 1) {
            mOrderlist = bean.orderlist;
            mFragmentAdapter.setDatas(bean.orderlist);
        } else {
            mOrderlist.addAll(bean.orderlist);
            mFragmentAdapter.addDatas(bean.orderlist);
        }
        mOrderRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onCancelOrderSuccess(String bean) {

    }

    @Override
    public void onEditAddressSuccess(String bean) {

    }

    @Override
    public void onRemindDeliverySuccess(String bean) {

    }

    @Override
    public void onZhiFuLieSuccess(OrderListPay bean) {

    }

    @Override
    public void onFailure() {
        hideProgress();
        mOrderRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onTakeDeliverySuccess(String bean) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(getContext(), bean);
        mOrderRecycle.refreshWithPull();
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if(null!=presenter){
                presenter.getOrderList(getActivity(), pagerNo + "", 2 + "");
            }
        }
    }

    @Override
    public void onRefresh() {
        pagerNo = 1;
        presenter.getOrderList(getActivity(), pagerNo + "", 2 + "");
    }

    @Override
    public void onLoadMore() {
        pagerNo++;
        presenter.getOrderList(getActivity(), pagerNo + "", 2 + "");
    }
}
