package com.taiyi.soul.moudles.main.home.mall.order.present;

import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderListPay;

public interface OrderView {
    void onGetListSuccess(OrderBean bean);
    void onCancelOrderSuccess(String bean);
    void onEditAddressSuccess(String bean);
    void onRemindDeliverySuccess(String bean);
    void onZhiFuLieSuccess(OrderListPay bean);
    void onFailure();
    void onTakeDeliverySuccess(String bean);

}
