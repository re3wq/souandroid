package com.taiyi.soul.moudles.mine.securitycenter.updateemail;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Handler;
import android.os.IBinder;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber.BindPhoneNumberPresent;
import com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber.BindPhoneNumberView;
import com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber.BindSuccessActivity;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.view.countdowntimer.CountDownTimerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

public class BindEmailActivity extends BaseActivity<BindPhoneNumberView, BindPhoneNumberPresent> implements BindPhoneNumberView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.logoIv)
    ImageView logoIv;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.backIv)
    ImageView backIv;
    @BindView(R.id.emailAddressEt)
    EditText emailAddressEt;
    @BindView(R.id.verificationCodeEt)
    EditText verificationCodeEt;
    @BindView(R.id.getVerificationCodeTv)
    TextView getVerificationCodeTv;
    @BindView(R.id.ll)
    LinearLayout ll;
    @BindView(R.id.top)
    ConstraintLayout top;
    int type = 0;
    private String passWord = "";
    private int loginType;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;
    private int bindType;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_bind_email;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }


    @Override
    public BindPhoneNumberPresent initPresenter() {
        return new BindPhoneNumberPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        bindType = getIntent().getExtras().getInt("type");
        if (bindType != 0) {
            titleTv.setText(getString(R.string.update_email));

        } else {
            titleTv.setText(getString(R.string.bind_email));
        }
        tv.setText("ENTER THE FOLLOWING INFORMATION TO\nCHANGE THE BOUND MAILBOX");
//        tv.setText("ENTER THE FOLLOWING INFORMATION TO\nALSO BIND THE MAILBOX");}
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.getVerificationCodeTv, R.id.nextStepTv})//, R.id.toLoginTv
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.getVerificationCodeTv:
                String phoneNumber = emailAddressEt.getText().toString();
                if (TextUtils.isEmpty(phoneNumber)) {
                    toast(getString(R.string.email_input));
                    return;
                }

//                if (!phoneNumber.contains("@")) {
//                    toast("邮箱格式错误");
//                    return;
//                }
//                showProgress();
//                presenter.getVerificationCode("", phoneNumber);
                if (emailFormat(phoneNumber)) {
                    showProgress();
                    presenter.getVerificationCode("", phoneNumber);
                } else {
                    toast(getString(R.string.email_err));
                }

                break;
            case R.id.nextStepTv:
                String phoneNumber1 = emailAddressEt.getText().toString();
                if (TextUtils.isEmpty(phoneNumber1)) {
                    toast(getString(R.string.email_input));
                    return;
                }
                if (emailFormat(phoneNumber1)) {
                    String verificationCode = verificationCodeEt.getText().toString();
                    if (TextUtils.isEmpty(verificationCode)) {
                        toast(getString(R.string.input_code));
                        return;
                    }
                    showPassword(view, phoneNumber1, verificationCode);
                } else {
                    toast(getString(R.string.email_err));
                }

                break;

        }
    }
    private boolean emailFormat(String email) {//邮箱判断正则表达式
        Pattern pattern = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
        Matcher mc = pattern.matcher(email);
        return mc.matches();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    //显示密码框
    private void showPassword(View view, String phoneNumber, String verificationCode) {
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInput(viewById, ev, keyboardPlace)) {
                        hideSoftInput(v.getWindowToken());
                    }
                }
                return false;
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            showProgress();
//                            presenter.checkPayPassword(BondExchangeActivity.this,s);
                            passWord = s;
                            pay_popup.dismiss();
                            showProgress();
                            presenter.bindPhoneNumberOrEmail("", phoneNumber, verificationCode, passWord);
                        }
                    }, 500);

                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }
    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};

            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() -keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom + DensityUtil.dip2px(BindEmailActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {
                pay_popup.dismiss();

            }
        }
    }
    @Override
    public void getVerificationCodeSuccess() {
        hideProgress();
        CountDownTimerUtils countDownTimerUtils = new CountDownTimerUtils(this, getVerificationCodeTv, 60000, 1000, "#a5a9ac");
        countDownTimerUtils.start();

        getVerificationCodeTv.setFocusable(true);
        getVerificationCodeTv.setFocusableInTouchMode(true);
        getVerificationCodeTv.requestFocus();
    }

    @Override
    public void getInvitePersonFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void updateSuccess(String msg) {
        hideProgress();
        toast(msg);
        WalletBean walletBean = MyApplication.getInstance().getWalletBean();
        walletBean.setWallet_email(emailAddressEt.getText().toString());
        MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
        MyApplication.getInstance().setWalletBean(walletBean);
        Utils.getSpUtils().put("email", emailAddressEt.getText().toString());

         Bundle bundle = new Bundle();
        bundle.putInt("starAct", 1);//邮箱
        bundle.putInt("updataType", bindType);//0绑定邮箱，1更换邮箱
        ActivityUtils.next(this, BindSuccessActivity.class, bundle, true);
    }

    @Override
    public void updateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getAreaCodeSuccess(List<AreaBean> list) {

    }

}
