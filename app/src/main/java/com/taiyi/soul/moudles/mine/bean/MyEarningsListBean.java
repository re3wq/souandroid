package com.taiyi.soul.moudles.mine.bean;

import java.util.List;

public class MyEarningsListBean {

    /**
     * total : 4
     * list : [{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":"323213.13","createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":null,"datetime":"2020-07-01"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":"1213.13","createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":null,"datetime":"2020-07-01"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":"6.000000000","createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":null,"datetime":"2020-07-01"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":"20.00000000","createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":null,"datetime":"2020-07-01"}]
     */

    private int total;
    private List<ListBean> list;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : null
         * userId : null
         * levelId : null
         * coinName : null
         * type : null
         * money : 323213.13
         * createTime : null
         * state : null
         * source : null
         * directPushNum : null
         * originStaticIncomeId : null
         * flowId : null
         * comPowerRatio : null
         * ngkRatio : null
         * num : null
         * hash : null
         * parentId : null
         * moneysum : null
         * datetime : 2020-07-01
         */

        private Object id;
        private Object userId;
        private Object levelId;
        private Object coinName;
        private Object type;
        private String money;
        private Object createTime;
        private Object state;
        private Object source;
        private Object directPushNum;
        private Object originStaticIncomeId;
        private Object flowId;
        private Object comPowerRatio;
        private Object ngkRatio;
        private Object num;
        private Object hash;
        private Object parentId;
        private String moneysum;
        private String datetime;

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public Object getLevelId() {
            return levelId;
        }

        public void setLevelId(Object levelId) {
            this.levelId = levelId;
        }

        public Object getCoinName() {
            return coinName;
        }

        public void setCoinName(Object coinName) {
            this.coinName = coinName;
        }

        public Object getType() {
            return type;
        }

        public void setType(Object type) {
            this.type = type;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Object getSource() {
            return source;
        }

        public void setSource(Object source) {
            this.source = source;
        }

        public Object getDirectPushNum() {
            return directPushNum;
        }

        public void setDirectPushNum(Object directPushNum) {
            this.directPushNum = directPushNum;
        }

        public Object getOriginStaticIncomeId() {
            return originStaticIncomeId;
        }

        public void setOriginStaticIncomeId(Object originStaticIncomeId) {
            this.originStaticIncomeId = originStaticIncomeId;
        }

        public Object getFlowId() {
            return flowId;
        }

        public void setFlowId(Object flowId) {
            this.flowId = flowId;
        }

        public Object getComPowerRatio() {
            return comPowerRatio;
        }

        public void setComPowerRatio(Object comPowerRatio) {
            this.comPowerRatio = comPowerRatio;
        }

        public Object getNgkRatio() {
            return ngkRatio;
        }

        public void setNgkRatio(Object ngkRatio) {
            this.ngkRatio = ngkRatio;
        }

        public Object getNum() {
            return num;
        }

        public void setNum(Object num) {
            this.num = num;
        }

        public Object getHash() {
            return hash;
        }

        public void setHash(Object hash) {
            this.hash = hash;
        }

        public Object getParentId() {
            return parentId;
        }

        public void setParentId(Object parentId) {
            this.parentId = parentId;
        }

        public String getMoneysum() {
            return moneysum;
        }

        public void setMoneysum(String moneysum) {
            this.moneysum = moneysum;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
