package com.taiyi.soul.moudles.login;

import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.moudles.register.bean.RegisterBean;

import java.util.List;

public interface LoginView {

    void loginSuccess(int status, String msg, RegisterBean registerBean);

    void loginFailure(int status, String errorMsg);

    void getAreaCodeSuccess(List<AreaBean>list);
    void getNumSuccess(String num);
    void getVerificationCodeSuccess();
    void getInvitePersonFailure(String errorMsg);
}
