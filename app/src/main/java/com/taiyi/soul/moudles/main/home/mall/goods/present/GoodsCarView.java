package com.taiyi.soul.moudles.main.home.mall.goods.present;

import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.EditCarNumBean;
import com.taiyi.soul.moudles.main.home.mall.bean.ShopCarSubmitBean;

public interface GoodsCarView {
    void onSuccess(CarListBean bean);
    void onDeleteSuccess(String bean);
    void onEditNumSuccess(EditCarNumBean bean);
    void onShopCarSubmitSuccess(ShopCarSubmitBean bean);
    void onFailure();
}
