package com.taiyi.soul.moudles.main.home.mall.order.present;

import com.taiyi.soul.moudles.main.home.mall.order.bean.FreightBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.GoodsOrderBean;

public interface CheckConfirmView {
    void onGetFeeSuccess(FreightBean bean);
    void onGoodsBuySuccess(GoodsOrderBean bean);
    void onGoodsCarBuySuccess(GoodsOrderBean bean);
    void onFailure();
}
