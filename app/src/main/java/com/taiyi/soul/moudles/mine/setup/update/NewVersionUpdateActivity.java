package com.taiyi.soul.moudles.mine.setup.update;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.mine.bean.VersionUpdateBean;

import butterknife.BindView;
import butterknife.OnClick;

public class NewVersionUpdateActivity extends BaseActivity<VersionUpdateView, VersionUpdatePresent> implements VersionUpdateView, View.OnClickListener {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.currentVersionTv)
    TextView currentVersionTv;
    @BindView(R.id.top)
    ConstraintLayout top;
    private Dialog dialog;
    PackageInfo packageInfo = null;
    TextView skipTv, nowUpdateTv, forceUpdateTv,updateContentTv;
    private String downloadUrl;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_new_version_update;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public VersionUpdatePresent initPresenter() {
        return new VersionUpdatePresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.new_version_update));

        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionName = packageInfo.versionName;
            currentVersionTv.setText("V" + versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        dialog = new Dialog(this, R.style.MyDialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        View view = LayoutInflater.from(this).inflate(R.layout.version_update_dialog_layout, null);
        updateContentTv = view.findViewById(R.id.updateContentTv);
        skipTv = view.findViewById(R.id.skipTv);
        skipTv.setOnClickListener(this);
        nowUpdateTv = view.findViewById(R.id.nowUpdateTv);
        nowUpdateTv.setOnClickListener(this);
        forceUpdateTv = view.findViewById(R.id.forceUpdateTv);
        forceUpdateTv.setOnClickListener(this);
        dialog.setContentView(view);

    }

    @Override
    protected void initData() {
        showProgress();
        presenter.checkVersionUpdate();
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.currentVersionTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.currentVersionTv:
//                dialog.show();
                showProgress();
                presenter.checkVersionUpdate();
                break;
        }
    }

    @Override
    public void checkVersionUpdateSuccess(VersionUpdateBean versionUpdateBean) {
        hideProgress();
//        int versionCode = packageInfo.versionCode;
        String versionName = "0.0";
        downloadUrl = versionUpdateBean.getUpUrl();
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
//        if (Integer.parseInt(versionUpdateBean.getVersionNum()) > versionCode) {//有新版本
        if (!versionUpdateBean.getVersionNum().equals(versionName)) {//有新版本
            dialog.show();
            updateContentTv.setText(versionUpdateBean.getContent());
            if (versionUpdateBean.isIsForceUp()) {//是否强制更新
                skipTv.setVisibility(View.GONE);
                nowUpdateTv.setVisibility(View.GONE);
                forceUpdateTv.setVisibility(View.VISIBLE);
            }
        } else {
            toast(getResources().getString(R.string.check_version));
        }
    }

    @Override
    public void checkVersionUpdateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skipTv:
                dialog.dismiss();
                break;
            case R.id.nowUpdateTv:
            case R.id.forceUpdateTv:
                dialog.dismiss();
                openBrowser(this, downloadUrl);
                break;
        }
    }

    public void openBrowser(Context context, String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
        startActivity(intent);


        // 注意此处的判断intent.resolveActivity()可以返回显示该Intent的Activity对应的组件名
        // 官方解释 : Name of the component implementing an activity that can display the intent
//        if (intent.resolveActivity(context.getPackageManager()) != null) {
//            ComponentName componentName = intent.resolveActivity(context.getPackageManager());
//            // 打印Log   ComponentName到底是什么
//            context.startActivity(Intent.createChooser(intent, "请选择浏览器"));
//        } else {
//            toast("未找到手机中的浏览器");
//        }
    }

}
