package com.taiyi.soul.moudles.welcome;

import com.taiyi.soul.bean.NodeListBean;
import com.taiyi.soul.bean.TokenListBean;
import com.taiyi.soul.moudles.mine.bean.VersionUpdateBean;

import java.util.List;

public interface WelcomeView {
    void onNodeSuccess(List<NodeListBean> bean);
    void onTokenSuccess(List<TokenListBean> bean);
    void checkVersionUpdateSuccess(VersionUpdateBean versionUpdateBean);
    void checkVersionUpdateFailure(String errorMsg);
    void getRateSuccess(String symbol,String rate);
    void getRateFailure(String errorMsg);
}
