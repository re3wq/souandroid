package com.taiyi.soul.moudles.main.assets.currency.currencydetail.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.CurrencyFragmentBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.view.CircleProgressView;
import com.taiyi.soul.utils.Utils;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class CurrencyDetailsOneAdapter extends BaseRecyclerAdapter<CurrencyFragmentBean.ListBean> {

    private PullRecyclerView pullRecyclerView;
    private Context context;
    private String coinUrl;

    //    private ItemClick dealItemClick;
    public CurrencyDetailsOneAdapter(Context context, String coinUrl) {
        this.context = context;
        this.coinUrl = coinUrl;
//        this.dealItemClick = dealItemClick;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.assets_currency_details_item_layout;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final CurrencyFragmentBean.ListBean data) {


        ImageView currency_coin = getView(holder, R.id.currency_coin);
        ImageView progress_img = getView(holder, R.id.progress_img);
        TextView currency_price = getView(holder, R.id.currency_price);
        TextView currency_date = getView(holder, R.id.currency_date);
        TextView currency_address = getView(holder, R.id.currency_address);
        TextView progress_text = getView(holder, R.id.progress_text);
        CircleProgressView mCircleProgressView = getView(holder, R.id.progress);
        Glide.with(context).load(coinUrl).into(currency_coin);

        currency_date.setText(data.create_time);
//
//        if (data.staus == 1) {
//
//            mCircleProgressView.setRadius(35);//设置半径
//            mCircleProgressView.setStokewidth(8);//设置环宽
//            mCircleProgressView.setTextSize(1);//设置文字进度大小
//            mCircleProgressView.setSpeed(0);////设置动画速度，这里的数值是每次进度加一所用时间，所以数值越小动画速度越快
//            //设置颜色（环的颜色，进度条的颜色，文字进度的字体颜色）
//            mCircleProgressView.setColor(context.getResources().getColor(R.color.color_394048), context.getResources().getColor(R.color.color_b7957f), context.getResources().getColor(R.color.transparent));
//            if (null == data.confirmations || data.confirmations.equals("0")) {
//                mCircleProgressView.setVisibility(View.VISIBLE);
//                mCircleProgressView.setProgress(0);
//                progress_text.setText("0");
//            } else if (data.confirmations.equals("1")) {
//                mCircleProgressView.setVisibility(View.VISIBLE);
//                mCircleProgressView.setProgress(100 / 6 * 1);
//                progress_text.setText("1");
//            } else if (data.confirmations.equals("2")) {
//                mCircleProgressView.setVisibility(View.VISIBLE);
//                mCircleProgressView.setProgress(100 / 6 * 2);
//                progress_text.setText("2");
//            } else if (data.confirmations.equals("3")) {
//                mCircleProgressView.setVisibility(View.VISIBLE);
//                mCircleProgressView.setProgress(100 / 6 * 3);
//                progress_text.setText("3");
//            } else if (data.confirmations.equals("4")) {
//                mCircleProgressView.setVisibility(View.VISIBLE);
//                mCircleProgressView.setProgress(100 / 6 * 4);
//                progress_text.setText("4");
//            } else if (data.confirmations.equals("5")) {
//                mCircleProgressView.setVisibility(View.VISIBLE);
//                mCircleProgressView.setProgress(100 / 6 * 5);
//                progress_text.setText("5");
//            } else if (data.confirmations.equals("6")) {
//                mCircleProgressView.setProgress(100);
//                mCircleProgressView.setVisibility(View.GONE);
//                progress_img.setVisibility(View.VISIBLE);
//            } else {
//                mCircleProgressView.setProgress(100);//设置进度
//                mCircleProgressView.setVisibility(View.GONE);
//                progress_img.setVisibility(View.VISIBLE);
//            }
//        } else if (data.staus == 0) {
//            progress_text.setText(context.getResources().getString(R.string.currency_type_one));
//            mCircleProgressView.setVisibility(View.GONE);
//        } else if (data.staus == 2) {
//            progress_text.setText(context.getResources().getString(R.string.currency_type_two));
//            mCircleProgressView.setVisibility(View.GONE);
//        }
        DecimalFormat decimalFormat = new DecimalFormat(data.precisio);

        if (!TextUtils.isEmpty(data.source)) {
            //1-6收益记录  money
            //7-19 集市交易  算力市场 ----  +charge
            //20 21 23  兑换

            switch (data.source) {
                case "1"://静态收益
                    String quantity = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.static_income));
                    break;
                case "2"://动态收益
                    String quantity2 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity2 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity2 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity2 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity2);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity2);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.dynamic_income));
                    break;
                case "3"://帮扶收益
                    String quantity3 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity3 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity3 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity3 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity3);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity3);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.help_income));
                    break;
                case "4"://社区收益

                    String quantity4 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity4 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity4 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity4 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity4);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity4);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.community_revenue));
                    break;
                case "5"://平级奖励
                    String quantity5 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity5 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity5 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity5 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity5);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity5);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.peer_reward));
                    break;
                case "6"://环球收益
                    String quantity6 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity6 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity6 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity6 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity6);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity6);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.global_revenue));
                    break;
                case "7"://购买算力
                    String quantity7 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity7 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity7 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity7 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity7);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity7);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.buy_computing_power));
                    break;
                case "8"://算力市场-买单
                    String quantity8 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity8 = new BigDecimal(data.moneyall).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity8 = new BigDecimal(data.moneyall).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity8 = new BigDecimal(data.moneyall).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity8);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity8);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.market_buy));
                    break;
                case "9"://算力市场-卖单
                    String quantity9 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity9 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity9 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity9 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity9);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity9);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.market_sell));
                    break;
                case "10"://集市交易-买单
                    String quantity10 = "";
                    if (data.type.equals("1")) {//收入
                        if (data.coin_name.equals("BTC")) {
                            quantity10 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                        } else if (data.coin_name.equals("ETH")) {
                            quantity10 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                        } else {
                            quantity10 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                        }
                        currency_price.setText("+" + quantity10);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        if (data.coin_name.equals("BTC")) {
                            quantity10 = new BigDecimal(data.moneyall).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                        } else if (data.coin_name.equals("ETH")) {
                            quantity10 = new BigDecimal(data.moneyall).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                        } else {
                            quantity10 = new BigDecimal(data.moneyall).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                        }
                        currency_price.setText("-" + quantity10);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.transaction_buy));
                    break;
                case "11"://集市交易-卖单
                    String quantity11 = "";

                    if (data.type.equals("1")) {//收入
                        if (data.coin_name.equals("BTC")) {
                            quantity11 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                        } else if (data.coin_name.equals("ETH")) {
                            quantity11 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                        } else {
                            quantity11 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                        }
                        currency_price.setText("+" + quantity11);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        if (data.coin_name.equals("BTC")) {
                            quantity11 = new BigDecimal(data.num).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                        } else if (data.coin_name.equals("ETH")) {
                            quantity11 = new BigDecimal(data.num).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                        } else {
                            quantity11 = new BigDecimal(data.num).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                        }
                        currency_price.setText("-" + quantity11);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.transaction_sell));
                    break;
                case "12"://取消算力市场-买单
                    String quantity12 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity12 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity12 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity12 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity12);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity12);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.market_buy_cancel));
                    break;
                case "13"://取消算力市场-卖单
                    String quantity13 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity13 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity13 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity13 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity13);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity13);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.market_sell_cancel));
                    break;
                case "14"://取消集市交易-买单
                    String quantity14 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity14 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity14 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity14 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity14);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity14);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.transaction_buy_cancel));
                    break;
                case "15"://取消集市交易-卖单
                    String quantity15 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity15 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity15 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity15 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity15);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity15);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.transaction_sell_cancel));
                    break;
                case "16"://算力市场-吃买单
                    String quantity16 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity16 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity16 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity16 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity16);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity16);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.market_buy_eat));
                    break;
                case "17"://算力市场-吃卖单
                    String quantity17 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity17 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity17 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity17 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity17);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity17);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.market_sell_eat));
                    break;
                case "18"://集市交易-吃买单
                    String quantity18 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity18 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity18 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity18 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity18);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity18);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.transaction_buy_eat));
                    break;
                case "19"://集市交易-吃卖单
                    String quantity19 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity19 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity19 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity19 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity19);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity19);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.transaction_sell_eat));
                    break;

                case "20": //兑换
                    String quantity20 = "";

                    if (new BigDecimal(data.num).compareTo(new BigDecimal(data.money)) == 1) {
                        if (data.type.equals("1")) {//收入
                            if (data.coin_name.equals("BTC")) {
                                quantity20 = new BigDecimal(data.num).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                            } else if (data.coin_name.equals("ETH")) {
                                quantity20 = new BigDecimal(data.num).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                            } else {
                                quantity20 = new BigDecimal(data.num).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                            }
                            currency_price.setText("+" + quantity20);
                            currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                        } else {
                            currency_price.setText("-" + quantity20);
                            currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                        }
                    } else {
                        if (data.coin_name.equals("BTC")) {
                            quantity20 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                        } else if (data.coin_name.equals("ETH")) {
                            quantity20 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                        } else {
                            quantity20 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                        }
                        if (data.type.equals("1")) {//收入
                            currency_price.setText("+" + quantity20);
                            currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                        } else {
                            currency_price.setText("-" + quantity20);
                            currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                        }
                    }
//                    if (data.coin_name.equals("USDK") || data.coin_name.equals("BKO")) {
//                        currency_address.setText(context.getResources().getString(R.string.transfer_address) + data.addr);
//                    } else
                    currency_address.setText(context.getString(R.string.exchange));
                    break;
                case "21"://债券
                    String quantity21 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity21 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity21 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity21 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity21);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity21);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.bond));
                    break;
                case "22"://转账
                    String quantity22 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity22 = new BigDecimal(data.amount).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity22 = new BigDecimal(data.amount).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity22 = new BigDecimal(data.amount).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.transfer_type.equals("1")) {//转出
                        currency_price.setText("-" + quantity22);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));

                        if (TextUtils.isEmpty(data.memo)) {
                            currency_address.setText(context.getResources().getString(R.string.transfer1) + data.to_address);
                        } else {
                            currency_address.setText(context.getResources().getString(R.string.transfer1) + data.to_address + "\n" + "MEMO: " + data.memo);
                        }
                    } else {
                        currency_price.setText("+" + quantity22);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));

                        if (TextUtils.isEmpty(data.memo)) {
                            currency_address.setText(context.getResources().getString(R.string.transfer1) + data.out_address);
                        } else {
                            currency_address.setText(context.getResources().getString(R.string.transfer1) + data.out_address + "\n" + "MEMO: " + data.memo);
                        }
                    }
//                    currency_address.setText("转账");

                    break;
                case "23"://二元期权
                    String quantity23 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity23 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity23 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity23 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity23);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity23);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    //                    currency_address.setText("二元期权");
                    currency_address.setText(context.getResources().getString(R.string.home_main_model));
                    break;
                case "24"://转账地址
                    String quantity24 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity24 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity24 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity24 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity24);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity24);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.background_recharge));
                    break;
                case "25"://空投
                    String quantity25 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity25 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity25 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity25 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity25);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity25);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.airdrop));
                    break;
                case "26"://
                    String quantity26 = "";
                    if (data.coin_name.equals("BTC")) {
                        quantity26 = new BigDecimal(data.money).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                    } else if (data.coin_name.equals("ETH")) {
                        quantity26 = new BigDecimal(data.money).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                    } else {
                        quantity26 = new BigDecimal(data.money).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                    }
                    if (data.type.equals("1")) {//收入
                        currency_price.setText("+" + quantity26);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                    } else {
                        currency_price.setText("-" + quantity26);
                        currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                    }
                    currency_address.setText(context.getString(R.string.add_usdn));
                    break;

            }

            if(null!= data.state) {
                if (data.state.equals("0")) {
                    mCircleProgressView.setProgress(100);
                    mCircleProgressView.setVisibility(View.GONE);
                    progress_img.setVisibility(View.VISIBLE);
                } else if (data.state.equals("1")) {
                    progress_img.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.mipmap.transfer_threee_iv).into(progress_img);
//                    progress_text.setText(context.getResources().getString(R.string.currency_type_one));
                    progress_text.setVisibility(View.GONE);
                    mCircleProgressView.setVisibility(View.GONE);
                } else if (data.state.equals("2")) {
                    progress_img.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.mipmap.transfer_refuse_iv).into(progress_img);
                    progress_text.setText(context.getResources().getString(R.string.currency_type_two));
                    progress_text.setVisibility(View.GONE);
                    mCircleProgressView.setVisibility(View.GONE);
                }
            }
        } else {
            if (data.transfer_type.equals("0")) {//收入

//                if (data.amount.contains("USDK")) {
//                    currency_price.setText("+" + data.amount.substring(0, data.amount.length() - 9));
//                } else if (data.amount.contains("BKO")) {
//                    currency_price.setText("+" + data.amount.substring(0, data.amount.length() - 4));
//                } else {

                currency_price.setText("+" + decimalFormat.format(data.amount));
//                }
                currency_price.setTextColor(context.getResources().getColor(R.color.color_00857c));
                if (TextUtils.isEmpty(data.memo)) {
                    currency_address.setText(context.getResources().getString(R.string.transfer1) + data.out_address);
                } else {
                    currency_address.setText(context.getResources().getString(R.string.transfer1) + data.out_address + "\n" + "MEMO: " + data.memo);
                }
            } else {
//                if (data.amount.contains("USDK")) {
//                    currency_price.setText("-" + data.amount.substring(0, data.amount.length() - 9));
//                } else if (data.amount.contains("BKO")) {
//                    currency_price.setText("-" + data.amount.substring(0, data.amount.length() - 4));
//                } else {
                currency_price.setText("-" + decimalFormat.format(data.amount));
//                }
//                currency_price.setText("-" + data.amount);
                currency_price.setTextColor(context.getResources().getColor(R.color.color_cco628));
                if (TextUtils.isEmpty(data.memo)) {
                    currency_address.setText(context.getResources().getString(R.string.transfer1) + data.to_address);
                } else {
                    currency_address.setText(context.getResources().getString(R.string.transfer1) + data.to_address + "\n" + "MEMO: " + data.memo);
                }
            }
        }

        if (null != data.staus) {
            if (data.staus.equals("1")) {
                mCircleProgressView.setRadius(35);//设置半径
                mCircleProgressView.setStokewidth(8);//设置环宽
                mCircleProgressView.setTextSize(1);//设置文字进度大小
                mCircleProgressView.setSpeed(0);////设置动画速度，这里的数值是每次进度加一所用时间，所以数值越小动画速度越快
                //设置颜色（环的颜色，进度条的颜色，文字进度的字体颜色）
                mCircleProgressView.setColor(context.getResources().getColor(R.color.color_394048), context.getResources().getColor(R.color.color_e62c2c), context.getResources().getColor(R.color.transparent));


                int currency_affirm = Utils.getSpUtils().getInt("currency_affirm");
//                Log.e("currency_affirm===",currency_affirm+"---"+Integer.parseInt(data.confirmations));
                if(null == data.confirmations){
                    data.confirmations = "0";
                }
                if (currency_affirm > Integer.parseInt(data.confirmations)) {
                    if (null == data.confirmations || data.confirmations.equals("0")) {
                        mCircleProgressView.setVisibility(View.VISIBLE);
                        mCircleProgressView.setProgress(0);
                        progress_text.setText("0");
                        progress_img.setVisibility(View.GONE);
                    } else {
                        mCircleProgressView.setVisibility(View.VISIBLE);
                        mCircleProgressView.setProgress(100 / 6 * Integer.parseInt(data.confirmations));
                        progress_text.setText(data.confirmations);
                        progress_img.setVisibility(View.GONE);
                    }
                }else {
                    mCircleProgressView.setProgress(100);//设置进度
                    mCircleProgressView.setVisibility(View.GONE);
                    progress_img.setVisibility(View.VISIBLE);
                }


            } else if (data.staus.equals("0")) {
//                progress_text.setText(context.getResources().getString(R.string.currency_type_one));
//                mCircleProgressView.setVisibility(View.GONE);
                progress_img.setVisibility(View.VISIBLE);
                Glide.with(context).load(R.mipmap.transfer_threee_iv).into(progress_img);
                progress_text.setText(context.getResources().getString(R.string.currency_type_one));
                progress_text.setVisibility(View.GONE);
                mCircleProgressView.setVisibility(View.GONE);
            } else if (data.staus.equals("2")) {
                progress_img.setVisibility(View.VISIBLE);
                Glide.with(context).load(R.mipmap.transfer_refuse_iv).into(progress_img);
                progress_text.setText(context.getResources().getString(R.string.currency_type_two));
                progress_text.setVisibility(View.GONE);
                mCircleProgressView.setVisibility(View.GONE);
            }
        }

    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }

    //买卖单点击
    public interface ItemClick {
        void onDeal(int position);
    }
}

