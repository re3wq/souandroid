package com.taiyi.soul.moudles.main.home.mall.order.present;

import com.taiyi.soul.moudles.main.assets.bean.AssetsBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;

public interface CheckPayView {
    void onCheckPayPasswordSuccess(String bean);

//    void onGetGoodsAllPriceSuccess(AllPriceBean bean);

    void getBalanceSuccess(String symbol, String balance);

    void onIssueFeeSuccess(IssueFeeBean bean);

    void getPayOrderSuccess(String bean);
    void getAssetsSuccess(AssetsBean assetsBean);

    void onFailure();
}
