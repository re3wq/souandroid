package com.taiyi.soul.moudles.main.home.mall.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\5\15 0015 17:32
 * @Author : yuan
 * @Describe ：商品详情bean
 */
public class GoodsDetailBean {

        public ProductBean product;
        public String commodityid;
        public String userid;
        public List<ProductSpecListBean> productSpecList;

        public static class ProductBean {
            public String flag;
            public String ifcollection;
            public String commodityid;
            public String salenum;
            public String type;
            public String brandnamezz;
            public String brandname;
            public String commodityspecid;
            public String zhuanstock;
            public String zhuanprice;
            public String zhuanyunfei;
            public String visitgoodsnum;
            public String price;
            public String businessuserid;
            public String brandid;
            public String sanfurl;
            public String logo;
            public String lableidname;
            public String cashPrice;
            public int stock;
            public String delflag;
            public String hybvprice;
            public String classificationidname;
            public String recommendflag;
            public String stockzong;
            public String createtime;
            public String introimg;
            public String sanfprice;
            public String bvprice;
            public String sanftype;
            public String youxiaoday;
            public String comimgurl;
            public String detailimgurl;
            public String videourl;
            public String name;
            public String conmnum;
            public String businessname;
            public String qiangurl;
            public String hyprice;
            public String categoryname;
            public String combz;
            public String smallimgurl;
            public String distribution_time;
            public String yunfei;
        }

        public static class ProductSpecListBean {
            public String specid;
            public String specName;
            public List<ProListBean> proList;

            public static class ProListBean {
                public String specProName;
                public String specproid;
                public boolean isChecked;
            }
        }
}
