package com.taiyi.soul.moudles.main.home.mall.goods.adapter;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
//public class ShoppingCarAdapter extends BaseQuickAdapter<CarListBean.CartListBean, BaseViewHolder>{
public class ShoppingCarAdapter extends RecyclerView.Adapter<ShoppingCarAdapter.ViewHolder> {


    private Context context;
    private NumClickListener mListener;
    private List<CarListBean.CartListBean> data = new ArrayList<>();
    private ViewHolder mViewHolder;

    private int currPosition = 0;
    Handler handler = new Handler();
    Runnable runnable;


    public ShoppingCarAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<CarListBean.CartListBean> data) {
        this.data = data;
    }

    public void addData(List<CarListBean.CartListBean> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mViewHolder = new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_shopping_cart, null));
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        currPosition = position;
        Glide.with(context).load(data.get(position).smallimgurl).apply(RequestOptions.bitmapTransform(new RoundedCorners(35)).override(300, 300)).into(holder.view);
        holder.cart_name.setText(data.get(position).name);
        holder.cart_price.setText(data.get(position).hyprice+context.getResources().getString(R.string.USDN));
        holder.cart_num.setText(data.get(position).number + "");
       String s1 =data.get(position).number + "";
        holder.cart_num.setSelection(s1.length());
        holder.cart_specification.setText(data.get(position).typename);

        holder.cart_num.setCursorVisible(false);
        holder.cart_num.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    holder.cart_num.setCursorVisible(true);
                }
            }
        });
        holder.cart_reduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onReduceNum(v, position);
            }
        });
        holder.cart_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onAddNum(v, position);
            }
        });
        holder.cart_num.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (runnable != null) {
                    handler.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (!holder.cart_num.getText().toString().equals("")) {
                            mListener.onEditNum(position,holder.cart_num.getText().toString());
                        }
                    }
                };
                handler.postDelayed(runnable, 800);
            }
        });
        holder.cart_checkbox.setChecked(data.get(position).isChecked);
        holder.cart_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                data.get(position).isChecked = isChecked;
                EventBus.getDefault().post("CarCheckedChange");
            }
        });

        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView view;
        TextView cart_name;
        TextView cart_price;
        EditText cart_num;
        TextView cart_specification;
        ImageView cart_reduce;
        ImageView cart_add;
        CheckBox cart_checkbox;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.cart_img);
            cart_name = itemView.findViewById(R.id.cart_name);
            cart_price = itemView.findViewById(R.id.cart_price);
            cart_num = itemView.findViewById(R.id.cart_num);
            cart_reduce = itemView.findViewById(R.id.cart_reduce);
            cart_add = itemView.findViewById(R.id.cart_add);
            cart_checkbox = itemView.findViewById(R.id.cart_checkbox);
            cart_specification = itemView.findViewById(R.id.cart_specification);
        }
    }

    public void setNumClickListener(NumClickListener onListener) {
        mListener = onListener;
    }

    public interface NumClickListener {
        void onAddNum(View view, int postion);

        void onReduceNum(View view, int position);

        void onEditNum(int position, String s);
    }

}

