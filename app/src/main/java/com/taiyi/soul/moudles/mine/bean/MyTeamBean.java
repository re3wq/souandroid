package com.taiyi.soul.moudles.mine.bean;

public class MyTeamBean {

    /**
     * mainAccount : 13032691159
     * headerImg : null
     * communityLevel : null
     * directPushNum : 31
     * totalPeople : 35
     * todayTotalPeople : 0
     * todayAddAchievement : 0
     * totalAchievement : 0
     */

    private String mainAccount;
    private String headerImg;
    private String communityLevel;
    private int directPushNum;
    private int totalPeople;
    private int todayTotalPeople;
    private String todayAddAchievement;
    private String totalAchievement;

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }

    public String getCommunityLevel() {
        return communityLevel;
    }

    public void setCommunityLevel(String communityLevel) {
        this.communityLevel = communityLevel;
    }

    public int getDirectPushNum() {
        return directPushNum;
    }

    public void setDirectPushNum(int directPushNum) {
        this.directPushNum = directPushNum;
    }

    public int getTotalPeople() {
        return totalPeople;
    }

    public void setTotalPeople(int totalPeople) {
        this.totalPeople = totalPeople;
    }

    public int getTodayTotalPeople() {
        return todayTotalPeople;
    }

    public void setTodayTotalPeople(int todayTotalPeople) {
        this.todayTotalPeople = todayTotalPeople;
    }

    public String getTodayAddAchievement() {
        return todayAddAchievement;
    }

    public void setTodayAddAchievement(String todayAddAchievement) {
        this.todayAddAchievement = todayAddAchievement;
    }

    public String getTotalAchievement() {
        return totalAchievement;
    }

    public void setTotalAchievement(String totalAchievement) {
        this.totalAchievement = totalAchievement;
    }
}
