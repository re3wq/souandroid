package com.taiyi.soul.moudles.main.assets.view;

//public class RoundView extends View {
//
//    private int mRingColor;
//    private int mGlobuleColor;
//    private float dimension;
//    private float mGlobuleRadius;
//    private float mCycleTime;
//    private Paint mPaint;
//
//    public RoundView(Context context) {
//        this(context, null);
//    }
//
//    public RoundView(Context context, AttributeSet attrs) {
//        this(context, attrs, 0);
//    }
//
//    public RoundView(Context context, AttributeSet attrs,
//                                  int defStyle) {
//        super(context, attrs, defStyle);
//        TypedArray attrsArray = context.getTheme().obtainStyledAttributes(
//                attrs, R.styleable.AccelerateCircularView, defStyle, 0);
//        mRingColor = attrsArray.getColor(
//                  R.styleable.AccelerateCircularView_ringColor, Color.GRAY);
//        mGlobuleColor = attrsArray.getColor(
//                R.styleable.AccelerateCircularView_globuleColor, Color.BLUE);
//        dimension = attrsArray.getDimension(
//                R.styleable.AccelerateCircularView_ringWidth, TypedValue
//                        .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1,
//                                getResources().getDisplayMetrics()));
//        mGlobuleRadius = attrsArray.getDimension(
//                R.styleable.AccelerateCircularView_globuleRadius, TypedValue
//                        .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6,
//                                getResources().getDisplayMetrics()));
//        mCycleTime  = attrsArray.getFloat(
//                R.styleable.AccelerateCircularView_cycleTime, 3000);
//        attrsArray.recycle();
//        mPaint = new Paint();
//
//    }
//
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        int mWidth , mHeight ;
//        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
//        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
//        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
//        if (widthMode == MeasureSpec.EXACTLY) {
//            mWidth = widthSize;
//        } else {
//            mWidth = 169;
//            if (widthMode == MeasureSpec.AT_MOST) {
//                mWidth = Math.min(mWidth, widthSize);
//            }
//
//        }
//        if (heightMode == MeasureSpec.EXACTLY) {
//            mHeight = heightSize;
//        } else {
//            mHeight = 169;
//            if (heightMode == MeasureSpec.AT_MOST) {
//                mHeight = Math.min(mWidth, heightSize);
//            }
//
//        }
//
//        setMeasuredDimension(mWidth, mHeight);
//    }
//
//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//
//        int central = Math.min(getWidth(), getHeight()) / 2;
//
//     mRingRadius = central - mGlobuleRadius;
//
//        if (mGlobuleRadius < mRingWidth / 2) {// 小球嵌在环里
//            mRingRadius = central - mRingWidth / 2;
//        }
//        mPaint.setStrokeWidth(mRingWidth);
//        mPaint.setStyle(Style.STROKE);
//        mPaint.setAntiAlias(true);
//        mPaint.setColor(mRingColor);
//        canvas.drawCircle(central, central, mRingRadius, mPaint);// 绘制圆环
//        mPaint.setStyle(Style.FILL);
//        mPaint.setAntiAlias(true);
//        mPaint.setColor(mGlobuleColor);
//
//        if (currentAngle == -1) {
//            startCirMotion();
//        }
//        drawGlobule(canvas, central);// 绘制小球
//    }
//    /**
//     * 绘制小球,起始位置为圆环最低点
//     *
//     * @param canvas
//     * @param central
//     */
//    private void drawGlobule(Canvas canvas, float central) {
//
//        float cx = central + (float) (mRingRadius * Math.cos(currentAngle));
//        float cy = (float) (central + mRingRadius * Math.sin(currentAngle));
//        canvas.drawCircle(cx, cy, mGlobuleRadius, mPaint);
//
//    }

//}
