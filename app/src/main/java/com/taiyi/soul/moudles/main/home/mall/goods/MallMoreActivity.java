package com.taiyi.soul.moudles.main.home.mall.goods;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bumptech.glide.Glide;
import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.GoodsDetailBean;
import com.taiyi.soul.moudles.main.home.mall.bean.MallMoreBean;
import com.taiyi.soul.moudles.main.home.mall.bean.TabChangeBean;
import com.taiyi.soul.moudles.main.home.mall.goods.fragment.MallOneFragment;
import com.taiyi.soul.moudles.main.home.mall.goods.fragment.MallThreeFragment;
import com.taiyi.soul.moudles.main.home.mall.goods.fragment.MallTwoFragment;
import com.taiyi.soul.moudles.main.home.mall.goods.present.MallMorePresent;
import com.taiyi.soul.moudles.main.home.mall.goods.present.MallMoreView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.OnClick;

public class MallMoreActivity extends BaseActivity<MallMoreView, MallMorePresent> implements MallMoreView{


    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.img_car)
    ImageView mImgCar;
    @BindView(R.id.tabLayout)
    SlidingTabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    private String[] mTitles;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;

    private int DESC = 0;
    private int isFirst = 0;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_mall_more;
    }

    @Override
    public MallMorePresent initPresenter() {
        return new MallMorePresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        EventBus.getDefault().post(this);
        presenter.getCarSizeData(this, 1 + "");
    }

    @Subscribe
    public void even(String event){
         if("car_change".equals(event)){
             presenter.getCarSizeData(this, 1 + "");
         }
    }
    @Override
    protected void initData() {
        mImgCar.setVisibility(View.VISIBLE);
        mTvTitle.setText(getString(R.string.mall_more));
        mTitles = new String[]{this.getResources().getString(R.string.mall_more_all),
                this.getResources().getString(R.string.mall_more_sales),
                this.getResources().getString(R.string.mall_more_price)
        };

        mFragments.add(new MallOneFragment());//综合
        mFragments.add(new MallTwoFragment());//销量
        mFragments.add(new MallThreeFragment());//价格

//        mFragments.add(new MallFourFragment());//VIP


        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mTabLayout.post(new Runnable() {
            @Override
            public void run() {

                mTabLayout.setIsBackGround(true);
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.node_hash_market_sel_iv);
                mTabLayout.setDrawBitMap(bitmap, 126);
                mTabLayout.setViewPager(mViewPager);

                mViewPager.setCurrentItem(0);
                //默认加载第一页数据
                if(isFirst==0){
                    isFirst=2;
                    EventBus.getDefault().post(new TabChangeBean(0, -1));

                }

                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                    if (i != 0) {
                        ImageView titleIv = mTabLayout.getTitleIv(i);
                        titleIv.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


        mTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                    DESC = 2;
                    if(position==0){
                        DESC=-1;
                    }
                    if (i == position) {
                        Glide.with(MallMoreActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(i));
//                        Log.e("position==", "tab+"+position );
                        EventBus.getDefault().post(new TabChangeBean(position, DESC));
                    } else {
                        Glide.with(MallMoreActivity.this).load(R.mipmap.options_group_nom).into(mTabLayout.getTitleIv(i));
                    }
                }

            }
            @Override
            public void onTabReselect(int position) {
                if (0 == position) {
                    return;
                }
                if (DESC == 1) {
                    DESC = 2;
                    Glide.with(MallMoreActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(position));
                } else if (DESC == 2) {
                    DESC = 1;
                    Glide.with(MallMoreActivity.this).load(R.mipmap.options_group_decline).into(mTabLayout.getTitleIv(position));
                }
//                Log.e("tab_position==", position + "  " + DESC);
                EventBus.getDefault().post(new TabChangeBean(position, DESC));
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                    DESC = 2;
                    if(position==0){
                        DESC=-1;
                    }
                    if (i == position) {
                        Glide.with(MallMoreActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(i));
                    } else {
                        Glide.with(MallMoreActivity.this).load(R.mipmap.options_group_nom).into(mTabLayout.getTitleIv(i));
                    }
                }
//                Log.e("position==", "pager+"+position );
                EventBus.getDefault().post(new TabChangeBean(position, DESC));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void initEvent() {

    }


    @OnClick({R.id.iv_back, R.id.img_car})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.img_car:
                ActivityUtils.next(MallMoreActivity.this, ShoppingCartActivity.class, false);
                break;
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }

    @Override
    public void onSuccess(MallMoreBean bean) {

    }

    @Override
    public void onSearchPriceSuccess(SearchPriceBean bean) {

    }

    @Override
    public void onGoodsSuccess(GoodsDetailBean bean) {

    }

    @Override
    public void onAddCarSuccess(String bean) {

    }

    @Override
    public void onAddOrderSuccess(GoAddOrderBean bean) {

    }

    @Override
    public void onCarSizeSuccess(CarListBean bean) {
        if(bean.cartList.size()==0){
            Glide.with(MallMoreActivity.this).load(R.mipmap.mall_car_nom).into(mImgCar);
        }else {
            Glide.with(MallMoreActivity.this).load(R.mipmap.mall_car_have_data).into(mImgCar);
        }
    }

    @Override
    public void onFailure() {

    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
