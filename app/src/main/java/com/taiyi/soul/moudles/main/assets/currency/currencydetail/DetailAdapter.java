package com.taiyi.soul.moudles.main.assets.currency.currencydetail;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.mine.bean.TestBean;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class DetailAdapter extends BaseQuickAdapter<TestBean, BaseViewHolder> {
    public DetailAdapter(List<TestBean> data) {
        super(R.layout.assets_currency_details_item_layout, data);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder holder, TestBean item) {

    }
}
