package com.taiyi.soul.moudles.mine.setup;

import android.content.Intent;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.mine.bean.DataBean;
import com.taiyi.soul.moudles.mine.setup.currencydisplay.CurrencyDisplayActivity;
import com.taiyi.soul.moudles.mine.setup.signout.SignOutActivity;
import com.taiyi.soul.moudles.mine.setup.switchlan.LanguageActivity;
import com.taiyi.soul.moudles.mine.setup.update.NewVersionUpdateActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SetUpActivity extends BaseActivity<NormalView, NormalPresenter> implements MultiItemTypeAdapter.OnItemClickListener {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.logoIv)
    ImageView logoIv;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.top)
    ConstraintLayout top;
    private List<DataBean> dataBeanList = new ArrayList<>();

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_set_up;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.set_up));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        dataBeanList.add(new DataBean(R.mipmap.mine_set_up_update_lan_iv, getString(R.string.language_switch)));
        dataBeanList.add(new DataBean(R.mipmap.mine_set_up_currency_display_iv, getString(R.string.currency_display)));
//        dataBeanList.add(new DataBean(R.mipmap.mine_set_up_theme_switch_iv, getString(R.string.theme_switch)));
        dataBeanList.add(new DataBean(R.mipmap.mine_set_up_new_version_update_iv, getString(R.string.new_version_update)));
        dataBeanList.add(new DataBean(R.mipmap.mine_set_up_sign_out_iv, getString(R.string.sign_out)));
        CommonAdapter setupListAdapter = AdapterManger.getSetupListAdapter(this, dataBeanList);
        recyclerView.setAdapter(setupListAdapter);
        setupListAdapter.setOnItemClickListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }


    @OnClick(R.id.backIv)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
//        Log.e("huyang--","点击设置方法---"+position);
        if (position == 0) {
            startActivity(new Intent(this, LanguageActivity.class));
//            ActivityUtils.next(this, LanguageActivity.class);
        } else if (position == 1) {
            startActivity(new Intent(this, CurrencyDisplayActivity.class));
//            ActivityUtils.next(this, CurrencyDisplayActivity.class);
        } else if (position == 2) {
            startActivity(new Intent(this, NewVersionUpdateActivity.class));
//            ActivityUtils.next(this, NewVersionUpdateActivity.class);
        } else if (position == 3) {
            startActivity(new Intent(this, SignOutActivity.class));
//            ActivityUtils.next(this, SignOutActivity.class);
        }
    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }
}
