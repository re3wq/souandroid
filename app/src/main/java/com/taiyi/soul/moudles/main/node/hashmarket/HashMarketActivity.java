package com.taiyi.soul.moudles.main.node.hashmarket;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import android.view.View;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.node.hashmarket.fragment.HashMarketPayFragment;
import com.taiyi.soul.moudles.main.node.hashmarket.fragment.HashMarketSellFragment;
import com.taiyi.soul.moudles.main.node.hashmarket.transactionrecord.TransactionRecordActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import org.greenrobot.eventbus.EventBus;

import java.math.BigDecimal;
import java.util.ArrayList;

import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.OnClick;
/**
 * Created by Android Studio.
 * @Date ： 2020\7\8 0008 11:38
 * @Author : yuan
 * @Describe ：算力市场-发布委托
 */
public class HashMarketActivity extends BaseActivity<NormalView, NormalPresenter> {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.rightTitleTv)
    TextView rightTitleTv;
    @BindView(R.id.top)
    ConstraintLayout top;
//    @BindView(R.id.payTv)
//    TextView payTv;
//    @BindView(R.id.sellOrdersTv)
//    TextView sellOrdersTv;
//    @BindView(R.id.recyclerView)
//    RecyclerView recyclerView;
//    private List<HashMarketBean>data=new ArrayList<>();



    @BindView(R.id.deal_tab)
    SlidingTabLayout mDealTab;
    @BindView(R.id.deal_viewpager)
    ViewPager mDealViewpager;


    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private String[] mTitles;
    private float mTabWidth;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_hash_market;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.computing_power_market));
        rightTitleTv.setText(getString(R.string.transaction_record));
//        payTv.setSelected(true);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        for (int i = 0; i < 10; i++) {
//            data.add(new HashMarketBean());
//        }
//        CommonAdapter hashMarketListAdapter = AdapterManger.getHashMarketListAdapter(this, data);
//        recyclerView.setAdapter(hashMarketListAdapter);

        mTitles = new String[]{getResources().getString(R.string.deal_pay),
                getResources().getString(R.string.deal_sell)};

        mFragments.add(new HashMarketPayFragment());//买单
        mFragments.add(new HashMarketSellFragment());//卖单

        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mDealViewpager.setAdapter(mAdapter);



        mTabWidth = mDealTab.getWidth();
        BigDecimal b1 = new BigDecimal(Double.toString(mTabWidth));
        BigDecimal b2 = new BigDecimal(Double.toString(2));
        int width = b1.divide(b2, 0, BigDecimal.ROUND_HALF_UP).intValue();
        mDealTab.setIsBackGround(true);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.node_hash_market_sel_iv);
        mDealTab.setDrawBitMap(bitmap, 126);
        mDealTab.setViewPager(mDealViewpager);
        mDealViewpager.setCurrentItem(0);

        mDealTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mAdapter.notifyDataSetChanged();
                EventBus.getDefault().post("refresh_anim");
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.rightTitleTv,R.id.releaseTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightTitleTv:
                ActivityUtils.next(this, TransactionRecordActivity.class);
                break;
            case R.id.releaseTv:
//                Bundle bundle = new Bundle();
//                bundle.putInt("hash_market_type",mDealViewpager.getCurrentItem());
//                ActivityUtils.next(this,ReleaseOrderActivity.class,bundle);
                break;
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
