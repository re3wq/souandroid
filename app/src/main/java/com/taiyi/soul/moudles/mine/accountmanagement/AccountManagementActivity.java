package com.taiyi.soul.moudles.mine.accountmanagement;

import android.app.Dialog;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.gen.WalletBeanDao;
import com.taiyi.soul.moudles.mine.accountmanagement.create.CreateAccountActivity;
import com.taiyi.soul.moudles.mine.accountmanagement.detail.AccountDetailActivity;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.bean.AccountListBean;
import com.taiyi.soul.moudles.mine.bean.SwitchAccountBean;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.CopyUtils;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.mytreeview.adapter.SimpleTreeListViewAdapter;
import com.taiyi.soul.view.mytreeview.bean.FileBean;
import com.taiyi.soul.view.mytreeview.node.Node;
import com.taiyi.soul.view.mytreeview.utils.TreeListViewAdapter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class AccountManagementActivity extends BaseActivity<AccountManagementView, AccountManagementPresent> implements View.OnClickListener, AccountManagementView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.rightTitleTv)
    TextView rightTitleTv;
    @BindView(R.id.rightIv)
    ImageView rightIv;
    @BindView(R.id.lv)
    ListView lv;

    @BindView(R.id.top)
    ConstraintLayout top;

    Dialog accountManagementDialog,noKey;
    private List<FileBean> mFileListLast = new ArrayList<>();
    private SimpleTreeListViewAdapter<FileBean> mAdapter;
    private String username;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_account_management;
    }

    @Override
    public AccountManagementPresent initPresenter() {
        return new AccountManagementPresent();
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.account_management));
        rightIv.setVisibility(View.VISIBLE);
        rightIv.setImageResource(R.mipmap.mine_account_management_add_iv);
        accountManagementDialog = new Dialog(this, R.style.MyDialog);
        View view = LayoutInflater.from(this).inflate(R.layout.mine_account_management_dialog_layout, null);
        view.findViewById(R.id.createAccountTv).setOnClickListener(this);
        view.findViewById(R.id.importAccountTv).setOnClickListener(this);
        view.findViewById(R.id.cancelTv).setOnClickListener(this);
        accountManagementDialog.setContentView(view);
        WindowManager.LayoutParams attributes = accountManagementDialog.getWindow().getAttributes();
        attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
        attributes.height = WindowManager.LayoutParams.WRAP_CONTENT;
        attributes.gravity = Gravity.BOTTOM;
        accountManagementDialog.getWindow().setAttributes(attributes);
        accountManagementDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFileListLast.clear();
        showProgress();
        presenter.getAccountList(AccountManagementActivity.this);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {
        initAdapter();
    }

    private void initAdapter() {


        try {
            String mainAccount = Utils.getSpUtils().getString("mainAccount");
            mAdapter = new SimpleTreeListViewAdapter<>(this, mainAccount, "1", lv, mFileListLast, 0);
            lv.setAdapter(mAdapter);
            mAdapter.setOnTreeNodeClickListener(new TreeListViewAdapter.OnTreeNodeClickListener() {
                @Override
                public void onNodeClick(Node node, int position) {
//                    List<Node> selectList = mAdapter.getSelectList();
//                        toast(selectList.get(position).getUsername());
//
//                    if (selectList.get(position).getIsPrivateKey().equals("0")) {
//                        toast("暂无私钥，请导入该账户私钥");
//                        return;
//                    }
//                    username = selectList.get(position).getUsername();
//                    showProgress();
//                    presenter.switchAccount(AccountManagementActivity.this,selectList.get(position).getUserid());
                    List<Node> selectList = mAdapter.getSelectList();
//                    toast(selectList.get(position).getUsername());

                    if (selectList.get(position).getIsPrivateKey().equals("0")) {
//                        toast(getString(R.string.no_private_key2));
//                        AlertDialogShowUtil.toastMessage(AccountManagementActivity.this,getString(R.string.no_private_key2));

                        noKey = new Dialog(AccountManagementActivity.this, R.style.MyDialog);
                        View view = LayoutInflater.from(AccountManagementActivity.this).inflate(R.layout.mine_no_key_dialog_layout, null);
                        noKey.setContentView(view);
                        WindowManager.LayoutParams attributes = accountManagementDialog.getWindow().getAttributes();
                        attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
                        attributes.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        attributes.gravity = Gravity.BOTTOM;
                        noKey.getWindow().setAttributes(attributes);
                        noKey.setCanceledOnTouchOutside(true);
                        noKey.show();

                        return;
                    }
//                    FileBean fileBean = mFileListLast.get(position);
                    Node node_ = selectList.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("accountName", node_.getUsername());
                    bundle.putString("inviteCode", node_.getUnion_id());
                    bundle.putString("createTime", node_.getCreatetime());
                    ActivityUtils.next(AccountManagementActivity.this, AccountDetailActivity.class, bundle);

                }
            });
            mAdapter.setSwitchClickListener(new SimpleTreeListViewAdapter.SwitchClickListener() {
                @Override
                public void switchAccount(int position) {
                    List<Node> selectList = mAdapter.getSelectList();
//                    toast(selectList.get(position).getUsername());
                    if (selectList.get(position).getIsPrivateKey().equals("0")) {
//                        toast(getString(R.string.no_private_key2));
//                        AlertDialogShowUtil.toastMessage(AccountManagementActivity.this,getString(R.string.no_private_key2));
                        noKey = new Dialog(AccountManagementActivity.this, R.style.MyDialog);
                        View view = LayoutInflater.from(AccountManagementActivity.this).inflate(R.layout.mine_no_key_dialog_layout, null);
                        noKey.setContentView(view);
                        WindowManager.LayoutParams attributes = accountManagementDialog.getWindow().getAttributes();
                        attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
                        attributes.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        attributes.gravity = Gravity.BOTTOM;
                        noKey.getWindow().setAttributes(attributes);
                        noKey.setCanceledOnTouchOutside(true);
                        noKey.show();
                        return;
                    }
                    username = selectList.get(position).getUsername();
                    showProgress();
                    presenter.switchAccount(AccountManagementActivity.this,selectList.get(position).getUserid());
                }
            });
            mAdapter.setOnCopyClickListener(new SimpleTreeListViewAdapter.OnCopyClickListener() {
                @Override
                public void onCopyClick(String value) {
                    CopyUtils.CopyToClipboard(AccountManagementActivity.this, value);
//                    toast(getString(R.string.copy_success));
                    AlertDialogShowUtil.toastMessage(AccountManagementActivity.this,getString(R.string.copy_success));

                }
            });
            mAdapter.setOnNameClickListener(new SimpleTreeListViewAdapter.OnNameClickListener() {
                @Override
                public void onNameClick(int position) {
                    List<Node> selectList = mAdapter.getSelectList();
//                    toast(selectList.get(position).getUsername());

                    if (selectList.get(position).getIsPrivateKey().equals("0")) {
//                        toast(getString(R.string.no_private_key2));
//                        AlertDialogShowUtil.toastMessage(AccountManagementActivity.this,getString(R.string.no_private_key2));
                        noKey = new Dialog(AccountManagementActivity.this, R.style.MyDialog);
                        View view = LayoutInflater.from(AccountManagementActivity.this).inflate(R.layout.mine_no_key_dialog_layout, null);
                        noKey.setContentView(view);
                        WindowManager.LayoutParams attributes = accountManagementDialog.getWindow().getAttributes();
                        attributes.width = WindowManager.LayoutParams.MATCH_PARENT;
                        attributes.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        attributes.gravity = Gravity.BOTTOM;
                        noKey.getWindow().setAttributes(attributes);
                        noKey.setCanceledOnTouchOutside(true);
                        noKey.show();
                        return;
                    }
//                    FileBean fileBean = mFileListLast.get(position);
                    Node node = selectList.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("accountName", node.getUsername());
                    bundle.putString("inviteCode", node.getUnion_id());
                    bundle.putString("createTime", node.getCreatetime());
                    ActivityUtils.next(AccountManagementActivity.this, AccountDetailActivity.class, bundle);
                }

                @Override
                public void onNameLongClick(int position) {
                    List<Node> selectList = mAdapter.getSelectList();
                    CopyUtils.CopyToClipboard(AccountManagementActivity.this, selectList.get(position).getUsername());
//                    toast(getString(R.string.copy_success));
                    AlertDialogShowUtil.toastMessage(AccountManagementActivity.this,getString(R.string.copy_success));

                }
            });
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.backIv, R.id.rightIv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightIv:
                accountManagementDialog.show();
                break;
        }
    }

//    private void initList(List<FileBean> list) {
//        if (list == null || list.size() <= 0) return;
//        for (FileBean item : list) {
//            mFileListLast.add(item);
//            if (item.getXiaList() != null && item.getXiaList().size() > 0) {
//                initList(item.getXiaList());
//            }
//        }
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createAccountTv:
                accountManagementDialog.dismiss();
                ActivityUtils.next(this, CreateAccountActivity.class);
                break;
            case R.id.importAccountTv:
                accountManagementDialog.dismiss();
                Bundle bundle = new Bundle();
                bundle.putSerializable("accounts", (Serializable) mFileListLast);
                bundle.putInt("isHaveKey",1);
                ActivityUtils.next(this, ImportAccountActivity.class, bundle);
                break;
            case R.id.cancelTv:
                accountManagementDialog.dismiss();
                break;
        }
    }

    @Override
    public void getAccountListSuccess(List<AccountListBean> list) {
        hideProgress();
        mFileListLast.clear();
        for (int i = 0; i < list.size(); i++) {
            FileBean fileBean = new FileBean(list.get(i).getId() + "", 0 + "", list.get(i).getAccount(), list.get(i).getInvitationCode(), list.get(i).getCreateTime());
            String account = list.get(i).getAccount();
            ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
            if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
                accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
            }
            int j = 0;
            for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
                if (accountInfoBean.getAccount_name().equals(account)) {
                    j += 1;
                }
            }
            if (j == 0) {
                fileBean.setIsPrivateKey("0");//代表没有私钥
            } else {
                fileBean.setIsPrivateKey("1");
            }
            mFileListLast.add(fileBean);
        }

//        ListSort(mFileListLast);
        try {
            mAdapter.refresh(mFileListLast, 0);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    private static void ListSort(List<FileBean> list) {
        Collections.sort(list, new Comparator<FileBean>() {
            @Override
            public int compare(FileBean o1, FileBean o2) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dt1 = format.parse(o1.getCreatetime());
                    Date dt2 = format.parse(o2.getCreatetime());
                    if (dt1.getTime() > dt2.getTime()) {
                        return 1;
                    } else if (dt1.getTime() < dt2.getTime()) {
                        return -1;
                    } else {
                        return 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
    }

    @Override
    public void getAccountListFailure(String errorMsg) {
        hideProgress();
//        toast(errorMsg);
        AlertDialogShowUtil.toastMessage(AccountManagementActivity.this,errorMsg);

    }

    @Override
    public void switchSuccess(SwitchAccountBean emptyBean) {
        hideProgress();
        Utils.getSpUtils().put(Constants.TOKEN, emptyBean.getToken());
        Utils.getSpUtils().put("mainAccount", username);
//        Log.i("mainAccount","-----"+username);
        /**
         * 0---手机号登录
         * 1---邮箱登录
         */
        int loginType = Utils.getSpUtils().getInt("loginType");
        String phoneNumber = Utils.getSpUtils().getString("phoneNumber");
        String email = Utils.getSpUtils().getString("email");

        WalletBean walletBean = null;
        if (loginType == 0) {
            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(phoneNumber)).build().list();
            if (null!=list) {
                if(list.size()>0)
                walletBean = list.get(list.size()-1);
            }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(phoneNumber)).build().unique();
        } else {
            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(email)).build().list();
            if (null!=list) {
                if(list.size()>0)
                walletBean = list.get(list.size()-1);
            }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(email)).build().unique();
        }
        walletBean.setWallet_main_account(username);
        MyApplication.getInstance().setWalletBean(walletBean);
        MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
        finish();
    }
}
