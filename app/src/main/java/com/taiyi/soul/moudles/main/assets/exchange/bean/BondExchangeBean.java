package com.taiyi.soul.moudles.main.assets.exchange.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\10 0010 15:04
 * @Author : yuan
 * @Describe ：
 */
public class BondExchangeBean {


        //余额为账户余额，点击刷新按钮刷新该接口所有数据
        public String usdn_exchange_rate;
        public String createtime;
        public String surplus_num;//可兑换债券剩余数量（type==0取）
        public String type;//0可点击兑换，1不可点击
        public String bond_num;//现有债券
        public String reward_num;//分配奖励
        public String alluser_num;//剩余可赎回
        public String stock_right_num;
        public String stock_right_num_surplus  ;//股权指数
        public String bond_num_redeem;//赎回债券
        public String num;//可兑换债券剩余数量（type==1取）
        public String bond_usdn_price;//兑换规律
        public String nexttime;//下次计价时间
        public String endtime;
        public String starttime;
        public String userid;
        public String number;
        public String mix_num;
        public String langu;
        public String nowtime;
        public int id;
        public String max_num;
        public String usdn_rate;

}
