package com.taiyi.soul.moudles.main.node.hashmarket.release;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.node.hashmarket.fragment.HashReleasePayFragment;
import com.taiyi.soul.moudles.main.node.hashmarket.fragment.HashReleaseSellFragment;
import com.taiyi.soul.moudles.main.node.hashmarket.transactionrecord.TransactionRecordActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.OnClick;
/**
 * Created by Android Studio.
 * @Date ： 2020\7\8 0008 11:38
 * @Author : yuan
 * @Describe ：算力市场-发布委托
 */
public class ReleaseOrderActivity extends BaseActivity<NormalView, NormalPresenter> {
    @BindView(R.id.titleTv)
    TextView titleTv;
//    @BindView(R.id.payTv)
//    TextView payTv;
//    @BindView(R.id.sellOrdersTv)
//    TextView sellOrdersTv;
//    @BindView(R.id.payLl)
//    LinearLayout payLl;
//    @BindView(R.id.sellLl)
//    LinearLayout sellLl;
    @BindView(R.id.top)
    ConstraintLayout top;
    private int mHash_market_type;

    @BindView(R.id.deal_tab)
    SlidingTabLayout mDealTab;
    @BindView(R.id.deal_viewpager)
    ViewPager mDealViewpager;

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private String[] mTitles;
    private float mTabWidth;
    @Override
    protected int getLayoutId() {
//        return R.layout.activity_release_order;
        return R.layout.activity_release_order_new;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }


    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        mHash_market_type = getIntent().getExtras().getInt("hash_market_type");
        titleTv.setText(getString(R.string.release_order));
//        payTv.setSelected(true);

        mTitles = new String[]{getResources().getString(R.string.deal_pay),
                getResources().getString(R.string.deal_sell)};

        mFragments.add(new HashReleasePayFragment());//买单
        mFragments.add(new HashReleaseSellFragment());//卖单

        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mDealViewpager.setAdapter(mAdapter);



//        mTabWidth = mDealTab.getWidth();
//        BigDecimal b1 = new BigDecimal(Double.toString(mTabWidth));
//        BigDecimal b2 = new BigDecimal(Double.toString(2));
//        int width = b1.divide(b2, 0, BigDecimal.ROUND_HALF_UP).intValue();
        mDealTab.setIsBackGround(true);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.node_hash_market_sel_iv);
        mDealTab.setDrawBitMap(bitmap, 126);
        mDealTab.setViewPager(mDealViewpager);
        mDealViewpager.setCurrentItem(mHash_market_type);

        mDealTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mAdapter.notifyDataSetChanged();
                EventBus.getDefault().post("refresh_anim");
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    private ArrayList<MyOnTouchListener> onTouchListeners = new ArrayList<MyOnTouchListener>(
            10);
    public interface MyOnTouchListener {
        public boolean onTouch(MotionEvent ev, View currentFocus);
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        for (MyOnTouchListener listener : onTouchListeners) {
            if(listener != null) {
                listener.onTouch(ev,getCurrentFocus());
            }
        }
        return super.dispatchTouchEvent(ev);
    }
    public void registerMyOnTouchListener(MyOnTouchListener myOnTouchListener) {
        onTouchListeners.add(myOnTouchListener);
    }
    public void unregisterMyOnTouchListener(MyOnTouchListener myOnTouchListener) {
        onTouchListeners.remove(myOnTouchListener) ;
    }
    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.rightTitleTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightTitleTv:
                ActivityUtils.next(this, TransactionRecordActivity.class);
                break;
//            case R.id.payTv:
//                payTv.setSelected(true);
//                sellOrdersTv.setSelected(false);
//                sellLl.setVisibility(View.GONE);
//                payLl.setVisibility(View.VISIBLE);
//                break;
//            case R.id.sellOrdersTv:
//                payTv.setSelected(false);
//                sellOrdersTv.setSelected(true);
//                payLl.setVisibility(View.GONE);
//                sellLl.setVisibility(View.VISIBLE);
//                break;
//            case R.id.releaseTv:
//                ActivityUtils.next(this, ReleaseOrderActivity.class);
//                break;
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
