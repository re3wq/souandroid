package com.taiyi.soul.moudles.mine.accountmanagement;

import com.taiyi.soul.moudles.mine.bean.AccountListBean;
import com.taiyi.soul.moudles.mine.bean.SwitchAccountBean;

import java.util.List;

public interface AccountManagementView {
    void getAccountListSuccess(List<AccountListBean> list);
    void getAccountListFailure(String errorMsg);
    void switchSuccess(SwitchAccountBean emptyBean);
}
