package com.taiyi.soul.moudles.register;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.moudles.login.bean.AreaCodeBean;
import com.taiyi.soul.moudles.mine.accountmanagement.create.CreateAccountActivity;
import com.taiyi.soul.moudles.mine.webview.WebViewActivity;
import com.taiyi.soul.moudles.register.bean.RegisterBean;
import com.taiyi.soul.moudles.scancode.ScanCodeActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.EncryptUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.AreaCodePopupWindow;
import com.taiyi.soul.view.countdowntimer.CountDownTimerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;
import me.ljp.permission.PermissionItem;

public class RegisterActivity extends BaseActivity<RegisterView, RegisterPresent> implements AreaCodePopupWindow.OnItemClick, RegisterView {

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.tv_code)
    TextView tvCode;

    @BindView(R.id.iv)
    ImageView iv;

    @BindView(R.id.ll_code)
    LinearLayout llCode;

    @BindView(R.id.iv_email)
    ImageView ivEmail;

    @BindView(R.id.iv_phone)
    ImageView ivPhone;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_login_password)
    EditText et_login_password;

    @BindView(R.id.et_tx_password)
    EditText et_tx_password;

    @BindView(R.id.et_sms_code)
    EditText et_sms_code;

    @BindView(R.id.inputInviteCodeEt)
    EditText inputInviteCodeEt;

    @BindView(R.id.tv_send_code)
    TextView tv_send_code;

    @BindView(R.id.invitePeopleEt)
    TextView invitePeopleTv;

    @BindView(R.id.top)
    RelativeLayout top;

    @BindView(R.id.ll_phone)
    LinearLayout llPhone;


    @BindView(R.id.tv_agreement)
    TextView tv_agreement;

    @BindView(R.id.iv_agreement)
    CheckBox iv_agreement;
    public static final int REQUEST_CODE = 0x1001;
    /**
     * 0---手机号注册
     * 1---邮箱注册
     */
    private int loginType = 0;


    private ArrayList<AreaCodeBean> list = new ArrayList<>();
    private String inviteCode;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        initView();
//        list.add(new AreaCodeBean(R.mipmap.language_china_iv, "+86"));
//        list.add(new AreaCodeBean(R.mipmap.language_french_iv, "+33"));
//        list.add(new AreaCodeBean(R.mipmap.language_german_iv, "+94"));
//        list.add(new AreaCodeBean(R.mipmap.language_spanish_iv, "+34"));
//        list.add(new AreaCodeBean(R.mipmap.language_portuguese_iv, "+351"));
//        list.add(new AreaCodeBean(R.mipmap.language_russian_iv, "+7"));
//        list.add(new AreaCodeBean(R.mipmap.language_arabic_iv, "+966"));
//        list.add(new AreaCodeBean(R.mipmap.language_japanese_iv, "+81"));
//        list.add(new AreaCodeBean(R.mipmap.language_korean_iv, "+82"));
//        list.add(new AreaCodeBean(R.mipmap.language_english_iv, "+1"));
//        tvCode.setText(list.get(0).getCode());
//        Glide.with(RegisterActivity.this).load(list.get(0).getIconD()).into(iv);
    }

    @Override
    protected void initData() {
        presenter.getAreaCode();
    }

    @Override
    public void initEvent() {
        inputInviteCodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 6) {
                    showProgress();
                    presenter.getInvitePerson(s.toString());
                }
            }
        });
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 11) {
//                    showProgress();
//                    presenter.verificationMainAccountOnly(s.toString());
                }
            }
        });

        et_login_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String loginPwd = et_login_password.getText().toString();
                    if (!TextUtils.isEmpty(loginPwd)) {
                        if (loginPwd.length() < 8) {//位数不足八位
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint1));
//                            toast("登录密码不足8位");
                        } else if (loginPwd.matches("^[0-9]+$")) {//输入的纯数字为弱
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                        } else if (loginPwd.matches("^[a-z]+$")) {//输入的纯小写字母为弱
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                        } else if (loginPwd.matches("^[A-Z]+$")) {//输入的纯大写字母为弱
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                        } else if (loginPwd.matches("^[A-Za-z]+$")) {//输入的大写字母和小写字母为中
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                        } else if (loginPwd.matches("^[a-z0-9]+$")) {//输入的小写字母和数字为中
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                        } else if (loginPwd.matches("^[A-Z0-9]+$")) {//输入的大写字母和数字为中
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                        } else if (loginPwd.matches("^[A-Za-z0-9]+$")) {//输入的大写字母和小写字母和数字为强
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint4));
//                            toast("密码安全系数为高级");
                        } else {//其他视为高强度
                            AlertDialogShowUtil.toastMessage(RegisterActivity.this, getResources().getString(R.string.password_hint4));
//                            toast("密码安全系数为高级");
                        }

                    }
                }
            }
        });
    }


    @Override
    public RegisterPresent initPresenter() {
        return new RegisterPresent();
    }


    @OnClick({R.id.iv_back,R.id.tv_code, R.id.ll_code, R.id.ll_email, R.id.ll_phone1, R.id.tv_send_code, R.id.nextStepTv, R.id.iv_agreement, R.id.tv_agreement, R.id.scanIv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_code:
            case R.id.ll_code:
                AreaCodePopupWindow popupWindow = new AreaCodePopupWindow(this, list, this);
                popupWindow.showAsDropDown(llCode, 0, DensityUtil.dip2px(this, -5));

                break;

            case R.id.ll_email:
                etEmail.setText("");
                et_sms_code.setText("");
                et_login_password.setText("");
                et_tx_password.setText("");
                inputInviteCodeEt.setText("");
                invitePeopleTv.setText("");
                loginType = 1;
                initView();
                break;

            case R.id.ll_phone1:
                etPhone.setText("");
                et_sms_code.setText("");
                et_login_password.setText("");
                et_tx_password.setText("");
                inputInviteCodeEt.setText("");
                invitePeopleTv.setText("");
                loginType = 0;
                initView();
                break;
            case R.id.tv_send_code:
                if (loginType == 1) {
                    String email = etEmail.getText().toString();
                    if (!TextUtils.isEmpty(email)) {
//                        if (!email.contains("@")) {
//                            toast("邮箱格式错误");
//                            return;
//                        }
                        if (emailFormat(email)) {
                            showProgress();
                            presenter.getVerificationCode("", email);
                        } else {
                            toast(getResources().getString(R.string.email_err));
                        }
                    } else {
                        toast(getResources().getString(R.string.email_input));
                    }
                } else {//手机号
                    String phoneNumber = etPhone.getText().toString();
                    if (!TextUtils.isEmpty(phoneNumber)) {
                        showProgress();
                        presenter.getVerificationCode(tvCode.getText().toString(), phoneNumber);
                    }
                }
                break;
            case R.id.nextStepTv:
                String email = etEmail.getText().toString();
                String phoneNumber = etPhone.getText().toString();
                String smsCode = et_sms_code.getText().toString();
                String loginPwd = et_login_password.getText().toString();
                String payPwd = et_tx_password.getText().toString();
                inviteCode = inputInviteCodeEt.getText().toString();

                if (loginType == 1) {//邮箱
                    if (TextUtils.isEmpty(email)) {
                        toast(getResources().getString(R.string.email_input));
                        return;
                    }
                    if (!email.contains("@")) {
                        toast(getResources().getString(R.string.email_err));
                        return;
                    }
                } else {//手机号

                    if (TextUtils.isEmpty(phoneNumber)) {
                        toast(getResources().getString(R.string.phone_input));
                        return;
                    }
                }


                if (TextUtils.isEmpty(smsCode)) {
                    toast(getResources().getString(R.string.code_input));
                    return;
                }

                if (TextUtils.isEmpty(loginPwd)) {
                    toast(getResources().getString(R.string.login_password_input));
                    return;
                }
                if (loginPwd.length() < 8) {
                    toast(getResources().getString(R.string.password_hint1));
                    return;
                }

                if (TextUtils.isEmpty(payPwd)) {
                    toast(getResources().getString(R.string.pay_password_input));
                    return;
                }
                if (payPwd.length() != 6) {
                    toast(getResources().getString(R.string.language_simple_err));
                    return;
                }

                if (TextUtils.isEmpty(inviteCode)) {
                    toast(getResources().getString(R.string.invite_code_input));
                    return;
                }
                if (!iv_agreement.isChecked()) {
                    toast(getResources().getString(R.string.protocol_select));
                    return;
                }
                Utils.getSpUtils().put("phone_country", tvCode.getText().toString());
                if (loginType == 1) {
                    showProgress();
                    presenter.verificationMainAccountOnly(email);
                } else {
                    showProgress();
                    presenter.verificationMainAccountOnly(phoneNumber);
                }


                break;

            case R.id.iv_agreement:

                break;

            case R.id.tv_agreement:
                Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("type", 4);
                startActivity(intent);
                break;

            case R.id.scanIv:
                List<PermissionItem> permissonItems = new ArrayList<PermissionItem>();
                permissonItems.add(new PermissionItem(Manifest.permission.CAMERA, getString(R.string.camer), R.drawable.permission_ic_camera));
                if (Utils.getPermissions(permissonItems, getString(R.string.open_camer_scan))) {
                    Bundle bundle = new Bundle();
                    bundle.putString("from", "transfer");
                    ActivityUtils.next(this, ScanCodeActivity.class, bundle, REQUEST_CODE);
                }
                break;
        }
    }

    private boolean emailFormat(String email) {//邮箱判断正则表达式
        Pattern pattern = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
        Matcher mc = pattern.matcher(email);
        return mc.matches();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (data != null)
                if (null != data.getExtras()) {

                    String strCode = data.getExtras().getString("id");
                    if (strCode.contains("invitationCode")) {
                        if(strCode.contains("=")){
                            String[] split = strCode.split("=");
                            if(split.length==1){
//                                AlertDialogShowUtil.toastMessage2(RegisterActivity.this,getString(R.string.invite_filed_hint));
                                ToastUtils.showShortToast(getString(R.string.invite_filed_hint));
                            }else {
                                inputInviteCodeEt.setText(split[1]);
                            }

                        }
//                        String substring = strCode.substring(strCode.length() - 6, strCode.length());
//                        inputInviteCodeEt.setText(substring);
//                        Log.i("dsadadas", "-------" + strCode);
                    } else {
                        AlertDialogShowUtil.toastMessage(this, getResources().getString(R.string.connect_err));
                    }

                }
        }
    }

    @Override
    public void onItemClick(int position) {
        tvCode.setText(list.get(position).getCode());
        Glide.with(RegisterActivity.this).load(list.get(position).getIcon()).into(iv);
    }

    @Override
    public void onHide(boolean isHide) {
//        if (isHide){
//            llHide.setVisibility(View.GONE);
//        }else {
//            llHide.setVisibility(View.VISIBLE);
//
//        }
    }


    private void initView() {
        if (loginType == 0) {
            llPhone.setVisibility(View.VISIBLE);
            etEmail.setVisibility(View.GONE);
            ivEmail.setImageResource(R.mipmap.iv_login_email);
            ivPhone.setImageResource(R.mipmap.iv_login_phone);
        } else {
            llPhone.setVisibility(View.GONE);
            etEmail.setVisibility(View.VISIBLE);
            ivEmail.setImageResource(R.mipmap.iv_login_email_ed);
            ivPhone.setImageResource(R.mipmap.iv_login_phone_un);
        }
        iv_agreement.setChecked(true);
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public void getVerificationCodeSuccess() {
        hideProgress();
        CountDownTimerUtils countDownTimerUtils = new CountDownTimerUtils(this, tv_send_code, 60000, 1000, "#a5a9ac");
        countDownTimerUtils.start();

        tv_send_code.setFocusable(true);
        tv_send_code.setFocusableInTouchMode(true);
        tv_send_code.requestFocus();
    }

    @Override
    public void getVerificationCodeFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getInvitePersonSuccess(String invitePeople) {
        hideProgress();
        invitePeopleTv.setText(invitePeople);
    }

    @Override
    public void getInvitePersonFailure(String errorMsg) {
        hideProgress();
        invitePeopleTv.setText(errorMsg);
        toast(errorMsg);
    }

    @Override
    public void registerSuccess(RegisterBean msg) {
        hideProgress();
        Utils.getSpUtils().put(Constants.TOKEN, msg.getToken());
        String email = etEmail.getText().toString();
        String phoneNumber = etPhone.getText().toString();
        WalletBean walletBean = new WalletBean();
        String randomString = EncryptUtil.getRandomString(32);
        if (loginType == 1) {//邮箱
            walletBean.setWallet_email(email);
            Utils.getSpUtils().put("email", email);
        } else {
            walletBean.setWallet_phone(phoneNumber);
            Utils.getSpUtils().put("phoneNumber", phoneNumber);
        }
//        walletBean.setWallet_shapwd(PasswordToKeyUtils.shaEncrypt(randomString + et_tx_password.getText().toString().trim()));
        String loginPwd = et_login_password.getText().toString();
        Utils.getSpUtils().put("loginType", loginType);
        Utils.getSpUtils().put("loginPwd", loginPwd);
        MyApplication.getDaoSession().getWalletBeanDao().insert(walletBean);
        MyApplication.getInstance().setWalletBean(walletBean);

        Bundle bundle = new Bundle();
        bundle.putString("from", "register");
        bundle.putString("inviteCode", inviteCode);
//        bundle.putString("loginPwd", loginPwd);
//        bundle.putString("loginType", loginType+"");
//        bundle.putString("account",loginType==1?email:phoneNumber);
        ActivityUtils.next(this, CreateAccountActivity.class, bundle);
        finish();
    }

    @Override
    public void registerFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void mainAccountOnlyFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void mainAccountOnlySuccess() {
        hideProgress();

        String email = etEmail.getText().toString();
        String phoneNumber = etPhone.getText().toString();
        String smsCode = et_sms_code.getText().toString();
        String loginPwd = et_login_password.getText().toString();
        String payPwd = et_tx_password.getText().toString();
        inviteCode = inputInviteCodeEt.getText().toString();

        showProgress();
        presenter.register(loginType == 1 ? "":tvCode.getText().toString(), loginType == 1 ? email : (phoneNumber), loginPwd, payPwd, smsCode, inviteCode);

    }

    @Override
    public void getAreaCodeSuccess(List<AreaBean> mList) {
        for (AreaBean areaBean : mList) {
            list.add(new AreaCodeBean(areaBean.getUrl(), areaBean.getArea()));
        }
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getArea().equals("+1")) {
                tvCode.setText(list.get(i).getCode());
                Glide.with(this).load(list.get(i).getIcon()).into(iv);
            }
        }

    }
}
