package com.taiyi.soul.moudles.main.assets.currency.currencydetail;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.main.assets.bean.AssetsBean;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean;
import com.taiyi.soul.moudles.main.assets.collectmoney.CollectMoneyActivity;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.fragment.CurrencyDetailsOneFragment;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.fragment.CurrencyDetailsThreeFragment;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.fragment.CurrencyDetailsTwoFragment;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.present.CurrencyDetailsPresent;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.present.CurrencyDetailsView;
import com.taiyi.soul.moudles.main.assets.transfer.TransferActivity;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import androidx.viewpager.widget.ViewPager;

import butterknife.BindView;
import butterknife.OnClick;

public class CurrencyDetailsActivity extends BaseActivity<CurrencyDetailsView, CurrencyDetailsPresent> implements CurrencyDetailsView {
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.amountTv)
    TextView amountTv;
    @BindView(R.id.valueTv)
    TextView valueTv;
    @BindView(R.id.select_tv)
    TextView select_tv;
    @BindView(R.id.rightCurrencyTv)
    TextView rightCurrencyTv;
    @BindView(R.id.chartView)
    LineChart chartView;
    @BindView(R.id.allTv)
    TextView allTv;
    @BindView(R.id.collectMoneyTv)
    TextView collectMoneyTv;
    @BindView(R.id.transferTv)
    TextView transferTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.top)
    ConstraintLayout top;
    //    private CurrencyListBean currencyBean;
    private AssetsBean.PropertelistBean currencyBean;
    private ValueFormatter mFormatter;

    //@BindView(R.id.recyclerView)
//    RecyclerView recyclerView;

    @BindView(R.id.tab)
    SlidingTabLayout mTab;
    @BindView(R.id.viewpager)
    ViewPager mViewpager;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private String[] mTitles;
    private double mRate;
    private String mNgk_to_usdt;
    private String mUsdn_to_usdt;
    private String mBii;
    public static double hashprice=0;
    public void getHashPrice(){
        String url_hashprice="http://18.167.53.241:8088/hash.txt";
        HttpUtils.getRequets(url_hashprice, this, null, new JsonCallback<String>()  {
            @Override
            public String convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                super.onSuccess(response);
                try{ //response.body().get(0).split(" ")[0]
                    hashprice=Double.parseDouble(response.body().toString());
                }catch(Exception e){
                    hashprice=0;
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
            }
        });
    }
    @Override
    protected int getLayoutId() {
        return R.layout.activity_currency_details;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
//        mImmersionBar.titleBar(top).init();
    }

    @Override
    public CurrencyDetailsPresent initPresenter() {
        return new CurrencyDetailsPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        presenter.getCoinList();
        currencyBean = (AssetsBean.PropertelistBean) getIntent().getExtras().getSerializable("currencyBean");
        mBii = Utils.getSpUtils().getString("coin_symbol");
        titleTv.setText(currencyBean.getCoinName());


        if (currencyBean.getCoinName().equals("SOU") || currencyBean.getCoinName().equals("USDS")||currencyBean.getCoinName().equals("HASH")) {
//            amountTv.setText(currencyBean.getMoneyandfor());
            amountTv.setText(new BigDecimal(currencyBean.getMoneyandfor()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());


            String replace = currencyBean.getMfcon().replace(mBii, "");
            valueTv.setText("≈" + mBii + new BigDecimal(replace).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());

        } else {
            String quantity = "";
            if (currencyBean.getCoinName().equals("BTC")) {
                quantity = new BigDecimal(currencyBean.getMoney()).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
            } else if (currencyBean.getCoinName().equals("ETH")) {
                quantity = new BigDecimal(currencyBean.getMoney()).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
            } else {
                quantity = new BigDecimal(currencyBean.getMoney()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
            }
            quantity = NumUtils.subZeroAndDot(quantity);
            amountTv.setText(quantity);
//            valueTv.setText("≈" + currencyBean.getConvert());
            String replace = currencyBean.getConvert().replace(mBii, "");
            valueTv.setText("≈" + mBii + new BigDecimal(replace).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());

//            amountTv.setText(decimalFormat.format(Float.parseFloat(currencyBean.getMoney())));
//            valueTv.setText("≈" + currencyBean.getConvert());
        }

        allTv.setSelected(true);
        select_tv.setText(currencyBean.getCoinName());


        mTitles = new String[]{getResources().getString(R.string.exchanges_all), getResources().getString(R.string.collect_money),
                getResources().getString(R.string.transfer)};

        mFragments.add(new CurrencyDetailsOneFragment(currencyBean.getCoinName(), currencyBean.getUrl()));//全部
        mFragments.add(new CurrencyDetailsTwoFragment(currencyBean.getCoinName(), currencyBean.getUrl()));//收款
        mFragments.add(new CurrencyDetailsThreeFragment(currencyBean.getCoinName(), currencyBean.getUrl()));//转账

        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewpager.setAdapter(mAdapter);


        mTab.setIsBackGround(true);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.node_hash_market_sel_iv);
        mTab.setDrawBitMap(bitmap, 126);
        mTab.setViewPager(mViewpager);
        mViewpager.setCurrentItem(0);

        mTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getCoinDetails(this, currencyBean.getCoinName());
        if (null != currencyBean) {
//            showProgress();
            presenter.getCurrencyList(this);
            getHashPrice();
        }

    }

    @Override
    protected void initData() {

    }


    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.allTv, R.id.collectMoneyTv, R.id.transferTv, R.id.collectMoneyBtnTv, R.id.transferBtnTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.allTv:
                allTv.setSelected(true);
                collectMoneyTv.setSelected(false);
                transferTv.setSelected(false);
                break;
            case R.id.collectMoneyTv:
                collectMoneyTv.setSelected(true);
                allTv.setSelected(false);
                transferTv.setSelected(false);
                break;
            case R.id.transferTv:
                transferTv.setSelected(true);
                collectMoneyTv.setSelected(false);
                allTv.setSelected(false);
                break;
            case R.id.collectMoneyBtnTv:
                if (currencyBean.getCoinName().equals("BTC")) {
                    toast(getResources().getString(R.string.not_open));
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putString("fromCoinName", currencyBean.getCoinName());
                ActivityUtils.next(this, CollectMoneyActivity.class, bundle);
                break;
            case R.id.transferBtnTv:

                if (currencyBean.getCoinName().equals("BTC")) {
                    toast(getResources().getString(R.string.not_open));
                    return;
                }
                Bundle bundle_ = new Bundle();
                bundle_.putString("fromCoinName", currencyBean.getCoinName());
                ActivityUtils.next(this, TransferActivity.class, bundle_);
                break;
        }
    }

    /************************************Call Interface callback****************************************************/
    @Override
    public void getSuccess(int status, String msg, List<CurrencyDetailsBean> assetsBean) {
        hideProgress();
        if (status == 200) {
            initLineTwo(assetsBean);
            initLine(assetsBean);
        } else {
            AlertDialogShowUtil.toastMessage(this, msg);
        }

    }

    @Override
    public void getFailure(String errorMsg) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(this, errorMsg);
    }

    private List<CurrencyListBean> list = new ArrayList<>();

    @Override
    public void getSelectCoinRateSuccess(List<SelectCoinRateBean> mList) {
        hideProgress();
        String selectCoin = Utils.getSpUtils().getString(Constants.SELECT_COIN);
        mNgk_to_usdt = Utils.getSpUtils().getString(Constants.NGK_TO_USDT_RATE);
        mUsdn_to_usdt = Utils.getSpUtils().getString(Constants.USDN_TO_USDT_RATE);

        for (SelectCoinRateBean selectCoinRateBean : mList) {
            if (selectCoinRateBean.getCurrency_code().equals(selectCoin)) {

                mRate = selectCoinRateBean.getRate();
                if (currencyBean.getCoinName().equals("SOU") || currencyBean.getCoinName().equals("USDS")||currencyBean.getCoinName().equals("HASH")) {
//                    showProgress();
                    presenter.getBalance(currencyBean.getCoinName());

                } else {
                    for (CurrencyListBean currencyListBean : list) {
                        if (currencyListBean.getCoinName().equals(currencyBean.getCoinName())) {
                            String quantity = "";
                            if (currencyBean.getCoinName().equals("BTC")) {
                                quantity = new BigDecimal(currencyListBean.getMoney()).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                            } else if (currencyBean.getCoinName().equals("ETH")) {
                                quantity = new BigDecimal(currencyListBean.getMoney()).setScale(6, BigDecimal.ROUND_DOWN).toPlainString();
                            } else {
                                quantity = new BigDecimal(currencyListBean.getMoney()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString();
                            }
                            quantity = NumUtils.subZeroAndDot(quantity);
                            amountTv.setText(quantity);
//                            valueTv.setText("≈" + currencyListBean.getConvert());
                            String replace = currencyBean.getConvert().replace(mBii, "");
                            valueTv.setText("≈" + mBii + new BigDecimal(replace).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());

                        }
                    }
                }

                break;
            }
        }
    }

    @Override
    public void getSelectCoinRateFailure(String errorMsg) {
        hideProgress();
    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        hideProgress();
        amountTv.setText(new BigDecimal(balance).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        if (currencyBean.getCoinName().equals("SOU")) {
            BigDecimal multiply = new BigDecimal(mNgk_to_usdt).multiply(new BigDecimal(balance)).multiply(new BigDecimal(mRate));
            valueTv.setText("≈" + mBii + multiply.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        }else  if (currencyBean.getCoinName().equals("USDS")) {
            BigDecimal multiply = new BigDecimal(mUsdn_to_usdt).multiply(new BigDecimal(balance)).multiply(new BigDecimal(mRate));
            valueTv.setText("≈" + mBii + multiply.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        }else  if (currencyBean.getCoinName().equals("HASH")) {

            BigDecimal multiply = new BigDecimal(hashprice).multiply(new BigDecimal(balance)).multiply(new BigDecimal(mRate));
            valueTv.setText("≈" + mBii + multiply.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        }
    }

    @Override
    public void getListSuccess(List<CurrencyListBean> mlist) {
        hideProgress();
        list.clear();
        for (CurrencyListBean currencyListBean : mlist) {
            if (!"TetherUSDT".equals(currencyListBean.getCoinName())) {
                list.add(currencyListBean);
            }
        }
        presenter.getSelectCoinRate();
    }

    @Override
    public void onFailure(String errorMsg) {
        hideProgress();
    }

    @Override
    public void getCoinListSuccess(List<CoinListBean> data) {
        for (int i = 0; i < data.size(); i++) {
            if (currencyBean.getCoinName().equals(data.get(i).getCoinName())) {
                Utils.getSpUtils().put("currency_affirm", data.get(i).getAffirm());
            }
        }
    }

    /************************************other maths****************************************************/

    //x周日期
    private void initLineTwo(List<CurrencyDetailsBean> assetsBean) {


        if (null == assetsBean || 0 == assetsBean.size()) {
            return;
        }

        mFormatter = new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                int v = (int) value;
                if (v < assetsBean.size() && v >= 0) {
                    String[] split = assetsBean.get(v).datetime.split("-");
                    String st = split[1] + "/" + split[2];
                    String tim1 = "";
                    tim1 = st;
                    return tim1;
                } else {
                    return null;
                }
            }
        };


    }

    //折线初始化
    @SuppressLint("NewApi")
    private void initLine(List<CurrencyDetailsBean> assetsBean) {
        if (null == assetsBean || 0 == assetsBean.size()) {
            return;
        }
        //得到图例
        Legend legend = chartView.getLegend();  //得到图例
        legend.setForm(Legend.LegendForm.EMPTY);  //设置图例形状为圆形
        legend.setOrientation(Legend.LegendOrientation.VERTICAL);  //设置图例条目垂直排列


        //设置X轴数据
        XAxis xAxis = chartView.getXAxis();

        xAxis.setTextSize(9.0f); // 设置字体大小
        xAxis.setTextColor(getResources().getColor(R.color.color_5e6164));
        xAxis.setDrawGridLines(false); //是否显示X坐标轴上的刻度竖线，默认是true
        xAxis.setDrawAxisLine(true); //是否绘制坐标轴的线，即含有坐标的那条线，默认是true
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1.0f);//x轴时间均分显示
        xAxis.setYOffset(8f);
        //设置Y轴left
        YAxis axisLeft = chartView.getAxisLeft();
        axisLeft.setAxisMinimum(0f);
        axisLeft.setGranularity(1f);
        axisLeft.setGridColor(getResources().getColor(R.color.black));//设置网格线颜色（横线）
        axisLeft.setGridLineWidth(1f);
        axisLeft.setLabelCount(8, false);
        //设置整个line描述信息
//        Description description = new Description();
//        description.setText("");
//        chartView.setDescription(description);//设置描述信息

        //设置LainView属性
        chartView.getDescription().setEnabled(false);  //隐藏描述
        chartView.setNoDataText(""); // 设置当 chart 为空时显示的描述文字
        chartView.getAxisLeft().setDrawLabels(false);// 设置左边的label不可用
        chartView.getAxisLeft().setDrawAxisLine(false); // 设置右边的线不可用
        chartView.getAxisRight().setEnabled(false);  // 隐藏右边 的坐标轴
        chartView.getAxisRight().setDrawLabels(false);// 设置右边的label不可用
        chartView.getAxisRight().setDrawGridLines(false); // 设置右边的线不可用
        chartView.getAxisRight().setDrawAxisLine(false); // 设置右边的线不可用
        chartView.setScaleEnabled(false);  //禁止缩放
        chartView.setTouchEnabled(false);  //禁止触摸
        chartView.setDragEnabled(false);
        chartView.setDrawGridBackground(false);//禁止网格
        chartView.setHighlightPerDragEnabled(false);
        chartView.setBackgroundColor(Color.TRANSPARENT);
        xAxis.setValueFormatter(mFormatter);

        //设置图表距离上下左右的距离    此项同  xAxis.setYOffset(10f);一起使用可设置x轴数据与表的距离
        chartView.setExtraOffsets(0, 0, 0, 8);
        //从X轴进入的动画
        chartView.animateX(1000);
        // add data
        setData(assetsBean.size(), assetsBean);
        chartView.invalidate();
    }

    //折线数据
    private void setData(int count, List<CurrencyDetailsBean> assetsBean) {


        //数据一
        ArrayList<Entry> yVals1 = new ArrayList<>();
        //数据二
        ArrayList<Entry> yVals2 = new ArrayList<>();
        float big1 = 0f;
        float big2 = 0f;
        for (int i = 0; i < count; i++) {
            Drawable drawable = ContextCompat.getDrawable(CurrencyDetailsActivity.this, R.mipmap.mine_progressbar_current_iv);
            Drawable drawable2 = ContextCompat.getDrawable(CurrencyDetailsActivity.this, R.mipmap.mine_progressbar_current_iv2);
            yVals1.add(new Entry(i, Float.parseFloat(assetsBean.get(i).convert), drawable2));
            yVals2.add(new Entry(i, Float.parseFloat(assetsBean.get(i).moneysum), drawable));
            if (Float.parseFloat(assetsBean.get(i).convert) > big1) {
                big1 = Float.parseFloat(assetsBean.get(i).convert);
            }
            if (Float.parseFloat(assetsBean.get(i).moneysum) > big2) {
                big2 = Float.parseFloat(assetsBean.get(i).moneysum);
            }
        }


        //将数据放入折线图
        LineDataSet set1, set2;
        if (chartView.getData() != null &&
                chartView.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chartView.getData().getDataSetByIndex(0);
            set2 = (LineDataSet) chartView.getData().getDataSetByIndex(1);
            set1.setValues(yVals1);
            set2.setValues(yVals2);
            chartView.getData().notifyDataChanged();
            chartView.notifyDataSetChanged();
        } else {

            set1 = new LineDataSet(yVals1, "");
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
//            set1.setColor(getResources().getColor(R.color.white));
            set1.setColor(getResources().getColor(R.color.white));

//            set1.setCircleColor(getResources().getColor(R.color.color_b7957f));
            set1.setLineWidth(2f);
            set1.setCircleRadius(3f);
            set1.setFillAlpha(45);
            set1.setDrawValues(false);
            set1.setDrawCircleHole(true);

            set2 = new LineDataSet(yVals2, "");
//            set2.setColor(getResources().getColor(R.color.color_b7957f));
//            set2.setCircleColor(getResources().getColor(R.color.color_b7957f));
            set2.setColor(getResources().getColor(R.color.color_fe2540));
            set2.setLineWidth(2f);
            set2.setCircleRadius(3f);
            set2.setFillAlpha(45);
            set2.setDrawCircleHole(true);

            ArrayList<ILineDataSet> allLinesList = new ArrayList<>();
            allLinesList.add(set1);
            allLinesList.add(set2);

            //LineData表示一个LineChart的所有数据(即一个LineChart中所有折线的数据)
            LineData data = new LineData(allLinesList);
            data.setDrawValues(false);//折线图不显示数值
            data.setHighlightEnabled(false);
            initNum();
            if (big1 == 0 && big2 == 0) {
                chartView.getAxisLeft().setAxisMinimum(0f);//设置y轴的最小值
                chartView.getAxisLeft().setAxisMaximum(0f);//设置y轴的最小值
            }
            chartView.setData(data);
        }
    }

    ValueFormatter mNumFormatter;

    //x周日期
    private void initNum() {

        mNumFormatter = new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                String st = "0";
                String tim1 = "";
                tim1 = st;
                return tim1;
            }
        };


    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
