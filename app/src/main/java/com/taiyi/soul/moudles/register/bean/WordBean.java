package com.taiyi.soul.moudles.register.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/6/16
 * Time: 1:42 PM
 */
public class WordBean implements Serializable {

    private ArrayList<String> words;

    private String public_key;
    private String private_key;


    public WordBean(ArrayList<String> words, String public_key, String private_key) {
        this.words = words;
        this.public_key = public_key;
        this.private_key = private_key;
    }

    public WordBean() {
    }

    public ArrayList<String> getWords() {
        return words;
    }


    public String getMnemonic(ArrayList<String> words){
        StringBuffer str=new StringBuffer();
        for (int i = 0; i < words.size(); i++) {
            if (i==0){
                str.append(words.get(i));
            }else {
                str.append(" "+words.get(i));
            }
        }
        return str.toString();
    }

    public String getWordsString() {
        String word = "";
        for (String s :words){
            word += " "+s;
        }
        return word;
    }

    public void setWords(ArrayList<String> words) {
        this.words = words;
    }

    public String getPublic_key() {
        return public_key;
    }

    public void setPublic_key(String public_key) {
        this.public_key = public_key;
    }

    public String getPrivate_key() {
        return private_key;
    }

    public void setPrivate_key(String private_key) {
        this.private_key = private_key;
    }
}
