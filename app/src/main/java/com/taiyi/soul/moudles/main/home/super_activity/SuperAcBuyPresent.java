package com.taiyi.soul.moudles.main.home.super_activity;

import android.app.Activity;

import com.alibaba.fastjson.JSONObject;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.MD5Util;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class SuperAcBuyPresent extends BasePresent<SuperAcBuyView> {

    //获取总价
    public void getTotalPrice(Activity activity, String num) {
        Map<String, String> map = new HashMap<>();
        map.put("num1", num);  //
        HttpUtils.postRequest(BaseUrl.CALCULATIONAIRDROP, this, map, new JsonCallback<BaseResultBean<SuperAcBuyNumBean>>() {
            @Override
            public BaseResultBean<SuperAcBuyNumBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<SuperAcBuyNumBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0 || response.body().code == 2) {
                    if (null != view)
                    view.getTotalPriceSuccess(response.body().code,response.body().msg_cn,response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                } else if (response.body().code == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<SuperAcBuyNumBean>> response) {
                super.onError(response);
            }
        });
    }
    //购买
    public void getBuyAir(Activity activity, String num,String json,String pri,String sign,String pass) {
        String md5Str = MD5Util.getMD5Str(sign+"airdrop", MD5Util.MD5_LOWER_CASE);

        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("num1", num);  //数量
        map.put("json", json);  //签名
        map.put("buyprice", pri);  //购买单价
        map.put("sign", md5Str);  //签名+单价
        map.put("paypass", password);  //密码
        HttpUtils.postRequest(BaseUrl.SAVEAIRDROP, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
              if (response.body().code == -1) {
                  if(Constants.isL==0){
                      Constants.isL=1;
                      ToastUtils.showShortToast(response.body().msg_cn);
                      //  AppManager.getAppManager().finishAllActivity();
                      ActivityUtils.next(activity, LoginActivity.class, true);
                      Utils.getSpUtils().remove(Constants.TOKEN);
                  }
                }else if (response.body().code == 3080005) {
                  if(Constants.isL==0){
                      Constants.isL=1;
                      ToastUtils.showShortToast(activity.getString(R.string.account_status));
                      //  AppManager.getAppManager().finishAllActivity();
                      ActivityUtils.next(activity, LoginActivity.class, true);
                      Utils.getSpUtils().remove(Constants.TOKEN);
                  }
                } else {
                    if (null != view)
                        view.getBuyAirSuccess(response.body().code,response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }
    //获取挂单信息（手续费、数量、价格、签名地址等）
    public void getFindOrderData(Activity activity) {
        HttpUtils.postRequest(BaseUrl.DEAFINDORDER, this, "", new JsonCallback<BaseResultBean<IssueFeeBean>>() {
            @Override
            public BaseResultBean<IssueFeeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if(null!=view)
                        view.onIssueFeeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                }else if (response.body().code == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                }else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onError(response);
            }
        });
    }
    //购买记录
    public void getBuyAirList(Activity activity, String PageNum) {
        Map<String, String> map = new HashMap<>();
        map.put("PageNum", PageNum);  //数量
        map.put("PageSize", "10");  //签名
        HttpUtils.postRequest(BaseUrl.FINDAIRDROPLIST, this, map, new JsonCallback<BaseResultBean<AirListBean>>() {
            @Override
            public BaseResultBean<AirListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<AirListBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.getBuyAirListSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                }  else if (response.body().code ==3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<AirListBean>> response) {
                super.onError(response);
            }
        });
    }
    /**
     * 获取余额
     */
    public void getBalance(String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account", Utils.getSpUtils().getString("mainAccount"));

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size() > 0) {
                            view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        } else {
                            view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        view.getBalanceSuccess(symbol, "0.0000");
                    }

                });
    }
    //验证交易密码
    public void checkPayPassword(Activity activity,String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
             if (response.body().code == -1) {
                 if(Constants.isL==0){
                     Constants.isL=1;
                     ToastUtils.showShortToast(response.body().msg_cn);
                     //  AppManager.getAppManager().finishAllActivity();
                     ActivityUtils.next(activity, LoginActivity.class, true);
                     Utils.getSpUtils().remove(Constants.TOKEN);
                 }
                }else   if (response.body().code == 3080005) {
                 if(Constants.isL==0){
                     Constants.isL=1;
                     ToastUtils.showShortToast(activity.getString(R.string.account_status));
                     //  AppManager.getAppManager().finishAllActivity();
                     ActivityUtils.next(activity, LoginActivity.class, true);
                     Utils.getSpUtils().remove(Constants.TOKEN);
                 }
                } else {
                    if(null!=view)
                        view.onCheckPayPasswordSuccess(response.body().code,response.body().msg_cn);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    //获取剩余额度
    public void getFirstData(){
        HttpUtils.postRequest(BaseUrl.GET_ACTIVITY_FIRST_DATA, this, (String) null, new JsonCallback<BaseResultBean<ActivityFirstBean>>() {
            @Override
            public BaseResultBean<ActivityFirstBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ActivityFirstBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().code==0){
                        view.activitySuccess(response.body().data);
                    }else {
                        view.activityFailure(response.body().msg_cn);
                    }
                }}
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ActivityFirstBean>> response) {
                super.onError(response);
            }
        });
    }
}
