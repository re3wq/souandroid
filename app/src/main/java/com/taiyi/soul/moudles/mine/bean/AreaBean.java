package com.taiyi.soul.moudles.mine.bean;

public class AreaBean {

    /**
     * area_name : 石家庄
     * fatherid : 100003
     * area_id : 1000033
     */

    private String area_name;
    private String fatherid;
    private String area_id;

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getFatherid() {
        return fatherid;
    }

    public void setFatherid(String fatherid) {
        this.fatherid = fatherid;
    }

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }
}
