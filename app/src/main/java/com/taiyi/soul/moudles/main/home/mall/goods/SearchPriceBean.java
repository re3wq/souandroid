package com.taiyi.soul.moudles.main.home.mall.goods;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\8\7 0007 9:35
 * @Author : yuan
 * @Describe ：
 */
public class SearchPriceBean {


        public CommodityspecBean commodityspec;
        public String specproids;
        public String commodityid;
        public String userid;

        public static class CommodityspecBean {
            public String specproids;
            public String createtime;
            public String price;
            public String yunfei;
            public String hyprice;
            public String commodityid;
            public String cashprice;
            public String bvprice;
            public String stock;
            public String commodityspecid;
            public String delflag;
            public String hybvprice;
        }
}
