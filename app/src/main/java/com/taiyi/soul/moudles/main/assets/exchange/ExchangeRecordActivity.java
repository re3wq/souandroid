package com.taiyi.soul.moudles.main.assets.exchange;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.assets.exchange.adapter.ExchangeRecordAdapter;
import com.taiyi.soul.moudles.main.assets.exchange.bean.CurrencyBalanceBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRateBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRecordBean;
import com.taiyi.soul.moudles.main.assets.exchange.present.ExchangePresent;
import com.taiyi.soul.moudles.main.assets.exchange.present.ExchangeView;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import butterknife.BindView;
import butterknife.OnClick;

public class ExchangeRecordActivity extends BaseActivity<ExchangeView, ExchangePresent> implements ExchangeView, PullRecyclerView.PullLoadMoreListener {



    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.record_recycle)
    PullRecyclerView mRecordRecycle;

    private int pageNo=1;
    private ExchangeRecordAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exchange_record;
    }

    @Override
    public ExchangePresent initPresenter() {
        return new ExchangePresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mTvTitle.setText(getResources().getString(R.string.exchanges_title_record));

        showProgress();
        presenter.getExchangerRecordData(pageNo+"","10");
        //实例化PullRecyclerView相关信息
        presenter.getFindOrderData(this);
        mRecordRecycle.setLayoutManager(new LinearLayoutManager(this));
        mRecordRecycle.setOnPullLoadMoreListener(this);
        mRecordRecycle.setEmptyView(LayoutInflater.from(this).inflate(R.layout.layout_empty,null));
        mAdapter = new ExchangeRecordAdapter(this);
        mAdapter.setPullRecyclerView(mRecordRecycle);
        mRecordRecycle.setItemAnimator(new DefaultItemAnimator());
        mRecordRecycle.setAdapter(mAdapter);
        mRecordRecycle.refreshWithPull();

    }

    @Override
    public void initEvent() {

    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }



    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
       switch (view.getId()){
           case R.id.iv_back:
               finish();
               break;
       }
    }
    /************************************Call Interface callback****************************************************/

    @Override
    public void onSuccess(int code, String msg_cn, ExchangeBean bean) {

    }

    @Override
    public void onGetRateSuccess(int code, String msg_cn, ExchangeRateBean bean) {

    }

    @Override
    public void onGetExchangerSuccess(int code, String msg_cn, String bean) {

    }

    @Override
    public void onGetExchangerRecordSuccess(int code, String msg_cn, ExchangeRecordBean bean) {
        hideProgress();
        if(code==0){
            if(pageNo==1){
                mAdapter.setDatas(bean.comlist);
            }else {
                mAdapter.addDatas(bean.comlist);
            }
        }else {
            AlertDialogShowUtil.toastMessage(this,msg_cn);
        }

        mRecordRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onIssueFeeSuccess(IssueFeeBean bean) {

    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg_cn, String bean) {

    }

    @Override
    public void getCurrencyBalanceSuccess(int code, String msg_cn, CurrencyBalanceBean balance) {

    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {

    }

    /************************************Refresh and LoadMore****************************************************/


    @Override
    public void onRefresh() {
        pageNo=1;
        presenter.getExchangerRecordData(pageNo+"","10");
    }

    @Override
    public void onLoadMore() {
       pageNo++;
        presenter.getExchangerRecordData(pageNo+"","10");
    }
}
