package com.taiyi.soul.moudles.register.bean;

import android.text.TextUtils;

public class RegisterBean {


    /**
     * token : e0f009f30780447592bb3ac7fee7e8cb
     * child : true
     * uniqueId : 9
     * mainAccount : 18302223400
     * bindAccount : null
     * account : mqecqifpagju
     */

    private String token;
    private boolean child;
    private int uniqueId;
    private String mainAccount;
    private String bindAccount;
    private String account;
    private String googleFlag;
    private String areaCode;
    private String cur_id;
    private String symbol;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCur_id() {
        return cur_id;
    }

    public void setCur_id(String cur_id) {
        this.cur_id = cur_id;
    }

    public String getGoogleFlag() {
        return googleFlag;
    }

    public void setGoogleFlag(String googleFlag) {
        this.googleFlag = googleFlag;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isChild() {
        return child;
    }

    public void setChild(boolean child) {
        this.child = child;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getBindAccount() {

        return TextUtils.isEmpty(bindAccount)?"":bindAccount;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public void setBindAccount(String bindAccount) {
        this.bindAccount = bindAccount;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
