package com.taiyi.soul.moudles.main.node;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;

import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.LineChart;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.node.bean.CpuNetBean;
import com.taiyi.soul.moudles.main.node.bean.NodeBean;
import com.taiyi.soul.moudles.main.node.cpubandwidth.CPUAndBandWidthActivity;
import com.taiyi.soul.moudles.main.node.hashmarket.HashMarketActivity;
import com.taiyi.soul.moudles.main.node.purchasehashrate.PurchaseHashrateActivity;
import com.taiyi.soul.moudles.main.node.ram.RamActivity;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.BigDecimalUtil;
import com.taiyi.soul.utils.BytesUtlis;
import com.taiyi.soul.utils.DateUtils;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;
import com.taiyi.soul.view.MyLineChartView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 * User: ahui
 * Date: 2020-02-26
 * Time: 15:41
 */
public class NodeFragment extends BaseFragment<NodeView, NodePresent> implements NormalView, View.OnClickListener, MultiItemTypeAdapter.OnItemClickListener, NodeView {

    @BindView(R.id.cv_node)
    MyLineChartView cvNode;
    @BindView(R.id.titleTv)
    LinearLayout titleTv;
    @BindView(R.id.topIndTv)
    TextView topIndTv;
    @BindView(R.id.firstClassTv)
    TextView firstLevelTv;
    @BindView(R.id.secondLevelTv)
    TextView secondLevelTv;
    @BindView(R.id.level3Tv)
    TextView level3Tv;
    @BindView(R.id.level4Tv)
    TextView level4Tv;
    @BindView(R.id.computingPowerTv)
    NumberRollingView computingPowerTv;
    @BindView(R.id.computingPowerValueTv)
    NumberRollingView computingPowerValueTv;
    @BindView(R.id.myAssetsTv)
    NumberRollingView myAssetsTv;
    @BindView(R.id.assetsValueTv)
    NumberRollingView assetsValueTv;
    @BindView(R.id.computerPriceTv)
    TextView computerPriceTv;
    @BindView(R.id.currentProgressIv)
    ImageView currentProgressIv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.lineChart)
    LineChart lineChart;
    private boolean node_hidden;
    List<String> xValues;   //x轴数据集合
    List<Float> yValues;  //y轴数据集合
    private List<CpuNetBean> cpuNetBeanList = new ArrayList<>();
    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    private String hash_collect_address;
    private String lastPrice = "0.0000";
    private int level;
    private float aFloat;
    private String ngkBalance = "";
    private List<NodeBean.ListBean> list;
    private NodeBean mNodeBeans;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_node;
    }

    @Override
    public NodePresent initPresenter() {
        return new NodePresent(getContext());
    }


    @Override
    protected void initViews(Bundle savedInstanceState) {

    }


    @Override
    protected void initData() {
        initChart(null);
    }

    private void initChart(List<NodeBean.ListBean> list) {
        xValues = new ArrayList<>();
        yValues = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (NodeBean.ListBean listBean : list) {
                xValues.add(listBean.getCreateTime().substring(0, 5));
                yValues.add(Float.parseFloat(listBean.getPrice()));
            }
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Los_Angeles"),Locale.US);
//            Calendar calendar = Calendar.getInstance();
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            String currentDate = "";
            if (month < 10) {
                if (day < 10) {
                    currentDate = "0" + month + "/0" + day;
                } else {
                    currentDate = "0" + month + "/" + day;
                }
            } else {
                if (day < 10) {
                    currentDate = month + "/0" + day;
                } else {
                    currentDate = month + "/" + day;
                }
            }
            xValues.add(currentDate);
            yValues.add(Float.parseFloat(lastPrice));
        }

//      xy轴集合自己添加数据
        cvNode.setXValues(xValues);
        cvNode.setYValues(yValues);
        cvNode.startAnim(2000);
    }

    private int dp2px(float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    @Override
    public void initEvent() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!node_hidden) {
            //TODO 刷新数据
//            Log.i("dasdasdas", "---刚那个那个你--");
            presenter.getAccountInfoData();
            presenter.getCurrentValue();
            presenter.getBalance("SOU");
            presenter.getFindOrderData(getActivity());
        }

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        node_hidden = hidden;
        if (!hidden) {
            //TODO 刷新数据
//            initLineAnimtion();
            presenter.getBalance("SOU");
            presenter.getAccountInfoData();
            presenter.getCurrentValue();
            presenter.getFindOrderData(getActivity());
//            presenter.getNodeData();
        }
    }


    private void initLineAnimtion() {

        if (level == 0) {
            topIndTv.setVisibility(View.GONE);
            currentProgressIv.setVisibility(View.GONE);
            return;
        } else if (level == 1) {
            if (aFloat >= 200 && aFloat <= 1000) {
                if(aFloat==1000){
                    topIndTv.setText("1k");
                }else {
                    topIndTv.setText(((int) aFloat) + "");
                }

                topIndTv.setVisibility(View.VISIBLE);
                currentProgressIv.setVisibility(View.VISIBLE);
                firstLevelTv.post(new Runnable() {
                    @Override
                    public void run() {
                        float v3 = 1.0f;
                        if (aFloat != 1000)
                            v3 = (aFloat - 183) / 800;
                        int width = firstLevelTv.getWidth();
                        float v = width * v3;

                        float v1 = v;
                        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(currentProgressIv, "translationX", 0, v1).setDuration(500);
                        objectAnimator.start();

                        float v2 = v;
                        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(topIndTv, "translationX", 0, v2).setDuration(500);
                        objectAnimator1.start();
                        progressBar.post(new Runnable() {
                            @Override
                            public void run() {
                                int progressBarWidth = progressBar.getWidth();
                                float progress = v / progressBarWidth * 100;
                                progressBar.setProgress((int) progress);
                            }
                        });
                    }
                });
            }
        } else if (level == 2) {
            if (aFloat > 1000 && aFloat <= 5000) {
//                topIndTv.setText((int) (aFloat / 1000) + "k");
                topIndTv.setText(NumUtils.subZeroAndDot(new BigDecimal(aFloat).divide(new BigDecimal("1000")).setScale(1,BigDecimal.ROUND_DOWN).toPlainString()) + "k");
                topIndTv.setVisibility(View.VISIBLE);
                currentProgressIv.setVisibility(View.VISIBLE);
                firstLevelTv.post(new Runnable() {
                    @Override
                    public void run() {
                        int firstWidth = firstLevelTv.getWidth();
                        secondLevelTv.post(new Runnable() {
                            @Override
                            public void run() {
                                float v3 = 1.0f;
                                if (aFloat != 5000)
                                    v3 = (aFloat - 983) / 5000;
                                int width = secondLevelTv.getWidth();
                                float v = firstWidth + (width * v3);
                                float v1 = v;
                                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(currentProgressIv, "translationX", 0, v1).setDuration(500);
                                objectAnimator.start();

                                float v2 = v;
                                ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(topIndTv, "translationX", 0, v2).setDuration(500);
                                objectAnimator1.start();
                                progressBar.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        int progressBarWidth = progressBar.getWidth();
                                        float progress = v / progressBarWidth * 100;
                                        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, (int) progress);
                                        valueAnimator.setDuration(500);
//                                        progressBar.setProgress((int) progress);
                                        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                            @Override
                                            public void onAnimationUpdate(ValueAnimator animation) {

                                                progressBar.setProgress((Integer) animation.getAnimatedValue());
                                            }
                                        });
                                        valueAnimator.start();
                                    }
                                });
                            }
                        });
                    }
                });

            }
        } else if (level == 3) {
            if (aFloat > 5000 && aFloat <= 10000) {
//                topIndTv.setText((int) (aFloat / 1000) + "k");
                topIndTv.setText(NumUtils.subZeroAndDot(new BigDecimal(aFloat).divide(new BigDecimal("1000")).setScale(1,BigDecimal.ROUND_DOWN).toPlainString()) + "k");
                topIndTv.setVisibility(View.VISIBLE);
                currentProgressIv.setVisibility(View.VISIBLE);
                firstLevelTv.post(new Runnable() {
                    @Override
                    public void run() {
                        int firstWidth = firstLevelTv.getWidth();
                        secondLevelTv.post(new Runnable() {
                            @Override
                            public void run() {
                                int secWidth = secondLevelTv.getWidth();
                                level3Tv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        float v3 = 1.0f;
                                        if (aFloat != 10000)
                                            v3 = (aFloat - 4983) / 5000;

                                        int width = level3Tv.getWidth();

                                        float v = firstWidth + secWidth + (width * v3);

                                        float v1 = v;
                                        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(currentProgressIv, "translationX", 0, v1).setDuration(500);
                                        objectAnimator.start();

                                        float v2 = v;
                                        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(topIndTv, "translationX", 0, v2).setDuration(500);
                                        objectAnimator1.start();

                                        progressBar.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                int progressBarWidth = progressBar.getWidth();
                                                float progress = v / progressBarWidth * 100;
                                                ValueAnimator valueAnimator = ValueAnimator.ofInt(0, (int) progress);
                                                valueAnimator.setDuration(500);
                                                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                    @Override
                                                    public void onAnimationUpdate(ValueAnimator animation) {

                                                        progressBar.setProgress((Integer) animation.getAnimatedValue());
                                                    }
                                                });
                                                valueAnimator.start();
                                            }
                                        });

                                    }
                                });

                            }
                        });
                    }
                });

            }
        } else if (level == 4) {
//            if (aFloat > 10000 && aFloat <= 1000000) {
            if ((int) aFloat > 10000 && (int) aFloat <= 20000) {
                topIndTv.setText((int) (aFloat/1000) + "k");
                topIndTv.setVisibility(View.VISIBLE);
                currentProgressIv.setVisibility(View.VISIBLE);
                firstLevelTv.post(new Runnable() {
                    @Override
                    public void run() {
                        int firstWidth = firstLevelTv.getWidth();
                        secondLevelTv.post(new Runnable() {
                            @Override
                            public void run() {
                                int secWidth = secondLevelTv.getWidth();
                                level3Tv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        int level3Width = level3Tv.getWidth();
                                        level4Tv.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                float v3 = 1.0f;
//                                                if (aFloat != 1000000)
//                                                if (aFloat != 20000)
                                                v3 = (aFloat - 9983) / 9900;
                                                int width = level4Tv.getWidth();
                                                float v = firstWidth + secWidth + level3Width + (width * v3);
                                                float v1 = v;
                                                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(currentProgressIv, "translationX", 0, v1).setDuration(500);
                                                objectAnimator.start();

                                                float v2 = v;
                                                ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(topIndTv, "translationX", 0, v2).setDuration(500);
                                                objectAnimator1.start();

                                                progressBar.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        int progressBarWidth = progressBar.getWidth();
                                                        float progress = v / progressBarWidth * 100;
                                                        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, (int) progress);
                                                        valueAnimator.setDuration(500);
                                                        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                            @Override
                                                            public void onAnimationUpdate(ValueAnimator animation) {

                                                                progressBar.setProgress((Integer) animation.getAnimatedValue());
                                                            }
                                                        });
                                                        valueAnimator.start();
                                                    }
                                                });

                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });

            }
        }

    }

    @OnClick({R.id.buyTv, R.id.marketTv})
    public void onViewClick(View v) {
        switch (v.getId()) {
            case R.id.buyTv:
                Bundle bundle = new Bundle();
                bundle.putString("hash_collect_address", hash_collect_address);
                bundle.putString("lastPrice", lastPrice);
                ActivityUtils.next(getActivity(), PurchaseHashrateActivity.class, bundle);
                break;
            case R.id.marketTv:
                ActivityUtils.next(getActivity(), HashMarketActivity.class);
                break;
        }
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(titleTv).init();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        }

    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
        if (position == 0)
            ActivityUtils.next(getActivity(), RamActivity.class);
        else
            ActivityUtils.next(getActivity(), CPUAndBandWidthActivity.class);
    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }

    @Override
    public void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean) {
        cpuNetBeanList.clear();
        BigDecimal memory_details = BigDecimalUtil.minus(new BigDecimal(blockChainAccountInfoBean.getRam_quota()), new BigDecimal(blockChainAccountInfoBean.getRam_usage()), 0);
        String memory = BytesUtlis.getPrintSize(new BigDecimal(memory_details.toPlainString())) + " / " + BytesUtlis.getPrintSize(new BigDecimal(blockChainAccountInfoBean.getRam_quota()));
        int progress = BigDecimalUtil.multiply(BigDecimalUtil.divide(memory_details, new BigDecimal(blockChainAccountInfoBean.getRam_quota())), new BigDecimal("100")).intValue();
        CpuNetBean cpuNetBean = null;
        if (progress == 0) {
            cpuNetBean = new CpuNetBean(getString(R.string.ram), R.mipmap.node_level_empty_iv, memory);
        } else if (progress > 0 && progress <= 20) {
            cpuNetBean = new CpuNetBean(getString(R.string.ram), R.mipmap.node_level_one_iv, memory);
        } else if (progress > 20 && progress <= 40) {
            cpuNetBean = new CpuNetBean(getString(R.string.ram), R.mipmap.node_level_two_iv, memory);
        } else if (progress > 40 && progress <= 60) {
            cpuNetBean = new CpuNetBean(getString(R.string.ram), R.mipmap.node_level_three_iv, memory);
        } else if (progress > 60 && progress <= 80) {
            cpuNetBean = new CpuNetBean(getString(R.string.ram), R.mipmap.node_level_four_iv, memory);
        } else {
            cpuNetBean = new CpuNetBean(getString(R.string.ram), R.mipmap.node_level_five_iv, memory);
        }

        String cpuStr = DateUtils.formatTime(new BigDecimal(blockChainAccountInfoBean.getCpu_limit().getAvailable())) + " / " + DateUtils.formatTime(new BigDecimal(blockChainAccountInfoBean.getCpu_limit().getMax()));
        int progress1 = (int) (Double.parseDouble(blockChainAccountInfoBean.getCpu_limit().getAvailable()) / Double.parseDouble(blockChainAccountInfoBean.getCpu_limit().getMax()) * 100);
        CpuNetBean cpuNetBean1 = null;
//      new CpuNetBean("CPU", R.mipmap.node_level_three_iv, cpuStr);
        if (progress1 == 0) {
            cpuNetBean1 = new CpuNetBean("CPU", R.mipmap.node_level_empty_iv, cpuStr);
        } else if (progress1 > 0 && progress1 <= 20) {
            cpuNetBean1 = new CpuNetBean("CPU", R.mipmap.node_level_one_iv, cpuStr);
        } else if (progress1 > 20 && progress1 <= 40) {
            cpuNetBean1 = new CpuNetBean("CPU", R.mipmap.node_level_two_iv, cpuStr);
        } else if (progress1 > 40 && progress1 <= 60) {
            cpuNetBean1 = new CpuNetBean("CPU", R.mipmap.node_level_three_iv, cpuStr);
        } else if (progress1 > 60 && progress1 <= 80) {
            cpuNetBean1 = new CpuNetBean("CPU", R.mipmap.node_level_four_iv, cpuStr);
        } else {
            cpuNetBean1 = new CpuNetBean("CPU", R.mipmap.node_level_five_iv, cpuStr);
        }
        //net
        String bandWidth = BytesUtlis.getPrintSize(new BigDecimal(blockChainAccountInfoBean.getNet_limit().getAvailable())) + " / " + BytesUtlis.getPrintSize(new BigDecimal(blockChainAccountInfoBean.getNet_limit().getMax()));
        int progress2 = (int) (Double.parseDouble(blockChainAccountInfoBean.getNet_limit().getAvailable()) / Double.parseDouble(blockChainAccountInfoBean.getNet_limit().getMax()) * 100);
//        Log.i("progress---", "----" + progress + "-----" + progress1 + "-----" + progress2);

        CpuNetBean cpuNetBean2 = null;
//        new CpuNetBean(getString(R.string.band_width), R.mipmap.node_level_five_iv, bandWidth);
        if (progress2 == 0) {
            cpuNetBean2 = new CpuNetBean(getString(R.string.band_width), R.mipmap.node_level_empty_iv, bandWidth);
        } else if (progress2 > 0 && progress2 <= 20) {
            cpuNetBean2 = new CpuNetBean(getString(R.string.band_width), R.mipmap.node_level_one_iv, bandWidth);
        } else if (progress2 > 20 && progress2 <= 40) {
            cpuNetBean2 = new CpuNetBean(getString(R.string.band_width), R.mipmap.node_level_two_iv, bandWidth);
        } else if (progress2 > 40 && progress2 <= 60) {
            cpuNetBean2 = new CpuNetBean(getString(R.string.band_width), R.mipmap.node_level_three_iv, bandWidth);
        } else if (progress2 > 60 && progress2 <= 80) {
            cpuNetBean2 = new CpuNetBean(getString(R.string.band_width), R.mipmap.node_level_four_iv, bandWidth);
        } else {
            cpuNetBean2 = new CpuNetBean(getString(R.string.band_width), R.mipmap.node_level_five_iv, bandWidth);
        }
        Collections.addAll(cpuNetBeanList, cpuNetBean, cpuNetBean1, cpuNetBean2);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        CommonAdapter cpuListAdapter = AdapterManger.getCPUListAdapter(getContext(), cpuNetBeanList);
        recyclerView.setAdapter(cpuListAdapter);
        cpuListAdapter.setOnItemClickListener(this);

    }

    @Override
    public void getDataHttpFail(String errorMsg) {
        toast(errorMsg);
    }

    @Override
    public void getComputerPriceSuccess(String price) {
        computerPriceTv.setText(price);
        lastPrice = price;
//        Log.e("hash=====",lastPrice);
        presenter.getNodeData();
    }

    @Override
    public void getComputerPriceFailure(String errorMsg) {
        toast(errorMsg);
    }

    @Override
    public void getNodeDataSuccess(NodeBean nodeBean) {
        mNodeBeans = nodeBean;
//        Log.e("hash===",nodeBean.getHashrate());
//        computingPowerTv.setMoney(Float.parseFloat(nodeBean.getHashrate()));
        computingPowerTv.setContent(new BigDecimal(nodeBean.getHashrate()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());

        aFloat = Float.parseFloat(lastPrice) * Float.parseFloat(nodeBean.getHashrate());
        if (((int) aFloat >= 20000)) {
            aFloat = 20000;
        }
        topIndTv.setText(String.valueOf(((int) aFloat)));
//        computingPowerValueTv.setMoney(aFloat);

        if (new BigDecimal(lastPrice).multiply(new BigDecimal(nodeBean.getHashrate())).compareTo(new BigDecimal("20000")) == -1) {
            computingPowerValueTv.setContent((new BigDecimal(lastPrice).multiply(new BigDecimal(nodeBean.getHashrate()))).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());

        } else {
            computingPowerValueTv.setContent("20000");

        }

        level = nodeBean.getLevel();

//        level=3;
//        aFloat=7000;
        initLineAnimtion();

        list = nodeBean.getList();

        initChart(list);
    }

    @Override
    public void getNodeDataFailure(String errorMsg) {
        toast(errorMsg);
    }

    @Override
    public void onIssueFeeSuccess(IssueFeeBean issueFeeBean) {
//        hash_collect_address = issueFeeBean.hash_collect_address;
        hash_collect_address = issueFeeBean.purchase_hash_address;

    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        if (symbol.equals("SOU")) {
//            myAssetsTv.setMoney(Float.parseFloat(balance));
            myAssetsTv.setContent(new BigDecimal(balance).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
            ngkBalance = balance;
            presenter.getRate();
        }
    }

    @Override
    public void getRateSuccess(String rate) {
        String assetValue = new BigDecimal(ngkBalance).multiply(new BigDecimal(rate)).toPlainString();
//        assetsValueTv.setMoney(assetValue);
        assetsValueTv.setContent(new BigDecimal(assetValue).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
    }

    @Override
    public void getRateFailure(String errorMsg) {
        toast(errorMsg);
    }
}
