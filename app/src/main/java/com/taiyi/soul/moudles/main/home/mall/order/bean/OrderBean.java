package com.taiyi.soul.moudles.main.home.mall.order.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\8\7 0007 17:22
 * @Author : yuan
 * @Describe ：
 */
public class OrderBean {

        public int totalpage;
        public int rowcount;
        public List<OrderlistBean> orderlist;

        public static class OrderlistBean {
            public String return_type;
            public String isevaluate;
            public String flag;
            public String remark;
            public String commodityid;
            public String salenum;
            public String type;
            public String userid;
            public String commodityspecid;
            public String classificationid;
            public String number;
            public String ngk_ratio;
            public String imgurl;
            public String paycoin;
            public String zhuanprice;
            public String zhuanyunfei;
            public String price;
            public String visitgoodsnum;
            public String businessuserid;
            public String xcday;
            public String shou_type;
            public String stock;
            public String cashPrice;
            public String ordercommodityid;
            public String delflag;
            public String hybvprice;
            public String sumprice;
            public String recommendflag;
            public String ifex;
            public String createtime;
            public String orderid;
            public String yunfei;
            public String addressid;
            public String payment_type;
            public String comcreatetime;
            public String name;
            public String hyprice;
            public String combz;
            public String typename;
            public String ordertype;
            public String smallimgurl;
            public String allmoney_ngk;
            public String allmoney_usdn;
            public String NGK_RATIO;
        }
}
