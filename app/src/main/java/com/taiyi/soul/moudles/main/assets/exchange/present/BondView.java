package com.taiyi.soul.moudles.main.assets.exchange.present;

import com.taiyi.soul.moudles.main.assets.exchange.bean.BondExchangeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondFeeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondRecordBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.RedirectsBean;


public interface BondView {
    void onSuccess(int code, String msg_cn, BondExchangeBean bean);//债券信息
    void onFeeDataSuccess(int code, String msg_cn, BondFeeBean bean);//计算债券花费
    void onSendSuccess(int code, String msg_cn, String bean);//债券
    void onRecordSuccess(int code, String msg_cn, BondRecordBean bean);//债券记录
    void getBalanceSuccess(String symbol,String balance);//获取账户余额
    void onCheckPayPasswordSuccess(int code, String msg_cn, String bean);//校验交易密码
    void onIssueFeeSuccess(IssueFeeBean bean);//获取转账地址
    void getRedirectDataSuccess(int code, String msg_cn, RedirectsBean bean);//文案信息
    void onFailure(String msg_cn);
}
