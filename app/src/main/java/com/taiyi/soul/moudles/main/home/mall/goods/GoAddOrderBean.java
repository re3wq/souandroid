package com.taiyi.soul.moudles.main.home.mall.goods;

import java.io.Serializable;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\8\7 0007 12:02
 * @Author : yuan
 * @Describe ：
 */
public class GoAddOrderBean implements Serializable{

        public ProductBean product;
        public AddressParamsBean addressParams;

        public static class ProductBean implements Serializable{
            public String specproids;
            public String createtime;
            public String yunfei;
            public String num;
            public String commodityid;
            public String bvprice;
            public String type;
            public String commodityspecid;
            public String imgurl;
            public String comtype;
            public String price;
            public String businessuserid;
            public String name;
            public String businessname;
            public String logo;
            public String hyprice;
            public String qiangurl;
            public String stock;
            public String hybvprice;
            public String delflag;
            public String combz;
            public String typename;
            public String smallimgurl;
        }

        public static class AddressParamsBean implements Serializable{
            public String createtime;
            public String address;
            public String useraddress_id;
            public String cityid;
            public String type;
            public String provinceid;
            public String userid;
            public String countryid;
            public String issh;
            public String cityName;
            public String provincename;
            public String surname;
            public String name;
            public String tel;
            public String countryName;
            public String delflag;
        }

}
