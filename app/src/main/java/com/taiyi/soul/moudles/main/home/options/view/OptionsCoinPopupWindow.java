package com.taiyi.soul.moudles.main.home.options.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.moudles.main.home.options.bean.OptionPopupBean;
import com.taiyi.soul.utils.DensityUtil;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class OptionsCoinPopupWindow extends PopupWindow{

    private Context context;
    private View mView;
    private RecyclerView recycle;

    private List<OptionPopupBean._$3Bean> list;
    private CommonAdapter mCommonAdapter;

    private OnCoinItemClick onItemClick;

    public OptionsCoinPopupWindow(Context context, List<OptionPopupBean._$3Bean> list, OnCoinItemClick onItemClick) {
        this.list = list;
        this.context = context;
        this.onItemClick = onItemClick;
        init();
    }


    private void init() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.popup_options_bi_two, null);
        recycle = mView.findViewById(R.id.recycle);


        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recycle.setLayoutManager(manager);
         mCommonAdapter = AdapterManger.getOptionsBiAdapter(context, list);
        recycle.setAdapter(mCommonAdapter);


        mCommonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                dismiss();
                onItemClick.onBiSelect(position,list.get(position).coin_name,list.get(position).id,list.get(position).url);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        // 设置外部可点击
        this.setOutsideTouchable(false);

        /* 设置弹出窗口特征 */
        // 设置视图
        this.setContentView(this.mView);
        // 设置弹出窗体的宽和高
        this.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        this.setWidth(DensityUtil.dip2px(context, 82));




        // 设置弹出窗体可点击
        this.setFocusable(true);

        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0x00000000);
        // 设置弹出窗体的背景
        this.setBackgroundDrawable(dw);

        // 设置弹出窗体显示时的动画，从底部向上弹出
        this.setAnimationStyle(R.style.shop_popup_window_anim);

        this.setOnDismissListener(new OnDismissListener() {

            // 在dismiss中恢复透明度
            public void onDismiss() {
                onItemClick.onHide(false);
            }
        });


    }


    public void notifyData(List<OptionPopupBean._$3Bean> list) {
        this.list = list;
        mCommonAdapter.notifyDataSetChanged();
    }


    public interface OnCoinItemClick {
        void onBiSelect(int position, String coinName, String coin_id, String url);
        void onHide(boolean isHide);
    }

}
