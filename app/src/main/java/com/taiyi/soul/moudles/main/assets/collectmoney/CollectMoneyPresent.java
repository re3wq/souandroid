package com.taiyi.soul.moudles.main.assets.collectmoney;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.CollectMoneyAddressBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class CollectMoneyPresent extends BasePresent<CollectMoneyView> {
    public void getCoinList() {
        HttpUtils.getRequets(BaseUrl.GET_COIN_LIST, this, null, new JsonCallback<BaseResultBean<List<CoinListBean>>>() {
            @Override
            public BaseResultBean<List<CoinListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<CoinListBean>>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getCoinListSuccess(response.body().data);
                    } else {
                        view.getCoinListFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<CoinListBean>>> response) {
                super.onError(response);
            }
        });
    }

    public void getCollectMoneyAddress(String coinName){
        Map<String,String>map=new HashMap<>();
        if(coinName.equals("HASH")){
            coinName="SOU";
        }
        map.put("coinName",coinName);

        HttpUtils.getRequets(BaseUrl.COLLECT_MONEY, this, map, new JsonCallback<BaseResultBean<CollectMoneyAddressBean>>() {
            @Override
            public BaseResultBean<CollectMoneyAddressBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CollectMoneyAddressBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getAddressSuccess(response.body().data);
                    } else {
                        view.getAddressFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<CollectMoneyAddressBean>> response) {
                super.onError(response);
            }
        });
    }
}
