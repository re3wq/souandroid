package com.taiyi.soul.moudles.main.home.options.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.RedirectsBean;
import com.taiyi.soul.moudles.main.home.options.adapter.OptionsDetailsFragmentAdaptert;
import com.taiyi.soul.moudles.main.home.options.bean.KLainBean;
import com.taiyi.soul.moudles.main.home.options.bean.KOtherBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionDetailsListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionPopupBean;
import com.taiyi.soul.moudles.main.home.options.presenter.OptionsDetailPresent;
import com.taiyi.soul.moudles.main.home.options.presenter.OptionsDetailView;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;


/**
 * Created by Android Studio.
 *
 * @Date ： 2020\6\2 0002 13:56
 * @Author : yuan
 * @Describe ：klain-历史明细
 */
public class OptionsHistoryFragment extends BaseFragment<OptionsDetailView, OptionsDetailPresent> implements OptionsDetailView, PullRecyclerView.PullLoadMoreListener {


    @BindView(R.id.recycle)
    PullRecyclerView recycle;
    private boolean node_hidden;
    private OptionsDetailsFragmentAdaptert mFragmentAdapter;

    private String id;
    private List<OptionDetailsListBean.ComlistBean> mComlist;

    public OptionsHistoryFragment(String id) {
        this.id = id;
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_options;
    }


    @Override
    public OptionsDetailPresent initPresenter() {
        return new OptionsDetailPresent();
    }

    public void reData(String id){
        presenter.getData(getActivity(),1+"","1",id);
    }
    @Override
    protected void initData() {

        presenter.getData(getActivity(), pageNo + "", "1", id);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 1);
        recycle.setLayoutManager(manager);
        //实例化PullRecyclerView相关信息
        recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        recycle.setIsRefreshEnabled(false);
        recycle.setIsLoadMoreEnabled(true);
        recycle.setOnPullLoadMoreListener(this);
        recycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new OptionsDetailsFragmentAdaptert(getContext());
        mFragmentAdapter.setPullRecyclerView(recycle);
        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setAdapter(mFragmentAdapter);
        recycle.refreshWithPull();
        recycle.setPullLoadMoreCompleted();
    }

    @Override
    public void initEvent() {

    }


    @Override
    protected void initViews(Bundle savedInstanceState) {
        if(!EventBus.getDefault().isRegistered(true))
        EventBus.getDefault().post(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.getData(getActivity(), pageNo + "", "1", id);
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser){
//            Log.e("tag===","isstart");
            presenter.getData(getActivity(), pageNo + "", "1", id);
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();

    }

    /************************************Call Interface callback****************************************************/
    private int pageNo = 1;

    //获取列表
    @Override
    public void getDataSuccess(int status, String msg, OptionDetailsListBean bean) {
        if (status == 0) {

            if (pageNo == 1) {
                mComlist = bean.comlist;
                mFragmentAdapter.setDatas(bean.comlist);
            } else {
                mComlist.addAll(bean.comlist);
                mFragmentAdapter.addDatas(bean.comlist);
            }

        } else {
            AlertDialogShowUtil.toastMessage(getContext(), msg);
        }
        recycle.setPullLoadMoreCompleted();
    }

    @Override
    public void getPopupSuccess(int status, String msg, OptionPopupBean bean) {

    }

    @Override
    public void getPopupBuySuccess(int status, String msg) {

    }

    @Override
    public void getCurrencyBalanceSuccess(int code, String msg_cn, String balance) {

    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {

    }

    @Override
    public void onIssueFeeSuccess(int code, String msg_cn, IssueFeeBean bean) {

    }

    @Override
    public void getKLineSuccess(int code, String msg_cn, KLainBean bean) {

    }

    @Override
    public void getKOtherSuccess(int code, String msg_cn, KOtherBean bean) {

    }

    @Override
    public void getPictureDataSuccess(int code, String msg_cn, List<OptionListBean> bean) {

    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg_cn, String bean) {

    }

    @Override
    public void getRedirectDataSuccess(int code, String msg_cn, RedirectsBean bean) {

    }

    @Override
    public void onFailure() {

    }

    /************************************refresh or load more****************************************************/
    @Override
    public void onRefresh() {
        pageNo = 1;
        presenter.getData(getActivity(), pageNo + "", "1", id);
    }

    @Override
    public void onLoadMore() {
        pageNo++;
        presenter.getData(getActivity(), pageNo + "", "1", id);
    }
}
