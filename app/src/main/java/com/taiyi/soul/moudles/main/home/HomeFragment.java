package com.taiyi.soul.moudles.main.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.exchange.ExchangeActivity;
import com.taiyi.soul.moudles.main.assets.transfer.TransferActivity;
import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;
import com.taiyi.soul.moudles.main.home.bean.HomeBean;
import com.taiyi.soul.moudles.main.home.bean.OptionIsOptenBean;
import com.taiyi.soul.moudles.main.home.latestnews.LatestNewsActivity;
import com.taiyi.soul.moudles.main.home.mall.goods.GoodsDetailActivity;
import com.taiyi.soul.moudles.main.home.mall.goods.MallActivity;
import com.taiyi.soul.moudles.main.home.mall.view.ResizableImageView;
import com.taiyi.soul.moudles.main.home.options.OptionsListActivity;
import com.taiyi.soul.moudles.main.home.options.adapter.OptionsFragmentAdapter;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.main.home.super_activity.SuperAcActivity;
import com.taiyi.soul.moudles.main.webview.TwoWebViewActivity;
import com.taiyi.soul.moudles.main.webview.WebViewActivity;
import com.taiyi.soul.moudles.mine.bean.CurrencyListBean;
import com.taiyi.soul.moudles.mine.invite.InviteActivity;
import com.taiyi.soul.moudles.mine.myearnings.MyEarningsActivity;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ScreenUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.CustomView;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import com.zhouwei.mzbanner.MZBannerView;
import com.zhouwei.mzbanner.holder.MZHolderCreator;
import com.zhouwei.mzbanner.holder.MZViewHolder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 * User: ahui
 * Date: 2020-02-26
 * Time: 15:41
 */
public class HomeFragment extends BaseFragment<HomeView, HomePresent> implements NormalView, View.OnClickListener, HomeView, OptionsFragmentAdapter.OptionsItemClick {


    @BindView(R.id.banner)
    MZBannerView banner;
//
//    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.recyclerViewBrowser)
    RecyclerView recyclerViewBrowser;
//    @BindView(R.id.relatedRecyclerView)
    RecyclerView relatedRecyclerView;


//    @BindView(R.id.gif_img)
    ImageView gif_img;
//    @BindView(R.id.drag_view)
    CustomView drag_view;

//    @BindView(R.id.event_cll)
    TextView event_cll;

    @BindView(R.id.recycle)
    PullRecyclerView recycle;
    @BindView(R.id.inv_img)
    ImageView inv_img;
    private OptionsFragmentAdapter mFragmentAdapter;
    private boolean home_hidden;
    private int newsID;
    private int isShow = 0;
    private String newsUrl;
    private double lastx, lastY;
    private int screenHeight;
    private BigDecimal b3;
    private BigDecimal divide2;
    private int mValue;

    //    private TranslateAnimation animationIn;
//    private TranslateAnimation animation;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_home_new;
//        return R.layout.fragment_home;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }


    @Override
    public HomePresent initPresenter() {
        return new HomePresent();
    }

    @OnClick({ R.id.collectMoneyTv, R.id.transferTv, R.id.exchangeTv, R.id.debtTv, R.id.inv_img})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.collectMoneyTv://收yi
                ActivityUtils.next(getActivity(), MyEarningsActivity.class);
//                ActivityUtils.next(getActivity(), CollectMoneyActivity.class);
                break;
            case R.id.transferTv://转账
                ActivityUtils.next(getActivity(), TransferActivity.class);
                break;
            case R.id.exchangeTv://兑换
//                toast(getResources().getString(R.string.not_open));
                ActivityUtils.next(getActivity(), ExchangeActivity.class, false);
                break;
            case R.id.debtTv://债权
                toast(getResources().getString(R.string.not_open));
//                ActivityUtils.next(getActivity(), BondExchangeActivity.class, false);
                break;
                case R.id.inv_img://邀请
                    ActivityUtils.next(getActivity(), InviteActivity.class);
//                ActivityUtils.next(getActivity(), BondExchangeActivity.class, false);
                break;
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initViews(Bundle savedInstanceState) {

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int maxWidth = metrics.widthPixels;
        if(Constants.Proportion==0){
            BitmapFactory.Options options = new BitmapFactory.Options();
            BitmapFactory.decodeResource(getResources(),R.mipmap.home_invite_img,options);
            int width =options.outWidth;
            int height =options.outHeight;
            BigDecimal b1 = new BigDecimal(width);  //1080  1:2     2160 1:2
            BigDecimal b2 = new BigDecimal(height);  //2160
            b3 = new BigDecimal(maxWidth);
            double divide = b2.divide(b1, 1, BigDecimal.ROUND_DOWN).doubleValue();//  h : w
            //宽高比
            divide2 = new BigDecimal(Double.toString(divide));
            mValue = divide2.multiply(b3).intValue();
            Constants.Proportion = divide2.multiply(b3).intValue();
        }else {
            mValue =  Constants.Proportion;
        }


        ViewGroup.LayoutParams params = inv_img.getLayoutParams();

//       int mValue = divide2.multiply(b3).intValue();
        params.width = maxWidth; // 宽度设置成屏幕宽度，这里根据自己喜好设置
        params.height = mValue; // 利用已知图片的宽高比计算高度
        inv_img.setLayoutParams(params);

        screenHeight = ScreenUtil.getScreenHeight(getContext());
        recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        recycle.setOnPullLoadMoreListener(null);
        recycle.setIsLoadMoreEnabled(false);
        recycle.setIsRefreshEnabled(false);
        recycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new OptionsFragmentAdapter(getContext(), this);
        mFragmentAdapter.setPullRecyclerView(recycle);
//        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setAdapter(mFragmentAdapter);
        recycle.refreshWithPull();
        recycle.setPullLoadMoreCompleted();
//        Glide.with(getContext()).load(R.mipmap.gif_img).into(gif_img);

//        gif_img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        ActivityUtils.next(getActivity(), SuperAcActivity.class, false);
//                    }
//                }, 50);
//
//            }
//        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }




    private void initNewsList(List<HomeBean.TwoNewsBean> newsBeanList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        CommonAdapter firstNewsListAdapter = AdapterManger.getFirstNewsListAdapter(getContext(), newsBeanList);
        recyclerView.setAdapter(firstNewsListAdapter);
        firstNewsListAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                newsID = newsBeanList.get(position).getId();
                newsUrl = newsBeanList.get(position).getUrl();
//                showProgress();
                presenter.updateReadNum(String.valueOf(newsBeanList.get(position).getId()));

            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    private void initRelated(List<HomeBean.OrganizeListBean> organizeList) {
//        List<TestBean> relatedList=new LinkedList<>();
//        for (int i = 0; i < 10; i++) {
//            relatedList.add(new TestBean());
//        }
        LinearLayoutManager relayoutManager = new LinearLayoutManager(getContext());
        relayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        relatedRecyclerView.setLayoutManager(relayoutManager);
        CommonAdapter relatedListAdapter = AdapterManger.getRelatedListAdapter(getContext(), organizeList);
        relatedRecyclerView.setAdapter(relatedListAdapter);
        relatedListAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                Bundle bundle = new Bundle();
                bundle.putInt("type", 1);
                bundle.putInt("id", organizeList.get(position).getId());
                bundle.putString("url", organizeList.get(position).getUrl());
                ActivityUtils.next(getActivity(), TwoWebViewActivity.class, bundle);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    private void initBrowers(List<HomeBean.BrowserslistBean> browserslist) {
//        List<TestBean> browserList=new LinkedList<>();
//        for (int i = 0; i < 5; i++) {
//            browserList.add(new TestBean());
//        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewBrowser.setLayoutManager(layoutManager);
        CommonAdapter browserListAdapter = AdapterManger.getBrowserListAdapter(getContext(), browserslist);
        recyclerViewBrowser.setAdapter(browserListAdapter);
        browserListAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                Bundle bundle = new Bundle();
                bundle.putInt("type", 1);
                bundle.putString("url", browserslist.get(position).getUrl());
                ActivityUtils.next(getActivity(), TwoWebViewActivity.class, bundle);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
    }

    private List<HomeBean.BannerBean> bannerNewsReco = new ArrayList<>();

    private void initBanner(List<HomeBean.BannerBean> bannerNews) {
//        List<HomeBean.BannerNewsBean> imgslun = new ArrayList<>();
//        //资源文件
//
//        imgslun.add(R.mipmap.banner_main_one);
//        imgslun.add(R.mipmap.banner_main_two);
//        imgslun.add(R.mipmap.banner_main_three);

        if (bannerNewsReco.size() == 0) {
            banner.setIndicatorVisible(true);
            banner.setIndicatorAlign(MZBannerView.IndicatorAlign.RIGHT);
            banner.setIndicatorRes(R.mipmap.mall_dot_nom, R.mipmap.mall_dot);
            banner.setIndicatorPadding(0, 40, 40, 0);
            banner.setPages(bannerNews, new MZHolderCreator<BannerViewHolder>() {
                @Override
                public BannerViewHolder createViewHolder() {
                    return new BannerViewHolder();
                }
            });

            banner.start();
            bannerNewsReco = bannerNews;
        } else {
            boolean b = compareList(bannerNews, bannerNewsReco);
            if (b == false) {
                banner.setIndicatorVisible(true);
                banner.setIndicatorAlign(MZBannerView.IndicatorAlign.RIGHT);
                banner.setIndicatorRes(R.mipmap.mall_dot_nom, R.mipmap.mall_dot);
                banner.setIndicatorPadding(0, 40, 40, 0);
                banner.setPages(bannerNews, new MZHolderCreator<BannerViewHolder>() {
                    @Override
                    public BannerViewHolder createViewHolder() {
                        return new BannerViewHolder();
                    }
                });

                banner.start();
                bannerNewsReco = bannerNews;
            }
        }


        banner.setBannerPageClickListener(new MZBannerView.BannerPageClickListener() {
            @Override
            public void onPageClick(View view, int position) {
//                toast("---"+position);
                HomeBean.BannerBean bannerBean = bannerNews.get(position);
                if(null!=bannerBean.getApptype()){
                    if (bannerBean.getApptype().equals("1")) {//资讯相关
                        String herfid = bannerBean.getHerfid();
                        if (herfid != null && !"".equals(herfid)) {
                            //跳h5
                            String h5url = bannerBean.getH5url();
                            Bundle bundle = new Bundle();
                            bundle.putInt("id", Integer.parseInt(herfid));
                            bundle.putString("url", h5url);
                            ActivityUtils.next(getActivity(), WebViewActivity.class, bundle);
                        } else {
                            //跳资讯列表
                            ActivityUtils.next(getActivity(), LatestNewsActivity.class);
                        }
//                    ActivityUtils.next(getActivity(), SuperAcActivity.class);

                    } else if (bannerBean.getApptype().equals("2")) {//商城相关
                        String herfid = bannerBean.getHerfid();
                        if (herfid != null) {//跳商品详情
                            Bundle bundle = new Bundle();
                            bundle.putString("commodityid", herfid);
                            ActivityUtils.next(getActivity(), GoodsDetailActivity.class, bundle, false);
                        } else {
                            //跳商城列表
                            ActivityUtils.next(getActivity(), MallActivity.class, false);
                        }
                    } else if (bannerBean.getApptype().equals("3")) {//活动
                        ActivityUtils.next(getActivity(), SuperAcActivity.class, false);
                    }
                }

            }
        });
    }

    /**
     * @param list1 集合1
     * @param list2 集合2
     * @param <T>   数据类型
     * @return
     */
    private <T> boolean compareList(List<T> list1, List<T> list2) {
        if (list1 == null) {
            return false;
        }
        if (list1.size() != list2.size()) {
            return false;
        }

        Set<Integer> hashCodeSet = new HashSet<>();
        for (T adInfoData : list1) {
            hashCodeSet.add(adInfoData.hashCode());
        }
        for (T adInfoData : list2) {
            if (!hashCodeSet.contains(adInfoData.hashCode())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onSuccess(HomeBean homeBean) {
        hideProgress();
        List<HomeBean.BannerBean> bannerNews = homeBean.getBanner();
        initBanner(bannerNews);
        List<HomeBean.BrowserslistBean> browserslist = homeBean.getBrowserslist();
        List<HomeBean.OrganizeListBean> organizeList = homeBean.getOrganizeList();
        List<HomeBean.TwoNewsBean> newsBeanList = homeBean.getTwoNews();
        if (null != homeBean.getBrowserslist() && homeBean.getBrowserslist().size() > 0) {
            initBrowers(browserslist);
        }
//        if (null != homeBean.getOrganizeList() && homeBean.getOrganizeList().size() > 0) {
//            initRelated(organizeList);
//        }
//        if (null != homeBean.getTwoNews() && homeBean.getTwoNews().size() > 0) {
//            initNewsList(newsBeanList);
//        }
    }

    @Override
    public void onFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void updateSuccess() {
        hideProgress();
        Bundle bundle = new Bundle();
        bundle.putInt("type", 0);
        bundle.putInt("id", newsID);
        bundle.putString("url", newsUrl);
        ActivityUtils.next(getActivity(), WebViewActivity.class, bundle);
    }

    @Override
    public void updateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void activitySuccess(ActivityFirstBean activityFirstBean) {
        hideProgress();
        if (activityFirstBean.isAirdrop_status() == true) {
            drag_view.setVisibility(View.VISIBLE);
        } else {
            drag_view.setVisibility(View.GONE);
        }
    }

    @Override
    public void activityFailure(int code, String errorMsg) {
        hideProgress();
        if (code == -1) {
            ToastUtils.showShortToast(errorMsg);
//           //  AppManager.getAppManager().finishAllActivity();
            ActivityUtils.next(getActivity(), LoginActivity.class, true);
            Utils.getSpUtils().remove(Constants.TOKEN);
        }
    }

    @Override
    public void getCoinSuccess(int status, String msg, List<CurrencyListBean> data) {
        hideProgress();
        if (status == 200) {
            String cur_id = Utils.getSpUtils().getString("cur_id");
            for (int i = 0; i < data.size(); i++) {
                String id = data.get(i).getId() + "";
                if (id.equals(cur_id)) {
                    Utils.getSpUtils().put("cur_coin_name", data.get(i).getName());
                    Utils.getSpUtils().put("cur_coin_symbol", data.get(i).getSymbol());
                }
            }
        } else {
            AlertDialogShowUtil.toastMessage(getContext(), msg);
        }
    }

    @Override
    public void getOptionOption(int status, String msg_cn, OptionIsOptenBean data) {
        if (status == 0) {
            if (data.getIf_up_down().equals("0")) {
                ActivityUtils.next(getActivity(), OptionsListActivity.class, false);
            } else {
                AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.not_open));
            }
        } else {
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }
    }
    private List<OptionListBean> mAssetsBean1;
    @Override
    public void getDataSuccess(int status, String msg, List<OptionListBean> data) {
        hideProgress();
        if (status == 200) {
            mAssetsBean1 = data;
            mFragmentAdapter.setDatas(data);
        } else {
            AlertDialogShowUtil.toastMessage(getContext(), msg);
        }
        recycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onItemClick(int position) {
//        Bundle bundle = new Bundle();
//        bundle.putString("id", mAssetsBean1.get(position).id);
//        bundle.putString("name", mAssetsBean1.get(position).instrument_id);
//        bundle.putString("img", mAssetsBean1.get(position).img_url);
//        ActivityUtils.next(getActivity(), OptionsDetailActivity.class, bundle, false);
    }


    public class BannerViewHolder implements MZViewHolder<HomeBean.BannerBean> {
        private ResizableImageView mImageView;
        private int mHeight = 0;
        private int mWidth = 0;

        @Override
        public View createView(Context context) {
            // 返回页面布局文件
            View view = LayoutInflater.from(context).inflate(R.layout.item_banner_new, null);
            mImageView = view.findViewById(R.id.banner_image);
            mImageView.setAdjustViewBounds(true);
            ViewGroup.LayoutParams lp = mImageView.getLayoutParams();
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int maxWidth = metrics.widthPixels;
            lp.width = maxWidth;
            lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            mImageView.setLayoutParams(lp);
            Resources res = getResources();

//            if (mWidth == 0 || mHeight == 0) {
//                Bitmap bitmap = BitmapFactory.decodeResource(res, R.mipmap.banner_main_one);
//                mWidth = bitmap.getWidth();
//                mHeight = bitmap.getHeight();
//                bitmap.recycle();
//            }

//            BigDecimal b1 = new BigDecimal(Double.toString(mWidth));
//            BigDecimal b2 = new BigDecimal(Double.toString(mHeight));
//            BigDecimal b3 = new BigDecimal(Double.toString(maxWidth));
//            double divide = b2.divide(b1, 2, BigDecimal.ROUND_HALF_UP).doubleValue();

//            BigDecimal divide2 = new BigDecimal(Double.toString(divide));
//            int mValue = divide2.multiply(b3).intValue();

            ViewGroup.LayoutParams params = banner.getLayoutParams();
//            params.width = maxWidth; // 宽度设置成屏幕宽度，这里根据自己喜好设置
//            params.height = mValue; // 利用已知图片的宽高比计算高度
            banner.setLayoutParams(params);
            return view;
        }


        @Override
        public void onBind(Context context, int position, HomeBean.BannerBean data) {
            // 数据绑定
            DrawableCrossFadeFactory drawableCrossFadeFactory = new DrawableCrossFadeFactory.Builder(0).setCrossFadeEnabled(true).build();

            Glide.with(context).load(Uri.parse(data.getImgurl())).apply(new RequestOptions().placeholder(R.drawable.icz)).apply(RequestOptions.bitmapTransform(new RoundedCorners(20))).transition(DrawableTransitionOptions.with(drawableCrossFadeFactory)).into(mImageView);
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {


    }

    @Override
    public void onResume() {
        super.onResume();

//        if (!home_hidden) {
        //TODO 刷新数据
        presenter.getData();
        presenter.getCurrencyList();
        presenter.getOptionData(getActivity());
//        }


    }

    @Override
    public void onPause() {
        super.onPause();
        banner.pause();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        home_hidden = hidden;

        if (!hidden) {
            //TODO 刷新数据
            if (null != presenter) {
                presenter.getData();
                presenter.getOptionData(getActivity());
//                presenter.getFirstData();
            }
        } else {
//            Log.e("isShow===", hidden + "");

//            if (isShow == 1) {
//                isShow = 0;
//                closeAnimationIn();
//            }

        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        }

    }

//    @OnClick({R.id.rl_shop, R.id.rl_two, R.id.moreTv})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//
//            case R.id.rl_shop:
////                ActivityUtils.next(getActivity(), MallActivity.class, false);
//                toast(getString(R.string.not_open));
////                AlertDialogShowUtil.toastMessage(getContext(), "暂未开放");
//                break;
//            case R.id.rl_two:
//                presenter.getOptionOpen();
////                ActivityUtils.next(getActivity(), OptionsListActivity.class, false);
//                break;
//            case R.id.moreTv:
//                ActivityUtils.next(getActivity(), LatestNewsActivity.class);
//                break;
//        }
//    }
}
