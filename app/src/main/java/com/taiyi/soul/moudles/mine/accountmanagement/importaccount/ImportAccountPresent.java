package com.taiyi.soul.moudles.mine.accountmanagement.importaccount;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.bean.Account;
import com.taiyi.soul.moudles.mine.bean.AccountListBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.List;

import okhttp3.Response;

public class ImportAccountPresent extends BasePresent<ImportAccountView> {

    private Context mContext;

    public ImportAccountPresent(Context mContext) {
        this.mContext = mContext;
    }


    public void getAccountInfoData(String accountname) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", accountname);
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_CHIAN_ACCOUNT_INFO, mContext, new Gson().toJson(hashMap), new JsonCallback<BlockChainAccountInfoBean>() {
            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                super.onSuccess(response);
                if(null!=view)
                view.getBlockchainAccountInfoDataHttp(response.body());

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                super.onError(response);
                if(null!=view)
                view.getDataHttpFail(mContext.getString(R.string.no_effective_account));
            }
        });


    }

    public void getAccounts(String public_key) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("public_key", public_key);
        HttpUtils.postRequestChain(BaseUrl.GET_ACCOUNT_BY_PUBLIC, mContext, new Gson().toJson(hashMap), new JsonCallback<Account>() {
            @Override
            public void onSuccess(com.lzy.okgo.model.Response<Account> response) {
                if(null!=view){
                    if (response.body().getAccount_names().size() > 0) {
                        view.getAccountHttp(response.body().getAccount_names());
                    } else {
                        view.getDataHttpFail(mContext.getString(R.string.no_effective_account));
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<Account> response) {
                super.onError(response);
                if(null!=view)
                view.getDataHttpFail(mContext.getString(R.string.private_format_err));
            }
        });
    }

    public void getAccountList(Activity activity){
        HttpUtils.getRequets(BaseUrl.GET_ACCOUNT_MANAGER_LIST, this, null, new JsonCallback<BaseResultBean<List<AccountListBean>>>() {
            @Override
            public BaseResultBean<List<AccountListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<AccountListBean>>> response) {
                super.onSuccess(response);
                if(null!=view){
                    if (response.body().status==200){
                        view.getAccountListSuccess(response.body().data);
                    }else if (response.body().status == 602 || response.body().status == 3080005) {//登录失效
                        if(Constants.isL==0){
                            Constants.isL=1;
                        ToastUtils.showLongToast(response.body().msg);

//                        ActivityUtils.next(activity, LoginActivity.class, true);
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        Utils.getSpUtils().remove(Constants.TOKEN);}
                    }
                    else {
                        view.getDataHttpFail(response.body().msg);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<AccountListBean>>> response) {
                super.onError(response);
                if(null!=view)
                view.getDataHttpFail(mContext.getString(R.string.socket_exception));
            }
        });
    }

}
