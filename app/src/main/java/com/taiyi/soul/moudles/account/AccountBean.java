package com.taiyi.soul.moudles.account;

import java.util.List;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/4/9
 * Time: 4:50 PM
 */
public class AccountBean {

    private String account_name;
    private String id;
    private String Invitation_code;
    private String son;

    private String isSelect;
    private int level;


    public AccountBean() {
    }

    public AccountBean(String account_name, String id, String invitation_code) {
        this.account_name = account_name;
        this.id = id;
        Invitation_code = invitation_code;
        this.son = son;
        this.isSelect = isSelect;
        this.child = child;
    }

    private List<AccountBean> child;

    public String getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(String isSelect) {
        this.isSelect = isSelect;
    }

    public List<AccountBean> getChild() {
        return child;
    }

    public void setChild(List<AccountBean> child) {
        this.child = child;
    }

    public String getInvitation_code() {
        return Invitation_code;
    }

    public void setInvitation_code(String invitation_code) {
        Invitation_code = invitation_code;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSon() {
        return son;
    }

    public void setSon(String son) {
        this.son = son;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
