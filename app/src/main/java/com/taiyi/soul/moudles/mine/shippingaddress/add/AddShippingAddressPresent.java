package com.taiyi.soul.moudles.mine.shippingaddress.add;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.mine.bean.AddressInfoBean;
import com.taiyi.soul.moudles.mine.bean.AreaBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.List;

import okhttp3.Response;

public class AddShippingAddressPresent extends BasePresent<AddShippingAddressView> {
    public void getList(String area_id) {
        HttpUtils.postRequest(BaseUrl.GET_COUNTRY_OR_PROVINCE_LIST + "?area_id=" + area_id, this, (String) null, new JsonCallback<BaseResultBean<List<AreaBean>>>() {
            @Override
            public BaseResultBean<List<AreaBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().code == 0) {
                        view.onListSuccess(response.body().data);
                    } else {
                        view.onListFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onError(response);
            }
        });
    }

    public void addAddress(Activity activity,String name,String surname, String tel, String countryid, String provinceid, String cityid, String address, String iftype) {
        String url = BaseUrl.ADD_RECEIPT_ADDRESS + "?name=" + name +"&surname="+surname+ "&tel=" + tel + "&countryid=" + countryid + "&provinceid=" + provinceid + "&cityid=" + cityid
                + "&address=" + address + "&iftype=" + iftype;
        HttpUtils.postRequest(url, this, (String) null, new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().code == 0) {
                        view.addSuccess();
                    } else if (response.body().code == -1) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else {
                        view.addFailure(response.body().msg_cn);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getAddressInfo(Activity activity,String addressId) {
        String url = BaseUrl.GET_ADDRESS_INFO + "?useraddress_id=" + addressId;
        HttpUtils.postRequest(url, this, (String) null, new JsonCallback<BaseResultBean<AddressInfoBean>>() {
            @Override
            public BaseResultBean<AddressInfoBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<AddressInfoBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().code == 0) {
                        view.getAddressInfoSuccess(response.body().data);
                    } else if (response.body().code == -1) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else {
                        view.getAddressInfoFailure(response.body().msg_cn);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<AddressInfoBean>> response) {
                super.onError(response);
            }
        });
    }

    public void deleteAddress(Activity activity,String addressId) {
        String url = BaseUrl.DELETE_ADDRESS + "?useraddress_id=" + addressId;
        HttpUtils.postRequest(url, this, (String) null, new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                if (response.body().code == 0) {
                    view.deleteSuccess();
                }else if (response.body().code == -1) {//登录失效
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                } else {
                    view.deleteFailure(response.body().msg_cn);
                }
            }}

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

    public void editAddress(Activity activity,String name,String surname, String tel, String countryid, String provinceid, String cityid, String address, String iftype,String useraddress_id) {
        String url = BaseUrl.EDIT_ADDRESS_INFO + "?name=" + name +"&surname="+surname+ "&tel=" + tel + "&countryid=" + countryid + "&provinceid=" + provinceid + "&cityid=" + cityid
                + "&address=" + address + "&iftype=" + iftype+"&useraddress_id="+useraddress_id;
        HttpUtils.postRequest(url, this, (String) null, new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().code == 0) {
                        view.addSuccess();
                    } else if (response.body().code == -1) {//登录失效
                        if(Constants.isL==0) {
                            Constants.isL = 1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    } else {
                        view.addFailure(response.body().msg_cn);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

}
