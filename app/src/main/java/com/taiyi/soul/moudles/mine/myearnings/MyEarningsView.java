package com.taiyi.soul.moudles.mine.myearnings;

import com.taiyi.soul.moudles.mine.bean.MyEarningsBean;
import com.taiyi.soul.moudles.mine.bean.MyEarningsListBean;

public interface MyEarningsView {
    void getListSuccess(MyEarningsListBean myEarningsListBean);
    void getListFailure(String errorMsg);
    void loginInvalid(String msg);
    void getEarningsSuccess(MyEarningsBean myEarningsBean);
    void getEarningsFailure(String errorMsg);
}
