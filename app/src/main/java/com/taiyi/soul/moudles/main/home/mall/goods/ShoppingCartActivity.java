package com.taiyi.soul.moudles.main.home.mall.goods;

import android.app.Dialog;
import android.os.Bundle;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.EditCarNumBean;
import com.taiyi.soul.moudles.main.home.mall.bean.ShopCarSubmitBean;
import com.taiyi.soul.moudles.main.home.mall.goods.adapter.ShoppingCarAdapter;
import com.taiyi.soul.moudles.main.home.mall.goods.present.GoodsCarPresent;
import com.taiyi.soul.moudles.main.home.mall.goods.present.GoodsCarView;
import com.taiyi.soul.moudles.main.home.mall.order.CheckConfirmActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.yanzhenjie.recyclerview.OnItemMenuClickListener;
import com.yanzhenjie.recyclerview.SwipeMenu;
import com.yanzhenjie.recyclerview.SwipeMenuBridge;
import com.yanzhenjie.recyclerview.SwipeMenuCreator;
import com.yanzhenjie.recyclerview.SwipeMenuItem;
import com.yanzhenjie.recyclerview.SwipeRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.OnClick;

public class ShoppingCartActivity extends BaseActivity<GoodsCarView, GoodsCarPresent> implements GoodsCarView {


    @BindView(R.id.cart_recycle)
    SwipeRecyclerView mCartRecycle;
    @BindView(R.id.cart_total)
    TextView cart_total;
    @BindView(R.id.nodata)
    LinearLayout nodata;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    private int pageNo = 1;
    private ShoppingCarAdapter mAdapter;

    private String isLoad = "refresh";
    private List<CarListBean.CartListBean> mCartList = new ArrayList<>();
    private int mCarNum;
    private List<String> mCarId;
    private String mShopcarid;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_shopping_cart;
    }

    @Override
    public GoodsCarPresent initPresenter() {
        return new GoodsCarPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }
        showProgress();
        presenter.getData(this, pageNo + "");
        mCartRecycle.loadMoreFinish(false, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void event(String event) {
        if ("CarCheckedChange".equals(event)) {
            Double allPrice = 0.00;
            mCarId = new ArrayList<>();
            for (int i = 0; i < mCartList.size(); i++) {
                if (mCartList.get(i).isChecked) {
                    allPrice += Double.parseDouble(mCartList.get(i).hyprice) * mCartList.get(i).number;
                    mCarId.add(mCartList.get(i).shopcarid);
                }
            }
            cart_total.setText(allPrice + "");
        } else if ("order_refresh".equals(event)) {
            presenter.getData(this, pageNo + "");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Log.e("on===", "onResume");
        presenter.getData(this, 1 + "");
        if(null!=cart_total){
            cart_total.setText("");
        }
    }

    @Override
    protected void initData() {
            mCartRecycle.setLayoutManager(new LinearLayoutManager(ShoppingCartActivity.this));
//        CommonAdapter adapter = AdapterManger.getCartListAdapter(ShoppingCartActivity.this, mCartListBean);
        mCartRecycle.setLoadMoreListener(mLoadMoreListener); // 加载更多的监听。
        mCartRecycle.setSwipeMenuCreator(new SwipeMenuCreator() {
            @Override
            public void onCreateMenu(SwipeMenu leftMenu, SwipeMenu rightMenu, int position) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(ShoppingCartActivity.this)
                        .setImage(getResources().getDrawable(R.mipmap.cart_delete))
                        .setHeight(ViewGroup.LayoutParams.MATCH_PARENT).setWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 56, getResources().getDisplayMetrics()));

                rightMenu.addMenuItem(deleteItem);
            }
        });
        mCartRecycle.setOnItemMenuClickListener(new OnItemMenuClickListener() {
            @Override
            public void onItemClick(SwipeMenuBridge menuBridge, int adapterPosition) {
                // 菜单在Item中的Position：
                int menuPosition = menuBridge.getPosition();
                // TODO: 2020\6\30 0030 在数据源中移除
                deleteCarGoods(adapterPosition);
                menuBridge.closeMenu();
                mAdapter.notifyDataSetChanged();
            }
        });
        ((SimpleItemAnimator) mCartRecycle.getItemAnimator()).setSupportsChangeAnimations(false);
//        mCartRecycle.useDefaultLoadMore(); // 使用默认的加载更多的View。
        mCartRecycle.setAutoLoadMore(true);
        mAdapter = new ShoppingCarAdapter(ShoppingCartActivity.this);
        mCartRecycle.setAdapter(mAdapter);
//        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
    }

    private int deletePosition = 0;

    private void deleteCarGoods(int menuPosition) {
        deletePosition = menuPosition;
        showProgress();
        presenter.deleteCarData(this, mCartList.get(menuPosition).shopcarid);
    }

    @Override
    public void initEvent() {

    }


    @OnClick({R.id.iv_back, R.id.cart_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.cart_next:
                mShopcarid = "";
                if (null == mCarId) {
                    AlertDialogShowUtil.toastMessage(ShoppingCartActivity.this,getResources().getString(R.string.select_goods));
                    return;
                }
                for (int i = 0; i < mCarId.size(); i++) {
                    if (0 == i) {
                        mShopcarid = mCarId.get(0);
                    } else {

                        mShopcarid += "," + mCarId.get(i);
                    }
                }
                showProgress();
                presenter.addSaleData(this, mShopcarid);

                break;
        }
    }

    /**
     * 刷新与加载的监听
     */
    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mCartRecycle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    isLoad = "refresh";
                    pageNo = 1;
                    presenter.getData(ShoppingCartActivity.this, pageNo + "");
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }, 1000);
        }
    };
    private SwipeRecyclerView.LoadMoreListener mLoadMoreListener = new SwipeRecyclerView.LoadMoreListener() {
        @Override
        public void onLoadMore() {
            // 该加载更多啦。
            isLoad = "load";
            pageNo++;

            presenter.getData(ShoppingCartActivity.this, pageNo + "");
            // 请求数据，并更新数据源操作。
            mAdapter.notifyDataSetChanged();


        }
    };

    @Override
    public void onSuccess(CarListBean bean) {
        hideProgress();
        if (pageNo == 1 && bean.cartList.size() == 0) {
            nodata.setVisibility(View.VISIBLE);
            mCartRecycle.setVisibility(View.GONE);
            mCartRecycle.loadMoreFinish(false, true);
            return;
        }
        if (bean.cartList.size() > 0) {
            nodata.setVisibility(View.GONE);
            mCartRecycle.setVisibility(View.VISIBLE);
            if (!isLoad.equals("load")) {
                mCartList.clear();
                mCartList = bean.cartList;
                mAdapter.setData(mCartList);
                mAdapter.notifyDataSetChanged();
            } else {
                for (int i = 0; i < bean.cartList.size(); i++) {
                    mCartList.add(bean.cartList.get(i));
                }
                mAdapter.addData(mCartList);
                mAdapter.notifyDataSetChanged();
            }

        }

        mAdapter.setNumClickListener(new ShoppingCarAdapter.NumClickListener() {
            @Override
            public void onAddNum(View view, int position) {
                mCarNum = mCartList.get(position).number;
                mCarNum++;
                showProgress();
                presenter.editCarData(ShoppingCartActivity.this, 1, position, mCartList.get(position).shopcarid, mCarNum + "");
            }

            @Override
            public void onReduceNum(View view, int position) {
                mCarNum = mCartList.get(position).number;
                if (mCarNum == 1) {
                    Dialog dialog = new Dialog(ShoppingCartActivity.this, R.style.MyDialog);

                    View inflate = LayoutInflater.from(ShoppingCartActivity.this).inflate(R.layout.delete_shopping_car, null);
                    dialog.setContentView(inflate);
                    inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    inflate.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCarNum--;
                            showProgress();
                            presenter.editCarData(ShoppingCartActivity.this, 0, position, mCartList.get(position).shopcarid, mCarNum + "");
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    mCarNum--;
                    showProgress();
                    presenter.editCarData(ShoppingCartActivity.this, 1, position, mCartList.get(position).shopcarid, mCarNum + "");
                }
            }

            @Override
            public void onEditNum(int position, String s) {
                if(mCarNum== Integer.parseInt(s)){
                    return;
                }
                showProgress();
                mCarNum = Integer.parseInt(s);
                presenter.editCarData(ShoppingCartActivity.this, 1, position, mCartList.get(position).shopcarid, mCarNum + "");

            }
        });
    }

    @Override
    public void onDeleteSuccess(String bean) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(ShoppingCartActivity.this, bean);
        mCartList.remove(deletePosition);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onEditNumSuccess(EditCarNumBean bean) {
        hideProgress();
        if (bean.code == 0) {
            if (bean.type == 0) {
                mCartList.remove(bean.position);
                mAdapter.notifyDataSetChanged();
            } else {
                mCartList.get(bean.position).number = mCarNum;
                mAdapter.notifyItemChanged(bean.position);
            }
        } else if (bean.code == 2) {
            AlertDialogShowUtil.toastMessage(ShoppingCartActivity.this, getResources().getString(R.string.stock_less));
            mCartList.get(bean.position).number = Integer.parseInt(bean.mCarBean.stock);
            mCartList.get(bean.position).stock = Integer.parseInt(bean.mCarBean.stock);
            mAdapter.notifyItemChanged(bean.position);
        } else {
            AlertDialogShowUtil.toastMessage(ShoppingCartActivity.this, bean.msg);
        }

        EventBus.getDefault().post("CarCheckedChange");
    }

    @Override
    public void onShopCarSubmitSuccess(ShopCarSubmitBean bean) {
        hideProgress();
        Bundle bundle = new Bundle();
        bundle.putSerializable("submitData", bean);
        bundle.putInt("buy_type", 1);
        bundle.putString("mShopcarid", mShopcarid);
        ActivityUtils.next(ShoppingCartActivity.this, CheckConfirmActivity.class, bundle, false);
    }

    @Override
    public void onFailure() {
        hideProgress();
    }
}
