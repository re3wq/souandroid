package com.taiyi.soul.moudles.main.home.options.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.home.options.OptionsDetailActivity;
import com.taiyi.soul.moudles.main.home.options.adapter.OptionsFragmentAdapter;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionsTabChangeBean;
import com.taiyi.soul.moudles.main.home.options.presenter.OptionsPresent;
import com.taiyi.soul.moudles.main.home.options.presenter.OptionsView;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\6\17 0017 10:34
 * @Author : yuan
 * @Describe ：期权-名称
 */
public class OptionsListOneFragment extends BaseFragment<OptionsView, OptionsPresent> implements OptionsView, OptionsFragmentAdapter.OptionsItemClick, PullRecyclerView.PullLoadMoreListener {
    @BindView(R.id.recycle)
    PullRecyclerView recycle;
    private OptionsFragmentAdapter mFragmentAdapter;
    private List<OptionListBean> mAssetsBean1;
    public int pageNo = 1;

   private int isJ = 0;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            presenter.getData(getActivity(), "2", sort + "");
            handler.postDelayed(this, 3000);
        }
    };


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        Log.e("ussss==",isVisibleToUser+"");
        if (isVisibleToUser) {//true
            handler.postDelayed(runnable,1000);
        }else {//false
            handler.removeCallbacks(runnable);
        }
    }
    @Override
    public OptionsPresent initPresenter() {
        return new OptionsPresent();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, 1000);
    }
    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if (EventBus.getDefault().isRegistered(this)) {
        } else {
            EventBus.getDefault().register(this);
        }

        showProgress();
        presenter.getData(getActivity(), "2", sort + "");
        //条件1成交量2名称3涨幅

        //实例化PullRecyclerView相关信息
        recycle.setLayoutManager(new LinearLayoutManager(getContext()));
        recycle.setOnPullLoadMoreListener(this);
        recycle.setIsLoadMoreEnabled(false);
        recycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new OptionsFragmentAdapter(getContext(), this);
        mFragmentAdapter.setPullRecyclerView(recycle);
        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setAdapter(mFragmentAdapter);
        recycle.refreshWithPull();
        recycle.setPullLoadMoreCompleted();
    }
    private int sort = 1;
    @Subscribe
    public void even(OptionsTabChangeBean bean) {
        if (bean.tab_position == 0) {
            if (bean.sort != sort) {
                sort = bean.sort;
                mFragmentAdapter.clearDatas();
                presenter.getData(getActivity(), "2", sort + "");
            }
        }
    }

    @Override
    protected void initData() {
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_options;
    }

    @Override
    public void onItemClick(int position) {
        if( mAssetsBean1.get(position).instrument_id.equals("USDS") ||  mAssetsBean1.get(position).instrument_id.equals("SOU") ){
            AlertDialogShowUtil.toastMessage(getContext(),getContext().getString(R.string.miaoheyue_no));
//            toast(getContext().getString(R.string.miaoheyue_no));
        }else {
            Bundle bundle = new Bundle();
            bundle.putString("id", mAssetsBean1.get(position).id);
            bundle.putString("name", mAssetsBean1.get(position).instrument_id);
            bundle.putString("img", mAssetsBean1.get(position).img_url);
            ActivityUtils.next(getActivity(), OptionsDetailActivity.class, bundle, false);
        }

    }

    @Override
    public void getDataSuccess(int status, String msg, List<OptionListBean> data) {
        hideProgress();
        if (status == 200) {
            mAssetsBean1 = data;
            mFragmentAdapter.setDatas(data);
        } else {
            AlertDialogShowUtil.toastMessage(getContext(), msg);
        }
        recycle.setPullLoadMoreCompleted();
    }


    @Override
    public void onFailure() {
        hideProgress();
    }

    @Override
    public void onRefresh() {
        hideProgress();
    }

    @Override
    public void onLoadMore() {
        hideProgress();
    }
}
