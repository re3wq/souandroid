package com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.MainActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BindSuccessActivity extends BaseActivity<NormalView, NormalPresenter> {


    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.toLoginTv)
    TextView toLoginTv;
    @BindView(R.id.iv_phone)
    ImageView ivPhone;
    @BindView(R.id.iv_email)
    ImageView ivEmail;
    private int bindType;
    private int loginType, starAct;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_bind_success;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        loginType = Utils.getSpUtils().getInt("loginType");
        starAct = getIntent().getIntExtra("starAct", 0);//0手机号  1邮箱号
        bindType = getIntent().getIntExtra("updataType", 0);//0绑定  1更换

        if (starAct == 0) {//手机 跳转
            if(loginType==0){
                toLoginTv.setText(getString(R.string.to_login));
            }else {
                toLoginTv.setText(getString(R.string.back));
            }
            if (bindType == 0) {
                titleTv.setText(getString(R.string.bind_phone_number));
                ivEmail.setVisibility(View.VISIBLE);
            } else {
                titleTv.setText(getString(R.string.update_phonenumber));
                ivEmail.setVisibility(View.VISIBLE);
            }
        } else {//邮箱 跳转
            if(loginType!=0){
                toLoginTv.setText(getString(R.string.to_login));
            }else {
                toLoginTv.setText(getString(R.string.back));
            }
            if (bindType == 0) {
                titleTv.setText(getString(R.string.bind_email));
                ivEmail.setVisibility(View.VISIBLE);
            } else {
                titleTv.setText(getString(R.string.update_email));
                ivEmail.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.backIv, R.id.toLoginTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
            case R.id.toLoginTv:
                if (starAct == 0) {//手机 跳转
                    if (loginType == 0) {//手机登录
                        ActivityUtils.next(this, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("startact_bind", 4);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(this, MainActivity.class, bundle, true);
                    }

                } else {//邮箱 跳转
                    if (loginType != 0) {//邮箱登录
                        ActivityUtils.next(this, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("startact_bind", 4);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(this, MainActivity.class, bundle, true);
                    }
                }
                break;
        }
    }
}