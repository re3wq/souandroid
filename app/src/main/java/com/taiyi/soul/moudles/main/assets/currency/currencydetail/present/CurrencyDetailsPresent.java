package com.taiyi.soul.moudles.main.assets.currency.currencydetail.present;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.CurrencyDetailsBean;
import com.taiyi.soul.moudles.mine.bean.SelectCoinRateBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class CurrencyDetailsPresent extends BasePresent<CurrencyDetailsView> {

    public void getCoinDetails(Activity activity,String coin_name){
        Map map = new HashMap<>();
        map.put("coinName",coin_name);
        HttpUtils.getRequets(BaseUrl.PROFITDETAILS, this, map, new JsonCallback<BaseResultBean<List<CurrencyDetailsBean>>>() {
            @Override
            public BaseResultBean<List<CurrencyDetailsBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<CurrencyDetailsBean>>> response) {
                super.onSuccess(response);
                if(null!=view){
                    if(response.body().status==602 || response.body().status == 3080005){
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                    }else {
                        view.getSuccess(response.body().status,response.body().msg,response.body().data);
                    }
                }


            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<CurrencyDetailsBean>>> response) {
                super.onError(response);
            }
        });
    }

    /**
     * 获取余额
     */
    public void getBalance(String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        }else  if(symbol.equals("USDS")){
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }else  if(symbol.equals("HASH")){
            hashMap.put("code", Constants.HASH_CONTRACT_ADDRESS);

        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account", Utils.getSpUtils().getString("mainAccount"));

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if(null!=view) {
                            if (response.body().size() > 0) {
                                view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                            } else {
                                view.getBalanceSuccess(symbol, "0.0000");
                            }
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if(null!=view) {
                            view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                });

    }

    public void getSelectCoinRate() {
        HttpUtils.getRequets(BaseUrl.GET_SELECT_COIN_RATE, this, null, new JsonCallback<BaseResultBean<List<SelectCoinRateBean>>>() {
            @Override
            public BaseResultBean<List<SelectCoinRateBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<SelectCoinRateBean>>> response) {
                super.onSuccess(response);
                if(null!=view) {
                    if (response.body().status == 200) {
                        view.getSelectCoinRateSuccess(response.body().data);
                    } else {
                        view.getSelectCoinRateFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<SelectCoinRateBean>>> response) {
                super.onError(response);
            }
        });
    }
    public void getCurrencyList(Activity activity){
        HttpUtils.getRequets(BaseUrl.GET_CURRENCY_LIST, this, null, new JsonCallback<BaseResultBean<List<CurrencyListBean>>>() {
            @Override
            public BaseResultBean<List<CurrencyListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<CurrencyListBean>>> response) {
                super.onSuccess(response);
                if(null!=view) {
                    if (response.body().status == 200) {
                        view.getListSuccess(response.body().data);
                    } else if (response.body().status == 602||response.body().status == 3080005) {//登录失效
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
//                       //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }


                    } else {
                        view.onFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<CurrencyListBean>>> response) {
                super.onError(response);
            }
        });
    }
    public void getCoinList() {
        HttpUtils.getRequets(BaseUrl.GET_COIN_LIST, this, null, new JsonCallback<BaseResultBean<List<CoinListBean>>>() {
            @Override
            public BaseResultBean<List<CoinListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<CoinListBean>>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getCoinListSuccess(response.body().data);
                    } else {
                        view.onFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<CoinListBean>>> response) {
                super.onError(response);
            }
        });
    }

}
