package com.taiyi.soul.moudles.main.home.bean;

public class ActivityFirstBean {

    /**
     * community_level : 0
     * hash_num : 10000
     * viplevel_num : 3000
     * stock_right_num : 0
     * hashlevel : 4
     * hash_flag : 0
     * ifname : 活动已结束
     * exchange_num : 0
     * langu : zh
     * ifopen : 2
     * id : 1
     * use_money : 0
     * surplus_time : 0
     * surplus_money : 3000
     * img_jump_url : http://114.115.131.170:8989/airdrop/#/
     * airdrop_status : true
     * charge : 0.8
     * person_num : 30
     * hash_allmoney : 0
     * end_time : 1596185644000
     * ngk_price : 5
     * copywrit_id : 1
     * father_num : 10
     * team_money : 0
     * user_money : 0
     * start_time : 1596081244000
     * copywriting : <p style="color:#D8DADD;font-size:14px;line-height:28px;padding-bottom:28px;" =""="">
     您同意本協議並註冊成功即成為商標圈的註冊用戶直至您的賬戶註銷。商標圈可能在必要的時候對本協議條款及商標圈各單項服務協議進行更改，此種更改在商標圈上公佈或在具體服務過程中經雙方以口頭、書面等形式協商一致生效。您可以選擇停止使用商標圈相關的服務或者註銷您在商標圈的賬戶，否則商標圈將認為您同意更改後的服務條款。未在商標圈發布或在具體服務過程中未經商標圈告知的服務條款將不具有法律效力，除非簽有書面協議，並且僅對簽署協議的當事人有效。 您應當擁有中華人民共和國法律認可的完全民事權利能力和完全民事行為能力，否則您和能夠代表您行使權利或承擔責任的其他主體將承擔一切後果。為此，您應當提供相應的證明。如果您是自然人，此類證明可以是您的居民身份證件、個人戶營業執照。如果您是法人，此類證明可以是您的營業執照。如果您是其他組織類型，您應當提供相應的合法證明。 如果使用商標圈提供的各單項服務，您可能還需要提交其他相關的資料和信息。為了提供更好的服務，您也可以向我們提供其他資料和信息，我們將按照商標圈的隱私政策保護您的資料和信息。 您應當保護好您的賬戶，除非已經得到您的提前通知，商標圈將認為您的賬戶處於您的控制之下。如果您的賬戶在不受您控制的情況下處於風險狀態或者已經發生損失，請您及時以有效方式通知商標圈，您可以要求商標圈暫停服務或者凍結賬戶。 如果您使用商標圈賬戶直接訪問其他網站，您應當遵守該網站的服務條款。他資料和信息，我們將按照商標圈的隱私政策保護您的資料和信息。 您應當保護好您的賬戶，除非已經得到您的提前通知，商標圈將認為您的賬戶處於您的控制之下。如果您的賬戶在不受您控制的情況下處於風險狀態或者已經發生損失，請您及時以有效方式通知商標圈，您可以要求商標圈暫停服務或者凍結賬戶。 如果您使用商標圈賬戶直接訪問其他網站，您應當遵守該網站的服務條款。
     </p>
     * team_num : 0
     * img_url : http://114.115.131.170:8888/group1/M00/00/03/wKgAp18rfsOAfXNrAALZoV7kxvY072.png
     * name : NGK公链电磁爆活动
     * all_money : 3000
     */

    private int community_level;
    private String hash_num;
    private String viplevel_num;
    private String stock_right_num;
    private String hashlevel;
    private String hash_flag;
    private String ifname;
    private String exchange_num;
    private String langu;
    private String ifopen;
    private int id;
    private String zong_time;
    private String use_money;
    private String surplus_time;
    private String surplus_money;
    private String img_jump_url;
    private boolean airdrop_status;
    private String charge;
    private String person_num;
    private String hash_allmoney;
    private long end_time;
    private String ngk_price;
    private int copywrit_id;
    private String father_num;
    private String team_money;
    private String user_money;
    private long start_time;
    private String copywriting;
    private String team_num;
    private String img_url;
    private String name;
    private String all_money;
    private String user_hash_num;
    private String user_all_money;
    private String user_surplus_money;

    public String getUser_surplus_money() {
        return user_surplus_money;
    }

    public void setUser_surplus_money(String user_surplus_money) {
        this.user_surplus_money = user_surplus_money;
    }

    public String getUser_all_money() {
        return user_all_money;
    }

    public void setUser_all_money(String user_all_money) {
        this.user_all_money = user_all_money;
    }

    public String getUser_hash_num() {
        return user_hash_num;
    }

    public void setUser_hash_num(String user_hash_num) {
        this.user_hash_num = user_hash_num;
    }

    public int getCommunity_level() {
        return community_level;
    }

    public void setCommunity_level(int community_level) {
        this.community_level = community_level;
    }

    public String getHash_num() {
        return hash_num;
    }

    public void setHash_num(String hash_num) {
        this.hash_num = hash_num;
    }

    public String getViplevel_num() {
        return viplevel_num;
    }

    public void setViplevel_num(String viplevel_num) {
        this.viplevel_num = viplevel_num;
    }

    public String getStock_right_num() {
        return stock_right_num;
    }

    public void setStock_right_num(String stock_right_num) {
        this.stock_right_num = stock_right_num;
    }

    public String getHashlevel() {
        return hashlevel;
    }

    public void setHashlevel(String hashlevel) {
        this.hashlevel = hashlevel;
    }

    public String getHash_flag() {
        return hash_flag;
    }

    public void setHash_flag(String hash_flag) {
        this.hash_flag = hash_flag;
    }

    public String getIfname() {
        return ifname;
    }

    public void setIfname(String ifname) {
        this.ifname = ifname;
    }

    public String getExchange_num() {
        return exchange_num;
    }

    public void setExchange_num(String exchange_num) {
        this.exchange_num = exchange_num;
    }

    public String getLangu() {
        return langu;
    }

    public void setLangu(String langu) {
        this.langu = langu;
    }

    public String getIfopen() {
        return ifopen;
    }

    public void setIfopen(String ifopen) {
        this.ifopen = ifopen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUse_money() {
        return use_money;
    }

    public void setUse_money(String use_money) {
        this.use_money = use_money;
    }

    public String getSurplus_time() {
        return surplus_time;
    }

    public void setSurplus_time(String surplus_time) {
        this.surplus_time = surplus_time;
    }

    public String getSurplus_money() {
        return surplus_money;
    }

    public void setSurplus_money(String surplus_money) {
        this.surplus_money = surplus_money;
    }

    public String getImg_jump_url() {
        return img_jump_url;
    }

    public void setImg_jump_url(String img_jump_url) {
        this.img_jump_url = img_jump_url;
    }

    public boolean isAirdrop_status() {
        return airdrop_status;
    }

    public void setAirdrop_status(boolean airdrop_status) {
        this.airdrop_status = airdrop_status;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getPerson_num() {
        return person_num;
    }

    public void setPerson_num(String person_num) {
        this.person_num = person_num;
    }

    public String getHash_allmoney() {
        return hash_allmoney;
    }

    public void setHash_allmoney(String hash_allmoney) {
        this.hash_allmoney = hash_allmoney;
    }

    public long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public String getNgk_price() {
        return ngk_price;
    }

    public void setNgk_price(String ngk_price) {
        this.ngk_price = ngk_price;
    }

    public int getCopywrit_id() {
        return copywrit_id;
    }

    public void setCopywrit_id(int copywrit_id) {
        this.copywrit_id = copywrit_id;
    }

    public String getFather_num() {
        return father_num;
    }

    public void setFather_num(String father_num) {
        this.father_num = father_num;
    }

    public String getTeam_money() {
        return team_money;
    }

    public void setTeam_money(String team_money) {
        this.team_money = team_money;
    }

    public String getUser_money() {
        return user_money;
    }

    public void setUser_money(String user_money) {
        this.user_money = user_money;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public String getCopywriting() {
        return copywriting;
    }

    public void setCopywriting(String copywriting) {
        this.copywriting = copywriting;
    }

    public String getTeam_num() {
        return team_num;
    }

    public void setTeam_num(String team_num) {
        this.team_num = team_num;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAll_money() {
        return all_money;
    }

    public void setAll_money(String all_money) {
        this.all_money = all_money;
    }

    public String getZong_time() {
        return zong_time;
    }

    public void setZong_time(String zong_time) {
        this.zong_time = zong_time;
    }
}
