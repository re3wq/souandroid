package com.taiyi.soul.moudles.login;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.register.bean.EmptyBean;
import com.taiyi.soul.moudles.register.bean.RegisterBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class LoginPresent extends BasePresent<LoginView> {
    public void login(String areaCode, String account, String pwd, String verifyCode) {
        String password= "";
        Map maps = new HashMap();
        maps.put("passWold", pwd);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areaCode", areaCode);
            jsonObject.put("accountMain", account);
            jsonObject.put("deviceId", Utils.getUniquePsuedoID());
            jsonObject.put("passWold", password);
            jsonObject.put("verifyCode", verifyCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.LOGIN, this, jsonObject.toString(), new JsonCallback<BaseResultBean<RegisterBean>>() {
            @Override
            public BaseResultBean<RegisterBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<RegisterBean>> response) {
                super.onSuccess(response);
                if (view != null) {
                    view.loginSuccess(response.body().status, response.body().msg, response.body().data);

                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<RegisterBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getFrequency(String mainAccount) {

        HashMap hashMap = new HashMap();
        hashMap.put("mainAccount", mainAccount);

        HttpUtils.getRequets(BaseUrl.LEFTNUM, this, hashMap, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (view != null) {
                    if (response.body().status == 200) {
                        view.getNumSuccess(response.body().data);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    public void getAreaCode() {
        HttpUtils.getRequets(BaseUrl.GET_AREA_CODE, this, null, new JsonCallback<BaseResultBean<List<AreaBean>>>() {
            @Override
            public BaseResultBean<List<AreaBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onSuccess(response);
                if (view != null) {
                    if (response.body().status == 200) {
                        view.getAreaCodeSuccess(response.body().data);
                    }
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<AreaBean>>> response) {
                super.onError(response);
            }
        });
    }

    public void getVerificationCode(String areaCode, String account) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("areaCode", areaCode);
            jsonObject.put("mainAccount", account);
            jsonObject.put("type", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.REGISTER_GET_VERIFICATION_CODE, this, jsonObject.toString(), new JsonCallback<BaseResultBean<EmptyBean>>() {
            @Override
            public BaseResultBean<EmptyBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getVerificationCodeSuccess();
                    } else {
                        view.getInvitePersonFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EmptyBean>> response) {
                super.onError(response);
            }
        });
    }

    public void push_transaction(String json) {

        HashMap hashMap = new HashMap();
        hashMap.put("json", json);

        HttpUtils.pushRequest(BaseUrl.PUSH_TRANSACTION, this, hashMap, new JsonCallback<String>() {
            @Override
            public String convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<String> response) {
                super.onSuccess(response);

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
            }
        });
    }
}
