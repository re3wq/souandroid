package com.taiyi.soul.moudles.main.node.cpubandwidth;

import android.animation.ValueAnimator;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.utils.BigDecimalUtil;
import com.taiyi.soul.utils.BytesUtlis;
import com.taiyi.soul.utils.DateUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.view.EditTextJudgeNumberWatcher;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CPUAndBandWidthActivity extends BaseActivity<CpuNetView, CupNetPresent> implements CpuNetView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.priceTv)
    TextView priceTv;
    @BindView(R.id.priceNetTv)
    TextView priceNetTv;
    @BindView(R.id.remainingTv)
    TextView remainingTv;
    @BindView(R.id.cpuProgressBar)
    ProgressBar cpuProgressBar;
    @BindView(R.id.cpuTotalMortgageAmountTv)
    TextView cpuTotalMortgageAmountTv;
    @BindView(R.id.cpuPersonMortgageAmountTv)
    TextView cpuPersonMortgageAmountTv;
    @BindView(R.id.cpuElseMortgageAmountTv)
    TextView cpuElseMortgageAmountTv;
    @BindView(R.id.bandWidthRemainingTv)
    TextView bandWidthRemainingTv;
    @BindView(R.id.bandWidthProgressBar)
    ProgressBar bandWidthProgressBar;
    @BindView(R.id.bandWidthTotalMortgageAmountTv)
    TextView bandWidthTotalMortgageAmountTv;
    @BindView(R.id.bandWidthPersonMortgageAmountTv)
    TextView bandWidthPersonMortgageAmountTv;
    @BindView(R.id.bandWidthElseMortgageAmountTv)
    TextView bandWidthElseMortgageAmountTv;
    @BindView(R.id.mortgageTv)
    TextView mortgageTv;
    @BindView(R.id.redemptionTv)
    TextView redemptionTv;
    @BindView(R.id.quantity)
    TextView quantity;
    @BindView(R.id.quantityEt)
    EditText quantityEt;
    @BindView(R.id.balanceTv)
    TextView balanceTv;
    @BindView(R.id.valueTv)
    TextView valueTv;
    @BindView(R.id.valueNetTv)
    TextView valueNetTv;
    @BindView(R.id.netMortgageQuantity)
    TextView netMortgageQuantity;
    @BindView(R.id.netMortgageQuantityEt)
    EditText netMortgageQuantityEt;
    @BindView(R.id.receiptAccount)
    TextView receiptAccount;
    @BindView(R.id.receiptAccountNameEt)
    EditText receiptAccountNameEt;
    @BindView(R.id.supportOtherAccountMortgageTv)
    TextView supportOtherAccountMortgageTv;
    @BindView(R.id.arriveIn72HoursTv)
    TextView arriveIn72HoursTv;
    @BindView(R.id.mortgageOrRedemptionTv)
    TextView mortgageOrRedemptionTv;
    @BindView(R.id.top)
    ConstraintLayout top;


    private String account = Utils.getSpUtils().getString("mainAccount");

    private String balance = "0.0000";
    private String cpu_price = "0.00";
    private String net_price = "0.00";


    private String type = "0"; //0---抵押 1---赎回
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_c_p_u_and_band_width;
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }


    @Override
    public CupNetPresent initPresenter() {
        return new CupNetPresent(this);
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText("CPU&" + getString(R.string.band_width));
        mortgageTv.setSelected(true);
        quantityEt.addTextChangedListener(new EditTextJudgeNumberWatcher(quantityEt, 4));
        netMortgageQuantityEt.addTextChangedListener(new EditTextJudgeNumberWatcher(netMortgageQuantityEt, 4));
        receiptAccountNameEt.setText(account);
    }

    @Override
    protected void initData() {
        showProgress();
        presenter.getAccountInfoData();
        presenter.getBalance();
    }

    private String passWord = "";

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    //显示密码框
    private void showPassword(View view) {
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInputp(viewById, ev,keyboardPlace)) {
                        hideSoftInputp(view.getWindowToken());
                    }
                }
                return false;
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            passWord = s;
                            pay_popup.dismiss();
                            showProgress();
                            presenter.checkPayPassword(s);
                        }
                    },500);

                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }
    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom +DensityUtil.dip2px(CPUAndBandWidthActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {

                pay_popup.dismiss();


            }
        }
    }

    @Override
    public void initEvent() {


        quantityEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s) && !s.toString().startsWith(".")) {

                    String kb = BigDecimalUtil.divide(new BigDecimal(quantityEt.getText().toString()), new BigDecimal(cpu_price), 4).toPlainString();
                    valueTv.setText("≈" + kb + " ms");


                } else {

                    valueTv.setText("≈0.0000 ms");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        netMortgageQuantityEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s) && !s.toString().startsWith(".")) {

                    String kb = BigDecimalUtil.divide(new BigDecimal(netMortgageQuantityEt.getText().toString()), new BigDecimal(net_price), 4).toPlainString();
                    valueNetTv.setText("≈" + kb + " KB");


                } else {

                    valueNetTv.setText("≈0.0000 KB");

                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


    }
    private  String cpu_amount ,net_amount,toFrom;

    @OnClick({R.id.backIv, R.id.mortgageTv, R.id.redemptionTv, R.id.mortgageOrRedemptionTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.mortgageTv:
                type = "0";
                mortgageTv.setSelected(true);
                redemptionTv.setSelected(false);
                supportOtherAccountMortgageTv.setVisibility(View.VISIBLE);
                arriveIn72HoursTv.setVisibility(View.GONE);
                mortgageOrRedemptionTv.setText(getString(R.string.mortgage));
                quantity.setText(getString(R.string.cpu_mortgage_quantity));
                netMortgageQuantity.setText(getString(R.string.net_mortgage_quantity));
                receiptAccount.setText(getString(R.string.receiving_account));
                quantityEt.setText("");
                netMortgageQuantityEt.setText("");
                break;
            case R.id.redemptionTv:
                type = "1";
                redemptionTv.setSelected(true);
                mortgageTv.setSelected(false);
                supportOtherAccountMortgageTv.setVisibility(View.GONE);
                arriveIn72HoursTv.setVisibility(View.VISIBLE);
                mortgageOrRedemptionTv.setText(getString(R.string.redemption));
                quantity.setText(getString(R.string.cpu_redemption_quantity));
                netMortgageQuantity.setText(getString(R.string.net_redemption_quantity));
                receiptAccount.setText(getString(R.string.from_account_redemption));
                quantityEt.setText("");
                netMortgageQuantityEt.setText("");
                break;
            case R.id.mortgageOrRedemptionTv:
                 cpu_amount = quantityEt.getText().toString();
                 net_amount = netMortgageQuantityEt.getText().toString();
                 toFrom = receiptAccountNameEt.getText().toString();

                if (TextUtils.isEmpty(toFrom)) {
                    toast(getString(R.string.edit_receiving_account));
                    return;
                }

                if (TextUtils.isEmpty(cpu_amount)) {
                    cpu_amount = "0";
                }

                if (TextUtils.isEmpty(net_amount)) {
                    net_amount = "0";
                }

                if (Double.parseDouble(net_amount) <= 0 && Double.parseDouble(cpu_amount) <= 0) {
                    toast(getString(R.string.edit_money));
                    return;
                }
                if (TextUtils.isEmpty(cpu_amount)) {
                    cpu_amount = "0.0000";
                }

                if (TextUtils.isEmpty(net_amount)) {
                    net_amount = "0.0000";
                }

                if (type.equals("0")) {
                    if (BigDecimalUtil.greaterThan(BigDecimalUtil.add(new BigDecimal(cpu_amount), new BigDecimal(net_amount)), new BigDecimal(balance))) {
                        toast(getString(R.string.toast_balance_lacking));
                        return;
                    }
                }

                showPassword(view);

//                showProgress();
//
//                if (type.equals("0")) {
//                    presenter.addStake(account,toFrom,cpu_amount,net_amount);
//                }else {
//                    presenter.unStake(account,toFrom,cpu_amount,net_amount);
//                }

                break;
        }
    }

    @Override
    public void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean) {


        if (blockChainAccountInfoBean != null) {

            BlockChainAccountInfoBean.SelfInfoBean selfInfoBean = blockChainAccountInfoBean.getSelf_delegated_bandwidth();
            String cpu_weight = "0 SOU";
            String net_weight = "0 SOU";
            if (selfInfoBean != null) {
                cpu_weight = selfInfoBean.getCpu_weight();
                net_weight = selfInfoBean.getNet_weight();
            }

            //net价格：
//            net_price = BigDecimalUtil.divide(
//                    new BigDecimal(blockChainAccountInfoBean.getNet_weight()),
//                    BigDecimalUtil.multiply(new BigDecimal(10), new BigDecimal(blockChainAccountInfoBean.getNet_limit().getMax())), 4).toPlainString();
            double netWeight =Double.parseDouble(blockChainAccountInfoBean.getNet_weight());
            double netMax =Double.parseDouble(blockChainAccountInfoBean.getNet_limit().getMax());
            double netv = (netWeight / 10000) / (netMax / 1024);
            net_price = new BigDecimal(netv +"").toPlainString();
            //net
            String netStr = BytesUtlis.getPrintSize(new BigDecimal(blockChainAccountInfoBean.getNet_limit().getAvailable())) + " / " + BytesUtlis.getPrintSize(new BigDecimal(blockChainAccountInfoBean.getNet_limit().getMax()));
            int progress2 = (int) (Double.parseDouble(blockChainAccountInfoBean.getNet_limit().getAvailable()) / Double.parseDouble(blockChainAccountInfoBean.getNet_limit().getMax()) * 100);


            //cup价格
            cpu_price = BigDecimalUtil.divide(
                    new BigDecimal(blockChainAccountInfoBean.getCpu_weight()),
                    BigDecimalUtil.multiply(new BigDecimal(10), new BigDecimal(blockChainAccountInfoBean.getCpu_limit().getMax())), 4).toPlainString();

            String cpuStr = DateUtils.formatTime(new BigDecimal(blockChainAccountInfoBean.getCpu_limit().getAvailable())) + " / " + DateUtils.formatTime(new BigDecimal(blockChainAccountInfoBean.getCpu_limit().getMax()));
            int progress1 = (int) (Double.parseDouble(blockChainAccountInfoBean.getCpu_limit().getAvailable()) / Double.parseDouble(blockChainAccountInfoBean.getCpu_limit().getMax()) * 100);


            if (Double.parseDouble(cpu_price) <= 0.0001){
                priceTv.setText(getString(R.string.mall_more_price) + new DecimalFormat("##0.0000").format(Double.parseDouble(cpu_price)*1000) + " SOU/s");
            }else {
                priceTv.setText(getString(R.string.mall_more_price) + new DecimalFormat("##0.0000").format(cpu_price) + " SOU/ms");
            }
            if (Double.parseDouble(net_price) <= 0.0001){
                priceNetTv.setText(getString(R.string.mall_more_price) + new DecimalFormat("##0.0000").format(Double.parseDouble(net_price)*1024) + " SOU/MB");
            }else {
                priceNetTv.setText(getString(R.string.mall_more_price) + new DecimalFormat("##0.0000").format(net_price) + " SOU/KB");
            }
//            priceNetTv.setText(getString(R.string.mall_more_price) + ":" + net_price + " BKO/KB");
//            priceTv.setText(getString(R.string.mall_more_price) + ":" + cpu_price + " BKO/KB");

            remainingTv.setText(getString(R.string.options_order_popup_remaining) +cpuStr);
            bandWidthRemainingTv.setText(getString(R.string.options_order_popup_remaining) +netStr);

            setAnimation(cpuProgressBar, progress1);
            setAnimation(bandWidthProgressBar, progress2);

            cpuTotalMortgageAmountTv.setText(blockChainAccountInfoBean.getTotal_resources().getCpu_weight().split(" ")[0]);
            cpuPersonMortgageAmountTv.setText(new DecimalFormat("##0.0000").format(new Double(cpu_weight.split(" ")[0])));
            cpuElseMortgageAmountTv.setText(BigDecimalUtil.minus(new BigDecimal(blockChainAccountInfoBean.getTotal_resources().getCpu_weight().split(" ")[0]), new BigDecimal(cpu_weight.split(" ")[0]), 4).toPlainString());

            bandWidthTotalMortgageAmountTv.setText(blockChainAccountInfoBean.getTotal_resources().getNet_weight().split(" ")[0]);
            bandWidthPersonMortgageAmountTv.setText(new DecimalFormat("##0.0000").format(new Double(net_weight.split(" ")[0])));
            bandWidthElseMortgageAmountTv.setText(BigDecimalUtil.minus(new BigDecimal(blockChainAccountInfoBean.getTotal_resources().getNet_weight().split(" ")[0]), new BigDecimal(net_weight.split(" ")[0]), 4).toPlainString());
        } else {
            priceTv.setText(getString(R.string.mall_more_price) + ":" + "0.0000 SOU/KB");
            priceNetTv.setText(getString(R.string.mall_more_price) + ":" + "0.0000 SOU/ms");
            remainingTv.setText(getString(R.string.options_order_popup_remaining) +"0.00 ms / 0.00 ms");
            bandWidthRemainingTv.setText(getString(R.string.options_order_popup_remaining) +"0.00 KB / 0.00 KB");
        }

        hideProgress();
    }

    @Override
    public void getDataHttpFail(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getBalanceSuccess(String balance) {
        this.balance = balance;
        balanceTv.setText(getString(R.string.balance) + " " + this.balance + " SOU");
        hideProgress();
    }

    @Override
    public void stakeSuccess() {
        toast(getString(R.string.successful_mortgage));
        presenter.getAccountInfoData();
        presenter.getBalance();

        quantityEt.setText("");
        netMortgageQuantityEt.setText("");
//        receiptAccountNameEt.setText("");
    }

    @Override
    public void unStackSuccess() {
        toast(getString(R.string.redemption_successful));
        presenter.getAccountInfoData();
        presenter.getBalance();

        quantityEt.setText("");
        netMortgageQuantityEt.setText("");
//        receiptAccountNameEt.setText("");
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg) {
        hideProgress();
        if (code == 0) {
            if (type.equals("0")) {
                if (BigDecimalUtil.greaterThan(BigDecimalUtil.add(new BigDecimal(cpu_amount), new BigDecimal(net_amount)), new BigDecimal(balance))) {
                    toast(getString(R.string.insufficient_balance));
                    return;
                }
            }

            showProgress();
            if (type.equals("0")) {
                presenter.addStake(account, toFrom, cpu_amount, net_amount);
            } else {
                presenter.unStake(account, toFrom, cpu_amount, net_amount);
            }
        } else {
            toast(msg);
        }
    }


    //进度条动画
    private void setAnimation(final ProgressBar view, final int mProgressBar) {
        ValueAnimator animator = ValueAnimator.ofInt(0, mProgressBar).setDuration(500);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                view.setProgress((int) valueAnimator.getAnimatedValue());
            }
        });
        animator.start();
    }

}
