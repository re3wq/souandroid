package com.taiyi.soul.moudles.mine.bean;

import java.util.List;

public class MyEarningsBean {

    /**
     * totalRevenue : 2464052.9684
     * mouthsum : 2408584.7252
     * daysum : 23469.2442
     * weeksum : [{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":null,"createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":"1312.12","datetime":"2020-06-29"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":null,"createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":"54156.1232","datetime":"2020-06-30"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":null,"createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":"23123.12","datetime":"2020-07-01"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":null,"createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":"1232.121","datetime":"2020-07-02"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":null,"createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":"45153.12","datetime":"2020-07-03"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":null,"createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":"161.12","datetime":"2020-07-04"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":null,"createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":"2315446","datetime":"2020-07-05"},{"id":null,"userId":null,"levelId":null,"coinName":null,"type":null,"money":null,"createTime":null,"state":null,"source":null,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":null,"ngkRatio":null,"num":null,"hash":null,"parentId":null,"moneysum":"23469.2442","datetime":"2020-07-06"}]
     */

    private String totalRevenue;
    private String mouthsum;
    private String daysum;
    private String convert;
    private List<WeeksumBean> weeksum;

    public String getConvert() {
        return convert;
    }

    public void setConvert(String convert) {
        this.convert = convert;
    }

    public String getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(String totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getMouthsum() {
        return mouthsum;
    }

    public void setMouthsum(String mouthsum) {
        this.mouthsum = mouthsum;
    }

    public String getDaysum() {
        return daysum;
    }

    public void setDaysum(String daysum) {
        this.daysum = daysum;
    }

    public List<WeeksumBean> getWeeksum() {
        return weeksum;
    }

    public void setWeeksum(List<WeeksumBean> weeksum) {
        this.weeksum = weeksum;
    }

    public static class WeeksumBean {
        /**
         * id : null
         * userId : null
         * levelId : null
         * coinName : null
         * type : null
         * money : null
         * createTime : null
         * state : null
         * source : null
         * directPushNum : null
         * originStaticIncomeId : null
         * flowId : null
         * comPowerRatio : null
         * ngkRatio : null
         * num : null
         * hash : null
         * parentId : null
         * moneysum : 1312.12
         * datetime : 2020-06-29
         */

        private Object id;
        private Object userId;
        private Object levelId;
        private Object coinName;
        private Object type;
        private Object money;
        private Object createTime;
        private Object state;
        private Object source;
        private Object directPushNum;
        private Object originStaticIncomeId;
        private Object flowId;
        private Object comPowerRatio;
        private Object ngkRatio;
        private Object num;
        private Object hash;
        private Object parentId;
        private String moneysum;
        private String datetime;

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public Object getLevelId() {
            return levelId;
        }

        public void setLevelId(Object levelId) {
            this.levelId = levelId;
        }

        public Object getCoinName() {
            return coinName;
        }

        public void setCoinName(Object coinName) {
            this.coinName = coinName;
        }

        public Object getType() {
            return type;
        }

        public void setType(Object type) {
            this.type = type;
        }

        public Object getMoney() {
            return money;
        }

        public void setMoney(Object money) {
            this.money = money;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Object getSource() {
            return source;
        }

        public void setSource(Object source) {
            this.source = source;
        }

        public Object getDirectPushNum() {
            return directPushNum;
        }

        public void setDirectPushNum(Object directPushNum) {
            this.directPushNum = directPushNum;
        }

        public Object getOriginStaticIncomeId() {
            return originStaticIncomeId;
        }

        public void setOriginStaticIncomeId(Object originStaticIncomeId) {
            this.originStaticIncomeId = originStaticIncomeId;
        }

        public Object getFlowId() {
            return flowId;
        }

        public void setFlowId(Object flowId) {
            this.flowId = flowId;
        }

        public Object getComPowerRatio() {
            return comPowerRatio;
        }

        public void setComPowerRatio(Object comPowerRatio) {
            this.comPowerRatio = comPowerRatio;
        }

        public Object getNgkRatio() {
            return ngkRatio;
        }

        public void setNgkRatio(Object ngkRatio) {
            this.ngkRatio = ngkRatio;
        }

        public Object getNum() {
            return num;
        }

        public void setNum(Object num) {
            this.num = num;
        }

        public Object getHash() {
            return hash;
        }

        public void setHash(Object hash) {
            this.hash = hash;
        }

        public Object getParentId() {
            return parentId;
        }

        public void setParentId(Object parentId) {
            this.parentId = parentId;
        }

        public String getMoneysum() {
            return moneysum;
        }

        public void setMoneysum(String moneysum) {
            this.moneysum = moneysum;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }
    }
}
