package com.taiyi.soul.moudles.main.home.mall.goods.fragment;

import android.os.Bundle;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.home.mall.bean.DemoMallHotBean;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 * Created by Android Studio.
 * @Date ： 2020\6\17 0017 10:34
 * @Author : yuan
 * @Describe ：商城-vip
 */
public class MallFourFragment extends BaseFragment<NormalView, NormalPresenter> {
    @BindView(R.id.mall_more_recycle)
    RecyclerView recycle;

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        List<DemoMallHotBean> hotBeanList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            DemoMallHotBean bean = new DemoMallHotBean();
            bean.img = R.mipmap.aa_more_goods_demo;
            bean.name = "ReFa進口美容儀";
            bean.price = "360.00 USDS";
            hotBeanList.add(bean);
        }

//        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
//        recycle.setLayoutManager(manager);
//        mMallMoreRecycle.setAdapter(AdapterManger.getMoreListAdapter(MallMoreActivity.this, hotBeanList));
//        mOrderRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
//        MallFragmentAdapter orderDeliverAdapter = new MallFragmentAdapter(getContext(), hotBeanList);
//        orderDeliverAdapter.setAnimationEnable(true);
//        orderDeliverAdapter.setAnimationFirstOnly(false);
//        orderDeliverAdapter.setAdapterAnimation(new CustomAnimation1());
//        recycle.setAdapter(orderDeliverAdapter);
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_mall;
    }

}
