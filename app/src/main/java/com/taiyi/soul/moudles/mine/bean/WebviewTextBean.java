package com.taiyi.soul.moudles.mine.bean;

public class WebviewTextBean {

    /**
     * id : 2
     * type : 2
     * langu : zh
     * content :  本《平台操作手册》仅适用于对应培训平台版本，如有平台功能升级，功能增减，将会及时通知学员，同时更新《平台操作手册》，如使用过程中存在问题，或者发现《平台操作手册》存在错误与不清楚的地方，请致电我院客服，感谢您的配合！


     服务热线：400-811-9908
     */

    private int id;
    private int type;
    private String langu;
    private String content;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLangu() {
        return langu;
    }

    public void setLangu(String langu) {
        this.langu = langu;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
