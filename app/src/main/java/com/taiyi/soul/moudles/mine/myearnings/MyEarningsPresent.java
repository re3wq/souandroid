package com.taiyi.soul.moudles.mine.myearnings;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.mine.bean.MyEarningsBean;
import com.taiyi.soul.moudles.mine.bean.MyEarningsListBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class MyEarningsPresent extends BasePresent<MyEarningsView> {
    public void getList(int pageNo, String createTime,String source) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNum", pageNo);
            jsonObject.put("pageSize", 10);
            jsonObject.put("source", source);
            if (!createTime.equals(""))
            jsonObject.put("createTime",createTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpUtils.postRequest(BaseUrl.MY_EARNING_LIST, this, jsonObject.toString(), new JsonCallback<BaseResultBean<MyEarningsListBean>>() {
            @Override
            public BaseResultBean<MyEarningsListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<MyEarningsListBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getListSuccess(response.body().data);
                    } else if (response.body().status == 602) {
                        view.loginInvalid(response.body().msg);
                    } else {
                        view.getListFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<MyEarningsListBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getMyEarnings(String source) {
        Map<String, String> map = new HashMap<>();
        map.put("source",source);
        HttpUtils.getRequets(BaseUrl.GET_MY_EARNINGS, this, map, new JsonCallback<BaseResultBean<MyEarningsBean>>() {
            @Override
            public BaseResultBean<MyEarningsBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<MyEarningsBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getEarningsSuccess(response.body().data);
                    } else if (response.body().status == 602) {
                        view.loginInvalid(response.body().msg);
                    } else {
                        view.getEarningsFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<MyEarningsBean>> response) {
                super.onError(response);
            }
        });
    }
}
