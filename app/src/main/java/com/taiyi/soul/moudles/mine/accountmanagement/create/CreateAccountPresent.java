package com.taiyi.soul.moudles.mine.accountmanagement.create;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;

import java.util.HashMap;

import okhttp3.Response;

public class CreateAccountPresent extends BasePresent<CreateAccountView> {
    public void getInvitePerson(String inviteCode) {
        HttpUtils.getRequets(BaseUrl.INVITE_PERSON_FROM_INVITE_CODE + inviteCode, this, null, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.getInvitePersonSuccess(response.body().data);
                    } else {
                        view.getInvitePersonFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    /**
     * 账户是否可用(随机生成)
     * @param eosAccountName
     */
    public void checkAccountUseableData(String eosAccountName) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", eosAccountName + Constants.ACCOUNT_SUFFIX);

        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_CHIAN_ACCOUNT_INFO, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<BlockChainAccountInfoBean>() {
                    @Override
                    public BlockChainAccountInfoBean convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                        super.onSuccess(response);
                        if (null!=view) {
                            view.checkAccountUseable(false);
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                        super.onError(response);
                        if (null!=view) {
                            view.checkAccountUseable(true);
                        }
                    }
                });

    }
    /**
     * 账户是否可用(手动输入)
     * @param eosAccountName
     */
    public void checkAccountUseableDataInput(String eosAccountName) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", eosAccountName + Constants.ACCOUNT_SUFFIX);

        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_CHIAN_ACCOUNT_INFO, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<BlockChainAccountInfoBean>() {
                    @Override
                    public BlockChainAccountInfoBean convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                        super.onSuccess(response);
                        if (null!=view) {
                        view.checkAccountUseableInput(false);}
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<BlockChainAccountInfoBean> response) {
                        super.onError(response);
                        if (null!=view) {
                        view.checkAccountUseableInput(true);}
                    }
                });

    }

}
