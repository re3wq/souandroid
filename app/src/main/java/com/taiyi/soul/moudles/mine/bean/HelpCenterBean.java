package com.taiyi.soul.moudles.mine.bean;

public class HelpCenterBean {
    /**
     * id : 1
     * title : a
     * url : www.123
     * createTime : 2020-07-11 15:40:31
     */

    private int id;
    private String title;
    private String url;
    private String createTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

}
