package com.taiyi.soul.moudles.mine.accountmanagement.importaccount;

import com.taiyi.soul.moudles.mine.bean.AccountListBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;

import java.util.ArrayList;
import java.util.List;

public interface ImportAccountView {

    void getBlockchainAccountInfoDataHttp(BlockChainAccountInfoBean blockChainAccountInfoBean);


    void getAccountListSuccess(List<AccountListBean> list);

    void getDataHttpFail(String msg);


    void getAccountHttp(ArrayList<String> account_names);
}
