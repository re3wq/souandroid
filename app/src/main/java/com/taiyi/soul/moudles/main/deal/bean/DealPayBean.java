package com.taiyi.soul.moudles.main.deal.bean;

/**
 * Created by Android Studio.
 * @author : yuan
 * @Date on 2020\5\13 0013
 */

public class DealPayBean {
    public String names;
    public String total;
    public String num;
    public String price;
    public String total_type;
    public String num_type;
    public String price_type;
    public String charge;
}
