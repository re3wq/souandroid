package com.taiyi.soul.moudles.main.assets.exchange;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import butterknife.BindView;
import butterknife.OnClick;

public class ExchangeSuccessActivity extends BaseActivity<NormalView, NormalPresenter> {


    @BindView(R.id.tv_title)
    TextView mTvTitle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exchange_success;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mTvTitle.setText(getResources().getString(R.string.exchanges_title));
    }

    @Override
    public void initEvent() {

    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }


    @OnClick({R.id.iv_back, R.id.accomplish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
            case R.id.accomplish:
                finish();
                break;
        }
    }
}
