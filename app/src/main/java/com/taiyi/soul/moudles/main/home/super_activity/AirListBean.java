package com.taiyi.soul.moudles.main.home.super_activity;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\8\6 0006 17:54
 * @Author : yuan
 * @Describe ：
 */
class AirListBean {

        public int totalpage;
        public int rowcount;
        public List<ComlistBean> comlist;

        public static class ComlistBean {
            public String createtime;
            public String charge;
            public long create_time;
            public String num;
            public int source;
            public boolean type;
            public String coin_name;
            public String money;
            public long user_id;
            public String price;
            public long id;
            public int state;
            public String hash;
            public String ifcharge;
    }
}
