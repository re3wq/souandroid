package com.taiyi.soul.moudles.main.node.bean;

import java.util.List;

public class ComputerPurchaseRecordBean {


    /**
     * total : 1
     * list : [{"id":15941181060018494,"userId":15941128124283836,"levelId":0,"coinName":"USDK","type":false,"money":"2.499882","createTime":"2020-07-07 18:35:06","state":0,"source":7,"directPushNum":null,"originStaticIncomeId":null,"flowId":null,"comPowerRatio":"10.8220","ngkRatio":"0.2450","num":"0.231","hash":"123","parentId":null}]
     */

    private int total;
    private List<ListBean> list;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 15941181060018494
         * userId : 15941128124283836
         * levelId : 0
         * coinName : USDN
         * type : false
         * money : 2.499882
         * createTime : 2020-07-07 18:35:06
         * state : 0
         * source : 7
         * directPushNum : null
         * originStaticIncomeId : null
         * flowId : null
         * comPowerRatio : 10.8220
         * ngkRatio : 0.2450
         * num : 0.231
         * hash : 123
         * parentId : null
         */

        private long id;
        private long userId;
        private int levelId;
        private String coinName;
        private boolean type;
        private String money;
        private String createTime;
        private int state;
        private int source;
        private Object directPushNum;
        private Object originStaticIncomeId;
        private Object flowId;
        private String comPowerRatio;
        private String ngkRatio;
        private String num;
        private String hash;
        private Object parentId;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getUserId() {
            return userId;
        }

        public void setUserId(long userId) {
            this.userId = userId;
        }

        public int getLevelId() {
            return levelId;
        }

        public void setLevelId(int levelId) {
            this.levelId = levelId;
        }

        public String getCoinName() {
            return coinName;
        }

        public void setCoinName(String coinName) {
            this.coinName = coinName;
        }

        public boolean isType() {
            return type;
        }

        public void setType(boolean type) {
            this.type = type;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public int getSource() {
            return source;
        }

        public void setSource(int source) {
            this.source = source;
        }

        public Object getDirectPushNum() {
            return directPushNum;
        }

        public void setDirectPushNum(Object directPushNum) {
            this.directPushNum = directPushNum;
        }

        public Object getOriginStaticIncomeId() {
            return originStaticIncomeId;
        }

        public void setOriginStaticIncomeId(Object originStaticIncomeId) {
            this.originStaticIncomeId = originStaticIncomeId;
        }

        public Object getFlowId() {
            return flowId;
        }

        public void setFlowId(Object flowId) {
            this.flowId = flowId;
        }

        public String getComPowerRatio() {
            return comPowerRatio;
        }

        public void setComPowerRatio(String comPowerRatio) {
            this.comPowerRatio = comPowerRatio;
        }

        public String getNgkRatio() {
            return ngkRatio;
        }

        public void setNgkRatio(String ngkRatio) {
            this.ngkRatio = ngkRatio;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public Object getParentId() {
            return parentId;
        }

        public void setParentId(Object parentId) {
            this.parentId = parentId;
        }
    }
}
