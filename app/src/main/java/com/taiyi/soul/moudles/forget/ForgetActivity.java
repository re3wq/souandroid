package com.taiyi.soul.moudles.forget;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.moudles.login.bean.AreaCodeBean;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.view.AreaCodePopupWindow;
import com.taiyi.soul.view.countdowntimer.CountDownTimerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgetActivity extends BaseActivity<ForgetView, ForgetPresent> implements AreaCodePopupWindow.OnItemClick, ForgetView {

    @BindView(R.id.iv_back)
    ImageView iv_back;

    @BindView(R.id.tv_code)
    TextView tvCode;

    @BindView(R.id.iv)
    ImageView iv;

    @BindView(R.id.ll_code)
    LinearLayout llCode;

    @BindView(R.id.iv_email)
    ImageView ivEmail;

    @BindView(R.id.iv_phone)
    ImageView ivPhone;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.et_sms_code)
    EditText et_sms_code;

    @BindView(R.id.tv_send_code)
    TextView tv_send_code;


    @BindView(R.id.ll_phone)
    LinearLayout llPhone;

    @BindView(R.id.ll_hide)
    LinearLayout llHide;
    Handler handler = new Handler();
    Runnable runnable;

    /**
     * 0---手机号修改密码
     * 1---邮箱修改密码
     */
    private int loginType = 0;


    private ArrayList<AreaCodeBean> list = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_forget;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        initView();
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (runnable != null) {
                    handler.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (s.toString().equals("")) {
                            return;
                        }
                        String loginPwd = s.toString();

                            if (loginPwd.length() < 8) {//位数不足八位
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint1));
//                            toast("登录密码不足8位");
                            } else if (loginPwd.matches("^[0-9]+$")) {//输入的纯数字为弱
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                            } else if (loginPwd.matches("^[a-z]+$")) {//输入的纯小写字母为弱
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                            } else if (loginPwd.matches("^[A-Z]+$")) {//输入的纯大写字母为弱
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint2));
//                            toast("密码安全系数较弱");
                            } else if (loginPwd.matches("^[A-Za-z]+$")) {//输入的大写字母和小写字母为中
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                            } else if (loginPwd.matches("^[a-z0-9]+$")) {//输入的小写字母和数字为中
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                            } else if (loginPwd.matches("^[A-Z0-9]+$")) {//输入的大写字母和数字为中
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint3));
//                            toast("密码安全系数为中级");
                            } else if (loginPwd.matches("^[A-Za-z0-9]+$")) {//输入的大写字母和小写字母和数字为强
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint4));
//                            toast("密码安全系数为高级");
                            } else {//其他视为高强度
                                AlertDialogShowUtil.toastMessage(ForgetActivity.this, getResources().getString(R.string.password_hint4));
//                            toast("密码安全系数为高级");
                            }




                    }
                };
                handler.postDelayed(runnable, 1000);

            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                }
            }
        });
//        list.add(new AreaCodeBean("", "+86"));
//        list.add(new AreaCodeBean("", "+86"));
//        list.add(new AreaCodeBean("", "+86"));
//        list.add(new AreaCodeBean("", "+86"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        list.add(new AreaCodeBean("", "+8686"));
//        tvCode.setText(list.get(0).getCode());


    }

    @Override
    protected void initData() {
presenter.getAreaCode();
    }

    @Override
    public void initEvent() {

    }


    @Override
    public ForgetPresent initPresenter() {
        return new ForgetPresent();
    }


    @OnClick({R.id.tv_code, R.id.ll_code, R.id.ll_email, R.id.ll_phone1, R.id.tv_send_code, R.id.tv_confirm, R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_code:
            case R.id.ll_code:
                AreaCodePopupWindow popupWindow = new AreaCodePopupWindow(this, list, this);
                popupWindow.showAsDropDown(llCode, 0, DensityUtil.dip2px(this, -5));

                break;

            case R.id.ll_email:
                etEmail.setText("");
                et_sms_code.setText("");
                etPassword.setText("");
                loginType = 1;
                initView();
                break;

            case R.id.ll_phone1:
                etPhone.setText("");
                et_sms_code.setText("");
                etPassword.setText("");
                loginType = 0;
                initView();
                break;

            case R.id.tv_send_code:
                if (loginType == 1) {//邮箱
                    String email = etEmail.getText().toString();
                    if (TextUtils.isEmpty(email)) {
                        toast(getString(R.string.email_input));
                        return;
                    }
                    if (emailFormat(email)) {
                        showProgress();
                        presenter.getVerificationCode("",email);
                    }else {
                        toast(getString(R.string.email_err));
                    }
                } else {//手机号
                    String phoneNumber = etPhone.getText().toString();
                    if (TextUtils.isEmpty(phoneNumber)) {
                        toast(getString(R.string.phone_input));
                        return;
                    }
                    showProgress();
                    presenter.getVerificationCode(tvCode.getText().toString(),phoneNumber);
                }
                break;
            case R.id.tv_confirm:
                if (loginType == 1) {//邮箱
                    String email = etEmail.getText().toString();
                    if (TextUtils.isEmpty(email)) {
                        toast(getString(R.string.email_input));
                        return;
                    }
                    if (!emailFormat(email)) {
                       toast(getString(R.string.email_err));
                       return;
                    }
                    String smsCode = et_sms_code.getText().toString();
                    if (TextUtils.isEmpty(smsCode)) {
                        toast(getString(R.string.input_code));
                        return;
                    }

                    String pwd = etPassword.getText().toString();
                    if (TextUtils.isEmpty(pwd)) {
                        toast(getString(R.string.password_input));
                        return;
                    }
                    if (etPassword.getText().toString().length() < 8) {
                        toast(getResources().getString(R.string.password_hint1));
                        return;
                    }
                    showProgress();
                    presenter.updatePwd("",email, pwd, smsCode);
                } else {//手机号
                    String phoneNumber = etPhone.getText().toString();
                    if (TextUtils.isEmpty(phoneNumber)) {
                        toast(getString(R.string.phone_input));
                        return;
                    }
                    String smsCode = et_sms_code.getText().toString();
                    if (TextUtils.isEmpty(smsCode)) {
                        toast(getString(R.string.input_code));
                        return;
                    }

                    String pwd = etPassword.getText().toString();
                    if (TextUtils.isEmpty(pwd)) {
                        toast(getString(R.string.password_input));
                        return;
                    }
                    if (etPassword.getText().toString().length() < 8) {
                        toast(getResources().getString(R.string.password_hint1));
                        return;
                    }
                    showProgress();
                    presenter.updatePwd(tvCode.getText().toString(),phoneNumber, pwd, smsCode);
                }
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }

    private boolean emailFormat(String email) {//邮箱判断正则表达式
        Pattern pattern = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
        Matcher mc = pattern.matcher(email);
        return mc.matches();
    }


    @Override
    public void onItemClick(int position) {
        tvCode.setText(list.get(position).getCode());
        Glide.with(this).load(list.get(position).getIcon()).into(iv);
    }

    @Override
    public void onHide(boolean isHide) {

    }


    private void initView() {
        if (loginType == 0) {
            llPhone.setVisibility(View.VISIBLE);
            etEmail.setVisibility(View.GONE);
            ivEmail.setImageResource(R.mipmap.iv_login_email);
            ivPhone.setImageResource(R.mipmap.iv_login_phone);
        } else {
            llPhone.setVisibility(View.GONE);
            etEmail.setVisibility(View.VISIBLE);
            ivEmail.setImageResource(R.mipmap.iv_login_email_ed);
            ivPhone.setImageResource(R.mipmap.iv_login_phone_un);
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(iv_back).init();
    }

    @Override
    public void getVerificationCodeSuccess() {
        hideProgress();
        CountDownTimerUtils countDownTimerUtils = new CountDownTimerUtils(this, tv_send_code, 60000, 1000, "#a5a9ac");
        countDownTimerUtils.start();

        tv_send_code.setFocusable(true);
        tv_send_code.setFocusableInTouchMode(true);
        tv_send_code.requestFocus();
    }

    @Override
    public void getInvitePersonFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void updateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void updateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getAreaCodeSuccess(List<AreaBean> mList) {
        for (AreaBean areaBean : mList) {
            list.add(new AreaCodeBean(areaBean.getUrl(), areaBean.getArea()));
        }
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getArea().equals("+1")) {
                tvCode.setText(list.get(i).getCode());
                Glide.with(this).load(list.get(i).getIcon()).into(iv);
            }
        }
//        tvCode.setText(list.get(0).getCode());
//        Glide.with(this).load(list.get(0).getIcon()).into(iv);
    }
}
