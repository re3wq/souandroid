package com.taiyi.soul.moudles.main.home.latestnews;

import com.taiyi.soul.moudles.main.home.bean.NewsListBean;

public interface LatestNewsView {
    void onSuccess(NewsListBean newsListBean);
    void onFailure(String errorMsg);
    void updateSuccess();
    void updateFailure(String errorMsg);
}
