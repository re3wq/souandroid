package com.taiyi.soul.moudles.main.home.super_activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.MyCircleProcessView;
import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

public class SuperAcActivity extends BaseActivity<SuperAcView, SuperAcPresent> implements SuperAcView {

    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_type)
    TextView tv_type;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.totalTv)
    TextView totalTv;
    @BindView(R.id.teamNumTv)
    TextView teamNumTv;
    @BindView(R.id.winningRateTv)
    TextView winningRateTv;
    @BindView(R.id.computingPowerValueTv)
    NumberRollingView computingPowerValueTv;
    @BindView(R.id.computingPowerTv)
    NumberRollingView computingPowerTv;
    @BindView(R.id.myAssetsTv)
    NumberRollingView myAssetsTv;
    @BindView(R.id.assetsValueTv)
    NumberRollingView assetsValueTv;
    @BindView(R.id.countView_price)
    NumberRollingView countView_price;
    @BindView(R.id.countView_price_bond)
    NumberRollingView countView_price_bond;
    @BindView(R.id.cv1)
    NumberRollingView cv1;
    @BindView(R.id.cv2)
    NumberRollingView cv2;
    @BindView(R.id.progress_day)
    MyCircleProcessView mProgressView1;
    @BindView(R.id.progress_hour)
    MyCircleProcessView mProgressView2;
    @BindView(R.id.progress_minutes)
    MyCircleProcessView mProgressView3;
    @BindView(R.id.progress_second)
    MyCircleProcessView mProgressView4;
    @BindView(R.id.text_second)
    TextView text_second;
    @BindView(R.id.text_minutes)
    TextView text_minutes;
    @BindView(R.id.text_hour)
    TextView text_hour;
    @BindView(R.id.text_day)
    TextView text_day;
    @BindView(R.id.image)
    ImageView image;
    private String img_jump_url;
    private String mIfopen;
    private String mSurplus_money = "0";
    private long mDay;// 天
    private long mHour;//小时,
    private long mMin;//分钟,
    private long mSecond;//秒
    private String ngkPrice = "0";
    private int second = 0;
    private int minus = 0;
    private int hour = 0;
    private int day = 0;
    Timer timer = new Timer();
    Timer timer1 = new Timer();
    Timer timer2 = new Timer();
    Timer timer3 = new Timer();
    int star = 1;
    @BindView(R.id.img_right)//
            ImageView img_right;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_super_ac;
    }

    @Override
    public SuperAcPresent initPresenter() {
        return new SuperAcPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        mTvTitle.setText(getString(R.string.activity_title));
        img_right.setVisibility(View.VISIBLE);
        Glide.with(SuperAcActivity.this).load(R.mipmap.icon_ac_file).into(img_right);
    }

    @Override
    protected void initData() {


    }

    @Override
    public void initEvent() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgress();
        timer = new Timer();
        timer1 = new Timer();
        timer2 = new Timer();
        timer3 = new Timer();
        presenter.getFirstData();
    }

    @OnClick({R.id.iv_back, R.id.super_ac_buy, R.id.image, R.id.img_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.super_ac_buy:
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.super_ac_buy);
                if (!fastDoubleClick) {
                    Bundle bundles = new Bundle();
                    bundles.putString("ngkPrice", ngkPrice);
                    bundles.putString("surplus_money", mSurplus_money);
                    bundles.putString("mIfopen", mIfopen);
                    ActivityUtils.next(this, SuperAcBuyActivity.class, bundles, false);
                }

                break;
            case R.id.img_right:
                fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.img_right);
                if (!fastDoubleClick) {
                    Bundle bundle = new Bundle();
                    bundle.putString("url", img_jump_url);
                    bundle.putInt("type", 8);
//                ActivityUtils.next(this, WebViewActivity.class,bundle);
                    ActivityUtils.next(this, com.taiyi.soul.moudles.mine.webview.WebViewActivity.class, bundle);
                }

                break;
        }
    }


    // 停止定时器
    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer1.cancel();
            timer2.cancel();
            timer3.cancel();
            // 一定设置为null，否则定时器不会被回收
            timer = null;
            timer1 = null;
            timer2 = null;
            timer3 = null;
        }
    }

    @Override
    public void activitySuccess(ActivityFirstBean activityFirstBean) {
        hideProgress();
        if (activityFirstBean.getIfopen().equals("2")) {//活动已结束
//            mProgressView1.setMax(0);
            mProgressView1.setProgress(0);
            mProgressView1.setSpeed(0);
//            mProgressView2.setMax(0);
            mProgressView2.setProgress(0);
            mProgressView2.setSpeed(0);
//            mProgressView3.setMax(0);
            mProgressView3.setProgress(0);
            mProgressView3.setSpeed(0);
//            mProgressView4.setMax(0);
            mProgressView4.setProgress(0);
            mProgressView4.setSpeed(0);
        } else if (activityFirstBean.getIfopen().equals("0")) {//活动未开始
            long millis = System.currentTimeMillis();
            long progressTime = activityFirstBean.getStart_time() - millis;
            formatTime(progressTime);
        } else if (activityFirstBean.getIfopen().equals("1")) {//活动进行中
            long millis = System.currentTimeMillis();
            long progressTime = activityFirstBean.getEnd_time() - millis;
            formatTime(progressTime);
        }

        img_jump_url = activityFirstBean.getImg_jump_url();
        Glide.with(this).load(activityFirstBean.getImg_url()).into(image);
//        computingPowerTv.setMoney(Float.parseFloat(activityFirstBean.getUser_hash_num()));
        computingPowerTv.setContent(new BigDecimal(activityFirstBean.getUser_hash_num()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        String hashlevel = activityFirstBean.getHashlevel();
        String hashLevel = "";
        switch (hashlevel) {
            case "0":
                hashLevel = "";
                break;
            case "1":
                hashLevel = getString(R.string.activity_ac_token_one);
                break;
            case "2":
                hashLevel = getString(R.string.activity_ac_token_two);
                break;
            case "3":
                hashLevel = getString(R.string.activity_ac_token_three);
                break;
            case "4":
                hashLevel = getString(R.string.activity_ac_token_four);
                break;
        }
        tv_type.setText(hashLevel);
//        computingPowerValueTv.setMoney(Float.parseFloat(activityFirstBean.getExchange_num()));
        computingPowerValueTv.setContent(new BigDecimal(activityFirstBean.getExchange_num()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        teamNumTv.setText(NumUtils.subZeroAndDot(activityFirstBean.getTeam_num()) + getString(R.string.activity_ac_person));
//        myAssetsTv.setMoney(Float.parseFloat(activityFirstBean.getTeam_money()));
        myAssetsTv.setContent(new BigDecimal(activityFirstBean.getTeam_money()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        tv1.setText(activityFirstBean.getIfname());
        if (activityFirstBean.getIfopen().equals("2")) {//活动已结束
            if (activityFirstBean.getHash_flag().equals("1")) {//中签
                winningRateTv.setText(getString(R.string.activity_ac_lot_winning_rate_yes));
            } else {//0未中签
                winningRateTv.setText(getString(R.string.activity_ac_lot_winning_rate_no));
            }
        } else {
//            winningRateTv.setText(getString(R.string.activity_ac_lot_winning_rate)+ (int) (Float.parseFloat(activityFirstBean.getCharge()) * 100) + "%");
        }

//        assetsValueTv.setMoney(Float.parseFloat(activityFirstBean.getHash_num()));
//        countView_price.setMoney(Float.parseFloat(activityFirstBean.getUser_money()));
//        countView_price_bond.setMoney(Float.parseFloat(activityFirstBean.getStock_right_num()));
        assetsValueTv.setContent(new BigDecimal(activityFirstBean.getHash_num()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
//        countView_price.setContent(new BigDecimal(activityFirstBean.getUser_money()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        countView_price.setContent(new BigDecimal(activityFirstBean.getStock_right_num()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        countView_price_bond.setContent(new BigDecimal(activityFirstBean.getStock_right_num()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        if (activityFirstBean.getCommunity_level() == 0)
            cv1.setText(getString(R.string.no));
        else
            cv1.setText(activityFirstBean.getCommunity_level() + "");
//        cv2.setMoney(Float.parseFloat(activityFirstBean.getViplevel_num()));
        cv2.setContent(new BigDecimal(activityFirstBean.getViplevel_num()).setScale(4, BigDecimal.ROUND_DOWN).toPlainString());
        String s = new BigDecimal(activityFirstBean.getUser_all_money()).setScale(4).toString();

        totalTv.setText(NumUtils.subZeroAndDot(s) + "SOU");
//        totalTv.setText(activityFirstBean.getUser_all_money() + "BKO");
        ngkPrice = activityFirstBean.getNgk_price();
        mSurplus_money = activityFirstBean.getUser_surplus_money();
        mIfopen = activityFirstBean.getIfopen();
    }


    @Override
    protected void onStop() {
        super.onStop();
        stopTimer();
    }

    /*
     * 毫秒转化时分秒毫秒
     */
    public void formatTime(Long ms) {
        Integer ss = 1000;
        Integer mi = ss * 60;
        Integer hh = mi * 60;
        Integer dd = hh * 24;

        int days = (int) (ms / dd);
        int hours = (int) (ms - days * dd) / hh;
        int minutes = (int) (ms - days * dd - hours * hh) / mi;
        int seconds = (int) (ms - days * dd - hours * hh - minutes * mi) / ss;

//        if (seconds <= 0) {
//            second = 0;
//            if (minutes <= 0) {
//                minus = 0;
//                if (hours <= 0) {
//                    hour = 0;
//                    if (days <= 0) {
//                        day = 0;
//                    } else {
//                        day = days - 1;
//                        hour = 23;
//                        minus = 60;
//                        second = 60;
//                    }
//                } else {
//                    hour = hours - 1;
//                    minus = 60;
//                    second = 60;
//                }
//            } else {
//                minus = minutes - 1;
//                second = 60;
//            }
//
//        } else {
//            if (minutes <= 0) {
//                minus = 0;
//                if (hours <= 0) {
//                    hour = 0;
//                    if (days <= 0) {
//                        day = 0;
//                    } else {
//                        day = days - 1;
//                        hour = 23;
//                        minus = 60;
//                    }
//                } else {
//                    hour = hours - 1;
//                    minus = 60;
//                }
//            } else {
//                if (hours <= 0) {
//                    hour = 0;
//                    if (days <= 0) {
//                        day = 0;
//                    } else {
//                        day = days - 1;
//                        hour = 23;
//                    }
//                } else {
//                    day = days;
//                    hour = hours;
//                }
//                minus = minutes;
//            }
//            second = seconds;
//        }
        if (seconds <= 0) {
            second = 0;
            if (minutes <= 0) {
                minus = 0;
                if (hours <= 0) {
                    hour = 0;
                    if (days <= 0) {
                        day = 0;
                    } else {
                        day = days;
                        hour =0;
                        minus = 0;
                        second =0;
                    }
                } else {
                    hour = hours;
                    minus = 0;
                    second =0;
                }
            } else {
                minus = minutes;
                second = 0;
            }

        } else {
            if (minutes <= 0) {
                minus = 0;
                if (hours <= 0) {
                    hour = 0;
                    if (days <= 0) {
                        day = 0;
                    } else {
                        day = days ;
                        hour = 0;
                        minus =0;
                    }
                } else {
                    hour = hours;
                    minus =0;
                }
            } else {
                if (hours <= 0) {
                    hour = 0;
                    if (days <= 0) {
                        day = 0;
                    } else {
                        day = days;
                        hour =0;
                    }
                } else {
                    day = days;
                    hour = hours;
                }
                minus = minutes;
            }
            second = seconds;
        }

        if(minus==60 && hour==0){
            hour++;
            minus=0;
        }


        if (minus == 0) {
            mProgressView3.setProgress(0);
        } else {
            mProgressView3.setProgress(minus);
        }
        if (second == 0) {
            mProgressView4.setProgress(0);
        } else {
            mProgressView4.setProgress(second);
        }
        if (hour == 0) {
            mProgressView2.setProgress(0);
        } else {
            mProgressView2.setProgress(hour);
        }
        if (day == 0) {
            mProgressView1.setProgress(0);
        } else {
            mProgressView1.setProgress(day);
        }

        text_minutes.setText(minus + "");
        mProgressView3.setMax(60);
        mProgressView3.setSpeed(0);


        text_hour.setText(hour + "");
        mProgressView2.setMax(24);

        mProgressView2.setSpeed(0);


        text_day.setText(day + "");
        mProgressView1.setMax(60);
        mProgressView1.setProgress(day);
        mProgressView1.setSpeed(0);


        text_second.setText(second + "");
        mProgressView4.setMax(60);
        mProgressView4.setSpeed(0);
        mProgressView4.setProgress(second);


        if(null==timer){
            return;
        }
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                if (second == 0) {//秒数递减至0，判断是否还有分钟数
                    if (minus != 0 || day != 0 || hour != 0) {
                        second = 60;
                        if (minus == 0) {//分钟递减至0，判断是否还有小时数
                            if (hour != 0 || day != 0) {
                                minus = 60;
                                if (hour == 0) {//分钟递减至0，判断是否还有小时数
                                    if (day != 0) {
                                        hour = 60;
                                        if (day >= 1) {
                                            day--;
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    text_day.setText(day + "");

                                                }
                                            });
                                            mProgressView1.setProgress(day);

                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    text_day.setText(0 + "");
                                                }
                                            });
                                            mProgressView1.setProgress(0);
                                        }
//                                        minus--;
//                                        second--;
//                                        hour--;

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                text_minutes.setText(minus + "");
                                                text_second.setText(second + "");
                                                text_hour.setText(hour + "");
                                            }
                                        });
                                        mProgressView2.setProgress(hour);
                                        mProgressView3.setProgress(minus);
                                        mProgressView4.setProgress(second);
                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                text_hour.setText(0 + "");
                                            }
                                        });
                                        mProgressView2.setProgress(0);
                                    }
                                } else {
                                    hour--;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            text_hour.setText(hour + "");
                                        }
                                    });
                                    mProgressView2.setProgress(hour);
                                }
                                minus--;
                                second--;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        text_minutes.setText(minus + "");
                                        text_second.setText(second + "");
                                    }
                                });
                                mProgressView3.setProgress(minus);
                                mProgressView4.setProgress(second);
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        text_minutes.setText(0 + "");
                                    }
                                });
                                mProgressView3.setProgress(0);
                            }
                        } else {
                            minus--;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    text_minutes.setText(minus + "");
                                }
                            });
                            mProgressView3.setProgress(minus);
                        }
                        second--;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                text_second.setText(second + "");
                            }
                        });
                        mProgressView4.setProgress(second);
                    } else {
                        star = 0;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                text_second.setText(0 + "");
                            }
                        });
                        mProgressView4.setProgress(0);
                        stopTimer();
                    }
                } else {
                    second--;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            text_second.setText(second + "");
                        }
                    });
                    mProgressView4.setProgress(second);
                }


            }
        }, 0, 1000);

    }

    @Override
    public void activityFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }
}
