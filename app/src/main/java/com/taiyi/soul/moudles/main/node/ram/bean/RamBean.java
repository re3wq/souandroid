package com.taiyi.soul.moudles.main.node.ram.bean;

import java.util.List;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/7/5
 * Time: 4:24 PM
 */
public class RamBean {


    /**
     * rows : [{"supply":"10000000000.0000 RAMCORE","base":{"balance":"68713400554 RAM","weight":"0.50000000000000000"},"quote":{"balance":"1000088.4293 NGK","weight":"0.50000000000000000"}}]
     * more : false
     * next_key :
     */

    private boolean more;
    private String next_key;
    private List<RowsBean> rows;

    public boolean isMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public String getNext_key() {
        return next_key;
    }

    public void setNext_key(String next_key) {
        this.next_key = next_key;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * supply : 10000000000.0000 RAMCORE
         * base : {"balance":"68713400554 RAM","weight":"0.50000000000000000"}
         * quote : {"balance":"1000088.4293 NGK","weight":"0.50000000000000000"}
         */

        private String supply;
        private BaseBean base;
        private QuoteBean quote;

        public String getSupply() {
            return supply;
        }

        public void setSupply(String supply) {
            this.supply = supply;
        }

        public BaseBean getBase() {
            return base;
        }

        public void setBase(BaseBean base) {
            this.base = base;
        }

        public QuoteBean getQuote() {
            return quote;
        }

        public void setQuote(QuoteBean quote) {
            this.quote = quote;
        }

        public static class BaseBean {
            /**
             * balance : 68713400554 RAM
             * weight : 0.50000000000000000
             */

            private String balance;
            private String weight;

            public String getBalance() {
                return balance != null ? balance.split(" ")[0] : "0";
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }

            public String getWeight() {
                return weight;
            }

            public void setWeight(String weight) {
                this.weight = weight;
            }
        }

        public static class QuoteBean {
            /**
             * balance : 1000088.4293 NGK
             * weight : 0.50000000000000000
             */

            private String balance;
            private String weight;

            public String getBalance() {
                return balance != null ? balance.split(" ")[0] : "0.0000";
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }

            public String getWeight() {
                return weight;
            }

            public void setWeight(String weight) {
                this.weight = weight;
            }
        }
    }
}
