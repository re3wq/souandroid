package com.taiyi.soul.moudles.main.assets.exchange.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\18 0018 14:07
 * @Author : yuan
 * @Describe ：
 */
public class CurrencyBalanceBean {

        public String balance;
        public List<ListBean> list;

        public static class ListBean {
            public String id;
            public String coinName;
            public String free;
            public boolean isDe;
        }

}
