package com.taiyi.soul.moudles.main.home.options.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：期权
 */
public class OptionsFragmentThreeAdapter extends BaseRecyclerAdapter<OptionListBean> {


    private OptionsItemClick optionsItemClick;

    private PullRecyclerView pullRecyclerView;
    private Context context;

    public OptionsFragmentThreeAdapter(Context context, OptionsItemClick optionsItemClick) {
        this.context = context;
        this.optionsItemClick = optionsItemClick;
    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_options;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, OptionListBean data) {

        ImageView view = getView(holder, R.id.currency_img);
        LinearLayout ll_options = getView(holder, R.id.ll_options);
        TextView currency_extent = getView(holder, R.id.currency_extent);
        TextView currency_name = getView(holder, R.id.currency_name);
        TextView currency_unit = getView(holder, R.id.currency_unit);
        TextView currency_hour_num = getView(holder, R.id.currency_hour_num);
        TextView currency_price = getView(holder, R.id.currency_price);
        TextView currency_num = getView(holder, R.id.currency_num);


//        DecimalFormat format = new DecimalFormat("0.0000");
//
//        double price = Double.parseDouble(data.price);
//        double base_volume_24h = Double.parseDouble(data.base_volume_24h);
//        double last = Double.parseDouble(data.last);

        if (data.instrument_id.equals("USDS") || data.instrument_id.equals("SOU")) {
            currency_num.setText(NumUtils.subZeroAndDot(new BigDecimal(data.last).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
            currency_price.setText(Utils.getSpUtils().getString("coin_symbol") + NumUtils.subZeroAndDot(new BigDecimal(data.price).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()) + "");
            currency_name.setText(data.instrument_id);
            Glide.with(context).load(data.img_url).into(view);
            currency_extent.setText("——");
            currency_hour_num.setText("——");
            currency_extent.setBackgroundResource(R.color.transparent);
            if (data.instrument_id.equals("USDS")) {
                currency_num.setTextColor(context.getResources().getColor(R.color.color_d8dadd));
                currency_unit.setText("/USD");
            }
        } else {
            currency_name.setText(data.instrument_id);
            currency_hour_num.setText(NumUtils.subZeroAndDot(new BigDecimal(data.base_volume_24h).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
            currency_num.setText(NumUtils.subZeroAndDot(new BigDecimal(data.last).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()));
            currency_price.setText(Utils.getSpUtils().getString("coin_symbol") + NumUtils.subZeroAndDot(new BigDecimal(data.price).setScale(4, BigDecimal.ROUND_DOWN).toPlainString()) + "");
            Glide.with(context).load(data.img_url).into(view);
            ll_options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionsItemClick.onItemClick(position);
                }
            });


            if (data.conversion.contains("-")) {
                currency_extent.setText(data.conversion + "%");
                currency_num.setTextColor(context.getResources().getColor(R.color.color_cco628));
                currency_extent.setBackgroundResource(R.mipmap.options_item_button_red);
            } else if (data.conversion.equals("0")) {
                currency_extent.setText(data.conversion + "%");
                currency_num.setTextColor(context.getResources().getColor(R.color.color_d8dadd));
                currency_extent.setBackgroundResource(R.mipmap.options_item_button_gray);
            } else {
                currency_extent.setText("+" + data.conversion + "%");
                currency_num.setTextColor(context.getResources().getColor(R.color.color_04a387));
                currency_extent.setBackgroundResource(R.mipmap.options_item_button_green);
            }

        }
    }
    //期权列表item点击事件
    public interface OptionsItemClick {
        void onItemClick(int position);
    }
}



