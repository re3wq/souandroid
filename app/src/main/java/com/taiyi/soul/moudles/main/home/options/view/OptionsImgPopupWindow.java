package com.taiyi.soul.moudles.main.home.options.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.moudles.main.home.mall.bean.DemoOptionsBean;
import com.taiyi.soul.utils.DensityUtil;

import java.util.ArrayList;

public class OptionsImgPopupWindow extends PopupWindow implements AdapterManger.BiListItemClick {

    private Context context;
    private View mView;
    private RecyclerView recycle;

    private ArrayList<DemoOptionsBean> list;
    private CommonAdapter mCommonAdapter;

    private OnItemClick onItemClick;

    public OptionsImgPopupWindow(Context context, ArrayList<DemoOptionsBean> list, OnItemClick onItemClick) {
        this.list = list;
        this.context = context;
        this.onItemClick = onItemClick;
        init();
    }



    private void init() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.popup_options_bi, null);
        recycle = mView.findViewById(R.id.recycle);


        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        layoutManager.setSmoothScrollbarEnabled(true);
        recycle.setLayoutManager(layoutManager);
        mCommonAdapter = AdapterManger.getOptionsBiListAdapter(context, list,this);
        recycle.setAdapter(mCommonAdapter);


        // 设置外部可点击
        this.setOutsideTouchable(false);

        /* 设置弹出窗口特征 */
        // 设置视图
        this.setContentView(this.mView);
        // 设置弹出窗体的宽和高
        this.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        this.setWidth(DensityUtil.dip2px(context,35));

        onItemClick.onHide(true);
//        // 产生背景变暗效果
//        WindowManager.LayoutParams lp = ((Activity)context).getWindow().getAttributes();
//        lp.alpha = 0.4f;
//        ((Activity)context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//        ((Activity)context).getWindow().setAttributes(lp);

        // 设置弹出窗体可点击
        this.setFocusable(true);

        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0x00000000);
        // 设置弹出窗体的背景
        this.setBackgroundDrawable(dw);

        // 设置弹出窗体显示时的动画，从底部向上弹出
        this.setAnimationStyle(R.style.shop_popup_window_anim);

        this.setOnDismissListener(new PopupWindow.OnDismissListener() {

            // 在dismiss中恢复透明度
            public void onDismiss() {
                onItemClick.onHide(false);
//                WindowManager.LayoutParams lp =  ((Activity)context).getWindow()
//                        .getAttributes();
//                lp.alpha = 1f;
//                ((Activity)context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//                ((Activity)context).getWindow().setAttributes(lp);
            }
        });


    }


    public void notifyData(ArrayList<DemoOptionsBean> list){
        this.list = list;
        mCommonAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBiItemClick(int position) {
        dismiss();
        onItemClick.onItemClick(position);
    }

    public interface OnItemClick{
        void  onItemClick(int position);
        void  onHide(boolean isHide);
    }

}
