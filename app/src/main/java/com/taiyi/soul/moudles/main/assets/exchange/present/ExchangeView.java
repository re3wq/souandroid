package com.taiyi.soul.moudles.main.assets.exchange.present;

import com.taiyi.soul.moudles.main.assets.exchange.bean.CurrencyBalanceBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRateBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeRecordBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;

public interface ExchangeView {
    void onSuccess(int code, String msg_cn, ExchangeBean bean);//兑换币种列表
    void onGetRateSuccess(int code, String msg_cn, ExchangeRateBean bean);//兑换数量、旷工费等
    void onGetExchangerSuccess(int code, String msg_cn, String bean);//兑换
    void onGetExchangerRecordSuccess(int code, String msg_cn, ExchangeRecordBean bean);//兑换记录
    void onIssueFeeSuccess(IssueFeeBean bean);
    void onCheckPayPasswordSuccess(int code, String msg_cn, String bean);//校验交易密码
    void getCurrencyBalanceSuccess(int code, String msg_cn, CurrencyBalanceBean balance);//查询币种对应余额
    void getBalanceSuccess(String symbol, String balance);//查询usdn和ngk余额
}
