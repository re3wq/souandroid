package com.taiyi.soul.moudles.main.home.options;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.options.bean.OptionsTabChangeBean;
import com.taiyi.soul.moudles.main.home.options.fragment.OptionsListOneFragment;
import com.taiyi.soul.moudles.main.home.options.fragment.OptionsListThreeFragment;
import com.taiyi.soul.moudles.main.home.options.fragment.OptionsListTwoFragment;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class OptionsListActivity extends BaseActivity<NormalView, NormalPresenter> {


    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_right_text)
    TextView mTvRightText;
    @BindView(R.id.tabLayout)
    SlidingTabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    private String[] mTitles;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;

    private int DESC = 0;
    private int DESC0 = 0;
    private int isFirst = 0;




    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_mall_more;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }


    @Override
    protected void initData() {
        mTvTitle.setText(getString(R.string.options_title));
        mTitles = new String[]{this.getResources().getString(R.string.options_name),
                this.getResources().getString(R.string.options_index),
                this.getResources().getString(R.string.options_hours)
        };

        mFragments.add(new OptionsListOneFragment());//币种名称
        mFragments.add(new OptionsListTwoFragment());//全球指数
        mFragments.add(new OptionsListThreeFragment());//24小时浮动


        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mTabLayout.post(new Runnable() {
            @Override
            public void run() {

                mTabLayout.setIsBackGround(true);
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.node_hash_market_sel_iv);
                mTabLayout.setDrawBitMap(bitmap, 126);
                mTabLayout.setViewPager(mViewPager);

                mViewPager.setCurrentItem(0);
                //默认加载第一页数据
                if (isFirst == 0) {
                    isFirst = 2;
                    DESC0 = 1;
                    DESC = 1;
                    EventBus.getDefault().post(new OptionsTabChangeBean(0, 1));
                    Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(0));
                }

                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                        ImageView titleIv = mTabLayout.getTitleIv(i);
                        titleIv.setVisibility(View.VISIBLE);
                }
            }
        });

        mTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {

                if (0 == position) {
                    if (DESC0 == 2) {
                        DESC0 = 1;
                        EventBus.getDefault().post(new OptionsTabChangeBean(position, 1));
                        Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(position));
                    } else if (DESC0 == 1) {
                        DESC0 = 2;
                        EventBus.getDefault().post(new OptionsTabChangeBean(position, 0));
                        Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_decline).into(mTabLayout.getTitleIv(position));
                    }

                } else {
                    DESC = 1;
                    DESC0 = 2;
                    for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                        if (position == i) {
                            EventBus.getDefault().post(new OptionsTabChangeBean(position, 1));
                            Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(i));
                        } else {

                            Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_nom).into(mTabLayout.getTitleIv(i));
                        }
                    }
                }


            }

            @Override
            public void onTabReselect(int position) {
                if(position!=0){
                    DESC0 = 2;
                    if (DESC == 2) {
                        DESC = 1;
                        EventBus.getDefault().post(new OptionsTabChangeBean(position, 1));
                        Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(position));
                    } else if (DESC == 1) {
                        DESC = 2;
                        EventBus.getDefault().post(new OptionsTabChangeBean(position, 0));
                        Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_decline).into(mTabLayout.getTitleIv(position));
                    }
                }else {
                    if (DESC0 == 2) {
                        DESC = 1;
                        DESC0 = 1;
                        EventBus.getDefault().post(new OptionsTabChangeBean(position, 1));
                        Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(position));
                    } else if (DESC0 == 1) {
                        DESC = 2;
                        EventBus.getDefault().post(new OptionsTabChangeBean(position, 0));
                        Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_decline).into(mTabLayout.getTitleIv(position));
                    }
                }


            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                DESC = 1;
                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                    if (i == position) {
                        Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_rise).into(mTabLayout.getTitleIv(i));
                    } else {
                        Glide.with(OptionsListActivity.this).load(R.mipmap.options_group_nom).into(mTabLayout.getTitleIv(i));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void initEvent() {

    }


    @OnClick({R.id.iv_back, R.id.img_car})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
