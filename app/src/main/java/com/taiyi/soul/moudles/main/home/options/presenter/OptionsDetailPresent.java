package com.taiyi.soul.moudles.main.home.options.presenter;

import android.app.Activity;

import com.alibaba.fastjson.JSONObject;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.exchange.bean.CurrencyBalanceBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.RedirectsBean;
import com.taiyi.soul.moudles.main.home.options.bean.KLainBean;
import com.taiyi.soul.moudles.main.home.options.bean.KOtherBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionDetailsListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionListBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionPopupBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.MD5Util;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class OptionsDetailPresent extends BasePresent<OptionsDetailView> {

    //记录
    public void getData(Activity activity, String PageNum, String type, String updown_id) {
        Map map = new HashMap();
        map.put("PageNum", PageNum);
        map.put("PageSize", "10");
        map.put("type", type); //0:持仓 1：历史
        map.put("updown_id", updown_id);//币种页的id

        HttpUtils.postRequest(BaseUrl.DOWNLIST, this, map, new JsonCallback<BaseResultBean<OptionDetailsListBean>>() {
            @Override
            public BaseResultBean<OptionDetailsListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<OptionDetailsListBean>> response) {
                super.onSuccess(response);
                if (response.body().code == -1) {
                    if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                }else if (response.body().code == 3080005) {
                    if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.getDataSuccess(response.body().code, response.body().msg, response.body().data);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<OptionDetailsListBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //购买的窗口数据
    public void getPopuData(Activity activity, String updown_id) {
        Map map = new HashMap();
        map.put("updown_id", updown_id);//币种页的id

        HttpUtils.postRequest(BaseUrl.FINDUPDOWNSYS, this, map, new JsonCallback<BaseResultBean<OptionPopupBean>>() {
            @Override
            public BaseResultBean<OptionPopupBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<OptionPopupBean>> response) {
                super.onSuccess(response);
                if (response.body().code == -1) {
                    if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                }else if (response.body().code == 3080005) {
                    if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.getPopupSuccess(response.body().code, response.body().msg_cn, response.body().data);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<OptionPopupBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //购买
    public void getBuyData(Activity activity, String time_id, String num_id, String json, String coin_id, String flag, String updown_id,String paypa) {

        String md5Str = MD5Util.getMD5Str(json + updown_id + "updown", MD5Util.MD5_LOWER_CASE);
        String paypass = "";
        Map maps = new HashMap();
        maps.put("paypass", paypa);
        maps.put("time", new Date().getTime());
        try {
            paypass = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map map = new HashMap();
        map.put("time_id", time_id);//购买时间段_id
        map.put("num_id", num_id);//购买数量_id
        map.put("json", json);//签名
        map.put("coin_id", coin_id);//购买币种id
        map.put("flag", flag);//买入方向 0：涨 1：跌
        map.put("updown_id", updown_id);//币种页的id交易对
        map.put("sign", md5Str);//md5
        map.put("paypass", paypass);//交易密码

        HttpUtils.postRequest(BaseUrl.SAVEBONDORDER, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                }else if (response.body().code == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.getPopupBuySuccess(response.body().code, response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //查询币种可用余额
    public void getCurrencyBalance(Activity activity, String coinName) {
        Map<String, String> map = new HashMap<>();
        map.put("coinName", coinName);
        HttpUtils.getRequets(BaseUrl.GET_CURRENCY_BALANCE, this, map, new JsonCallback<BaseResultBean<CurrencyBalanceBean>>() {
            @Override
            public BaseResultBean<CurrencyBalanceBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CurrencyBalanceBean>> response) {
                super.onSuccess(response);
                if (null != view)
//                    if (response.body().status==200){
                    if (response.body().status == 602||response.body().status == 3080005) {
                        if(Constants.isL==0){
                            Constants.isL=1;
                            ToastUtils.showShortToast(response.body().msg);
                            //  AppManager.getAppManager().finishAllActivity();
                            ActivityUtils.next(activity, LoginActivity.class, true);
                            Utils.getSpUtils().remove(Constants.TOKEN);
                        }
                        if (null != view)
                            view.onFailure();
                    } else {
                        if (null != view)
                            view.getCurrencyBalanceSuccess(response.body().status, response.body().msg, response.body().data.balance);
                    }
            }

            @Override
            public void onError
                    (com.lzy.okgo.model.Response<BaseResultBean<CurrencyBalanceBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    // 获取余额(usdn/ngk)
    public void getBalance(Activity activity, String account, String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }

        //        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account",
                account);
        hashMap.put("symbol", symbol);
        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size() > 0) {
                            if (null != view)
                                view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        } else {
                            if (null != view)
                                view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if (null != view) {
                            view.getBalanceSuccess(symbol, "0.0000");
                        }

                    }

                });

    }

    //获取转账地址
    public void getFindOrderData(Activity activity) {
        HttpUtils.postRequest(BaseUrl.DEAFINDORDER, this, "", new JsonCallback<BaseResultBean<IssueFeeBean>>() {
            @Override
            public BaseResultBean<IssueFeeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onSuccess(response);

                if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } if (response.body().code == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view)
                        view.onIssueFeeSuccess(response.body().code, response.body().msg_cn, response.body().data);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //k线
    public void getKLainData(Activity activity, String id, String range) {
        Map map = new HashMap();
        map.put("id", id);
        map.put("range", range);//1 true 0false


        HttpUtils.getRequets(BaseUrl.GETKLINE, this, map, new JsonCallback<BaseResultBean<KLainBean>>() {
            @Override
            public BaseResultBean<KLainBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<KLainBean>> response) {
                super.onSuccess(response);
                if (response.body().status == 602||response.body().status == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view) {
                        view.getKLineSuccess(response.body().status, response.body().msg, response.body().data);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<KLainBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    } //k线

    //获取K线其他数据
    public void getKData(Activity activity, String coinName) {//期权详情
        Map map = new HashMap();
        map.put("coinName", coinName);
        map.put("rateName", Utils.getSpUtils().getString("cur_coin_name"));

        HttpUtils.getRequets(BaseUrl.GETKDATA, this, map, new JsonCallback<BaseResultBean<KOtherBean>>() {
            @Override
            public BaseResultBean<KOtherBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<KOtherBean>> response) {
                super.onSuccess(response);
                if (response.body().status == 602||response.body().status == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view) {
                        view.getKOtherSuccess(response.body().status, response.body().msg, response.body().data);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<KOtherBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //获取币种图片列表
    public void getPictureData(Activity activity) {
        Map map = new HashMap();
        map.put("term", "2");
        map.put("reversed", "1");//1 true 0false
        map.put("coin", Utils.getSpUtils().getString("cur_coin_name"));

        HttpUtils.getRequets(BaseUrl.MAKETALL, this, map, new JsonCallback<BaseResultBean<List<OptionListBean>>>() {
            @Override
            public BaseResultBean<List<OptionListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<OptionListBean>>> response) {
                super.onSuccess(response);
                if (response.body().status == 602|| response.body().status == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view) {
                        view.getPictureDataSuccess(response.body().status, response.body().msg, response.body().data);

                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<OptionListBean>>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //获取文案
    public void getRedirectData(Activity activity) {
        Map map = new HashMap();
        map.put("type", "6");
        HttpUtils.getRequets(BaseUrl.FINDREDIRECT, this, map, new JsonCallback<BaseResultBean<RedirectsBean>>() {
            @Override
            public BaseResultBean<RedirectsBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<RedirectsBean>> response) {
                super.onSuccess(response);
                if (response.body().status == 602||response.body().status == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg);
                        //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    if (null != view) {
                        view.getRedirectDataSuccess(response.body().status, response.body().msg, response.body().data);

                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<RedirectsBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //验证交易密码
    public void checkPayPassword(String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null != view)
                    view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn, response.body().msg_cn);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }
}
