package com.taiyi.soul.moudles.main.home.mall.goods.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.moudles.main.home.mall.bean.MallFirstBean;
import com.taiyi.soul.moudles.main.home.mall.goods.GoodsDetailActivity;
import com.taiyi.soul.moudles.main.home.mall.goods.MallMoreActivity;
import com.taiyi.soul.utils.DensityUtil;
import com.zhouwei.mzbanner.MZBannerView;
import com.zhouwei.mzbanner.holder.MZHolderCreator;
import com.zhouwei.mzbanner.holder.MZViewHolder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class MallAdapter extends RecyclerView.Adapter implements AdapterManger.GoodsHotItemClick {


    private Context context;
    List<MallFirstBean.CarousellistBean> carousellist;//轮播图
    List<MallFirstBean.CatelistBean> catelist;//剩余模块

    private int BANNER = 0;
    private int VIEWIND = 1;
    private int VIEW = 2;
    private View mInflate, mInflate1, mInflate2;
    private BannerViewHolder mBannerViewHolder;
    private MallFirstBean.CatelistBean mCatelistBean;
    private MallFirstBean.CatelistBean mlistBean;
    private List<ImageView> mDotView = new ArrayList<>();
    private int tox = 1;

    public MallAdapter(Context context, List<MallFirstBean.CarousellistBean> carousellist, List<MallFirstBean.CatelistBean> catelist) {
        this.context = context;
        this.carousellist = carousellist;
        this.catelist = catelist;

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return BANNER;
        } else if (catelist.get(position - 1).getType().equals("1")) {
            return VIEWIND;
        } else if (catelist.get(position - 1).getType().equals("2")) {
            return VIEW;
        }
//        return super.getItemViewType(position);
        return VIEW;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == BANNER) {
            mInflate = LayoutInflater.from(context).inflate(R.layout.item_mall_banner, parent, false);
            return new BannerViewHolder(mInflate);
        } else if (viewType == VIEWIND) {
            mInflate1 = LayoutInflater.from(context).inflate(R.layout.item_mall_recycle_ind, parent, false);
            return new IndViewHolder(mInflate1);
        } else if (viewType == VIEW) {
            mInflate2 = LayoutInflater.from(context).inflate(R.layout.item_mall_recycle, parent, false);
            return new ListViewHolder(mInflate2);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BannerViewHolder) {
            ((BannerViewHolder) holder).mall_banner.setIndicatorVisible(true);
            ((BannerViewHolder) holder).mall_banner.setIndicatorAlign(MZBannerView.IndicatorAlign.RIGHT);
            ((BannerViewHolder) holder).mall_banner.setIndicatorRes(R.mipmap.mall_dot_nom, R.mipmap.mall_dot);
            ((BannerViewHolder) holder).mall_banner.setIndicatorPadding(0, 0, 50, 20);
            ((BannerViewHolder) holder).mall_banner.setPages(carousellist, new MZHolderCreator<BannerDataViewHolder>() {
                @Override
                public BannerDataViewHolder createViewHolder() {
                    return new BannerDataViewHolder(holder);
                }
            });
            ((BannerViewHolder) holder).mall_banner.start();
        } else if (holder instanceof IndViewHolder) {
//            ((IndViewHolder) holder).type_name.setText(context.getResources().getString(R.string.mall_hot));
            ((IndViewHolder) holder).type_name.setText(catelist.get(position - 1).getIntroname());
            for (int i = 0; i < catelist.size(); i++) {
                if (catelist.get(i).getType().equals("1")) {
                    mCatelistBean = catelist.get(i);
                }
            }
            initPageNew((IndViewHolder) holder, mCatelistBean.getComlist());
            ((IndViewHolder) holder).mall_hot_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
//                    bundle.putString("categoryid",mCatelistBean.getCategoryid());
                    ActivityUtils.next((Activity) context, MallMoreActivity.class, bundle, false);
                }
            });
        } else if (holder instanceof ListViewHolder) {
//            ((ListViewHolder) holder).type_name.setText(context.getResources().getString(R.string.mall_discount));
            if (!catelist.get(position - 1).getType().equals("1")) {
                mlistBean = catelist.get(position - 1);
            }
                ((ListViewHolder) holder).type_name.setText(catelist.get(position - 1).getIntroname());
                initListData((ListViewHolder) holder, mlistBean.getComlist());
            ((ListViewHolder) holder).mall_discount_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
//                    bundle.putString("categoryid",catelist.get(position - 1).getCategoryid());
                    ActivityUtils.next((Activity) context, MallMoreActivity.class, bundle, false);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return catelist.size() + 1;
    }

    @Override
    public void onItemClick(int position, String commodityid) {
        Bundle bundle = new Bundle();
        bundle.putString("commodityid", commodityid);
        ActivityUtils.next((Activity) context, GoodsDetailActivity.class, bundle, false);
    }

    public class BannerViewHolder extends RecyclerView.ViewHolder {
        public MZBannerView mall_banner;

        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);
            mall_banner = itemView.findViewById(R.id.mall_banner);
        }
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        public TextView type_name;
        public TextView mall_discount_more;
        public RecyclerView mall_discount;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            type_name = itemView.findViewById(R.id.type_name);
            mall_discount_more = itemView.findViewById(R.id.mall_discount_more);
            mall_discount = itemView.findViewById(R.id.mall_discount);
        }
    }

    public class IndViewHolder extends RecyclerView.ViewHolder {
        public TextView type_name;
        public TextView mall_hot_more;
        public RecyclerView topicRecyclerView;
        public LinearLayout ll_top_dot;

        public IndViewHolder(@NonNull View itemView) {
            super(itemView);
            type_name = itemView.findViewById(R.id.type_name);
            mall_hot_more = itemView.findViewById(R.id.mall_hot_more);
            topicRecyclerView = itemView.findViewById(R.id.topicRecyclerView);
            ll_top_dot = itemView.findViewById(R.id.ll_top_dot);
        }
    }

    /**************************************轮播图**********************************************************/
    private int maxWidth;
    int mWidth;
    int mHeight;
    private int mValue;

    public class BannerDataViewHolder implements MZViewHolder<MallFirstBean.CarousellistBean> {
        private ImageView mImageView;
        private BannerViewHolder mHolder;

        public BannerDataViewHolder(RecyclerView.ViewHolder holder) {
            mHolder = (BannerViewHolder) holder;
        }

        @Override
        public View createView(Context context) {
            // 返回页面布局文件
            View view = LayoutInflater.from(context).inflate(R.layout.item_banner, null);
            mImageView = view.findViewById(R.id.banner_image);
            mImageView.setAdjustViewBounds(true);
            ViewGroup.LayoutParams lp = mImageView.getLayoutParams();
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            maxWidth = metrics.widthPixels - DensityUtil.dip2px(context, 40);
            lp.width = maxWidth;
            lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            mImageView.setLayoutParams(lp);


            Resources res = context.getResources();
            Bitmap bitmap = BitmapFactory.decodeResource(res, R.mipmap.banner_main_one);

            mWidth = bitmap.getWidth();
            mHeight = bitmap.getHeight();

            BigDecimal b1 = new BigDecimal(Double.toString(mWidth));
            BigDecimal b2 = new BigDecimal(Double.toString(mHeight));
            BigDecimal b3 = new BigDecimal(Double.toString(maxWidth));
            double divide = b2.divide(b1, 2, BigDecimal.ROUND_HALF_UP).doubleValue();

            BigDecimal divide2 = new BigDecimal(Double.toString(divide));
            mValue = divide2.multiply(b3).intValue();

            ViewGroup.LayoutParams params = mHolder.mall_banner.getLayoutParams();
            params.width = maxWidth; // 宽度设置成屏幕宽度，这里根据自己喜好设置
            params.height = mValue; // 利用已知图片的宽高比计算高度
            mHolder.mall_banner.setLayoutParams(params);
            return view;
        }


        @Override
        public void onBind(Context context, int position, MallFirstBean.CarousellistBean data) {
            // 数据绑定
            Glide.with(context).load(data.getImgurl()).apply(RequestOptions.bitmapTransform(new RoundedCorners(35)).override(300, 300)).into(mImageView);
        }
    }

    /**************************************带分页指示**********************************************************/

    private void initPageNew(IndViewHolder holder, List<MallFirstBean.CatelistBean.ComlistBean> comlist) {

        int pager = 0;
        if (comlist.size() % 2 == 0) {
            pager = comlist.size() / 2;
        } else {
            pager = comlist.size() / 2 + 1;
        }
        for (int i = 0; i < pager; i++) {
            ImageView imageView = new ImageView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            params.leftMargin = 10;     //设置圆点相距距离
            params.rightMargin = 10;
            if (i == 0) {               //初始化为红点
                imageView.setBackgroundResource(R.mipmap.mall_dot_selected);
            } else {
                imageView.setBackgroundResource(R.mipmap.mall_dot_unselected);
            }
            holder.ll_top_dot.addView(imageView, params);
            mDotView.add(imageView);
        }
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.topicRecyclerView.setLayoutManager(manager);
        holder.topicRecyclerView.setAdapter(AdapterManger.getHotListAdapter(context, comlist, this));
        holder.topicRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx > 0) {//右动作
                    tox = 1;
                }
                if (dx < 0) {//左动作
                    tox = 0;
                }
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                //判断是当前layoutManager是否为LinearLayoutManager
                // 只有LinearLayoutManager才有查找第一个和最后一个可见view位置的方法
                if (layoutManager instanceof LinearLayoutManager) {
                    LinearLayoutManager linearManager = (LinearLayoutManager) layoutManager;
                    //获取最后一个可见view的位置
                    int lastItemPosition = linearManager.findLastCompletelyVisibleItemPosition();
                    int firstItemPosition = linearManager.findFirstCompletelyVisibleItemPosition();
//                     Log.e("firstItemPosition==",firstItemPosition+"");
                    if (tox == 1) {
                        if ((lastItemPosition + 1) % 2 == 0) {
                            for (int i = 0; i < mDotView.size(); i++) {
                                if (i != ((lastItemPosition + 1) / 2 - 1)) {
                                    mDotView.get(i).setBackgroundResource(R.mipmap.mall_dot_unselected);
                                } else {
                                    mDotView.get(i).setBackgroundResource(R.mipmap.mall_dot_selected);
                                }
                            }
                        } else {
                            if (lastItemPosition == comlist.size() - 1) {
                                for (int i = 0; i < mDotView.size(); i++) {
                                    if (i == mDotView.size() - 1) {
                                        mDotView.get(i).setBackgroundResource(R.mipmap.mall_dot_selected);
                                    } else {
                                        mDotView.get(i).setBackgroundResource(R.mipmap.mall_dot_unselected);
                                    }
                                }
                            } else {

                            }
                        }
                    } else if (tox == 0) {
                        if (firstItemPosition % 2 == 0) {
                            for (int i = 0; i < mDotView.size(); i++) {
                                if (i != firstItemPosition / 2) {
                                    mDotView.get(i).setBackgroundResource(R.mipmap.mall_dot_unselected);
                                } else {
                                    mDotView.get(i).setBackgroundResource(R.mipmap.mall_dot_selected);
                                }
                            }
                        } else {

                        }
                    }


                }
            }
        });

    }

    /**************************************纯列表**********************************************************/

    private void initListData(ListViewHolder holder, List<MallFirstBean.CatelistBean.ComlistBean> mlistBean) {
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.mall_discount.setLayoutManager(manager);
        holder.mall_discount.setAdapter(AdapterManger.getDiscountListAdapter(context, mlistBean,this));
    }
}

