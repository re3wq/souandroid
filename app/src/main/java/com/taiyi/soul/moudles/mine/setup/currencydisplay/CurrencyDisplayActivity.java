package com.taiyi.soul.moudles.mine.setup.currencydisplay;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.mine.bean.CurrencyListBean;
import com.taiyi.soul.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CurrencyDisplayActivity extends BaseActivity<CurrencyDisplayView, CurrencyDisplayPresent> implements MultiItemTypeAdapter.OnItemClickListener, CurrencyDisplayView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.usDollarTv)
    TextView usDollarTv;
    @BindView(R.id.rightTitleTv)
    TextView rightTitleTv;
    @BindView(R.id.selectCurrencyTv)
    TextView selectCurrencyTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.top)
    ConstraintLayout top;
    private List<CurrencyListBean> dataList = new LinkedList<>();
    private int selPosition = 0;
    private int click = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_currency_display;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public CurrencyDisplayPresent initPresenter() {
        return new CurrencyDisplayPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.currency_display));
//        rightTitleTv.setText(getString(R.string.save));
//        dataList.add(new DataBean(R.mipmap.currency_display_us_dollar_iv,getString(R.string.currency_display_us_dollar)));
//        dataList.add(new DataBean(R.mipmap.currency_display_chinese_yuan_iv,getString(R.string.currency_display_chinese_yuan)));
//        dataList.add(new DataBean(R.mipmap.currency_display_won_iv,getString(R.string.currency_display_won)));
//        dataList.add(new DataBean(R.mipmap.currency_display_hongkong_dollar_iv,getString(R.string.currency_display_hongkong_dollar)));
//        dataList.add(new DataBean(R.mipmap.currency_display_sgd_iv,getString(R.string.currency_display_sgd)));
//        dataList.add(new DataBean(R.mipmap.currency_display_australian_dollar_iv,getString(R.string.currency_display_australian_dollar)));
//        dataList.add(new DataBean(R.mipmap.currency_display_gbp_iv,getString(R.string.currency_display_gbp)));
//        dataList.add(new DataBean(R.mipmap.currency_display_eur_iv,getString(R.string.currency_display_eur)));
//        dataList.add(new DataBean(R.mipmap.currency_display_cad_iv,getString(R.string.currency_display_cad)));
//        dataList.add(new DataBean(R.mipmap.currency_display_jpy_iv,getString(R.string.currency_display_jpy)));
//        dataList.add(new DataBean(R.mipmap.currency_display_idr_iv,getString(R.string.currency_display_idr)));
//        dataList.get(0).setSelect(true);
//        selectCurrencyTv.setText(getString(R.string.current_select_currency)+dataList.get(0).getName());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        CommonAdapter setupLanguageListAdapter = AdapterManger.getCurrencyPriceListAdapter(this, dataList);
        recyclerView.setAdapter(setupLanguageListAdapter);
        setupLanguageListAdapter.setOnItemClickListener(this);
    }

    @Override
    protected void initData() {
        showProgress();
        presenter.getCurrencyList();
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.usDollarTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.usDollarTv:
                break;
        }
    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
        selPosition = position;
        int id = dataList.get(position).getId();

        if(click==0){
            click=1;
            showProgress();
            presenter.switchCurrency(id+"");
        }


    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }

    @Override
    public void getCurrencyListSuccess(List<CurrencyListBean> currencyListBeanList) {
        hideProgress();
        dataList.clear();
        dataList.addAll(currencyListBeanList);
        recyclerView.getAdapter().notifyDataSetChanged();
        String cur_id = Utils.getSpUtils().getString("cur_id");

        for (CurrencyListBean currencyListBean : dataList) {
            if (Integer.parseInt(cur_id) == currencyListBean.getId()) {
                currencyListBean.setSelect(true);
                selectCurrencyTv.setText(getString(R.string.current_select_currency) + currencyListBean.getCurName());
                usDollarTv.setText(getString(R.string.currency_display_currency) + " " + currencyListBean.getCurName());
                Utils.getSpUtils().put("coin_symbol", currencyListBean.getSymbol());
                Utils.getSpUtils().put("symbol", dataList.get(selPosition).getSymbol());
//                Log.e("cccc==",currencyListBean.getSymbol());
                break;
            }
        }

    }

    @Override
    public void getCurrencyListFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);

    }

    @Override
    public void switchSuccess() {
        hideProgress();
        Utils.getSpUtils().put("cur_id", dataList.get(selPosition).getId() + "");

        for (int i = 0; i < dataList.size(); i++) {
            if (selPosition == i) {
                dataList.get(selPosition).setSelect(true);
                selectCurrencyTv.setText(getString(R.string.current_select_currency) + dataList.get(selPosition).getCurName());
                usDollarTv.setText(getString(R.string.currency_display_currency) + " " + dataList.get(selPosition).getCurName());
                Utils.getSpUtils().put("symbol", dataList.get(selPosition).getSymbol());
                Utils.getSpUtils().put(Constants.SELECT_COIN, dataList.get(selPosition).getName());
                Utils.getSpUtils().put("coin_symbol", dataList.get(selPosition).getSymbol());
//                Log.e("cccc==", dataList.get(selPosition).getSymbol());
            } else {
                dataList.get(i).setSelect(false);
            }
        }
        recyclerView.getAdapter().notifyDataSetChanged();
        EventBus.getDefault().post("switchSuccess");
        click=0;
    }

    @Override
    public void switchFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
        click=0;
    }
}
