package com.taiyi.soul.moudles.mine.shippingaddress.add;

import com.taiyi.soul.moudles.mine.bean.AddressInfoBean;
import com.taiyi.soul.moudles.mine.bean.AreaBean;

import java.util.List;

public interface AddShippingAddressView  {
    void onListSuccess(List<AreaBean> list);
    void onListFailure(String errorMsg);
    void addSuccess();
    void addFailure(String errorMsg);
    void getAddressInfoSuccess(AddressInfoBean addressInfoBean);
    void getAddressInfoFailure(String errorMsg);
    void deleteSuccess();
    void deleteFailure(String errorMsg);
}
