package com.taiyi.soul.moudles.mine.bean;

import java.util.List;

public class MyTeamPerformanceListBean {

    /**
     * total : 1
     * list : [{"date":"07-03-2020","num":3,"achievement":0}]
     */

    private int total;
    private List<ListBean> list;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * date : 07-03-2020
         * num : 3
         * achievement : 0
         */

        private String date;
        private String num;
        private String achievement;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getAchievement() {
            return achievement;
        }

        public void setAchievement(String achievement) {
            this.achievement = achievement;
        }
    }
}
