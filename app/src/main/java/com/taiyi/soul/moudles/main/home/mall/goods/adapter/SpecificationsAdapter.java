package com.taiyi.soul.moudles.main.home.mall.goods.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.home.mall.bean.GoodsDetailBean;
import com.taiyi.soul.moudles.main.home.mall.goods.manager.FlowLayoutManager;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 规格列表适配器
 */
public class SpecificationsAdapter extends RecyclerView.Adapter<SpecificationsAdapter.MyHolder>{
    private Context context;
    private List<GoodsDetailBean.ProductSpecListBean> data;

    public SpecificationsAdapter(Context context, List<GoodsDetailBean.ProductSpecListBean> data) {
        this.context = context;
        this.data = data;
    }


    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new MyHolder(LayoutInflater.from(context).inflate(R.layout.item_specifications, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, final int i) {
        myHolder.item_name.setText(data.get(i).specName);
        myHolder.item_recycle.setLayoutManager(new FlowLayoutManager(context,true));
        SpecificationsItemAdapter itemAdapter = new SpecificationsItemAdapter(context,data.get(i).proList);
        myHolder.item_recycle.setAdapter(itemAdapter);
        myHolder.item_recycle.setNestedScrollingEnabled(false);
        itemAdapter.setItemClickListener(new SpecificationsItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if(data.get(i).proList.get(position).isChecked==false){
                    data.get(i).proList.get(position).isChecked = true;
                    for (int j = 0; j < data.get(i).proList.size(); j++) {
                        if(j!=position){
                            data.get(i).proList.get(j).isChecked = false;
                        }
                    }
//                    Log.i("isChecked===","  "+position);
                    notifyDataSetChanged();
                    EventBus.getDefault().post("select_change");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView item_name;
        RecyclerView item_recycle;

        public MyHolder(View itemView) {
            super(itemView);
            item_name = itemView.findViewById(R.id.item_name);
            item_recycle = itemView.findViewById(R.id.item_recycle);
        }
    }
}
