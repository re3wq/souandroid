package com.taiyi.soul.moudles.mine.message.list;

import com.taiyi.soul.moudles.mine.message.bean.MessageListBean;

public interface MessageListView {
    void getMessageListSuccess(MessageListBean listBean);
    void getMessageListFailure(String errorMsg);
    void updateStateSuccess();
    void onSuccess();
    void onFailure(String errorMsg);
}
