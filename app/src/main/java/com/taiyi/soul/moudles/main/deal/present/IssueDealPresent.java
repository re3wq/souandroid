package com.taiyi.soul.moudles.main.deal.present;

import android.app.Activity;
import android.content.Intent;

import com.alibaba.fastjson.JSONObject;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.deal.bean.EntryOrderBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueDealTotalBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.MD5Util;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：集市交易买单卖单相关接口
 */
public class IssueDealPresent extends BasePresent<IssueDealView> {


    /**
     * 获取交易金额
     *
     * @param downType
     * @param num
     * @param price
     */
    public void getData(String downType, String num, String price) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "1");//0:算力市场，1：集市交易
        map.put("downType", downType);//0：买，1：卖
        map.put("num1", num);//数量
        map.put("price", price);//单价
        HttpUtils.postRequest(BaseUrl.DEAFINDORDERPRICE, this, map, new JsonCallback<BaseResultBean<IssueDealTotalBean>>() {
            @Override
            public BaseResultBean<IssueDealTotalBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueDealTotalBean>> response) {
                super.onSuccess(response);
                if(null!=view)
                    view.onSuccess(response.body().code,response.body().msg_cn,response.body().data);

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueDealTotalBean>> response) {
                super.onError(response);
            }
        });
    }


    //验证交易密码
    public void checkPayPassword(String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if(null!=view)
                view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    //挂单提交
    public void entryOrders(String downType, String price,String num,String json,String pass) {
        String md5Str = MD5Util.getMD5Str(json + price+"entrust", MD5Util.MD5_LOWER_CASE);
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("type", "1");//0:算力市场，1：集市交易
        map.put("downType", downType);//0：买，1：卖
        map.put("num1", num);//数量
        map.put("price", price);//单价
        map.put("json", json);//签名
        map.put("sign", md5Str);
        map.put("paypass", password);
        HttpUtils.postRequest(BaseUrl.SAVEENTRYORDER, this, map, new JsonCallback<BaseResultBean<EntryOrderBean>>() {
            @Override
            public BaseResultBean<EntryOrderBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EntryOrderBean>> response) {
                super.onSuccess(response);
                if(null!=view)
                view.entryOrdersSuccess(response.body().code, response.body().msg_cn);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EntryOrderBean>> response) {
                super.onError(response);
            }
        });
    }
    /**
     * 获取余额
     */
    public void getBalance(String account,String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account",
                account);

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size()>0){
                            if(null!=view)
                            view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        }else {
                            if(null!=view)
                            view.getBalanceSuccess(symbol,"0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if(null!=view)
                        view.getBalanceSuccess(symbol,"0.0000");
                    }

                });

    }

    //获取挂单信息（手续费、数量、价格等）
    public void getFindOrderData(Activity activity){
        HttpUtils.postRequest(BaseUrl.DEAFINDORDER, this,"", new JsonCallback<BaseResultBean<IssueFeeBean>>() {
            @Override
            public BaseResultBean<IssueFeeBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onSuccess(response);
                if (response.body().code==0){
                    if(null!=view)
                    view.onIssueFeeSuccess(response.body().data);
                }else if(response.body().code==-1){
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showLongToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onError(response);
            }
        });
    }

}
