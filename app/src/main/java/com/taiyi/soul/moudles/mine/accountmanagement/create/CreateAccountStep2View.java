package com.taiyi.soul.moudles.mine.accountmanagement.create;

import com.taiyi.soul.moudles.register.bean.EmptyBean;

public interface CreateAccountStep2View {
    void createSuccess(EmptyBean emptyBean);
    void createFailure(String errorMsg);
}
