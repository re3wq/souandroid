package com.taiyi.soul.moudles.main.assets.transfer;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.IBinder;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.PushDataManger;
import com.taiyi.soul.blockchain.util.StringUtils;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.TransferBalanceBean;
import com.taiyi.soul.moudles.main.assets.view.CurrencyPopupWindow;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.mine.accountmanagement.importaccount.ImportAccountActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.moudles.scancode.ScanCodeActivity;
import com.taiyi.soul.utils.AccountPrivateUtils;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;
import me.ljp.permission.PermissionItem;


public class TransferActivity extends BaseActivity<TransferView, TransferPresent> implements TransferView, MultiItemTypeAdapter.OnItemClickListener, SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.coinIv)
    ImageView coinIv;
    @BindView(R.id.coinNameTv)
    TextView coinNameTv;
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.balanceTv)
    TextView balanceTv;
    @BindView(R.id.transactionAmountEt)
    EditText transactionAmountEt;
    @BindView(R.id.collectMoneyAddressEt)
    EditText collectMoneyAddressEt;
    //    @BindView(R.id.memoEt)
//    EditText memoEt;
    @BindView(R.id.minerFee)
    TextView minerFee;
    @BindView(R.id.downIv)
    ImageView downIv;
    @BindView(R.id.minerFeeTv)
    TextView minerFeeTv;
    @BindView(R.id.transfer_failed_hint)
    TextView transfer_failed_hint;
    @BindView(R.id.transfer_eos_hint)
    TextView transfer_eos_hint;
    @BindView(R.id.seekBar)
    SeekBar seekBar;
    @BindView(R.id.coinCl)
    ConstraintLayout coinCl;
    @BindView(R.id.top)
    ConstraintLayout top;
    @BindView(R.id.group)
    LinearLayout group;
    @BindView(R.id.key_main)
    RelativeLayout key_main;
    @BindView(R.id.key_scroll)
    LinearLayout key_scroll;
    @BindView(R.id.keyboardPlace)
    LinearLayout keyboardPlace;
    List<CoinListBean> list = new ArrayList<>();
    List<CoinListBean> allList = new ArrayList<>();
    RecyclerView recyclerView;
    public static final int REQUEST_CODE = 0x1001;
    private CurrencyPopupWindow currencyPopupWindow;
    private CurrencyPopupWindow minerPopupWindow;
    private String transferAmount;
    private String transferAddress;
    private String memo;
    private int free = 0;
    private List<TransferBalanceBean.ListBean> feeList;
    CoinListBean tetherUSDTBean,tetherUSDTBean2;
    private Dialog usdtDialog;
    private TextView wayOneTv, wayTwoTv,wayThreeTv, way1Tv, way2Tv, way3Tv;
    private String coinName, mBalance,coinNameCurr;
    private String fromCoinName;
    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    private String scanSymbol;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_transfer_new;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    private void initDialog() {
        if (usdtDialog == null) {
            usdtDialog = new Dialog(this, R.style.MyDialog);
            View view = LayoutInflater.from(this).inflate(R.layout.usdt_layout_dialog, null);
            usdtDialog.setContentView(view);
            wayOneTv = view.findViewById(R.id.wayOneTv);
            wayTwoTv = view.findViewById(R.id.wayTwoTv);
            wayThreeTv = view.findViewById(R.id.wayThreeTv);
            TextView way = view.findViewById(R.id.way);
            way.setText(getString(R.string.choose_usdt_two));
            wayOneTv.setSelected(true);
            view.findViewById(R.id.closeIv).setOnClickListener(this);
            wayOneTv.setOnClickListener(this);
            wayTwoTv.setOnClickListener(this);
            wayThreeTv.setOnClickListener(this);
        }
    }


    @Override
    public TransferPresent initPresenter() {
        return new TransferPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.transfer));
        initDialog();
        if (getIntent().getExtras() != null) {
            fromCoinName = getIntent().getExtras()
                    .getString("fromCoinName");
        }

        transactionAmountEt.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);

        List<EditText> lists = new ArrayList<>();
        lists.add(transactionAmountEt);
        lists.add(collectMoneyAddressEt);
//        lists.add(memoEt);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, lists, true);
            }
        },500);


        View inflate = LayoutInflater.from(this).inflate(R.layout.coin_list_layout, null);
        recyclerView = inflate.findViewById(R.id.recyclerView);
        CommonAdapter coinListAdapter = AdapterManger.getCoinListAdapter(this, list);
        recyclerView.setAdapter(coinListAdapter);
        coinListAdapter.setOnItemClickListener(this);
        currencyPopupWindow = new CurrencyPopupWindow(this, inflate);
        currencyPopupWindow.setWidth(getResources().getDisplayMetrics().widthPixels - DensityUtil.dip2px(this, 24));
        currencyPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        View inflate1 = LayoutInflater.from(this).inflate(R.layout.transfer_miner_fee_layout, null);
        way1Tv = inflate1.findViewById(R.id.way1Tv);
        way2Tv = inflate1.findViewById(R.id.way2Tv);
        way3Tv = inflate1.findViewById(R.id.way3Tv);
        way1Tv.setOnClickListener(this);
        way2Tv.setOnClickListener(this);
        way3Tv.setOnClickListener(this);
        minerPopupWindow = new CurrencyPopupWindow(this, inflate1);
        minerPopupWindow.setWidth(getResources().getDisplayMetrics().widthPixels - DensityUtil.dip2px(this, 24));
        minerPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        minerPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                downIv.setImageResource(R.mipmap.mine_safety_center_down_iv);
                isOpen = false;
                Window window = getWindow();
                WindowManager.LayoutParams attributes = window.getAttributes();
                attributes.alpha = 1.0f;
                window.setAttributes(attributes);
            }
        });
//        way1Tv.setSelected(true);


    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
            View v = getCurrentFocus();

            if (isShouldHideInput(v, ev)) {
                hideSoftInput(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (event.getY() > top) {
//                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                if (null!=safeKeyboard&&safeKeyboard.isShow()) {
                    return true;
                } else {
                    return false;
                }

            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            }

        }
    }
    @Override
    protected void initData() {
        showProgress();
        presenter.getCoinList();
    }

    @Override
    public void initEvent() {
        seekBar.setOnSeekBarChangeListener(this);
    }

    @OnClick({R.id.backIv, R.id.iv, R.id.coinCl, R.id.scanIv, R.id.transferTv, R.id.transferOutAllTv, R.id.group})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.coinCl:
                currencyPopupWindow.showAsDropDown(view, 0, 20);
                break;
            case R.id.scanIv:
                List<PermissionItem> permissonItems = new ArrayList<PermissionItem>();
                permissonItems.add(new PermissionItem(Manifest.permission.CAMERA, getString(R.string.camer), R.drawable.permission_ic_camera));
                if (Utils.getPermissions(permissonItems, getString(R.string.open_camer_scan))) {
                    Bundle bundle = new Bundle();
                    bundle.putString("from", "transfer");
                    ActivityUtils.next(this, ScanCodeActivity.class, bundle, REQUEST_CODE);
                }
                break;
            case R.id.transferTv:
                if (!ButtonUtils.isFastDoubleClick(R.id.transferTv)) {
                    transferAmount = transactionAmountEt.getText().toString();
                    transferAddress = collectMoneyAddressEt.getText().toString();
//                    memo = memoEt.getText().toString();
                    if (TextUtils.isEmpty(transferAmount)) {
                        toast(getResources().getString(R.string.please_enter_the_amount));
                        return;
                    }
//                    if (memo.toLowerCase().contains("script")) {
//                        toast(getString(R.string.edit_address_illegal));
//                        return;
//                    }

                    if (!TextUtils.isEmpty(scanSymbol)) {
                        if (!scanSymbol.equals(coinNameTv.getText().toString())) {
                            AlertDialogShowUtil.toastMessage(this, getResources().getString(R.string.currency_does_not_match_cannot_transfer));
                            return;
                        }
                    }
                    coinNameCurr = coinNameTv.getText().toString();
                    if (TextUtils.isEmpty(transferAddress)) {
                        AlertDialogShowUtil.toastMessage(this, getResources().getString(R.string.transfer_address_no));
//                        toast(getResources().getString(R.string.transfer_address_no));
                        return;
                    }
                    if(coinNameCurr.equals("BNB")||coinNameCurr.equals("EOS")||coinNameCurr.equals("XRP")){
//                        if(memoEt.getText().toString().equals("")){
//                            AlertDialogShowUtil.toastMessage(TransferActivity.this,getResources().getString(R.string.enter_memo_not));
//                            return;
//                        }
                    }
                    if (coinNameCurr.equals("SOU")||coinNameCurr.equals("USDS") ||coinNameCurr.equals("HASH")) {
                        this.memo=" ";
                        if(transferAddress.length()!=12){
                            AlertDialogShowUtil.toastMessage(TransferActivity.this,getResources().getString(R.string.transfer_address_input_erro));
                            return;
                        }
                    } else if (coinNameCurr.equals("BTC") ||coinNameCurr.equals("LTC")||coinNameCurr.equals("XRP")||coinNameCurr.equals("BSV")||coinNameCurr.equals("TRX") || selusdt==0) {
                        if(transferAddress.length()!=34){
                            AlertDialogShowUtil.toastMessage(TransferActivity.this,getResources().getString(R.string.transfer_address_input_erro));
                            return;
                        }
                    }else if (coinNameCurr.equals("ETH") ||coinNameCurr.equals("HT")||coinNameCurr.equals("ETC")||coinNameCurr.equals("BNB")||coinNameCurr.equals("OKB") || selusdt==1) {
                        if(transferAddress.length()!=42){
                            AlertDialogShowUtil.toastMessage(TransferActivity.this,getResources().getString(R.string.transfer_address_input_erro));
                            return;
                        }
                    }else if (coinNameCurr.equals("BCH")) {
                        if(transferAddress.length()!=54){
                            AlertDialogShowUtil.toastMessage(TransferActivity.this,getResources().getString(R.string.transfer_address_input_erro));
                            return;
                        }
                    }
                    String replace = minerFeeTv.getText().toString().replace(" " + coinNameTv.getText().toString(), "");//手续费
                    if (new BigDecimal(mBalance).compareTo(new BigDecimal(replace)) == -1 || new BigDecimal(mBalance).compareTo(new BigDecimal(replace)) == 0 || new BigDecimal(transferAmount).compareTo(new BigDecimal(mBalance)) == 1) {
                        AlertDialogShowUtil.toastMessage(this, getResources().getString(R.string.insufficient_balance));
                        return;
                    }
//                if (TextUtils.isEmpty(transferAddress)) {
//                    toast(getResources().getString(R.string.account_input));
//                    return;
//                }

                    if (coinNameTv.getText().toString().equals("HASH")||
                            coinNameTv.getText().toString().equals(getResources().getString(R.string.USDN))
                            || coinNameTv.getText().toString().equals(getResources().getString(R.string.NGK))) {
                        if (transferAddress.length() != 12) {
                            toast(getResources().getString(R.string.transfer_address_input_erro));
                            return;
                        }
                        String substring = transferAddress.substring(0, 8);
                        if (!substring.matches("^[a-z1-5]+$")) {
                            toast(getResources().getString(R.string.transfer_address_input_erro));
                            return;
                        }

//                        Log.i("address==", transferAddress);
                        showProgress();
                        presenter.checkAccountUseableDataInput(view, transferAddress);
                    } else {
                        if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                            showPassword(view);
                        } else {//未导入私钥 ，去设置
                            Dialog dialog = new Dialog(TransferActivity.this, R.style.MyDialog);
                            View inflate = LayoutInflater.from(TransferActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                            dialog.setContentView(inflate);
                            inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("accountName", mainAccount);
                                    ActivityUtils.next(TransferActivity.this, ImportAccountActivity.class, bundle, false);
                                }
                            });
                            dialog.show();
                        }
                    }
                }
                break;
            case R.id.transferOutAllTv:
                transactionAmountEt.setText(mBalance);
                break;

            case R.id.group:

                if (!isOpen)
                    downIv.setImageResource(R.mipmap.mine_safety_center_up_iv);
                else
                    downIv.setImageResource(R.mipmap.mine_safety_center_down_iv);
                isOpen = !isOpen;
                Window window = getWindow();
                WindowManager.LayoutParams attributes = window.getAttributes();
                attributes.alpha = 0.2f;
                window.setAttributes(attributes);
//                minerPopupWindow.showAsDropDown(view, 0, DensityUtil.dip2px(this, -93.0f));
                minerPopupWindow.showAsDropDown(view, 0, DensityUtil.dip2px(this, -142.0f));

                break;
        }
    }

    private boolean isOpen;
    private String passWord = "";
    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    //显示密码框
    private void showPassword(View view) {
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgot_password = v.findViewById(R.id.forgot_password);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInputp(viewById, ev,keyboardPlace)) {
                        hideSoftInputp(view.getWindowToken());
                    }
                }
                return false;
            }
        });

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(TransferActivity.this, UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    if (s.length() == 6) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                passWord = s;
                                pay_popup.dismiss();
                                if (coinNameTv.getText().toString().equals("SOU") || coinNameTv.getText().toString().equals("USDS")|| coinNameTv.getText().toString().equals("HASH")) {
                                    presenter.checkPayPassword(TransferActivity.this,s);
                                } else {
//                                    String memo = memoEt.getText().toString();

                                    showProgress();
//                                    presenter.transfer(TransferActivity.this, transferAmount, coinNameTv.getText().toString(), String.valueOf(free), TextUtils.isEmpty(memo) ? "" : memo, passWord, transferAddress);
                                    presenter.transfer(TransferActivity.this, transferAmount, coinName, String.valueOf(free), TextUtils.isEmpty(memo) ? "" : memo, passWord, transferAddress);
                                }
                            }
                        }, 500);
                    }

                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }
    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() <  bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() <  (bottom +DensityUtil.dip2px(TransferActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }
    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {
                pay_popup.dismiss();
            }
        }
    }
    private void transferNgkOrUsdn() {
        String ss = "";
        if (coinNameTv.getText().toString().equals("USDS")||coinNameTv.getText().toString().equals("HASH")) {
//            ss = new DecimalFormat("##0.00000000").format(new Double(transferAmount));
            ss = new BigDecimal(transferAmount).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
        } else {
//            ss = new DecimalFormat("##0.0000").format(new Double(transferAmount));
            ss = new BigDecimal(transferAmount).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
        }
        String uni = "";
        if(coinNameTv.getText().toString().equals("USDS")){
            uni = Constants.USDN_CONTRACT_ADDRESS;
        }else if(coinNameTv.getText().toString().equals("SOU")){
            uni = Constants.NGK_CONTRACT_ADDRESS;
        }else if(coinNameTv.getText().toString().equals("HASH")){
            uni = Constants.HASH_CONTRACT_ADDRESS;
        }
        new PushDataManger(this, new PushDataManger.Callback() {
            @Override
            public void getResult(String result) {
                hideProgress();
                toast(getString(R.string.transfer_success));
                finish();
            }

            @Override
            public void fail() {
                hideProgress();
                toast(getString(R.string.transfer_failed));
            }
        }).pushAction(uni,
                Constants.ACTIONTRANSFER,
                new Gson().toJson(
                        new TransferEosMessageBean(memo,
                                transferAddress,
                                ss + " " + coinNameTv.getText().toString(),
                                Utils.getSpUtils().getString("mainAccount"))),
                Utils.getSpUtils().getString("mainAccount"));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (data != null)
                if (null != data.getExtras()) {
                    String strCode = data.getExtras().getString("id");
                    if (strCode.contains("{")) {
                        try {
                            scanSymbol = new JSONObject(strCode).optString("symbol");
                            String address = new JSONObject(strCode).optString("address");
                            collectMoneyAddressEt.setText(address);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        collectMoneyAddressEt.setText(strCode);
                    }
//                    Log.i("dsadadas", "-------" + strCode);
                }
        }
    }

    @Override
    public void getCoinListSuccess(List<CoinListBean> coinListBeanList) {
        hideProgress();
        if (coinListBeanList != null) {
            list.clear();
            allList.clear();
            allList.addAll(coinListBeanList);
            for (CoinListBean coinListBean : coinListBeanList) {
                if ("TetherUSDT".equals(coinListBean.getCoinName())) {
                    tetherUSDTBean = coinListBean;
                }else if ("TRC20USDT".equals(coinListBean.getCoinName())) {
                    tetherUSDTBean2 = coinListBean;
                }else if ("BTC".equals(coinListBean.getCoinName())) {
                }else {
                    list.add(coinListBean);
                }
            }

//          list.addAll(coinListBeanList);
            recyclerView.getAdapter().notifyDataSetChanged();
            if (fromCoinName == null) {
                coinName = list.get(0).getCoinName();
                coinNameTv.setText(coinName);
                Glide.with(this).load(list.get(0).getUrl()).into(coinIv);
                if (coinName.equals("SOU") || coinName.equals("USDS")|| coinName.equals("HASH")|| coinName.equals("SPC")|| coinName.equals("BGV")) {
                    group.setVisibility(View.GONE);
                    transfer_failed_hint.setVisibility(View.VISIBLE);
                    presenter.getBalance(coinName);
                }else {
                    if(coinName.equals("EOS")){
                        transfer_eos_hint.setText(getResources().getString(R.string.transfer_eos_hint));
                        transfer_eos_hint.setVisibility(View.VISIBLE);
                    }else {
                        if(coinName.equals("XRP")){
                            if(transfer_eos_hint.getText().toString().contains("EOS")){
                                transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("EOS","XRP"));
                            }else{
                                transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("BNB","XRP"));
                            }
                            transfer_eos_hint.setVisibility(View.VISIBLE);
                        }else {
                            if(coinName.equals("BNB")){
                                if(transfer_eos_hint.getText().toString().contains("EOS")){
                                    transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("EOS","BNB"));
                                }else{
                                    transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("XRP","BNB"));
                                }
                                transfer_eos_hint.setVisibility(View.VISIBLE);
                            }else {
                                transfer_eos_hint.setVisibility(View.GONE);
                            }
                        }
                    }

                    presenter.getCurrencyBalance(this, list.get(0).getCoinName());
                }


            } else {
                if(null==list||list.size()==0){
                    return;
                }
                for (int i = 0; i < list.size(); i++) {
                    CoinListBean coinListBean = list.get(i);
                    if (fromCoinName.equals(coinListBean.getCoinName())) {
                        coinName = coinListBean.getCoinName();
                        coinNameTv.setText(coinName);
                        recyclerView.scrollToPosition(i);
                        Glide.with(this).load(coinListBean.getUrl()).into(coinIv);
                        if (coinName.equals("SOU") || coinName.equals("USDS")|| coinName.equals("HASH")|| coinName.equals("SPC")|| coinName.equals("BGV")) {
                            group.setVisibility(View.GONE);
                            transfer_failed_hint.setVisibility(View.VISIBLE);
                            presenter.getBalance(coinName);
                        } else {
                            group.setVisibility(View.VISIBLE);
                            if(coinName.equals("EOS")){
                                transfer_eos_hint.setText(getResources().getString(R.string.transfer_eos_hint));
                                transfer_eos_hint.setVisibility(View.VISIBLE);
                            }else {
                                if(coinName.equals("XRP")){
                                    if(transfer_eos_hint.getText().toString().contains("EOS")){
                                        transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("EOS","XRP"));
                                    }else{
                                        transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("BNB","XRP"));
                                    }
                                    transfer_eos_hint.setVisibility(View.VISIBLE);
                                }else {
                                    if(coinName.equals("BNB")){
                                        if(transfer_eos_hint.getText().toString().contains("EOS")){
                                            transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("EOS","BNB"));
                                        }else{
                                            transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("XRP","BNB"));
                                        }
                                        transfer_eos_hint.setVisibility(View.VISIBLE);
                                    }else {
                                        transfer_eos_hint.setVisibility(View.GONE);
                                    }
                                }
                            }

                            transfer_failed_hint.setVisibility(View.GONE);
                            if ("USDT".equals(fromCoinName)) {
                                usdtDialog.show();
                                presenter.getCurrencyBalance(this,"USDT");
                            }else {
                                presenter.getCurrencyBalance(this, coinListBean.getCoinName());
                            }


                        }
                        break;
                    }
                }
            }

        }

        if (coinName.equals("BTC")) {
            transactionAmountEt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(8)});
        } else if (coinName.equals("ETH")) {
            transactionAmountEt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(6)});
        } else {
            transactionAmountEt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(4)});
        }

        if (coinName.equals("SOU")||coinName.equals("USDS")||coinName.equals("HASH")) {
            collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(12)});
        } else if (coinName.equals("BTC") ||coinName.equals("LTC")||coinName.equals("XRP")||coinName.equals("BSV")||coinName.equals("TRX") || selusdt==0) {
            collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(34)});
        }else if (coinName.equals("ETH") ||coinName.equals("HT")||coinName.equals("ETC")||coinName.equals("BNB")||coinName.equals("OKB") || selusdt==1) {
            collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(42)});
        }else if (coinName.equals("BCH")) {
            collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(54)});
        }
    }

    @Override
    public void getCoinListFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getBalanceSuccess(TransferBalanceBean balance) {
        hideProgress();
        if (coinName.equals("BTC")) {
            mBalance =new BigDecimal(balance.getBalance()).setScale(8,BigDecimal.ROUND_DOWN).toPlainString();
        } else if (coinName.equals("ETH")) {
            mBalance =new BigDecimal(balance.getBalance()).setScale(6,BigDecimal.ROUND_DOWN).toPlainString();
        } else {
            mBalance =new BigDecimal(balance.getBalance()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
        }

        if(coinName.contains("USDT")){
            balanceTv.setText(getString(R.string.available) + mBalance+"USDT");
        }else {
            balanceTv.setText(getString(R.string.available) + mBalance+coinName);
        }


        feeList = balance.getList();
        if (feeList != null || feeList.size() != 0) {
            if (feeList.size() == 3) {
                way1Tv.setText(feeList.get(0).getFreeStr());
                way2Tv.setText(feeList.get(1).getFreeStr());
                way3Tv.setText(feeList.get(2).getFreeStr());
            }
            seekBar.setMax(feeList.size() - 1);
            for (int i = 0; i < feeList.size(); i++) {
                TransferBalanceBean.ListBean listBean = feeList.get(i);
                if (listBean.isDe()) {
                    if (i == 0) {
                        way1Tv.setSelected(true);
                    } else if (i == 1) {
                        way2Tv.setSelected(true);
                    } else {
                        way3Tv.setSelected(true);
                    }
                    free = listBean.getId();
                    minerFeeTv.setText(listBean.getFreeStr() + " " + coinNameTv.getText().toString());
                    seekBar.setProgress(i);
                    break;
                }
            }
        } else {

        }
    }

    @Override
    public void transferSuccess(String msg) {
        hideProgress();
        EventBus.getDefault().post("transferSuccess");
//        AlertDialogShowUtil.toastMessage(TransferActivity.this,msg);
        toast(msg);
        finish();
    }

    @Override
    public void transferFailure(String errorMsg) {
        hideProgress();
        AlertDialogShowUtil.toastMessage(TransferActivity.this,errorMsg);
    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {
        hideProgress();
        mBalance = new BigDecimal(balance).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
        balanceTv.setText(getString(R.string.available) +mBalance+coinName);
        coinNameTv.setText(symbol);
        minerFeeTv.setText("0.0000 " + symbol);
    }

    @Override
    public void checkAccountUseableInput(View viewButton, boolean s) {
        hideProgress();
        if (s) {//用户名不存在，无法转账                            toast(getResources().getString(R.string.transfer_address_input_erro));
            AlertDialogShowUtil.toastMessage(TransferActivity.this, getString(R.string.transfer_address_input_erro));
        } else {//用户名存在。可以转账
            if (AccountPrivateUtils.isHavePrivateKey()) {//有私钥
                presenter.getAccountStatus(viewButton);

            } else {//未导入私钥 ，去设置
                Dialog dialog = new Dialog(TransferActivity.this, R.style.MyDialog);
                View inflate = LayoutInflater.from(TransferActivity.this).inflate(R.layout.dialog_import_sub_wallet, null);
                dialog.setContentView(inflate);
                inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Bundle bundle = new Bundle();
                        bundle.putString("accountName", mainAccount);
                        ActivityUtils.next(TransferActivity.this, ImportAccountActivity.class, bundle, false);
                    }
                });
                dialog.show();
            }
        }
    }

    @Override
    public void onCheckPayPasswordSuccess(int code, String msg_cn, String bean) {
        if(code==0){
            showProgress();
            transferNgkOrUsdn();
        }else {
            AlertDialogShowUtil.toastMessage(this,msg_cn);
        }
    }

    @Override
    public void getAccountStatusSuccess(int code, String msg_cn, String bean, View viewButton) {
        if(code==200){
            if(bean.equals("0")){
                showPassword(viewButton);
            }else if(bean.equals("2")){
                if(Constants.isL==0) {
                    Constants.isL = 1;
                    AlertDialogShowUtil.toastMessage(TransferActivity.this,getString(R.string.account_status));
                    //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                }
            }else if(bean.equals("1")){
                AlertDialogShowUtil.toastMessage(TransferActivity.this,getString(R.string.account_status2));
            }
        }else {

        }
    }

    private void update() {
        way1Tv.setSelected(false);
        way2Tv.setSelected(false);
        way3Tv.setSelected(false);
    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
        currencyPopupWindow.dismiss();
        transactionAmountEt.setText("");
        transactionAmountEt.setHint("0.0000");
        collectMoneyAddressEt.setText("");
        collectMoneyAddressEt.setHint(getString(R.string.input_collect_money_address));
//        memoEt.setText("");
//        memoEt.setHint(getString(R.string.enter_memo));
        coinName = list.get(position).getCoinName();
        coinNameTv.setText(coinName);
        Glide.with(this).load(list.get(position).getUrl()).into(coinIv);
        if ("USDS".equals(coinName) || "SOU".equals(coinName) || "HASH".equals(coinName) || "SPC".equals(coinName) || "BGV".equals(coinName)) {
            selusdt = -1;
//            seekBar.setVisibility(View.INVISIBLE);
//            downIv.setVisibility(View.INVISIBLE);
            group.setVisibility(View.GONE);
            transfer_failed_hint.setVisibility(View.VISIBLE);
            showProgress();
            presenter.getBalance(coinName);
        } else if ("USDT".equals(coinName)) {
            usdtDialog.show();
            group.setVisibility(View.VISIBLE);
            transfer_failed_hint.setVisibility(View.GONE);


        } else {
            selusdt = -1;
//            seekBar.setVisibility(View.VISIBLE);
//            downIv.setVisibility(View.VISIBLE);
            if(coinName.equals("EOS")){
                transfer_eos_hint.setText(getResources().getString(R.string.transfer_eos_hint));
                transfer_eos_hint.setVisibility(View.VISIBLE);
            }else {
                if(coinName.equals("XRP")){
                    if(transfer_eos_hint.getText().toString().contains("EOS")){
                        transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("EOS","XRP"));
                    }else{
                        transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("BNB","XRP"));
                    }
                    transfer_eos_hint.setVisibility(View.VISIBLE);
                }else {
                    if(coinName.equals("BNB")){
                        if(transfer_eos_hint.getText().toString().contains("EOS")){
                            transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("EOS","BNB"));
                        }else{
                            transfer_eos_hint.setText(transfer_eos_hint.getText().toString().replace("XRP","BNB"));
                        }
                        transfer_eos_hint.setVisibility(View.VISIBLE);
                    }else {
                        transfer_eos_hint.setVisibility(View.GONE);
                    }
                }
            }

            group.setVisibility(View.VISIBLE);
            transfer_failed_hint.setVisibility(View.GONE);
            showProgress();
            presenter.getCurrencyBalance(this, coinName);
        }

        if (coinName.equals("BTC")) {
            transactionAmountEt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(8)});
        } else if (coinName.equals("ETH")) {
            transactionAmountEt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(6)});
        } else {
            transactionAmountEt.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(4)});
        }
//       BTC  TetherUSDT  LTC  XRP
//BSV  TRX
//3Mv8mme9kFB9ksJK7RP6kFpeWzgyh6zNcj
//34
//
//
//BCH
//bitcoincash:qzpkczjw2a2tryt7a7hl04rqnnckupxysy736y8r39
//49
//
//EOS    MEMO：1580829664
//wotoutest123
//12
//
//
//
//ETH  USDT-erc20  HT  ETC
//BNB
//OKB
//0x7b1487a8fbd54786f78d8188a37290689dc97479
//42

//        selusdt==0  TetherUSDT
//        selusdt==1  USDT-erc20
        if (coinName.equals("SOU")||coinName.equals("USDS")||coinName.equals("HASH")||coinName.equals("SPC")||coinName.equals("BGV")||coinName.equals("EOS")) {
            collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(12)});
        } else if (coinName.equals("BTC") ||coinName.equals("LTC")||coinName.equals("XRP")||coinName.equals("BSV")||coinName.equals("TRX") ) {
            collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(34)});
        }else if (coinName.equals("ETH") ||coinName.equals("HT")||coinName.equals("ETC")||coinName.equals("BNB")||coinName.equals("OKB")) {
            collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(42)});
        }else if (coinName.equals("BCH")) {
            collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(54)});
        }
    }

    public class DecimalDigitsInputFilter implements InputFilter {

        private final int decimalDigits;
        Pattern mPattern;
        private static final String POINTER = ".";


        private static final String ZERO = "0";

        /**
         * Constructor.
         *
         * @param decimalDigits maximum decimal digits
         */
        public DecimalDigitsInputFilter(int decimalDigits) {
            this.decimalDigits = decimalDigits;
            mPattern = Pattern.compile("([0-9]|\\.)*");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            String destText = dest.toString();
            int dotPos = -1;
            int len = dest.length();
            for (int i = 0; i < len; i++) {
                char c = dest.charAt(i);
                if (c == '.' || c == ',') {
                    dotPos = i;
                    break;
                }
            }
            if (dotPos >= 0) {

                // protects against many dots
                if (source.equals(".") || source.equals(",")) {
                    return "";
                }


                // if the text is entered before the dot
                if (dend <= dotPos) {
                    return null;
                }
                if (len - dotPos > decimalDigits) {
                    return "";
                }

            }
            Matcher matcher = mPattern.matcher(source);
            if (!matcher.matches()) {
                return "";
            } else {
                if ((POINTER.equals(source.toString())) && StringUtils.isEmpty(destText)) {  //首位不能输入小数点
                    return "";
                } else if (!POINTER.equals(source.toString()) && ZERO.equals(destText)) { //如果首位输入0，接下来只能输入小数点
                    return "";
                }
            }
            return null;
        }

    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
//        Log.i("seekBar", "---" + progress);
        TransferBalanceBean.ListBean listBean = feeList.get(progress);
        free = listBean.getId();
        minerFeeTv.setText(listBean.getFreeStr() + " " + coinNameTv.getText().toString());
        if (progress == 0) {
            update();
            way1Tv.setSelected(true);
        } else if (progress == 1) {
            update();
            way2Tv.setSelected(true);
        } else {
            update();
            way3Tv.setSelected(true);
        }
    }

    private int selusdt = -1;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wayOneTv:
                wayOneTv.setSelected(true);
                wayTwoTv.setSelected(false);
                wayThreeTv.setSelected(false);
                selusdt = 0;
                usdtDialog.dismiss();
                showProgress();
                collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(34)});
                coinName = tetherUSDTBean.getCoinName();
                presenter.getCurrencyBalance(this, "USDT");
                break;
            case R.id.wayTwoTv:
                wayOneTv.setSelected(false);
                wayThreeTv.setSelected(false);
                wayTwoTv.setSelected(true);
                selusdt = 1;
                usdtDialog.dismiss();
                showProgress();
                coinName = "USDT";
                collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(42)});
                presenter.getCurrencyBalance(this, "USDT");
                break;
            case R.id.wayThreeTv:
                wayOneTv.setSelected(false);
                wayTwoTv.setSelected(false);
                wayThreeTv.setSelected(true);
                selusdt = 3;
                usdtDialog.dismiss();
                showProgress();
                coinName = tetherUSDTBean2.getCoinName();
//                coinName = tetherUSDTBean.getCoinName();
                collectMoneyAddressEt.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(34)});
                presenter.getCurrencyBalance(this, "USDT");


                break;
            case R.id.closeIv:
                usdtDialog.dismiss();
                showProgress();
                presenter.getCurrencyBalance(this, coinName);
                break;
            case R.id.way1Tv:
                minerPopupWindow.dismiss();
                seekBar.setProgress(0);
                TransferBalanceBean.ListBean listBean = feeList.get(0);
                free = listBean.getId();
                minerFeeTv.setText(listBean.getFreeStr() + " " + coinNameTv.getText().toString());
                update();
                way1Tv.setSelected(true);
                break;
            case R.id.way2Tv:
                minerPopupWindow.dismiss();
                seekBar.setProgress(1);
                TransferBalanceBean.ListBean listBean1 = feeList.get(1);
                free = listBean1.getId();
                minerFeeTv.setText(listBean1.getFreeStr() + " " + coinNameTv.getText().toString());
                update();
                way2Tv.setSelected(true);
                break;
            case R.id.way3Tv:
                minerPopupWindow.dismiss();
                seekBar.setProgress(2);
                TransferBalanceBean.ListBean listBean2 = feeList.get(2);
                free = listBean2.getId();
                minerFeeTv.setText(listBean2.getFreeStr() + " " + coinNameTv.getText().toString());
                update();
                way3Tv.setSelected(true);
                break;
        }
    }
}
