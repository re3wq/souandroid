package com.taiyi.soul.moudles.main.deal.bean;

import java.io.Serializable;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\1 0001 17:58
 * @Author : yuan
 * @Describe ：
 */
public class IssueFeeBean implements Serializable {

                public String ngk_out_address;
                public String ngk_maxnum_sell;
                public String systeminformationid;
                public String ngk_charge_sell;
                public String ngk_eat_sell_mixnum;
                public String hash_min_ngk_sell;
                public String ngk_max_ngk_buy;
                public String hash_min_ngk_buy;
                public String up_down_sign;
                public String ngk_recommend_price_buy;
                public String ngk_charge_buy;
                public String hash_maxprice_buy;
                public String ngk_max_ngk_sell;
                public String purchase_hash_address;
                public String hash_maxnum_buy;
                public String ngk_maxprice_buy;
                public String hash_collect_address;
                public String hash_recommend_price_sell;
                public String ngk_min_ngk_sell;
                public String hash_max_ngk_sell;
                public String bond_address_sign;
                public String ngk_mixnum_buy;
                public String hash_mixnum_sell;
                public String ngk_recommend_price_sell;
                public String ngk_collect_sign;
                public String hash_out_address;
                public String hash_recommend_price_buy;
                public String hash_collect_sign;
                public String hash_max_ngk_buy;
                public String exchange_address_sign;
                public String hash_mixprice_sell;
                public String purchase_hash_sign;
                public String hash_charge_buy;
                public String ngk_maxnum_buy;
                public String hash_maxprice_sell;
                public String ngk_min_ngk_buy;
                public String up_down_address;
                public String hash_charge_sell;
                public String hash_eat_buy_mixnum;
                public String hash_out_sign;
                public String ngk_collect_address;
                public String ngk_mixprice_sell;
                public String bond_address;
                public String ngk_eat_buy_mixnum;
                public String ngk_mixnum_sell;
                public String hash_mixnum_buy;
                public String exchange_address;
                public String ngk_out_sign;
                public String ngk_mixprice_buy;
                public String ngk_maxprice_sell;
                public String hash_mixprice_buy;
                public String hash_eat_sell_mixnum;
                public String hash_maxnum_sell;
                public String airdrop_address;
                public String shop_address;


}
