package com.taiyi.soul.moudles.mine.securitycenter.fingerprint;

import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import butterknife.BindView;
import butterknife.OnClick;

public class FingerprintEnterActivity extends BaseActivity<NormalView, NormalPresenter> {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.resultTv)
    TextView resultTv;
    @BindView(R.id.resultReasonTv)
    TextView resultReasonTv;
    @BindView(R.id.reRegisterTv)
    TextView reRegisterTv;
    @BindView(R.id.backTv)
    TextView backTv;
    @BindView(R.id.top)
    ConstraintLayout top;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_fingerprint_enter;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.fingerprint_entry));
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.reRegisterTv, R.id.backTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.reRegisterTv:
                break;
            case R.id.backTv:
                break;
        }
    }
}
