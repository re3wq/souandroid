package com.taiyi.soul.moudles.main.home.mall.order.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderBean;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class OrderFinishAdapter extends BaseRecyclerAdapter<OrderBean.OrderlistBean> {

    private Context context;
    private OrderItemButtonClick itemClick;
    private PullRecyclerView pullRecyclerView;
//    public OrderFinishAdapter(Context context, AdapterManger.OrderItemClick itemClick) {
//        this.context = context;
//        this.itemClick = itemClick;
//    }
    public OrderFinishAdapter(Context context,OrderItemButtonClick itemClick) {
        this.context = context;
        this.itemClick = itemClick;
    }
    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }
    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_order;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, OrderBean.OrderlistBean data) {
        TextView view = getView(holder, R.id.goods_remind_shipment);//提醒发货
        TextView view2 = getView(holder, R.id.goods_cancel_order);//取消订单
        TextView view3 = getView(holder, R.id.goods_confirm_receipt);//确认收货
        TextView view4 = getView(holder, R.id.goods_change_address);//修改地址
        TextView view5 = getView(holder, R.id.goods_pay);//支付
        TextView view6 = getView(holder, R.id.goods_confirm_finish);//已完成
        TextView goods_all_num = getView(holder, R.id.goods_all_num);
        TextView goods_all_price = getView(holder, R.id.goods_all_price);
        RecyclerView goods_recycle = getView(holder,R.id.goods_recycle);
        LinearLayout order_item = getView(holder, R.id.order_item);

        BigDecimal sumprice=new BigDecimal(data.sumprice);
        BigDecimal yunfei=new BigDecimal(data.yunfei);
        BigDecimal ngk_ratio=new BigDecimal(data.ngk_ratio);
        BigDecimal bigDecimal = sumprice.add(yunfei);
        goods_all_num.setText("1");
        if(data.paycoin.equals("SOU")){
            BigDecimal bigDecimal1 = bigDecimal.multiply(ngk_ratio).setScale(4, BigDecimal.ROUND_UP);
            goods_all_price.setText(bigDecimal1.toPlainString()+" SOU");
        }else {
            goods_all_price.setText(bigDecimal.toPlainString()+" USDS");
        }


        view6.setVisibility(View.VISIBLE);


        List<OrderBean.OrderlistBean> beans = new ArrayList<>();
        beans.add(data);
        goods_recycle.setLayoutManager(new LinearLayoutManager(context));
        CommonAdapter dealSellAdapter = AdapterManger.getOrderPaymentAdapter(context, beans);
        goods_recycle.setAdapter(dealSellAdapter);
        order_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onItemClick(position);
            }
        });
        dealSellAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int pos) {
                itemClick.onItemClick(position);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int pos) {
                return false;
            }
        });
    }


    //订单列表item下按钮点击
    public interface OrderItemButtonClick {

        void onItemClick(int position);//详情

    }
}

