package com.taiyi.soul.moudles.mine.setup.update;

import com.taiyi.soul.moudles.mine.bean.VersionUpdateBean;

public interface VersionUpdateView {
    void checkVersionUpdateSuccess(VersionUpdateBean versionUpdateBean);
    void checkVersionUpdateFailure(String errorMsg);
}
