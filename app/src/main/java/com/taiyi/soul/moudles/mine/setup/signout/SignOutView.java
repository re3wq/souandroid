package com.taiyi.soul.moudles.mine.setup.signout;

public interface SignOutView  {
    void signOutSuccess();
    void signOutFailure(String errorMsg);
}
