package com.taiyi.soul.moudles.main.webview

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import com.taiyi.soul.R
import com.taiyi.soul.base.BaseActivity
import com.taiyi.soul.moudles.normalvp.NormalPresenter
import com.taiyi.soul.moudles.normalvp.NormalView
import com.taiyi.soul.utils.SPUtil
import com.taiyi.soul.utils.Utils
import kotlinx.android.synthetic.main.activity_two_web_view.*
import kotlinx.android.synthetic.main.base_top_layout.*

class TwoWebViewActivity : BaseActivity<NormalView, NormalPresenter>() {

    override fun getLayoutId(): Int = R.layout.activity_two_web_view

    override fun initPresenter(): NormalPresenter = NormalPresenter()

    override fun initViews(savedInstanceState: Bundle?) {
        var id = intent.extras.getInt("id")
        var type = intent.extras.getInt("type")
        var url = intent.extras.getString("url")
//        titleTv.text = ""
        var settings = webView.settings
        webView.setBackgroundColor(0)
        webView.background.alpha = 0
        settings.run {
            javaScriptEnabled = true
            useWideViewPort = true
            loadWithOverviewMode = true
        }
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return super.shouldOverrideUrlLoading(view, url)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
//                showProgress()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
//                hideProgress()
            }
        }


       if (type == 0){
            webView.loadUrl(url+"?lang="+ Utils.getSpUtils().getString("current_language", ""))
        }else
//            webView.loadUrl(url+"&lang="+ Utils.getSpUtils().getString("current_language", ""))

//           Log.e("account===", url+"   "+Utils.getSpUtils().getString("mainAccount"));
            webView.loadUrl(url+"?name="+ Utils.getSpUtils().getString("mainAccount")+"&lang="+ Utils.getSpUtils().getString("current_language", ""))
    }

    /**
     * 获取选择的语言设置
     *
     * @param context
     * @return
     */
    fun getSetLanguageLocale(context: Context?): String? {
        return when (SPUtil.getInstance(context).selectLanguage) {
            0 -> "zh-rCN"
            1 -> "en" //英语
            2 -> "fr" //法语
            3 -> "de" //德语
            4 -> "es" //西班牙语
            5 -> "pt" //葡萄牙语
            6 -> "ru" //俄语
            7 -> "ar" //阿拉伯语
            8 -> "zh-rCN"//繁体中文
            9 -> "zh"//简体中文
            10 -> "ja" //日语
            11 -> "ko" //韩语
            else -> "zh" //简体中文
        }
    }

    override fun initImmersionBar() {
        super.initImmersionBar()
        mImmersionBar.titleBar(top).init()
    }

    override fun initData() {

    }

    override fun initEvent() {
        backIv.setOnClickListener { finish() }
    }
}
