package com.taiyi.soul.moudles.main.node;

import android.app.Activity;
import android.content.Context;

import com.google.gson.Gson;
import com.lzy.okgo.model.Response;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.node.bean.NodeBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NodePresent extends BasePresent<NodeView> {

    private Context context;

    public NodePresent(Context context) {
        this.context = context;
    }

    public void getAccountInfoData() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", Utils.getSpUtils().getString("mainAccount"));
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_CHIAN_ACCOUNT_INFO, this, new Gson().toJson(hashMap), new JsonCallback<BlockChainAccountInfoBean>() {
            @Override
            public void onSuccess(Response<BlockChainAccountInfoBean> response) {
                if (null != view)
                    view.getBlockchainAccountInfoDataHttp(response.body());
            }

            @Override
            public void onError(Response<BlockChainAccountInfoBean> response) {
                super.onError(response);
                if (null != view)
                    view.getDataHttpFail(context.getString(R.string.socket_exception));
            }
        });
    }

    //获取最新算力价格
    public void getCurrentValue() {
        HttpUtils.getRequets(BaseUrl.GET_LAST_COMPUTER_PRICE, this, null, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (response.body().status == 200) {
                    if (null != view)
                        view.getComputerPriceSuccess(response.body().data);
                } else {
                    if (null != view)
                        view.getComputerPriceFailure(response.body().msg);
                }
            }

            @Override
            public void onError(Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

    //获取节点数据
    public void getNodeData() {
        HttpUtils.getRequets(BaseUrl.GET_NODE_DATA, this, null, new JsonCallback<BaseResultBean<NodeBean>>() {
            @Override
            public BaseResultBean<NodeBean> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(Response<BaseResultBean<NodeBean>> response) {
                super.onSuccess(response);
                if (response.body().status == 200) {
                    if (null != view)
                        view.getNodeDataSuccess(response.body().data);
                } else {
                    if (null != view)
                        view.getNodeDataFailure(response.body().msg);
                }
            }

            @Override
            public void onError(Response<BaseResultBean<NodeBean>> response) {
                super.onError(response);
            }
        });
    }

    //获取挂单信息（手续费、数量、价格等）
    public void getFindOrderData(Activity activity) {
        HttpUtils.postRequest(BaseUrl.DEAFINDORDER, this, "", new JsonCallback<BaseResultBean<IssueFeeBean>>() {


            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onIssueFeeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                }else if (response.body().code ==3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<IssueFeeBean>> response) {
                super.onError(response);
            }
        });
    }


    /**
     * 获取余额
     */
    public void getBalance(String symbol) {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        if (symbol.equals("SOU")) {
            hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);
        } else {
            hashMap.put("code", Constants.USDN_CONTRACT_ADDRESS);

        }
//        for ( TokenListBean b: Constants.tokenListBeans) {
//            if(symbol.equals(b.symbol)){
//                hashMap.put("code", b.contract);
//            }
//        }
        hashMap.put("account", Utils.getSpUtils().getString("mainAccount"));

        hashMap.put("symbol", symbol);

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(okhttp3.Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (response.body().size() > 0) {
                            if (null != view)
                                view.getBalanceSuccess(symbol, response.body().get(0).split(" ")[0]);
                        } else {
                            if (null != view)
                                view.getBalanceSuccess(symbol, "0.0000");
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if (null != view)
                            view.getBalanceSuccess(symbol, "0.0000");
                    }

                });

    }


    public void getRate() {
        Map<String, String> map = new HashMap<>();
        map.put("coinNames", "SOU");
        map.put("coinNamee", "USDS");
        HttpUtils.getRequets(BaseUrl.COIN_TO_COIN_RATE, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (response.body().status == 200) {
                    if (null != view){
                    view.getRateSuccess(response.body().data);
                    }
                } else {
                    if (null != view){
                    view.getRateFailure(response.body().msg);
                    }
                }
            }

            @Override
            public void onError(Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }


}
