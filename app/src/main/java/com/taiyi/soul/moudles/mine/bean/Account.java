package com.taiyi.soul.moudles.mine.bean;

import java.util.ArrayList;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/7/3
 * Time: 3:49 PM
 */
public class Account {

    private ArrayList<String> account_names;

    public ArrayList<String> getAccount_names() {
        return account_names;
    }

    public void setAccount_names(ArrayList<String> account_names) {
        this.account_names = account_names;
    }

}
