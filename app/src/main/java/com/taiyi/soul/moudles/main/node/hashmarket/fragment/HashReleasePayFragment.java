package com.taiyi.soul.moudles.main.node.hashmarket.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.TransferEosMessageBean;
import com.taiyi.soul.blockchain.EosSignDataManger;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.deal.bean.IssueDealTotalBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.moudles.main.node.bean.HashBalanceBean;
import com.taiyi.soul.moudles.main.node.hashmarket.present.ReleaseOrderPresent;
import com.taiyi.soul.moudles.main.node.hashmarket.present.ReleaseOrderView;
import com.taiyi.soul.moudles.main.node.hashmarket.release.ReleaseOrderActivity;
import com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd.UpdateLoginPasswordActivity;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.view.EditTextJudgeNumberWatcher;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 * @Date ： 2020\7\8 0008 11:38
 * @Author : yuan
 * @Describe ：算力市场-发布委托-买单
 */
public class HashReleasePayFragment extends BaseFragment<ReleaseOrderView, ReleaseOrderPresent> implements ReleaseOrderView {


    String mainAccount = Utils.getSpUtils().getString("mainAccount");
    @BindView(R.id.price)
    EditText mPrice;//价格
    @BindView(R.id.num)
    EditText mNum;//数量
    @BindView(R.id.all_price)
    TextView mAllPrice;//合计金额
    @BindView(R.id.charge)
    TextView charge;//手续费百分比
    @BindView(R.id.charge_price)
    TextView charge_price;//手续费金额
    @BindView(R.id.balanceTv)
    TextView balanceTv;//余额
    @BindView(R.id.marketLowPriceTv)
    TextView mMarketLowPriceTv;//市场最低出售价
    @BindView(R.id.purchasedTv)
    TextView purchasedTv;//已买数量
     @BindView(R.id.key_main)
     RelativeLayout key_main;
     @BindView(R.id.key_scroll)
     LinearLayout key_scroll;
     @BindView(R.id.keyboardPlace)
     LinearLayout keyboardPlace;
    private IssueFeeBean mBean;
    private String mAllMoney;
    private String mBalances;

    private int mItemclick = 0;//判断是否可进行下一步
    Handler handler = new Handler();
    Runnable runnable;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;
    private String button_hint;
    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_release_order_one;
    }
    @Override
    public ReleaseOrderPresent initPresenter() {
        return new ReleaseOrderPresent();
    }
    @Override
    protected void initViews(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }
    @Subscribe
    public void even(String event) {
//        Log.e("fragment==", event);

    }


    @Override
    protected void initData() {
        presenter.getBalance(mainAccount, "USDS");//获取账户余额
        presenter.getFindOrderData(getActivity());
        presenter.getUserHashBalanceData(getActivity());


        mPrice.setInputType(InputType.TYPE_CLASS_NUMBER |InputType.TYPE_NUMBER_FLAG_DECIMAL);
        mNum.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(mPrice);
        list.add(mNum);
        safeKeyboard = ShowKeyboard.initKeyBoard(getContext(), keyboardPlace, key_main, key_scroll, list, true);
        mPrice.addTextChangedListener(new NumberWatcher(mPrice,4));
        mNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mItemclick=1;
                if (runnable != null) {
                    handler.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (s.toString().equals("")) {
                            return;
                        }
                        if (!mPrice.getText().toString().equals("")) {
                            showProgress();
                            presenter.getData("0", mNum.getText().toString(), mPrice.getText().toString());
                        }
                    }
                };
                handler.postDelayed(runnable, 800);
            }
        });
        ReleaseOrderActivity.MyOnTouchListener myOnTouchListener = new ReleaseOrderActivity.MyOnTouchListener() {
            @Override
            public boolean onTouch(MotionEvent ev, View currentFocus) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
                    View v = currentFocus;

                    if (isShouldHideInput(v, ev,keyboardPlace)) {
                        hideSoftInput(v.getWindowToken());
                    }
                }
                return false;
            }
        };
        ((ReleaseOrderActivity) getActivity()).registerMyOnTouchListener(myOnTouchListener);
    }
    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
//            if (event.getY() > top ) {

            if(safeKeyboard != null && safeKeyboard.isShow()){
                if (event.getY() > top && event.getY() < bottom || event.getY() > getActivity().getWindowManager().getDefaultDisplay().getHeight() -  keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }else {
                if (event.getY() > top && event.getY() < bottom) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            }

        }
    }
    @Override
    public void initEvent() {


    }
    @Subscribe
    public void event(String event) {

    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
//        Log.e("fragment==", "hidden");
        if (!hidden) {

        }
    }
    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();
    }
    @OnClick({R.id.releaseTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.releaseTv:
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.releaseTv);
                if (!fastDoubleClick) {

                    if (mItemclick == 0) {
                        if (mPrice.getText().toString().equals("")) {
                            AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.toast_hash_pricr));
                            return;
                        }
                        if (mNum.getText().toString().equals("")) {
                            AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.toast_hash_num));
                            return;
                        }
                        if (Double.parseDouble(mPrice.getText().toString()) <= 0) {
                            AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.toast_hash_pricr));
                            return;
                        }
                        if (Integer.parseInt(mNum.getText().toString()) <= 0) {
                            AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.toast_hash_num));
                            return;
                        }
                        double s = Double.parseDouble(mAllPrice.getText().toString());
                        if( null!=mBalances&&!"".equals(mBalances)){
                            double s1 = Double.parseDouble(NumUtils.subZeroAndDot(mBalances));
                            if (s > s1) {
                                AlertDialogShowUtil.toastMessage(getContext(), getString(R.string.toast_balance_lacking));
                            } else {
                                if(null==mAllMoney){
                                    return;
                                }
                                buyMemo++;
                                showPassword(view);


                            }
                        }

                    }else if(mItemclick ==2){
                        AlertDialogShowUtil.toastMessage(getContext(),button_hint);
                    }
                }

                break;
        }
    }

    /************************************Other methods****************************************************/


    private void showPassword(View view) {    //显示密码框
        View v = LayoutInflater.from(getContext()).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(getContext(), v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);
        TextView forgotPassword = v.findViewById(R.id.forgot_password);
        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getContext(), keyboardPlace, key_main, key_scroll, list, true);

        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInputp(viewById, ev,keyboardPlace)) {
                        hideSoftInputp(v.getWindowToken());
                    }
                }
                return false;
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt("type", 2);
                ActivityUtils.next(getActivity(), UpdateLoginPasswordActivity.class, bundle2);
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showProgress();
                            String unit = Constants.USDN_CONTRACT_ADDRESS;
                            String mainAccount = Utils.getSpUtils().getString("mainAccount");
//                            String memo = "hash_order_buy"+"_"+buyMemo;
                            String memo = "pay expenses"+"_"+buyMemo;

                            String price = new BigDecimal(mAllMoney).setScale(8,BigDecimal.ROUND_DOWN).toPlainString()+ " USDS";
                            new EosSignDataManger(getContext(), new EosSignDataManger.Callback() {
                                @Override
                                public void onSignSuccess(String sign_data) {
                                    showProgress();
                                    presenter.entryOrders("0", mPrice.getText().toString(), mNum.getText().toString(), sign_data,s);
                                }

                                @Override
                                public void fail() {

                                }
                            }).pushAction(new Gson().toJson(new TransferEosMessageBean(memo, mBean.hash_collect_address, price
                                            , mainAccount)),
                                    unit,
                                    mainAccount);
                            pay_popup.dismiss();
                        }
                    },500);
                }
            }
        });
        pay_popup.show(view, getActivity().getWindow(), 1);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
//            if (event.getY() > top ) {

            if (event.getY() > top && event.getY() < bottom || event.getY() >getActivity().getWindowManager().getDefaultDisplay().getHeight()- keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                if (null!=safeKeyboard&&safeKeyboard.isShow()) {
                    return true;
                } else {
                    return true;
                }

            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            }else {
                pay_popup.dismiss();
            }

        }
    }

    public class NumberWatcher extends EditTextJudgeNumberWatcher {


        public NumberWatcher(EditText editText, int precision) {
            super(editText, precision);

        }

        @Override
        public void afterTextChanged(Editable s) {
            super.afterTextChanged(s);
            mItemclick=1;

            if (runnable != null) {
                handler.removeCallbacks(runnable);
            }
            runnable = new Runnable() {
                @Override
                public void run() {
                    //结束后进行操作
                    if (s.toString().equals("")) {
                        return;
                    }

                    if (!mNum.getText().toString().equals("")) {
                        showProgress();
                        presenter.getData("0", mNum.getText().toString(), mPrice.getText().toString());
                    }
                }
            };
            handler.postDelayed(runnable, 800);


        }
    }

    /************************************Call Interface callback****************************************************/
    @Override
    public void onSuccess(int code, String msg_cn, IssueDealTotalBean bean) {//根据数量获取手续费以及合计金额
        mItemclick=code;
        button_hint=msg_cn;
        hideProgress();
       if (code == 0) {
            charge_price.setText(" " + NumUtils.subZeroAndDot(bean.charge) + " " + getString(R.string.USDN));
            mAllPrice.setText(NumUtils.subZeroAndDot(bean.allmoney));
            mAllMoney = bean.allmoney;
        } else if(code==2) {
            charge_price.setText(" " + NumUtils.subZeroAndDot(bean.charge) + " " + getString(R.string.USDN));
            mAllPrice.setText(NumUtils.subZeroAndDot(bean.allmoney));
            mAllMoney = bean.allmoney;
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }else if(code==-1){
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(getActivity(), LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        }else if(code==3080005){
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(getString(R.string.account_status));
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(getActivity(), LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        }else {
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }
    }
    private int buyMemo = 0;
    @Override
    public void onCheckPayPasswordSuccess(int code, String msg_cn) {//校验密码成功

    }

    @Override
    public void entryOrdersSuccess(int code, String msg_cn) {//委托买入成功
        hideProgress();
        AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        if(code==0){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post("hash_issue_refresh");
                    if(null!=getActivity()){
                        getActivity().finish();
                    }

                }
            }, 2050);
        }else if(code==-1){
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(getActivity(), LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }

        }else if(code==3080005){
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(getString(R.string.account_status));
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(getActivity(), LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }

        }

    }

    @Override
    public void getBalanceSuccess(String symbol, String balance) {//获取余额
        hideProgress();
        mBalances = NumUtils.subZeroAndDot(balance);
        balanceTv.setText(getResources().getString(R.string.available) +new BigDecimal(mBalances).setScale(4,BigDecimal.ROUND_DOWN).toPlainString()+" "+getString(R.string.USDN));
    }

    @Override
    public void onIssueFeeSuccess(IssueFeeBean bean) {//获取手续费以及最低处售价等相关信息
        hideProgress();
        mBean = bean;
        hideProgress();
        mBean = bean;

        double hash_charge = Double.parseDouble(mBean.hash_charge_buy);
        charge.setText(getContext().getResources().getString(R.string.issue_service_fee) + "(" + (hash_charge * 100) + "%)：");
        mMarketLowPriceTv.setText(getString(R.string.market_low_price) + NumUtils.subZeroAndDot(bean.hash_recommend_price_buy)+getString(R.string.USDN));


    }

    @Override
    public void onHashBalanceSuccess(int code, String msg_cn, HashBalanceBean data) {
        hideProgress();
        if (code == 0) {
//            mBalances = data.num+"";
//            balanceTv.setText(getResources().getString(R.string.available) +data.num+" "+getString(R.string.USDN));
            purchasedTv.setText(getResources().getString(R.string.purchase) + " " + data.hashrate+" "+"Hash");
        } else if(code==-1){
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(msg_cn);
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(getActivity(), LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        }else if(code==3080005){
            if(Constants.isL==0){
                Constants.isL=1;
                ToastUtils.showShortToast(getString(R.string.account_status));
                //  AppManager.getAppManager().finishAllActivity();
                ActivityUtils.next(getActivity(), LoginActivity.class, true);
                Utils.getSpUtils().remove(Constants.TOKEN);
            }
        }else {
            AlertDialogShowUtil.toastMessage(getContext(), msg_cn);
        }
    }


}
