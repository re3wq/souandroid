package com.taiyi.soul.moudles.main.assets;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.taiyi.soul.R;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.moudles.main.assets.bean.AssetsBean;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class AssetsAdapter extends BaseQuickAdapter<AssetsBean.PropertelistBean, BaseViewHolder> {
    RequestOptions requestOptions=new RequestOptions();


    public AssetsAdapter(List<AssetsBean.PropertelistBean> data) {
        super(R.layout.assets_bottom_list_item_layout, data);
        requestOptions.placeholder(R.mipmap.ngk_item_logo).error(R.mipmap.ngk_item_logo);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder holder, AssetsBean.PropertelistBean o) {
        holder.setText(R.id.coinNameTv,o.getCoinName());
        holder.setText(R.id.quantityTv,o.getMoneyandfor());
        holder.setText(R.id.valueTv,"≈"+o.getMfcon());

        ImageView iv = holder.getView(R.id.coinIv);
        Glide.with(MyApplication.getInstance()).applyDefaultRequestOptions(requestOptions).load(o.getUrl()).into(iv);
    }
}
