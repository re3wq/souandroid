package com.taiyi.soul.moudles.main.node.ram;

import android.content.Context;

import com.google.gson.Gson;
import com.lzy.okgo.model.Response;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.blockchain.PushDataManger;
import com.taiyi.soul.moudles.main.node.ram.bean.BuyRamBean;
import com.taiyi.soul.moudles.main.node.ram.bean.RamBean;
import com.taiyi.soul.moudles.main.node.ram.bean.SealRamBean;
import com.taiyi.soul.moudles.mine.bean.BlockChainAccountInfoBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.BigDecimalUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.Utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.taiyi.soul.base.Constants.ACTION_BUYRAM;
import static com.taiyi.soul.base.Constants.ACTION_SELLRAM;
import static com.taiyi.soul.base.Constants.DELEGATEBWCONTRACT;

public class RamPresent extends BasePresent<RamView> {

    private Context context;

    public RamPresent(Context context) {
        this.context = context;
    }

    /**
     * 获取账户cup、net、 ram资源信息
     */
    public void getAccountInfoData() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", Utils.getSpUtils().getString("mainAccount"));
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_CHIAN_ACCOUNT_INFO, this, new Gson().toJson(hashMap), new JsonCallback<BlockChainAccountInfoBean>() {
            @Override
            public void onSuccess(Response<BlockChainAccountInfoBean> response) {
                if (null!=view) {
                    view.getBlockchainAccountInfoDataHttp(response.body());
                }
            }

            @Override
            public void onError(Response<BlockChainAccountInfoBean> response) {
                super.onError(response);
                if (null!=view) {
                    view.getDataHttpFail(context.getString(R.string.socket_exception));
                }
            }
        });
    }


    /**
     * 获取ram价格
     */
    public void getRamPrice() {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("json", "true");
        hashMap.put("code", "sou");
        hashMap.put("scope", "sou");
        hashMap.put("table", "rammarket");
        hashMap.put("limit", 100);
        HttpUtils.postRequestChain(BaseUrl.HTTP_EOS_GET_TABLE, context, new Gson().toJson(hashMap), new JsonCallback<RamBean>() {
            @Override
            public void onSuccess(Response<RamBean> response) {

                if (response.body() != null
                        && response.body().getRows() != null
                        && response.body().getRows().size() > 0
                ){

                    String basePrice = response.body().getRows().get(0).getBase().getBalance();
                    String quotePrice = response.body().getRows().get(0).getQuote().getBalance();

                    String ram_price = BigDecimalUtil.divide(
                            new BigDecimal(quotePrice),
                            BigDecimalUtil.add(
                                    new BigDecimal("1"),
                                    BigDecimalUtil.divide(new BigDecimal(basePrice), new BigDecimal("1024"))),
                            4
                    ).toPlainString();
                    if (null!=view) {
                    view.getRamPriceSuccess(ram_price + " SOU/KB");}
                }else {
                    if (null!=view) {
                    view.getRamPriceSuccess("0.0000 SOU/KB");}
                }

            }

            @Override
            public void onError(Response<RamBean> response) {
                super.onError(response);
                if (null!=view) {
                view.getRamPriceSuccess("0.0000 SOU/KB");}
            }
        });


    }



    /**
     * 获取NGK余额
     */
    public void getBalance() {
        HashMap<String, String> hashMap = new HashMap<String, String>();

        hashMap.put("code", Constants.NGK_CONTRACT_ADDRESS);

        hashMap.put("account", Utils.getSpUtils().getString("mainAccount"));

        hashMap.put("symbol", "SOU");

        HttpUtils.postRequestChain(BaseUrl.GET_BALANNCE, this, JsonUtil.getGson().toJson(hashMap),
                new JsonCallback<ArrayList<String>>() {
                    @Override
                    public ArrayList<String> convertResponse(okhttp3.Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onSuccess(response);
                        if (null!=view) {
                            if (response.body().size() > 0) {
                                view.getBalanceSuccess(response.body().get(0).split(" ")[0]);
                            } else {
                                view.getBalanceSuccess("0.0000");
                            }
                        }
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<ArrayList<String>> response) {
                        super.onError(response);
                        if (null!=view) {
                            view.getBalanceSuccess("0.0000");
                        }
                    }

                });

    }


    /**
     * 购买ram
     */
    public void buyRam(String account,String amount) {

        BuyRamBean buyRamBean = new BuyRamBean();
        buyRamBean.setPayer(account);
        buyRamBean.setReceiver(account);
        buyRamBean.setQuant(new DecimalFormat("##0.0000").format(new Double(amount)) + " SOU");

        new PushDataManger(context, new PushDataManger.Callback() {
            @Override
            public void getResult(String result) {
                if (null!=view) {
                    if (result.contains("transaction_id")) {
                        view.buySuccess();
                    } else {
                        view.getDataHttpFail(context.getResources().getString(R.string.buy_faile));
                    }
                }
            }

            @Override
            public void fail() {
                if (null!=view) {
                    view.getDataHttpFail(context.getResources().getString(R.string.buy_faile));
                }
            }


        }).pushAction(
                DELEGATEBWCONTRACT,
                ACTION_BUYRAM,
                new Gson().toJson(buyRamBean),
                account);

    }

    /**
     * 出售ram
     */
    public void sellRam(String account,String amount) {

        SealRamBean sealRamBean = new SealRamBean();
        sealRamBean.setAccount(account);
        sealRamBean.setBytes(BigDecimalUtil.multiply(new BigDecimal(amount), new BigDecimal("1024")).intValue());

        new PushDataManger(context, new PushDataManger.Callback() {
            @Override
            public void getResult(String result) {
                if (null!=view) {
                    if (result.contains("transaction_id")) {
                        view.sellSuccess();
                    } else {
                        view.getDataHttpFail(context.getResources().getString(R.string.sale_faile));
                    }
                }
            }

            @Override
            public void fail() {
                if (null!=view) {
                    view.getDataHttpFail(context.getResources().getString(R.string.sale_faile));
                }
            }

        }).pushAction(DELEGATEBWCONTRACT, ACTION_SELLRAM,
                new Gson().toJson(sealRamBean), account);

    }


    //验证交易密码
    public void checkPayPassword(String pass) {
        String password = "";
        Map maps = new HashMap();
        maps.put("paypass", pass);
        maps.put("time", new Date().getTime());
        try {
            password = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("paypass", password);//密码
        HttpUtils.postRequest(BaseUrl.CHECKPAYPASS, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    view.onCheckPayPasswordSuccess(response.body().code, response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }




}
