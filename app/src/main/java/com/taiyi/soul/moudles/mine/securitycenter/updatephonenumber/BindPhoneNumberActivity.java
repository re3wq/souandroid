package com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Handler;
import android.os.IBinder;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.moudles.login.bean.AreaCodeBean;
import com.taiyi.soul.moudles.main.home.mall.order.view.OrderPopupWindow;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.view.AreaCodePopupWindow;
import com.taiyi.soul.view.countdowntimer.CountDownTimerUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class BindPhoneNumberActivity extends BaseActivity<BindPhoneNumberView, BindPhoneNumberPresent> implements AreaCodePopupWindow.OnItemClick, BindPhoneNumberView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.countryCodeTv)
    TextView countryCodeTv;
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.backIv)
    ImageView backIv;
    @BindView(R.id.phoneNumberEt)
    EditText phoneNumberEt;
    @BindView(R.id.verificationCodeEt)
    EditText verificationCodeEt;
    @BindView(R.id.getVerificationCodeTv)
    TextView getVerificationCodeTv;
    @BindView(R.id.ll)
    LinearLayout ll;

    @BindView(R.id.pop_ll)
    LinearLayout pop_ll;
    @BindView(R.id.top)
    ConstraintLayout top;
    int type = 0;
    private ArrayList<AreaCodeBean> list = new ArrayList<>();
    private int loginType;
    private String passWord = "";
    private boolean isUse = false;
    private SafeKeyboard safeKeyboard;
    private OrderPopupWindow pay_popup;
    private int bindType;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_bind_phone_number;
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public BindPhoneNumberPresent initPresenter() {
        return new BindPhoneNumberPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        bindType = getIntent().getExtras().getInt("type");
        if (bindType != 0) {
            titleTv.setText(getString(R.string.update_phonenumber));
            tv.setText("ENTER THE FOLLOWING INFORMATION TO\nCHANGE THE BOUND PHONE");
        } else {
            titleTv.setText(getString(R.string.bind_phone_number));
            tv.setText("ENTER THE FOLLOWING INFORMATION TO\nALSO BIND THE PHONE");
        }
    }

    @Override
    protected void initData() {
        presenter.getAreaCode();
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.pop_ll, R.id.countryCodeTv, R.id.iv, R.id.getVerificationCodeTv, R.id.nextStepTv})
//, R.id.toLoginTv
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.iv://更换手机号
            case R.id.countryCodeTv://更换手机号
            case R.id.pop_ll://更换手机号
                AreaCodePopupWindow popupWindow = new AreaCodePopupWindow(this, list, this);
                popupWindow.showAsDropDown(pop_ll, DensityUtil.dip2px(this, -15), DensityUtil.dip2px(this, 0));

                break;
            case R.id.getVerificationCodeTv:
                String phoneNumber = phoneNumberEt.getText().toString();
                if (TextUtils.isEmpty(phoneNumber)) {
                    toast(getString(R.string.phone_input));
                    return;
                }
                if (phoneNumber.length() != 11) {
                    toast("手机号码格式错误");
                    return;
                }
                showProgress();
                presenter.getVerificationCode(countryCodeTv.getText().toString(), phoneNumber);
                break;
            case R.id.nextStepTv:
                String phoneNumber1 = phoneNumberEt.getText().toString();
                if (TextUtils.isEmpty(phoneNumber1)) {
                    toast(getString(R.string.phone_input));
                    return;
                }
                String verificationCode = verificationCodeEt.getText().toString();
                if (TextUtils.isEmpty(verificationCode)) {
                    toast(getString(R.string.input_code));
                    return;
                }

                String phoneNumber2 = Utils.getSpUtils().getString("phoneNumber");
                if (!TextUtils.isEmpty(phoneNumber2)) {
                    if ((countryCodeTv.getText().toString() + phoneNumber1).equals(phoneNumber2)) {
                        toast("不能更换成原手机号");
                        return;
                    }
                }
                if (isUse) {
                    toast("手机号码已占用");
                    return;
                }
                showPassword(view, phoneNumber1, verificationCode);

                break;
//            case R.id.toLoginTv:
//                if(bindType!=0){//-绑定手机号或邮箱提交验证码校验成功后，直接跳到【我的】页面
//                    Bundle bundle = new Bundle();
//                    bundle.putInt("startact_bind", 4);
//                    //  AppManager.getAppManager().finishAllActivity();
//                    ActivityUtils.next(this, MainActivity.class, bundle, true);
//                }else {//更改手机号
//
//                    if (loginType == 0) {//当前登录是否是手机 是
//                        //  AppManager.getAppManager().finishAllActivity();
//                        Bundle bundle = new Bundle();
//                        bundle.putInt("updataType",0);
//                        ActivityUtils.next(this, BindSuccessActivity.class, bundle,true);
//                    } else {
//                        Bundle bundle = new Bundle();
//                        bundle.putInt("startact_bind", 4);
//                        //  AppManager.getAppManager().finishAllActivity();
//                        ActivityUtils.next(this, MainActivity.class, bundle, true);
//                    }
//                }
//
//                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShowKeyboard.releaseKeyboard();
    }

    //显示密码框
    private void showPassword(View view, String phoneNumber, String verificationCode) {
        View v = LayoutInflater.from(this).inflate(R.layout.popup_order_pay, null);
        pay_popup = new OrderPopupWindow(this, v);
        MNPasswordEditText viewById = v.findViewById(R.id.pay_pwdEditText);

        RelativeLayout key_main = v.findViewById(R.id.key_main);
        LinearLayout key_scroll = v.findViewById(R.id.key_scroll);
        LinearLayout keyboardPlace = v.findViewById(R.id.keyboardPlace);

        viewById.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(viewById);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
        pay_popup.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）


                    if (isShouldHideInput(viewById, ev, keyboardPlace)) {
                        hideSoftInput(v.getWindowToken());
                    }
                }
                return false;
            }
        });
        viewById.setOnTextChangeListener(new MNPasswordEditText.OnTextChangeListener() {
            @Override
            public void onTextChange(String s, boolean b) {
                if (s.length() == 6) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            passWord = s;
                            pay_popup.dismiss();
                            showProgress();
                            presenter.bindPhoneNumberOrEmail(countryCodeTv.getText().toString(), phoneNumber, verificationCode, passWord);
                        }
                    }, 500);

                }
            }
        });
        pay_popup.show(view, getWindow(), 1);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};

            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (safeKeyboard != null && safeKeyboard.isShow()) {
                if (event.getY() > top && event.getY() < bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            } else {
                if (event.getY() > top && event.getY() < (bottom + DensityUtil.dip2px(BindPhoneNumberActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            } else {
                pay_popup.dismiss();

            }
        }
    }

    @Override
    public void onItemClick(int position) {
        countryCodeTv.setText(list.get(position).getCode());
        Glide.with(BindPhoneNumberActivity.this).load(list.get(position).getIcon()).into(iv);
    }

    @Override
    public void onHide(boolean isHide) {

    }

    @Override
    public void getVerificationCodeSuccess() {
        hideProgress();
        CountDownTimerUtils countDownTimerUtils = new CountDownTimerUtils(this, getVerificationCodeTv, 60000, 1000, "#a5a9ac");
        countDownTimerUtils.start();

        getVerificationCodeTv.setFocusable(true);
        getVerificationCodeTv.setFocusableInTouchMode(true);
        getVerificationCodeTv.requestFocus();
    }

    @Override
    public void getInvitePersonFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
        isUse = true;
    }

    @Override
    public void updateSuccess(String msg) {
        hideProgress();
        toast(msg);
        type = 1;

        WalletBean walletBean = MyApplication.getInstance().getWalletBean();
        walletBean.setWallet_phone(phoneNumberEt.getText().toString());
        MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
        MyApplication.getInstance().setWalletBean(walletBean);
        Utils.getSpUtils().put("phoneNumber", phoneNumberEt.getText().toString());
        Utils.getSpUtils().put("phone_country", countryCodeTv.getText().toString());

        Bundle bundle = new Bundle();
        bundle.putInt("starAct",0);//手机
        bundle.putInt("updataType", bindType);//0绑定，1更换
        ActivityUtils.next(this, BindSuccessActivity.class, bundle, true);

    }

    @Override
    public void updateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void getAreaCodeSuccess(List<AreaBean> mList) {
        for (AreaBean areaBean : mList) {
            list.add(new AreaCodeBean(areaBean.getUrl(), areaBean.getArea()));
        }
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getArea().equals("+1")) {
                countryCodeTv.setText(list.get(i).getCode());
                Glide.with(this).load(list.get(i).getIcon()).into(iv);
            }
        }

    }

}
