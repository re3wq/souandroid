package com.taiyi.soul.moudles.main.home.mall.order.present;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderDetailsBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderListPay;

public interface OrderDetailsView {
    void onGetOrderDetailsSuccess(OrderDetailsBean bean);
    void onCancelOrderSuccess(String bean);
    void onTakeDeliverySuccess(String bean);
    void onEditAddressSuccess(String bean);
    void onRemindDeliverySuccess(String bean);
    void onZhiFuLieSuccess(OrderListPay bean);
    void onFailure();
}
