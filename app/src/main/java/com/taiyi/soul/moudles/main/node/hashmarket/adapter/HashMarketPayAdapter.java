package com.taiyi.soul.moudles.main.node.hashmarket.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.deal.bean.DealBean;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.NumUtils;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class HashMarketPayAdapter extends BaseRecyclerAdapter<DealBean.ComlistBean>  {


    private PullRecyclerView pullRecyclerView;
    private Context context;
    private DealItemClick dealItemClick;
    public HashMarketPayAdapter(Context context, DealItemClick dealItemClick) {
        this.context = context;
        this.dealItemClick = dealItemClick;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_deal_sell1;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final DealBean.ComlistBean data) {


        TextView deal_user = getView(holder,R.id.deal_user);
        TextView deal_total = getView(holder,R.id.deal_total);
        TextView deal_amount = getView(holder,R.id.deal_amount);
        TextView deal_price = getView(holder,R.id.deal_price);
        TextView deal_total_unit = getView(holder,R.id.deal_total_unit);
        TextView deal_amount_unit = getView(holder,R.id.deal_amount_unit);
        TextView deal_price_unit = getView(holder,R.id.deal_price_unit);
        TextView deal_buying = getView(holder,R.id.deal_buying);
        deal_buying.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.deal_buying);
                if(!fastDoubleClick) {
                    dealItemClick.onDeal(position);
                }
            }
        });

        deal_user.setText( data.account);
        deal_total.setText(NumUtils.subZeroAndDot(data.surplusmoney));
        deal_amount.setText(data.surplusnum+"");
        deal_price.setText(NumUtils.subZeroAndDot(data.price));

        deal_total_unit.setText(context.getResources().getString(R.string.USDN));
        deal_amount_unit.setText("Hash");
        deal_price_unit.setText(context.getResources().getString(R.string.USDN));

    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }
    //买卖单点击
    public interface DealItemClick {
        void onDeal(int position);
    }
}

