package com.taiyi.soul.moudles.main.home.options.bean;

public class OptionsTabChangeBean {

    public int tab_position;
    public int sort;

    public OptionsTabChangeBean(int tab_position, int sort) {
        this.tab_position = tab_position;
        this.sort = sort;
    }
}
