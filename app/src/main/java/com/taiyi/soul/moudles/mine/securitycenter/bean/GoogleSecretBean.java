package com.taiyi.soul.moudles.mine.securitycenter.bean;

public class GoogleSecretBean {
    private String secret;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
