package com.taiyi.soul.moudles.welcome;

import com.taiyi.soul.app.AppManager;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.bean.NodeListBean;
import com.taiyi.soul.bean.TokenListBean;
import com.taiyi.soul.moudles.mine.bean.VersionUpdateBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.RsaUtil;
import com.taiyi.soul.utils.ToastUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：集市交易相关接口
 */
public class WelcomePresent extends BasePresent<WelcomeView> {


    /**
     * 节点列表
     */
    public void getNodeData(){
        Map<String,String> map = new HashMap<>();
        map.put("coinName","SOU");
        HttpUtils.getRequets(BaseUrl.GETNODELIST, this, map, new JsonCallback<BaseResultBean<List<NodeListBean>>>() {
            @Override
            public BaseResultBean<List<NodeListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<NodeListBean>>> response) {
                super.onSuccess(response);
                if(null!=view)
                if (response.body().code==0){
                    view.onNodeSuccess(response.body().data);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<NodeListBean>>> response) {
                super.onError(response);
            }
        });
    }
    /**
     * 币种列表
     */
    public void getTokenData(){
//        Map<String,String> map = new HashMap<>();
//        map.put("chainCode","BKO");
        HttpUtils.postRequest(BaseUrl.GETTOKENLIST, this, "SOU", new JsonCallback<BaseResultBean<List<TokenListBean>>>() {
            @Override
            public BaseResultBean<List<TokenListBean>> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<List<TokenListBean>>> response) {
                super.onSuccess(response);
                if(null!=view){
                if (response.body().code==0){
                    view.onTokenSuccess(response.body().data);
                }}
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<List<TokenListBean>>> response) {
                super.onError(response);
            }
        });
    }

    public void checkVersionUpdate(){
        Map<String,String>map=new HashMap<>();
        map.put("deviceType","0");
        HttpUtils.getRequets(BaseUrl.VERSION_UPDATE, this, map, new JsonCallback<BaseResultBean<VersionUpdateBean>>() {
            @Override
            public BaseResultBean<VersionUpdateBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<VersionUpdateBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.checkVersionUpdateSuccess(response.body().data);
                    } else {
                        view.checkVersionUpdateFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<VersionUpdateBean>> response) {
                super.onError(response);
            }
        });
    }

    public void getRate(String symbol) {
        Map<String, String> map = new HashMap<>();
        map.put("coinNames", symbol);
        map.put("coinNamee", "USDS");
        HttpUtils.getRequets(BaseUrl.COIN_TO_COIN_RATE, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getRateSuccess(symbol, response.body().data);
                    } else {
                        view.getRateFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

  public void getVer(String value) {

        String values = "";
      Map maps = new HashMap();
      maps.put("value", value );
      maps.put("time", new Date().getTime());
      try {
          values = RsaUtil.encryptByPublicKey(com.alibaba.fastjson.JSONObject.toJSONString(maps));
      } catch (Exception e) {
          e.printStackTrace();
      }
        Map<String, String> map = new HashMap<>();
        map.put("value", values);
        HttpUtils.getRequets(BaseUrl.VER, this, map, new JsonCallback<BaseResultBean<String>>() {
            @Override
            public BaseResultBean<String> convertResponse(okhttp3.Response response) throws Throwable {
                return super.convertResponse(response);

            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {

                    } else if (response.body().status == 702){
                        ToastUtils.showShortToast(response.body().msg);
                        AppManager.getAppManager().finishAllActivity();
                    }
                }

            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                super.onError(response);
            }
        });
    }

}
