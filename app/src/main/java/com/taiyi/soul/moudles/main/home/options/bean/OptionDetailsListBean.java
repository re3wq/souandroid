package com.taiyi.soul.moudles.main.home.options.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\20 0020 15:39
 * @Author : yuan
 * @Describe ：
 */
public class OptionDetailsListBean {

        public int totalpage;
        public int rowcount;
        public long nowTime;
        public List<ComlistBean> comlist;

        public static class ComlistBean {
            public String createtime;
            public String charge;
            public int flag;
            public Integer sign;
            public String orderid;
            public String buyprice;
            public String num;
            public int spendtime;
            public String endtime;
            public String endtime2;
            public String type;
            public String userid;
            public String coin_name;
            public String buytime;
            public int id;
            public int time;
            public String updown_id;
            public String url;
            public String endprice;
            public String get_num;
            public String price_now;

        }

}
