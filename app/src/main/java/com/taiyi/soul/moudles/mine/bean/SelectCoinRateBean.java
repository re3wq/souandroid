package com.taiyi.soul.moudles.mine.bean;

public class SelectCoinRateBean {

    /**
     * id : 449
     * name : Singapore
     * name_zh : 新加坡
     * code : SG
     * currency_name : Dollar
     * currency_name_zh : 新币
     * currency_code : SGD
     * rate : 1.38502
     * hits : 1661694
     * selected : 0
     * top : 0
     * lat : 1.3667
     * lng : 103.8
     * code3 : SGP
     * code_num : 702
     */

    private int id;
    private String name;
    private String name_zh;
    private String code;
    private String currency_name;
    private String currency_name_zh;
    private String currency_code;
    private double rate;
    private int hits;
    private int selected;
    private int top;
    private double lat;
    private double lng;
    private String code3;
    private String code_num;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_zh() {
        return name_zh;
    }

    public void setName_zh(String name_zh) {
        this.name_zh = name_zh;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCurrency_name() {
        return currency_name;
    }

    public void setCurrency_name(String currency_name) {
        this.currency_name = currency_name;
    }

    public String getCurrency_name_zh() {
        return currency_name_zh;
    }

    public void setCurrency_name_zh(String currency_name_zh) {
        this.currency_name_zh = currency_name_zh;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getCode3() {
        return code3;
    }

    public void setCode3(String code3) {
        this.code3 = code3;
    }

    public String getCode_num() {
        return code_num;
    }

    public void setCode_num(String code_num) {
        this.code_num = code_num;
    }
}
