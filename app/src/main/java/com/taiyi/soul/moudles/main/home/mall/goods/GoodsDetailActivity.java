package com.taiyi.soul.moudles.main.home.mall.goods;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.GoodsDetailBean;
import com.taiyi.soul.moudles.main.home.mall.goods.adapter.SpecificationsAdapter;
import com.taiyi.soul.moudles.main.home.mall.goods.present.GoodsDetailPresent;
import com.taiyi.soul.moudles.main.home.mall.goods.present.GoodsDetailView;
import com.taiyi.soul.moudles.main.home.mall.loader.GlideImageLoader;
import com.taiyi.soul.moudles.main.home.mall.loader.IjkVideoLoader;
import com.taiyi.soul.moudles.main.home.mall.order.CheckConfirmActivity;
import com.taiyi.soul.moudles.main.home.mall.view.SpecificationsPopupWindow;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zk.banner.Banner;
import com.zk.banner.BannerConfig;
import com.zk.banner.listener.OnBannerListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class GoodsDetailActivity extends BaseActivity<GoodsDetailView, GoodsDetailPresent> implements OnBannerListener, GoodsDetailView {


    @BindView(R.id.scroll)
    ScrollView mScroll;
    @BindView(R.id.goods_banner_vs)
    Banner mGoodsBanner;
    @BindView(R.id.goods_name)//名称
            TextView mGoodsName;
    @BindView(R.id.goods_price)//价格
            TextView mGoodsPrice;
    @BindView(R.id.brand)//品牌
            TextView brand;
    @BindView(R.id.have_goods)//是否有货
            TextView have_goods;
    @BindView(R.id.have_goods_f)//发货时长
            TextView have_goods_f;
    @BindView(R.id.fee)//运费
            TextView fee;
    @BindView(R.id.goods_serial_number)//编号
            TextView goods_serial_number;
    @BindView(R.id.price_two)//价格
            TextView price_two;
    @BindView(R.id.name_two)//名称
            TextView name_two;
    @BindView(R.id.goods_web)
    WebView mGoodsWeb;
    @BindView(R.id.details_back)
    RelativeLayout mDetailsBack;
    @BindView(R.id.details_cart)
    ImageView mDetailsCart;
    @BindView(R.id.scroll_title)
    LinearLayout mScrollTitle;
    @BindView(R.id.marks_goods)
    RelativeLayout marks_goods;
    @BindView(R.id.details_cart_banner)
    ImageView details_cart_banner;

    private String mCommodityspecid;
    private GoodsDetailBean mGoodsDetailBean;
    private String mCommodityid;
    private String mProd = "";
    private SearchPriceBean mSearchPriceBean;
    private TextView mSpecification_price;
    private TextView mSpecification_stock;
    private SpecificationsPopupWindow mPopupWindow;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_goods_detail;
    }

    @Override
    public GoodsDetailPresent initPresenter() {
        return new GoodsDetailPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
    }

    @Override
    protected void initData() {
        Bundle extras = getIntent().getExtras();
        mCommodityid = extras.getString("commodityid");
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        maxWidth = windowManager.getDefaultDisplay().getWidth();
        initScroll();
        showProgress();
        presenter.getData(this, mCommodityid);
    }

    /************************************about banner****************************************************/

    private int maxWidth;
    int mValue;
    int mWidth;
    int mHeight;
    private void initBanner(String comimgurl) {
        List<String> imgslun = new ArrayList<>();
        String[] split = comimgurl.split(",");

        for (int i = 0; i < split.length - 1; i++) {
            imgslun.add(split[i]);
        }

        ViewGroup.LayoutParams params = mGoodsBanner.getLayoutParams();
        params.width = maxWidth; // 宽度设置成屏幕宽度，这里根据自己喜好设置
        params.height = mValue; // 利用已知图片的宽高比计算高度
        mGoodsBanner.setLayoutParams(params);
        //设置banner样式
//        mGoodsBanner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        //设置图片加载器
        mGoodsBanner.setImageLoader(new GlideImageLoader());
        //设置视频加载
        mGoodsBanner.setVideoLoader(new IjkVideoLoader());
        //设置图片和视频集合
        mGoodsBanner.setImages(imgslun);
        //设置banner动画效果
//        mGoodsBanner.setBannerAnimation(Transformer.DepthPage);
        //设置标题集合（当banner样式有显示title时）
//        mGoodsBanner.setBannerTitles(titles);
        //设置自动轮播，默认为true
        mGoodsBanner.setOnBannerListener(this);
        mGoodsBanner.isAutoPlay(true);
        //设置指示器位置（当banner模式中有指示器时）
        mGoodsBanner.setIndicatorGravity(BannerConfig.RIGHT);
        //banner设置方法全部调用完毕时最后调用
        mGoodsBanner.start();


    }

    //如果你需要考虑更好的体验，可以这么操作
    @Override
    protected void onStart() {
        super.onStart();
        //开始轮播
        mGoodsBanner.startAutoPlay();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Log.e("onResume===", "onResume()");
        presenter.getCarSizeData(this, 1 + "");
    }

    @Override
    protected void onStop() {
        super.onStop();
        //结束轮播
        mGoodsBanner.stopAutoPlay();
    }


    @Override
    public void initEvent() {

    }

    /************************************set webView****************************************************/
    private void initWebView(String detailimgurl) {

        mGoodsWeb.getSettings().setBuiltInZoomControls(false);
        mGoodsWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mGoodsWeb.getSettings().setUseWideViewPort(true); //将图片调整到适合webview的大小
        mGoodsWeb.getSettings().setLoadWithOverviewMode(true); // 缩放至屏幕的大小
//        mGoodsWeb.setBackgroundColor(0);//设置背景色
//        mGoodsWeb.getBackground().setAlpha(0);//设置填充透明度（布局中一定要设置background，不然getbackground会是null）
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mGoodsWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            mGoodsWeb.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);//支持内容重新布局
        else
            mGoodsWeb.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        mGoodsWeb.getSettings().setLoadsImagesAutomatically(true);
//        mGoodsWeb.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                mGoodsWeb.measure(0, 0);
//                int measuredHeight = mGoodsWeb.getMeasuredHeight();
//                ViewGroup.LayoutParams layoutParams = mGoodsWeb.getLayoutParams();
//                layoutParams.height = measuredHeight;
//                mGoodsWeb.setLayoutParams(layoutParams);
//            }
//        });
        mGoodsWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

        mGoodsWeb.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                imgReset();

            }
        });


        String data = "<html><head><style>img{width:100% !important;}</style></head><body style='margin:0;padding:0'>" + detailimgurl + "</body></html>";
//margin:0;padding:0   去除webView自带padding边距
        mGoodsWeb.loadData(data, "text/html", "UTF-8");
    }

    private void imgReset() {
        mGoodsWeb.loadUrl("javascript:(function(){" +
                "var objs = document.getElementsByTagName('img'); " +
                "for(var i=0;i<objs.length;i++) " +
                "{"
                + "var img = objs[i]; " +
                " img.style.maxWidth = '100%'; img.style.height = 'auto'; " +
                "}" +
                "})()");
    }


    /************************************pager scroll****************************************************/

    //滑动显示标题栏
    @SuppressLint("NewApi")
    private void initScroll() {
        mScroll.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                int i = dip2px(GoodsDetailActivity.this, scrollY);
                if (i > 50) {
                    mScrollTitle.setVisibility(View.VISIBLE);
                    mImmersionBar.fitsSystemWindows(false).statusBarColor(R.color.transparent).statusBarDarkFont(true, 0f).init();
//                    mImmersionBar.keyboardEnable(true); //解决软键盘与底部输入框冲突问题;
//                    mImmersionBar.init();
                } else {
                    mScrollTitle.setVisibility(View.GONE);
                    mImmersionBar.fitsSystemWindows(false).statusBarColor(R.color.transparent).statusBarDarkFont(true, 0f).init();
//                    mImmersionBar.keyboardEnable(true); //解决软键盘与底部输入框冲突问题;
//                    mImmersionBar.init();
                }
                int px = dip2px(GoodsDetailActivity.this, 300);
                float v1 = scrollY * 1.0f / px;
                mScrollTitle.setAlpha(v1);
            }
        });
    }


    /************************************click method****************************************************/

    @OnClick({R.id.details_back_banner, R.id.details_back, R.id.add_car, R.id.details_cart_banner, R.id.goods_buy, R.id.go_top})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.details_back:
            case R.id.details_back_banner:
                finish();
                break;
            case R.id.add_car:
                initPopupWindow(0, view);
//                addCar();
                break;
            case R.id.details_cart_banner:
                ActivityUtils.next(GoodsDetailActivity.this, ShoppingCartActivity.class, false);
                break;
            case R.id.go_top:
                mScroll.smoothScrollTo(0, 0);
                break;
            case R.id.goods_buy:
                initPopupWindow(1, view);
//                ActivityUtils.next(GoodsDetailActivity.this, CheckConfirmActivity.class, false);
                break;
        }
    }

    //轮播图点击事件
    @Override
    public void OnBannerClick(int position) {
//        Toast.makeText(GoodsDetailActivity.this, "this==" + position, Toast.LENGTH_SHORT).show();
    }

    //规格的选择回传
    @Subscribe
    public void event(String event) {
        if (event.equals("select_change")) {
            List<String> mSelect_data = new ArrayList<>();
            mProd = "";
            for (int i = 0; i < mGoodsDetailBean.productSpecList.size(); i++) {
                for (int j = 0; j < mGoodsDetailBean.productSpecList.get(i).proList.size(); j++) {
                    if (mGoodsDetailBean.productSpecList.get(i).proList.get(j).isChecked) {
//                        Log.i("iseck===", mGoodsDetailBean.productSpecList.get(i).proList.get(j).specproid);
                        mSelect_data.add(mGoodsDetailBean.productSpecList.get(i).proList.get(j).specproid);
                    }
                }
            }

            for (int i = 0; i < mSelect_data.size(); i++) {
                if (i == 0) {
                    mProd = mSelect_data.get(i);
                } else {
                    mProd += "," + mSelect_data.get(i);
                }
            }

            presenter.searchPrice(this, mCommodityid, mProd);
        }

    }

    /************************************other method****************************************************/
    int goods_number = 1;
    Handler handler = new Handler();
    Runnable runnable;

    //加入购物车或立即购买的规格窗口
    private void initPopupWindow(int type, View view) {
        View inflate = LayoutInflater.from(this).inflate(R.layout.popup_specifications, null);

        ImageView specification_img = inflate.findViewById(R.id.specification_img);//商品图片
        EditText specification_num = inflate.findViewById(R.id.specification_num);//数量
        TextView specification_name = inflate.findViewById(R.id.specification_name);
        TextView sure_buy_now = inflate.findViewById(R.id.sure_buy_now);
        LinearLayout ll_button = inflate.findViewById(R.id.ll_button);

        ll_button.setVisibility(View.GONE);
        sure_buy_now.setVisibility(View.VISIBLE);
        //商品价格
        mSpecification_price = inflate.findViewById(R.id.specification_price);

        //库存
        mSpecification_stock = inflate.findViewById(R.id.specification_stock);
        RecyclerView specification_recycle = inflate.findViewById(R.id.specification_recycle);//商品规格列表
        goods_number = 1;

        SpecificationsAdapter adapter = new SpecificationsAdapter(this, mGoodsDetailBean.productSpecList);
        specification_recycle.setLayoutManager(new LinearLayoutManager(this));
        specification_recycle.setAdapter(adapter);

        Glide.with(this).load(mGoodsDetailBean.product.smallimgurl).into(specification_img);
        specification_name.setText(mGoodsDetailBean.product.name);
        mSpecification_price.setText(mGoodsDetailBean.product.hyprice + " USDN");
        mSpecification_stock.setText(getString(R.string.goods_details_stock) + " " + mSearchPriceBean.commodityspec.stock);

        inflate.findViewById(R.id.specification_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (goods_number == Integer.parseInt(mSearchPriceBean.commodityspec.stock)) {
//                    AlertDialogShowUtil.toastMessage(GoodsDetailActivity.this, getResources().getString(R.string.stock_less));
//                } else {
                goods_number++;
                String s1 = goods_number + "";
                specification_num.setText(goods_number + "");
                specification_num.setSelection(s1.length());
//                }
            }
        });
        inflate.findViewById(R.id.specification_minus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (goods_number == 1) {
                    String s1 = goods_number + "";
                    specification_num.setText(goods_number + "");
                    specification_num.setSelection(s1.length());
                    return;
                }
                goods_number--;
                String s1 = goods_number + "";
                specification_num.setText(goods_number + "");
                specification_num.setSelection(s1.length());
            }
        });
        specification_num.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (runnable != null) {
                    handler.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {

                        if (!specification_num.getText().toString().equals("")) {
                            if (Integer.parseInt(mSearchPriceBean.commodityspec.stock) < Integer.parseInt(specification_num.getText().toString().trim())) {
                                AlertDialogShowUtil.toastMessage(GoodsDetailActivity.this, getResources().getString(R.string.stock_less));
                                goods_number = Integer.parseInt(mSearchPriceBean.commodityspec.stock);
                                specification_num.setText(goods_number + "");
                                String s1 = goods_number + "";
                                specification_num.setSelection(s1.length());
                            }
//                            else {
//                                specification_num.setText(goods_number + "");
//                                String s1 = goods_number + "";
//                                specification_num.setSelection(s1.length());
//                            }
                        }
                    }
                };
                handler.postDelayed(runnable, 800);
            }
        });

        //加入购物车
        inflate.findViewById(R.id.sure_buy_now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 0) {
                    addCar();
                } else {
                    goAddOrder();
                }
            }
        });
        mPopupWindow = new SpecificationsPopupWindow(this, inflate);
        mPopupWindow.show(view, getWindow(), 0);
    }

    //立即购买点击
    private void goAddOrder() {
        mPopupWindow.dismiss();
        showProgress();
        presenter.goAddOrder(this, mSearchPriceBean.commodityspec.commodityspecid, goods_number + "");

    }

    //加入购物车点击
    private void addCar() {
        mPopupWindow.dismiss();
        showProgress();
        presenter.addCar(this, mSearchPriceBean.commodityspec.commodityspecid, goods_number + "");
    }

    //dip转px
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /************************************Call Interface callback****************************************************/

    @Override
    public void onSuccess(GoodsDetailBean bean) {//商品详情
        hideProgress();
        mGoodsDetailBean = bean;
        String[] split = bean.product.comimgurl.split(",");
        Glide.with(GoodsDetailActivity.this).asBitmap().load(split[0]).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                mWidth = bitmap.getWidth();
                mHeight = bitmap.getHeight();

                BigDecimal b1 = new BigDecimal(Double.toString(mWidth));
                BigDecimal b2 = new BigDecimal(Double.toString(mHeight));
                BigDecimal b3 = new BigDecimal(Double.toString(maxWidth));
                double divide = b2.divide(b1, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
                BigDecimal divide2 = new BigDecimal(Double.toString(divide));
                mValue = divide2.multiply(b3).intValue();
                initBanner(bean.product.comimgurl);
            }
        });
        for (int i = 0; i < mGoodsDetailBean.productSpecList.size(); i++) {
            for (int i1 = 0; i1 < mGoodsDetailBean.productSpecList.get(i).proList.size(); i1++) {
                if (i1 == 0) {
                    mGoodsDetailBean.productSpecList.get(i).proList.get(i1).isChecked = true;
                    if (mProd.equals("")) {
                        mProd = mGoodsDetailBean.productSpecList.get(i).proList.get(i1).specproid;
                    } else {
                        mProd += "," + mGoodsDetailBean.productSpecList.get(i).proList.get(i1).specproid;
                    }
                } else {
                    mGoodsDetailBean.productSpecList.get(i).proList.get(i1).isChecked = false;
                }
            }
        }

        presenter.searchPrice(this, mCommodityid, mProd);
        mCommodityspecid = bean.product.commodityspecid;
        mGoodsName.setText(bean.product.name);//名称
        mGoodsPrice.setText(bean.product.hyprice + " USDN");//价格
        fee.setText(bean.product.yunfei);
        brand.setText(bean.product.brandname);//品牌
        if (null != bean.product.combz && !"".equals(bean.product.combz)) {
            name_two.setText(bean.product.combz);//名称
        } else {
            marks_goods.setVisibility(View.GONE);
        }

        price_two.setText(bean.product.hyprice + " USDN");//价格
        goods_serial_number.setText(bean.product.conmnum);//商品编号
        if (bean.product.stock > 0) {
            have_goods.setText(getString(R.string.goods_have));
            have_goods_f.setVisibility(View.VISIBLE);
        } else {
            have_goods.setText(getString(R.string.goods_no));
            have_goods_f.setVisibility(View.GONE);
        }
        have_goods_f.setText(bean.product.distribution_time);

        initWebView(bean.product.detailimgurl);
    }

    @Override
    public void onAddCarSuccess(String bean) {//加入购物车
        hideProgress();
        AlertDialogShowUtil.toastMessage(GoodsDetailActivity.this, getString(R.string.add_car_goods));
//        AlertDialogShowUtil.toastMessage(GoodsDetailActivity.this, bean);
        presenter.getCarSizeData(this, 1 + "");
    }

    @Override
    public void onSearchPriceSuccess(SearchPriceBean bean) {//根据规格获取价格
        hideProgress();
        mSearchPriceBean = bean;
        if (null != mSpecification_price) {
            mSpecification_price.setText(mSearchPriceBean.commodityspec.hyprice + " USDN");
            mSpecification_stock.setText(getString(R.string.goods_details_stock) + " " + mSearchPriceBean.commodityspec.stock);
        }
    }

    @Override
    public void onCarSizeSuccess(CarListBean bean) {
        hideProgress();
        if (bean.cartList.size() == 0) {
            Glide.with(GoodsDetailActivity.this).load(R.mipmap.mall_car_nom).into(mDetailsCart);
            Glide.with(GoodsDetailActivity.this).load(R.mipmap.mall_car_nom).into(details_cart_banner);
        } else {
            Glide.with(GoodsDetailActivity.this).load(R.mipmap.mall_car_have_data).into(mDetailsCart);
            Glide.with(GoodsDetailActivity.this).load(R.mipmap.mall_car_have_data).into(details_cart_banner);
        }

    }

    @Override
    public void onAddOrderSuccess(GoAddOrderBean bean) {//准备立即购买
        hideProgress();
        Bundle bundle = new Bundle();
        bundle.putSerializable("details", bean);
        bundle.putInt("buy_type", 0);
        ActivityUtils.next(GoodsDetailActivity.this, CheckConfirmActivity.class, bundle, false);
    }

    @Override
    public void onFailure() {//数据请求失败
        hideProgress();
    }

}
