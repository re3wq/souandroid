package com.taiyi.soul.moudles.mine.setup.switchlan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.main.MainActivity;
import com.taiyi.soul.moudles.mine.bean.DataBean;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.language.ChangeLanguage;
import com.taiyi.soul.utils.language.LanguageUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class LanguageActivity extends BaseActivity<NormalView, NormalPresenter> implements MultiItemTypeAdapter.OnItemClickListener {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.rightTitleTv)
    TextView rightTitleTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.top)
    ConstraintLayout top;
    private List<DataBean> dataBeanList = new ArrayList<>();
    private int selectPosition = 0;
    private String mString = "";
    private String langs = "";
    String language = "";
    private Locale locale;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_language;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.language));
        rightTitleTv.setText(getString(R.string.save));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        mString = Utils.getSpUtils().getString("current_language", "");
        if ("".equals(mString)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                //获取系统语言
                locale = LanguageActivity.this.getResources().getConfiguration().getLocales().get(0);
                mString = locale.getLanguage();
            }
        }

        initLanguage();
        dataBeanList.add(new DataBean(R.mipmap.language_english_iv, getString(R.string.language_english)));
        dataBeanList.add(new DataBean(R.mipmap.language_french_iv, getString(R.string.language_french)));
        dataBeanList.add(new DataBean(R.mipmap.language_german_iv, getString(R.string.language_german)));
        dataBeanList.add(new DataBean(R.mipmap.language_spanish_iv, getString(R.string.language_spanish)));
        dataBeanList.add(new DataBean(R.mipmap.language_portuguese_iv, getString(R.string.language_portuguese)));
        dataBeanList.add(new DataBean(R.mipmap.language_russian_iv, getString(R.string.language_russian)));
//        dataBeanList.add(new DataBean(R.mipmap.language_arabic_iv, getString(R.string.language_arabic)));
        dataBeanList.add(new DataBean(R.mipmap.language_china_iv, getString(R.string.language_china)));
        dataBeanList.add(new DataBean(R.mipmap.language_china_iv, getString(R.string.language_simple_china)));
        dataBeanList.add(new DataBean(R.mipmap.language_japanese_iv, getString(R.string.language_japanese)));
        dataBeanList.add(new DataBean(R.mipmap.language_korean_iv, getString(R.string.language_korean)));

        Log.e("seee===","ss"+language);
        for (int i = 0; i < dataBeanList.size(); i++) {
            if (dataBeanList.get(i).name.equals(language)) {
                dataBeanList.get(i).isSelect = true;
            }
        }


        CommonAdapter setupLanguageListAdapter = AdapterManger.getSetupLanguageListAdapter(this, dataBeanList);
        recyclerView.setAdapter(setupLanguageListAdapter);
        setupLanguageListAdapter.setOnItemClickListener(this);


    }

    private void initLanguage() {
        if (mString.equals("en")) {//英语
            language = getString(R.string.language_english);
            selectPosition = 0;
            langs = "en";
        } else if (mString.equals("fr")) {//法语
            language = getString(R.string.language_french);
            selectPosition = 1;
            langs = "fra";
        } else if (mString.equals("de")) {//德语
            language = getString(R.string.language_german);
            selectPosition = 2;
            langs = "de";
        } else if (mString.equals("spa")) {//西班牙语
            language = getString(R.string.language_spanish);
            selectPosition = 3;
            langs = "spa";
        } else if (mString.equals("pt")) {//葡萄牙语
            language = getString(R.string.language_portuguese);
            selectPosition = 4;
            langs = "pt";
        } else if (mString.equals("ru")) {//俄语
            language = getString(R.string.language_russian);
            selectPosition = 5;
            langs = "ru";
        } else if (mString.equals("cht")) {//繁体中文
            language = getString(R.string.language_china);
            selectPosition = 6;
            langs = "cht";
        } else if (mString.equals("zh")) {//\中文
            if(null!=locale){
                if(locale.getCountry().equals("CN")){
                    language = getString(R.string.language_simple_china);
                    selectPosition = 7;
                    langs = "zh";
                }else {
                    language = getString(R.string.language_china);
                    selectPosition = 6;
                    langs = "cht";
                }
            }else {
                language = getString(R.string.language_simple_china);
                selectPosition = 7;
                langs = "zh";
            }


        } else if (mString.equals("ja")) {//日语
            language = getString(R.string.language_japanese);
            selectPosition = 8;
            langs = "jp";
        } else if (mString.equals("ko")) {//韩语
            language = getString(R.string.language_korean);
            selectPosition = 9;
            langs = "kor";
        }
//        Utils.getSpUtils().put("current_language", langs);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.rightTitleTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.rightTitleTv:
                boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.rightTitleTv);
                if (!fastDoubleClick) {
                    showProgress();
                    switch (selectPosition) {
                        case 0://英语
//                            Utils.getSpUtils().put("current_language", "en");
                            Utils.getSpUtils().put("current_language", "en");
                            break;
                        case 1://法语
//                            Utils.getSpUtils().put("current_language", "fr");
                            Utils.getSpUtils().put("current_language", "fra");
                            break;
                        case 2://德语
//                            Utils.getSpUtils().put("current_language", "de");
                            Utils.getSpUtils().put("current_language", "de");
                            break;
                        case 3://西班牙语
//                            Utils.getSpUtils().put("current_language", "ES");
                            Utils.getSpUtils().put("current_language", "spa");
                            break;
                        case 4://葡萄牙语
//                            Utils.getSpUtils().put("current_language", "PT");
                            Utils.getSpUtils().put("current_language", "pt");
                            break;
                        case 5://俄语
//                            Utils.getSpUtils().put("current_language", "RU");
                            Utils.getSpUtils().put("current_language", "ru");
                            break;
                        case 6://繁体中文
//                            Utils.getSpUtils().put("current_language", "TW");
                            Utils.getSpUtils().put("current_language", "cht");
                            break;
                        case 8://日语
//                            Utils.getSpUtils().put("current_language", "ja");
                            Utils.getSpUtils().put("current_language", "jp");
                            break;
                        case 9://韩语
//                            Utils.getSpUtils().put("current_language", "ko");
                            Utils.getSpUtils().put("current_language", "kor");
                            break;
                        case 7://简体中文
//                            Utils.getSpUtils().put("current_language", "zh");
                            Utils.getSpUtils().put("current_language", "zh");
                            break;

                    }

                    hideProgress();
                    Constants.LANGUAGEPOSITION = selectPosition + 1;
                    toSetLanguage(ChangeLanguage.getSetLanguageLocale());

                }

                break;
        }
    }

    private void toSetLanguage(Locale locale) {
        LanguageUtils.saveAppLocaleLanguage(locale.toLanguageTag());
        Context context = MyApplication.getInstance();
        actionActivity(context);
    }

    public static void actionActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
        for (int i = 0; i < dataBeanList.size(); i++) {
            if (position == i) {
                selectPosition = position;
                dataBeanList.get(position).setSelect(true);
            } else {
                dataBeanList.get(i).setSelect(false);
            }
        }
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }


}
