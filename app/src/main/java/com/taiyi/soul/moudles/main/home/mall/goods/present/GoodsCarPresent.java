package com.taiyi.soul.moudles.main.home.mall.goods.present;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.EditCarNumBean;
import com.taiyi.soul.moudles.main.home.mall.bean.ResultBean;
import com.taiyi.soul.moudles.main.home.mall.bean.ShopCarSubmitBean;
import com.taiyi.soul.moudles.main.home.mall.goods.EditCarBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class GoodsCarPresent extends BasePresent<GoodsCarView> {

    public void getData(Activity activity, String pagenum) {
        Map<String, String> map = new HashMap<>();
        map.put("pagenum", pagenum);
        HttpUtils.postRequest(BaseUrl.GOODS_CARLIST, this, map, new JsonCallback<BaseResultBean<CarListBean>>() {
            @Override
            public BaseResultBean<CarListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CarListBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<CarListBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    public void deleteCarData(Activity activity, String shopcarid) {
        Map<String, String> map = new HashMap<>();
        map.put("shopcarid", shopcarid);
        HttpUtils.postRequest(BaseUrl.CARTDEL, this, map, new JsonCallback<BaseResultBean<ResultBean>>() {
            @Override
            public BaseResultBean<ResultBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ResultBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onDeleteSuccess(response.body().msg_cn);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showShortToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ResultBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    public void editCarData(Activity activity, int type, int postion, String shopcarid, String number) {
        Map<String, String> map = new HashMap<>();
        map.put("shopcarid", shopcarid);
        map.put("number1", number);
        HttpUtils.postRequest(BaseUrl.CARTEDIT, this, map, new JsonCallback<BaseResultBean<EditCarBean>>() {
            @Override
            public BaseResultBean<EditCarBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EditCarBean>> response) {
                super.onSuccess(response);
                if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showLongToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();
                } else if (response.body().code == 2) {
                    if (null != view)
                        view.onEditNumSuccess(new EditCarNumBean(type, postion, response.body().code, response.body().msg_cn, response.body().data));
                } else {
                    if (null != view)
                    view.onEditNumSuccess(new EditCarNumBean(type, postion, response.body().code, response.body().msg_cn));
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EditCarBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //购物车准备立即下单
    public void addSaleData(Activity activity, String shopcarid) {

        Map<String, String> map = new HashMap<>();
        map.put("shopcarids", shopcarid);
        HttpUtils.postRequest(BaseUrl.GOADDORDER, this, map, new JsonCallback<BaseResultBean<ShopCarSubmitBean>>() {
            @Override
            public BaseResultBean<ShopCarSubmitBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<ShopCarSubmitBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onShopCarSubmitSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0) {
                        Constants.isL = 1;
                        ToastUtils.showLongToast(response.body().msg_cn);
//                   //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if (null != view)
                        view.onFailure();

                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<ShopCarSubmitBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

}
