package com.taiyi.soul.moudles.main.home.mall.goods.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.home.mall.bean.GoodsDetailBean;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 规格列表适配器
 */
public class SpecificationsItemAdapter extends RecyclerView.Adapter<SpecificationsItemAdapter.MyHolder> implements CompoundButton.OnCheckedChangeListener {
    private Context context;
    private List<GoodsDetailBean.ProductSpecListBean.ProListBean> data;
    private OnItemClickListener mOnItemClickListener = null;
    public SpecificationsItemAdapter(Context context, List<GoodsDetailBean.ProductSpecListBean.ProListBean> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.item_specifications_next, null);
        MyHolder myHolder = new MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, final int position) {
        holder.item_sp.setText(data.get(position).specProName);
        holder.item_sp.setChecked(data.get(position).isChecked);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        int position = (int) buttonView.getTag();

//        if(isChecked==true){
//            data.get(position).isChecked = isChecked;
//            for (int i = 0; i < data.size(); i++) {
//                if(i!=position){
//                    data.get(i).isChecked = false;
//                }
//            }
//            Log.i("isChecked===",isChecked+"  "+position);
//            notifyDataSetChanged();

//        }
    }


    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RadioButton item_sp;
        public MyHolder(View itemView) {
            super(itemView);
            item_sp = itemView.findViewById(R.id.item_sp);

            item_sp.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view , int position);
    }
    public void setItemClickListener(OnItemClickListener myItemClickListener) {
        this.mOnItemClickListener = myItemClickListener;
    }
}
