package com.taiyi.soul.moudles.main.home.mall.loader;

import android.content.Context;
import android.view.View;

import com.zk.banner.listener.OnVideoStateListener;
import com.zk.banner.loader.VideoLoader;

public class IjkVideoLoader extends VideoLoader {



    @Override
    public void displayView(Context context, Object path, View view, OnVideoStateListener listener) {
        /**
         注意：
         1.播放加载器由自己选择，这里不限制，这里使用JzvdStd播放
         2.返回的视频路径为Object类型,传输的到的是什么格式，那么这种就使用Object接收和返回，
         你只需要强转成你传输的类型就行，
         切记不要胡乱强转！
         */
//        eg：
//        String imgs = (String) SPUtils.get(context, "imgs", "");
//
//        LogUtils.e("imglunbo===" + path);
        MyJzvdStd jzVideo = (MyJzvdStd) view;

//        RequestOptions options = new RequestOptions()
//                .centerCrop()
//                .fitCenter()
//                .diskCacheStrategy(DiskCacheStrategy.NONE);
//        Glide.with(context).load(imgs).apply(options).into(jzVideo.thumbImageView);
//        jzVideo.setUp((String) path, "");
//        jzVideo.setOnVideoStateListener(listener);
//        jzVideo.bottomContainer.setVisibility(View.GONE);
//        jzVideo.fullscreenButton.setVisibility(View.GONE);
//        jzVideo.progressBar.setVisibility(View.GONE);
//        jzVideo.currentTimeTextView.setVisibility(View.GONE);
//        jzVideo.totalTimeTextView.setVisibility(View.GONE);
    }

    //提供createImageView 方法，如果不用可以不重写这个方法，主要是方便自定义播放view的创建
    @Override
    public View createView(Context context) {
        MyJzvdStd view = new MyJzvdStd(context);

        return view;
    }
}
