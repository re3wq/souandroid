package com.taiyi.soul.moudles.main.assets.currency.currencydetail.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.adapter.CurrencyDetailsOneAdapter;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.CurrencyFragmentBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean.UsdnOrNgkTransferRecordBean;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.present.CurrencyDetailsFPresent;
import com.taiyi.soul.moudles.main.assets.currency.currencydetail.present.CurrencyDetailsFView;
import com.taiyi.soul.utils.Utils;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import butterknife.BindView;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\19 0019 14:34
 * @Author : yuan
 * @Describe ：币种详情下方列表-全部
 */
public class CurrencyDetailsOneFragment extends BaseFragment<CurrencyDetailsFView, CurrencyDetailsFPresent> implements CurrencyDetailsFView, PullRecyclerView.PullLoadMoreListener {


    @BindView(R.id.recyclerView)
    PullRecyclerView recyclerView;
    @BindView(R.id.noDataLl)
    LinearLayout noDataLl;
    private CurrencyDetailsOneAdapter mFragmentAdapter;
    private int pageNo = 1;
    private String coinName;
    private String coinUrl;
    private List<CurrencyFragmentBean.ListBean> mList;
    private String mainAccount;
    private boolean mHidden;
    private List<CoinListBean> coinListBeans;
    private List<CurrencyFragmentBean.ListBean> list;

    public CurrencyDetailsOneFragment() {

    }

    public CurrencyDetailsOneFragment(String coinName, String coinUrl) {
        this.coinName = coinName;
        this.coinUrl = coinUrl;
    }


    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_currency;
    }


    @Override
    public CurrencyDetailsFPresent initPresenter() {
        return new CurrencyDetailsFPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if (EventBus.getDefault().isRegistered(false))
            EventBus.getDefault().register(this);
        mainAccount = Utils.getSpUtils().getString("mainAccount");
        presenter.getCoinList();
    }

    @Subscribe
    public void refresh(String msg) {
        if ("transferSuccess".equals(msg)) {
            recyclerView.refreshWithPull();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void even(String event) {
//        Log.e("fragment==", event);
        if ("refresh_anim".equals(event)) {
            mFragmentAdapter.notifyDataSetChanged();
            recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_anim));
            recyclerView.scheduleLayoutAnimation();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mHidden) {
            pageNo = 1;
//            if (coinName.equals("BKO") || coinName.equals("USDK")) {
//                presenter.getUsdnOrNgkTransferRecord(coinName, coinName.equals("BKO") ? Constants.NGK_CONTRACT_ADDRESS : Constants.USDN_CONTRACT_ADDRESS, String.valueOf(pageNo));
//            } else
            presenter.getDatas(getActivity(), coinName, pageNo, "");
        }
    }

    @Override
    protected void initData() {
        //实例化PullRecyclerView相关信息
//        presenter.getFindOrderData(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setOnPullLoadMoreListener(this);
        recyclerView.setIsRefreshEnabled(true);
        recyclerView.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty, null));
        mFragmentAdapter = new CurrencyDetailsOneAdapter(getContext(), coinUrl);
        mFragmentAdapter.setPullRecyclerView(recyclerView);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mFragmentAdapter);
//        recyclerView.refreshWithPull();
        pageNo = 1;
//        if (coinName.equals("BKO") || coinName.equals("USDK")) {
//            presenter.getUsdnOrNgkTransferRecord(coinName, coinName.equals("BKO") ? Constants.NGK_CONTRACT_ADDRESS : Constants.USDN_CONTRACT_ADDRESS, String.valueOf(pageNo));
//        } else

        presenter.getDatas(getActivity(), coinName, pageNo, "");
    }


    @Override
    public void initEvent() {


    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        mHidden = hidden;
//        Log.e("fragment==", "hidden");
        if (!hidden) {
//            if (coinName.equals("BKO") || coinName.equals("USDK")) {
//                presenter.getUsdnOrNgkTransferRecord(coinName, coinName.equals("BKO") ? Constants.NGK_CONTRACT_ADDRESS : Constants.USDN_CONTRACT_ADDRESS, String.valueOf(pageNo));
//            } else
            presenter.getDatas(getActivity(), coinName, pageNo, "");
        }
    }




    /************************************Other methods****************************************************/


    /************************************Refresh and load more****************************************************/

    @Override
    public void onRefresh() {
        pageNo = 1;
//        if (coinName.equals("BKO") || coinName.equals("USDK")) {
//            presenter.getUsdnOrNgkTransferRecord(coinName, coinName.equals("BKO") ? Constants.NGK_CONTRACT_ADDRESS : Constants.USDN_CONTRACT_ADDRESS, String.valueOf(pageNo));
//        } else
        presenter.getDatas(getActivity(), coinName, pageNo, "");
    }

    @Override
    public void onLoadMore() {
        pageNo++;
//        if (coinName.equals("BKO") || coinName.equals("USDK")) {
//            presenter.getUsdnOrNgkTransferRecord(coinName, coinName.equals("BKO") ? Constants.NGK_CONTRACT_ADDRESS : Constants.USDN_CONTRACT_ADDRESS, String.valueOf(pageNo));
//        } else
        presenter.getDatas(getActivity(), coinName, pageNo, "");
    }


    /************************************Call Interface callback****************************************************/
    @Override
    public void getSuccess(int status, String msg, CurrencyFragmentBean bean) {
        list = bean.list;
        if (status == 200) {
            if (pageNo == 1) {
                mFragmentAdapter.setDatas(bean.list);
            } else {
                mFragmentAdapter.addDatas(bean.list);
            }
        } else {
            noDataLl.setVisibility(View.VISIBLE);
        }

        recyclerView.setPullLoadMoreCompleted();
    }


    @Override
    public void getUsdnOrNgkRecordSuccess(UsdnOrNgkTransferRecordBean usdnOrNgkTransferRecordBean) {
//        recyclerView.setPullLoadMoreCompleted();
//        if (null != usdnOrNgkTransferRecordBean) {
//            if (usdnOrNgkTransferRecordBean.getData() != null) {
////                if (pageNo == 1)
////                    usdnOrNgkList.clear();
//                List<CurrencyFragmentBean.ListBean> usdnOrNgkList = new ArrayList<>();
//                for (UsdnOrNgkTransferRecordBean.DataBean datum : usdnOrNgkTransferRecordBean.getData()) {
//                    CurrencyFragmentBean.ListBean listBean = new CurrencyFragmentBean.ListBean();
//                    listBean.memo = datum.getMemo();
//                    listBean.coin_name = coinName;
//                    listBean.transfer_type = (!datum.getTo().equals(mainAccount)) ? "1" : "0";
//                    listBean.staus = 1;
//                    listBean.confirmations = "7";
//                    listBean.amount = datum.getQuantity();
////                    long l = DateUtils.dateToStamp(UTCStringtODefaultString(datum.getBlockTime()));
//                    String s = UTCStringtODefaultString(datum.getBlockTime());
//                    SimpleDateFormat defaultFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    try {
//                        Date date = defaultFormat.parse(s);
//                        long rightTime = (long) (date.getTime() + 8 * 60 * 60 * 1000);
//                        listBean.create_time = defaultFormat.format(rightTime);
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                    if (!datum.getTo().equals(mainAccount)) {//支出
//                        listBean.to_address = datum.getTo();
//                    } else {//收款
//                        listBean.out_address = datum.getFrom();
//                    }
//
//                    //to 与自己一样---加钱->收款
//                    usdnOrNgkList.add(listBean);
//                }
//                if (pageNo == 1) {
//                    mFragmentAdapter.setDatas(usdnOrNgkList);
//                } else {
//                    mFragmentAdapter.addDatas(usdnOrNgkList);
//                }
//
//            }
//        }
    }

    @Override
    public void onFailure() {
        hideProgress();
        noDataLl.setVisibility(View.VISIBLE);
    }

    @Override
    public void getCoinListSuccess(List<CoinListBean> coinListBeanList) {
        hideProgress();

        if (coinListBeanList != null) {
            for (int i = 0; i < coinListBeanList.size(); i++) {
                if(coinListBeanList.get(i).getCoinName().equals(coinName)){
                    Utils.getSpUtils().put("affirm",coinListBeanList.get(i).getAffirm());
                }
            }

        }
    }

    @Override
    public void getCoinListFailure(String errorMsg) {
        hideProgress();
    }


    public static String UTCStringtODefaultString(String UTCString) {
        try {
            if (!TextUtils.isEmpty(UTCString)) {
                UTCString = UTCString.replace("Z", " UTC");
                SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
                SimpleDateFormat defaultFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = utcFormat.parse(UTCString);
                return defaultFormat.format(date);
            } else {
                return null;
            }

        } catch (ParseException pe) {
            pe.printStackTrace();
            return null;
        }
    }

//    public static String formatTimeZoneToCST(String utcStr,String format){
//        if(StringUtils.isEmpty(format)){
//            format ="yyyy-MM-dd HH:mm:ss";
//        }
//        if(StringUtils.isEmpty(utcStr)){
//            return StringUtils.EMPTY;
//        }
//        String result;
//        ZonedDateTime parse = ZonedDateTime.parse(utcStr);
//        ZonedDateTime zonedDateTime = parse.withZoneSameInstant(ZoneId.of("Asia/Shanghai"));//中国标准时间 (北京)
//        result = zonedDateTime.format(DateTimeFormatter.ofPattern(format));
//        return result;
//    }

    public static String dealDate(String oldDateStr) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = null;
        try {
            date = df.parse(oldDateStr);
            SimpleDateFormat df1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
            Date date1 = df1.parse(date.toString());
            DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return df2.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


}
