package com.taiyi.soul.moudles.main.home.mall.order.present;

import android.app.Activity;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.home.mall.order.bean.CancelOrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.EditAddressBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderListPay;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：订单列表
 */
public class OrderPresent extends BasePresent<OrderView> {
    //订单列表
    public void getOrderList(Activity activity, String pagenum, String ordertype) {
        Map<String, String> map = new HashMap<>();
        map.put("pagenum", pagenum);
        map.put("ordertype", ordertype);//选传参数（ordertype订单状态 0:待付款 1:待发货:2待收货3:待评价-2:售后,-3已完成）
        HttpUtils.postRequest(BaseUrl.GOODSORDERLIST, this, map, new JsonCallback<BaseResultBean<OrderBean>>() {
            @Override
            public BaseResultBean<OrderBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<OrderBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onGetListSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(null!=activity){  if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(response.body().msg_cn);
                       //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);}
                    }
                } else if (response.body().code == 3080005) {
                    if(null!=activity){  if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                       //  AppManager.getAppManager().finishAllActivity();
                        ActivityUtils.next(activity, LoginActivity.class, true);
                        Utils.getSpUtils().remove(Constants.TOKEN);}
                    }
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }


            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<OrderBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //取消订单
    public void getCancelOrder(Activity activity, String ordercommodityid) {
        Map<String, String> map = new HashMap<>();
        map.put("ordercommodityid", ordercommodityid);
        HttpUtils.postRequest(BaseUrl.ORDERCANCEL, this, map, new JsonCallback<BaseResultBean<CancelOrderBean>>() {
            @Override
            public BaseResultBean<CancelOrderBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CancelOrderBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onCancelOrderSuccess(response.body().msg_cn);
                } else if (response.body().code == -1) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                }else if (response.body().code == 3080005) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }


            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<CancelOrderBean>> response) {
                super.onError(response);
            }
        });
    }

    //修改地址
    public void getEditOrderAddress(Activity activity, String ordercommodityid, String addressid) {
        Map<String, String> map = new HashMap<>();
        map.put("ordercommodityid", ordercommodityid);
        map.put("addressid", addressid);
        HttpUtils.postRequest(BaseUrl.EDITADDRESS, this, map, new JsonCallback<BaseResultBean<EditAddressBean>>() {
            @Override
            public BaseResultBean<EditAddressBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EditAddressBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onEditAddressSuccess(response.body().msg_cn);
                } else if (response.body().code == -1) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                }else if (response.body().code == 3080005) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }


            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EditAddressBean>> response) {
                super.onError(response);
            }
        });
    }

    //提醒发货
    public void getRemindDelivery(Activity activity, String ordercommodityid) {
        Map<String, String> map = new HashMap<>();
        map.put("ordercommodityid", ordercommodityid);
        HttpUtils.postRequest(BaseUrl.TXEX, this, map, new JsonCallback<BaseResultBean<EditAddressBean>>() {
            @Override
            public BaseResultBean<EditAddressBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EditAddressBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onRemindDeliverySuccess(response.body().msg_cn);
                } else if (response.body().code == -1) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else if (response.body().code ==3080005) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }


            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EditAddressBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

   //去支付
    public void getZhiFuLie(Activity activity, String ordercommodityid) {
        Map<String, String> map = new HashMap<>();
        map.put("ordercommodityid", ordercommodityid);
        map.put("paytype", "1");
        HttpUtils.postRequest(BaseUrl.ZHIFULIE, this, map, new JsonCallback<BaseResultBean<OrderListPay>>() {
            @Override
            public BaseResultBean<OrderListPay> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<OrderListPay>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onZhiFuLieSuccess(response.body().data);
                } else if (response.body().code == -1) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                }  else if (response.body().code == 3080005) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }


            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<OrderListPay>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }
    //订单确认收货
    public void getTakeDeliveryOrder(Activity activity, String ordercommodityid) {
        Map<String, String> map = new HashMap<>();
        map.put("ordercommodityid", ordercommodityid);
        HttpUtils.postRequest(BaseUrl.AFFIRMORDERGOODS, this, map, new JsonCallback<BaseResultBean<CancelOrderBean>>() {
            @Override
            public BaseResultBean<CancelOrderBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CancelOrderBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onTakeDeliverySuccess(response.body().msg_cn);
                } else if (response.body().code == -1) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                }  else if (response.body().code == 3080005) {  if(Constants.isL==0){
                    Constants.isL=1;
                    ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }

            }


            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<CancelOrderBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }


}
