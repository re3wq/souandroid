package com.taiyi.soul.moudles.main.assets.exchange.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\11 0011 15:29
 * @Author : yuan
 * @Describe ：
 */
public class BondRecordBean {


        public int totalpage;
        public int rowcount;
        public List<ComlistBean> comlist;

        public static class ComlistBean {
            public String charge;
            public String create_time;
            public String createtime;
            public String order_name;
            public String orderid;
            public String num;
            public int source;
            public boolean type;
            public String coin_name;
            public String money;
            public long user_id;
            public String price;
            public long id;
            public int state;
            public String hash;
            public String ifcharge;
            public int positotals;

    }
}
