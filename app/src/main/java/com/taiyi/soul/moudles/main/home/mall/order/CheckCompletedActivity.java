package com.taiyi.soul.moudles.main.home.mall.order;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import butterknife.BindView;
import butterknife.OnClick;

public class CheckCompletedActivity extends BaseActivity<NormalView, NormalPresenter> {
    @BindView(R.id.tv_title)
    TextView mTvTitle;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_completed;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mTvTitle.setText(getString(R.string.check_order_title));

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.iv_back, R.id.check_order_find})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.check_order_find:
                Bundle bundle = new Bundle();
                bundle.putInt("type",1);
                ActivityUtils.next(CheckCompletedActivity.this, OrderActivity.class,bundle, true);

                break;

        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }
}
