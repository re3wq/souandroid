package com.taiyi.soul.moudles.main.home.mall.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\5\15 0015 17:32
 * @Author : yuan
 * @Describe ：
 */
public class DemoOrderBean {
    public List<GoodBean> goods;
    public int orderType;
    public String orderNum;
    public String orderPrice;


    public static class GoodBean {
        public String img;
        public String name;
        public String price;
        public String disprice;
        public String num;
        public String specification;
    }
}
