package com.taiyi.soul.moudles.main.deal.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.deal.bean.EntrustOneBean;
import com.taiyi.soul.utils.NumUtils;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class EntrustOneAdapter extends BaseRecyclerAdapter<EntrustOneBean.ComlistBean>  {


    private PullRecyclerView pullRecyclerView;
    private Context context;
    ItemClick itemClick;
    public EntrustOneAdapter(Context context,ItemClick itemClick) {
        this.context = context;
        this.itemClick = itemClick;

    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_entrust;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final EntrustOneBean.ComlistBean data) {
        TextView entrust_time = getView(holder,R.id.entrust_time);
        TextView entrust_total =getView(holder,R.id.entrust_total);
        TextView entrust_amount =getView(holder,R.id.entrust_amount);
        TextView entrust_price = getView(holder,R.id.entrust_price);
        TextView entrust_pay = getView(holder,R.id.entrust_pay);
        TextView entrust_total_unit =getView(holder,R.id.entrust_total_unit);
        TextView entrust_amount_unit = getView(holder,R.id.entrust_amount_unit);
        TextView entrust_price_unit =getView(holder,R.id.entrust_price_unit);
        TextView entrust_charge =getView(holder,R.id.entrust_charge);
        TextView entrust_cancel =getView(holder,R.id.entrust_cancel);
        TextView issue_num =getView(holder,R.id.issue_num);
        issue_num.setText(context.getResources().getString(R.string.issue_num_two));

        if(data.flag==0){//买入
            entrust_pay.setBackgroundResource(R.mipmap.deal_item_buy);
            entrust_pay.setText(R.string.deal_bt_pay);
        }
        if(data.flag==1){//卖出
            entrust_pay.setBackgroundResource(R.mipmap.deal_item_sell);
            entrust_pay.setText(R.string.deal_bt_sell);
        }
        entrust_time.setText(data.createtime);
        entrust_total.setText(NumUtils.subZeroAndDot(data.dealmoney));//总价格
        entrust_amount.setText(data.postersnum+"");//数量
        entrust_price.setText(NumUtils.subZeroAndDot(data.price));//单价
        entrust_charge.setText( data.charge);//手续费
        entrust_cancel.setText(context.getResources().getString(R.string.entrust_cancel));
        entrust_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                      if(itemClick!=null){
                          itemClick.onItemClick(position);
                      }
            }
        });
        entrust_price_unit.setText(context.getResources().getString(R.string.USDN));//单位
        entrust_total_unit.setText(context.getResources().getString(R.string.USDN));//总价单位
//        entrust_total_unit.setText(data.total_type);
//        entrust_amount_unit.setText(data.num_type);
//        entrust_price_unit.setText(data.price_type);

    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }
    //点击
    public interface ItemClick {
        void onItemClick(int position);
    }
}

