package com.taiyi.soul.moudles.main.assets.bean;

import java.util.List;

public class TransferBalanceBean {

    /**
     * balance : 0.0
     * list : [{"id":4,"coinName":"ETH","free":1},{"id":5,"coinName":"ETH","free":2},{"id":6,"coinName":"ETH","free":3}]
     */

    private String balance;
    private List<ListBean> list;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 4
         * coinName : ETH
         * free : 1.0
         */

        private int id;
        private String coinName;
        private String free;
        private String freeStr;
        private boolean isDe;

        public String getFreeStr() {
            return freeStr;
        }

        public void setFreeStr(String freeStr) {
            this.freeStr = freeStr;
        }

        public boolean isDe() {
            return isDe;
        }

        public void setDe(boolean de) {
            isDe = de;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCoinName() {
            return coinName;
        }

        public void setCoinName(String coinName) {
            this.coinName = coinName;
        }

        public String getFree() {
            return free;
        }

        public void setFree(String free) {
            this.free = free;
        }
    }
}
