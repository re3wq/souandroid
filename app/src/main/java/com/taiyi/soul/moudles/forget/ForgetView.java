package com.taiyi.soul.moudles.forget;

import com.taiyi.soul.bean.AreaBean;

import java.util.List;

public interface ForgetView {
    void  getVerificationCodeSuccess();
    void getInvitePersonFailure(String errorMsg);

    void updateSuccess();

    void updateFailure(String errorMsg);

    void getAreaCodeSuccess(List<AreaBean>list);
}
