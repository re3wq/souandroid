package com.taiyi.soul.moudles.main.assets.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.taiyi.soul.R;
import com.taiyi.soul.utils.DensityUtil;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\6\1 0001 16:20
 * @Author : yuan
 * @Describe ：
 */
public class CurrencyPopupWindow extends PopupWindow {
    private Context mContext;
    private Window mWindow;

    public CurrencyPopupWindow(Context context, final View view) {
        super(context);
        this.mContext = context;
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN|View.INVISIBLE);//沉浸式
        // 设置自定义PopupWindow的View
        this.setContentView(view);

        // 设置自定义PopupWindow弹出窗体的宽
        this.setWidth(DensityUtil.dip2px(context,134));
        // 设置自定义PopupWindow弹出窗体的高
        this.setHeight(DensityUtil.dip2px(context,421));
        // 设置自定义PopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 设置自定义PopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.shop_popup_window_anim);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(00000000);
        // 设置自定义PopupWindow弹出窗体的背景
//        this.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bg_deal_popup));
        this.setBackgroundDrawable(dw);
        // popupwindow消失，恢复透明度
//        this.setOnDismissListener(new OnDismissListener() {
//
//            @Override
//            public void onDismiss() {
//                WindowManager.LayoutParams lp = mWindow.getAttributes();
//                lp.alpha = 1f;
//                mWindow.setAttributes(lp);
//            }
//        });
    }

    /**
     * 显示dlg，window用来设置背景变暗
     *
     * @param parent
     * @param typ
     */
    public void show(View parent, Window window, int typ) {

        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            if (typ == 1) {
                showAtLocation(parent, Gravity.TOP, 0, 0);
            } else
                showAtLocation(parent, Gravity.BOTTOM, 0, 0);

            mWindow = window;
            // 设置背景变暗
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.alpha = 0.2f;
            window.setAttributes(lp);
        } else {
            this.dismiss();
        }

    }

    /**
     * 显示dlg，window用来设置背景变暗
     *
     * @param parent
     */
    public void show(View parent, Window window) {

        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            showAsDropDown(parent);

            mWindow = window;
            // 设置背景变暗
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.alpha = 0.2f;
            window.setAttributes(lp);
        } else {
            this.dismiss();
        }

    }
}
