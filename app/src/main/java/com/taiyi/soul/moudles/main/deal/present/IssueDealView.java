package com.taiyi.soul.moudles.main.deal.present;

import com.taiyi.soul.moudles.main.deal.bean.IssueDealTotalBean;
import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;

public interface IssueDealView {
    void onSuccess(int code, String msg_cn, IssueDealTotalBean bean);
    void onCheckPayPasswordSuccess(int code, String bean);
    void entryOrdersSuccess(int code, String bean);
    void getBalanceSuccess(String symbol,String balance);
    void onIssueFeeSuccess(IssueFeeBean bean);
}
