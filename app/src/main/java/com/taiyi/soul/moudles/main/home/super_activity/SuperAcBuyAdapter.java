package com.taiyi.soul.moudles.main.home.super_activity;

import android.content.Context;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class SuperAcBuyAdapter extends BaseRecyclerAdapter<AirListBean.ComlistBean>  {


    private PullRecyclerView pullRecyclerView;
    private Context context;

    public SuperAcBuyAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.node_buy_item_layout;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final AirListBean.ComlistBean data) {

        TextView dateTv = getView(holder,R.id.dateTv);
        TextView priceTv = getView(holder,R.id.priceTv);
        TextView quantityTv = getView(holder,R.id.quantityTv);
        TextView amountTv = getView(holder,R.id.amountTv);


        dateTv.setText(data.createtime);
        priceTv.setText(data.price);
        quantityTv.setText(data.num);
        amountTv.setText(data.money);
    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }

}

