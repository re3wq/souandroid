package com.taiyi.soul.moudles.main.home.mall.order.present;

import android.app.Activity;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.home.mall.order.bean.FreightBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.GoodsOrderBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\7\1 0001 17:34
 * @Author : yuan
 * @Describe ：确认订单获取运费
 */
public class CheckConfirmPresent extends BasePresent<CheckConfirmView> {

    //验证交易密码
    public void getFee(Activity activity,String addressid, String commodityspecids) {
        Map<String, String> map = new HashMap<>();
        map.put("addressid", addressid);//地址id
        map.put("commodityspecids", commodityspecids);//规格id逗号拼接
        HttpUtils.postRequest(BaseUrl.YUNFEI, this, map, new JsonCallback<BaseResultBean<FreightBean>>() {
            @Override
            public BaseResultBean<FreightBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<FreightBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if(null!=view)
                        view.onGetFeeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(null!=view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                }  else if (response.body().code == 3080005) {
                    if(null!=view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                } else {
                    if (null != view)
                        view.onFailure();
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<FreightBean>> response) {
                super.onError(response);
                if(null!=view)
                    view.onFailure();
            }
        });
    }

    //详情下单
    public void goodsOrder(Activity activity,String commodityspecid,String paytype,String number,String addressid,String remark) {
        Map<String, String> map = new HashMap<>();

        map.put("commodityspecid", commodityspecid);
        map.put("paytype", paytype); //支付类型1微信2支付宝3余额
        map.put("number1", number);
        map.put("addressid", addressid);
        map.put("remark", remark);
        HttpUtils.postRequest(BaseUrl.GOODADDORDER, this, map, new JsonCallback<BaseResultBean<GoodsOrderBean>>() {
            @Override
            public BaseResultBean<GoodsOrderBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<GoodsOrderBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if(null!=view)
                        view.onGoodsBuySuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if(null!=view)
                    view.onFailure();
                } else if (response.body().code == 3080005) {
                    if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                    }
                    if(null!=view)
                    view.onFailure();
                } else {
                    if(null!=view)
                        view.onFailure();
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }

            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<GoodsOrderBean>> response) {
                super.onError(response);
                if(null!=view)
                    view.onFailure();
            }
        });
    }

    //购物车下单
    public void goodsCarOrder(Activity activity,String shopcarids,String paytype,String addressid,String remark) {
        Map<String, String> map = new HashMap<>();
        map.put("shopcarids", shopcarids);
        map.put("paytype", paytype); //支付类型1微信2支付宝3余额
        map.put("addressid", addressid);
        map.put("remark", remark);
        HttpUtils.postRequest(BaseUrl.ADDORDER, this, map, new JsonCallback<BaseResultBean<GoodsOrderBean>>() {
            @Override
            public BaseResultBean<GoodsOrderBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<GoodsOrderBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if(null!=view)
                        view.onGoodsCarBuySuccess(response.body().data);
                } else if (response.body().code == -1) {
                    if(null!=view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                } else if (response.body().code ==3080005) {
                    if(null!=view)
                        view.onFailure();  if(Constants.isL==0){
                        Constants.isL=1;
                        ToastUtils.showShortToast(activity.getString(R.string.account_status));
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);}
                } else {
                    if(null!=view)
                        view.onFailure();
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }


            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<GoodsOrderBean>> response) {
                super.onError(response);
                if(null!=view)
                    view.onFailure();
            }
        });
    }


}
