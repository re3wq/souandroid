package com.taiyi.soul.moudles.main.assets.currency.currencydetail.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\19 0019 15:28
 * @Author : yuan
 * @Describe ：
 */
public class CurrencyFragmentBean {

        public int total;
        public List<ListBean> list;
        public static class ListBean {
            public String id;
            public String userId;
            public String walletId;
            public String coin_name;
            public String out_address;
            public String to_address;
            public String amount;
            public String money;
            public String addr;
            public String transfer_type;
            public String hash;
            public String memo;
            public String create_time;
            public String source;
            public String moneyall;
            public String precisio;
            public String num;
            public String upTime;
            public String staus;
            public String free;
            public String type;
            public String state;
            public String confirmations;

    }
}
