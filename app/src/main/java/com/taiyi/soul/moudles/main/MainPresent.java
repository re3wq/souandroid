package com.taiyi.soul.moudles.main;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.mine.bean.VersionUpdateBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

class MainPresent extends BasePresent<MainView> {
    public void checkVersionUpdate(){
        Map<String,String> map=new HashMap<>();
        map.put("deviceType","0");
        HttpUtils.getRequets(BaseUrl.VERSION_UPDATE, this, map, new JsonCallback<BaseResultBean<VersionUpdateBean>>() {
            @Override
            public BaseResultBean<VersionUpdateBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<VersionUpdateBean>> response) {
                super.onSuccess(response);
                if (null!=view) {
                    if (response.body().status == 200) {
                        view.checkVersionUpdateSuccess(response.body().data);
                    } else {
                        view.checkVersionUpdateFailure(response.body().status,response.body().msg);
                    }
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<VersionUpdateBean>> response) {
                super.onError(response);
            }
        });
    }

}
