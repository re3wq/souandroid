package com.taiyi.soul.moudles.main.assets.exchange.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\8 0008 16:01
 * @Author : yuan
 * @Describe ：
 */
public class ExchangeRateBean {

        public String pay_coin_name;//币种单位（USDN与NGK获取获取签名需要）
        public String pay_money_all;//签名价格
        public String have_coin_name;
        public String pay_money;
        public String have_money;
        public String pay_charge;
        public String exchange_rate;
        //{"code":2,"msg":"Success !!","msg_cn":"余额不足","data":{"pay_coin_name":"ETH","pay_money_all":105,"have_coin_name":"USDK","pay_money":100,"have_money":950.0000,"pay_charge":5}}
}
