package com.taiyi.soul.moudles.main.node.hashmarket.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseFragment;
import com.taiyi.soul.moudles.main.deal.adapter.DealPayAdapter;
import com.taiyi.soul.moudles.main.deal.bean.EntrustOneBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustThreeBean;
import com.taiyi.soul.moudles.main.deal.bean.EntrustTwoBean;
import com.taiyi.soul.moudles.main.deal.view.DealPopupWindow;
import com.taiyi.soul.moudles.main.node.hashmarket.adapter.HashOneAdapter;
import com.taiyi.soul.moudles.main.node.hashmarket.present.HashPresent;
import com.taiyi.soul.moudles.main.node.hashmarket.present.HashView;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import butterknife.BindView;

/**
 * Created by Android Studio.
 * @Date ： 2020\7\8 0008 11:41
 * @Author : yuan
 * @Describe ：算力市场-交易记录-当前委托
 */
public class HashOneFragment extends BaseFragment<HashView, HashPresent> implements HashView, DealPayAdapter.DealItemClick, PullRecyclerView.PullLoadMoreListener, HashOneAdapter.ItemClick {


    @BindView(R.id.pull_recycle)
    PullRecyclerView mPayRecycle;

    private boolean node_hidden;

    private HashOneAdapter mFragmentAdapter;
    private int pageNo = 1;
    private EntrustOneBean mEntrustOneBean;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_entrust_one;
    }


    @Override
    public HashPresent initPresenter() {
        return new HashPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        if(EventBus.getDefault().isRegistered(false)){

            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void initData() {

        mPayRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        mPayRecycle.setOnPullLoadMoreListener(this);
        mPayRecycle.setEmptyView(LayoutInflater.from(getContext()).inflate(R.layout.layout_empty,null));
        mFragmentAdapter = new HashOneAdapter(getContext(),this);
        mFragmentAdapter.setPullRecyclerView(mPayRecycle);
        mPayRecycle.setItemAnimator(new DefaultItemAnimator());
        mFragmentAdapter.notifyDataSetChanged();
        mPayRecycle.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getContext(),R.anim.layout_anim));
        mPayRecycle.scheduleLayoutAnimation();
        mPayRecycle.setAdapter(mFragmentAdapter);
        mPayRecycle.refreshWithPull();


    }



    @Override
    public void initEvent() {

    }



    @Subscribe
   public void event(String event){

   }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
//        Log.e("fragment==","hidden");
        node_hidden = hidden;
        if (!hidden) {
            presenter.getData(getActivity(),"0",pageNo+"",mPayRecycle);
        }
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.fitsSystemWindows(false).statusBarDarkFont(false, 0f).init();

    }



    @Override
    public void onDeal(int position) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.popup_deal, null);
        DealPopupWindow dealPopupWindow = new DealPopupWindow(getContext(),view);
        ((TextView)view.findViewById(R.id.deal_popup_title)).setText(getString(R.string.deal_popu_buy_title));
        ((TextView)view.findViewById(R.id.deal_popup_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealPopupWindow.dismiss();
            }
        });
        ((TextView)view.findViewById(R.id.deal_popup_sure)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealPopupWindow.dismiss();

            }
        });

        dealPopupWindow.show(getView(),getActivity().getWindow(),1);
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        presenter.getData(getActivity(),"0",pageNo+"",mPayRecycle);
    }

    @Override
    public void onLoadMore() {
        pageNo ++;
        presenter.getData(getActivity(),"0",pageNo+"",mPayRecycle);
    }

    @Override
    public void onSuccess(int code, String msg_cn, EntrustOneBean bean) {
        mEntrustOneBean = bean;
        if(code==0){
            if(pageNo==1){
                mFragmentAdapter.setDatas(bean.comlist);
            }else {
                mFragmentAdapter.addDatas(bean.comlist);
            }
        }else {
            AlertDialogShowUtil.toastMessage(getContext(),msg_cn);
        }
        mPayRecycle.setPullLoadMoreCompleted();
    }

    @Override
    public void onTwoSuccess(int code, String msg_cn, EntrustTwoBean bean) {

    }

    @Override
    public void onThreeSuccess(int code, String msg_cn, EntrustThreeBean bean) {

    }

    @Override
    public void onCancelSuccess(int code, String data) {
        AlertDialogShowUtil.toastMessage(getContext(),data);
        mPayRecycle.refreshWithPull();
    }

    @Override
    public void onItemClick(int position) {
        Dialog dialog = new Dialog(getContext(), R.style.MyDialog);
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.dialog_cancel_order, null);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        inflate.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                String postersid = mEntrustOneBean.comlist.get(position).postersid;
                presenter.cancelOrder(getActivity(),postersid);
            }
        });
        dialog.show();

    }
}
