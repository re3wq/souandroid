package com.taiyi.soul.moudles.login;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.AppManager;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.gen.WalletBeanDao;
import com.taiyi.soul.moudles.face.RegisterAndRecognizeActivity;
import com.taiyi.soul.moudles.forget.ForgetActivity;
import com.taiyi.soul.moudles.login.bean.AreaCodeBean;
import com.taiyi.soul.moudles.login.bean.UserInfoBean;
import com.taiyi.soul.moudles.main.MainActivity;
import com.taiyi.soul.moudles.mine.accountmanagement.create.CreateAccountActivity;
import com.taiyi.soul.moudles.register.RegisterActivity;
import com.taiyi.soul.moudles.register.bean.RegisterBean;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ButtonUtils;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.keyboard.SafeKeyboard;
import com.taiyi.soul.utils.keyboard.ShowKeyboard;
import com.taiyi.soul.view.AreaCodePopupWindow;
import com.taiyi.soul.view.countdowntimer.CountDownTimerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity<LoginView, LoginPresent> implements AreaCodePopupWindow.OnItemClick, LoginView {

    @BindView(R.id.tv_code)
    TextView tvCode;

    @BindView(R.id.iv)
    ImageView iv;

    @BindView(R.id.ll_code)
    LinearLayout llCode;

    @BindView(R.id.iv_email)
    ImageView ivEmail;

    @BindView(R.id.iv_phone)
    ImageView ivPhone;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.tv_forget)
    TextView tvForget;

    @BindView(R.id.tv_register)
    TextView tvRegister;

    @BindView(R.id.ll_phone)
    LinearLayout llPhone;

    @BindView(R.id.ll_hide)
    LinearLayout llHide;


    @BindView(R.id.key_main)
    RelativeLayout key_main;
    @BindView(R.id.key_scroll)
    LinearLayout key_scroll;
    @BindView(R.id.keyboardPlace)
    LinearLayout keyboardPlace;


    /**
     * 0---手机号登录
     * 1---邮箱登录
     */
    private int loginType = 0;

    private int loginFastType = 0;

    private Dialog mDialog;
    private Dialog mFaceDialog;
    private Dialog googleDialog;

    private int isnotpassword = 0;//密码登陆 0   指纹面部识别1
    private ArrayList<AreaCodeBean> list = new ArrayList<>();
    //    private boolean isOpenFinger;
//    private boolean isFace;
    private String userInfo;
    private CancellationSignal cancellationSignal;
    private FingerprintManager fingerprintManger;
    private Handler handler = new Handler() {

        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1://验证错误
                    if(isclosseFinger!=1)
                    ToastUtils.showShortToast(getResources().getString(R.string.check_err));
                    isclosseFinger = 0;
                    break;
                case 2://指纹验证成功
                case 12://人脸验证成功
                    ToastUtils.showShortToast(getResources().getString(R.string.check_success));
                    mDialog.dismiss();

                    showProgress();
//                    Log.e("zzzz===", loginPwd + "===");
                    loginFastType = 1;
                    isnotpassword = 1;
                    closeF(loginAccount);
                    if (loginType == 0) {//手机号
                        presenter.login(tvCode.getText().toString(), loginAccount, loginPwd, "");
                    } else {//邮箱
                        presenter.login("", loginAccount, loginPwd, "");
                    }
                    break;
                case 3://指纹验证失败
                case 13://人脸验证失败
                    ToastUtils.showShortToast(getResources().getString(R.string.check_fail));
                    break;
            }
        }
    };
    private String loginAccount;
    private String loginPwd;
    private int face_cuccess;
    private int isShowFace;
    private View faceView;
    private View googleView;
    private TextView safety_code;
    private LoginPopupWindow popupWindow;
    private TextView safety_hint;
    private TextView close;
    private EditText safety_input_hint;
    private TextView safety;
    private RelativeLayout key_mains;
    private SafeKeyboard safeKeyboard;
    private SafeKeyboard safeKeyboard2;
    private int isclosseFinger=0;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_login_new;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        initView();
        Constants.isL = 0;
        AppManager.getAppManager().finishAllZActivity();

    }


    protected void initKeyBoard() {
        List<EditText> list = new ArrayList<>();
        list.add(etPhone);
        list.add(etEmail);
        list.add(etPassword);
        etPhone.setInputType(InputType.TYPE_CLASS_NUMBER);
        safeKeyboard = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_main, key_scroll, list, true);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
            View v = getCurrentFocus();

            if (isShouldHideInput(v, ev)) {
                hideSoftInput(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (event.getY() > top) {
//                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                if (null != safeKeyboard && safeKeyboard.isShow()) {
                    return true;
                } else {
                    return false;
                }

            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     *
     * @param token
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
                safeKeyboard.hideKeyboard();
            }

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
            safeKeyboard.hideKeyboard();
            return true;
        }
        if (safeKeyboard2 != null && safeKeyboard2.stillNeedOptManually(false)) {
            safeKeyboard2.hideKeyboard();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initaccount() {
        loginType = Utils.getSpUtils().getInt("loginType", 0);
        if (loginType == 0) {//手机号
            llPhone.setVisibility(View.VISIBLE);
            etEmail.setVisibility(View.GONE);
            String phoneNumber = Utils.getSpUtils().getString("phoneNumber");
            if (!TextUtils.isEmpty(phoneNumber)) {
                etPhone.setText(phoneNumber);
            }
            ivEmail.setImageResource(R.mipmap.iv_login_email);
            ivPhone.setImageResource(R.mipmap.iv_login_phone);
        } else {//邮箱
            llPhone.setVisibility(View.GONE);
            etEmail.setVisibility(View.VISIBLE);
            String email = Utils.getSpUtils().getString("email");
            if (!TextUtils.isEmpty(email)) {
                etEmail.setText(email);
            }
            ivEmail.setImageResource(R.mipmap.iv_login_email_ed);
            ivPhone.setImageResource(R.mipmap.iv_login_phone_un);
        }
    }

    @Override
    protected void initData() {

        initaccount();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initKeyBoard();
            }
        }, 600);

    }


    @Override
    public void initEvent() {
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (loginType == 0) {
                    if (!"".equals(s)) {
                        Utils.getSpUtils().put("phoneNumber", s.toString());
                    }
                } else {
                    if (!"".equals(s)) {
                        Utils.getSpUtils().put("email", s.toString());
                    }


                }
            }
        });

        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                if (safeKeyboard != null && safeKeyboard.stillNeedOptManually(false)) {
//                    safeKeyboard.hideKeyboard();
//                }
                String account = "";
                if (loginType == 0) {
                    String phoneNumber = etPhone.getText().toString();
                    if (TextUtils.isEmpty(phoneNumber)) {
                        return;
                    }
                    account = phoneNumber;
                } else {
                    String email = etEmail.getText().toString();
                    if (TextUtils.isEmpty(email)) {
                        return;
                    }
                    account = email;
                }

                if (hasFocus) {
                    if (!TextUtils.isEmpty(userInfo)) {
                        List<UserInfoBean> list = new Gson().fromJson(userInfo, new TypeToken<List<UserInfoBean>>() {
                        }.getType());
//                        Log.i("dasdadas", "----" + list);
                        for (UserInfoBean userBean : list) {

                            if (userBean.getAccount().equals(account) && userBean.isStartFinger() == true || userBean.getAccount().equals(account) && userBean.isStartFace() == true) {
                                if (userBean.getAccount().equals(account) && userBean.isStartFinger() == true) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        if (null != safeKeyboard && safeKeyboard.isShow()) {
                                            safeKeyboard.hideKeyboard();
                                        }
                                        fingerprintManger = getSystemService(FingerprintManager.class);
                                        cancellationSignal = new CancellationSignal();
                                        loginAccount = userBean.getAccount();
                                        loginPwd = userBean.getPwd();
//                                    fingerprintManger.authenticate(null, cancellationSignal, 0, callback, handler);
                                        fingerprintManger.authenticate(null, cancellationSignal, 0, new FingerprintManager.AuthenticationCallback() {

                                            @Override
                                            public void onAuthenticationError(int errMsgId, CharSequence errString) {
                                                super.onAuthenticationError(errMsgId, errString);
                                                //验证错误时，回调该方法。当连续验证5次错误时，将会走onAuthenticationFailed()方法
                                                handler.obtainMessage(1, errMsgId, 0).sendToTarget();
                                            }

                                            @Override
                                            public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
                                                super.onAuthenticationHelp(helpMsgId, helpString);
                                            }

                                            @Override
                                            public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                                                super.onAuthenticationSucceeded(result);
                                                //验证成功时，回调该方法。fingerprint对象不能再验证
                                                handler.obtainMessage(2).sendToTarget();
                                            }

                                            @Override
                                            public void onAuthenticationFailed() {
                                                super.onAuthenticationFailed();
                                                //验证失败时，回调该方法。fingerprint对象不能再验证并且需要等待一段时间才能重新创建指纹管理对象进行验证
                                                handler.obtainMessage(3).sendToTarget();
                                            }
                                        }, handler);

                                        mDialog.show();
                                        safeKeyboard.hideKeyboard();
                                        initPopuKeyBoard();
                                    }
                                    break;
                                }
                                if (userBean.getAccount().equals(account) && userBean.isStartFace() == true) {
                                    isShowFace = 1;
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        loginAccount = userBean.getAccount();
                                        loginPwd = userBean.getPwd();
                                        mFaceDialog.show();
                                        initPopuKeyBoard();
                                    }
                                    break;
                                }

                            }

                        }

                    }
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.getAreaCode();
        loginType = Utils.getSpUtils().getInt("loginType", 1);
        face_cuccess = Utils.getSpUtils().getInt("face_cuccess", 1000);
        isShowFace = 0;
        if (face_cuccess == 1001) {
            handler.obtainMessage(12).sendToTarget();
            Utils.getSpUtils().put("face_cuccess", 1000);
        } else if (face_cuccess == 1000 && isShowFace == 1) {
            handler.obtainMessage(13).sendToTarget();
        }

        if (llPhone.getVisibility()== View.VISIBLE) {
            loginType=0;
        }else {
            loginType=1;
        }
    }

    @Override
    public LoginPresent initPresenter() {
        return new LoginPresent();
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }


    @OnClick({R.id.tv_code, R.id.ll_code, R.id.ll_email, R.id.ll_phone1, R.id.tv_forget, R.id.tv_register, R.id.tv_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_code:
            case R.id.ll_code:
                AreaCodePopupWindow popupWindow = new AreaCodePopupWindow(this, list, this);
                popupWindow.showAsDropDown(llCode, 0, DensityUtil.dip2px(this, -5));
                break;
            case R.id.ll_email:
                etEmail.setText("");
                etPassword.setText("");
                loginType = 1;
                initView();
                break;
            case R.id.ll_phone1:
                etPhone.setText("");
                etPassword.setText("");
                loginType = 0;
                initView();
                break;
            case R.id.tv_forget:
                ActivityUtils.next(this, ForgetActivity.class);
                break;
            case R.id.tv_register:
                ActivityUtils.next(this, RegisterActivity.class);
                break;
            case R.id.tv_login:
                initPopuKeyBoard();
                /**
                 * 0---手机号登录
                 * 1---邮箱登录
                 */
                if (loginType == 0) {
                    String phoneNumber = etPhone.getText().toString();
                    if (TextUtils.isEmpty(phoneNumber)) {
                        toast(getResources().getString(R.string.phone_input));
                        return;
                    }
                    String pwd = etPassword.getText().toString();
                    if (TextUtils.isEmpty(pwd)) {
                        toast(getResources().getString(R.string.password_input));
                        return;
                    }
                    showProgress();
                    presenter.login(tvCode.getText().toString(), phoneNumber, pwd, "");
//                    presenter.login(phoneNumber,pwd);

                } else {
                    String email = etEmail.getText().toString();
                    if (TextUtils.isEmpty(email)) {
                        toast(getResources().getString(R.string.email_input));
                        return;
                    }
                    String pwd = etPassword.getText().toString();
                    if (TextUtils.isEmpty(pwd)) {
                        toast(getResources().getString(R.string.password_input));
                        return;
                    }
                    boolean fastDoubleClick = ButtonUtils.isFastDoubleClick(R.id.tv_login);
                    if (!fastDoubleClick) {
                        showProgress();
                        presenter.login("", email, pwd, "");
                    }
                }

                break;
        }
    }

    private void initPopuKeyBoard() {

//        CancellationSignal mCancellationSignal = new CancellationSignal();
//
//
//        fingerprintManger.authenticate(null, mCancellationSignal, 0, null);
//
////取消指纹识别
//        mCancellationSignal.cancel();
        googleView = LayoutInflater.from(this).inflate(R.layout.google_dialog_layout, null);
        popupWindow = new LoginPopupWindow(this, googleView);
//            popupWindow.setContentView(googleView);
        key_mains = googleView.findViewById(R.id.key_main);
        keyboardPlace = googleView.findViewById(R.id.keyboardPlaces);
        key_scroll = googleView.findViewById(R.id.key_scrolls);

        safety_hint = googleView.findViewById(R.id.safety_hint);
        safety_input_hint = googleView.findViewById(R.id.safety_input_hint);
        safety_code = googleView.findViewById(R.id.safety_code);
        close = googleView.findViewById(R.id.close);
        safety = googleView.findViewById(R.id.safety);

        safety_input_hint.setInputType(InputType.TYPE_CLASS_NUMBER);
        List<EditText> list = new ArrayList<>();
        list.add(safety_input_hint);
        safeKeyboard2 = ShowKeyboard.initKeyBoard(getApplicationContext(), keyboardPlace, key_mains, key_scroll, list, true);

        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View vi, MotionEvent ev) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {

                    // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
                    if (isShouldHideInputp(safety_input_hint, ev, keyboardPlace)) {
                        hideSoftInputp(vi.getWindowToken());
                    }
                }
                return false;
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow.isShowing()) {

                    popupWindow.dismiss();

                }
            }
        });
    }


    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     *
     * @param v
     * @param event
     * @param keyboardPlace
     * @return
     */
    private boolean isShouldHideInputp(View v, MotionEvent event, LinearLayout keyboardPlace) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
//            if (event.getX() > left && event.getX() < right
            if (safeKeyboard2 != null && safeKeyboard2.isShow()) {
                if (event.getY() > top && event.getY() < bottom || event.getY() > getWindowManager().getDefaultDisplay().getHeight() - keyboardPlace.getHeight()) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            } else {
                if (event.getY() > top && event.getY() < (bottom + DensityUtil.dip2px(LoginActivity.this, 180))) {
//                    && event.getY() > top && event.getY() < bottom) {
                    // 点击EditText的事件，忽略它。
                    return false;
                } else {
                    return true;
                }
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    private void hideSoftInputp(IBinder token) {
        if (token != null) {
            if (safeKeyboard2 != null && safeKeyboard2.stillNeedOptManually(false)) {
                safeKeyboard2.hideKeyboard();
            } else {
                popupWindow.dismiss();
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        tvCode.setText(list.get(position).getCode());
        Glide.with(LoginActivity.this).load(list.get(position).getIcon()).into(iv);
    }

    @Override
    public void onHide(boolean isHide) {

    }


    private void initView() {

        Utils.getSpUtils().put("face_cuccess", 1000);
        userInfo = Utils.getSpUtils().getString("userInfo");
//        TextUtils.isEmpty(userInfo)

        //初始化 指纹登录提示和面部识别登录提示 弹窗
        mDialog = new Dialog(this, R.style.MyDialog);
        View view = LayoutInflater.from(this).inflate(R.layout.finger_dialog_layout, null);
        mDialog.setContentView(view);
        view.findViewById(R.id.dismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginType == 0) {
                    String phoneNumber = etPhone.getText().toString();
                    if (TextUtils.isEmpty(phoneNumber)) {
                        return;
                    }
                    closeF(phoneNumber);
                } else {
                    String email = etEmail.getText().toString();
                    if (TextUtils.isEmpty(email)) {
                        return;
                    }
                    closeF(email);
                }
                mDialog.cancel();

            }
        });
        mFaceDialog = new Dialog(this, R.style.MyDialog);
        faceView = LayoutInflater.from(this).inflate(R.layout.face_dialog_layout, null);
        mFaceDialog.setContentView(faceView);
        faceView.findViewById(R.id.dismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mFaceDialog.cancel();
            }
        });
        faceView.findViewById(R.id.sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterAndRecognizeActivity.class));
                mFaceDialog.cancel();
            }
        });


        if (loginType == 0) {
            llPhone.setVisibility(View.VISIBLE);
            etEmail.setVisibility(View.GONE);

            ivEmail.setImageResource(R.mipmap.iv_login_email);
            ivPhone.setImageResource(R.mipmap.iv_login_phone);
        } else {
            llPhone.setVisibility(View.GONE);
            etEmail.setVisibility(View.VISIBLE);
            ivEmail.setImageResource(R.mipmap.iv_login_email_ed);
            ivPhone.setImageResource(R.mipmap.iv_login_phone_un);
        }


    }


    public void closeF(String account) {
        if (!TextUtils.isEmpty(userInfo)) {
            List<UserInfoBean> list = new Gson().fromJson(userInfo, new TypeToken<List<UserInfoBean>>() {
            }.getType());
            for (UserInfoBean userBean : list) {
                if (userBean.getAccount().equals(account) && userBean.isStartFinger() == true) {
                    isclosseFinger = 1;
                    cancellationSignal.cancel();//取消指纹识别
                    break;
                }

            }


        }

    }


    private int frequencyError = 0;

    @Override
    public void loginSuccess(int status, String msg, RegisterBean registerBean) {
        hideProgress();
        if (status == 200) {
            if (loginType == 0) {//手机号
//                SharedPreferences
                Utils.getSpUtils().put("phone_country", tvCode.getText().toString());
                Utils.getSpUtils().put("face_account", etPhone.getText().toString());
                UserInfoBean userInfoBean = new UserInfoBean(etPhone.getText().toString(), etPassword.getText().toString());
                if (TextUtils.isEmpty(userInfo)) {
                    List<UserInfoBean> list = new ArrayList<>();
                    list.add(userInfoBean);
                    Utils.getSpUtils().put("userInfo", new Gson().toJson(list));
                } else {
                    List<UserInfoBean> list = new Gson().fromJson(userInfo, new TypeToken<List<UserInfoBean>>() {
                    }.getType());
                    int i = 0;
                    for (UserInfoBean infoBean : list) {
                        if (infoBean.getAccount().equals(userInfoBean.getAccount())) {
                            i = 1;
                            break;
                        }
                    }
                    if (i == 0)
                        list.add(userInfoBean);
                    Utils.getSpUtils().put("userInfo", new Gson().toJson(list));

                }

            } else {//邮箱
                Utils.getSpUtils().put("face_account", etEmail.getText().toString());
                UserInfoBean userInfoBean = new UserInfoBean(etEmail.getText().toString(), etPassword.getText().toString());
                if (TextUtils.isEmpty(userInfo)) {
                    List<UserInfoBean> list = new ArrayList<>();
                    list.add(userInfoBean);
                    Utils.getSpUtils().put("userInfo", new Gson().toJson(list));
                } else {
                    List<UserInfoBean> list = new Gson().fromJson(userInfo, new TypeToken<List<UserInfoBean>>() {
                    }.getType());
                    int i = 0;
                    for (UserInfoBean infoBean : list) {
                        if (infoBean.getAccount().equals(userInfoBean.getAccount())) {
                            i = 1;
                            break;
                        }
                    }
                    if (i == 0)
                        list.add(userInfoBean);
                    Utils.getSpUtils().put("userInfo", new Gson().toJson(list));

                }

            }

            Utils.getSpUtils().put(Constants.TOKEN, registerBean.getToken());
            Utils.getSpUtils().put("loginType", loginType);

            if (!"".equals(etPassword.getText().toString())) {
                Utils.getSpUtils().put("loginPwd", etPassword.getText().toString());
            } else {
                Utils.getSpUtils().put("loginPwd", loginPwd);
            }

            Utils.getSpUtils().put("cur_id", registerBean.getCur_id());
            Utils.getSpUtils().put(Constants.SELECT_COIN, registerBean.getName());
            Utils.getSpUtils().put(Constants.IS_OPEN_GOOGLE, registerBean.getGoogleFlag());//0未绑定 1绑定未开启

            if (registerBean.getMainAccount().contains("@")) {
                Utils.getSpUtils().put("email", registerBean.getMainAccount());
                Utils.getSpUtils().put("phoneNumber", registerBean.getBindAccount());
            } else {
                Utils.getSpUtils().put("phoneNumber", registerBean.getMainAccount());
                Utils.getSpUtils().put("email", registerBean.getBindAccount());
            }

            Utils.getSpUtils().put(Constants.AREA_CODE, registerBean.getAreaCode());
            Utils.getSpUtils().put("mainAccount", registerBean.getAccount());
            Utils.getSpUtils().put("symbol", registerBean.getSymbol());
            Utils.getSpUtils().put("coin_symbol", registerBean.getSymbol());
//            Log.e("cur_id==", registerBean.getSymbol());
            WalletBean walletBean = null;
            if (registerBean.getMainAccount() != null && registerBean.getMainAccount().contains("@")) {
                //邮箱注册的
                //通过手机号和钱包类型查询有无账户
                List<WalletBean> listAll = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(registerBean.getMainAccount())).build().list();
                if (null != listAll) {
                    if (listAll.size() > 0) {
                        walletBean = listAll.get(listAll.size() - 1);
                    }
                }
                // walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(registerBean.getMainAccount())).build().unique();
                if (walletBean == null) {
                    if (registerBean.getBindAccount() != null) {
                        List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(registerBean.getBindAccount())).build().list();
                        if (null != list) {
                            if (list.size() > 0)
                                walletBean = list.get(list.size() - 1);
                        }

                    }
                }

                if (walletBean == null) {//创建钱包
                    walletBean = new WalletBean();
                    walletBean.setWallet_phone(registerBean.getBindAccount());
                    walletBean.setWallet_email(registerBean.getMainAccount());
                    MyApplication.getDaoSession().getWalletBeanDao().insert(walletBean);
                } else {//修改钱包
                    walletBean.setWallet_phone(registerBean.getBindAccount());
                    walletBean.setWallet_email(registerBean.getMainAccount());
                    walletBean.setWallet_main_account(registerBean.getAccount());
                    MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
                }
            } else {//手机号注册
                //通过手机号和钱包类型查询有无账户
                List<WalletBean> listAll = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(registerBean.getMainAccount())).build().list();
                if (null != listAll) {
                    if (listAll.size() > 0)
                        walletBean = listAll.get(listAll.size() - 1);
                }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(registerBean.getMainAccount())).build().unique();
                if (walletBean == null) {
                    if (registerBean.getBindAccount() != null) {
                        List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(registerBean.getBindAccount())).build().list();
                        if (null != list) {
                            if (list.size() > 0)
                                walletBean = list.get(list.size() - 1);
                        }
//                    walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(registerBean.getBindAccount())).build().unique();
                    }
                }
                if (walletBean == null) {//创建钱包
                    walletBean = new WalletBean();
                    walletBean.setWallet_phone(registerBean.getMainAccount());
                    walletBean.setWallet_email(registerBean.getBindAccount());
                    MyApplication.getDaoSession().getWalletBeanDao().insert(walletBean);
                } else {//修改钱包
                    walletBean.setWallet_phone(registerBean.getMainAccount());
                    walletBean.setWallet_email(registerBean.getBindAccount());
                    walletBean.setWallet_main_account(registerBean.getAccount());
                    MyApplication.getDaoSession().getWalletBeanDao().update(walletBean);
                }

            }
            MyApplication.getInstance().setWalletBean(walletBean);


            if (!registerBean.isChild()) {//没有账户
                Bundle bundle = new Bundle();
                bundle.putString("from", "register");
                bundle.putString("inviteCode", "");
                Utils.getSpUtils().put("isLogin", false);
                ActivityUtils.next(this, CreateAccountActivity.class, bundle);
            } else {//有账户
                Utils.getSpUtils().put("isLogin", true);
                ActivityUtils.next(this, MainActivity.class, true);
            }
        } else if (status == 611 && loginFastType == 1) {
            AlertDialogShowUtil.toastMessage(LoginActivity.this, getString(R.string.fast_login_fail));
        } else if (status == 611) {
            toast(msg);
            if (loginType == 0) {
                presenter.getFrequency(tvCode.getText().toString() + etPhone.getText().toString());
            } else {
                presenter.getFrequency(etEmail.getText().toString());
            }
        } else if (status == 615) {
            safety_code.setText(getString(R.string.safety_verification_code));
            if (loginType == 0) {//手机号
                safety_hint.setText(getString(R.string.safety_verification_hint2));
                safety_input_hint.setHint(getString(R.string.safety_input_hint2));
                safety_code.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //手机号
                        String phoneNumber = etPhone.getText().toString();
                        if (!TextUtils.isEmpty(phoneNumber)) {
                            showProgress();
                            presenter.getVerificationCode(tvCode.getText().toString(), phoneNumber);
                        }
                    }
                });
                if (!ButtonUtils.isFastDoubleClick()) {
                    safety.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!safety_input_hint.getText().toString().equals("")) {
                                popupWindow.dismiss();
                                showProgress();
                                if (isnotpassword == 1) {
                                    presenter.login(tvCode.getText().toString(), loginAccount, loginPwd, safety_input_hint.getText().toString());
                                } else {
                                    presenter.login(tvCode.getText().toString(), etPhone.getText().toString(), etPassword.getText().toString(), safety_input_hint.getText().toString());
                                }
                            }
                        }
                    });
                }
            } else if (loginType == 1) {//邮箱号
                String phoneNumber = Utils.getSpUtils().getString("email");
                safety_hint.setText(getString(R.string.safety_verification_hint3));
                safety_input_hint.setHint(getString(R.string.safety_input_hint3));
                safety_code.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String email = etEmail.getText().toString();
                        if (!TextUtils.isEmpty(email)) {
                            if (emailFormat(email)) {
                                showProgress();
                                presenter.getVerificationCode("", email);
                            } else {
                                toast(getResources().getString(R.string.email_err));
                            }
                        } else {
                            toast(getResources().getString(R.string.email_input));
                        }

                    }
                });
                if (!ButtonUtils.isFastDoubleClick()) {
                    safety.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!safety_input_hint.getText().toString().equals("")) {
                                popupWindow.dismiss();
                                showProgress();
                                if (isnotpassword == 1) {
                                    presenter.login("", loginAccount, loginPwd, safety_input_hint.getText().toString());
                                } else {
                                    presenter.login("", etEmail.getText().toString(), etPassword.getText().toString(), safety_input_hint.getText().toString());

                                }

                            }
                        }
                    });
                }
            }

            popupWindow.setContentView(googleView);
            popupWindow.show(key_main, getWindow(), 1);
        } else if (status == 614 || status == 700) {
            initPopuKeyBoard();
            String isOpenGoogle = Utils.getSpUtils().getString(Constants.IS_OPEN_GOOGLE, "-1");
            if (isOpenGoogle.equals("2")) {
                safety_code.setText(getString(R.string.safety_input_paste));
                safety_code.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String clipboardContent = getClipboardContent(LoginActivity.this);
                        if (TextUtils.isEmpty(clipboardContent)) {
                            toast(getString(R.string.no_clip));
                            return;
                        }
                        safety_input_hint.setText(clipboardContent);

                    }
                });

                if (!ButtonUtils.isFastDoubleClick()) {
                    safety.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!safety_input_hint.getText().toString().equals("")) {
                                popupWindow.dismiss();
                                if (loginType == 0) {
                                    showProgress();
                                    if (isnotpassword == 1) {
                                        presenter.login(tvCode.getText().toString(), loginAccount, loginPwd, safety_input_hint.getText().toString());
                                    } else {
                                        presenter.login(tvCode.getText().toString(), etPhone.getText().toString(), etPassword.getText().toString(), safety_input_hint.getText().toString());
                                    }
                                }else {
                                    showProgress();
                                    if (isnotpassword == 1) {
                                        presenter.login("", loginAccount, loginPwd, safety_input_hint.getText().toString());
                                    } else {
                                        presenter.login("", etEmail.getText().toString(), etPassword.getText().toString(), safety_input_hint.getText().toString());

                                    }
                                }
//                                    presenter.login(tvCode.getText().toString(), etPhone.getText().toString(), etPassword.getText().toString(), safety_input_hint.getText().toString());
                            }
//                            else {

//                                showProgress();
//                                if (isnotpassword == 1) {
//                                    presenter.login("", loginAccount, loginPwd, safety_input_hint.getText().toString());
//                                } else {
//                                    presenter.login("", etEmail.getText().toString(), etPassword.getText().toString(), safety_input_hint.getText().toString());
//
//                                }
//                            }
                        }
                    });
                }
            } else {
                safety_code.setText(getString(R.string.safety_verification_code));
                if (loginType == 0) {//手机号
                    safety_hint.setText(getString(R.string.safety_verification_hint2));
                    safety_input_hint.setHint(getString(R.string.safety_input_hint2));
                    safety_code.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //手机号
                            String phoneNumber = etPhone.getText().toString();
                            if (!TextUtils.isEmpty(phoneNumber)) {
                                showProgress();
                                presenter.getVerificationCode(tvCode.getText().toString(), phoneNumber);
                            }
                        }
                    });
                    if (!ButtonUtils.isFastDoubleClick()) {
                        safety.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!safety_input_hint.getText().toString().equals("")) {
                                    popupWindow.dismiss();
                                    showProgress();
                                    if (isnotpassword == 1) {
                                        presenter.login(tvCode.getText().toString(), loginAccount, loginPwd, safety_input_hint.getText().toString());
                                    } else {
                                        presenter.login(tvCode.getText().toString(), etPhone.getText().toString(), etPassword.getText().toString(), safety_input_hint.getText().toString());
                                    }
//

                                }
                            }
                        });
                    }
                } else if (loginType == 1) {//邮箱号
                    safety_hint.setText(getString(R.string.safety_verification_hint3));
                    safety_input_hint.setHint(getString(R.string.safety_input_hint3));
                    safety_code.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String email = etEmail.getText().toString();
                            if (!TextUtils.isEmpty(email)) {
                                if (emailFormat(email)) {
                                    showProgress();
                                    presenter.getVerificationCode("", email);
                                } else {
                                    toast(getResources().getString(R.string.email_err));
                                }
                            } else {
                                toast(getResources().getString(R.string.email_input));
                            }
                        }
                    });

                    if (!ButtonUtils.isFastDoubleClick()) {
                        safety.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!safety_input_hint.getText().toString().equals("")) {
                                    popupWindow.dismiss();
                                    showProgress();
                                    if (isnotpassword == 1) {
                                        presenter.login("", loginAccount, loginPwd, safety_input_hint.getText().toString());
                                    } else {
                                        presenter.login("", etEmail.getText().toString(), etPassword.getText().toString(), safety_input_hint.getText().toString());
                                    }

//
                                }
                            }
                        });
                    }
                }
            }

//            popupWindow.setContentView(googleView);
            popupWindow.setContentView(googleView);
            popupWindow.show(key_mains, getWindow(), 1);

        } else {
            toast(msg);
        }


        // TODO: 2020/9/7  谷歌验证


    }

    /**
     * 获取剪切板上的内容
     */
    @Nullable
    public static String getClipboardContent(Context context) {
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (cm != null) {
            ClipData data = cm.getPrimaryClip();
            if (data != null && data.getItemCount() > 0) {
                ClipData.Item item = data.getItemAt(0);
                if (item != null) {
                    CharSequence sequence = item.coerceToText(context);
                    if (sequence != null) {
                        return sequence.toString();
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void loginFailure(int status, String errorMsg) {
        hideProgress();
        if (status == 611 && loginFastType == 1) {
            AlertDialogShowUtil.toastMessage(LoginActivity.this, getString(R.string.fast_login_fail));
        } else {
            toast(errorMsg);
        }

    }


    @Override
    public void getAreaCodeSuccess(List<AreaBean> mList) {
        for (AreaBean areaBean : mList) {
            list.add(new AreaCodeBean(areaBean.getUrl(), areaBean.getArea()));
        }
        for (int i = 0; i < mList.size(); i++) {
            if (!Utils.getSpUtils().getString("phone_country", "").equals("")) {
                if (mList.get(i).getArea().equals(Utils.getSpUtils().getString("phone_country"))) {
                    tvCode.setText(list.get(i).getCode());
                    Glide.with(this).load(list.get(i).getIcon()).into(iv);
                }
            } else {
                if (mList.get(i).getArea().equals("+1")) {
                    tvCode.setText(list.get(i).getCode());
                    Glide.with(this).load(list.get(i).getIcon()).into(iv);
                }
            }

        }
//        tvCode.setText(list.get(0).getCode());
//        Glide.with(LoginActivity.this).load(list.get(0).getIcon()).into(iv);
    }

    @Override
    public void getNumSuccess(String num) {
//        toast(num);
    }

    @Override
    public void getVerificationCodeSuccess() {
        hideProgress();

        if (null != safety_code) {
            CountDownTimerUtils countDownTimerUtils = new CountDownTimerUtils(this, safety_code, 60000, 1000, "#a5a9ac", 1);
            countDownTimerUtils.start();

            safety_code.setFocusable(true);
            safety_code.setFocusableInTouchMode(true);
            safety_code.requestFocus();
        }

    }

    @Override
    public void getInvitePersonFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    private boolean emailFormat(String email) {//邮箱判断正则表达式
        Pattern pattern = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
        Matcher mc = pattern.matcher(email);
        return mc.matches();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if(null!=safeKeyboard){
//            safeKeyboard.release();
//        }
//        if(null!=safeKeyboard2){
//            safeKeyboard2.release();
//        }


    }


}
