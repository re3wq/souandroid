package com.taiyi.soul.moudles.main.assets.bean;

import java.io.Serializable;
import java.util.List;

public class AssetsBean implements Serializable{

    /**
     * totalAssets : 667.6009
     * totalMoenys : 351.3657
     * totalFrozenMoney : 316.2351
     * propertelist : [{"coinName":"USDK","convert":"351.3657","money":"50.12350000"},{"coinName":"ETH","convert":"0.0000","money":"0E-8"}]
     */

    private String totalAssets;
    private String totalMoenys;
    private String totalFrozenMoney;
    private List<PropertelistBean> propertelist;

    public String getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(String totalAssets) {
        this.totalAssets = totalAssets;
    }

    public String getTotalMoenys() {
        return totalMoenys;
    }

    public void setTotalMoenys(String totalMoenys) {
        this.totalMoenys = totalMoenys;
    }

    public String getTotalFrozenMoney() {
        return totalFrozenMoney;
    }

    public void setTotalFrozenMoney(String totalFrozenMoney) {
        this.totalFrozenMoney = totalFrozenMoney;
    }

    public List<PropertelistBean> getPropertelist() {
        return propertelist;
    }

    public void setPropertelist(List<PropertelistBean> propertelist) {
        this.propertelist = propertelist;
    }

    public static class PropertelistBean implements Serializable{
        /**
         * coinName : USDN
         * convert : 351.3657
         * money : 50.12350000
         */

        private String coinName;
        private String convert;
        private String money;
        private String url;
        private String moneyandfor;
        private String mfcon;
        private String price;

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getMoneyandfor() {
            return moneyandfor;
        }

        public void setMoneyandfor(String moneyandfor) {
            this.moneyandfor = moneyandfor;
        }

        public String getMfcon() {
            return mfcon;
        }

        public void setMfcon(String mfcon) {
            this.mfcon = mfcon;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCoinName() {
            return coinName;
        }

        public void setCoinName(String coinName) {
            this.coinName = coinName;
        }

        public String getConvert() {
            return convert;
        }

        public void setConvert(String convert) {
            this.convert = convert;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }
    }
}
