package com.taiyi.soul.moudles.main.home.mall.goods.present;

import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.MallFirstBean;

public interface MallView {
    void onFailure();
    void onSuccess(MallFirstBean bean);
    void onCarSizeSuccess(CarListBean bean);
}
