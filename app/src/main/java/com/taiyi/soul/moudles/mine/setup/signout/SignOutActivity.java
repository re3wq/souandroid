package com.taiyi.soul.moudles.mine.setup.signout;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.View;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class SignOutActivity extends BaseActivity<SignOutView, SignOutPresent> implements SignOutView {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.top)
    ConstraintLayout top;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_signout;
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    public SignOutPresent initPresenter() {
        return new SignOutPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.sign_out));
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv, R.id.cancelTv, R.id.confirmTv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;
            case R.id.cancelTv:
                finish();
                break;
            case R.id.confirmTv:
                showProgress();
                presenter.signOut();
                break;
        }
    }

    @Override
    public void signOutSuccess() {
        hideProgress();
       //  AppManager.getAppManager().finishAllActivity();
        ActivityUtils.next(this, LoginActivity.class, true);
        Utils.getSpUtils().remove(Constants.TOKEN);
    }

    @Override
    public void signOutFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
       //  AppManager.getAppManager().finishAllActivity();
        ActivityUtils.next(this, LoginActivity.class, true);
        Utils.getSpUtils().remove(Constants.TOKEN);
    }
}
