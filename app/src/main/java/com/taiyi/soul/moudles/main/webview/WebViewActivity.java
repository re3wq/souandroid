package com.taiyi.soul.moudles.main.webview;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.SPUtil;
import com.taiyi.soul.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class WebViewActivity extends BaseActivity<NormalView, NormalPresenter> {

    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.backIv)
    ImageView backIv;
    @BindView(R.id.titleTv)
    TextView titleTv;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_web_view;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        int id = getIntent().getExtras().getInt("id");
        String url = getIntent().getExtras().getString("url");
        int type = getIntent().getExtras().getInt("type");
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.setBackgroundColor(0);//设置背景色
        webView.getBackground().setAlpha(0);//设置填充透明度（布局中一定要设置background，不然getbackground会是null）
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showProgress();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideProgress();
            }
        });
        if (type == 2) {
            titleTv.setText(R.string.activity_sp_title);
        }
        webView.loadUrl(url + "?id=" + id + "&lang=" + Utils.getSpUtils().getString("current_language", ""));
//        if (type == 0) {
////            Log.e("news===","$url?id=$id&lang=" + Utils.getSpUtils().getString("current_language", ""));
////            webView.loadUrl(url + "?id=" + id + "&lang=" + getSetLanguageLocale(this));
//            webView.loadUrl(url + "?id=" + id + "&lang=" + Utils.getSpUtils().getString("current_language", ""));
//        } else
////            webView.loadUrl(url + "&lang=" + getSetLanguageLocale(this));
////            webView.loadUrl(url + "&lang=" + Utils.getSpUtils().getString("current_language", ""));
//            webView.loadUrl(url + "?id=" + id + "&lang=" + Utils.getSpUtils().getString("current_language", ""));
    }

    public static String getSetLanguageLocale(Context context) {

        switch (SPUtil.getInstance(context).getSelectLanguage()) {
            case 0: //繁体
                return "zh-rCN";
            case 1:
                return "en";//英语
            case 2:
                return "fr";//法语
            case 3:
                return "de";//德语
            case 4:
                return "es";//西班牙语
            case 5:
                return "pt";//葡萄牙语
            case 6:
                return "ru";//俄语
            case 7:
                return "ar";//阿拉伯语
            case 8://繁体中文
                return "zh-rCN";
//                return new Locale("wyw");
            case 9://简体中文
                return "zh";
            case 10:
                return "ja";//日语
            case 11:
                return "ko";//韩语
//            case 1:
            default:
                return "zh-rCN";//简体中文
        }
    }


    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(webView).init();
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }

    @OnClick(R.id.backIv)
    public void onViewClicked() {
        finish();
    }
}
