package com.taiyi.soul.moudles.main.deal;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.deal.bean.DealPayBean;
import com.taiyi.soul.moudles.main.deal.fragment.EntrustOneFragment;
import com.taiyi.soul.moudles.main.deal.fragment.EntrustThreeFragment;
import com.taiyi.soul.moudles.main.deal.fragment.EntrustTwoFragment;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import org.greenrobot.eventbus.EventBus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Android Studio.
 *
 * @Date ： 2020\5\14 0014 14:32
 * @Author : yuan
 * @Describe ： 我的委托列表
 */
public class MainEntrustActivity extends BaseActivity<NormalView, NormalPresenter> {
    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.entrust_recycle)
    RecyclerView mEntrustRecycle;
    @BindView(R.id.entrust_tab)
    SlidingTabLayout mEntrustTab;
    @BindView(R.id.entrust_viewpager)
    ViewPager mEntrustViewpager;


    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private String[] mTitles;
    private float mTabWidth;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main_entrust;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        mTitles = new String[]{getResources().getString(R.string.deal_entrust_one),
                getResources().getString(R.string.deal_entrust_two),getResources().getString(R.string.deal_entrust_three)};
        mFragments.add(new EntrustOneFragment());//我的委托
        mFragments.add(new EntrustTwoFragment());//成交明细
        mFragments.add(new EntrustThreeFragment());//历史委托

        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mEntrustViewpager.setAdapter(mAdapter);

        mTabWidth = mEntrustTab.getWidth();
        BigDecimal b1 = new BigDecimal(Double.toString(mTabWidth));
        BigDecimal b2 = new BigDecimal(Double.toString(2));
        int width = b1.divide(b2, 0, BigDecimal.ROUND_HALF_UP).intValue();
        mEntrustTab.setIsBackGround(true);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.node_hash_market_sel_iv3);
        mEntrustTab.setDrawBitMap(bitmap, 126);
        mEntrustTab.setViewPager(mEntrustViewpager);
        mEntrustViewpager.setCurrentItem(0);

        mEntrustTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mAdapter.notifyDataSetChanged();
                EventBus.getDefault().post("refresh_anim");
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    @Override
    protected void initData() {
        mTvTitle.setText(getResources().getString(R.string.deal_entrust));



        List<DealPayBean> beanList = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            DealPayBean bean = new DealPayBean();
            bean.names = "2020-03-26  18:32:54";
            bean.total = "123674.1950";
            bean.num = "1565";
            bean.price = "1000.4543";
            bean.total_type = "USDS";
            bean.num_type = "SOU";
            bean.price_type = "USDS";
            beanList.add(bean);
        }

//        mEntrustRecycle.setLayoutManager(new LinearLayoutManager(MainEntrustActivity.this));
//        CommonAdapter dealSellAdapter = AdapterManger.getDealPayAdapter(getContext(), beanList,this);
//        MainEntrustAdapter adapter = new MainEntrustAdapter(beanList);
//        adapter.setAnimationEnable(true);
//        adapter.setAnimationFirstOnly(false);
//        adapter.setAdapterAnimation(new CustomAnimation1());
//        mEntrustRecycle.setAdapter(adapter);
//        mEntrustRecycle.setLayoutManager(new LinearLayoutManager(MainEntrustActivity.this));
//        CommonAdapter adapter = AdapterManger.getMainEntrustAdapter(MainEntrustActivity.this, beanList);
//        mEntrustRecycle.setAdapter(adapter);
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
