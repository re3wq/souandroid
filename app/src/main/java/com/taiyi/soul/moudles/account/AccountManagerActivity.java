package com.taiyi.soul.moudles.account;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class AccountManagerActivity extends BaseActivity<NormalView, NormalPresenter> implements AdapterManger.OnItemClick {


    @BindView(R.id.title)
    RelativeLayout mTitle;

    @BindView(R.id.recycle_account)
    RecyclerView recycleAccount;


    private CommonAdapter mCommonAdapter;
    private List<AccountBean> list = new ArrayList<>();

    private List<AccountBean> list_last = new ArrayList<>();



    @Override
    protected int getLayoutId() {
        return R.layout.activity_account_manager;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        setCenterTitle("账户管理");

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setSmoothScrollbarEnabled(true);
        recycleAccount.setLayoutManager(layoutManager);
        mCommonAdapter = AdapterManger.getManagerListAdapter(this, list_last,this);
        recycleAccount.setAdapter(mCommonAdapter);



    }

    @Override
    protected void initData() {

        list.clear();

        List<AccountBean> list3 = new ArrayList<>();
        //4级
        AccountBean account3 = new AccountBean();
        account3.setAccount_name("zhao");
        account3.setId("3");
        account3.setInvitation_code("aaaa");
        list3.add(account3);
        list3.add(account3);
        list3.add(account3);


        List<AccountBean> list2 = new ArrayList<>();
        //3级
        AccountBean account2 = new AccountBean();
        account2.setAccount_name("qin");
        account2.setId("2");
        account2.setInvitation_code("wwww");
        account2.setChild(list3);
        list2.add(account2);
        list2.add(account2);
        list2.add(account2);


        List<AccountBean> list1 = new ArrayList<>();
        //2级
        AccountBean account1 = new AccountBean();
        account1.setAccount_name("zhang");
        account1.setId("1");
        account1.setInvitation_code("zzzz");
        account1.setChild(list2);
        list1.add(account1);
        list1.add(account1);


        //1级
        AccountBean account = new AccountBean();
        account.setAccount_name("wang");
        account.setId("0");
        account.setInvitation_code("qqqqqqq");
        account.setChild(list1);
        list.add(account);

        /*******************************/


        //1级
        List<AccountBean> list_1 = new ArrayList<>();
        AccountBean account_1 = new AccountBean();
        account_1.setAccount_name("hahaha");
        account_1.setId("5");
        account_1.setInvitation_code("hhhhhh");
        account_1.setChild(list_1);
        list.add(account_1);

        /*******************************/


        List<AccountBean> list_2 = new ArrayList<>();
        //2级
        AccountBean account_3 = new AccountBean();
        account_3.setAccount_name("zhang");
        account_3.setId("7");
        account_3.setInvitation_code("zzzz");
        list_2.add(account_3);


        //1级
        AccountBean account_2 = new AccountBean();
        account_2.setAccount_name("wang");
        account_2.setId("6");
        account_2.setInvitation_code("qqqqqqq");
        account_2.setChild(list_2);
        list.add(account_2);


        list_last.clear();
        initList(list,0);


        mCommonAdapter.notifyDataSetChanged();

        mCommonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                toast(list_last.get(position).getId());
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });


    }

    @Override
    public void initEvent() {


    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }




    @Override
    public void onCopy(String value) {
        toast(value);
    }

    @Override
    public void onSelect(int position) {
        toast(position+"");
    }


    private void initList(List<AccountBean> list, int level) {
        if (list == null || list.size() <= 0) return;
        for (AccountBean item: list) {
            item.setLevel(level);
            list_last.add(item);
            if (item.getChild() != null && item.getChild().size() > 0) {
                initList(item.getChild(), level + 1);
            }
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(mTitle).init();
    }
}
