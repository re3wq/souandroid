package com.taiyi.soul.moudles.main.home.super_activity;

import com.taiyi.soul.moudles.main.deal.bean.IssueFeeBean;
import com.taiyi.soul.moudles.main.home.bean.ActivityFirstBean;

public interface SuperAcBuyView {
    void getTotalPriceSuccess(int code, String msg_cn, SuperAcBuyNumBean bean);//计算价格
    void getBuyAirSuccess(int code, String bean);//购买结果
    void getBalanceSuccess(String symbol,String balance);//余额
    void getBuyAirListSuccess(AirListBean bean);//购买记录
    void onIssueFeeSuccess(IssueFeeBean bean);//签名地址
    void onCheckPayPasswordSuccess(int code, String bean);
    void activitySuccess(ActivityFirstBean activityFirstBean);
    void activityFailure(String errorMsg);
}
