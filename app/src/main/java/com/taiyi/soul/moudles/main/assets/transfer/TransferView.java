package com.taiyi.soul.moudles.main.assets.transfer;

import android.view.View;

import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.bean.TransferBalanceBean;

import java.util.List;

public interface TransferView {
    void getCoinListSuccess(List<CoinListBean> coinListBeanList);
    void getCoinListFailure(String errorMsg);
    void getBalanceSuccess(TransferBalanceBean balance);
    void transferSuccess(String msg);
    void transferFailure(String errorMsg);
    void getBalanceSuccess(String symbol,String balance);
    void checkAccountUseableInput(View viewButton, boolean s);
    void onCheckPayPasswordSuccess(int code, String msg_cn, String bean);//校验交易密码
    void getAccountStatusSuccess(int code, String msg_cn, String bean, View viewButton);//校验交易密码
}
