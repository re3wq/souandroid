package com.taiyi.soul.moudles.main.home.bean;

import java.util.List;

public class HomeBean {


    private List<BrowserslistBean> browserslist;
    private List<OrganizeListBean> organizeList;
    private List<BannerBean> banner;
    private List<TwoNewsBean> twoNews;

    public List<BrowserslistBean> getBrowserslist() {
        return browserslist;
    }

    public void setBrowserslist(List<BrowserslistBean> browserslist) {
        this.browserslist = browserslist;
    }

    public List<OrganizeListBean> getOrganizeList() {
        return organizeList;
    }

    public void setOrganizeList(List<OrganizeListBean> organizeList) {
        this.organizeList = organizeList;
    }

    public List<BannerBean> getBanner() {
        return banner;
    }

    public void setBanner(List<BannerBean> banner) {
        this.banner = banner;
    }

    public List<TwoNewsBean> getTwoNews() {
        return twoNews;
    }

    public void setTwoNews(List<TwoNewsBean> twoNews) {
        this.twoNews = twoNews;
    }

    public static class BrowserslistBean {
        /**
         * id : 3
         * name : LTC
         * ico : http://114.115.131.170:8888/group1/M00/00/02/wKgAp18SvyCATBx9AAANLakktpw546.png
         * url : https://www.okcoin.cn/ltc
         * sort : 3
         */

        private int id;
        private String name;
        private String ico;
        private String url;
        private int sort;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIco() {
            return ico;
        }

        public void setIco(String ico) {
            this.ico = ico;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }
    }

    public static class OrganizeListBean {
        /**
         * id : 1
         * name : UNION
         * ico : http://114.115.131.170:8888/group1/M00/00/02/wKgAp18Sv3aAPCYDAABDR6kApwI447.png
         * url : https://www.baidu.com/
         * introduce : <p style="color:#D8DADD;font-size:14px;line-height:28px;padding-top:18px;" =""="">
         您同意本協議並註冊成功即成為商標圈的註冊用戶直至您的賬戶註銷。商標圈可能在必要的時候對本協議條款及商標圈各單項服務協議進行更改，此種更改在商標圈上公佈或在具體服務過程中經雙方以口頭、書面等形式協商一致生效。您可以選擇停止使用商標圈相關的服務或者註銷您在商標圈的賬戶，否則商標圈將認為您同意更改後的服務條款。
         <img src="http://114.115.131.170:8888/group1/M00/00/02/wKgAp18UGKGARNW8AAmCRhbU8qE967.png" style="width:100%;height:100%;margin-bottom:8px;margin-top:11px;"/>
         未在商標圈發布或在具體服務過程中未經商標圈告知的服務條款將不具有法律效力，除非簽有書面協議，並且僅對簽署協議的當事人有效。 您應當擁有中華人民共和國法律認可的完全民事權利能力和完全民事行為能力，否則您和能夠代表您行使權利或承擔責任的其他主體將承擔一切後果。為此，您應當提供相應的證明。
         </p>
         * sort : 1
         */

        private int id;
        private String name;
        private String ico;
        private String url;
        private String introduce;
        private int sort;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIco() {
            return ico;
        }

        public void setIco(String ico) {
            this.ico = ico;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getIntroduce() {
            return introduce;
        }

        public void setIntroduce(String introduce) {
            this.introduce = introduce;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }
    }

    public static class BannerBean {
        /**
         * id : 4
         * imgurl : http://114.115.131.170:8888/group1/M00/00/02/wKgAp18UJa6AKOR7AALk2-NhI-0854.png
         * apptype : 1
         * herfid : null
         * h5url :
         * sort : 4
         * isShow : 1
         */

        private int id;
        private String imgurl;
        private String apptype;
        private String herfid;
        private String h5url;
        private int sort;
        private int isShow;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getApptype() {
            return apptype;
        }

        public void setApptype(String apptype) {
            this.apptype = apptype;
        }

        public String getHerfid() {
            return herfid;
        }

        public void setHerfid(String herfid) {
            this.herfid = herfid;
        }

        public String getH5url() {
            return h5url;
        }

        public void setH5url(String h5url) {
            this.h5url = h5url;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public int getIsShow() {
            return isShow;
        }

        public void setIsShow(int isShow) {
            this.isShow = isShow;
        }
    }

    public static class TwoNewsBean {
        /**
         * id : 6
         * title : 银江股份与亿邦国际就共同推进区块链等技术达成合作
         * outline : 【行业】外媒：部分推特被黑事件受害者可收回比特币
         * conent : <p style="color:#D8DADD;font-size:14px;line-height:28px;padding-top:18px;" =""="">
         您同意本協議並註冊成功即成為商標圈的註冊用戶直至您的賬戶註銷。商標圈可能在必要的時候對本協議條款及商標圈各單項服務協議進行更改，此種更改在商標圈上公佈或在具體服務過程中經雙方以口頭、書面等形式協商一致生效。您可以選擇停止使用商標圈相關的服務或者註銷您在商標圈的賬戶，否則商標圈將認為您同意更改後的服務條款。
         <img src="http://114.115.131.170:8888/group1/M00/00/02/wKgAp18UGKGARNW8AAmCRhbU8qE967.png" style="width:100%;height:100%;margin-bottom:8px;margin-top:11px;"/>
         未在商標圈發布或在具體服務過程中未經商標圈告知的服務條款將不具有法律效力，除非簽有書面協議，並且僅對簽署協議的當事人有效。 您應當擁有中華人民共和國法律認可的完全民事權利能力和完全民事行為能力，否則您和能夠代表您行使權利或承擔責任的其他主體將承擔一切後果。為此，您應當提供相應的證明。
         </p>
         * url : http://www.baidu.com/
         * createTime : 2020-07-11 15:23:29
         * pictureTop : http://114.115.131.170:8888/group1/M00/00/02/wKgAp18UGKGARNW8AAmCRhbU8qE967.png
         * pictureDetails : http://114.115.131.170:8888/group1/M00/00/02/wKgAp18UGKGARNW8AAmCRhbU8qE967.png
         * pictureList : http://114.115.131.170:8888/group1/M00/00/02/wKgAp18UGKGARNW8AAmCRhbU8qE967.png
         * isShow : 1
         * langu : zh
         * num : 1
         */

        private int id;
        private String title;
        private String outline;
        private String conent;
        private String url;
        private String createTime;
        private String pictureTop;
        private String pictureDetails;
        private String pictureList;
        private int isShow;
        private String langu;
        private int num;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getOutline() {
            return outline;
        }

        public void setOutline(String outline) {
            this.outline = outline;
        }

        public String getConent() {
            return conent;
        }

        public void setConent(String conent) {
            this.conent = conent;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getPictureTop() {
            return pictureTop;
        }

        public void setPictureTop(String pictureTop) {
            this.pictureTop = pictureTop;
        }

        public String getPictureDetails() {
            return pictureDetails;
        }

        public void setPictureDetails(String pictureDetails) {
            this.pictureDetails = pictureDetails;
        }

        public String getPictureList() {
            return pictureList;
        }

        public void setPictureList(String pictureList) {
            this.pictureList = pictureList;
        }

        public int getIsShow() {
            return isShow;
        }

        public void setIsShow(int isShow) {
            this.isShow = isShow;
        }

        public String getLangu() {
            return langu;
        }

        public void setLangu(String langu) {
            this.langu = langu;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }
}
