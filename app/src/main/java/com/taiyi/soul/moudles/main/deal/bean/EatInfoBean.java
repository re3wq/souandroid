package com.taiyi.soul.moudles.main.deal.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\5 0005 15:26
 * @Author : yuan
 * @Describe ：
 */
public class EatInfoBean {

        public String createtime;
        public String relation_orderid;
        public String flag;
        public String charge;
        public String orderid;
        public String ifrevoke;
        public String maybemoney;
        public String surplusnum;
        public String type;
        public String ifeat;
        public String userid;
        public String surpluscharge;
        public String get_coinname;
        public String money;
        public String price;
        public int postersid;
        public String surplusmoney;
        public String percentage;
        public String lose_coinname;
        public int postersnum;
        public String ifnum;
        public String ordertype;
}
