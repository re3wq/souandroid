package com.taiyi.soul.moudles.mine.myteam;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.taiyi.soul.R;
import com.taiyi.soul.utils.DensityUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * TreeListView的抽象adapter，用户只需要实现getConvertView来定义item的UI，其它具体实现有本类来处理
 *
 * @param <T>
 */
public abstract class TeamTreeViewAdapter<T> extends BaseAdapter
        implements AdapterView.OnItemClickListener {
    protected Context mContext;
    protected List<Node> mNodeList;
    protected List<Node> mVisibleNodeList;
    protected List<Node> mLastNodeList = new ArrayList<>();
    protected ListView mListView;
    protected LayoutInflater mInflater;
    private OnTreeNodeClickListener mOnTreeNodeClickListener;

    public TeamTreeViewAdapter(Context context, ListView listView, List<T> datas, int defaultNodeLevel)
            throws IllegalAccessException {
        this.mContext = context;
        this.mListView = listView;
        mInflater = LayoutInflater.from(context);
        mNodeList = TeamTreeHelper.getNodes(datas, defaultNodeLevel);
        mVisibleNodeList = TeamTreeHelper.getVisibleNodes(mNodeList);
        mListView.setOnItemClickListener(this);
    }

    public void setOnTreeNodeClickListener(OnTreeNodeClickListener onTreeNodeClickListener) {
        this.mOnTreeNodeClickListener = onTreeNodeClickListener;
    }

    @Override
    public int getCount() {
        return mVisibleNodeList.size();
    }

    @Override
    public Object getItem(int position) {
        return mVisibleNodeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int noShow;//当前节点 ，是否有同级 （1有 0无）
    private int isLastG;//判断当前节点是否是所有节点最后一级（1不是 0是）
    private List<Integer> issh;

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Node node = mVisibleNodeList.get(position);
        int i1 = node.getXiaList().size() - 1;
        convertView = getConvertView(node, position, convertView, parent);
        //设置内边距
        noShow = 0;
        isLastG = 0;

        for (int i = (position + 1); i < mVisibleNodeList.size(); i++) {//当前节点 ，下方是否有同级
            if (node.getLevel() == mVisibleNodeList.get(i).getLevel() && node.getFatherid().equals(mVisibleNodeList.get(i).getFatherid())) {//下方还有同级
                noShow = 1;
            }
        }

        if (noShow == 0) {//无同级列表
            mLastNodeList.add(node);
        }
        if (mLastNodeList.size() > 0) {
            for (int j = 0; j < mLastNodeList.size(); j++) {
                if (node.getFatherid().equals(mLastNodeList.get(j).getUserid())) {//当前节点的父id是否是其他节点的id
                    isLastG = 1;//判断当前节点是否是最后一级
                }
            }
        }

        RelativeLayout viewById = convertView.findViewById(R.id.rl_view);
        RecyclerView rv_line = convertView.findViewById(R.id.rv_line);
         if (!node.isRootNode()) {
//            convertView.setPadding((DensityUtil.dip2px(mContext, 40)) + DensityUtil.dip2px(mContext, 15), 0, DensityUtil.dip2px(mContext, 18), 0);
            convertView.setPadding((DensityUtil.dip2px(mContext, 40)), 0, DensityUtil.dip2px(mContext, 8), 0);
        }


        Node node1 = node;
        issh = new ArrayList<>();
        for (int i = 0; i < node.getLevel(); i++) {//node.getLevel()==4  有四级 {0,0,0,0}  循环查看所有级别
            issh.add(0);
            for (int j = 0; j < mVisibleNodeList.size(); j++) {//循环展示的列表

                if (node1.getUserid().equals(mVisibleNodeList.get(j).getUserid())) { //获取当前id排在第j位
                    if (j + 1 >= mVisibleNodeList.size()) {
//                        issh.set(i, 0);
                    } else {
                        for (int k = (j + 1); k < mVisibleNodeList.size(); k++) {//获取第j位后是否有与当前节点相同父ID的节点
                            if (node1.getFatherid().equals(mVisibleNodeList.get(k).getFatherid()) && node1.getLevel() == mVisibleNodeList.get(k).getLevel()) {//下方还有同级
                                issh.set(i, 1);//有相同等级
                            }
                        }
                    }

                }
            }
            node1 = node1.getParentNode();//当前node
        }
        Collections.reverse(issh);

        //在此处修改布局排列方向
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_line.setLayoutManager(layoutManager);
        rv_line.setAdapter(new RecyclerView.Adapter<Holder>() {

            private Holder mHolder;
            View inflate;

            @NonNull
            @Override
            public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                inflate = LayoutInflater.from(mContext).inflate(R.layout.item_team_line, null);
                mHolder = new Holder(inflate);
                return mHolder;
            }

            @Override
            public void onBindViewHolder(@NonNull Holder viewHolder, int i) {

                //i是添加的  |-  图标的个数  ，
                if (i != node.getLevel() - 1) {
                    mHolder.line3.setVisibility(View.INVISIBLE);
                } else {
                    mHolder.line3.setVisibility(View.VISIBLE);
                }


//                if (noShow == 0 && i == node.getLevel() - 1) {//当前下方没有同级 并且 是节点最后一个
//                    mHolder.line2.setVisibility(View.INVISIBLE);//隐藏下边竖线
//                }


                if (isLastG == 1 && i != node.getLevel() - 1) {
                    mHolder.line1.setVisibility(View.VISIBLE);
                    mHolder.line2.setVisibility(View.INVISIBLE);
                }

                if (i != node.getLevel() - 1) {
                    mHolder.line1.setVisibility(View.INVISIBLE);
                }

                if (issh.get(i) == 0) {
                    mHolder.line2.setVisibility(View.INVISIBLE);
                } else {
                    mHolder.line1.setVisibility(View.VISIBLE);
                    mHolder.line2.setVisibility(View.VISIBLE);
                }

                if (noShow == 0 && i == node.getLevel() - 1) {//当前下方没有同级 并且 是节点最后一个
                    mHolder.line2.setVisibility(View.INVISIBLE);//隐藏下边竖线
                }
            }

            @Override
            public int getItemCount() {
                return node.getLevel();
            }
        });
        return convertView;
    }


    class Holder extends RecyclerView.ViewHolder {
        public View line1, line2, line3;


        public Holder(@NonNull View itemView) {
            super(itemView);
            line1 = itemView.findViewById(R.id.line1);
            line2 = itemView.findViewById(R.id.line2);
            line3 = itemView.findViewById(R.id.line3);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        expendOrCollapseNode(position);
        if (mOnTreeNodeClickListener != null) {
            mOnTreeNodeClickListener.onNodeClick(mVisibleNodeList.get(position), position);
        }
    }

    /**
     * node的点击处理过程
     *
     * @param position
     */
    public void expendOrCollapseNode(int position) {
        Node node = mVisibleNodeList.get(position);

        if (!node.isRootNode()) {
            return;
        }
        if (node == null) {
            return;
        }
        if (node.isLeafNode()) {
            return;
        }
        //设置node的展开状态
        node.setExpend(!node.isExpend());
        mVisibleNodeList = TeamTreeHelper.getVisibleNodes(mNodeList);
        notifyDataSetChanged();

    }

    /**
     * TreeListView的Node对外点击接口
     */
    public interface OnTreeNodeClickListener {
        void onNodeClick(Node node, int position);
    }

    /**
     * 对外用户来负责listView的VIew UI，而内部有本类来实现
     *
     * @param node
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    public abstract View getConvertView(Node node, int position, View convertView, ViewGroup parent);


    public void refresh(List<T> datas, int defaultNodeLevel)
            throws IllegalAccessException {
        mNodeList = TeamTreeHelper.getNodes(datas, defaultNodeLevel);
        mVisibleNodeList = TeamTreeHelper.getVisibleNodes(mNodeList);
        notifyDataSetChanged();

    }

}
