package com.taiyi.soul.moudles.login.bean;

public class UserInfoBean {


    private String account;
    private String pwd;
    private boolean isStartFinger;
    private boolean isStartFace;

    public UserInfoBean(String account, String pwd) {
        this.account = account;
        this.pwd = pwd;
    }

    public String getAccount() {
        return account;
    }

    public String getPwd() {
        return pwd;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public boolean isStartFinger() {
        return isStartFinger;
    }

    public void setStartFinger(boolean startFinger) {
        isStartFinger = startFinger;
    }

    public boolean isStartFace() {
        return isStartFace;
    }

    public void setStartFace(boolean startFace) {
        isStartFace = startFace;
    }

    @Override
    public String toString() {
        return "UserInfoBean{" +
                "account='" + account + '\'' +
                ", pwd='" + pwd + '\'' +
                ", isStartFinger='" + isStartFinger + '\'' +
                ", isStartFace='" + isStartFace + '\'' +
                '}';
    }
}
