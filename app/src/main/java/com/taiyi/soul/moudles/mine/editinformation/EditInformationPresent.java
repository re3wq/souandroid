package com.taiyi.soul.moudles.mine.editinformation;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Response;

public class EditInformationPresent extends BasePresent<EditInformationView> {

    //获取会员信息
    public void getUserInfo() {
        HttpUtils.getRequets(BaseUrl.MEMBERINFO, this, null, new JsonCallback<BaseResultBean<EditInformationBean>>() {
            @Override
            public BaseResultBean<EditInformationBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<EditInformationBean>> response) {
                super.onSuccess(response);
                   if(null!=view)
                    view.getSuccess(response.body().status,response.body().msg,response.body().data);
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<EditInformationBean>> response) {
                super.onError(response);
            }
        });
    }


    //修改会员信息
    public void getChangeInfo(String address, String birthday, String nickname, int sex) {


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("address",address);
            jsonObject.put("birthday", birthday);
            jsonObject.put("nickName", nickname);
            jsonObject.put("sex", sex);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        HttpUtils.postRequest(BaseUrl.EDITINFO, this, jsonObject.toString(), new JsonCallback<BaseResultBean<String>>() {
                    @Override
                    public BaseResultBean<String> convertResponse(Response response) throws Throwable {
                        return super.convertResponse(response);
                    }

                    @Override
                    public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                        super.onSuccess(response);
                        if(null!=view)
                        view.getChangeInfoSuccess(response.body().status,response.body().msg,response.body().data);
                    }

                    @Override
                    public void onError(com.lzy.okgo.model.Response<BaseResultBean<String>> response) {
                        super.onError(response);
                    }
                });

    }


}
