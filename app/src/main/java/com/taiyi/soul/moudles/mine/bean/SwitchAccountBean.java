package com.taiyi.soul.moudles.mine.bean;

public class SwitchAccountBean {

    /**
     * token : bc2ae741abbe4b9f9dc48ceaa39db9c3
     * child : true
     * uniqueId : null
     * mainAccount : null
     * bindAccount : null
     * account : null
     * googleFlag : null
     * areaCode : null
     * cur_id : null
     */

    private String token;
    private boolean child;
    private Object uniqueId;
    private Object mainAccount;
    private Object bindAccount;
    private Object account;
    private Object googleFlag;
    private Object areaCode;
    private Object cur_id;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isChild() {
        return child;
    }

    public void setChild(boolean child) {
        this.child = child;
    }

    public Object getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Object uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Object getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(Object mainAccount) {
        this.mainAccount = mainAccount;
    }

    public Object getBindAccount() {
        return bindAccount;
    }

    public void setBindAccount(Object bindAccount) {
        this.bindAccount = bindAccount;
    }

    public Object getAccount() {
        return account;
    }

    public void setAccount(Object account) {
        this.account = account;
    }

    public Object getGoogleFlag() {
        return googleFlag;
    }

    public void setGoogleFlag(Object googleFlag) {
        this.googleFlag = googleFlag;
    }

    public Object getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(Object areaCode) {
        this.areaCode = areaCode;
    }

    public Object getCur_id() {
        return cur_id;
    }

    public void setCur_id(Object cur_id) {
        this.cur_id = cur_id;
    }
}
