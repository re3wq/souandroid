package com.taiyi.soul.moudles.mine.webview;

import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.mine.bean.HelpCenterBean;
import com.taiyi.soul.moudles.mine.bean.WebviewTextBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class WebViewPresent extends BasePresent<WebviewView> {
    public void getData(String type){
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("type",type);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        Map<String,String>map=new HashMap<>();
        map.put("type",type);
    HttpUtils.getRequets(BaseUrl.GET_ABOUT_US, this, map, new JsonCallback<BaseResultBean<WebviewTextBean>>() {
        @Override
        public BaseResultBean<WebviewTextBean> convertResponse(Response response) throws Throwable {
            return super.convertResponse(response);
        }

        @Override
        public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<WebviewTextBean>> response) {
            super.onSuccess(response);
            if (null != view) {
                if (response.body().status == 200) {
                    view.onSuccess(response.body().data);
                } else {
                    view.onFailure(response.body().msg);
                }
            }
        }
        @Override
        public void onError(com.lzy.okgo.model.Response<BaseResultBean<WebviewTextBean>> response) {
            super.onError(response);
        }
    });
    }

    public void getHelpCenter(String type){
        Map<String,String>map=new HashMap<>();
        map.put("type",type);
        HttpUtils.getRequets(BaseUrl.GET_HELP_CENTER, this, map, new JsonCallback<BaseResultBean<HelpCenterBean>>() {
            @Override
            public BaseResultBean<HelpCenterBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<HelpCenterBean>> response) {
                super.onSuccess(response);
                if (null != view) {
                    if (response.body().status == 200) {
                        view.getHelpCenterSuccess(response.body().data);
                    } else {
                        view.onFailure(response.body().msg);
                    }
                }
            }
            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<HelpCenterBean>> response) {
                super.onError(response);
            }
        });
    }
}
