package com.taiyi.soul.moudles.main.deal.adapter;

import android.content.Context;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.deal.bean.EntrustThreeBean;
import com.taiyi.soul.utils.NumUtils;
import com.zcolin.gui.pullrecyclerview.BaseRecyclerAdapter;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：
 */
public class EntrustThreeAdapter extends BaseRecyclerAdapter<EntrustThreeBean.ComlistBean> {


    private PullRecyclerView pullRecyclerView;
    private Context context;

    //    private DealItemClick dealItemClick;
    public EntrustThreeAdapter(Context context) {
        this.context = context;
//        this.dealItemClick = dealItemClick;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_entrust;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final EntrustThreeBean.ComlistBean data) {
        TextView entrust_time = getView(holder, R.id.entrust_time);
        TextView entrust_total = getView(holder, R.id.entrust_total);
        TextView entrust_amount = getView(holder, R.id.entrust_amount);
        TextView issue_num = getView(holder, R.id.issue_num);
        TextView entrust_price = getView(holder, R.id.entrust_price);
        TextView entrust_total_unit = getView(holder, R.id.entrust_total_unit);
        TextView entrust_amount_unit = getView(holder, R.id.entrust_amount_unit);
        TextView entrust_price_unit = getView(holder, R.id.entrust_price_unit);
        TextView entrust_charge = getView(holder, R.id.entrust_charge);
        TextView entrust_cancel = getView(holder, R.id.entrust_cancel);
        TextView entrust_pay = getView(holder, R.id.entrust_pay);
        issue_num.setText(context.getResources().getString(R.string.issue_num_two));
        entrust_time.setText(data.applytime);
        entrust_total.setText(NumUtils.subZeroAndDot(data.dealmoney));//总价格
        entrust_amount.setText(data.postersnum + "");//成交数量
        entrust_price.setText(NumUtils.subZeroAndDot(data.price));//单价
        entrust_charge.setText(data.charge);//手续费


        if (data.flag == 0) {//买入
            entrust_pay.setBackgroundResource(R.mipmap.deal_item_buy);
            entrust_pay.setText(R.string.deal_bt_pay);
        } else if(data.flag ==1){//卖出
            entrust_pay.setBackgroundResource(R.mipmap.deal_item_sell);
            entrust_pay.setText(R.string.deal_bt_sell);
        }

        if(data.ordertype== 2 ){//已取消
            entrust_cancel.setText(context.getResources().getString(R.string.entrust_canceled));
        }else if(data.ordertype== 1 ){//已完成
            entrust_cancel.setText(context.getResources().getString(R.string.entrust_completed));
        }

//        entrust_total_unit.setText(data.total_type);
//        entrust_amount_unit.setText(data.num_type);
        entrust_price_unit.setText(context.getResources().getString(R.string.USDN));
        entrust_total_unit.setText(context.getResources().getString(R.string.USDN));

    }

    public void setPullRecyclerView(PullRecyclerView pullRecyclerView) {
        this.pullRecyclerView = pullRecyclerView;
    }
//    //买卖单点击
//    public interface DealItemClick {
//        void onDeal(int position);
//    }
}

