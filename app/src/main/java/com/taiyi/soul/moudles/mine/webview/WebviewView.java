package com.taiyi.soul.moudles.mine.webview;

import com.taiyi.soul.moudles.mine.bean.HelpCenterBean;
import com.taiyi.soul.moudles.mine.bean.WebviewTextBean;

public interface WebviewView {
    void onSuccess(WebviewTextBean list);
    void onFailure(String errorMsg);
    void getHelpCenterSuccess(HelpCenterBean helpCenterBean);
}
