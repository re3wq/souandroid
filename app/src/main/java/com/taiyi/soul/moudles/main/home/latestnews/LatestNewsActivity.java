package com.taiyi.soul.moudles.main.home.latestnews;

import android.content.Context;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.container.DefaultHeader;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.MultiItemTypeAdapter;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.home.bean.NewsListBean;
import com.taiyi.soul.moudles.main.webview.WebViewActivity;
import com.taiyi.soul.utils.DensityUtil;
import com.zhouwei.mzbanner.MZBannerView;
import com.zhouwei.mzbanner.holder.MZHolderCreator;
import com.zhouwei.mzbanner.holder.MZViewHolder;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class LatestNewsActivity extends BaseActivity<LatestNewsView, LatestNewsPresent> implements LatestNewsView, SpringView.OnFreshListener, MultiItemTypeAdapter.OnItemClickListener {

    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.springView)
    SpringView springView;
    @BindView(R.id.top)
    ConstraintLayout top;
    @BindView(R.id.banner)
    MZBannerView banner;
    private int pageNo = 1;
    List<NewsListBean.PageVOBean.ListBean> list = new LinkedList<>();
    private int newsID;
    private String newsUrl;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_latest_news;
    }

    @Override
    public LatestNewsPresent initPresenter() {
        return new LatestNewsPresent();
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText(getString(R.string.news));
        springView.setFooter(new DefaultFooter(this));
        springView.setHeader(new DefaultHeader(this));
        springView.setGive(SpringView.Give.BOTH);
        springView.setType(SpringView.Type.FOLLOW);
        springView.setListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        CommonAdapter firstNewsListAdapter = AdapterManger.getLatestNewsListAdapter(this, list);
        recyclerView.setAdapter(firstNewsListAdapter);
        firstNewsListAdapter.setOnItemClickListener(this);
    }

    @Override
    protected void initData() {
        showProgress();
        presenter.getData(pageNo);
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.backIv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIv:
                finish();
                break;

        }
    }

    @Override
    public void onSuccess(NewsListBean newsListBean) {
        hideProgress();
        springView.onFinishFreshAndLoad();
        List<NewsListBean.PageVOBean.ListBean> mList = newsListBean.getPageVO().getList();
        if (pageNo == 1) {
            list.clear();
            List<NewsListBean.BannerNewsBean> bannerNews = newsListBean.getBannerNews();
            initBanner(bannerNews);
        }
        list.addAll(mList);
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    private void initBanner(List<NewsListBean.BannerNewsBean> bannerNews) {
        banner.setIndicatorVisible(true);
        banner.setIndicatorAlign(MZBannerView.IndicatorAlign.RIGHT);
        banner.setIndicatorRes(R.mipmap.mall_dot_nom, R.mipmap.mall_dot);
        banner.setIndicatorPadding(0, 0, 40, 20);
        banner.setPages(bannerNews, new MZHolderCreator<BannerViewHolder>() {
            @Override
            public BannerViewHolder createViewHolder() {
                return new BannerViewHolder();
            }
        });

        banner.start();
    }

    @Override
    public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
        newsID = list.get(position).getId();
        newsUrl = list.get(position).getUrl();
        showProgress();
        presenter.updateReadNum(String.valueOf(newsID));
    }

    @Override
    public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
        return false;
    }

    public class BannerViewHolder implements MZViewHolder<NewsListBean.BannerNewsBean> {
        private ImageView mImageView;

        @Override
        public View createView(Context context) {
            // 返回页面布局文件
            View view = LayoutInflater.from(context).inflate(R.layout.item_banner, null);
            mImageView = view.findViewById(R.id.banner_image);
            return view;
        }


        @Override
        public void onBind(Context context, int position, NewsListBean.BannerNewsBean data) {
            // 数据绑定
            Glide.with(context).load(data.getPictureTop()).apply(RequestOptions.bitmapTransform(new RoundedCorners(DensityUtil.dip2px(LatestNewsActivity.this,20)))).into(mImageView);
        }
    }


    @Override
    public void onFailure(String errorMsg) {
        hideProgress();
        springView.onFinishFreshAndLoad();
        toast(errorMsg);
    }

    @Override
    public void updateSuccess() {
        hideProgress();
        Bundle bundle = new Bundle();
        bundle.putInt("id", newsID);
        bundle.putString("url", newsUrl);
        ActivityUtils.next(this, WebViewActivity.class, bundle);
    }

    @Override
    public void updateFailure(String errorMsg) {
        hideProgress();
        toast(errorMsg);
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        showProgress();
        presenter.getData(pageNo);
    }

    @Override
    public void onLoadmore() {
        pageNo += 1;
        showProgress();
        presenter.getData(pageNo);
    }
}
