package com.taiyi.soul.moudles.main.assets.exchange.bean;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\7 0007 16:21
 * @Author : yuan
 * @Describe ：兑换币种列表
 */
public class ExchangeBean {

        public List<CowListBean> cowList;

        public static class CowListBean {
            public String to_coin_name;
            public String charge;
            public String hour_max_num;
            public String exchange_rate;
            public String to_coin_id;
            public String usdn_rate;
            public String min_num;
            public String id;
            public String from_coin_id;
            public String from_coin_name;
            public String max_num;
            public String url;
    }
}
