package com.taiyi.soul.moudles.main.deal.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\4 0004 15:34
 * @Author : yuan
 * @Describe ：
 */
public class EatNumberBean {

    //生成签名 -pay_price+ pay_coin_name（HASH不生成签名，根据pay_coin_name获取对应合约）
    public String pay_price= "";//吃卖
    public String have_price= "";//吃买
    public String pay_coin_name= "";
    public String have_coin_name= "";
}
