package com.taiyi.soul.moudles.main.deal.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.deal.bean.DealPayBean;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\6\16 0016 15:58
 * @Author : yuan
 * @Describe ：交易-我的委托-adapter
 */
public class MainEntrustAdapter extends BaseQuickAdapter<DealPayBean, BaseViewHolder> {

    private DealItemClick dealItemClick;
    public MainEntrustAdapter(List<DealPayBean> data) {
        super(R.layout.item_entrust, data);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder holder, DealPayBean item) {
        TextView entrust_time = holder.getView(R.id.entrust_time);
        TextView entrust_total = holder.getView(R.id.entrust_total);
        TextView entrust_amount = holder.getView(R.id.entrust_amount);
        TextView entrust_price = holder.getView(R.id.entrust_price);
        TextView entrust_total_unit = holder.getView(R.id.entrust_total_unit);
        TextView entrust_amount_unit = holder.getView(R.id.entrust_amount_unit);
        TextView entrust_price_unit = holder.getView(R.id.entrust_price_unit);

        entrust_time.setText(item.names);
        entrust_total.setText(item.total);
        entrust_amount.setText(item.num);
        entrust_price.setText(item.price);

        entrust_total_unit.setText(item.total_type);
        entrust_amount_unit.setText(item.num_type);
        entrust_price_unit.setText(item.price_type);

    }

    public void onDealItemClick(DealItemClick dealItemClick){
        this.dealItemClick = dealItemClick;
    }
    //买卖单点击
    public interface DealItemClick{
        void onDeal();
    }
}

