package com.taiyi.soul.moudles.main.assets.distribution;

import android.graphics.Color;
import android.os.Bundle;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.AdapterManger;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.moudles.main.assets.distribution.bean.AssetDistributionBean;
import com.taiyi.soul.moudles.main.assets.distribution.bean.PieBean;
import com.taiyi.soul.moudles.main.assets.distribution.present.AssetDistributionPresent;
import com.taiyi.soul.moudles.main.assets.distribution.present.AssetDistributionView;
import com.taiyi.soul.utils.AlertDialogShowUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static java.lang.Math.abs;

public class AssetDistributionActivity extends BaseActivity<AssetDistributionView, AssetDistributionPresent> implements AssetDistributionView {


    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.pie_chart)
    PieChart mPieChart;
    @BindView(R.id.pie_recycle)
    RecyclerView mPieRecycle;
    @BindView(R.id.percentage)
    TextView mPercentage;
    @BindView(R.id.unit)
    TextView mUnit;
    @BindView(R.id.asset_tit)
    TextView mAssetTit;
    private float mTotalAssets;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_asset_distribution;
    }

    @Override
    public AssetDistributionPresent initPresenter() {
        return new AssetDistributionPresent();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initData() {
        mTvTitle.setText(getResources().getString(R.string.asset_distribution_title));
        showProgress();
        presenter.getData();
    }

    @Override
    public void initEvent() {

    }


    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }

    /************************************Other methods****************************************************/

    //实例化扇形图与赋值
    private void initPie(double totalAssets, List<AssetDistributionBean.PercentBean> bean) {
        PieBean pieBean = new PieBean();
        pieBean.beanList = new ArrayList<>();

        if (totalAssets > 0) {
            for (int i = 0; i < bean.size(); i++) {
                float percentage = Float.parseFloat(bean.get(i).percentage) * 100;
                String color = bean.get(i).color;
                pieBean.beanList.add(new PieBean.PieDataBean(percentage, Color.parseColor(color), bean.get(i).coinName));
            }

        } else {
            for (int i = 0; i < bean.size(); i++) {
                String color = bean.get(i).color;
                float v = 100.0f / bean.size();
                pieBean.beanList.add(new PieBean.PieDataBean(v, Color.parseColor(color), bean.get(i).coinName));
            }
        }


        List<PieEntry> list = new ArrayList<>();//比率
        for (int i = 0; i < pieBean.beanList.size(); i++) {
//            float percentage = Float.parseFloat(bean.get(i).percentage)*100;
            list.add(new PieEntry(pieBean.beanList.get(i).percentages * 100, ""));
        }
        ArrayList<Integer> colors = new ArrayList<Integer>();//颜色
        for (int i = 0; i < pieBean.beanList.size(); i++) {
//            colors.add(Color.parseColor(bean.get(i).color));
            colors.add(pieBean.beanList.get(i).colors);
        }
        ArrayList<String> units = new ArrayList<String>();
        for (int i = 0; i < pieBean.beanList.size(); i++) {
//            units.add(bean.get(i).coinName);
            units.add(pieBean.beanList.get(i).unit);
        }


        PieDataSet dataSet = new PieDataSet(list, "");
        dataSet.setColors(colors);
        PieData pieData = new PieData(dataSet);

        //设置是否显示各个单位的百分比
        pieData.setDrawValues(false);
        //中心文字是否显示
        mPieChart.setDrawCenterText(false);
        //中心圆颜色，半径
        mPieChart.setTransparentCircleAlpha(100);
        mPieChart.setHoleColor(getResources().getColor(R.color.transparent));
        mPieChart.setHoleRadius(70f);
        mPieChart.setTransparentCircleRadius(70f);
        //是否可以手动旋转
        mPieChart.setRotationEnabled(false);

        mPieChart.animateY(1000, Easing.EaseInOutQuad); //设置动画

        mPieChart.setData(pieData);

        mPieChart.invalidate();

        //设置右下角显示的标签lable
        Description description = new Description();
        description.setText("");
        mPieChart.setDescription(description);
        Legend mLegend = mPieChart.getLegend();
        mLegend.setEnabled(false);//禁用下方比例块
        mPieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {//选择
//                Log.e("tag===", String.valueOf(h));
                mAssetTit.setVisibility(View.GONE);
                mPercentage.setVisibility(View.VISIBLE);
                mUnit.setVisibility(View.VISIBLE);

                mPercentage.setText(pieBean.beanList.get((int) h.getX()).percentages + "%");
                mUnit.setText(units.get((int) h.getX()));
            }

            @Override
            public void onNothingSelected() {//关闭选择
//                Log.e("tag===", "close");
                mAssetTit.setVisibility(View.VISIBLE);
                mPercentage.setVisibility(View.GONE);
                mUnit.setVisibility(View.GONE);
            }
        });

        List<PieBean.PieDataBean> datas = new ArrayList<>();
        if(mTotalAssets!=0.0f){
            for (int i = 0; i < pieBean.beanList.size(); i++) {
                if(pieBean.beanList.get(i).percentages != 0.0f){
                    String color = bean.get(i).color;
                    datas.add(new PieBean.PieDataBean(pieBean.beanList.get(i).percentages, Color.parseColor(color), bean.get(i).coinName));
                }
            }
        }else {
            for (int i = 0; i < pieBean.beanList.size(); i++) {
                if("USDS".equals(bean.get(i).coinName) || "SOU".equals(bean.get(i).coinName) ){
                    String color = bean.get(i).color;
                    datas.add(new PieBean.PieDataBean(0.0000f, Color.parseColor(color), bean.get(i).coinName));
                }
            }

        }
        mPieRecycle.setLayoutManager(new GridLayoutManager(AssetDistributionActivity.this,2));
        mPieRecycle.setAdapter(AdapterManger.getAssetCurrencyListAdapter(AssetDistributionActivity.this, datas));
    }


    /************************************Call Interface callback****************************************************/

    @Override
    public void onSuccess(int code, String msg_cn, AssetDistributionBean bean) {
        hideProgress();
        if (code == 200) {
            mTotalAssets = Float.parseFloat(bean.totalAssets);
            initPie(Double.parseDouble(bean.totalAssets), bean.percent);
        } else {
            AlertDialogShowUtil.toastMessage(this, msg_cn);
        }
    }
}
