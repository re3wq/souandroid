package com.taiyi.soul.moudles.mine.accountmanagement.detail.importprivatekey;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.base.BaseActivity;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.moudles.normalvp.NormalPresenter;
import com.taiyi.soul.moudles.normalvp.NormalView;
import com.taiyi.soul.utils.CopyUtils;
import com.taiyi.soul.utils.EncryptUtil;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.zxing.ZXingUtils;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ImportPrivateKeyActivity extends BaseActivity<NormalView, NormalPresenter> {
    @BindView(R.id.top)
    ConstraintLayout top;
    @BindView(R.id.qrCodeIv)
    ImageView qrCodeIv;
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.accountNameTv)
    TextView accountNameTv;
    @BindView(R.id.privateKeyTv)
    TextView privateKeyTv;
    String privateKey = "";
    @Override
    protected int getLayoutId() {
        return R.layout.activity_import_private_key;
    }

    @Override
    public NormalPresenter initPresenter() {
        return new NormalPresenter();
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {
        titleTv.setText("Export Private Key");
        ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
        if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
            accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
        }

        String accountName = getIntent().getExtras().getString("accountName");
        accountNameTv.setText(accountName);
        for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
            if (accountInfoBean.getAccount_name().equals(accountName)) {
                String account_active_private_key =
                        accountInfoBean.getAccount_active_private_key().equals("")?
                                accountInfoBean.getAccount_owner_private_key()
                                : accountInfoBean.getAccount_active_private_key();

                String loginPwd = Utils.getSpUtils().getString("loginPwd");
                try {
                    privateKey = EncryptUtil.getDecryptString(account_active_private_key, loginPwd);
                } catch (NoSuchAlgorithmException e) {
                } catch (InvalidKeySpecException e) {
                }
                privateKeyTv.setText(privateKey);
                Bitmap qrImage = ZXingUtils.createQRImage(privateKey, 148, 148);
                qrCodeIv.setImageBitmap(qrImage);
                break;
            }
        }
    }

    @Override
    protected void initImmersionBar() {
        super.initImmersionBar();
        mImmersionBar.titleBar(top).init();
    }

    @Override
    protected void initData() {

    }

    @Override
    public void initEvent() {

    }
    @OnClick({R.id.copyTv,R.id.backIv})
public void onClick(View view){
        switch (view.getId()){
            case R.id.backIv:
                finish();
                break;
            case R.id.copyTv:
                toast(getString(R.string.copy_success));
                CopyUtils.CopyToClipboard(this,privateKey);
                break;
        }
}
}
