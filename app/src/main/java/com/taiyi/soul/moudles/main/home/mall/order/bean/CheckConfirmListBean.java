package com.taiyi.soul.moudles.main.home.mall.order.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\8\7 0007 14:03
 * @Author : yuan
 * @Describe ：
 */
public class CheckConfirmListBean {
    public String img;
    public String name;
    public String price;
    public String specification;
    public String num;
}
