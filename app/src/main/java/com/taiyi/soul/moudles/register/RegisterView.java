package com.taiyi.soul.moudles.register;

import com.taiyi.soul.bean.AreaBean;
import com.taiyi.soul.moudles.register.bean.RegisterBean;

import java.util.List;

public interface RegisterView {
    void getVerificationCodeSuccess();
    void getVerificationCodeFailure(String errorMsg);

    void getInvitePersonSuccess(String invitePeople);

    void getInvitePersonFailure(String errorMsg);

    void registerSuccess(RegisterBean registerBean);

    void registerFailure(String errorMsg);

    void mainAccountOnlyFailure(String errorMsg);
    void mainAccountOnlySuccess();

    void getAreaCodeSuccess(List<AreaBean> list);
}
