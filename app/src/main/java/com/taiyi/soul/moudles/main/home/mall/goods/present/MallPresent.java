package com.taiyi.soul.moudles.main.home.mall.goods.present;

import android.app.Activity;

import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.base.BasePresent;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.bean.BaseResultBean;
import com.taiyi.soul.moudles.login.LoginActivity;
import com.taiyi.soul.moudles.main.home.mall.bean.CarListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.MallFirstBean;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.AlertDialogShowUtil;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class MallPresent extends BasePresent<MallView> {


    //商城首页数据
    public void getData(Activity activity){
        HttpUtils.postRequest(BaseUrl.MALL_FIRST, this, (String) null, new JsonCallback<BaseResultBean<MallFirstBean>>() {
            @Override
            public BaseResultBean<MallFirstBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<MallFirstBean>> response) {
                super.onSuccess(response);
                if (response.body().code==0){
                    if(null!=view)
                    view.onSuccess(response.body().data);
                }else if(response.body().code==-1){
                    if (null != view)
                        view.onFailure();
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                }else {
                    if (null != view)
                        view.onFailure();
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<MallFirstBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }

    //获取购物车是否为空
    public void getCarSizeData(Activity activity, String pagenum) {
        Map<String, String> map = new HashMap<>();
        map.put("pagenum", pagenum);
        HttpUtils.postRequest(BaseUrl.GOODS_CARLIST, this, map, new JsonCallback<BaseResultBean<CarListBean>>() {
            @Override
            public BaseResultBean<CarListBean> convertResponse(Response response) throws Throwable {
                return super.convertResponse(response);
            }

            @Override
            public void onSuccess(com.lzy.okgo.model.Response<BaseResultBean<CarListBean>> response) {
                super.onSuccess(response);
                if (response.body().code == 0) {
                    if (null != view)
                        view.onCarSizeSuccess(response.body().data);
                } else if (response.body().code == -1) {
                    ToastUtils.showShortToast(response.body().msg_cn);
                   //  AppManager.getAppManager().finishAllActivity();
                    ActivityUtils.next(activity, LoginActivity.class, true);
                    Utils.getSpUtils().remove(Constants.TOKEN);
                    if (null != view)
                        view.onFailure();
                } else {
                    AlertDialogShowUtil.toastMessage(activity, response.body().msg_cn);
                    if (null != view)
                        view.onFailure();
                }
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<BaseResultBean<CarListBean>> response) {
                super.onError(response);
                if (null != view)
                    view.onFailure();
            }
        });
    }
}
