package com.taiyi.soul.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDexApplication;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheEntity;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.cookie.CookieJarImpl;
import com.lzy.okgo.cookie.store.SPCookieStore;
import com.lzy.okgo.interceptor.HttpLoggingInterceptor;
import com.lzy.okgo.model.HttpHeaders;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.gen.DaoMaster;
import com.taiyi.soul.gen.DaoSession;
import com.taiyi.soul.gen.MySQLiteOpenHelper;

import com.taiyi.soul.utils.ContextHolder;
import com.taiyi.soul.utils.LocalManageUtil;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.utils.language.LanguageUtils;
import com.tencent.bugly.crashreport.CrashReport;
import com.zhy.autolayout.config.AutoLayoutConifg;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.crypto.NoSuchPaddingException;

import devliving.online.securedpreferencestore.DefaultRecoveryHandler;
import devliving.online.securedpreferencestore.SecuredPreferenceStore;
import okhttp3.OkHttpClient;


/**
 * Created by NGK on 2020/4/7.
 */

public class MyApplication extends MultiDexApplication {

    private static MyApplication mInstance;

    public SPCookieStore mSPCookieStore;

    private static DaoSession daoSession;

    private WalletBean walletBean = new WalletBean();

    public WalletBean getWalletBean() {
        return walletBean;
    }

    public void setWalletBean(WalletBean walletBean) {
        this.walletBean = walletBean;
    }


    public static DaoSession getDaoSession() {
        return daoSession;
    }

    public void setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    public SPCookieStore getSPCookieStore() {
        return mSPCookieStore;
    }

    public void setSPCookieStore(SPCookieStore SPCookieStore) {
        mSPCookieStore = SPCookieStore;
    }


    @Override
    protected void attachBaseContext(Context base) {

//        if (Utils.getSpUtils().getString("current_language","").equals("")) {
//            //保存系统选择语言
        LocalManageUtil.saveSystemCurrentLanguage(base);
        super.attachBaseContext(LocalManageUtil.setLocal(base));
//        }
    }


    @Override
    public void onCreate() {
        super.onCreate();

//
//        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                .detectDiskReads()
//                .detectDiskWrites()
//                .detectNetwork()
//                .detectAll()
//                .penaltyLog()
//                //penaltyDeath()
//                .build());
        mInstance = this;
        ContextHolder.initial(this);


        LocalManageUtil.setApplicationLanguage(getApplicationContext());


        AutoLayoutConifg.getInstance().useDeviceSize();
        String storeFileName = "securedStore";
//not mandatory, can be null too
        String keyPrefix = "vss";
//it's better to provide one, and you need to provide the same key each time after the first time
        byte[] seedKey = "SecuredSeedData".getBytes();
        try {
            SecuredPreferenceStore.init(this, storeFileName, keyPrefix, seedKey, new DefaultRecoveryHandler());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (SecuredPreferenceStore.MigrationFailedException e) {
            e.printStackTrace();
        }

        //查看数据库数据（还有依赖）
//        DebugDB.getAddressLog();
        //配置数据库

        setupDatabase();

        Utils.init(getApplicationContext());

        setSPCookieStore(new SPCookieStore(this));

        initOkGo();
        initBugly();

        registerActivityLifecycleCallbacks();

    }


    private void registerActivityLifecycleCallbacks() {
        mInstance.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
                // 对Application和Activity更新上下文的语言环境
                LanguageUtils.applyAppLanguage(activity);
            }

            @Override
            public void onActivityStarted(@NonNull Activity activity) {

            }

            @Override
            public void onActivityResumed(@NonNull Activity activity) {

            }

            @Override
            public void onActivityPaused(@NonNull Activity activity) {

            }

            @Override
            public void onActivityStopped(@NonNull Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(@NonNull Activity activity) {

            }
        });
    }


    private void initBugly() {
        Context context = getApplicationContext();
// 获取当前包名
        String packageName = context.getPackageName();
// 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
// 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
// 初始化Bugly
        CrashReport.initCrashReport(context, "b5a8da60e4", false, strategy);
    }
    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 配置数据库
     */
    private void setupDatabase() {
        //创建数据库
        MySQLiteOpenHelper helper = new MySQLiteOpenHelper(this, "sou.db", null);
        //获取可写数据库
        SQLiteDatabase db = helper.getWritableDatabase();
        //获取数据库对象
        DaoMaster daoMaster = new DaoMaster(db);
        //获取Dao对象管理者
        daoSession = daoMaster.newSession();

    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mInstance != null) {
            mInstance = null;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //保存系统选择语言
        LocalManageUtil.onConfigurationChanged(getApplicationContext());
    }


    public void initOkGo() {


        //先设置代理
//        Properties prop = System.getProperties();
//        //proxyhostIPaddress
//        String proxyHost = "localhost";
//        //proxyport
//        String proxyPort = "8888";
//        prop.put("proxyHost", proxyHost);
//        prop.put("proxyPort", proxyPort);
//        prop.put("proxySet", "true");


        HttpHeaders headers = new HttpHeaders();
//        if (new SPCookieStore(this).getAllCookie().size() != 0) {
//            headers.put("Set-Cookie", String.valueOf(mSPCookieStore.getCookie(HttpUrl.parse(BaseUrl.HTTP_Get_code_auth))));
//        }
        headers.put("version", "3.0");
//        if (Utils.getSpUtils().getString("loginmode", "").equals("phone")) {
//            headers.put("uid", MyApplication.getDaoInstant().getUserBeanDao().queryBuilder().where(UserBeanDao.Properties.Wallet_phone.eq(Utils.getSpUtils().getString("firstUser"))).build().unique().getWallet_uid());
//        } else {
//            headers.put("uid", "6f1a8e0eb24afb7ddc829f96f9f74e9d");
//        }


        OkHttpClient.Builder builder = new OkHttpClient.Builder();

//            HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(getAssets().open("papi_ngk_org.cer"));
//            builder.sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager);

        //log相关
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor("OkGo");

        loggingInterceptor.setPrintLevel(HttpLoggingInterceptor.Level.BODY);        //log打印级别，决定了log显示的详细程度
        loggingInterceptor.setColorLevel(Level.INFO);                               //log颜色级别，决定了log在控制台显示的颜色
        builder.addInterceptor(loggingInterceptor);                                 //添加OkGo默认debug日志
        //超时时间设置
        builder.readTimeout(30000, TimeUnit.MILLISECONDS);      //全局的读取超时时间
        builder.writeTimeout(30000, TimeUnit.MILLISECONDS);     //全局的写入超时时间
        builder.connectTimeout(30000, TimeUnit.MILLISECONDS);   //全局的连接超时时间
        builder.cookieJar(new CookieJarImpl(mSPCookieStore));            //使用sp保持cookie，如果cookie不过期，则一直有效

//            //方法三：使用预埋证书，校验服务端证书（自签名证书）
//            HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(getAssets().open("papi_ngk_org.chained.crt"));

        // 其他统一的配置
        OkGo.getInstance().init(this)//必须调用初始化
                .setOkHttpClient(builder.build())//必须设置OkHttpClient
                .setCacheMode(CacheMode.NO_CACHE)               //全局统一缓存模式，默认不使用缓存，可以不传
                .setCacheTime(CacheEntity.CACHE_NEVER_EXPIRE)   //全局统一缓存时间，默认永不过期，可以不传
                .setRetryCount(0)          //全局统一超时重连次数，默认为三次，那么最差的情况会请求4次(一次原始请求，三次重连请求)，不需要可以设置为0
                .addCommonHeaders(headers);              //全局公共头
//                .addCommonParams(httpParams);                       //全局公共参数
//            Glide.get(getApplicationContext()).getRegistry().replace(GlideUrl.class, InputStream.class,new OkHttpUrlLoader.Factory(builder.build()));


//        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(getAssets().open("server.cer"));
//        builder.sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager);
////        //配置https的域名匹配规则，使用不当会导致https握手失败
//        builder.hostnameVerifier(HttpsUtils.UnSafeHostnameVerifier);


    }


//    /**
//     * 初始化夜间模式
//     */
//    private void setNightMode() {
//
//        boolean isNight = Utils.getSpUtils().getBoolean("isNight", false);
//        AppCompatDelegate.setDefaultNightMode(isNight  ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
//    }


}

