package com.taiyi.soul.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 *
 * 账号信息
 */

public class AccountInfoBean implements Parcelable, Serializable {
    private String account_name;
    private String account_active_private_key;
    private String account_active_public_key;
    private String account_owner_private_key;
    private String account_owner_public_key;
    private String account_gen_mnemonic;

    public String getAccount_gen_mnemonic() {
        return account_gen_mnemonic == null ? "" : account_gen_mnemonic;
    }

    public void setAccount_gen_mnemonic(String account_gen_mnemonic) {
        this.account_gen_mnemonic = account_gen_mnemonic;
    }

    public String getAccount_name() {
        return account_name == null ? "" : account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }


    public String getAccount_active_private_key() {
        return account_active_private_key == null ? "" : account_active_private_key;
    }

    public void setAccount_active_private_key(String account_active_private_key) {
        this.account_active_private_key = account_active_private_key;
    }

    public String getAccount_active_public_key() {
        return account_active_public_key == null ? "" : account_active_public_key;
    }

    public void setAccount_active_public_key(String account_active_public_key) {
        this.account_active_public_key = account_active_public_key;
    }

    public String getAccount_owner_private_key() {
        return account_owner_private_key == null ? "" : account_owner_private_key;
    }

    public void setAccount_owner_private_key(String account_owner_private_key) {
        this.account_owner_private_key = account_owner_private_key;
    }

    public String getAccount_owner_public_key() {
        return account_owner_public_key == null ? "" : account_owner_public_key;
    }

    public void setAccount_owner_public_key(String account_owner_public_key) {
        this.account_owner_public_key = account_owner_public_key;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.account_name);
        dest.writeString(this.account_active_private_key);
        dest.writeString(this.account_active_public_key);
        dest.writeString(this.account_owner_private_key);
        dest.writeString(this.account_owner_public_key);
        dest.writeString(this.account_gen_mnemonic);
    }

    public AccountInfoBean() {
    }

    protected AccountInfoBean(Parcel in) {
        this.account_name = in.readString();
        this.account_active_private_key = in.readString();
        this.account_active_public_key = in.readString();
        this.account_owner_private_key = in.readString();
        this.account_owner_public_key = in.readString();
        this.account_gen_mnemonic = in.readString();

    }

    public static final Creator<AccountInfoBean> CREATOR = new Creator<AccountInfoBean>() {
        @Override
        public AccountInfoBean createFromParcel(Parcel source) {
            return new AccountInfoBean(source);
        }

        @Override
        public AccountInfoBean[] newArray(int size) {
            return new AccountInfoBean[size];
        }
    };




    @Override
    public String toString() {
        return "AccountInfoBean{" +
                "account_name='" + account_name + '\'' +
                ", account_active_private_key='" + account_active_private_key + '\'' +
                ", account_active_public_key='" + account_active_public_key + '\'' +
                ", account_owner_private_key='" + account_owner_private_key + '\'' +
                ", account_owner_public_key='" + account_owner_public_key + '\'' +
                ", account_gen_mnemonic='" + account_gen_mnemonic + '\'' +
                '}';
    }
}
