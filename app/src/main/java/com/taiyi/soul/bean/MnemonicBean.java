package com.taiyi.soul.bean;

public class MnemonicBean {
    public String name;
    public boolean isSelect;

    public MnemonicBean(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
