package com.taiyi.soul.bean;

import java.io.Serializable;

/**
 * author: pearce on 2019-10-16
 * version: v1.0.0
 */
public class BaseResult<T> implements Serializable {
    public int code;
    public String msg;
    public String msg_cn;
    public T data = null;
}
