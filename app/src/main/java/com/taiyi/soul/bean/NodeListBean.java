package com.taiyi.soul.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\2 0002 17:25
 * @Author : yuan
 * @Describe ：
 */
public class NodeListBean {

        public String id;
        public String chainCode;
        public String name;
        public String httpAddress;
        public boolean isSuper;
        public String address;
        public int errorCount;
        public int timeOut;
        public int priority;
        public boolean queryAlternative;
        public boolean playerAlternative;
        public boolean serverAlternative;
        public String deleter;
        public Object deleteTime;
        public String deleteIp;
        public int state;
        public String createTime;

}
