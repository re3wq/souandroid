package com.taiyi.soul.bean;

public class Language {

    private String lang;
    private String isSelect;
    private String code;

    public Language(String lang, String isSelect, String code) {
        this.lang = lang;
        this.isSelect = isSelect;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(String isSelect) {
        this.isSelect = isSelect;
    }

    public String getLang() {

        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
