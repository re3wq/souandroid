package com.taiyi.soul.bean;

import java.io.Serializable;

/**
 * Created by NGK on 2018/4/2.
 */


public class BaseResultBean<T> implements Serializable {

    public int status;
    public int code;
    public String msg;
    public String msg_cn;
    public T data = null;

}