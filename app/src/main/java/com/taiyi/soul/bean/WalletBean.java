package com.taiyi.soul.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/4/14
 * Time: 3:15 PM
 */

@Entity
public class WalletBean {

    @Id(autoincrement = true)
    private Long id;
    private String wallet_uid;
    private String wallet_name;
    private String wallet_main_account;
    private String wallet_phone;
    private String wallet_email;
    private String wallet_shapwd;
    private String account_info;
    private String wallet_type;

    @Generated(hash = 1349119695)
    public WalletBean(Long id, String wallet_uid, String wallet_name, String wallet_main_account,
            String wallet_phone, String wallet_email, String wallet_shapwd, String account_info,
            String wallet_type) {
        this.id = id;
        this.wallet_uid = wallet_uid;
        this.wallet_name = wallet_name;
        this.wallet_main_account = wallet_main_account;
        this.wallet_phone = wallet_phone;
        this.wallet_email = wallet_email;
        this.wallet_shapwd = wallet_shapwd;
        this.account_info = account_info;
        this.wallet_type = wallet_type;
    }
    @Generated(hash = 1814219826)
    public WalletBean() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getWallet_uid() {
        return this.wallet_uid;
    }
    public void setWallet_uid(String wallet_uid) {
        this.wallet_uid = wallet_uid;
    }
    public String getWallet_name() {
        return this.wallet_name;
    }
    public void setWallet_name(String wallet_name) {
        this.wallet_name = wallet_name;
    }
    public String getWallet_main_account() {
        return this.wallet_main_account;
    }
    public void setWallet_main_account(String wallet_main_account) {
        this.wallet_main_account = wallet_main_account;
    }
    public String getWallet_phone() {
        return this.wallet_phone;
    }
    public void setWallet_phone(String wallet_phone) {
        this.wallet_phone = wallet_phone;
    }
    public String getWallet_shapwd() {
        return this.wallet_shapwd;
    }
    public void setWallet_shapwd(String wallet_shapwd) {
        this.wallet_shapwd = wallet_shapwd;
    }
    public String getAccount_info() {
        return this.account_info;
    }
    public void setAccount_info(String account_info) {
        this.account_info = account_info;
    }
    public String getWallet_type() {
        return this.wallet_type;
    }
    public void setWallet_type(String wallet_type) {
        this.wallet_type = wallet_type;
    }

    public String getWallet_email() {
        return wallet_email;
    }

    public void setWallet_email(String wallet_email) {

        this.wallet_email = wallet_email;
    }

    @Override
    public String toString() {
        return "WalletBean{" +
                "id=" + id +
                ", wallet_uid='" + wallet_uid + '\'' +
                ", wallet_name='" + wallet_name + '\'' +
                ", wallet_main_account='" + wallet_main_account + '\'' +
                ", wallet_phone='" + wallet_phone + '\'' +
                ", wallet_email='" + wallet_email + '\'' +
                ", wallet_shapwd='" + wallet_shapwd + '\'' +
                ", account_info='" + account_info + '\'' +
                ", wallet_type='" + wallet_type + '\'' +
                '}';
    }

}
