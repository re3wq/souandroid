package com.taiyi.soul.bean;

public class AreaBean {

    /**
     * id : 1
     * url : null
     * area : +86
     */

    private int id;
    private String url;
    private String area;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
