package com.taiyi.soul.bean;

/**
 * Created by Android Studio.
 *
 * @Date on 2020\7\2 0002 17:30
 * @Author : yuan
 * @Describe ：
 */
public class TokenListBean {


        public String name;
        public String symbol;
        public String issuer;
        public String contract;
        public String logo;
        public boolean isSystem;
        public String webSite;
        public String whitePaperUrl;
        public String faceBookUrl;
        public String twitterUrl;
        public String issueState;
        public String issueDate;
        public String issueCost;
        public int rmbPrice;
        public double dollarPrice;

}
