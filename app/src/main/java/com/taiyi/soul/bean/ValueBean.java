package com.taiyi.soul.bean;

/**
 * Created by Android Studio.
 * User: ahui
 * Date: 2020-02-03
 * Time: 19:48
 */
public class ValueBean {

    private String key;
    private String value;


    public ValueBean(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
