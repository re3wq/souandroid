package com.taiyi.soul.blockchain;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lzy.okgo.model.Response;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.blockchain.api.EosChainInfo;
import com.taiyi.soul.blockchain.bean.GetRequiredKeys;
import com.taiyi.soul.blockchain.bean.JsonToBeanResultBean;
import com.taiyi.soul.blockchain.bean.JsonToBinRequest;
import com.taiyi.soul.blockchain.bean.RequreKeyResult;
import com.taiyi.soul.blockchain.chain.Action;
import com.taiyi.soul.blockchain.chain.PackedTransaction;
import com.taiyi.soul.blockchain.chain.SignedTransaction;
import com.taiyi.soul.blockchain.cypto.ec.EosPrivateKey;
import com.taiyi.soul.blockchain.types.TypeChainId;
import com.taiyi.soul.blockchain.util.GsonEosTypeAdapterFactory;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.PublicAndPrivateKeyUtils;
import com.taiyi.soul.utils.ShowDialog;
import com.taiyi.soul.utils.ToastUtils;
import com.taiyi.soul.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ngk on 2020/6/30.
 * ngk离线签名
 */

public class EosSignDataManger {
    static String ACTIONTRANSFER = Constants.ACTIONTRANSFER;

    Context mContext;
    EosChainInfo mChainInfoBean = new EosChainInfo();
    JsonToBeanResultBean mJsonToBeanResultBean = new JsonToBeanResultBean();
    String[] permissions;
    SignedTransaction txnBeforeSign;
    Gson mGson = new GsonBuilder()
            .registerTypeAdapterFactory(new GsonEosTypeAdapterFactory())
            .excludeFieldsWithoutExposeAnnotation().create();

    String contract, action, message;

    Callback mCallback;

    public EosSignDataManger(Context context, Callback callback) {
        mContext = context;
        this.mCallback = callback;
    }


    public void pushAction(String message, String contract, String permissionAccount) {
        this.message = message;
        this.contract = contract;

        this.action = ACTIONTRANSFER;

        permissions = new String[]{permissionAccount + "@" + PublicAndPrivateKeyUtils.getActivePublicKey(permissionAccount)};

        getChainInfo();
    }

    public void getChainInfo() {//chain/get_info
        HttpUtils.getRequetsChain(BaseUrl.HTTP_GET_CHAIN_INFO, this, new HashMap<String, String>(), new JsonCallback<EosChainInfo>() {
            @Override
            public void onSuccess(Response<EosChainInfo> response) {
                mChainInfoBean =  response.body();
                getabi_json_to_bin();
            }

            @Override
            public void onError(Response<EosChainInfo> response) {
                super.onError(response);
                if (ShowDialog.dialog != null) {
                    ShowDialog.dissmiss();
                }
                mCallback.fail();
            }
        });
    }

    public void getabi_json_to_bin() {

        JsonToBinRequest jsonToBinRequest = new JsonToBinRequest(contract, action, message.replaceAll("\\r|\\n", ""));
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_ABI_JSON_TO_BIN, this, mGson.toJson(jsonToBinRequest), new JsonCallback<JsonToBeanResultBean>() {
            @Override
            public void onSuccess(Response<JsonToBeanResultBean> response) {

                mJsonToBeanResultBean = response.body();
                txnBeforeSign = createTransaction(contract, action, mJsonToBeanResultBean.getBinargs(), permissions, mChainInfoBean);
//                //扫描钱包列出所有可用账号的公钥
                List<String> pubKey =  PublicAndPrivateKeyUtils.getActivePublicKey();
                if (pubKey==null){
                    ToastUtils.showLongToast(mContext.getString(R.string.private_key_err));
                    if (ShowDialog.dialog != null) {
                        ShowDialog.dissmiss();
                    }
                    return;
                }
                getRequreKey(new GetRequiredKeys(txnBeforeSign, pubKey));

            }


            @Override
            public void onError(Response<JsonToBeanResultBean> response) {
                super.onError(response);
                if (ShowDialog.dialog != null) {
                    ShowDialog.dissmiss();
                }
                mCallback.fail();
            }
        });
    }

    private SignedTransaction createTransaction(String contract, String actionName, String dataAsHex,
                                                String[] permissions, EosChainInfo chainInfo) {

        Action action = new Action(contract, actionName);
        action.setAuthorization(permissions);
        action.setData(dataAsHex);


        SignedTransaction txn = new SignedTransaction();
        txn.addAction(action);
        txn.putSignatures(new ArrayList<String>());


        if (null != chainInfo) {
            txn.setReferenceBlock(chainInfo.getHeadBlockId());
            txn.setExpiration(chainInfo.getTimeAfterHeadBlockTime(30000));
        }
        return txn;
    }

    public void getRequreKey(GetRequiredKeys getRequiredKeys) {

        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_REQUIRED_KEYS, this, mGson.toJson(getRequiredKeys), new JsonCallback<RequreKeyResult>() {
            @Override
            public void onSuccess(Response<RequreKeyResult> response) {

                RequreKeyResult requreKeyResult = response.body();
                if (requreKeyResult == null || requreKeyResult.getRequired_keys() == null || requreKeyResult.getRequired_keys().size()<1){
                    ToastUtils.showLongToast(mContext.getString(R.string.private_key_err));
                    if (ShowDialog.dialog != null) {
                        ShowDialog.dissmiss();
                    }
                    return;
                }

                EosPrivateKey eosPrivateKey = new EosPrivateKey(PublicAndPrivateKeyUtils.getPrivateKey(requreKeyResult.getRequired_keys().get(0), Utils.getSpUtils().getString("loginPwd","")));


                txnBeforeSign.sign(eosPrivateKey, new TypeChainId(mChainInfoBean.getChain_id()));


                if (ShowDialog.dialog != null) {
                    ShowDialog.dissmiss();
                }
                mCallback.onSignSuccess(mGson.toJson(new PackedTransaction(txnBeforeSign)));



            }

            @Override
            public void onError(Response<RequreKeyResult> response) {
                super.onError(response);
                if (ShowDialog.dialog != null) {
                    ShowDialog.dissmiss();
                }
                mCallback.fail();
            }
            @Override
            public void onFinish() {
                super.onFinish();

            }
        });

    }


    public interface Callback {
        void onSignSuccess(String sign_data);
        void fail();
    }


}
