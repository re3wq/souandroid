package com.taiyi.soul.blockchain;

import android.content.Context;

import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.blockchain.api.EosChainInfo;
import com.taiyi.soul.blockchain.bean.GetRequiredKeys;
import com.taiyi.soul.blockchain.bean.JsonToBeanResultBean;
import com.taiyi.soul.blockchain.bean.JsonToBinRequest;
import com.taiyi.soul.blockchain.bean.PushSuccessBean;
import com.taiyi.soul.blockchain.bean.RequreKeyResult;
import com.taiyi.soul.blockchain.chain.Action;
import com.taiyi.soul.blockchain.chain.PackedTransaction;
import com.taiyi.soul.blockchain.chain.SignedTransaction;
import com.taiyi.soul.blockchain.cypto.ec.EosPrivateKey;
import com.taiyi.soul.blockchain.types.TypeChainId;
import com.taiyi.soul.blockchain.util.GsonEosTypeAdapterFactory;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.PublicAndPrivateKeyUtils;
import com.taiyi.soul.utils.ToastUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lzy.okgo.model.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by LinkToken on 2018/5/8.
 * Dapp合约调用
 */

public class DappDatamanger {

    private final static String ACTIONTRANSFER = Constants.ACTIONTRANSFER;
    private final static String PERMISSONION = Constants.PERMISSONION;

    private Callback mCallback;
    private Context mContext;
    private EosChainInfo mChainInfoBean = new EosChainInfo();
    private JsonToBeanResultBean mJsonToBeanResultBean = new JsonToBeanResultBean();
    private String[] permissions;
    private SignedTransaction txnBeforeSign;
    private Gson mGson = new GsonBuilder()
            .registerTypeAdapterFactory(new GsonEosTypeAdapterFactory())
            .excludeFieldsWithoutExposeAnnotation().create();

    private String contract, action, message, userpassword;

    public DappDatamanger(Context context, String userpassword, Callback callback) {
        mCallback = callback;
        mContext = context;
        this.userpassword = userpassword;
    }


    public void pushAction(String message, String contract, String permissionAccount) {
        this.message = message;
        this.contract = contract;

        this.action = ACTIONTRANSFER;

        permissions = new String[]{permissionAccount + "@" + PublicAndPrivateKeyUtils.getActivePublicKey(permissionAccount)};
        getChainInfo();
    }

    /**
     * dapp签名
     * @param message
     */
    public void pushAction1(String message) {
        this.message = message;
        getChainInfo1();
    }

    public void getChainInfo() {
        HttpUtils.getRequetsChain(BaseUrl.HTTP_GET_CHAIN_INFO, this, new HashMap<String, String>(), new JsonCallback<EosChainInfo>() {
            @Override
            public void onSuccess(Response<EosChainInfo> response) {

                mChainInfoBean = response.body();
                getabi_json_to_bin();

            }

            @Override
            public void onError(Response<EosChainInfo> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
                mCallback.erroMsg(mContext.getString(R.string.socket_exception));
            }
        });
    }



    public void getChainInfo1() {
        HttpUtils.getRequetsChain(BaseUrl.HTTP_GET_CHAIN_INFO, this, new HashMap<String, String>(), new JsonCallback<EosChainInfo>() {
            @Override
            public void onSuccess(Response<EosChainInfo> response) {

                mChainInfoBean = response.body();

                //扫描钱包列出所有可用账号的公钥
                List<String> pubKey = PublicAndPrivateKeyUtils.getActivePublicKey();

                JsonObject jsonObject = (JsonObject)JsonUtil.parseStringToBean(message, JsonObject.class);
                JsonObject jsonObject1 = jsonObject.getAsJsonObject("transaction");

                txnBeforeSign = createTransaction1(jsonObject1);

                getRequreKey1(new GetRequiredKeys(txnBeforeSign, pubKey));

            }

            @Override
            public void onError(Response<EosChainInfo> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
                mCallback.erroMsg(mContext.getString(R.string.socket_exception));
            }
        });
    }

    private SignedTransaction createTransaction1(JsonObject jsonObject) {
        String account ;
        String name ;
        String data ;
        String[] authorization = null;
        SignedTransaction txn = new SignedTransaction();

        JsonArray array = jsonObject.getAsJsonArray("actions");
        if (array != null && array.size()>0){
            for (int i = 0 ; i < array.size(); i++){

                account = array.get(i).getAsJsonObject().get("account").getAsString();
                name = array.get(i).getAsJsonObject().get("name").getAsString();
                data = array.get(i).getAsJsonObject().get("data").getAsString();

                JsonArray arrayAuth = array.get(i).getAsJsonObject().get("authorization").getAsJsonArray();
                if (arrayAuth != null && arrayAuth.size()>0){

                    authorization = new String[arrayAuth.size()];

                    for (int j = 0 ; j < arrayAuth.size(); j++){
                        String actor = arrayAuth.get(j).getAsJsonObject().get("actor").getAsString();
                        String active = arrayAuth.get(j).getAsJsonObject().get("permission").getAsString();
                        authorization[j] = actor + "@" + active;
                    }
                }

                Action action = new Action(account, name);
//                String actor = arrayAuth.get(0).getAsJsonObject().get("actor").getAsString();
//                String active = arrayAuth.get(0).getAsJsonObject().get("permission").getAsString();
//                String [] authorization = new String[]{actor + "@" + active};
                action.setAuthorization(authorization);
                action.setData(data);
                txn.addAction(action);
            }
        }

        txn.setDelay_sec(0);
        txn.setMax_net_usage_words(0);
        txn.setMax_cpu_usage_ms(0);
        txn.setExpiration(jsonObject.get("expiration").getAsString());
        txn.setRef_block_num(Integer.parseInt(jsonObject.get("ref_block_num").getAsString()));
        txn.setRef_block_prefix(Long.parseLong(jsonObject.get("ref_block_prefix").getAsString()));
        txn.putSignatures(new ArrayList<String>());

        return txn;
    }


    public void getabi_json_to_bin() {
        JsonToBinRequest jsonToBinRequest = new JsonToBinRequest(contract, action, message.replaceAll("\\r|\\n", ""));
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_ABI_JSON_TO_BIN, this, mGson.toJson(jsonToBinRequest), new JsonCallback<JsonToBeanResultBean>() {
            @Override
            public void onSuccess(Response<JsonToBeanResultBean> response) {

                JsonToBeanResultBean jsonToBeanResultBean = response.body();
                mJsonToBeanResultBean = jsonToBeanResultBean;
                txnBeforeSign = createTransaction(contract, action, mJsonToBeanResultBean.getBinargs(), permissions, mChainInfoBean);
                //扫描钱包列出所有可用账号的公钥
                List<String> pubKey = PublicAndPrivateKeyUtils.getActivePublicKey();

                getRequreKey(new GetRequiredKeys(txnBeforeSign, pubKey));
            }

            @Override
            public void onError(Response<JsonToBeanResultBean> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
                mCallback.erroMsg(mContext.getString(R.string.socket_exception));
            }
        });
    }

    private SignedTransaction createTransaction(String contract, String actionName, String dataAsHex,
                                                String[] permissions, EosChainInfo chainInfo) {

        Action action = new Action(contract, actionName);
        action.setAuthorization(permissions);
        action.setData(dataAsHex);


        SignedTransaction txn = new SignedTransaction();
        txn.addAction(action);
        txn.putSignatures(new ArrayList<String>());


        if (null != chainInfo) {
            txn.setReferenceBlock(chainInfo.getHeadBlockId());
            txn.setExpiration(chainInfo.getTimeAfterHeadBlockTime(30000));
        }
        return txn;
    }

    public void getRequreKey(GetRequiredKeys getRequiredKeys) {

        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_REQUIRED_KEYS, this, mGson.toJson(getRequiredKeys), new JsonCallback<RequreKeyResult>() {
            @Override
            public void onSuccess(Response<RequreKeyResult> response) {
                RequreKeyResult requreKeyResult = response.body();
                EosPrivateKey eosPrivateKey = new EosPrivateKey(PublicAndPrivateKeyUtils.getPrivateKey(requreKeyResult.getRequired_keys().get(0), userpassword));
                txnBeforeSign.sign(eosPrivateKey, new TypeChainId(mChainInfoBean.getChain_id()));
                pushTransactionRetJson(new PackedTransaction(txnBeforeSign));

            }

            @Override
            public void onError(Response<RequreKeyResult> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
                mCallback.erroMsg(mContext.getString(R.string.socket_exception));
            }
        });

    }

    public void getRequreKey1(GetRequiredKeys getRequiredKeys) {

        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_REQUIRED_KEYS, this, mGson.toJson(getRequiredKeys), new JsonCallback<RequreKeyResult>() {
            @Override
            public void onSuccess(Response<RequreKeyResult> response) {

                RequreKeyResult requreKeyResult = response.body();
                if (requreKeyResult.getRequired_keys() == null || requreKeyResult.getRequired_keys().size()<0){
                    ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
                    mCallback.erroMsg(mContext.getString(R.string.socket_exception));
                    return;
                }
                EosPrivateKey eosPrivateKey = new EosPrivateKey(PublicAndPrivateKeyUtils.getPrivateKey(requreKeyResult.getRequired_keys().get(0), userpassword));
                txnBeforeSign.sign(eosPrivateKey, new TypeChainId(mChainInfoBean.getChain_id()));
                mCallback.getTxid(JsonUtil.getGson().toJson(txnBeforeSign.getSignatures()));
            }

            @Override
            public void onError(Response<RequreKeyResult> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
                mCallback.erroMsg(mContext.getString(R.string.socket_exception));
            }
        });

    }


    public void pushTransactionRetJson(PackedTransaction body) {
        HttpUtils.postRequestChain(BaseUrl.HTTP_PUSH_TRANSACTION, this, mGson.toJson(body), new JsonCallback< PushSuccessBean.DataBeanX>() {
            @Override
            public void onSuccess(final Response< PushSuccessBean.DataBeanX> response) {
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }

                PushSuccessBean.DataBeanX dataBeanX = response.body();
                mCallback.getTxid(dataBeanX.getTransaction_id());

            }

            @Override
            public void onError(Response<PushSuccessBean.DataBeanX> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
                mCallback.erroMsg(mContext.getString(R.string.socket_exception));
            }
        });
    }

    public void push(String contract, String action, String message, String permissionAccount) {
        this.message = message;
        this.contract = contract;
        this.action = action;
        permissions = new String[]{permissionAccount + "@" + PERMISSONION};
        getChainInfo();
    }

    public interface Callback {

        void getTxid(String txId);

        void erroMsg(String msg);
    }
}
