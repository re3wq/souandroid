package com.taiyi.soul.blockchain;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lzy.okgo.model.Response;
import com.taiyi.soul.R;
import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.blockchain.api.EosChainInfo;
import com.taiyi.soul.blockchain.bean.GetRequiredKeys;
import com.taiyi.soul.blockchain.bean.JsonToBeanResultBean;
import com.taiyi.soul.blockchain.bean.JsonToBinRequest;
import com.taiyi.soul.blockchain.bean.PushSuccessBean;
import com.taiyi.soul.blockchain.bean.RequreKeyResult;
import com.taiyi.soul.blockchain.chain.Action;
import com.taiyi.soul.blockchain.chain.PackedTransaction;
import com.taiyi.soul.blockchain.chain.SignedTransaction;
import com.taiyi.soul.blockchain.cypto.ec.EosPrivateKey;
import com.taiyi.soul.blockchain.types.TypeChainId;
import com.taiyi.soul.blockchain.util.GsonEosTypeAdapterFactory;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.PublicAndPrivateKeyUtils;
import com.taiyi.soul.utils.ToastUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by LinkToken on 2018/5/11.
 * 币答测试管理
 */

public class AskansDatamanger {

    private Callback mCallback;
    private Context mContext;
    private EosChainInfo mChainInfoBean = new EosChainInfo();
    private JsonToBeanResultBean mJsonToBeanResultBean = new JsonToBeanResultBean();
    private String[] permissions;
    private SignedTransaction txnBeforeSign;
    private Gson mGson = new GsonBuilder()
            .registerTypeAdapterFactory(new GsonEosTypeAdapterFactory())
            .excludeFieldsWithoutExposeAnnotation().create();

    private String contract, action, message, userpassword;

    public AskansDatamanger(Context context, String userpassword, Callback callback) {
        mContext = context;
        mCallback = callback;
        this.userpassword = userpassword;
    }

    public void pushAction(String contract, String action, String message, String permissionAccount) {
        this.message = message;
        this.contract = contract;
        this.action = action;
        permissions = new String[]{permissionAccount + "@" + PublicAndPrivateKeyUtils.getActivePublicKey(permissionAccount)};
        getChainInfo();
    }

    public void getChainInfo() {
        HttpUtils.getRequetsChain(BaseUrl.HTTP_GET_CHAIN_INFO, this, new HashMap<String, String>(), new JsonCallback<EosChainInfo>() {
            @Override
            public void onSuccess(Response<EosChainInfo> response) {
                mChainInfoBean =  response.body();
                getabi_json_to_bin();
            }

            @Override
            public void onError(Response<EosChainInfo> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
            }
        });
    }

    public void getabi_json_to_bin() {

        JsonToBinRequest jsonToBinRequest = new JsonToBinRequest(contract, action, message.replaceAll("\\r|\\n", ""));
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_ABI_JSON_TO_BIN, this, mGson.toJson(jsonToBinRequest), new JsonCallback<JsonToBeanResultBean>() {
            @Override
            public void onSuccess(Response<JsonToBeanResultBean> response) {
                mJsonToBeanResultBean = response.body();
                txnBeforeSign = createTransaction(contract, action, mJsonToBeanResultBean.getBinargs(), permissions, mChainInfoBean);
                //扫描钱包列出所有可用账号的公钥
                List<String> pubKey =  PublicAndPrivateKeyUtils.getActivePublicKey();

                getRequreKey(new GetRequiredKeys(txnBeforeSign, pubKey));

            }


            @Override
            public void onError(Response<JsonToBeanResultBean> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
            }
        });
    }

    private SignedTransaction createTransaction(String contract, String actionName, String dataAsHex,
                                                String[] permissions, EosChainInfo chainInfo) {

        Action action = new Action(contract, actionName);
        action.setAuthorization(permissions);
        action.setData(dataAsHex);


        SignedTransaction txn = new SignedTransaction();
        txn.addAction(action);
        txn.putSignatures(new ArrayList<String>());


        if (null != chainInfo) {
            txn.setReferenceBlock(chainInfo.getHeadBlockId());
            txn.setExpiration(chainInfo.getTimeAfterHeadBlockTime(30000));
        }
        return txn;
    }



    public void getRequreKey(GetRequiredKeys getRequiredKeys) {

        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_REQUIRED_KEYS, this, mGson.toJson(getRequiredKeys), new JsonCallback<RequreKeyResult>() {
            @Override
            public void onSuccess(Response<RequreKeyResult> response) {
                RequreKeyResult requreKeyResult = response.body();
                EosPrivateKey eosPrivateKey = new EosPrivateKey(PublicAndPrivateKeyUtils.getPrivateKey(requreKeyResult.getRequired_keys().get(0), userpassword));
                txnBeforeSign.sign(eosPrivateKey, new TypeChainId(mChainInfoBean.getChain_id()));
                pushTransactionRetJson(new PackedTransaction(txnBeforeSign));

            }

            @Override
            public void onError(Response<RequreKeyResult> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
            }
        });

    }



    public void pushTransactionRetJson(PackedTransaction body) {
        HttpUtils.postRequestChain(BaseUrl.HTTP_PUSH_TRANSACTION, this, mGson.toJson(body), new JsonCallback<PushSuccessBean.DataBeanX>() {
            @Override
            public void onSuccess(final Response<PushSuccessBean.DataBeanX> response) {
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }

                PushSuccessBean.DataBeanX dataBeanX = response.body();
                mCallback.isApprove(action);

            }

            @Override
            public void onError(Response<PushSuccessBean.DataBeanX> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                ToastUtils.showLongToast(mContext.getString(R.string.socket_exception));
            }
        });
    }

    public interface Callback {
        void isApprove(String action);
    }
}
