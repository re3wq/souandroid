package com.taiyi.soul.blockchain.util;

import org.bitcoinj.core.ECKey;
import org.bitcoinj.crypto.ChildNumber;
import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.crypto.HDKeyDerivation;
import org.bitcoinj.crypto.MnemonicCode;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.github.novacrypto.bip39.MnemonicGenerator;
import io.github.novacrypto.bip39.Words;
import io.github.novacrypto.bip39.wordlists.English;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/6/10
 * Time: 2:33 PM
 */
public class ChainUtils {

    private static String PATH = "m/44'/194'/0'/0/0";


    public static ECKey genECKey(String mnemonic, String passphrase){

        ChildNumber childNumber = null;
        byte[] seedBytes = MnemonicCode.toSeed(Arrays.asList(mnemonic.split(" ")), passphrase);
        DeterministicKey dkKey = HDKeyDerivation.createMasterPrivateKey(seedBytes);
        ArrayList<String> pathArray = null;
        if (!PATH.isEmpty()){
            pathArray = (ArrayList<String>) Arrays.asList(PATH.split("/"));
        }

        if (pathArray != null) {

            for (int i = 0; i < pathArray.size(); i++) {
                if (pathArray.get(i).contains("'")) {
                    int number = Integer.parseInt(pathArray.get(i).substring(0, pathArray.get(i).length() - 1));
                    new ChildNumber(number, true);
                } else {
                    int number = Integer.parseInt(pathArray.get(i));
                    new ChildNumber(number, false);
                }

                dkKey = HDKeyDerivation.deriveChildKey(dkKey, childNumber);

            }

            return ECKey.fromPrivate(dkKey.getPrivKeyBytes());
        }else {

            return null;
        }

    }

    public static String genMnemonic(){

        StringBuilder sb = new StringBuilder();
        byte[] entropy = new byte[Words.TWELVE.byteLength()];
        new SecureRandom().nextBytes(entropy);
        new MnemonicGenerator(English.INSTANCE).createMnemonic(entropy, sb::append);
        return sb.toString();

    }


    public static List<String> genMnemonicList(){
        return Arrays.asList(genMnemonic().split(" "));

    }
}
