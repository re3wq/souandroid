package com.taiyi.soul.blockchain.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by swapnibble on 2017-09-12.
 */

public class RequreKeyResult implements Serializable {

    private List<String> required_keys;

    public List<String> getRequired_keys() {
        return required_keys;
    }

    public void setRequired_keys(List<String> required_keys) {
        this.required_keys = required_keys;
    }
}
