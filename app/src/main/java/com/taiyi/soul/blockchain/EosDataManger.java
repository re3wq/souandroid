package com.taiyi.soul.blockchain;

import android.content.Context;
import android.os.Bundle;

import com.taiyi.soul.base.BaseUrl;
import com.taiyi.soul.base.BaseView;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.blockchain.api.EosChainInfo;
import com.taiyi.soul.blockchain.bean.GetRequiredKeys;
import com.taiyi.soul.blockchain.bean.JsonToBeanResultBean;
import com.taiyi.soul.blockchain.bean.JsonToBinRequest;
import com.taiyi.soul.blockchain.bean.PushSuccessBean;
import com.taiyi.soul.blockchain.bean.RequreKeyResult;
import com.taiyi.soul.blockchain.chain.Action;
import com.taiyi.soul.blockchain.chain.PackedTransaction;
import com.taiyi.soul.blockchain.chain.SignedTransaction;
import com.taiyi.soul.blockchain.cypto.ec.EosPrivateKey;
import com.taiyi.soul.blockchain.types.TypeChainId;
import com.taiyi.soul.blockchain.util.GsonEosTypeAdapterFactory;
import com.taiyi.soul.net.HttpUtils;
import com.taiyi.soul.net.callbck.JsonCallback;
import com.taiyi.soul.utils.JsonUtil;
import com.taiyi.soul.utils.PublicAndPrivateKeyUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lzy.okgo.model.Response;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLException;

import okhttp3.ResponseBody;

/**
 * Created by LinkToken on 2018/5/2.
 * eosX适配
 */

public class EosDataManger {
    private static String ACTIONTRANSFER = Constants.ACTIONTRANSFER;

    private Context mContext;
    private EosChainInfo mChainInfoBean = new EosChainInfo();
    private JsonToBeanResultBean mJsonToBeanResultBean = new JsonToBeanResultBean();
    private String[] permissions;
    private SignedTransaction txnBeforeSign;
    private Gson mGson = new GsonBuilder()
            .registerTypeAdapterFactory(new GsonEosTypeAdapterFactory())
            .excludeFieldsWithoutExposeAnnotation().create();

    private String contract, action, message, userpassword;
    private int type = 1; //0即为红包 ， 1为转账

    private Callback mCallback;
    private BaseView mView;

    public EosDataManger(Context context, BaseView view, String password, Callback callback) {
        mContext = context;
        mView = view;
        this.userpassword = password;
        this.mCallback = callback;
    }

    public EosDataManger(Context context, String password, Callback callback) {
        mContext = context;
        this.userpassword = password;
        this.mCallback = callback;
    }



    public void pushAction(String message, String contract, String permissionAccount) {
        this.message = message;
        this.contract = contract;

        this.action = ACTIONTRANSFER;
        this.type = 1;

        permissions = new String[]{permissionAccount + "@" +
                PublicAndPrivateKeyUtils.getActivePublicKey(permissionAccount)};
        getChainInfo();
    }

    public void getChainInfo() {
        if (mView != null) mView.showProgress();
        HttpUtils.getRequetsChain(BaseUrl.HTTP_GET_CHAIN_INFO, this, new HashMap<>(),
                new JsonCallback<String>() {
            @Override
            public void onSuccess(Response<String> response) {
                try {
                    mChainInfoBean = JsonUtil.parseStringToBean(response.body(), EosChainInfo.class);
                    getabi_json_to_bin();
                } catch (Exception e) {
                    e.printStackTrace();
                    mCallback.fail(0, e.getMessage());
                }
            }

            @Override
            public void onError(Response<String> response) {
//                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                fail(response);
            }
        });
    }

    public void getabi_json_to_bin() {
        if (mView != null) mView.showProgress();
        JsonToBinRequest jsonToBinRequest = new JsonToBinRequest(contract, action, message.replaceAll("\\r|\\n", ""));
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_ABI_JSON_TO_BIN, this, mGson.toJson(jsonToBinRequest), new JsonCallback<String>() {
            @Override
            public void onSuccess(Response<String> response) {
                try {
                    mJsonToBeanResultBean = JsonUtil.parseStringToBean(response.body(), JsonToBeanResultBean.class);
                    if (mJsonToBeanResultBean != null) {
                        txnBeforeSign = createTransaction(contract, action, mJsonToBeanResultBean.getBinargs(), permissions, mChainInfoBean);
                    }
                    //扫描钱包列出所有可用账号的公钥
                    List<String> pubKey = PublicAndPrivateKeyUtils.getActivePublicKey();

                    getRequreKey(new GetRequiredKeys(txnBeforeSign, pubKey));
                } catch (Exception e) {
                    e.printStackTrace();
                    mCallback.fail(0, e.getMessage());
                }
            }

            @Override
            public void onError(Response<String> response) {
                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                fail(response);
            }
        });
    }

    private SignedTransaction createTransaction(String contract, String actionName, String dataAsHex,
                                                String[] permissions, EosChainInfo chainInfo) {

        Action action = new Action(contract, actionName);
        action.setAuthorization(permissions);
        action.setData(dataAsHex);


        SignedTransaction txn = new SignedTransaction();
        txn.addAction(action);
        txn.putSignatures(new ArrayList<String>());


        if (null != chainInfo) {
            txn.setReferenceBlock(chainInfo.getHeadBlockId());
            txn.setExpiration(chainInfo.getTimeAfterHeadBlockTime(30000));
        }
        return txn;
    }

    public void getRequreKey(GetRequiredKeys getRequiredKeys) {
        if (mView != null) mView.showProgress();
        HttpUtils.postRequestChain(BaseUrl.HTTP_GET_REQUIRED_KEYS, this, mGson.toJson(getRequiredKeys), new JsonCallback<String>() {
            @Override
            public void onSuccess(Response<String> response) {
                RequreKeyResult requreKeyResult = JsonUtil.parseStringToBean(response.body(),RequreKeyResult.class);
                if (requreKeyResult != null && requreKeyResult.getRequired_keys() != null && requreKeyResult.getRequired_keys().size() > 0) {
                    EosPrivateKey eosPrivateKey = new EosPrivateKey(PublicAndPrivateKeyUtils.getPrivateKey(requreKeyResult.getRequired_keys().get(0),
                            userpassword));
                    txnBeforeSign.sign(eosPrivateKey, new TypeChainId(mChainInfoBean.getChain_id()));
                    pushTransactionRetJson(new PackedTransaction(txnBeforeSign));
                } else {
                    fail(response);
                }
            }

            @Override
            public void onError(Response<String> response) {
//                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                fail(response);
            }
        });
    }

    private void fail(Response<?> response) {
        try {
            if (response.getException() instanceof SocketTimeoutException ||
                    response.getException() instanceof SocketException ||
                    response.getException() instanceof UnknownHostException ||
                    response.getException() instanceof SSLException) {
                //网络原因
                if (mCallback != null) {
                    mCallback.fail(0x9999, "");
                }
                return;
            }
            String body = "";
            if (response.body() == null) {
                ResponseBody responseBody = response.getRawResponse().body();
                if (responseBody != null) {
                    body = responseBody.string();
                }
            } else {
                body = response.body().toString();
            }
            Gson gson = JsonUtil.getGson();
            Map map = gson.fromJson(body, Map.class);
            if (map != null) {
                Map error = (Map) map.get("error");
                if (error != null) {
                    List<Map> details = (List<Map>)error.get("details");
                    if (details != null && details.size() > 0) {
                        mCallback.fail((double) error.get("code"), (String) details.get(0).get("message"));
                    }else {
                        mCallback.fail((double) map.get("code"), map.toString());
                    }
                } else {
                    mCallback.fail((double) map.get("code"), map.toString());
                }
            } else {
                mCallback.fail(0, body);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mCallback.fail(0, e.getMessage());
        }

    }

    public void pushTransactionRetJson(PackedTransaction body) {
        if (mView != null) mView.showProgress();
        HttpUtils.postRequestChain(BaseUrl.HTTP_PUSH_TRANSACTION, this, mGson.toJson(body), new JsonCallback<String>() {
            @Override
            public void onSuccess(final Response<String> response) {
                try {
                    PushSuccessBean.DataBeanX dataBeanX = JsonUtil.parseStringToBean(response.body(), PushSuccessBean.DataBeanX.class);
                    final Bundle bundle = new Bundle();
                    if (type == 1) {
//                    if (ShowDialog.dialog != null) {
//                        ShowDialog.dissmiss();
//                    }
                        if (dataBeanX != null) {
                            mCallback.onSuccess(dataBeanX.getTransaction_id());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    mCallback.fail(0, e.getMessage());
                }
            }

            @Override
            public void onError(Response<String> response) {
//                super.onError(response);
//                if (ShowDialog.dialog != null) {
//                    ShowDialog.dissmiss();
//                }
                fail(response);
            }
        });
    }


    public interface Callback {

        void onSuccess(String data);

        void fail(double code, String msg);
    }
}
