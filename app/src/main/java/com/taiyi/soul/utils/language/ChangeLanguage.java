package com.taiyi.soul.utils.language;

import android.app.backup.BackupManager;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

import com.taiyi.soul.base.Constants;


import java.util.Locale;


public class ChangeLanguage {

    private static Locale setLanguageLocale;

    public static Context changeLanguage(Context context) {

        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
//API >= 17 可以使用
        if (Constants.LANGUAGEPOSITION == 20) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setLanguageLocale = context.getResources().getConfiguration().getLocales().get(0);
            }
        } else {
            setLanguageLocale = getSetLanguageLocale();

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(setLanguageLocale);
        } else {
            configuration.locale = setLanguageLocale;
        }
        context = context.createConfigurationContext(configuration);
        BackupManager.dataChanged("com.android.providers.settings");
        return context;
    }

    public static Locale getSetLanguageLocale() {

        switch (Constants.LANGUAGEPOSITION) {
            case 0:
                return Locale.TRADITIONAL_CHINESE;
            case 1:
                return Locale.ENGLISH;//英语
            case 2:
                return Locale.FRANCE;//法语
            case 3:
                return Locale.GERMANY;//德语
            case 4:
                return new Locale("ES");//西班牙语
            case 5:
                return new Locale("PT");//葡萄牙语
            case 6:
                return new Locale("RU");//俄语
            case 7://繁体中文
                return Locale.TRADITIONAL_CHINESE;
            case 8://简体中文
                return Locale.CHINA;
            case 9:
                return Locale.JAPAN;//日语
            case 10:
                return Locale.KOREA;//韩语
            default:
                return Locale.CHINA;//简体中文
        }
    }



}
