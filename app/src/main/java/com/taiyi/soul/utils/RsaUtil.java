package com.taiyi.soul.utils;


import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class RsaUtil {
    private static final String RSA_PUBLICE = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDACkTvwdSoNUF/7sHIRwsRBOSd" +
            "Hfly0/VTbRyZuIQtG9K1JNLpQR6wjlT6sp6ZXpS41ULUAW+F3v+E8ILFLHXUtEKZ" +
            "hKyAE/b+F0TGJgF8Z6lJ7odIzt4Pi9IUfFkCtLbjpolAcYz0qy7qnh35NkT5KNdX" +
            "OCRhTRZVbFjcFMgU2wIDAQAB";

 private static final String RSA_key = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAMAKRO/B1Kg1QX/uwchHCxEE5J0d+XLT9VNtHJm4hC0b0rUk0ulBHrCOVPqynplelLjVQtQBb4Xe/4TwgsUsddS0QpmErIAT9v4XRMYmAXxnqUnuh0jO3g+L0hR8WQK0tuOmiUBxjPSrLuqeHfk2RPko11c4JGFNFlVsWNwUyBTbAgMBAAECgYA+x9yriPFtoCNuoG5HfDo48SGP38m56WMtR+Fuu6yyVoM25vfwe3J+A7nyT0ycFRiHqGh1iJad7D4ahsopjZRjSP39e8SvQaEtGFHqcTgVjVba8BXBc9cnPmfdA1tYfP22uFfnfo4TEnX+gU+OW22xPFcxCO4PK4e3PMm7xXa5+QJBAOg/DapMdO+Tm5Y266PpQAcFdPSUWvG1HIaTEoCLfAQhDMa3oZ6COmqMCR49azXl17IDXw72VKt8rL7tqMzcIQUCQQDTrn11MSH/BQR/gzph8k01Rf7txuRVbYuobXundwUK9mlvk98mhTe7G/T+/4qh0j+DwFheuH9LpJg98zeXkMRfAkEAsE7tuNLCEv4jpy2aRZzVFn92AhZ90/CB70fHS/6X7yqH4HOXJhYG8svMQECwGwKZdHJKv7kRHf5PxcH1209hYQJBAJVlJ7wHAhRGyUbpKdIovN7lWKcd4NZ96rvjEQs+z7tN71nFBg/co1mVwJIbbg8JILRukq+/DHApaR0Hw2tfy4cCQQDK+iaYuujHSz7oA6eMI9sSHYB8m0fiMa3r0ELitVRB6JMTHW0SW4HWud/Vuydvq7x1mBVBd8jKek0RjQnZ5ohA";
    /** */
    /**
     * 加密算法RSA
     */
    public static final String KEY_ALGORITHM = "RSA";// RSA/NONE/NoPadding,RSA/NONE/PKCS1Padding

    /**
     * String to hold name of the encryption padding.
     */
    public static final String PADDING = "RSA/NONE/PKCS1Padding";// RSA/NONE/NoPadding

    /**
     * String to hold name of the security provider.
     */
    public static final String PROVIDER = "BC";

    /** */
    /**
     * 签名算法
     */
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

    /** */
    /**
     * 获取公钥的key
     */
    private static final String PUBLIC_KEY = "RSAPublicKey";

    /** */
    /**
     * 获取私钥的key
     */
    private static final String PRIVATE_KEY = "RSAPrivateKey";

    /** */
    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;

    /** */
    /**
     * RSA最大解密密文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;

    /*
     * 公钥加密
     */
    public static String encryptByPublicKey(String str) throws Exception {

        Cipher cipher = Cipher.getInstance(PADDING, PROVIDER);
        // 获得公钥
        Key publicKey = getPublicKey();

        // 用公钥加密
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        // 读数据源
        byte[] data = str.getBytes("UTF-8");

        int inputLen = data.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段加密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(data, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_ENCRYPT_BLOCK;
        }
        byte[] encryptedData = out.toByteArray();
        out.close();

        return Base64Util.encode(encryptedData);
    }

    /*
     * 公钥解密
     */
    public static String decryptByPublicKey(String str) throws Exception {
        Cipher cipher = Cipher.getInstance(PADDING, PROVIDER);

        // 获得公钥
        Key publicKey = getPublicKey();

        // 用公钥解密
        cipher.init(Cipher.DECRYPT_MODE, publicKey);

        // 读数据源
        byte[] encryptedData = Base64Util.decode(str);

        int inputLen = encryptedData.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher
                        .doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher
                        .doFinal(encryptedData, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();

        return new String(decryptedData, "UTF-8");
    }

    /**
     * 读取公钥
     *
     * @return
     * @throws Exception
     * @author kokJuis
     * @date 2016-4-6 下午4:38:22
     * @comment
     */
    private static Key getPublicKey() throws Exception {

        String key =RSA_PUBLICE;

        byte[] keyBytes;
        keyBytes = Base64Util.decode(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return publicKey;
    }

    /*
     * 私钥解密
     */
    public static String decryptByRAKey(String str) throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        Cipher cipher = Cipher.getInstance(PADDING, PROVIDER);
        // 得到Key
        Key privateKey = getRAKey();
        // 用私钥去解密
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        // 读数据源
        byte[] encryptedData = Base64Util.decode(str);

        int inputLen = encryptedData.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher
                        .doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher
                        .doFinal(encryptedData, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();

        // 二进制数据要变成字符串需解码
        return new String(decryptedData, "UTF-8");
    }

    private static Key getRAKey() throws Exception {


        String key = RSA_key;

        byte[] keyBytes;
        keyBytes = Base64Util.decode(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }
}