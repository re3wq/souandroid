package com.taiyi.soul.utils;

import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.bean.AccountInfoBean;

import java.util.ArrayList;

/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/7/9
 * Time: 3:25 PM
 */
public class AccountPrivateUtils {

    /**
     * 查询账户私钥是否存在
     * @return
     */
    public static boolean isHavePrivateKey(){

        String mMainAccount = Utils.getSpUtils().getString("mainAccount");
        ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
        if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
            accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
        }

        for (AccountInfoBean accountInfoBean : accountInfoBeanArrayList) {
            if (accountInfoBean.getAccount_name().equals(mMainAccount)) {
               return true;
            }
        }


        return false;
    }
}
