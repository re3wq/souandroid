package com.taiyi.soul.utils;

import java.math.BigDecimal;

public class BytesUtlis {

    public static String getPrintSize(BigDecimal size) {

        //如果字节数少于1024，则直接以B为单位，否则先除于1024，后3位因太少无意义
        if (!BigDecimalUtil.greaterThan(size,new BigDecimal(1024))){
            return size.toPlainString() + " Bytes";
        }else {
            size = BigDecimalUtil.divide(size,new BigDecimal(1024),2);
        }


        //如果原字节数除于1024之后，少于1024，则可以直接以KB作为单位
        //因为还没有到达要使用另一个单位的时候
        //接下去以此类推
        if (!BigDecimalUtil.greaterThan(size,new BigDecimal(1024))){
            return size.toPlainString() + " KB";
        } else {
            size = BigDecimalUtil.divide(size,new BigDecimal(1024),2);
        }


        if (!BigDecimalUtil.greaterThan(size,new BigDecimal(1024))){

            return size.toPlainString() + " MB";
        } else {
            size = BigDecimalUtil.divide(size,new BigDecimal(1024),2);
        }

        if (!BigDecimalUtil.greaterThan(size,new BigDecimal(1024))){
            return size.toPlainString() + " GB";
        } else {
            size = BigDecimalUtil.divide(size,new BigDecimal(1024),2);
            return size.toPlainString() + " TB";
        }

    }

}
