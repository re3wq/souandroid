package com.taiyi.soul.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class NetPingManager {


    public static long ping(String domian) {
        long delay = 0;
        Process p;
        try {
            p = Runtime.getRuntime().exec("ping -c 1 -w 1 " + domian);
            BufferedReader buf = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String str;

            while ((str = buf.readLine()) != null) {
                Log.i("wang", str);
                if (str.contains("avg")) {
                    int i = str.indexOf("/", 20);
                    int j = str.indexOf(".", i);
                    delay = Long.parseLong(str.substring(i + 1, j));
                    //sleep = sleep+"ms";
                }
            }

        }catch (IOException e){
            delay = 700;
        }

        return delay;
    }

}