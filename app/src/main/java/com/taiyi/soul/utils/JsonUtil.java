package com.taiyi.soul.utils;

import com.google.gson.GsonBuilder;
import com.taiyi.soul.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by NGK on 2020/4/23.
 */
public class JsonUtil {
    /**
     * The constant jsonUtils.
     */
    public static JsonUtil jsonUtils;
    private static Gson gson;

    /**
     * 增加后台返回""和"null"的处理
     * 1.int=>0
     * 2.double=>0.00
     * 3.long=>0L
     *
     * @return
     */
    public static Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .serializeNulls()
//                    .registerTypeAdapter(Integer.class, new IntegerDefault0Adapter())
//                    .registerTypeAdapter(int.class, new IntegerDefault0Adapter())
//                    .registerTypeAdapter(Double.class, new DoubleDefault0Adapter())
//                    .registerTypeAdapter(double.class, new DoubleDefault0Adapter())
//                    .registerTypeAdapter(Long.class, new LongDefault0Adapter())
//                    .registerTypeAdapter(long.class, new LongDefault0Adapter())
                    .create();
        }
        return gson;
    }
    /**
     * Instantiates a new Json util.
     */
    public JsonUtil() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static JsonUtil getInstance() {
        if (jsonUtils == null) {
            synchronized (JsonUtil.class) {
                if (jsonUtils == null) {
                    jsonUtils = new JsonUtil();
                }
            }
        }
        return jsonUtils;
    }

    /**
     * Parse string to bean object.
     *
     * @param json   the str
     * @param clazz the clazz
     * @return the object
     */
    public static <T> T parseStringToBean(String json, Class<T> clazz) {
        try {
            T object = null;
            try {
                Gson gson = getGson();
                object = gson.fromJson(json, clazz);
            } catch (JsonSyntaxException e) {
                ToastUtils.showShortToast(R.string.error_parse);
            }
            return object;
        } catch (Exception e) {
//            Log.e("JSON",json);
            e.printStackTrace();
        }
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Parse json to array list array list.
     *
     * @param <T>   the type parameter
     * @param json  the json
     * @param clazz the clazz
     * @return the array list
     */
    public static <T> ArrayList<T> parseJsonToArrayList(String json, Class<T> clazz) {
        try {
            Type type = new TypeToken<ArrayList<JsonObject>>() {
            }.getType();
            ArrayList<JsonObject> jsonObjects = getGson().fromJson(json, type);
            ArrayList<T> arrayList = new ArrayList<>();
            for (JsonObject jsonObject : jsonObjects) {
                arrayList.add(getGson().fromJson(jsonObject, clazz));
            }
            return arrayList;
        } catch (Exception e) {
//            Log.e("JSON",json);
            e.printStackTrace();
        }
        return new ArrayList<T>();
    }
}

