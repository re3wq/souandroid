package com.taiyi.soul.utils;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.Nullable;


import com.taiyi.soul.base.Constants;

import java.util.Map;
import java.util.Set;

import devliving.online.securedpreferencestore.SecuredPreferenceStore;


/**
 * Created by NGK on 2020/4/23.
 */

public final class SPUtils {
    //
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    SecuredPreferenceStore prefStore = SecuredPreferenceStore.getSharedInstance();

    SecuredPreferenceStore.Editor mEditor = prefStore.edit();

    private static String token = "";
    private static String launge = "";
    private static String coinSymbol = "";
    private static String mainaccoun = "";

    /**
     * SPUtils构造函数
     * <p>在Application中初始化</p>
     *
     * @param spName spName
     */
    public SPUtils(String spName) {
        sp = Utils.getContext().getSharedPreferences(spName, Context.MODE_PRIVATE);
        editor = sp.edit();
        editor.apply();
    }

    /**
     * SP中写入String类型value
     *
     * @param key   键
     * @param value 值
     */
    public void put(String key, @Nullable String value) {

        if (key.equals(Constants.TOKEN)) {
            token = value;
        } else if (key.equals("current_language")) {
            launge = value;
        } else if (key.equals("coin_symbol")) {
            coinSymbol = value;
        } else if (key.equals("mainAccount")) {
            mainaccoun = value;
        }

        mEditor.putString(key, value).apply();
//        editor.putString(key, value).apply();
    }

    /**
     * SP中读取String
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值{@code null}
     */
    public String getString(String key) {
//        return getString(key, null);
        if (key.equals("coin_symbol")) {
            if (!coinSymbol.equals("")) {
                return coinSymbol;
            } else {
                coinSymbol = prefStore.getString(key, "");
                return prefStore.getString(key, "");
            }
        }else if(key.equals("mainAccount")){
            if (!mainaccoun.equals("")) {
                return mainaccoun;
            } else {
                mainaccoun = prefStore.getString(key, "");
                return prefStore.getString(key, "");
            }
        }
        return prefStore.getString(key, "");
    }

    /**
     * SP中读取String
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public String getString(String key, String defaultValue) {
//        return sp.getString(key, defaultValue);

        if (key.equals(Constants.TOKEN)) {
            if (!token.equals("")) {
                return token;
            } else {
                token = prefStore.getString(key, "");
                return prefStore.getString(key, "");
            }
        }
        if (key.equals("current_language")) {
            if (!launge.equals("")) {
                return launge;
            } else {
                launge = prefStore.getString(key, "");
                return prefStore.getString(key, "");
            }
        }
        return prefStore.getString(key, defaultValue);
    }

    /**
     * SP中写入int类型value
     *
     * @param key   键
     * @param value 值
     */
    public void put(String key, int value) {

//        editor.putInt(key, value).apply();
        mEditor.putInt(key, value).apply();
    }

    /**
     * SP中读取int
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值-1
     */
    public int getInt(String key) {
//        return getInt(key, -1);
        return prefStore.getInt(key, -1);
    }

    /**
     * SP中读取int
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public int getInt(String key, int defaultValue) {
//        return sp.getInt(key, defaultValue);
        return prefStore.getInt(key, defaultValue);
    }

    /**
     * SP中写入long类型value
     *
     * @param key   键
     * @param value 值
     */
    public void put(String key, long value) {
//        editor.putLong(key, value).apply();
        mEditor.putLong(key, value).apply();
    }

    /**
     * SP中读取long
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值-1
     */
    public long getLong(String key) {
//        return getLong(key, -1L);
        return prefStore.getLong(key, -1L);
    }

    /**
     * SP中读取long
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public long getLong(String key, long defaultValue) {
//        return sp.getLong(key, defaultValue);
        return prefStore.getLong(key, defaultValue);
    }

    /**
     * SP中写入float类型value
     *
     * @param key   键
     * @param value 值
     */
    public void put(String key, float value) {
//        editor.putFloat(key, value).apply();
        mEditor.putFloat(key, value).apply();
    }

    /**
     * SP中读取float
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值-1
     */
    public float getFloat(String key) {
//        return getFloat(key, -1f);
        return prefStore.getFloat(key, -1f);
    }

    /**
     * SP中读取float
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public float getFloat(String key, float defaultValue) {
//        return sp.getFloat(key, defaultValue);
        return prefStore.getFloat(key, defaultValue);
    }

    /**
     * SP中写入boolean类型value
     *
     * @param key   键
     * @param value 值
     */
    public void put(String key, boolean value) {
//        editor.putBoolean(key, value).apply();
        mEditor.putBoolean(key, value).apply();
    }

    /**
     * SP中读取boolean
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值{@code false}
     */
    public boolean getBoolean(String key) {
//        return getBoolean(key, false);
        return prefStore.getBoolean(key, false);
    }

    /**
     * SP中读取boolean
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public boolean getBoolean(String key, boolean defaultValue) {
//        return sp.getBoolean(key, defaultValue);
        return prefStore.getBoolean(key, defaultValue);
    }

    /**
     * SP中写入String集合类型value
     *
     * @param key    键
     * @param values 值
     */
    public void put(String key, @Nullable Set<String> values) {
//        editor.putStringSet(key, values).apply();
        mEditor.putStringSet(key, values).apply();
    }

    /**
     * SP中读取StringSet
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值{@code null}
     */
    public Set<String> getStringSet(String key) {
//        return getStringSet(key, null);
        return prefStore.getStringSet(key, null);
    }

    /**
     * SP中读取StringSet
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public Set<String> getStringSet(String key, @Nullable Set<String> defaultValue) {
//        return sp.getStringSet(key, defaultValue);
        return prefStore.getStringSet(key, defaultValue);
    }

    /**
     * SP中获取所有键值对
     *
     * @return Map对象
     */
    public Map<String, ?> getAll() {
//        return sp.getAll();
        return prefStore.getAll();
    }

    /**
     * SP中移除该key
     *
     * @param key 键
     */
    public void remove(String key) {
//        editor.remove(key).apply();
        mEditor.remove(key).apply();
    }

    /**
     * SP中是否存在该key
     *
     * @param key 键
     * @return {@code true}: 存在<br>{@code false}: 不存在
     */
    public boolean contains(String key) {
//        return sp.contains(key);
        return prefStore.contains(key);
    }

    /**
     * SP中清除所有数据
     */
    public void clear() {
//        editor.clear().apply();
        mEditor.clear().apply();
    }

}
