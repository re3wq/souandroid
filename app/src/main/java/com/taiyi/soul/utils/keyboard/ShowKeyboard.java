package com.taiyi.soul.utils.keyboard;

import android.content.Context;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.taiyi.soul.R;

import java.util.List;

public class ShowKeyboard {

   static SafeKeyboard safeKeyboard;

    /**
     *
     * @param context content
     * @param keyboardPlace 软键盘布局
     * @param key_main 软键盘所在根布局
     * @param key_scroll edit所在父布局，有多个就传总布局
     * @param texts 需要弹出键盘的edit
     * @param isClose 是否需要关闭预览
     */
    public static SafeKeyboard initKeyBoard(Context context, LinearLayout keyboardPlace, LinearLayout key_main, LinearLayout key_scroll, List<EditText> texts,boolean isClose) {
        safeKeyboard = new SafeKeyboard(context, keyboardPlace,
                R.layout.layout_keyboard_containor, R.id.safeKeyboardLetter, key_main, key_scroll, true, false);

        for (int i = 0; i < texts.size(); i++) {
            safeKeyboard.putEditText(texts.get(i));
        }
        if(isClose==true){
            safeKeyboard.setForbidPreview(true);
        }else {
            safeKeyboard.setForbidPreview(false);
        }
        return safeKeyboard;
    }
    /**
     *  @param context content
     * @param keyboardPlace 软键盘布局
     * @param key_main 软键盘所在根布局
     * @param key_scroll edit所在父布局，有多个就传总布局
     * @param texts 需要弹出键盘的edit
     * @param isClose 是否需要关闭预览
     */
    public static SafeKeyboard initKeyBoard(Context context, LinearLayout keyboardPlace, RelativeLayout key_main, LinearLayout key_scroll, List<EditText> texts, boolean isClose) {


        safeKeyboard = new SafeKeyboard(context, keyboardPlace,
                R.layout.layout_keyboard_containor, R.id.safeKeyboardLetter, key_main, key_scroll, true, false);
        for (int i = 0; i < texts.size(); i++) {
            safeKeyboard.putEditText(texts.get(i));
        }
        if(isClose==true){
            safeKeyboard.setForbidPreview(true);
        }else {
            safeKeyboard.setForbidPreview(false);
        }
        return safeKeyboard;
    }

    public static void releaseKeyboard(){
        if(null!=safeKeyboard){
            safeKeyboard.release();
        }

    }
}
