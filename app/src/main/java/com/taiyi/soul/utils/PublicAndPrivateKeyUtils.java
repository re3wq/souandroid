package com.taiyi.soul.utils;
import android.text.TextUtils;

import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.bean.AccountInfoBean;
import com.taiyi.soul.bean.WalletBean;
import com.taiyi.soul.blockchain.cypto.ec.EosPrivateKey;
import com.taiyi.soul.gen.WalletBeanDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LinkToken on 2018/2/3.
 */

public class PublicAndPrivateKeyUtils {


    //获取钱包下所有账号activepublickey
    public static List<String> getActivePublicKey() {
        List<String> keyList = new ArrayList<>();

        WalletBean walletBean = MyApplication.getInstance().getWalletBean();

        if (walletBean == null || walletBean.getAccount_info() == null){
            return null;
        }

        List<AccountInfoBean> mAccountInfoBeanList = JsonUtil.parseJsonToArrayList(walletBean.getAccount_info(),
                AccountInfoBean.class);//遍历本地所有账号信息
        if (mAccountInfoBeanList==null||mAccountInfoBeanList.size()==0){
            return null;
        }
        for (int i = 0; i < mAccountInfoBeanList.size(); i++) {
            if (!TextUtils.isEmpty(mAccountInfoBeanList.get(i).getAccount_active_private_key())) {
                keyList.add(mAccountInfoBeanList.get(i).getAccount_active_public_key());
            }else {
                keyList.add(mAccountInfoBeanList.get(i).getAccount_owner_public_key());
            }
        }

        //去除空的公钥
        List<String> mList = new ArrayList<>();
        for (String s : keyList) {
            if (!TextUtils.isEmpty(s)) {
                mList.add(s);
            }
        }
        return mList;
    }

    //获取钱包下所有账号OwnerPublicKey
    public static List<String> getOwnerPublicKey() {
        List<String> keyList = new ArrayList<>();

        WalletBean walletBean = MyApplication.getInstance().getWalletBean();

        if (walletBean == null || walletBean.getAccount_info() == null){
            return null;
        }

        List<AccountInfoBean> mAccountInfoBeanList = JsonUtil.parseJsonToArrayList(walletBean.getAccount_info(),
                AccountInfoBean.class);//遍历本地所有账号信息

        if (mAccountInfoBeanList==null||mAccountInfoBeanList.size()==0){
            return null;
        }

        for (int i = 0; i < mAccountInfoBeanList.size(); i++) {
            String account_owner_public_key = mAccountInfoBeanList.get(i).getAccount_owner_public_key();
            String account_owner_private_key = mAccountInfoBeanList.get(i).getAccount_owner_private_key();
            if (!TextUtils.isEmpty(account_owner_public_key) && !TextUtils.isEmpty(account_owner_private_key)) {
                keyList.add(account_owner_public_key);
            }
        }

        //去除空的公钥
        List<String> mList = new ArrayList<>();
        for (String s : keyList) {
            if (!TextUtils.isEmpty(s)) {
                mList.add(s);
            }
        }
        return mList;
    }


    //查询是active 还是owner
    public static String getActivePublicKey(String account) {

        WalletBean walletBean = null;
        int loginType = Utils.getSpUtils().getInt("loginType");
        //通过手机号和钱包类型查询有无账户
        if (loginType==1) {
            List<WalletBean> listAll = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(account)).build().list();
            if (null != listAll) {
                if(listAll.size()>0)
                walletBean = listAll.get(listAll.size() - 1);
            }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_email.eq(account)).build().unique();
        } else {

            List<WalletBean> list = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(account)).build().list();
            if (null != list) {
                if(list.size()>0)
                walletBean = list.get(list.size() - 1);
            }
//            walletBean = MyApplication.getDaoSession().getWalletBeanDao().queryBuilder().where(WalletBeanDao.Properties.Wallet_phone.eq(account)).build().unique();

        }
        if (walletBean != null){
            ArrayList<AccountInfoBean> accountInfoBeanArrayList = new ArrayList<>();
            if (MyApplication.getInstance().getWalletBean().getAccount_info() != null) {
                accountInfoBeanArrayList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance().getWalletBean().getAccount_info(), AccountInfoBean.class);
                for (int i = 0; i < accountInfoBeanArrayList.size() ;i++){
                    if (accountInfoBeanArrayList.get(i).getAccount_name().equals(account)){
                        if (!TextUtils.isEmpty(accountInfoBeanArrayList.get(i).getAccount_active_private_key())) {
                            return "active";
                        }else {
                            return "owner";
                        }
                    }
                }
            }
        }

        return "active";
    }


    //通过公钥获取私钥
    public static String getPrivateKey(String publicKey, String password) {
        String activePrivateKey = null;
        List<AccountInfoBean> mAccountInfoBeanList = JsonUtil.parseJsonToArrayList(MyApplication.getInstance()
                .getWalletBean().getAccount_info(), AccountInfoBean.class);//遍历本地所有账号信息
        for (int i = 0; i < mAccountInfoBeanList.size(); i++) {
            AccountInfoBean bean = mAccountInfoBeanList.get(i);
            if (bean.getAccount_active_public_key().equals(publicKey) && !TextUtils.isEmpty(bean.getAccount_active_private_key())) {
                activePrivateKey = bean.getAccount_active_private_key();
            } else if (bean.getAccount_owner_public_key().equals(publicKey) && !TextUtils.isEmpty(bean.getAccount_owner_private_key())) {
                activePrivateKey = bean.getAccount_owner_private_key();
            }
        }
        if (activePrivateKey != null) {
            String key =null;
            try {
                key =  EncryptUtil.getDecryptString(activePrivateKey, password);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return key;
        } else {
            return null;
        }
    }

    public static EosPrivateKey[] getPrivateKey(int count) {
        EosPrivateKey[] retKeys = new EosPrivateKey[count];
        for (int i = 0; i < count; i++) {
            retKeys[i] = new EosPrivateKey();
        }

        return retKeys;
    }

}
