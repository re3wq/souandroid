package com.taiyi.soul.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.taiyi.soul.R;

/**
 * Created by NGK on 2020/4/23.
 * app信息
 */
public class AlertDialogShowUtil {

    private static AlertDialog ad;

    public static void toastMessage(Context context, String message) {
        if(null==context){
            return;
        }
        ad = new AlertDialog.Builder(context).setMessage(message).show();
        View inflate = LayoutInflater.from(context).inflate(R.layout.alert_dialog, null);
       TextView messageS = inflate.findViewById(R.id.message);
        messageS.setText(message);
        ad.getWindow().setContentView(inflate);
        Window window = ad.getWindow();
        View view = window.getDecorView();
        setViewFontSize(view, 14);
        ad.getWindow().setBackgroundDrawableResource(R.drawable.bg_shape);
        WindowManager.LayoutParams params = ad.getWindow().getAttributes();
        params.width = DensityUtil.dip2px(context, 220);
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;
        ad.setCanceledOnTouchOutside(false);
        ad.setCancelable(false);
        ad.getWindow().setAttributes(params);
        android.os.Handler hander = new android.os.Handler();
        hander.postDelayed(new Runnable() {
            @Override
            public void run() {
                    if (ad.isShowing()) {
                        ad.dismiss();
                    }

            }
        }, 1500);
    } public static void toastMessage2(Context context, String message) {
        ad = new AlertDialog.Builder(context).setMessage(message).show();
        View inflate = LayoutInflater.from(context).inflate(R.layout.alert_dialog, null);
       TextView messageS = inflate.findViewById(R.id.message);
        messageS.setText(message);
        ad.getWindow().setContentView(inflate);
        Window window = ad.getWindow();
        View view = window.getDecorView();
        setViewFontSize(view, 14);
        ad.getWindow().setBackgroundDrawableResource(R.drawable.bg_shape);
        WindowManager.LayoutParams params = ad.getWindow().getAttributes();
        params.width = DensityUtil.dip2px(context, 220);
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;

        ad.getWindow().setAttributes(params);
//        android.os.Handler hander = new android.os.Handler();
//        hander.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                    if (ad.isShowing()) {
//                        ad.dismiss();
//                    }
//
//            }
//        }, 5000);
    }

    public static void dismissMessage() {
        if(null!=ad){
            if (ad.isShowing()) {
                ad.dismiss();
            }
        }
    }

    private static void setViewFontSize(View view, int size) {
        if (view instanceof ViewGroup) {
            ViewGroup parent = (ViewGroup) view;
            int count = parent.getChildCount();
            for (int i = 0; i < count; i++) {
                setViewFontSize(parent.getChildAt(i), size);
            }
        } else if (view instanceof TextView) {
            TextView textview = (TextView) view;
            textview.setTextSize(size);
        }
    }

}
