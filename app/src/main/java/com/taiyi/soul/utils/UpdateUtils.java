package com.taiyi.soul.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;

import com.taiyi.soul.R;
import com.taiyi.soul.view.dialog.updatadialog.UpdataCallback;
import com.taiyi.soul.view.dialog.updatadialog.UpdataDialog;

import java.util.HashMap;

import static com.taiyi.soul.net.HttpUtils.language;

/**
 * Created by NGK on 2018/2/10.
 */

public class UpdateUtils {

    /**
     * 获取apk的版本号 currentVersionCode
     *
     * @param ctx
     * @return
     */
    public static int getAPPLocalVersion(Context ctx) {
        int currentVersionCode = 0;
        PackageManager manager = ctx.getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(ctx.getPackageName(), 0);
            String appVersionName = info.versionName; // 版本名
            currentVersionCode = info.versionCode; // 版本号
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return currentVersionCode;
    }



    /**
     * 获取服务器的版本号
     *
     * @param context  上下文
     * @param callBack 为了控制线程，需要获取服务器版本号成功的回掉
     */
    public static void getAPPServerVersion(final Context context, final VersionCallBack callBack) {
//        String lang = "";
//        if (Utils.getSpUtils().getString("current_language","").equals("")){
//            lang = language;
//        }else {
//            lang = Utils.getSpUtils().getString("current_language","");
//        }
//
//        HashMap<String,String> hashMap = new HashMap<String, String>();
//        hashMap.put("phoneVersion","android");
//        //获取服务器最新版本号
//        OkGo.<String>post(BaseUrl.HTTP_GET_CURRENT_VERSION)
//                .tag(context)
//                .headers("lang",lang)
//                .headers("Authorization", Utils.getSpUtils().getString(Constants.TOKEN, ""))
//                .params(hashMap)
//                .execute(new StringCallback() {
//                    @Override
//                    public void onSuccess(Response<String> response) {
//                        UpdateAppBean updateAppBean = (UpdateAppBean) JsonUtil.parseStringToBean(response.body(), UpdateAppBean.class);
//                        if (updateAppBean.getCode() == 0) {
//                            callBack.callBack(Integer.parseInt(updateAppBean.getData().getVersionUp()), updateAppBean.getData().getDownUrl(),
//                                    updateAppBean.getData().getIsForce(), updateAppBean.getData().getIntroduce());//数字110代表1.1.0
//                        }else {
//                            getSystemWitch(context);
//                        }
//                    }
//
//                    @Override
//                    public void onError(Response<String> response) {
////                        super.onError(response);
//                        getSystemWitch(context);
//                    }
//                });
    }


    /**
     * 获取服务器的系统权限
     *
     * @param context  上下文
     *
     */
    public static void getSystemWitch(final Context context) {
        String lang = "";
        if (Utils.getSpUtils().getString("current_language","").equals("")){
            lang = language;
        }else {
            lang = Utils.getSpUtils().getString("current_language","");
        }

        HashMap<String,String> hashMap = new HashMap<String, String>();
//
//        String date_old = Utils.getSpUtils().getString("time_before","");
//        String date_new = DateUtils.getTimeBefore();
//        String date ;
//        if (date_old.equals("")){
//            date = date_new;
//        }else {
//            date = DateUtils.timeCompare(date_old,date_new);
//        }
//
//        hashMap.put("datatime",date);
//        hashMap.put("serviceName","BKO");
//        OkGo.<BaseResultBean<NoticeInfoBean>>post(BaseUrl.GET_NOTICE_INFO)
//                .tag(context)
//                .headers("lang",lang)
//                .params(hashMap)
//                .execute(new JsonCallback<BaseResultBean<NoticeInfoBean>>() {
//                    @Override
//                    public void onSuccess(Response<BaseResultBean<NoticeInfoBean>> response) {
//                        if ( response.body()!=null && response.body().code.equals("0")) {
//
//                            if (response.body().data.getSystemnotice().equals("1")){
//                                NoticeInfoDialog noticeInfoDialog = new NoticeInfoDialog(context, new NoticeInfoCallback() {
//                                    @Override
//                                    public void callback() {
//                                        if (response.body().data.getSystemnotice().equals("1")){//0：关闭系统 1：系统正常使用
//                                            getSystemWitch(context);
//                                        }
//                                    }
//
//                                    @Override
//                                    public void cancel() {
//
//                                    }
//
//
//                                });
//                                noticeInfoDialog.setCancelable(false);
//                                noticeInfoDialog.setToast(response.body().data.getNoticetitle(), response.body().data.getNoticetext());
//                                noticeInfoDialog.show();
//                            }
////                            Utils.getSpUtils().put("time_before",DateUtils.getTime(new Date()));
//
//                        }
//
//                    }
//                });

    }



    /**
     * 更新APP
     *
     * @param context
     */
    public static void updateApp(final Context context, final int status) {
        getAPPServerVersion(context, new VersionCallBack() {
            @Override
            public void callBack(final int version, final String apkurl, final String isForce, final String introduce) {
                if (version > 0) {
                    if (version > getAPPLocalVersion(context)) {

                        UpdataDialog dialog = new UpdataDialog(context, new UpdataCallback() {
                            @Override
                            public void goData() {
//                                Intent intent = new Intent(context, DownLoadServerice.class);
//                                intent.putExtra("url", apkurl);
//                                intent.putExtra("versionName", versionName);
//                                context.startService(intent);

                                Intent intent = new Intent(Intent.ACTION_VIEW);    //为Intent设置Action属性
                                intent.setData(Uri.parse(apkurl.contains("http://") ? apkurl : ("http://"+apkurl))); //为Intent设置DATA属性
                                context.startActivity(intent);

                            }

                            @Override
                            public void onCancel() {
                                if (isForce.equalsIgnoreCase("1")){
                                   //  AppManager.getAppManager().finishAllActivity();
                                }
                            }
                        });
                        dialog.setContent(introduce);
                        dialog.setCancelable(false);
                        dialog.show();
                    } else {
                        if (status == 1) {
                            Toast.makeText(context, context.getString(R.string.version_new), Toast.LENGTH_SHORT).show();
                        }else {
                            getSystemWitch(context);
                        }
                    }
                }
            }
        });


    }


    public interface VersionCallBack {
        void callBack(int versionCode, String apkurl, String versionName, String versionDetail);
    }
}
