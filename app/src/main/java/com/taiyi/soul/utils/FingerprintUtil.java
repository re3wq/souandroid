package com.taiyi.soul.utils;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.Build;

import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import com.taiyi.soul.R;

public class FingerprintUtil {

    public static boolean isSupport(Context context) {

        if (Build.VERSION.SDK_INT < 23) {
            ToastUtils.showShortToast(context.getString(R.string.not_support_finger));
            return false;
        } else {
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            FingerprintManagerCompat fingerprintManagerCompat = FingerprintManagerCompat.from(context);
            if (!fingerprintManagerCompat.isHardwareDetected()) {//是否支持指纹解锁
                ToastUtils.showShortToast(context.getString(R.string.not_support_finger));
                return false;
            } else if (!keyguardManager.isKeyguardSecure()) {//设备是否设置锁屏
                ToastUtils.showShortToast(context.getString(R.string.add_fingerprint));
                return false;
            } else if (!fingerprintManagerCompat.hasEnrolledFingerprints()) {//设备是否保存过指纹
                ToastUtils.showShortToast(context.getString(R.string.least_one_fingerprint));
                return false;
            }
        }
        return true;
    }

    public static void startVerification(Context context) {

    }
}
