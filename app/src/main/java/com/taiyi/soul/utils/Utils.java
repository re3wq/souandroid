package com.taiyi.soul.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Build;


import com.taiyi.soul.R;
import com.taiyi.soul.service.KillSelfService;

import java.util.List;
import java.util.UUID;

import me.ljp.permission.HiPermission;
import me.ljp.permission.PermissionCallback;
import me.ljp.permission.PermissionItem;

/**
 * Created by NGK on 2020/4/23.
 * Utils初始化相关
 */
public final class Utils {

    private static Context context;
    private static SPUtils spUtils;


    private Utils() {
        throw new UnsupportedOperationException("...");
    }

    /**
     * 初始化工具类
     *
     * @param context 上下文
     */
    public static void init(Context context) {
        Utils.context = context.getApplicationContext();
        spUtils = new SPUtils("USER");
    }

    /**
     * 获取ApplicationContext
     *
     * @return ApplicationContext context
     */
    public static Context getContext() {
        if (context != null) {
            return context;
        }
        throw new NullPointerException("应该首先初始化");
    }

    /**
     * Gets sp utils.
     *
     * @return the sp utils
     */
    public static SPUtils getSpUtils() {
        return spUtils;
    }

    public static boolean getPermissions(List<PermissionItem> permissonItems, String msg) {
        final Boolean[] isGetPermissions = {false};
        HiPermission.create(context)
                .permissions(permissonItems)
                .title(context.getResources().getString(R.string.dear_user))
                .msg(msg)
                .animStyle(R.style.PermissionAnimScale)
                .style(R.style.PermissionThemeStyle)
                .checkMutiPermission(new PermissionCallback() {
                    @Override
                    public void onClose() {
                        isGetPermissions[0] = false;
                        ToastUtils.showShortToast(context.getResources().getString(R.string.close_permisson_toast));
                    }

                    @Override
                    public void onFinish() {
                        isGetPermissions[0] = true;
                    }

                    @Override
                    public void onDeny(String permisson, int position) {
                    }

                    @Override
                    public void onGuarantee(String permisson, int position) {
                    }
                });
        return isGetPermissions[0];
    }

    /**
     * 重启整个APP
     * @param Delayed 延迟多少毫秒
     */
    public static void restartAPP(long Delayed){

        /**开启一个新的服务，用来重启本APP*/
        Intent intent1=new Intent(context,KillSelfService.class);
        intent1.putExtra("PackageName",context.getPackageName());
        intent1.putExtra("Delayed",Delayed);
        context.startService(intent1);

        /**杀死整个进程**/
        android.os.Process.killProcess(android.os.Process.myPid());
    }


    //获得独一无二的Psuedo ID
    public static String getUniquePsuedoID() {
        String serial = null;

        String m_szDevIDShort = "35" +
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +

                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +

                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +

                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +

                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +

                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +

                Build.USER.length() % 10; //13 位

        try {
            serial = Build.class.getField("SERIAL").get(null).toString();
            //API>=9 使用serial号
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            //serial需要一个初始化
            serial = "serial"; // 随便一个初始化
        }
        //使用硬件信息拼凑出来的15位号码
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }


}
