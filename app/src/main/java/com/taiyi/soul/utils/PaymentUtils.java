package com.taiyi.soul.utils;


import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.widget.Toast;

import com.taiyi.soul.R;


public class PaymentUtils {

    private Activity context;

    public PaymentUtils(Activity context){
        this.context = context;
    }


//    public void payment(PaymentCallBack paymentCallBack){
//        if (Utils.getSpUtils().getInt(Constants.IS_FINGEREPRINT + Utils.getSpUtils().getString(Constants.PHONE,""),0) == 1){
//
//            FingerprintDialog fingerprintDialog = new FingerprintDialog(context,welcomeActivity.mCipher, new FingerprintDialog.FingerPrintCallBack() {
//                @Override
//                public void onAuthenticated(String password) {
//                    paymentCallBack.fingerprintPayment(password);
//                }
//
//                @Override
//                public void inputPassword() {
//                    PasswordDialog mPasswordDialog = new PasswordDialog(context, "",new PasswordCallback() {
//                        @Override
//                        public void sure(final String password) {
//
////                            if (MyApplication.getInstance().getWalletBean().getWallet_shapwd().equals(PasswordToKeyUtils.shaCheck(password))) {
//
//                                paymentCallBack.fingerprintPayment(password);
////                            } else {
////                                Toast.makeText(context, context.getString(R.string.password_error), Toast.LENGTH_SHORT).show();
////                            }
//                        }
//
//                        @Override
//                        public void cancle() {
//                            paymentCallBack.fail();
//                        }
//                    });
//                    mPasswordDialog.setCancelable(false);
//                    mPasswordDialog.show();
//                }
//
//                @Override
//                public void cancel() {
//                    paymentCallBack.fail();
//                }
//            });
//            fingerprintDialog.setCancelable(false);
//            fingerprintDialog.show();
//        }else {
//            PasswordDialog mPasswordDialog = new PasswordDialog(context,"", new PasswordCallback() {
//                @Override
//                public void sure(final String password) {
//
////                    if (MyApplication.getInstance().getWalletBean().getWallet_shapwd().equals(PasswordToKeyUtils.shaCheck(password))) {
//
//                        paymentCallBack.fingerprintPayment(password);
////                    } else {
////                        Toast.makeText(context, context.getString(R.string.password_error), Toast.LENGTH_SHORT).show();
////                    }
//                }
//
//                @Override
//                public void cancle() {
//                    paymentCallBack.fail();
//                }
//            });
//            mPasswordDialog.setCancelable(false);
//            mPasswordDialog.show();
//        }
//    }
//
//
//    public void loginCheck(LoginCheckCallBack loginCheckCallBack){
//        if (Utils.getSpUtils().getInt(Constants.IS_DEVICE_LOCK + Utils.getSpUtils().getString(Constants.PHONE,""),0) == 1){
//
//            FingerprintDialog fingerprintDialog = new FingerprintDialog(context,welcomeActivity.mCipher, new FingerprintDialog.FingerPrintCallBack() {
//                @Override
//                public void onAuthenticated(String password) {
//                    loginCheckCallBack.fingerprintLogin();
//                }
//
//                @Override
//                public void inputPassword() {
//                    loginCheckCallBack.inputPassword();
//                }
//
//                @Override
//                public void cancel() {
//                    loginCheckCallBack.fail();
//                }
//            });
//            fingerprintDialog.setCancelable(false);
//            fingerprintDialog.show();
//        }
//    }
//
//
//    public interface LoginCheckCallBack{
//        void fingerprintLogin();
//        void inputPassword();
//        void fail();
//    }
//
//    public interface PaymentCallBack{
//        void fingerprintPayment(String password);
//        void fail();
//    }


    public static boolean supportFingerprint(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            Toast.makeText(context, context.getString(R.string.system_version_low), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            KeyguardManager keyguardManager = context.getSystemService(KeyguardManager.class);
            FingerprintManager fingerprintManager = context.getSystemService(FingerprintManager.class);
            if (fingerprintManager == null){
                Toast.makeText(context, context.getString(R.string.not_support_fingereprint), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!fingerprintManager.isHardwareDetected()) {
                Toast.makeText(context, context.getString(R.string.not_support_fingereprint), Toast.LENGTH_SHORT).show();
                return false;
            } else if (!keyguardManager.isKeyguardSecure()) {
                Toast.makeText(context, context.getString(R.string.not_set_fingerprint), Toast.LENGTH_SHORT).show();
                return false;
            } else if (!fingerprintManager.hasEnrolledFingerprints()) {
                Toast.makeText(context, context.getString(R.string.not_set_fingerprint), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    public static boolean supportFingerprint1(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
//            Toast.makeText(context, context.getString(R.string.system_version_low), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            KeyguardManager keyguardManager = context.getSystemService(KeyguardManager.class);
            FingerprintManager fingerprintManager = context.getSystemService(FingerprintManager.class);
            if (fingerprintManager == null){
                return false;
            }
            if (!fingerprintManager.isHardwareDetected()) {
//                Toast.makeText(context, context.getString(R.string.not_support_fingereprint), Toast.LENGTH_SHORT).show();
                return false;
            } else if (!keyguardManager.isKeyguardSecure()) {
//                Toast.makeText(context, context.getString(R.string.not_set_fingerprint), Toast.LENGTH_SHORT).show();
                return false;
            } else if (!fingerprintManager.hasEnrolledFingerprints()) {
//                Toast.makeText(context, context.getString(R.string.not_set_fingerprint), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }


    /**
     * 是否支持指纹识别
     * @param context
     * @return
     */
    public static boolean isSupportFingerprint(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
//            Toast.makeText(context, context.getString(R.string.system_version_low), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            KeyguardManager keyguardManager = context.getSystemService(KeyguardManager.class);
            FingerprintManager fingerprintManager = context.getSystemService(FingerprintManager.class);
            if (fingerprintManager == null){
                return false;
            }
            if (!fingerprintManager.isHardwareDetected()) {
//                Toast.makeText(context, context.getString(R.string.not_support_fingereprint), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }
}
