package com.taiyi.soul.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Environment;
import android.view.View;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by NGK on 2018/4/18.
 */
public class ViewToImageUtils {
    private static final String IMG_PATH = Environment.getExternalStorageDirectory() + "/sou/share/";

    public static final String IMG_NAME = "sou.png";

    /**
     * Load bitmap from view bitmap.
     *
     * @param v the v
     * @return the bitmap
     */
    public static Bitmap loadBitmapFromView(View v) {
        int w = v.getWidth();
        int h = v.getHeight();
        Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp);

        c.drawColor(Color.WHITE);
        /** 如果不设置canvas画布为白色，则生成透明 */

        v.layout(0, 0, w, h);
        v.draw(c);

        return bmp;
    }

    //保存方法
    public static File saveFile(Bitmap bm) throws IOException {
        File foder = new File(IMG_PATH);
        if (!foder.exists()) {
            foder.mkdirs();
        }

        File myCaptureFile = new File(IMG_PATH, System.currentTimeMillis()+"_"+IMG_NAME);
        String ends = myCaptureFile.getPath();

        if (!myCaptureFile.exists()) {
            myCaptureFile.createNewFile();
        }
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        bos.flush();
        bos.close();

        return myCaptureFile;
    }
}
