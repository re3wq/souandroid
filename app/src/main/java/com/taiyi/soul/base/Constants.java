package com.taiyi.soul.base;

import com.taiyi.soul.bean.TokenListBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */

public class Constants {
    //FaceEngine 常量
    public static final String APP_ID = "9cpd55Wh5YC4nKj2qGmQGhPCsdd2zBcdiuWHVerRcVwB";
    public static final String SDK_KEY = "2ssisvSY2QbWQrDuEgpJQ2FEbcDQy37U6eDS3M11RgYe";

    public static String EOSCONTRACT = "eosio.token";
    public static String DELEGATEBWCONTRACT = "sou"; //抵押、赎回合约
    public static String ACTIONTRANSFER = "transfer";
    public static String PERMISSONION = "active";
    public static String ACCOUNT_SUFFIX = ".sou";
    public static String NGK_CONTRACT_ADDRESS = "sou.token";
    public static String USDN_CONTRACT_ADDRESS = "usds.token";
    public static String HASH_CONTRACT_ADDRESS = "hashmain.sou";
    public static List<TokenListBean> tokenListBeans = new ArrayList<>();
    public static final String AREA_CODE = "areaCode";
    public static final String CURRENT_ACCOUNT_NAME = "mainAccount";
    public static final String IS_OPEN_GOOGLE = "isOpenGoogle";

    public static String ACTIONDELEGATEBW = "delegatebw"; //抵押
    public static String ACTION_UNDELEGATEBW = "undelegatebw"; //赎回
    public static String ACTION_UPDATEAUTH = "updateauth"; //更新权限

    public static String ACTION_BUYRAM = "buyram"; //购买内存
    public static String ACTION_SELLRAM = "sellram"; //出售内存

    public static final String NGK_TO_USDT_RATE = "sou_to_usdt";
    public static final String USDN_TO_USDT_RATE = "usdn_to_usdt";
    public static final String SELECT_COIN="select_coin";


    public static String IS_FINGEREPRINT = "is_open_fingerprint";
    public static String PASSWORD = "password";
    public static String FACE="isRegisterFace";

    /**
     * 手机号
     */
    public static String PHONE = "phone";

    /**
     * token
     */
    public static String TOKEN = "token";
    public static int isL = 0;


    public static int LANGUAGEPOSITION =20;
    public static final String I18N = "i18n";
    public static final String LOCALE_LANGUAGE = "locale_language";
    public static int Proportion = 0;

}
