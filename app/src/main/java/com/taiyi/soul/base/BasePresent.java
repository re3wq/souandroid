package com.taiyi.soul.base;

/**
 * Created by NGK on 2020/4/23.
 */
public abstract class BasePresent<T> {
    public T view;

    public void attach(T view){
        this.view = view;
    }

    public void detach(){
        this.view = null;
    }
}