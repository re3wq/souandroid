package com.taiyi.soul.base;


/**
 * Created by NGK on 2020/4/23.
 */

public class BaseUrl {



//  public final static String BASE_URL = "https://api.bkos.cc/1.0/product";
//    public final static String BASE_URL_shop = "https://api.bkos.cc/1.0/shop";
//    public final static String BASE_URL_ = "https://api.bkos.cc";
    public final static String BASE_URL_ = "https://api.soulswap.io";
//    public final static String BASE_URL_ = "http://18.167.53.241:7002";
//    public final static String BASE_URL_ = "http://175.41.217.158:7002";
    //
    //系统维护
    public final static String FINDMENDINFO = BASE_URL_ + "/1.0/user/mend/findMendInfo";
    //查询余额
    public final static String GET_BALANNCE = "chain/get_currency_balance";

    //通过公钥查询账户
    public final static String GET_ACCOUNT_BY_PUBLIC = "history/get_key_accounts";

    // 获取区块链账号信息
    public final static String HTTP_GET_CHIAN_ACCOUNT_INFO = "chain/get_account";

    // 获取链上信息
    public final static String HTTP_EOS_GET_TABLE = "chain/get_table_rows";

    // 获取区块链状态
    public final static String HTTP_GET_CHAIN_INFO = "chain/get_info";
    // 交易JSON序列化
    public final static String HTTP_GET_ABI_JSON_TO_BIN = "chain/abi_json_to_bin";
    // 获取keys
    public final static String HTTP_GET_REQUIRED_KEYS = "chain/get_required_keys";
    // 发起交易
    public final static String HTTP_PUSH_TRANSACTION = "chain/push_transaction";

    /*********************************************三方*********************************************************/
    //节点列表
    public static final String GETNODELIST = BASE_URL_ + "/1.0/busin/treeCall/GetNodeListForPlayer";
    //币种列表
    public static final String GETTOKENLIST = BASE_URL_ + "/1.0/busin/treeCall/GetTokenList";
    /*********************************************集市交易*********************************************************/
    //交易列表
    public static final String DEALLIST = BASE_URL_ + "/1.0/product/mifaEntryOrders/findEntryOrderList";
    //挂单参数
    public static final String DEAFINDORDER = BASE_URL_ + "/1.0/product/mifaEntryOrders/findOrderSys";
    //挂单交易总价以及手续费计算
    public static final String DEAFINDORDERPRICE = BASE_URL_ + "/1.0/product/mifaEntryOrders/calculation";
    //挂单校验密码
    public static final String CHECKPAYPASS = BASE_URL_ + "/1.0/product/mifaEntryOrders/checkPayPass";
    //挂单提交
    public static final String SAVEENTRYORDER = BASE_URL_ + "/1.0/product/mifaEntryOrders/saveEntryOrder";
    //吃单数量查询
    public static final String CALCULATION = BASE_URL_ + "/1.0/product/UserMifaOrder/calculation";
    //吃单
    public static final String CLICKBUY = BASE_URL_ + "/1.0/product/UserMifaOrder/clickBuy";
    //订单详情
    public static final String ORDERINFO = BASE_URL_ + "/1.0/product/UserMifaOrder/getOrderInfo";
    //我的委托列表
    public static final String ORDERLIST = BASE_URL_ + "/1.0/product/UserMifaOrder/findUserOrderList";
    //撤单
    public static final String REVOKEORDER = BASE_URL_ + "/1.0/product/mifaEntryOrders/revokeEntryOrder";
    /*********************************************算力市场*********************************************************/
    //获取用户可用算力
    public static final String USERHASHBALANCE = BASE_URL_ + "/1.0/product/mifaEntryOrders/findUserHashBalance";
    //是否可发布委托
    public static final String ISFAORDER = BASE_URL_ + "/1.0/product/airdrop/iFaOrder";
    public static final String IFFUPDOWNOPEN = BASE_URL_ + "/1.0/product/airdrop/iFupdownOpen";
    //"http://114.115.131.170:7002/1.0/product/airdrop/iFupdownOpen"
    /*********************************************资产*********************************************************/

    //获取资产分布页面信息
    public static final String PRECENT = BASE_URL_ + "/1.0/user/property/percent";
    /*********************************************债券*********************************************************/
    //获取债券页面信息
    public static final String BONDSYS = BASE_URL_ + "/1.0/product/Bond/findBondSys";
    //计算债券花费
    public static final String BONDSYSCALCULATION = BASE_URL_ + "/1.0/product/Bond/calculation";
    //推送交易
    public static final String BONDPUSH = BASE_URL_ + "/1.0/product/Bond/pushTransaction";
    //债券
    public static final String BONDSEND = BASE_URL_ + "/1.0/product/Bond/saveBondOrder";
    //债券记录
    public static final String BONDRECORD = BASE_URL_ + "/1.0/product/Bond/findBondOrderList";

    /*********************************************兑换*********************************************************/

    //获取兑换币种列表
    public static final String EXCHANGELIST = BASE_URL_ + "/1.0/product/ExchangeAndBond/exchangeParameterList";

    //根据币种获取汇率
    public static final String EXCHANGEORDER = BASE_URL_ + "/1.0/product/ExchangeAndBond/calculationExchangeOrder";

    //兑换
    public static final String EXCHANG = BASE_URL_ + "/1.0/product/ExchangeAndBond/saveExchangeOrder";

    //兑换记录
    public static final String EXCHANGRERECORD = BASE_URL_ + "/1.0/product/ExchangeAndBond/findExchangeOrderdList";
    /*********************************************期权*********************************************************/

    //期权
    public static final String MAKETALL = BASE_URL_ + "/1.0/busin/maket/maketAll";

    //查询二元期权记录-加分页
    public static final String DOWNLIST = BASE_URL_ + "/1.0/product/UpAndDown/findUpDownOrderList";

    //获取购买信息(买涨买跌弹窗数据)
    public static final String FINDUPDOWNSYS = BASE_URL_ + "/1.0/product/UpAndDown/findUpDownSys";

    //点击购买调用
    public static final String SAVEBONDORDER = BASE_URL_ + "/1.0/product/UpAndDown/saveBondOrder";

    //获取KLain
    public static final String GETKLINE = BASE_URL_ + "/1.0/busin/maket/getKLine";

    public static final String GETKDATA = BASE_URL_ + "/1.0/busin/maket/maket";
    /*********************************************商城*********************************************************/
    //商城首页
    public static final String MALL_FIRST = BASE_URL_ + "/1.0/shop/index/showindex";
    //商品详情
    public static final String MALL_GOODS_DETAILS = BASE_URL_ + "/1.0/shop/commodity/commoditydetail";
    //商品更多列表
    public static final String MALL_MORE_LIST = BASE_URL_ + "/1.0/shop/commodity/findcommoditylist";
    //加入购物车
    public static final String GOODS_ADDCAR = BASE_URL_ + "/1.0/shop/cart/cartadd";
    //根据规格 获取价格
    public static final String COMMODITYPRICE = BASE_URL_ + "/1.0/shop/commodity/commodityspecprice";
    //购物车
    public static final String GOODS_CARLIST = BASE_URL_ + "/1.0/shop/cart/cartList";
    //购物车删除（单个或多个）
    public static final String CARTDEL = BASE_URL_ + "/1.0/shop/cart/cartdel";
    //购物车数量编辑
    public static final String CARTEDIT = BASE_URL_ + "/1.0/shop/cart/cartedit";
    //提供后台进行标记（准备下单购物车）
    public static final String GOADDORDER = BASE_URL_ + "/1.0/shop/order/shopcargoaddorder";
    //提供后台进行标记（准备下单商品详情）
    public static final String GOODGOADDORDER = BASE_URL_ + "/1.0/shop/order/goaddorder";
    //下单 购物车确认订单后
    public static final String ADDORDER = BASE_URL_ + "/1.0/shop/order/shopcaraddorder";
    //下单 商品详情确认订单后
    public static final String GOODADDORDER = BASE_URL_ + "/1.0/shop/order/orderaddbynow";

    //下单 订单列表
    public static final String ZHIFULIE = BASE_URL_ + "/1.0/shop/order/zhifulie";
    //下单 计算运费
    public static final String YUNFEI = BASE_URL_ + "/1.0/shop/order/jisuanyunfei";

    //支付
    public static final String PAYORDER = BASE_URL_ + "/1.0/shop/order/payOrder";
    //订单列表
    public static final String GOODSORDERLIST = BASE_URL_ + "/1.0/shop/order/orderlist";
    //取消订单
    public static final String ORDERCANCEL = BASE_URL_ + "/1.0/shop/order/orderqx";
    //订单修改地址
    public static final String EDITADDRESS = BASE_URL_ + "/1.0/shop/order/editaddress";
    //提醒发货
    public static final String TXEX = BASE_URL_ + "/1.0/shop/order/txex";
    //订单详情
    public static final String ODERDETAILS = BASE_URL_ + "/1.0/shop/order/orderdetail";
    //订单详情
    public static final String AFFIRMORDERGOODS = BASE_URL_ + "/1.0/shop/order/affirmordergoods";

    //添加收货地址
    public static final String ADD_RECEIPT_ADDRESS = BASE_URL_ + "/1.0/shop/useraddress/addaddress";

    //获取地址列表
    public static final String GET_ADDRESS_LIST = BASE_URL_ + "/1.0/shop/useraddress/getaddresslist";

    //获取地址信息
    public static final String GET_ADDRESS_INFO = BASE_URL_ + "/1.0/shop/useraddress/getaddressdetail";

    //编辑地址信息
    public static final String EDIT_ADDRESS_INFO = BASE_URL_ + "/1.0/shop/useraddress/updateaddress";

    //删除地址
    public static final String DELETE_ADDRESS = BASE_URL_ + "/1.0/shop/useraddress/deleteuseraddress";

    //获取省市县列表
    public static final String GET_COUNTRY_OR_PROVINCE_LIST = BASE_URL_ + "/1.0/shop/useraddress/getcityorarealist";


    /*********************************************注册*********************************************************/
    //注册获取验证码
    public static final String REGISTER_GET_VERIFICATION_CODE = BASE_URL_ + "/1.0/user/user/sendVerifyCode";

    //根据邀请码获取邀请人
    public static final String INVITE_PERSON_FROM_INVITE_CODE = BASE_URL_ + "/1.0/user/user/invitationCode/";

    //注册
    public static final String REGISTER = BASE_URL_ + "/1.0/user/user/register";

    //登录
    public static final String LOGIN = BASE_URL_ + "/1.0/user/user/login";

    //获取区域码
    public static final String GET_AREA_CODE = BASE_URL_ + "/1.0/user/area/findAreaCodeAll";

    //退出登录
    public static final String SIGN_OUT = BASE_URL_ + "/1.0/user/user/loginOut";

    //获取反馈意见类型
    public static final String GET_FEED_BACK_CLASS = BASE_URL_ + "/1.0/user/idea/findAreaCodeAll";

    //注册验证主账号唯一性
    public static final String MAIN_ACCOUNT_ONLY = BASE_URL_ + "/1.0/user/user/verifyUnique";

    //添加意见反馈
    public static final String FEED_BACK_COMMIT = BASE_URL_ + "/1.0/user/idea/addIdea";

    //创建子账户
    public static final String CREATE_CHILD_ACCOUNT = BASE_URL_ + "/1.0/user/user/createChild";

    //获取用户信息
    public static final String GET_USER_INFO = BASE_URL_ + "/1.0/user/user/myHome";

    //获取当前登录账号
    public static final String GET_CURRENT_ACCOUNT = BASE_URL_ + "/1.0/user/user/getchild";

    //获取关于我们
    public static final String GET_ABOUT_US = BASE_URL_ + "/1.0/user/guide/findGuide";

    //帮助中心
    public static final String GET_HELP_CENTER = BASE_URL_ + "/1.0/user/guide/findHelpCenter";

    //获取节点顶部最新算力价格
    public static final String GET_LAST_COMPUTER_PRICE = BASE_URL_ + "/1.0/busin/node/latestPrice";

    //获取节点界面折线图及我的算力数据
    public static final String GET_NODE_DATA = BASE_URL_ + "/1.0/busin/node/nodeInfo";

    //获取账号管理列表
    public static final String GET_ACCOUNT_MANAGER_LIST = BASE_URL_ + "/1.0/user/user/findChild";

    //切换账号
    public static final String SWITCH_ACCOUNT = BASE_URL_ + "/1.0/user/user/switchAccount";

    //修改密码
    public static final String UPDATE_PASSWORD = BASE_URL_ + "/1.0/user/user/editPass";

    //忘记密码
    public static final String FORGET_PASSWORD = BASE_URL_ + "/1.0/user/user/forgetPass";

    //版本更新
    public static final String VERSION_UPDATE = BASE_URL_ + "/1.0/user/tvnorm/findTvnorm";

    public static final String VER = BASE_URL_ + "/1.0/user/tvnorm/verification";
    //账号动态
    public static final String account_status = BASE_URL_ + "/1.0/user/user/findAccountType";

    //我的团队
    public static final String MY_TEAM = BASE_URL_ + "/1.0/user/user/teamMy";

    //我的团队-团队等级
    public static final String MY_TEAM_TEAM_LIST = BASE_URL_ + "/1.0/user/user/team";

    //我的团队-业绩列表
    public static final String MY_TEAM_PERFORMANCE = BASE_URL_ + "/1.0/user/user/performance";

    //我的收益
    public static final String GET_MY_EARNINGS = BASE_URL_ + "/1.0/user/assets/findProfit";

    //我的收益-列表
    public static final String MY_EARNING_LIST = BASE_URL_ + "/1.0/user/assets/findProfitList";

    //绑定手机号/邮箱
    public static final String BING_PHONE_NUMBER_OR_EMAIL = BASE_URL_ + "/1.0/user/user/sendPhoneEm";

    //首页
    public static final String FIRST_NEWS = BASE_URL_ + "/1.0/user/home/home";

    //购买算力
    public static final String PURCHASE_COMPUTER = BASE_URL_ + "/1.0/busin/node/buyHash";

    //算力购买记录
    public static final String COMPUTER_PURCHASE_RECORD = BASE_URL_ + "/1.0/busin/node/hashBuyFlow";

    //资产首页
    public static final String ASSETS_FIRST = BASE_URL_ + "/1.0/user/property/findProfit";

    //资产详情
    public static final String PROFITDETAILS = BASE_URL_ + "/1.0/user/property/profitDetails";
    //转账记录
    public static final String PROFITTRANSFER = BASE_URL_ + "/1.0/user/property/transfer";


    //获取谷歌验证器密钥
    public static final String GET_GOOGLE_SECRET = BASE_URL_ + "/1.0/product/Google/getKeyandURL";

    //绑定谷歌验证器
    public static final String BIND_GOOGLE_VERIFICATION = BASE_URL_ + "/1.0/product/Google/saveOrUpdateGoogle";

    //打开或关闭谷歌验证器
    public static final String OPEN_OR_CLOSE_GOOGLE = BASE_URL_ + "/1.0/product/Google/openOrClose";

    //获取消息首页
    public static final String GET_FIRST_MESSAGE = BASE_URL_ + "/1.0/user/notice/findTwoNotice";

    //获取消息列表
    public static final String GET_MESSAGE_LIST = BASE_URL_ + "/1.0/user/notice/findNotice";

    //修改消息状态
    public static final String UPDATE_MESSAGE_STATE = BASE_URL_ + "/1.0/user/notice/editRead";

    //获取币种列表
    public static final String GET_COIN_LIST = BASE_URL_ + "/1.0/busin/transfer/coinList";

    //查询币种可用余额
    public static final String GET_CURRENCY_BALANCE = BASE_URL_ + "/1.0/busin/transfer/findBalance";

    //获取币价显示列表
    public static final String GET_CURRENCY_PRICE_LIST = BASE_URL_ + "/1.0/user/currency/curlist";

    //获取邀请页信息
    public static final String GET_INVITE_INFO = BASE_URL_ + "/1.0/user/user/invite";

    //转账
    public static final String TRANSFER = BASE_URL_ + "/1.0/busin/transfer/send";

    //切换货币
    public static final String SWITCH_CURRENCY = BASE_URL_ + "/1.0/user/currency/editcur";

    //获取会员信息
    public static final String MEMBERINFO = BASE_URL_ + "/1.0/user/user/memberInfo";

    //修改会员信息
    public static final String EDITINFO = BASE_URL_ + "/1.0/user/user/editInfo";

    //获取资讯列表
    public static final String GET_NEWS_LIST = BASE_URL_ + "/1.0/user/news/newsList";

    //收款地址二維碼
    public static final String COLLECT_MONEY = BASE_URL_ + "/1.0/busin/transfer/blockAddress";

    //验证验证码
    public static final String VERIFICATION_VERIFICATION_CODE = BASE_URL_ + "/1.0/user/user/verifyCode";

    //币币汇率
    public static final String COIN_TO_COIN_RATE = BASE_URL_ + "/1.0/user/currency/convert";


    //获取资产币种列表
    public static final String GET_CURRENCY_LIST = BASE_URL_ + "/1.0/user/property/profitList";

    //币价显示
    public static final String GET_SELECT_COIN_RATE = BASE_URL_ + "/1.0/user/currency/coinRate";

    //修改新闻浏览次数
    public static final String UPDATE_NEWS_READ_NUM = BASE_URL_ + "/1.0/user/news/editread";

    //修改头像
    public static final String UPDATE_AVATAR = BASE_URL_ + "/1.0/user/image/upAvatar";

    //获取空投首页数据
    public static final String GET_ACTIVITY_FIRST_DATA = BASE_URL_ + "/1.0/product/airdrop/findAirdropInfo";

    //计算空头支付合计
    public static final String CALCULATIONAIRDROP = BASE_URL_ + "/1.0/product/airdrop/calculationAirdrop";
    //空头购买
    public static final String SAVEAIRDROP = BASE_URL_ + "/1.0/product/airdrop/saveAirdrop";
    //空头购买记录
    public static final String FINDAIRDROPLIST = BASE_URL_ + "/1.0/product/airdrop/findAirdropList";

    //消息全部已读
    public static final String MESSAGE_ALL_READ = BASE_URL_ + "/1.0/user/notice/editAllRead";

    //广播交易
    public static final String PUSH_TRANSACTION = BASE_URL_ + "/1.0/product/mifaEntryOrders/pushTransaction";

    //文案说明
    public static final String FINDREDIRECT = BASE_URL_ + "/1.0/user/guide/findGuide";

    //剩下错误次数
    public static final String LEFTNUM = BASE_URL_ + "/1.0/user/user/leftNum";
    public static final String ABOUTUS = BASE_URL_ + "/1.0/product/AboutUs/findAboutUs";
}
