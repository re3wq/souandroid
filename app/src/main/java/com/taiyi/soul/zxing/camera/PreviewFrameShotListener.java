package com.taiyi.soul.zxing.camera;

public interface PreviewFrameShotListener {
    public void onPreviewFrame(byte[] data, Size frameSize);
}
