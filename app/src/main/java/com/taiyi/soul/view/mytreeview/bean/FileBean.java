package com.taiyi.soul.view.mytreeview.bean;


import com.taiyi.soul.view.mytreeview.annotation.TreeNodeCreateTime;
import com.taiyi.soul.view.mytreeview.annotation.TreeNodeDesc;
import com.taiyi.soul.view.mytreeview.annotation.TreeNodeId;
import com.taiyi.soul.view.mytreeview.annotation.TreeNodeIsPrivateKey;
import com.taiyi.soul.view.mytreeview.annotation.TreeNodeName;
import com.taiyi.soul.view.mytreeview.annotation.TreeNodeNum;
import com.taiyi.soul.view.mytreeview.annotation.TreeNodePLevel;
import com.taiyi.soul.view.mytreeview.annotation.TreeNodePid;
import com.taiyi.soul.view.mytreeview.annotation.TreeNodeZLevel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 利用注解来标记bean字段
 * 使之在解析bean对象时，可以通过注解来获取对应的字段值，而不用知道具体的字段名
 */
public class FileBean implements Serializable {
    @TreeNodeId
    private String userid;

    @TreeNodePid
    private String fatherid;

    @TreeNodeName
    private String username;

    @TreeNodeDesc
    private String union_id;

    @TreeNodePLevel
    private String viplevel;
    @TreeNodeZLevel
    private String honorlevel;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    @TreeNodeNum
    private String num;

    @TreeNodeCreateTime
    private String createtime;
    private String zitype;
    private int fid;
    private String ztype;
    private String del_boolean;
    @TreeNodeIsPrivateKey
    private String isPrivateKey;


    public String getIsPrivateKey() {
        return isPrivateKey;
    }

    public void setIsPrivateKey(String isPrivateKey) {
        this.isPrivateKey = isPrivateKey;
    }

    /**
     * 子节点集合
     */
    private List<FileBean> xiaList = new ArrayList<>();


    public List<FileBean> getXiaList() {
        return xiaList;
    }

    public void setXiaList(List<FileBean> xiaList) {
        this.xiaList = xiaList;
    }

    public FileBean(String userid, String fatherid, String username, String union_id, String createtime) {
        this.userid = userid;
        this.fatherid = fatherid;
        this.username = username;
        this.union_id = union_id;
        this.createtime = createtime;
    }


    public FileBean(String userid, String fatherid, String username) {
        this.userid = userid;
        this.fatherid = fatherid;
        this.username = username;
    }

   public FileBean(String userid, String fatherid, String username,String num) {
        this.userid = userid;
        this.fatherid = fatherid;
        this.username = username;
        this.num = num;
    }



    public String getHonorlevel() {
        return honorlevel == null ? "0" : honorlevel;
    }

    public void setHonorlevel(String honorlevel) {
        this.honorlevel = honorlevel;
    }

    public String getViplevel() {
        return viplevel == null ? "0" : viplevel;
    }

    public void setViplevel(String viplevel) {
        this.viplevel = viplevel;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getZitype() {
        return zitype;
    }

    public void setZitype(String zitype) {
        this.zitype = zitype;
    }

    public String getFatherid() {
        return fatherid;
    }

    public void setFatherid(String fatherid) {
        this.fatherid = fatherid;
    }

    public String getZtype() {
        return ztype;
    }

    public void setZtype(String ztype) {
        this.ztype = ztype;
    }

    public String getDel_boolean() {
        return del_boolean;
    }

    public void setDel_boolean(String del_boolean) {
        this.del_boolean = del_boolean;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getFid() {
        return fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUnion_id() {
        return union_id;
    }

    public void setUnion_id(String union_id) {
        this.union_id = union_id;
    }
}
