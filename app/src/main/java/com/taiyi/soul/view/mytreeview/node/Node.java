package com.taiyi.soul.view.mytreeview.node;


import java.util.ArrayList;
import java.util.List;

/**
 * listview的节点类，任何数据对象都需要转化为本节点类
 */
public class Node {

    private String userid;
    private String fatherid;
    private String username;
    private String union_id;

    private String createtime;
    private String zitype;
    private int fid;
    private String ztype;
    private String del_boolean;
    private String viplevel;//普通
    private String honorlevel;//高级
    private String isPrivateKey;

    public String getIsPrivateKey() {
        return isPrivateKey;
    }

    public void setIsPrivateKey(String isPrivateKey) {
        this.isPrivateKey = isPrivateKey;
    }

    private String isSelect;

    /**
     * 节点图标
     */
    private int icon = -1;
    /**
     * 父节点
     */
    private Node parentNode;
    /**
     * 子节点集合
     */
    private List<Node> xiaList = new ArrayList<>();
    /**
     * 节点层级
     */
    private int level = 0;
    /**
     * 节点是否展开
     */
    private boolean isExpend = true;

    public Node(String userid, String fatherid, String username, String union_id,String  createtime, String isPrivateKey ) {
        this.userid = userid;
        this.fatherid = fatherid;
        this.username = username;
        this.union_id = union_id;
        this.createtime = createtime;
        this.isPrivateKey = isPrivateKey;
    }

    public String getHonorlevel() {
        return honorlevel == null ? "0" : honorlevel;
    }

    public String getHonorlevelIcon() {

//        if (!getHonorlevel().equals("0") && !getHonorlevel().equals("")) {
//            return BaseUrl.BASE_IMAGE_URL + "dengji/z" + getHonorlevel() + ".png";
//        }
        return "";
    }

    public void setHonorlevel(String honorlevel) {
        this.honorlevel = honorlevel;
    }

    public String getViplevel() {
        return viplevel == null ? "0" :  viplevel;
    }

    public String getViplevelIcon() {

//        if (!getViplevel().equals("0") && !getViplevel().equals("")) {
//            return BaseUrl.BASE_IMAGE_URL + "dengji/p" + getViplevel() + ".png";
//        }
        return "";
    }

    public void setViplevel(String viplevel) {
        this.viplevel = viplevel;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getFid() {
        return fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUnion_id() {
        return union_id;
    }

    public void setUnion_id(String union_id) {
        this.union_id = union_id;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getZitype() {
        return zitype;
    }

    public void setZitype(String zitype) {
        this.zitype = zitype;
    }

    public String getFatherid() {
        return fatherid;
    }

    public void setFatherid(String fatherid) {
        this.fatherid = fatherid;
    }

    public String getZtype() {
        return ztype;
    }

    public void setZtype(String ztype) {
        this.ztype = ztype;
    }

    public String getDel_boolean() {
        return del_boolean;
    }

    public void setDel_boolean(String del_boolean) {
        this.del_boolean = del_boolean;
    }

    public String getIsSelect() {
        return isSelect;
    }

    public void setIsSelect(String isSelect) {
        this.isSelect = isSelect;
    }

    public List<Node> getXiaList() {
        return xiaList;
    }

    public void setXiaList(List<Node> xiaList) {
        this.xiaList = xiaList;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public Node getParentNode() {
        return parentNode;
    }

    public void setParentNode(Node parentNode) {
        this.parentNode = parentNode;
    }


    /**
     * 返回节点层级
     */
    public int getLevel() {
        if (parentNode == null) {
            return 0;
        }
        return parentNode.getLevel() + 1;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isExpend() {
        return isExpend;
    }

    /**
     * 设置节点展开状态
     */
    public void setExpend(boolean expend) {
        isExpend = expend;
        //当关闭节点时，则还需要关闭其下的所有子节点
        if (!isExpend) {
            for (Node node : xiaList) {
                node.setExpend(false);
            }
        }
    }

    /**
     * 是否为根节点
     */
    public boolean isRootNode() {
        return parentNode == null;
    }

    /**
     * 判断父节点是否为展开状态
     */
    public boolean isParentExpand() {
        if (parentNode == null) {
            return false;
        }
        return parentNode.isExpend;
    }

    /**
     * 判断该节点是否为叶子节点
     */
    public boolean isLeafNode() {
        return xiaList.size() == 0;
    }


}
