package com.taiyi.soul.view.countdowntimer;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.widget.TextView;

import com.taiyi.soul.R;

public class CountDownTimerUtils extends CountDownTimer {
    private TextView mTextView;
    String img_id;
    private Context context;
    int type = 0;
    public CountDownTimerUtils(Context context, TextView textView, long millisInFuture, long countDownInterval, String img_id) {
        super(millisInFuture, countDownInterval);
        this.context = context;
        this.mTextView = textView;
        this.img_id=img_id;
        this.type=0;
    }
    public CountDownTimerUtils(Context context, TextView textView, long millisInFuture, long countDownInterval, String img_id,int type) {
        super(millisInFuture, countDownInterval);
        this.context = context;
        this.mTextView = textView;
        this.img_id=img_id;
        this.type=type;
    }
    @Override
    public void onTick(long millisUntilFinished) {
        mTextView.setClickable(false); //设置不可点击
        String sTimeFormat="";
        if(type==1){
             sTimeFormat = "%1$d s";
        }else {
             sTimeFormat = "%1$d s"+context.getResources().getString(R.string.retrieve_time);
        }

        mTextView.setText(String.format(sTimeFormat, millisUntilFinished/1000));  //设置倒计时时间
        mTextView.setTextColor(Color.parseColor(img_id));
        /**
         * 超链接 URLSpan
         * 文字背景颜色 BackgroundColorSpan
         * 文字颜色 ForegroundColorSpan
         * 字体大小 AbsoluteSizeSpan
         * 粗体、斜体 StyleSpan
         * 删除线 StrikethroughSpan
         * 下划线 UnderlineSpan
         * 图片 ImageSpan
         * http://blog.csdn.net/ah200614435/article/details/7914459
         */
      //  SpannableString spannableString = new SpannableString(mTextView.getText().toString());  //获取按钮上的文字
       // ForegroundColorSpan span = new ForegroundColorSpan(Color.GRAY);//设置灰色
        /**
         * public void setSpan(Object what, int start, int end, int flags) {
         * 主要是start跟end，start是起始位置,无论中英文，都算一个。
         * 从0开始计算起。end是结束位置，所以处理的文字，包含开始位置，但不包含结束位置。
         */
     /*   if(millisUntilFinished / 1000 < 10)
            spannableString.setSpan(span, 0, 7, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);//将倒计时的时间设置为灰色
        else
            spannableString.setSpan(span, 0, 7, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);//将倒计时的时间设置为灰色*/
       // mTextView.setText(spannableString);
    }

    @Override
    public void onFinish() {
//        mTextView.setText(context.getString(R.string.send_code));
        mTextView.setText(context.getString(R.string.again_send));
        mTextView.setClickable(true);//重新获得点击
       mTextView.setTextColor(Color.parseColor("#fefefe"));
        cancel();
    }
    public void onMyStop() {
        mTextView.setText(context.getString(R.string.send_code));
        mTextView.setClickable(true);//重新获得点击
        mTextView.setTextColor(Color.parseColor("#ffffff"));
        cancel();
    }
}
