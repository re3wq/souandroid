package com.taiyi.soul.view.dialog.activationcodebottomdialog;

import android.app.Dialog;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.taiyi.soul.R;

public class ActivationCodeBottomDialog extends Dialog implements View.OnClickListener {


    private Context mContext;
    private CallBack callBack;
    private ImageView iv_member;

    public ActivationCodeBottomDialog(Context context, CallBack callBack) {
        super(context, R.style.PhotoDialog);
        this.mContext = context;
        this.callBack = callBack;
    }

    public void setDialog(String s1, String s2, String s3, String s4, String s5, String s6) {
        View view = View.inflate(mContext, R.layout.activation_code_bottom_dialog_layout, null);
        ImageView backIv = view.findViewById(R.id.backIv);
        TextView contractNameTv = view.findViewById(R.id.contractNameTv);
        TextView actionTv = view.findViewById(R.id.actionTv);
        TextView operatingAccountTv = view.findViewById(R.id.operatingAccountTv);
        TextView accountsReceivableTv = view.findViewById(R.id.accountsReceivableTv);
        TextView transferAmountTv = view.findViewById(R.id.transferAmountTv);
        TextView remarksTv = view.findViewById(R.id.remarksTv);
        TextView confirmTv = view.findViewById(R.id.confirmTv);
        iv_member = view.findViewById(R.id.iv_select);
        remarksTv.setMovementMethod(ScrollingMovementMethod.getInstance());//数据过长滚动
        contractNameTv.setText(s1);
        actionTv.setText(s2);
        operatingAccountTv.setText(s3);
        accountsReceivableTv.setText(s4);
        transferAmountTv.setText(s5);
        remarksTv.setText(s6);
        confirmTv.setOnClickListener(this);
        backIv.setOnClickListener(this);
        iv_member.setOnClickListener(this);
        super.setContentView(view);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.width = mContext.getResources().getDisplayMetrics().widthPixels;
        layoutParams.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(layoutParams);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backIv:
                dismiss();
                callBack.cancel();
                break;
            case R.id.confirmTv:
                dismiss();
                callBack.sure();
                break;
            case R.id.iv_select:
                callBack.memberPassword(iv_member);
                break;
        }
    }

    public interface CallBack {
        void sure();

        void cancel();

        void memberPassword(ImageView view);
    }


}
