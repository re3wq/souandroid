package com.taiyi.soul.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.github.fujianlian.klinechart.utils.ViewUtil;

public class CustomView extends RelativeLayout {

    private Paint paint;//画笔
    private int lastX;//记录上一个X坐标
    private int lastY;//记录上一个Y坐标


    public CustomView(Context context) {
        super(context);
        initDraw();
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initDraw();

    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initDraw();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int x = (int) event.getX();
        int y = (int) event.getY();

        int rawX = (int) event.getRawX();
        int rawY = (int) event.getRawY();

//        int offsetX = x - lastX;
        int offsetX = 0;//x轴偏移量为0，只可以y轴上下移动
        int offsetY = y - lastY;

        ViewGroup mViewGroup = (ViewGroup) getParent();

        //取组件父集的宽高
//        int pWidth = mViewGroup.getWidth();
        int pWidth = mViewGroup.getWidth();
        int pHeight = mViewGroup.getHeight();

        //设置上下左右的坐标临界值
        int left = getLeft() + offsetX;
        left = left <= 0 ? 0 : left;

        int top = getTop() + offsetY;
        top = top <= 20 ? 20 : top;

        int right = getRight() + offsetX;
//        right = right >= pWidth ? pWidth : right;
        right = right >= pWidth ? pWidth : right;

        int bottom = getBottom() + offsetY;
        bottom = bottom >= pHeight ? pHeight : bottom;

        //设置上下左右的坐标为临界值时相关变量的值
        if(top==0){
            bottom =getHeight();
        }
        if(left==0){
            right=getWidth();
        }
        if(right==pWidth){
            left=pWidth-getWidth();
        }
        if(bottom==pHeight){
            top=pHeight-getHeight();
        }
        //判断时间种类
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = x;
                lastY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                layout(left, top, right, bottom);//移动式更新布局
                break;
            case MotionEvent.ACTION_UP:
                //松开手时根据组件中间的位置，判断吸左吸右
//                if (rawX <= pWidth / 2) {
//                    layout(0, top, getWidth(), bottom);
//                } else {
//                    layout(pWidth - getWidth(), top, pWidth, bottom);
//                }
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewUtil.Dp2Px(getContext(),140), ViewUtil.Dp2Px(getContext(),70));
                params.setMargins(getLeft(), getTop(), 0, getBottom());
                setLayoutParams(params);
                break;
        }

        return true;
    }

    /**
     * 初始化画笔
     * */
    private void initDraw() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.YELLOW);
        paint.setStrokeWidth(1.5f);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        //画布划线
        canvas.drawLine(0, height / 2, width, height / 2, paint);
    }


}