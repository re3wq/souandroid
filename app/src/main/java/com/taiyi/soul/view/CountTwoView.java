package com.taiyi.soul.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import java.math.BigDecimal;
import java.text.DecimalFormat;


/**
 * Created by Android Studio.
 * User: flh
 * Date: 2020/7/6
 * Time: 3:59 PM
 */
public class CountTwoView extends AppCompatTextView {

    private ValueAnimator mValueAnimator;
    private DecimalFormat mDf;
    private String msg="";

    public CountTwoView(Context context) {
        this(context,null);
    }
    public CountTwoView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }
    public CountTwoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        //格式化小数（保留小数点后4位）
        mDf = new DecimalFormat("0.00");
        initAnim();
    }
    /**
     * 初始化动画
     */
    private void initAnim() {
        mValueAnimator = ValueAnimator.ofFloat(0,0);//由于金钱是小数所以这里使用ofFloat方法
        mValueAnimator.setDuration(500);//动画时间为1秒
        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                if(value>0){//当数值大于0的时候才赋值
                    setText(new BigDecimal(value).setScale(2,BigDecimal.ROUND_DOWN).toPlainString());
                }
            }
        });
    }

    /**
     * 设置要显示的金钱
     * @param money
     */
    public void setMoney(float money){
        mValueAnimator.setFloatValues(0,money);//重新设置数值的变化区间
        mValueAnimator.start();//开启动画
    }
    /**
     * 取消动画和动画监听（优化内存）
     */
    public void cancle(){
        mValueAnimator.removeAllUpdateListeners();//清除监听事件
        mValueAnimator.cancel();//取消动画
    }

}