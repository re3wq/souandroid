package com.taiyi.soul.view;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.VideoView;

/**
 *
 * 视频播放,主要是因为手机的大小很多，不能保证原生的VideoView能实现全屏
 * Created by Android Studio.
 * User: flh
 * Date: 2020/4/8
 * Time: 2:17 PM
 */
public class CustomVideoView extends VideoView {
    //声明屏幕的大小
    int widths = 1080;
    int heights = 1920;
    public CustomVideoView(Context context) {
        super(context);
    }

    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //我们重新计算高度
        int width = getDefaultSize(0, widthMeasureSpec);
        int height = getDefaultSize(0, heightMeasureSpec);


        setMeasuredDimension(width, height);
//        setMeasuredDimension(width, mValue);
    }

    @Override
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener l) {
        super.setOnPreparedListener(l);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

}