package com.taiyi.soul.view;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class EditTextJudgeNumberWatcher implements TextWatcher {
    private EditText editText;
    private int precision = 4;

    public EditTextJudgeNumberWatcher(EditText editText,int precision) {
        this.editText = editText;
        this.precision = precision;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }


    @Override
    public void afterTextChanged(Editable editable) {
        judgeNumber(editable, editText);
    }

    public void judgeNumber(Editable edt, EditText editText) {
        String temp = edt.toString();
        int posDot = temp.indexOf(".");
        //返回指定字符在此字符串中第一次出现处的索引
        int index = editText.getSelectionStart();

        if (posDot == 0) {//必须先输入数字后才能输入小数点
            edt.delete(0, temp.length());//删除所有字符
            return;
        }

        if (posDot < 0) {//不包含小数点
            if (temp.length() <= 9) {
                return;//小于五位数直接返回
            } else {
                edt.delete(index - 1, index);//删除光标前的字符
                return;
            }
        }
        if (posDot > 5) {//小数点前大于5位数就删除光标前一位
            edt.delete(index - 1, index);//删除光标前的字符

        }
        if (temp.length() - posDot - 1 > precision) {
            //如果包含小数点
            edt.delete(index - 1, index);//删除光标前的字符
            return;
        }
    }
}
