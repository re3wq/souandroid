package com.taiyi.soul.view.mytreeview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.view.mytreeview.node.Node;
import com.taiyi.soul.view.mytreeview.utils.TreeListViewAdapter;


import java.util.List;

public class SimpleTreeListViewAdapter<T> extends TreeListViewAdapter<T> {


    private OnCopyClickListener onCopyClickListener;
    private String main_account;

    private String from;

    public OnCopyClickListener getOnCopyClickListener() {
        return onCopyClickListener;
    }

    public OnNameClickListener onNameClickListener;
    public SwitchClickListener switchClickListener;

    public OnNameClickListener getOnNameClickListener() {
        return onNameClickListener;
    }

    public void setOnNameClickListener(OnNameClickListener onNameClickListener) {
        this.onNameClickListener = onNameClickListener;
    }

    public void setOnCopyClickListener(OnCopyClickListener onCopyClickListener) {
        this.onCopyClickListener = onCopyClickListener;
    }

    public void setSwitchClickListener(SwitchClickListener switchClickListener) {
        this.switchClickListener = switchClickListener;
    }

    public SimpleTreeListViewAdapter(Context context, String main_account, String from, ListView listView, List<T> datas, int defaultNodeLevel)
            throws IllegalAccessException {
        super(context, listView, datas, defaultNodeLevel);
        this.main_account = main_account;
        this.from = from;
    }

    @Override
    public View getConvertView(Node node, final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_account_tree, parent, false);
            holder = new ViewHolder();
            holder.iv = convertView.findViewById(R.id.iv);
            holder.tv_name = convertView.findViewById(R.id.tv_name);
            holder.tv_code = convertView.findViewById(R.id.tv_code);
            holder.iv_copy = convertView.findViewById(R.id.iv_copy);
            holder.iv_selected = convertView.findViewById(R.id.iv_selected);
            holder.iv_p_level = convertView.findViewById(R.id.iv_p_level);
            holder.iv_z_level = convertView.findViewById(R.id.iv_z_level);
            holder.invite_code = convertView.findViewById(R.id.invite_code);
//            holder.ll_item = convertView.findViewById(R.id.ll_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
//        holder.ll_item.setLayoutParams(new RelativeLayout.LayoutParams((DensityUtil.dip2px(mContext,10) * node.getLevel())+DensityUtil.dip2px(mContext,515) , ViewGroup.LayoutParams.WRAP_CONTENT));

        //图标
        if (node.getIcon() == -1) {
//            holder.iv.setVisibility(View.INVISIBLE);
        } else {
//            holder.iv.setVisibility(View.VISIBLE);
            holder.iv.setImageResource(node.getIcon());
        }

        if (node.getUsername().equals(main_account)) {
            holder.iv_selected.setImageResource(R.mipmap.language_select_iv);
        } else {
            holder.iv_selected.setImageResource(R.mipmap.language_unselect_iv);
        }


        if (node.getIsPrivateKey().equals("0")) {//没有私钥
            holder.tv_name.setText(node.getUsername() + mContext.getString(R.string.no_private_key));
            holder.tv_name.setTextColor(MyApplication.getInstance().getResources().getColor(R.color.color_818181));
            holder.tv_code.setTextColor(MyApplication.getInstance().getResources().getColor(R.color.color_818181));
        } else {
            if (node.getUsername().equals(main_account)) {
                holder.tv_name.setTextColor(MyApplication.getInstance().getResources().getColor(R.color.white));
                holder.tv_code.setTextColor(MyApplication.getInstance().getResources().getColor(R.color.white));
            }
            else {
                holder.tv_name.setTextColor(Color.parseColor("#ccffffff"));
                holder.tv_code.setTextColor(Color.parseColor("#ccffffff"));
            }
            holder.tv_name.setText(node.getUsername());
        }
        holder.tv_code.setText(mContext.getString(R.string.invite_code_) + node.getUnion_id());

        if(null==node.getUnion_id()){
            holder.invite_code.setVisibility(View.GONE);

        }else {
            holder.invite_code.setVisibility(View.VISIBLE);

        }
        holder.iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expendOrCollapseNode(position);
            }
        });
        holder.iv_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchClickListener.switchAccount(position);
            }
        });
        holder.iv_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onCopyClickListener.onCopyClick(node.getUnion_id());

            }
        });
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNameClickListener.onNameClick(position);
            }
        });

        holder.tv_name.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onNameClickListener.onNameLongClick(position);
                return true;
            }
        });

        return convertView;
    }

    private class ViewHolder {
        ImageView iv;
        TextView tv_name;
        TextView tv_code;
        ImageView iv_copy;
        ImageView iv_selected;
        ImageView iv_p_level;
        ImageView iv_z_level;
        LinearLayout ll_item;
        LinearLayout invite_code;
    }


    /**
     * TreeListView的icon对外点击接口
     */
    public interface OnCopyClickListener {
        void onCopyClick(String value);
    }

    public interface OnNameClickListener {
        void onNameClick(int position);

        void onNameLongClick(int position);
    }

    public interface SwitchClickListener{
        void switchAccount(int position);
    }
}
