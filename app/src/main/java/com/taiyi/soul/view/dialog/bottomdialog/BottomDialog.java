package com.taiyi.soul.view.dialog.bottomdialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.taiyi.soul.R;


public class BottomDialog extends Dialog implements View.OnClickListener {

    BottomCallBack callback;
    private TextView tv_cancel;
    private TextView tv_confirm;
    private TextView tv_account_in;
    private TextView tv_account_out;
    private TextView tv_amount;
    private TextView tv_memo;

    private Context context;

    public BottomDialog(Context context, BottomCallBack callback) {
        super(context, R.style.PhotoDialog);
        this.callback = callback;
        this.context = context;
    }


    public void setCustomDialog(String address_in,String address_out, String amount, String memo) {

        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_bottom, null);

        tv_cancel = (TextView) mView.findViewById(R.id.tv_cancel);
        tv_confirm = (TextView) mView.findViewById(R.id.tv_confirm);
        tv_account_in = (TextView) mView.findViewById(R.id.tv_account_in);
        tv_account_out = (TextView) mView.findViewById(R.id.tv_account_out);
        tv_amount = (TextView) mView.findViewById(R.id.tv_amount);
        tv_memo = (TextView) mView.findViewById(R.id.tv_memo);

        tv_account_in.setText(address_in);
        tv_account_out.setText(address_out);
        tv_amount.setText(amount);
        tv_memo.setText(memo);

        tv_cancel.setOnClickListener(this);
        tv_confirm.setOnClickListener(this);
        super.setContentView(mView);

        WindowManager windowManager = getWindow().getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.width = (int) (display.getWidth()); //设置宽度
//        lp.height = (int) (display.getHeight() - 20); //设置宽度
        this.getWindow().setAttributes(lp);
    }


    public BottomDialog setContent() {
        return this;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_confirm:
                callback.sure();
                this.cancel();
                break;
            case R.id.tv_cancel:
                this.cancel();
                break;
        }
    }

}
