package com.taiyi.soul.view.dialog.passworddialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taiyi.soul.R;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.utils.PaymentUtils;
import com.taiyi.soul.utils.Utils;
import com.taiyi.soul.view.ClearEditText;


/**
 * 验证钱包密码框
 */

public class PasswordDialog extends Dialog implements View.OnClickListener {

    PasswordCallback callback;
    private ClearEditText mClearEditText;
    private TextView sureBtn;
    private TextView cancleBtn;
    private Context context;
    private InputMethodManager inputMethodManager;
    private TextView tvOpeFinger;
    private ImageView ivMore;
    private TextView tv_tips;

    /**
     * 0---钱包密码
     * 1---解析私钥密码
     */
    private int type;

    public PasswordDialog(Context context,String from, PasswordCallback callback) {
        super(context, R.style.CustomDialog);
        this.callback = callback;
        this.context = context;
        type = 0;
        setCustomDialog(from);
    }

    public PasswordDialog(Context context, PasswordCallback callback) {
        super(context, R.style.CustomDialog);
        this.callback = callback;
        this.context = context;
        type = 1;
        setCustomDialog();
    }



    private void setCustomDialog(String from) {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_password, null);
        sureBtn = (TextView) mView.findViewById(R.id.dialog_confirm_sure);
        cancleBtn = (TextView) mView.findViewById(R.id.dialog_confirm_cancle);
        mClearEditText = (ClearEditText) mView.findViewById(R.id.dialog_password);
        tvOpeFinger = mView.findViewById(R.id.tv_open_finger);
        ivMore = mView.findViewById(R.id.iv_more);
        tv_tips = mView.findViewById(R.id.tv_tips);

        mClearEditText.setFocusable(true);
        mClearEditText.setFocusableInTouchMode(true);
        mClearEditText.requestFocus();

        if (Utils.getSpUtils().getInt(Constants.IS_FINGEREPRINT + Utils.getSpUtils().getString(Constants.PHONE,""),0) == 0
                && !from.equals("me")
                && PaymentUtils.isSupportFingerprint(context)
                ){
            ivMore.setVisibility(View.VISIBLE);
            tvOpeFinger.setVisibility(View.VISIBLE);

        }else {
            ivMore.setVisibility(View.GONE);
            tvOpeFinger.setVisibility(View.GONE);

        }

        inputMethodManager = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
        sureBtn.setOnClickListener(this);
        cancleBtn.setOnClickListener(this);
        tvOpeFinger.setOnClickListener(this);
        super.setContentView(mView);
        WindowManager windowManager = getWindow().getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.width = (int) (display.getWidth() * 0.78); //设置宽度
        this.getWindow().setAttributes(lp);
    }

    private void setCustomDialog() {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_password, null);
        sureBtn = (TextView) mView.findViewById(R.id.dialog_confirm_sure);
        cancleBtn = (TextView) mView.findViewById(R.id.dialog_confirm_cancle);
        mClearEditText = (ClearEditText) mView.findViewById(R.id.dialog_password);
        tvOpeFinger = mView.findViewById(R.id.tv_open_finger);
        ivMore = mView.findViewById(R.id.iv_more);
        tv_tips = mView.findViewById(R.id.tv_tips);

        mClearEditText.setHint(context.getString(R.string.please_qr_password));

        mClearEditText.setFocusable(true);
        mClearEditText.setFocusableInTouchMode(true);
        mClearEditText.requestFocus();


        ivMore.setVisibility(View.GONE);
        tvOpeFinger.setVisibility(View.GONE);



        inputMethodManager = ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE));
        sureBtn.setOnClickListener(this);
        cancleBtn.setOnClickListener(this);
        tvOpeFinger.setOnClickListener(this);
        super.setContentView(mView);
        WindowManager windowManager = getWindow().getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.width = (int) (display.getWidth() * 0.78); //设置宽度
        this.getWindow().setAttributes(lp);
    }


    public PasswordDialog setContent() {
        return this;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.dialog_confirm_cancle:
                inputMethodManager.hideSoftInputFromWindow(mClearEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                callback.cancle();
                this.cancel();
                break;

            case R.id.dialog_confirm_sure:
                if (mClearEditText.getText().toString().trim().length() != 0) {
                    if (type == 0) {
//                        if (MyApplication.getInstance().getWalletBean() != null
//                                && MyApplication.getInstance().getWalletBean().getWallet_shapwd() != null
//                                && MyApplication.getInstance().getWalletBean().getWallet_shapwd().equals(PasswordToKeyUtils.shaCheck(mClearEditText.getText().toString().trim()))) {

                            inputMethodManager.hideSoftInputFromWindow(mClearEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            callback.sure(mClearEditText.getText().toString().trim());

                            this.cancel();
//                        } else {
//                            Toast.makeText(context,R.string.password_error,Toast.LENGTH_LONG).show();
//                        }
                    }else {
                        inputMethodManager.hideSoftInputFromWindow(mClearEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        callback.sure(mClearEditText.getText().toString().trim());
                        this.cancel();
                    }
                } else {
                    Toast.makeText(context,R.string.input_wallet_pwd,Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.tv_open_finger:
                Bundle bundle = new Bundle();
                bundle.putInt("type",10);
//                ActivityUtils.next((Activity) context, MeActivity.class, bundle,false);
                callback.cancle();
                this.cancel();
                break;
        }
    }
}
