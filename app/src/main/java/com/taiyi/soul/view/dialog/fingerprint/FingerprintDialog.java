package com.taiyi.soul.view.dialog.fingerprint;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.taiyi.soul.R;
import com.taiyi.soul.base.Constants;
import com.taiyi.soul.utils.Utils;

import javax.crypto.Cipher;


public class FingerprintDialog extends Dialog implements View.OnClickListener {

    FingerPrintCallBack callback;
    private TextView error_msg;
    private TextView tv_password;
    private TextView tv_cancel;
    private Context context;

    private FingerprintManager mFingerprintManager;
    private CancellationSignal mCancellationSignal;
    private Cipher mCipher;

    private boolean isSelfCancelled;


    public FingerprintDialog(Context context, Cipher cipher,FingerPrintCallBack callback) {
        super(context, R.style.CustomDialog);
        this.callback = callback;
        this.mCipher = cipher;
        this.context = context;
        setCustomDialog();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void setCustomDialog() {
        mFingerprintManager = context.getSystemService(FingerprintManager.class);

        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_fingerprint, null);
        tv_password = (TextView) mView.findViewById(R.id.tv_password);
        tv_cancel = (TextView) mView.findViewById(R.id.tv_cancel);
        error_msg = (TextView) mView.findViewById(R.id.error_msg);
        tv_password.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        super.setContentView(mView);
        WindowManager windowManager = getWindow().getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.width = (int) (display.getWidth() * 0.78); //设置宽度
        this.getWindow().setAttributes(lp);

        startListening(mCipher);
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void startListening(Cipher cipher) {
        isSelfCancelled = false;

        mCancellationSignal = new CancellationSignal();
        mFingerprintManager.authenticate(new FingerprintManager.CryptoObject(cipher),
                mCancellationSignal,
                0,
                new FingerprintManager.AuthenticationCallback() {
                    @Override
                    public void onAuthenticationError(int errorCode, CharSequence errString) {
                        if (!isSelfCancelled) {
                            error_msg.setText(errString);
                            if (errorCode == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
                                Toast.makeText(context, errString, Toast.LENGTH_SHORT).show();
                                dismiss();
                                callback.cancel();
                            }
                        }
                    }

                    @Override
                    public void onAuthenticationFailed() {
                        error_msg.setText(context.getString(R.string.fingerprint_err));
                    }

                    @Override
                    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                        error_msg.setText(helpString);
                    }

                    @Override
                    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                        stopListening();
                        cancel();
                        callback.onAuthenticated(Utils.getSpUtils().getString(Constants.PASSWORD + Utils.getSpUtils().getString(Constants.PHONE, "")));
                    }
                },
                null);
    }


    private void stopListening() {
        if (mCancellationSignal != null) {
            mCancellationSignal.cancel();
            mCancellationSignal = null;
            isSelfCancelled = true;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_password:
                stopListening();
                callback.inputPassword();
                this.cancel();
                break;

            case R.id.tv_cancel:
                stopListening();
                this.cancel();
                callback.cancel();
                break;
        }
    }

    public interface FingerPrintCallBack{
        void onAuthenticated(String password);
        void inputPassword();
        void cancel();
    }

}
