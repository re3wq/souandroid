package com.taiyi.soul.view.dialog.sharedialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taiyi.soul.R;


public class ShareDialog extends Dialog{

    OnShareItemClick callback;
    private LinearLayout ll;
    private ImageView iv_share_img;
    private ImageView iv_share_wechat;
    private ImageView iv_share_wechat_friends;
    private TextView tv_cancel;

    private Bitmap bm;


    private Context context;


    public ShareDialog(Context context, Bitmap bitmap, OnShareItemClick callback) {
        super(context, R.style.PhotoDialog);
        this.callback = callback;
        this.context = context;
        this.bm = bitmap;
        setCustomDialog();
    }


    public void setCustomDialog(){

        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_share, null);
        iv_share_img = mView.findViewById(R.id.iv_share_img);
        iv_share_wechat = mView.findViewById(R.id.iv_share_wechat);
        iv_share_wechat_friends = mView.findViewById(R.id.iv_share_wechat_friends);
        tv_cancel = mView.findViewById(R.id.tv_cancel);

        ll = mView.findViewById(R.id.ll);

        iv_share_img.setImageBitmap(bm);

        super.setContentView(mView);




        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });

        iv_share_wechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.wechatShareClick();
            }
        });

        iv_share_wechat_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.wechatFriendsShareClick();
            }
        });

        WindowManager windowManager = getWindow().getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.width = (int) (display.getWidth()); //设置宽度
//        lp.height = (int) (display.getHeight() - 20); //设置宽度
        this.getWindow().setAttributes(lp);
    }


    public ShareDialog setContent() {
        return this;
    }



    public interface OnShareItemClick{
        void wechatShareClick();
        void wechatFriendsShareClick();
    }
}
