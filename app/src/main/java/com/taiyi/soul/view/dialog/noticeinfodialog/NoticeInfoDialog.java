package com.taiyi.soul.view.dialog.noticeinfodialog;

import android.app.Dialog;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.taiyi.soul.R;


public class NoticeInfoDialog extends Dialog implements View.OnClickListener {

    NoticeInfoCallback callback;
    private TextView dialog_content;
    private TextView sureBtn;
    private TextView dialog_title;
    private Context context;

    public NoticeInfoDialog(Context context, NoticeInfoCallback callback) {
        super(context, R.style.CustomDialog);
        this.callback = callback;
        this.context = context;
        setCustomDialog();
    }

    private void setCustomDialog() {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_notice_info, null);
        sureBtn = mView.findViewById(R.id.dialog_confirm_sure);
        dialog_content = mView.findViewById(R.id.dialog_content);
        dialog_title = mView.findViewById(R.id.dialog_title);

        dialog_content.setMovementMethod(new ScrollingMovementMethod());
        sureBtn.setOnClickListener(this);

        super.setContentView(mView);
        WindowManager windowManager = getWindow().getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.width = (int) (display.getWidth() * 0.78); //设置宽度
        this.getWindow().setAttributes(lp);
    }



    public NoticeInfoDialog setToast(String title ,String cont) {
        dialog_title.setText(title);
        dialog_content.setText(cont);
        return this;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_confirm_sure:
                callback.callback();
                this.cancel();
                break;
        }
    }

}
