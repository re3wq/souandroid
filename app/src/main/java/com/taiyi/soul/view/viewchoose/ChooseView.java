package com.taiyi.soul.view.viewchoose;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taiyi.soul.R;


public class ChooseView extends LinearLayout implements View.OnClickListener{

    private LinearLayout ll_view;
    private TextView tv_left;
    private TextView tv_right;


    private View mView;

    private Context context;

    private ChooseItemClick chooseItemClick;


    public ChooseItemClick getChooseItemClick() {
        return chooseItemClick;
    }

    public void setChooseItemClick(ChooseItemClick chooseItemClick) {
        this.chooseItemClick = chooseItemClick;
    }

    public ChooseView(Context context) {
        super(context,null);
        this.context = context;

    }


    public ChooseView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        mView = LayoutInflater.from(getContext()).inflate(R.layout.view_choose, null);
        ll_view = (LinearLayout) mView.findViewById(R.id.ll_view);
        tv_left = (TextView) mView.findViewById(R.id.tv_left);
        tv_right = (TextView) mView.findViewById(R.id.tv_right);
        tv_left.setBackgroundResource(R.drawable.blue_shape_btn);
        tv_right.setBackgroundResource(R.drawable.gray_shape_btn);
        tv_left.setTextColor(context.getResources().getColor(R.color.white));
        tv_right.setTextColor(context.getResources().getColor(R.color.main_color));
        tv_left.setOnClickListener(this);
        tv_right.setOnClickListener(this);
        this.addView(mView);
    }


    public void setContent(String left, String right){
        tv_left.setText(left);
        tv_right.setText(right);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tv_left:
                tv_left.setBackgroundResource(R.drawable.blue_shape_btn);
                tv_right.setBackgroundResource(R.drawable.gray_shape_btn);
                tv_left.setTextColor(context.getResources().getColor(R.color.white));
                tv_right.setTextColor(context.getResources().getColor(R.color.main_color));
                chooseItemClick.leftOnClick(this);
                break;
            case R.id.tv_right:

                tv_left.setBackgroundResource(R.drawable.gray_shape_btn);
                tv_right.setBackgroundResource(R.drawable.blue_shape_btn);
                tv_left.setTextColor(context.getResources().getColor(R.color.main_color));
                tv_right.setTextColor(context.getResources().getColor(R.color.white));
                chooseItemClick.rightOnClick(this);
                break;
        }
    }



    public interface ChooseItemClick{
        void leftOnClick(View view);
        void rightOnClick(View view);
    }
}
