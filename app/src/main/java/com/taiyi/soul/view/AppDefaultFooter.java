package com.taiyi.soul.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.taiyi.soul.R;
import com.liaoinstan.springview.container.BaseFooter;

public class AppDefaultFooter extends BaseFooter {
    private Context context;
    private TextView footerTitle;
    private ProgressBar footerProgressbar;

    public AppDefaultFooter(Context context) {
        this.context = context;
    }

    @Override
    public View getView(LayoutInflater inflater, ViewGroup viewGroup) {
        View view = inflater.inflate(R.layout.app_defeat_footer_view, viewGroup, true);
        footerTitle = view.findViewById(R.id.app_defeat_header_text);
        footerProgressbar = view.findViewById(R.id.app_defeat_header_progressbar);
        return view;
    }

    @Override
    public void onPreDrag(View rootView) {
    }

    @Override
    public void onDropAnim(View rootView, int dy) {
    }

    @Override
    public void onLimitDes(View rootView, boolean upORdown) {
        if (upORdown) {
            footerTitle.setText(context.getString(R.string.open_load_more));
        } else {
            footerTitle.setText(context.getString(R.string.look_more));
        }
    }

    @Override
    public void onStartAnim() {
        footerTitle.setVisibility(View.VISIBLE);
        footerProgressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFinishAnim() {
        footerTitle.setText(context.getString(R.string.look_more));
        footerTitle.setVisibility(View.VISIBLE);
        footerProgressbar.setVisibility(View.VISIBLE);
    }

}