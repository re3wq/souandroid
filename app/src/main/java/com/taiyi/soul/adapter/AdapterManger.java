package com.taiyi.soul.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.taiyi.soul.R;
import com.taiyi.soul.adapter.baseadapter.CommonAdapter;
import com.taiyi.soul.adapter.baseadapter.base.ViewHolder;
import com.taiyi.soul.app.ActivityUtils;
import com.taiyi.soul.app.MyApplication;
import com.taiyi.soul.bean.Language;
import com.taiyi.soul.bean.MnemonicBean;
import com.taiyi.soul.moudles.account.AccountBean;
import com.taiyi.soul.moudles.login.bean.AreaCodeBean;
import com.taiyi.soul.moudles.main.assets.bean.AssetsBean;
import com.taiyi.soul.moudles.main.assets.bean.CoinListBean;
import com.taiyi.soul.moudles.main.assets.distribution.bean.PieBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.BondRecordBean;
import com.taiyi.soul.moudles.main.assets.exchange.bean.ExchangeBean;
import com.taiyi.soul.moudles.main.deal.bean.DealPayBean;
import com.taiyi.soul.moudles.main.home.bean.HomeBean;
import com.taiyi.soul.moudles.main.home.bean.NewsListBean;
import com.taiyi.soul.moudles.main.home.mall.bean.DemoMallHotBean;
import com.taiyi.soul.moudles.main.home.mall.bean.DemoOptionsBean;
import com.taiyi.soul.moudles.main.home.mall.bean.DemoOrderBean;
import com.taiyi.soul.moudles.main.home.mall.bean.MallFirstBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.CheckConfirmListBean;
import com.taiyi.soul.moudles.main.home.mall.order.bean.OrderBean;
import com.taiyi.soul.moudles.main.home.options.bean.OptionPopupBean;
import com.taiyi.soul.moudles.main.node.bean.ComputerPurchaseRecordBean;
import com.taiyi.soul.moudles.main.node.bean.CpuNetBean;
import com.taiyi.soul.moudles.main.node.bean.HashMarketBean;
import com.taiyi.soul.moudles.main.node.bean.TransactionRecordBean;
import com.taiyi.soul.moudles.mine.bean.AddressListBean;
import com.taiyi.soul.moudles.mine.bean.AreaBean;
import com.taiyi.soul.moudles.mine.bean.CurrencyListBean;
import com.taiyi.soul.moudles.mine.bean.DataBean;
import com.taiyi.soul.moudles.mine.bean.MyEarningsListBean;
import com.taiyi.soul.moudles.mine.bean.MyTeamPerformanceListBean;
import com.taiyi.soul.moudles.mine.bean.TestBean;
import com.taiyi.soul.moudles.mine.message.bean.MessageListBean;
import com.taiyi.soul.moudles.mine.shippingaddress.add.AddShippingAddressActivity;
import com.taiyi.soul.utils.DensityUtil;
import com.taiyi.soul.utils.NumUtils;
import com.taiyi.soul.utils.Utils;
import com.yanzhenjie.recyclerview.SwipeRecyclerView;

import java.math.BigDecimal;
import java.util.List;


/**
 * Created by NGK on 2018/3/14.
 */
public class AdapterManger {

    /**
     * The M common adapter.
     */
    static CommonAdapter mCommonAdapter;


    /**
     * 国际化
     *
     * @param context
     * @param languages
     * @return
     */
    public static CommonAdapter getLanguageListAdapter(final Context context, List<Language> languages) {
        mCommonAdapter = new CommonAdapter<Language>(context, R.layout.item_node_layout, languages) {
            @Override
            protected void convert(final ViewHolder holder, final Language item, final int position) {
                holder.setText(R.id.tv_name, item.getLang());

                if (item.getIsSelect().equals("0")) {
                    holder.setVisible1(R.id.iv_selected, false);
                } else {
                    holder.setVisible1(R.id.iv_selected, true);
                }
            }
        };
        return mCommonAdapter;
    }


    /**
     * 账户管理
     *
     * @param context
     * @param accountBeans
     * @return
     */
    public static CommonAdapter getManagerListAdapter(final Context context, List<AccountBean> accountBeans, OnItemClick onItemClick) {

        mCommonAdapter = new CommonAdapter<AccountBean>(context, R.layout.item_account_tree, accountBeans) {
            @Override
            protected void convert(final ViewHolder holder, final AccountBean item, final int position) {

                //不做组件复用
                holder.setIsRecyclable(false);

                View v = holder.getView(R.id.view);

                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.getLayoutParams();
                lp.width = DensityUtil.dip2px(context, 30) * item.getLevel();
                v.setLayoutParams(lp);


                //当一个完整的层级结束，加分割线
//                if (position != accountBeans.size() - 1 && accountBeans.get(position + 1).getLevel() == 0) {
//                    View view_line = holder.getView(R.id.view_line);
//                    LinearLayout.LayoutParams lp1 = (LinearLayout.LayoutParams) view_line.getLayoutParams();
//                    lp1.height = DensityUtil.dip2px(context, 20);
//                    view_line.setLayoutParams(lp1);
//                }


                holder.setText(R.id.tv_name, item.getAccount_name());
                holder.setText(R.id.tv_code, "邀请码：" + item.getInvitation_code());

//                if (item.getIsSelect().equals("0")) {
//                    holder.setVisible1(R.id.iv_selected, false);
//                } else {
//                    holder.setVisible1(R.id.iv_selected, true);
//                }

                holder.setOnClickListener(R.id.iv_selected, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClick.onSelect(position);
                    }
                });

                holder.setOnClickListener(R.id.iv_selected, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClick.onCopy(item.getInvitation_code());
                    }
                });
            }
        };
        return mCommonAdapter;
    }


    /**
     * 国际化短信code
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getCodeListAdapter(final Context context, List<AreaCodeBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<AreaCodeBean>(context, R.layout.item_code, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final AreaCodeBean item, final int position) {
                holder.setText(R.id.tv_name, item.getCode());
                ImageView iv = holder.getView(R.id.iv);
                Glide.with(context).load(item.getIcon()).into(iv);
            }
        };
        return mCommonAdapter;
    }
    /**************************************兑换****************************************/
    /**
     * 交易（卖单）
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getBoundExchangetAdapter(final Context context, List<BondRecordBean.ComlistBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<BondRecordBean.ComlistBean>(context, R.layout.item_deal_sell, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final BondRecordBean.ComlistBean item, final int position) {


            }
        };
        return mCommonAdapter;
    }
    /**************************************交易****************************************/
    /**
     * 交易（卖单）
     *
     * @param context
     * @param areaCodeBeans
     * @param dealItemClick
     * @return
     */
    public static CommonAdapter getDealSellAdapter(final Context context, List<DealPayBean> areaCodeBeans, DealItemClick dealItemClick) {
        mCommonAdapter = new CommonAdapter<DealPayBean>(context, R.layout.item_deal_sell, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DealPayBean item, final int position) {

                holder.setText(R.id.deal_user, item.names);
                holder.setText(R.id.deal_total, item.total);
                holder.setText(R.id.deal_amount, item.num);
                holder.setText(R.id.deal_price, item.price);

                holder.setText(R.id.deal_total_unit, item.total_type);
                holder.setText(R.id.deal_amount_unit, item.num_type);
                holder.setText(R.id.deal_price_unit, item.price_type);
                holder.setOnClickListener(R.id.deal_buying, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dealItemClick.onDeal(position);
                    }
                });
            }
        };
        return mCommonAdapter;
    }

    /**
     * 交易（买单）
     *
     * @param context
     * @param areaCodeBeans
     * @param dealItemClick
     * @return
     */
    public static CommonAdapter getDealPayAdapter(final Context context, List<DealPayBean> areaCodeBeans, DealItemClick dealItemClick) {
        mCommonAdapter = new CommonAdapter<DealPayBean>(context, R.layout.item_deal_pay, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DealPayBean item, final int position) {

                holder.setText(R.id.deal_user, item.names);
                holder.setText(R.id.deal_total, item.total);
                holder.setText(R.id.deal_amount, item.num);
                holder.setText(R.id.deal_price, item.price);

                holder.setText(R.id.deal_total_unit, item.total_type);
                holder.setText(R.id.deal_amount_unit, item.num_type);
                holder.setText(R.id.deal_price_unit, item.price_type);
                holder.setOnClickListener(R.id.deal_buying, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dealItemClick.onDeal(position);
                    }
                });
            }
        };
        return mCommonAdapter;
    }

    /**
     * 我的委托
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getMainEntrustAdapter(final Context context, List<DealPayBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<DealPayBean>(context, R.layout.item_entrust, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DealPayBean item, final int position) {
                //名称、总价、数量、单价、总价单位、数量单位、单价单位、买入按钮
                holder.setText(R.id.entrust_time, item.names);
                holder.setText(R.id.entrust_total, item.total);
                holder.setText(R.id.entrust_amount, item.num);
                holder.setText(R.id.entrust_price, item.price);

                holder.setText(R.id.entrust_total_unit, item.total_type);
                holder.setText(R.id.entrust_amount_unit, item.num_type);
                holder.setText(R.id.entrust_price_unit, item.price_type);
            }
        };
        return mCommonAdapter;
    }
    /**************************************商城****************************************/
    /**
     * 热销
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getHotListAdapter(final Context context, List<MallFirstBean.CatelistBean.ComlistBean> areaCodeBeans, GoodsHotItemClick itemClick) {
        mCommonAdapter = new CommonAdapter<MallFirstBean.CatelistBean.ComlistBean>(context, R.layout.item_mall_hot, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final MallFirstBean.CatelistBean.ComlistBean item, final int position) {
                int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
                int px = DensityUtil.dip2px(context, 36);
                int width = (int) ((widthPixels - px) / 2.5f);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.getConvertView().setLayoutParams(layoutParams);
                ImageView view = holder.getView(R.id.mall_goods_img);
                Glide.with(context).load(item.getSmallimgurl()).apply(RequestOptions.bitmapTransform(new RoundedCorners(35)).override(300, 300)).into(view);
                holder.setText(R.id.mall_goods_name, item.getName());
                holder.setText(R.id.mall_goods_price, item.getHyprice() + " " + context.getResources().getString(R.string.USDN));

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClick.onItemClick(position, item.getCommodityid());

                    }
                });
            }
        }

        ;
        return mCommonAdapter;
    }

    /**
     * 折扣
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getDiscountListAdapter(final Context context, List<MallFirstBean.CatelistBean.ComlistBean> areaCodeBeans, GoodsHotItemClick itemClick) {
        mCommonAdapter = new CommonAdapter<MallFirstBean.CatelistBean.ComlistBean>(context, R.layout.item_mall_discount, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final MallFirstBean.CatelistBean.ComlistBean item, final int position) {
                int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
                int px = DensityUtil.dip2px(context, 36);
                int width = (int) ((widthPixels - px) / 4.0f);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.getConvertView().setLayoutParams(layoutParams);

                ImageView view = holder.getView(R.id.mall_goods_img);
                Glide.with(context).load(item.getSmallimgurl()).apply(RequestOptions.bitmapTransform(new RoundedCorners(35)).override(300, 300)).into(view);
                holder.setText(R.id.mall_goods_name, item.getName());
                holder.setText(R.id.mall_goods_price, item.getHyprice() + " " + context.getResources().getString(R.string.USDN));
                TextView disprice = holder.getView(R.id.mall_goods_disprice);
                disprice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                holder.setText(R.id.mall_goods_disprice, item.getPrice() + " " + context.getResources().getString(R.string.USDN));
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClick.onItemClick(position, item.getCommodityid());

                    }
                });
            }
        };
        return mCommonAdapter;
    }

    /**
     * 商城更多
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getMoreListAdapter(final Context context, List<DemoMallHotBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<DemoMallHotBean>(context, R.layout.item_mall_more, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DemoMallHotBean item, final int position) {
                ImageView view = holder.getView(R.id.mall_goods_img);
                Glide.with(context).load(item.img).into(view);
                holder.setText(R.id.mall_goods_name, item.name);
                holder.setText(R.id.mall_goods_price, item.price);
            }
        };
        return mCommonAdapter;
    }

    /**
     * 购物车
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getCartListAdapter(final Context context, List<DemoMallHotBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<DemoMallHotBean>(context, R.layout.item_shopping_cart, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DemoMallHotBean item, final int position) {
                ImageView view = holder.getView(R.id.cart_img);
                Glide.with(context).load(item.img).into(view);
                holder.setText(R.id.cart_name, item.name);
                holder.setText(R.id.cart_price, item.price);
            }
        };
        return mCommonAdapter;
    }

    /**
     * 确认订单
     *
     * @param context
     * @param recyclerView
     * @param beanList
     * @return
     */
    public static CommonAdapter getCheckOrderListAdapter(final Context context, SwipeRecyclerView recyclerView, List<CheckConfirmListBean> beanList) {
        mCommonAdapter = new CommonAdapter<CheckConfirmListBean>(context, R.layout.item_check_confirm, beanList) {
            @Override
            protected void convert(final ViewHolder holder, final CheckConfirmListBean item, final int position) {
                int height = recyclerView.getHeight();
//                Log.e("heigh===", height + "");
                int heights = (int) (height / 3f);

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, heights);
                layoutParams.setMargins(0, 0, 0, 0);
                holder.getConvertView().setLayoutParams(layoutParams);

                ImageView view = holder.getView(R.id.cart_img);
                Glide.with(context).load(item.img).into(view);
                holder.setText(R.id.cart_name, item.name);
                holder.setText(R.id.cart_price, item.price + " USDN");
                holder.setText(R.id.num, "X" + item.num);
                holder.setText(R.id.cart_specification, item.specification);
            }
        };
        return mCommonAdapter;
    }

    /**
     * 订单列表
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getOrderDeliverAdapter(final Context context, List<DemoOrderBean> areaCodeBeans, OrderItemClick itemClick, OrderItemButtonClick itemButton) {
        mCommonAdapter = new CommonAdapter<DemoOrderBean>(context, R.layout.item_order, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DemoOrderBean item, final int position) {
                holder.setText(R.id.goods_all_num, item.orderNum);
                holder.setText(R.id.goods_all_price, item.orderPrice);
                TextView view = holder.getView(R.id.goods_remind_shipment);//提醒发货
                TextView view2 = holder.getView(R.id.goods_cancel_order);//取消订单
                TextView view3 = holder.getView(R.id.goods_confirm_receipt);//确认收货
                TextView view4 = holder.getView(R.id.goods_change_address);//修改地址
                TextView view5 = holder.getView(R.id.goods_pay);//支付
                TextView view6 = holder.getView(R.id.goods_confirm_finish);//已完成
                holder.setOnClickListener(R.id.goods_remind_shipment, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemButton.onRemindShipmentClick(position);
                    }
                });
                holder.setOnClickListener(R.id.goods_cancel_order, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemButton.onCancelOrderClick(position);
                    }
                });
                holder.setOnClickListener(R.id.goods_confirm_receipt, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemButton.onConfirmReceiptClick(position);
                    }
                });
                holder.setOnClickListener(R.id.goods_change_address, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemButton.onChangeAddressClick(position);
                    }
                });
                holder.setOnClickListener(R.id.goods_pay, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemButton.onPayClick(position);
                    }
                });
                if (item.orderType == 1) {
                    view2.setVisibility(View.VISIBLE);
                    view5.setVisibility(View.VISIBLE);
                    view4.setVisibility(View.VISIBLE);

                } else if (item.orderType == 2) {
                    view.setVisibility(View.VISIBLE);
                } else if (item.orderType == 3) {
                    view3.setVisibility(View.VISIBLE);
                } else if (item.orderType == 4) {
                    view6.setVisibility(View.VISIBLE);
                }
                RecyclerView goods_recycle = holder.getView(R.id.goods_recycle);
                goods_recycle.setLayoutManager(new LinearLayoutManager(context));
                CommonAdapter dealSellAdapter = AdapterManger.getOrderGoodsAdapter(context, item.goods, itemClick);
                goods_recycle.setAdapter(dealSellAdapter);
                holder.setOnClickListener(R.id.order_item, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClick.onItemClick(position);
                    }
                });
            }
        };
        return mCommonAdapter;
    }

    /**
     * 订单中商品列表
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getOrderGoodsAdapter(final Context context, List<DemoOrderBean.GoodBean> areaCodeBeans, OrderItemClick itemClick) {
        mCommonAdapter = new CommonAdapter<DemoOrderBean.GoodBean>(context, R.layout.item_order_goods, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DemoOrderBean.GoodBean item, final int position) {
                ImageView view = holder.getView(R.id.goods_img);
                Glide.with(context).load(item.img).into(view);
                holder.setText(R.id.goods_name, item.name);
                holder.setText(R.id.goods_num, "X" + item.num);
                holder.setText(R.id.goods_specification, item.specification);
                holder.setText(R.id.goods_price, item.price + " USDN");
                holder.setOnClickListener(R.id.goods, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClick.onItemClick(position);
                    }
                });
            }
        };
        return mCommonAdapter;
    }

    /**
     * 订单中商品列表(待付款)
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getOrderPaymentAdapter(final Context context, List<OrderBean.OrderlistBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<OrderBean.OrderlistBean>(context, R.layout.item_order_goods, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final OrderBean.OrderlistBean item, final int position) {
                ImageView view = holder.getView(R.id.goods_img);
                Glide.with(context).load(item.smallimgurl).into(view);
                holder.setText(R.id.goods_name, item.name);
                holder.setText(R.id.goods_num, "X" + item.number);
                holder.setText(R.id.goods_specification, item.typename);
                holder.setText(R.id.goods_price, item.hyprice + " USDN");
//                holder.setOnClickListener(R.id.goods, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
////                        itemClick.onItemClick(position);
//                    }
//                });
            }
        };
        return mCommonAdapter;
    }

    /**
     * 订单中商品列表(待发货)
     *
     * @param context
     * @param areaCodeBeans
     * @return
     */
    public static CommonAdapter getOrderDeliverAdapter(final Context context, List<OrderBean.OrderlistBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<OrderBean.OrderlistBean>(context, R.layout.item_order_goods, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final OrderBean.OrderlistBean item, final int position) {
                ImageView view = holder.getView(R.id.goods_img);
                Glide.with(context).load(item.smallimgurl).into(view);
                holder.setText(R.id.goods_name, item.name);
                holder.setText(R.id.goods_num, "X" + item.number);
                holder.setText(R.id.goods_specification, item.typename);
                holder.setText(R.id.goods_price, item.hyprice + " USDN");
            }
        };
        return mCommonAdapter;
    }

    /**************************************期权相关****************************************/

    //期权列表
    public static CommonAdapter getOptionsListAdapter(final Context context, List<DemoOptionsBean> areaCodeBeans, OptionsItemClick optionsItemClick) {
        mCommonAdapter = new CommonAdapter<DemoOptionsBean>(context, R.layout.item_options, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DemoOptionsBean item, final int position) {
                ImageView view = holder.getView(R.id.currency_img);
                Glide.with(context).load(item.img).into(view);
                holder.setText(R.id.currency_name, item.name);
                holder.setText(R.id.currency_unit, item.unit);
                holder.setText(R.id.currency_hour_num, item.hour_num);
                holder.setText(R.id.currency_price, item.price);

                holder.setText(R.id.currency_num, item.num);
                holder.setOnClickListener(R.id.ll_options, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        optionsItemClick.onItemClick(position);
                    }
                });
                if (item.type == 0) {
                    holder.setTextColor(R.id.currency_num, context.getResources().getColor(R.color.color_d8dadd));
                    holder.setBackgroundRes(R.id.currency_extent, context.getResources().getDrawable(R.mipmap.options_item_button_gray));
                    holder.setText(R.id.currency_extent, item.extent + "%");
                } else if (item.type == 1) {
                    holder.setTextColor(R.id.currency_num, context.getResources().getColor(R.color.color_cco628));
                    holder.setBackgroundRes(R.id.currency_extent, context.getResources().getDrawable(R.mipmap.options_item_button_red));
                    holder.setText(R.id.currency_extent, "-" + item.extent + "%");
                } else if (item.type == 2) {
                    holder.setTextColor(R.id.currency_num, context.getResources().getColor(R.color.color_04a387));
                    holder.setBackgroundRes(R.id.currency_extent, context.getResources().getDrawable(R.mipmap.options_item_button_green));
                    holder.setText(R.id.currency_extent, "+" + item.extent + "%");
                }

            }
        };
        return mCommonAdapter;
    }

    //币种图片列表
    public static CommonAdapter getOptionsBiListAdapter(final Context context, List<DemoOptionsBean> areaCodeBeans, BiListItemClick biItemClick) {
        mCommonAdapter = new CommonAdapter<DemoOptionsBean>(context, R.layout.item_options_bi, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DemoOptionsBean item, final int position) {
                ImageView view = holder.getView(R.id.imgs);
                Glide.with(context).load(item.img).into(view);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (biItemClick != null) {
                            biItemClick.onBiItemClick(position);
                        }
                    }
                });
            }
        };
        return mCommonAdapter;
    }

    //更多kline时间列表
    public static CommonAdapter getOptionsMoreListAdapter(final Context context, List<String> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<String>(context, R.layout.item_options_more, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final String item, final int position) {
                holder.setText(R.id.times, item);
            }
        };
        return mCommonAdapter;
    }

    //kline-持仓明细
    public static CommonAdapter getOptionsPositionListAdapter(final Context context, List<DemoOptionsBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<DemoOptionsBean>(context, R.layout.item_options_position, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DemoOptionsBean item, final int position) {
//                ImageView view = holder.getView(R.id.imgs);
//                Glide.with(context).load(item.img).into(view);
            }
        };
        return mCommonAdapter;
    }

    //kline-历史明细
    public static CommonAdapter getOptionsHistoryListAdapter(final Context context, List<DemoOptionsBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<DemoOptionsBean>(context, R.layout.item_options_history, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final DemoOptionsBean item, final int position) {
//                ImageView view = holder.getView(R.id.imgs);
//                Glide.with(context).load(item.img).into(view);
            }
        };
        return mCommonAdapter;
    }

    /**************************************资产-兑换相关****************************************/
    //兑换记录
    public static CommonAdapter getExchangeListAdapter(final Context context, List<String> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<String>(context, R.layout.item_exchange, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final String item, final int position) {
//                ImageView view = holder.getView(R.id.imgs);
//                Glide.with(context).load(item.img).into(view);
            }
        };
        return mCommonAdapter;
    }

    //债券兑换记录
    public static CommonAdapter getExchangeBondListAdapter(final Context context, List<String> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<String>(context, R.layout.item_exchange_bond, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final String item, final int position) {
//                ImageView view = holder.getView(R.id.imgs);
//                Glide.with(context).load(item.img).into(view);
            }
        };
        return mCommonAdapter;
    }

    //币种+名称列表
    public static CommonAdapter getExchangeCurrencyListAdapter(final Context context, List<ExchangeBean.CowListBean> areaCodeBeans, OnExchangeItemClick onExchangeItemClick) {
        mCommonAdapter = new CommonAdapter<ExchangeBean.CowListBean>(context, R.layout.item_currency, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final ExchangeBean.CowListBean item, final int position) {
                ImageView imgs = holder.getView(R.id.imgs);
                Glide.with(context).load(item.url).into(imgs);
                holder.setText(R.id.names, item.from_coin_name);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onExchangeItemClick != null) {
                            onExchangeItemClick.onCoinItemClick(position);
                        }
                    }
                });

            }
        };
        return mCommonAdapter;
    }

    //币种+名称列表
    public static CommonAdapter getExchangeToListAdapter(final Context context, List<ExchangeBean.CowListBean> areaCodeBeans, OnExchangeItemClick onExchangeItemClick) {
        mCommonAdapter = new CommonAdapter<ExchangeBean.CowListBean>(context, R.layout.item_currency, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final ExchangeBean.CowListBean item, final int position) {
                ImageView imgs = holder.getView(R.id.imgs);
                Glide.with(context).load(item.url).into(imgs);
                holder.setText(R.id.names, item.to_coin_name);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onExchangeItemClick != null) {
                            onExchangeItemClick.onCoinClick(position);
                        }
                    }
                });
            }
        };
        return mCommonAdapter;
    }

    /**************************************资产分布****************************************/
    //资产分布列表
    public static CommonAdapter getAssetCurrencyListAdapter(final Context context, List<PieBean.PieDataBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<PieBean.PieDataBean>(context, R.layout.item_asset_currency, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final PieBean.PieDataBean item, final int position) {
                ImageView currency_img = holder.getView(R.id.currency_img);
                currency_img.setBackgroundColor(item.colors);

                holder.setText(R.id.currency_percentage, "(" + new BigDecimal(item.percentages).setScale(4,BigDecimal.ROUND_DOWN).toPlainString() + "%)");
                holder.setText(R.id.currency_name, item.unit);
            }
        };
        return mCommonAdapter;
    }

    public static CommonAdapter getbondRecycleListAdapter(final Context context, List<PieBean.PieDataBean> areaCodeBeans) {
        mCommonAdapter = new CommonAdapter<PieBean.PieDataBean>(context, R.layout.item_exchange_bond, areaCodeBeans) {
            @Override
            protected void convert(final ViewHolder holder, final PieBean.PieDataBean item, final int position) {
                ImageView currency_img = holder.getView(R.id.currency_img);
                currency_img.setBackgroundColor(item.colors);

                holder.setText(R.id.currency_percentage, "(" + new BigDecimal(item.percentages).setScale(4,BigDecimal.ROUND_DOWN).toPlainString() + "%)");
                holder.setText(R.id.currency_name, item.unit);
            }
        };
        return mCommonAdapter;
    }

    /**************************************节点模块****************************************/
    //算力市场
    public static CommonAdapter getHashMarketListAdapter(Context context, List<HashMarketBean> list) {
        mCommonAdapter = new CommonAdapter<HashMarketBean>(context, R.layout.node_market_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, HashMarketBean o, int position) {

            }
        };
        return mCommonAdapter;
    }

    //CPU
    public static CommonAdapter getCPUListAdapter(Context context, List<CpuNetBean> list) {
        mCommonAdapter = new CommonAdapter<CpuNetBean>(context, R.layout.node_bottom_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, CpuNetBean o, int position) {
                holder.setText(R.id.cpu, o.getName());
                holder.setImageResource(R.id.levelIv, o.getResId());
                holder.setText(R.id.speedTv, o.getSpeed());
                if (position == list.size() - 1) {
                    holder.setVisible1(R.id.iv, false);
                }
            }
        };
        return mCommonAdapter;
    }

    //交易记录-当前委托
    public static CommonAdapter getTransactionRecordListAdapter(Context context, List<TransactionRecordBean> list) {
        mCommonAdapter = new CommonAdapter<TransactionRecordBean>(context, R.layout.node_transaction_record_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, TransactionRecordBean o, int position) {

            }

        };
        return mCommonAdapter;
    }

    //交易记录-历史明细
    public static CommonAdapter getTransactionRecordHistoryListAdapter(Context context, List<TransactionRecordBean> list) {
        mCommonAdapter = new CommonAdapter<TransactionRecordBean>(context, R.layout.node_transaction_record_history_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, TransactionRecordBean o, int position) {

            }

        };
        return mCommonAdapter;
    }

    //节点-购买算力
    public static CommonAdapter getPurchaseComputingPower(Context context, List<ComputerPurchaseRecordBean.ListBean> list) {
        mCommonAdapter = new CommonAdapter<ComputerPurchaseRecordBean.ListBean>(context, R.layout.node_buy_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, ComputerPurchaseRecordBean.ListBean o, int position) {
                holder.setText(R.id.dateTv, o.getCreateTime());
                holder.setText(R.id.priceTv, o.getComPowerRatio());
                holder.setText(R.id.quantityTv, o.getNum());
                holder.setText(R.id.amountTv, o.getMoney());
            }
        };
        return mCommonAdapter;
    }

    /**************************************我的模塊****************************************/
    //我的模塊
    public static CommonAdapter getMineListAdapter(Context context, List<DataBean> list) {
        mCommonAdapter = new CommonAdapter<DataBean>(context, R.layout.mine_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, DataBean o, int position) {
                holder.setText(R.id.nameTv, o.getName());
                holder.setImageResource(R.id.iv, o.getResId());
                if (position == list.size() - 1) {
                    holder.setVisible1(R.id.lineIv, false);
                }
            }
        };
        return mCommonAdapter;
    }

    //收款币种描述
    public static CommonAdapter getCurrencyDescribeAdapter(Context context, List<CoinListBean> list) {
        mCommonAdapter = new CommonAdapter<CoinListBean>(context, R.layout.collect_describe_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, CoinListBean o, int position) {
                holder.setText(R.id.describeTv, o.getIntroduce());
                holder.setText(R.id.dateTv, context.getResources().getString(R.string.collect_describe_1)+"\n" + o.getIssueDate());
                holder.setText(R.id.quantityTv, context.getResources().getString(R.string.collect_describe_2)+"\n" + o.getCirculationTotal());
                holder.setText(R.id.addressTv, context.getResources().getString(R.string.collect_describe_3)+"\n" + o.getOfficialAddress());
                holder.setText(R.id.whitePagerTv, context.getResources().getString(R.string.collect_describe_4)+"\n" + o.getWhitePaper());
                holder.setText(R.id.queryTv, context.getResources().getString(R.string.collect_describe_5)+"\n" + o.getBlockQuery());

//                holder.setOnLongClickListener(R.id.addressTv, new View.OnLongClickListener() {
//                    @Override
//                    public boolean onLongClick(View v) {
//                        CopyUtils.CopyToClipboard(context,o.getOfficialAddress());
//                        ToastUtils.showLongToast("复制成功");
//                        return false;
//                    }
//                });
            }
        };
        return mCommonAdapter;
    }

    //我的团队-业绩列表
    public static CommonAdapter getMyTeamPerformanceListAdapter(Context context, List<MyTeamPerformanceListBean.ListBean> list) {
        mCommonAdapter = new CommonAdapter<MyTeamPerformanceListBean.ListBean>(context, R.layout.mine_my_team_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, MyTeamPerformanceListBean.ListBean o, int position) {
                holder.setText(R.id.dateTv, o.getDate());
                holder.setText(R.id.numTv, o.getNum() + "");
                holder.setText(R.id.performanceTv, o.getAchievement());
            }
        };
        return mCommonAdapter;
    }

    //我的团队-业绩列表
    public static CommonAdapter getMyTeamPerformanceListAdapter2(Context context, List<String> list) {
        mCommonAdapter = new CommonAdapter<String>(context, R.layout.mine_my_team_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, String o, int position) {
                holder.setText(R.id.dateTv, o);
//                holder.setText(R.id.numTv, o.getNum() + "");
//                holder.setText(R.id.performanceTv, o.getAchievement());
            }
        };
        return mCommonAdapter;
    }

    //我的收益-收益列表
    public static CommonAdapter getMyEarningsListAdapter(Context context, List<MyEarningsListBean.ListBean> list) {
        mCommonAdapter = new CommonAdapter<MyEarningsListBean.ListBean>(context, R.layout.mine_my_earnings_list_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, MyEarningsListBean.ListBean o, int position) {
                if (position == list.size() - 1) {
                    holder.setVisible1(R.id.bottomIv, false);
                }
                holder.setText(R.id.dateTv, o.getDatetime());
                holder.setText(R.id.amountTv, "+" + o.getMoneysum() + "SOU");

            }

        };
        return mCommonAdapter;
    }


    //设置界面
    public static CommonAdapter getSetupListAdapter(Context context, List<DataBean> list) {
        mCommonAdapter = new CommonAdapter<DataBean>(context, R.layout.mine_set_up_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, DataBean o, int position) {
                holder.setText(R.id.nameTv, o.getName());
                holder.setImageResource(R.id.iv, o.getResId());
                if (position == list.size() - 1) {
                    holder.setVisible1(R.id.lineIv, false);
                }
            }


        };
        return mCommonAdapter;
    }

    //語言列表
    public static CommonAdapter getSetupLanguageListAdapter(Context context, List<DataBean> list) {
        mCommonAdapter = new CommonAdapter<DataBean>(context, R.layout.language_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, DataBean o, int position) {
                holder.setText(R.id.languageTv, o.getName());
                holder.setImageResource(R.id.iv, o.getResId());
                if (position == list.size() - 1) {
                    holder.setVisible1(R.id.lineIv, false);
                } else {
                    holder.setVisible1(R.id.lineIv, true);
                }
                if (o.isSelect()) {
                    holder.setImageResource(R.id.selectIv, R.mipmap.language_select_iv);
                } else {
                    holder.setImageResource(R.id.selectIv, R.mipmap.language_unselect_iv);
                }
            }
        };
        return mCommonAdapter;
    }

    //获取币价列表
    public static CommonAdapter getCurrencyPriceListAdapter(Context context, List<CurrencyListBean> list) {
        mCommonAdapter = new CommonAdapter<CurrencyListBean>(context, R.layout.language_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, CurrencyListBean o, int position) {
                holder.setText(R.id.languageTv, o.getCurName());
                holder.loadNetImg(R.id.iv, o.getImgurl());
                if (position == list.size() - 1) {
                    holder.setVisible1(R.id.lineIv, false);
                } else {
                    holder.setVisible1(R.id.lineIv, true);
                }
                if (o.isSelect()) {
                    holder.setImageResource(R.id.selectIv, R.mipmap.language_select_iv);
                } else {
                    holder.setImageResource(R.id.selectIv, R.mipmap.language_unselect_iv);
                }
            }
        };
        return mCommonAdapter;
    }

    //助記詞界面
    public static CommonAdapter getMnemonicListAdapter(Context context, List<String> list) {
        mCommonAdapter = new CommonAdapter<String>(context, R.layout.create_account_mnemonic_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, String o, int position) {
                holder.setText(R.id.tv, o);
            }
        };
        return mCommonAdapter;
    }

    //助記詞點擊選擇界面
    public static CommonAdapter getMnemonicClickListAdapter(Context context, List<MnemonicBean> list) {
        mCommonAdapter = new CommonAdapter<MnemonicBean>(context, R.layout.create_account_mnemonic_item_click_layout, list) {
            @Override
            protected void convert(ViewHolder holder, MnemonicBean o, int position) {
                holder.setText(R.id.tv, o.getName());
                holder.setSelected(R.id.tv, o.isSelect());

            }

        };
        return mCommonAdapter;
    }

    //转账页面弹框列表
    public static CommonAdapter getCoinListAdapter(Context context, List<CoinListBean> list) {
        mCommonAdapter = new CommonAdapter<CoinListBean>(context, R.layout.coin_list_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, CoinListBean o, int position) {
                holder.setText(R.id.tv, o.getCoinName());
                holder.loadNetImg(R.id.iv, o.getUrl());
            }
        };
        return mCommonAdapter;
    }

    //货币界面列表适配器
    public static CommonAdapter getCurrencyListAdapter(Context context, List<com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean> list, CurrencyItemClick onClick) {
        mCommonAdapter = new CommonAdapter<com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean>(context, R.layout.assets_currency_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, com.taiyi.soul.moudles.main.assets.bean.CurrencyListBean o, int position) {
                holder.setText(R.id.coinNameTv, o.getCoinName());
                String quantity = "";
                if(o.getCoinName().equals("BTC")){
                    quantity = new BigDecimal(o.getMoneyandfor()).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                }else  if(o.getCoinName().equals("ETH")){
                    quantity = new BigDecimal(o.getMoneyandfor()).setScale(6,BigDecimal.ROUND_DOWN).toPlainString();
                }else {
                    quantity = new BigDecimal(o.getMoneyandfor()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
                }
                quantity = NumUtils.subZeroAndDot(quantity);
                holder.setText(R.id.quantityTv,quantity);

                String coin_symbol = Utils.getSpUtils().getString("coin_symbol");

                String replace = o.getMfcon().replace(coin_symbol, "");
                String s = new BigDecimal(replace).setScale(2,BigDecimal.ROUND_HALF_UP).toPlainString();
                holder.setText(R.id.valueTv, "≈"+ coin_symbol + s);
                holder.setText(R.id.coinRateTv, "≈"+ coin_symbol +new BigDecimal(o.getPrice()).setScale(2,BigDecimal.ROUND_HALF_UP).toPlainString());
                ImageView iv = holder.getView(R.id.coinIv);
                Glide.with(MyApplication.getInstance()).load(o.getUrl()).into(iv);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClick.onItemClick(position);
                    }
                });
            }
        };
        return mCommonAdapter;
    }

    //资产界面列表适配器
    public static CommonAdapter getAssetsCurrencyListAdapter(Context context, List<AssetsBean.PropertelistBean> list) {
        mCommonAdapter = new CommonAdapter<AssetsBean.PropertelistBean>(context, R.layout.assets_bottom_list_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, AssetsBean.PropertelistBean o, int position) {
                String coin_symbol = Utils.getSpUtils().getString("coin_symbol");

                if (position == list.size() - 1) {
                    holder.setVisible1(R.id.iv, false);
                }
                holder.setText(R.id.coinNameTv, o.getCoinName());
                //，BTC小数后8位，ETH小数后6位，其余币种4位。
                String quantity = "";
                if(o.getCoinName().equals("BTC")){
                     quantity = new BigDecimal(o.getMoneyandfor()).setScale(8, BigDecimal.ROUND_DOWN).toPlainString();
                }else  if(o.getCoinName().equals("ETH")){
                    quantity = new BigDecimal(o.getMoneyandfor()).setScale(6,BigDecimal.ROUND_DOWN).toPlainString();
                }else {
                    quantity = new BigDecimal(o.getMoneyandfor()).setScale(4,BigDecimal.ROUND_DOWN).toPlainString();
                }
                quantity = NumUtils.subZeroAndDot(quantity);
                holder.setText(R.id.quantityTv,quantity);

//                Log.e("mfc===",coin_symbol+"---"+o.getMfcon());

                holder.setText(R.id.valueTv, "≈" + coin_symbol + new BigDecimal(o.getMfcon().replace(coin_symbol,"")).setScale(2,BigDecimal.ROUND_HALF_UP).toPlainString());
                holder.loadNetImg(R.id.coinIv, o.getUrl());

                holder.setText(R.id.coinRateTv,"≈"+ coin_symbol +new BigDecimal( o.getPrice()).setScale(2,BigDecimal.ROUND_HALF_UP).toPlainString());
            }
        };
        return mCommonAdapter;
    }

    //货币详情界面列表适配器
    public static CommonAdapter getCurrencyDetailListAdapter(Context context, List<TestBean> list) {
        mCommonAdapter = new CommonAdapter<TestBean>(context, R.layout.assets_currency_details_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, TestBean o, int position) {

            }
        };
        return mCommonAdapter;
    }

    //获取消息列表
    public static CommonAdapter getMessageListAdapter(Context context, List<MessageListBean.ListBean> list) {
        mCommonAdapter = new CommonAdapter<MessageListBean.ListBean>(context, R.layout.mine_message_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, MessageListBean.ListBean o, int position) {
                holder.setText(R.id.contentTv, o.getAbstrac());
                holder.setText(R.id.timeTv, o.getCreateTime());
                holder.setVisible1(R.id.isOpenIv, o.getIsRead() == 1);
//                holder.setText(R.id.titleTv, o.getType() == 1 ? "系统消息" : "系统公告");
                holder.setText(R.id.titleTv, o.getTitle());
                holder.setImageResource(R.id.messageTypeIv, o.getType() == 1 ? R.mipmap.mine_message_iv : R.mipmap.mine_system_iv);
            }
        };
        return mCommonAdapter;
    }


    //首页区块浏览器列表适配器
    public static CommonAdapter getBrowserListAdapter(Context context, List<HomeBean.BrowserslistBean> list) {
        mCommonAdapter = new CommonAdapter<HomeBean.BrowserslistBean>(context, R.layout.first_browser_item_layout_new, list) {
            @Override
            protected void convert(ViewHolder holder, HomeBean.BrowserslistBean o, int position) {
                LinearLayout iv = holder.getView(R.id.ll);
                int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
                int px = DensityUtil.dip2px(context, 68);
                int width = (int) ((widthPixels - px) / 3.0f);
//                int height = width * 55 / 107;
                int height =DensityUtil.dip2px(context, 44);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
                layoutParams.rightMargin = DensityUtil.dip2px(context, 10);
                iv.setLayoutParams(layoutParams);
                ImageView iiv = holder.getView(R.id.iv);
                Glide.with(context).load(o.getIco()).into(iiv);
                TextView tv = holder.getView(R.id.tv);
                tv.setText(o.getName());

            }
        };
        return mCommonAdapter;
    }

    //首页相关机构列表适配器
    public static CommonAdapter getRelatedListAdapter(Context context, List<HomeBean.OrganizeListBean> list) {
        mCommonAdapter = new CommonAdapter<HomeBean.OrganizeListBean>(context, R.layout.first_related_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, HomeBean.OrganizeListBean o, int position) {
                int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
                int px = DensityUtil.dip2px(context, 42);
                int width = (int) ((widthPixels - px) / 4f);
//                int height =80;
//                if (position != 0) {
//                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    layoutParams.setMargins(DensityUtil.dip2px(context, 20), 0, 0, 0);
//                    holder.getConvertView().setLayoutParams(layoutParams);
//                } else {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0, 0, 0, 0);
                holder.getConvertView().setLayoutParams(layoutParams);
                holder.loadNetImg(R.id.iv, o.getIco());
                holder.setText(R.id.tv, o.getName());
//                }
            }


        };
        return mCommonAdapter;
    }


    //首页新闻列表适配器
    public static CommonAdapter getFirstNewsListAdapter(Context context, List<HomeBean.TwoNewsBean> list) {
        mCommonAdapter = new CommonAdapter<HomeBean.TwoNewsBean>(context, R.layout.home_news_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, HomeBean.TwoNewsBean o, int position) {
                if (position == list.size() - 1)
                    holder.setVisible1(R.id.bottomIv, false);
                holder.setText(R.id.titleTv, o.getOutline());
                String[] s = o.getCreateTime().split(" ");
                holder.setText(R.id.dateTv, s[0]);
                holder.loadNetImg(R.id.iv, o.getPictureList());
            }
        };
        return mCommonAdapter;
    }


    //首页新闻列表适配器
    public static CommonAdapter getLatestNewsListAdapter(Context context, List<NewsListBean.PageVOBean.ListBean> list) {
        mCommonAdapter = new CommonAdapter<NewsListBean.PageVOBean.ListBean>(context, R.layout.latest_news_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, NewsListBean.PageVOBean.ListBean o, int position) {
                holder.loadNetImg(R.id.iv, o.getPictureList());
                holder.setText(R.id.titleTv, o.getTitle());
                holder.setText(R.id.contentTv, o.getOutline());
                String[] s = o.getCreateTime().split(" ");
                String[] split = s[1].split(":");
                int hour = 0;
                if (Integer.parseInt(split[0])>12) {
                     hour = Integer.parseInt(split[0]) - 12;
                    holder.setText(R.id.timeTv,hour+":"+split[1]+" "+"PM");
                }else {
                    holder.setText(R.id.timeTv,split[0]+":"+split[1]+" "+"AM");
                }

            }
        };
        return mCommonAdapter;
    }


    //地址页省市县列表
    public static CommonAdapter getPopupListAdapter(Context context, List<AreaBean> list) {
        mCommonAdapter = new CommonAdapter<AreaBean>(context, R.layout.item_tv_layout, list) {
            @Override
            protected void convert(ViewHolder holder, AreaBean o, int position) {
                holder.setText(R.id.tv, o.getArea_name());
            }
        };
        return mCommonAdapter;
    }

    //收货地址列表
    public static CommonAdapter getShippingAddressListAdapter(Activity activity, Context context, List<AddressListBean.AddresslistBean> list, int type) {
        mCommonAdapter = new CommonAdapter<AddressListBean.AddresslistBean>(context, R.layout.mine_shipping_address_item_layout, list) {
            @Override
            protected void convert(ViewHolder holder, AddressListBean.AddresslistBean o, int position) {
                holder.setText(R.id.nameTv, o.getSurname() + o.getName());
                holder.setText(R.id.phoneNumberTv, o.getTel());
                String countryName,provincename,cityName,address;
                if(null==o.getCountryName()){
                    countryName = "";
                }  else {
                    countryName = o.getCountryName();
                }
                if(null==o.getProvincename()){
                    provincename = "";
                }   else {
                    provincename = o.getProvincename();
                }
                if(null==o.getCityName()){
                    cityName = "";
                }   else {
                    cityName = o.getCityName();
                }
                if(null==o.getAddress()){
                    address = "";
                } else {
                    address = o.getAddress();
                }
                holder.setText(R.id.addressTv,  countryName +provincename + cityName + address);

                if (o.getType().equals("0")) {//非默认
                    holder.setVisible(R.id.defaultIv,false);
                    holder.setVisible(R.id.editIv,false);
                    holder.setVisible(R.id.editNIv,true);
                }else {
                    holder.setVisible(R.id.defaultIv,true);
                    holder.setVisible(R.id.editIv,true);
                    holder.setVisible(R.id.editNIv,false);
                }
                holder.setOnClickListener(R.id.editIv, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        bundle.putString("addressId", String.valueOf(o.getUseraddress_id()));
                        ActivityUtils.next(activity, AddShippingAddressActivity.class, bundle);
                    }
                });
                holder.setOnClickListener(R.id.editNIv, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        bundle.putString("addressId", String.valueOf(o.getUseraddress_id()));
                        ActivityUtils.next(activity, AddShippingAddressActivity.class, bundle);
                    }
                });
                if (type == 1) {
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.putExtra("addressId", o.getUseraddress_id() + "");
                            intent.putExtra("name", o.getName());
                            intent.putExtra("phone", o.getTel());
//                            intent.putExtra("address", o.getProvincename() + o.getCityName() + o.getAddress());
                            intent.putExtra("address", countryName +provincename + cityName + address);
                            activity.setResult(1001, intent);
                            activity.finish();
                        }
                    });
                }

            }
        };
        return mCommonAdapter;
    }

    /**
     * 购买期权弹出框时间
     *
     * @param context
     * @param
     * @return
     */
    public static CommonAdapter getOptionsTimeAdapter(final Context context, List<OptionPopupBean._$1Bean> beans, OnOptionsTimeClick itemClick) {

        mCommonAdapter = new CommonAdapter<OptionPopupBean._$1Bean>(context, R.layout.item_popup_times, beans) {
            @Override
            protected void convert(final ViewHolder holder, final OptionPopupBean._$1Bean item, final int position) {
                int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
                int px = DensityUtil.dip2px(context, 25);
                int width = (int) ((widthPixels - px) / 3.5f);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
                holder.getConvertView().setLayoutParams(layoutParams);

                holder.setText(R.id.item_time, item.time);
                BigDecimal bigDecimal = new BigDecimal(item.percentage);

                holder.setText(R.id.item_profit, (bigDecimal.multiply(new BigDecimal(100)).setScale(2)).toPlainString() + "%");

                if (item.select == 2) {
                    holder.setTextColor(R.id.tv_unit, context.getResources().getColor(R.color.white));
                    holder.setTextColor(R.id.item_time, context.getResources().getColor(R.color.white));
                    holder.setTextColor(R.id.tv_profit, context.getResources().getColor(R.color.color_b7957f));
                    holder.setTextColor(R.id.item_profit, context.getResources().getColor(R.color.color_b7957f));
                    holder.setBackgroundResource(R.id.bg, R.mipmap.popup_options_order_time_bg_two);
                } else if (item.select == 1) {
                    holder.setTextColor(R.id.tv_unit, context.getResources().getColor(R.color.color_2b3137));
                    holder.setTextColor(R.id.item_time, context.getResources().getColor(R.color.color_2b3137));
                    holder.setTextColor(R.id.tv_profit, context.getResources().getColor(R.color.color_d7d9dd));
                    holder.setTextColor(R.id.item_profit, context.getResources().getColor(R.color.color_d7d9dd));
                    holder.setBackgroundResource(R.id.bg, R.mipmap.popup_options_order_time_bg);
                }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClick.onTimeSelect(position, item.id);
                    }
                });
            }
        };
        return mCommonAdapter;
    }

    /**
     * 购买期权弹出框币种选择
     *
     * @param context
     * @param
     * @return
     */
    public static CommonAdapter getOptionsBiAdapter(final Context context, List<OptionPopupBean._$3Bean> beans) {

        mCommonAdapter = new CommonAdapter<OptionPopupBean._$3Bean>(context, R.layout.item_popup_bi, beans) {
            @Override
            protected void convert(final ViewHolder holder, final OptionPopupBean._$3Bean item, final int position) {
                ImageView view = holder.getView(R.id.imgs);
                TextView bi = holder.getView(R.id.bi);
                Glide.with(context).load(item.url).into(view);
                bi.setText(item.coin_name);

            }
        };
        return mCommonAdapter;
    }

    public interface OnOptionsTimeClick {
        void onTimeSelect(int position, String coin_name);
    }

    public interface BiListItemClick {
        void onBiItemClick(int position);
    }


    public interface OnItemClick {

        void onCopy(String value);

        void onSelect(int position);
    }

    public interface OnExchangeItemClick {

        void onCoinItemClick(int position);

        void onCoinClick(int position);
    }

    //期权列表item点击事件
    public interface OptionsItemClick {
        void onItemClick(int position);
    }

    //商城热销商品列表item点击事件
    public interface GoodsHotItemClick {
        void onItemClick(int position, String commodityid);
    }

    //买卖单点击
    public interface DealItemClick {
        void onDeal(int position);
    }

    //币种item点击
    public interface CurrencyItemClick {
        void onItemClick(int position);
    }
    //订单列表item点击
    public interface OrderItemClick {
        void onItemClick(int position);
    }

    //订单列表item下按钮点击
    public interface OrderItemButtonClick {
        void onRemindShipmentClick(int position);//确认收货

        void onCancelOrderClick(int position);//取消订单

        void onConfirmReceiptClick(int position);//确认收货

        void onChangeAddressClick(int position);//修改地址

        void onPayClick(int position);//支付
    }

}
