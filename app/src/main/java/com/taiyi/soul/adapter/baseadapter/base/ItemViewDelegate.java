package com.taiyi.soul.adapter.baseadapter.base;


/**
 * Created by NGK on 2020/4/23.
 */
public interface ItemViewDelegate<T>
{

    int getItemViewLayoutId();

    boolean isForViewType(T item, int position);

    void convert(ViewHolder holder, T t, int position);

}
