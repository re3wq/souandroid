// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.setup.currencydisplay;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CurrencyDisplayActivity_ViewBinding implements Unbinder {
  private CurrencyDisplayActivity target;

  private View view7f0a0574;

  private View view7f0a009f;

  @UiThread
  public CurrencyDisplayActivity_ViewBinding(CurrencyDisplayActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CurrencyDisplayActivity_ViewBinding(final CurrencyDisplayActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.usDollarTv, "field 'usDollarTv' and method 'onViewClicked'");
    target.usDollarTv = Utils.castView(view, R.id.usDollarTv, "field 'usDollarTv'", TextView.class);
    view7f0a0574 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rightTitleTv = Utils.findRequiredViewAsType(source, R.id.rightTitleTv, "field 'rightTitleTv'", TextView.class);
    target.selectCurrencyTv = Utils.findRequiredViewAsType(source, R.id.selectCurrencyTv, "field 'selectCurrencyTv'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CurrencyDisplayActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.usDollarTv = null;
    target.rightTitleTv = null;
    target.selectCurrencyTv = null;
    target.recyclerView = null;
    target.top = null;

    view7f0a0574.setOnClickListener(null);
    view7f0a0574 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
