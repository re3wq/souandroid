// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node.purchasehashrate;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import com.taiyi.soul.view.CountView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PurchaseHashrateActivity_ViewBinding implements Unbinder {
  private PurchaseHashrateActivity target;

  private View view7f0a009f;

  private View view7f0a00f6;

  @UiThread
  public PurchaseHashrateActivity_ViewBinding(PurchaseHashrateActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PurchaseHashrateActivity_ViewBinding(final PurchaseHashrateActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.computingPowerPriceTv = Utils.findRequiredViewAsType(source, R.id.computingPowerPriceTv, "field 'computingPowerPriceTv'", TextView.class);
    target.amountTv = Utils.findRequiredViewAsType(source, R.id.amountTv, "field 'amountTv'", CountView.class);
    target.inputQuantityEt = Utils.findRequiredViewAsType(source, R.id.inputQuantityEt, "field 'inputQuantityEt'", EditText.class);
    target.balanceTv = Utils.findRequiredViewAsType(source, R.id.balanceTv, "field 'balanceTv'", TextView.class);
    target.totalPaymentTv = Utils.findRequiredViewAsType(source, R.id.totalPaymentTv, "field 'totalPaymentTv'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.noDataLl = Utils.findRequiredViewAsType(source, R.id.noDataLl, "field 'noDataLl'", LinearLayout.class);
    target.key_main = Utils.findRequiredViewAsType(source, R.id.key_main, "field 'key_main'", RelativeLayout.class);
    target.key_scroll = Utils.findRequiredViewAsType(source, R.id.key_scroll, "field 'key_scroll'", LinearLayout.class);
    target.keyboardPlace = Utils.findRequiredViewAsType(source, R.id.keyboardPlace, "field 'keyboardPlace'", LinearLayout.class);
    target.springView = Utils.findRequiredViewAsType(source, R.id.spring_view, "field 'springView'", SpringView.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.buyTv, "method 'onViewClicked'");
    view7f0a00f6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PurchaseHashrateActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.computingPowerPriceTv = null;
    target.amountTv = null;
    target.inputQuantityEt = null;
    target.balanceTv = null;
    target.totalPaymentTv = null;
    target.recyclerView = null;
    target.top = null;
    target.noDataLl = null;
    target.key_main = null;
    target.key_scroll = null;
    target.keyboardPlace = null;
    target.springView = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a00f6.setOnClickListener(null);
    view7f0a00f6 = null;
  }
}
