// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.goods.fragment;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MallTwoFragment_ViewBinding implements Unbinder {
  private MallTwoFragment target;

  @UiThread
  public MallTwoFragment_ViewBinding(MallTwoFragment target, View source) {
    this.target = target;

    target.mall_more_pull = Utils.findRequiredViewAsType(source, R.id.mall_more_pull, "field 'mall_more_pull'", PullRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MallTwoFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mall_more_pull = null;
  }
}
