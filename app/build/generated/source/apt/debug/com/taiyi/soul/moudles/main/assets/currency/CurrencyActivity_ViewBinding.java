// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.currency;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CurrencyActivity_ViewBinding implements Unbinder {
  private CurrencyActivity target;

  private View view7f0a009f;

  @UiThread
  public CurrencyActivity_ViewBinding(CurrencyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CurrencyActivity_ViewBinding(final CurrencyActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.pull_recycle = Utils.findRequiredViewAsType(source, R.id.pull_recycle, "field 'pull_recycle'", PullRecyclerView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CurrencyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.pull_recycle = null;
    target.top = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
