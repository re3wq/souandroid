// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.securitycenter;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SecurityCenterActivity_ViewBinding implements Unbinder {
  private SecurityCenterActivity target;

  private View view7f0a0270;

  private View view7f0a026f;

  private View view7f0a026e;

  private View view7f0a009f;

  private View view7f0a00c3;

  private View view7f0a00c1;

  private View view7f0a0572;

  private View view7f0a0571;

  private View view7f0a0205;

  private View view7f0a0234;

  @UiThread
  public SecurityCenterActivity_ViewBinding(SecurityCenterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SecurityCenterActivity_ViewBinding(final SecurityCenterActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.phoneNumberTv = Utils.findRequiredViewAsType(source, R.id.phoneNumberTv, "field 'phoneNumberTv'", TextView.class);
    target.emailTv = Utils.findRequiredViewAsType(source, R.id.emailTv, "field 'emailTv'", TextView.class);
    target.levelTv = Utils.findRequiredViewAsType(source, R.id.levelTv, "field 'levelTv'", TextView.class);
    target.bindPhoneNumberTv = Utils.findRequiredViewAsType(source, R.id.bindPhoneNumberTv, "field 'bindPhoneNumberTv'", TextView.class);
    target.bindEmailTv = Utils.findRequiredViewAsType(source, R.id.bindEmailTv, "field 'bindEmailTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.isOpenGoogleIv, "field 'isOpenGoogleIv' and method 'onViewClicked'");
    target.isOpenGoogleIv = Utils.castView(view, R.id.isOpenGoogleIv, "field 'isOpenGoogleIv'", ImageView.class);
    view7f0a0270 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.starThreeIv = Utils.findRequiredViewAsType(source, R.id.starThreeIv, "field 'starThreeIv'", ImageView.class);
    target.starFourIv = Utils.findRequiredViewAsType(source, R.id.starFourIv, "field 'starFourIv'", ImageView.class);
    target.starFiveIv = Utils.findRequiredViewAsType(source, R.id.starFiveIv, "field 'starFiveIv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.isOpenFingerprintIv, "field 'isOpenFingerprintIv' and method 'onViewClicked'");
    target.isOpenFingerprintIv = Utils.castView(view, R.id.isOpenFingerprintIv, "field 'isOpenFingerprintIv'", ImageView.class);
    view7f0a026f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.isOpenFaceIv, "field 'isOpenFaceIv' and method 'onViewClicked'");
    target.isOpenFaceIv = Utils.castView(view, R.id.isOpenFaceIv, "field 'isOpenFaceIv'", ImageView.class);
    view7f0a026e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.bindPhoneNumberRl, "method 'onViewClicked'");
    view7f0a00c3 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.bindEmailRl, "method 'onViewClicked'");
    view7f0a00c1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.updateTransactionPwdRl, "method 'onViewClicked'");
    view7f0a0572 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.updateLoginPwdRl, "method 'onViewClicked'");
    view7f0a0571 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.fingerprintRl, "method 'onViewClicked'");
    view7f0a0205 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.googleVerificationRl, "method 'onViewClicked'");
    view7f0a0234 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SecurityCenterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.phoneNumberTv = null;
    target.emailTv = null;
    target.levelTv = null;
    target.bindPhoneNumberTv = null;
    target.bindEmailTv = null;
    target.isOpenGoogleIv = null;
    target.starThreeIv = null;
    target.starFourIv = null;
    target.starFiveIv = null;
    target.isOpenFingerprintIv = null;
    target.isOpenFaceIv = null;
    target.progressBar = null;
    target.top = null;

    view7f0a0270.setOnClickListener(null);
    view7f0a0270 = null;
    view7f0a026f.setOnClickListener(null);
    view7f0a026f = null;
    view7f0a026e.setOnClickListener(null);
    view7f0a026e = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a00c3.setOnClickListener(null);
    view7f0a00c3 = null;
    view7f0a00c1.setOnClickListener(null);
    view7f0a00c1 = null;
    view7f0a0572.setOnClickListener(null);
    view7f0a0572 = null;
    view7f0a0571.setOnClickListener(null);
    view7f0a0571 = null;
    view7f0a0205.setOnClickListener(null);
    view7f0a0205 = null;
    view7f0a0234.setOnClickListener(null);
    view7f0a0234 = null;
  }
}
