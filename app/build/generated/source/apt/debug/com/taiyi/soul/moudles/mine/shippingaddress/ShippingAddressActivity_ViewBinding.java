// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.shippingaddress;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ShippingAddressActivity_ViewBinding implements Unbinder {
  private ShippingAddressActivity target;

  private View view7f0a009f;

  private View view7f0a035b;

  @UiThread
  public ShippingAddressActivity_ViewBinding(ShippingAddressActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ShippingAddressActivity_ViewBinding(final ShippingAddressActivity target, View source) {
    this.target = target;

    View view;
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.noDataLl = Utils.findRequiredViewAsType(source, R.id.noDataLl, "field 'noDataLl'", LinearLayout.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.springView = Utils.findRequiredViewAsType(source, R.id.springView, "field 'springView'", SpringView.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.newShippingAddressTv, "method 'onViewClicked'");
    view7f0a035b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ShippingAddressActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.top = null;
    target.titleTv = null;
    target.noDataLl = null;
    target.recyclerView = null;
    target.springView = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a035b.setOnClickListener(null);
    view7f0a035b = null;
  }
}
