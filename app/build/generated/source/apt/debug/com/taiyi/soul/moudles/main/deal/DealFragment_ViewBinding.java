// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.deal;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DealFragment_ViewBinding implements Unbinder {
  private DealFragment target;

  private View view7f0a0183;

  private View view7f0a0184;

  @UiThread
  public DealFragment_ViewBinding(final DealFragment target, View source) {
    this.target = target;

    View view;
    target.mDealTab = Utils.findRequiredViewAsType(source, R.id.deal_tab, "field 'mDealTab'", SlidingTabLayout.class);
    target.mDealViewpager = Utils.findRequiredViewAsType(source, R.id.deal_viewpager, "field 'mDealViewpager'", ViewPager.class);
    view = Utils.findRequiredView(source, R.id.deal_entrust, "method 'onViewClicked'");
    view7f0a0183 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.deal_issue, "method 'onViewClicked'");
    view7f0a0184 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DealFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mDealTab = null;
    target.mDealViewpager = null;

    view7f0a0183.setOnClickListener(null);
    view7f0a0183 = null;
    view7f0a0184.setOnClickListener(null);
    view7f0a0184 = null;
  }
}
