// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.accountmanagement;

import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AccountManagementActivity_ViewBinding implements Unbinder {
  private AccountManagementActivity target;

  private View view7f0a041a;

  private View view7f0a009f;

  @UiThread
  public AccountManagementActivity_ViewBinding(AccountManagementActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AccountManagementActivity_ViewBinding(final AccountManagementActivity target,
      View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.rightTitleTv = Utils.findRequiredViewAsType(source, R.id.rightTitleTv, "field 'rightTitleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rightIv, "field 'rightIv' and method 'onViewClicked'");
    target.rightIv = Utils.castView(view, R.id.rightIv, "field 'rightIv'", ImageView.class);
    view7f0a041a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.lv = Utils.findRequiredViewAsType(source, R.id.lv, "field 'lv'", ListView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AccountManagementActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.rightTitleTv = null;
    target.rightIv = null;
    target.lv = null;
    target.top = null;

    view7f0a041a.setOnClickListener(null);
    view7f0a041a = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
