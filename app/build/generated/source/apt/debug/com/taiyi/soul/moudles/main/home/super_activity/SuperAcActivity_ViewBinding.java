// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.super_activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.taiyi.soul.moudles.main.home.MyCircleProcessView;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SuperAcActivity_ViewBinding implements Unbinder {
  private SuperAcActivity target;

  private View view7f0a0253;

  private View view7f0a025e;

  private View view7f0a028e;

  private View view7f0a04a7;

  @UiThread
  public SuperAcActivity_ViewBinding(SuperAcActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SuperAcActivity_ViewBinding(final SuperAcActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    target.tv_type = Utils.findRequiredViewAsType(source, R.id.tv_type, "field 'tv_type'", TextView.class);
    target.tv1 = Utils.findRequiredViewAsType(source, R.id.tv1, "field 'tv1'", TextView.class);
    target.totalTv = Utils.findRequiredViewAsType(source, R.id.totalTv, "field 'totalTv'", TextView.class);
    target.teamNumTv = Utils.findRequiredViewAsType(source, R.id.teamNumTv, "field 'teamNumTv'", TextView.class);
    target.winningRateTv = Utils.findRequiredViewAsType(source, R.id.winningRateTv, "field 'winningRateTv'", TextView.class);
    target.computingPowerValueTv = Utils.findRequiredViewAsType(source, R.id.computingPowerValueTv, "field 'computingPowerValueTv'", NumberRollingView.class);
    target.computingPowerTv = Utils.findRequiredViewAsType(source, R.id.computingPowerTv, "field 'computingPowerTv'", NumberRollingView.class);
    target.myAssetsTv = Utils.findRequiredViewAsType(source, R.id.myAssetsTv, "field 'myAssetsTv'", NumberRollingView.class);
    target.assetsValueTv = Utils.findRequiredViewAsType(source, R.id.assetsValueTv, "field 'assetsValueTv'", NumberRollingView.class);
    target.countView_price = Utils.findRequiredViewAsType(source, R.id.countView_price, "field 'countView_price'", NumberRollingView.class);
    target.countView_price_bond = Utils.findRequiredViewAsType(source, R.id.countView_price_bond, "field 'countView_price_bond'", NumberRollingView.class);
    target.cv1 = Utils.findRequiredViewAsType(source, R.id.cv1, "field 'cv1'", NumberRollingView.class);
    target.cv2 = Utils.findRequiredViewAsType(source, R.id.cv2, "field 'cv2'", NumberRollingView.class);
    target.mProgressView1 = Utils.findRequiredViewAsType(source, R.id.progress_day, "field 'mProgressView1'", MyCircleProcessView.class);
    target.mProgressView2 = Utils.findRequiredViewAsType(source, R.id.progress_hour, "field 'mProgressView2'", MyCircleProcessView.class);
    target.mProgressView3 = Utils.findRequiredViewAsType(source, R.id.progress_minutes, "field 'mProgressView3'", MyCircleProcessView.class);
    target.mProgressView4 = Utils.findRequiredViewAsType(source, R.id.progress_second, "field 'mProgressView4'", MyCircleProcessView.class);
    target.text_second = Utils.findRequiredViewAsType(source, R.id.text_second, "field 'text_second'", TextView.class);
    target.text_minutes = Utils.findRequiredViewAsType(source, R.id.text_minutes, "field 'text_minutes'", TextView.class);
    target.text_hour = Utils.findRequiredViewAsType(source, R.id.text_hour, "field 'text_hour'", TextView.class);
    target.text_day = Utils.findRequiredViewAsType(source, R.id.text_day, "field 'text_day'", TextView.class);
    view = Utils.findRequiredView(source, R.id.image, "field 'image' and method 'onViewClicked'");
    target.image = Utils.castView(view, R.id.image, "field 'image'", ImageView.class);
    view7f0a0253 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_right, "field 'img_right' and method 'onViewClicked'");
    target.img_right = Utils.castView(view, R.id.img_right, "field 'img_right'", ImageView.class);
    view7f0a025e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.super_ac_buy, "method 'onViewClicked'");
    view7f0a04a7 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SuperAcActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.tv_type = null;
    target.tv1 = null;
    target.totalTv = null;
    target.teamNumTv = null;
    target.winningRateTv = null;
    target.computingPowerValueTv = null;
    target.computingPowerTv = null;
    target.myAssetsTv = null;
    target.assetsValueTv = null;
    target.countView_price = null;
    target.countView_price_bond = null;
    target.cv1 = null;
    target.cv2 = null;
    target.mProgressView1 = null;
    target.mProgressView2 = null;
    target.mProgressView3 = null;
    target.mProgressView4 = null;
    target.text_second = null;
    target.text_minutes = null;
    target.text_hour = null;
    target.text_day = null;
    target.image = null;
    target.img_right = null;

    view7f0a0253.setOnClickListener(null);
    view7f0a0253 = null;
    view7f0a025e.setOnClickListener(null);
    view7f0a025e = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a04a7.setOnClickListener(null);
    view7f0a04a7 = null;
  }
}
