// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.feedback;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FeedbackActivity_ViewBinding implements Unbinder {
  private FeedbackActivity target;

  private View view7f0a041b;

  private View view7f0a02e7;

  private View view7f0a02ea;

  private View view7f0a02e9;

  private View view7f0a009f;

  private View view7f0a013a;

  @UiThread
  public FeedbackActivity_ViewBinding(FeedbackActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FeedbackActivity_ViewBinding(final FeedbackActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rightTitleTv, "field 'rightTitleTv' and method 'onViewClicked'");
    target.rightTitleTv = Utils.castView(view, R.id.rightTitleTv, "field 'rightTitleTv'", TextView.class);
    view7f0a041b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.todaySendTv = Utils.findRequiredViewAsType(source, R.id.todaySendTv, "field 'todaySendTv'", TextView.class);
    target.typeOneTv = Utils.findRequiredViewAsType(source, R.id.typeOneTv, "field 'typeOneTv'", TextView.class);
    target.typeTwoTv = Utils.findRequiredViewAsType(source, R.id.typeTwoTv, "field 'typeTwoTv'", TextView.class);
    target.typeThreeTv = Utils.findRequiredViewAsType(source, R.id.typeThreeTv, "field 'typeThreeTv'", TextView.class);
    target.contentEt = Utils.findRequiredViewAsType(source, R.id.contentEt, "field 'contentEt'", EditText.class);
    target.typeOneIv = Utils.findRequiredViewAsType(source, R.id.typeOneIv, "field 'typeOneIv'", ImageView.class);
    target.typeTwoIv = Utils.findRequiredViewAsType(source, R.id.typeTwoIv, "field 'typeTwoIv'", ImageView.class);
    target.typeThreeIv = Utils.findRequiredViewAsType(source, R.id.typeThreeIv, "field 'typeThreeIv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.llOne, "field 'llOne' and method 'onViewClicked'");
    target.llOne = Utils.castView(view, R.id.llOne, "field 'llOne'", LinearLayout.class);
    view7f0a02e7 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llTwo, "field 'llTwo' and method 'onViewClicked'");
    target.llTwo = Utils.castView(view, R.id.llTwo, "field 'llTwo'", LinearLayout.class);
    view7f0a02ea = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.llThree, "field 'llThree' and method 'onViewClicked'");
    target.llThree = Utils.castView(view, R.id.llThree, "field 'llThree'", LinearLayout.class);
    view7f0a02e9 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.commitTv, "method 'onViewClicked'");
    view7f0a013a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    FeedbackActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.rightTitleTv = null;
    target.todaySendTv = null;
    target.typeOneTv = null;
    target.typeTwoTv = null;
    target.typeThreeTv = null;
    target.contentEt = null;
    target.typeOneIv = null;
    target.typeTwoIv = null;
    target.typeThreeIv = null;
    target.llOne = null;
    target.llTwo = null;
    target.llThree = null;
    target.top = null;

    view7f0a041b.setOnClickListener(null);
    view7f0a041b = null;
    view7f0a02e7.setOnClickListener(null);
    view7f0a02e7 = null;
    view7f0a02ea.setOnClickListener(null);
    view7f0a02ea = null;
    view7f0a02e9.setOnClickListener(null);
    view7f0a02e9 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a013a.setOnClickListener(null);
    view7f0a013a = null;
  }
}
