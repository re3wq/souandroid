// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.exchange;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExchangeActivity_ViewBinding implements Unbinder {
  private ExchangeActivity target;

  private View view7f0a0552;

  private View view7f0a028e;

  private View view7f0a01f2;

  private View view7f0a01f1;

  private View view7f0a01ed;

  private View view7f0a01f4;

  @UiThread
  public ExchangeActivity_ViewBinding(ExchangeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ExchangeActivity_ViewBinding(final ExchangeActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.tv_right_text, "field 'mTvRightText' and method 'onViewClicked'");
    target.mTvRightText = Utils.castView(view, R.id.tv_right_text, "field 'mTvRightText'", TextView.class);
    view7f0a0552 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mExchangeUnit = Utils.findRequiredViewAsType(source, R.id.exchange_unit, "field 'mExchangeUnit'", TextView.class);
    target.exchange_currency_rate = Utils.findRequiredViewAsType(source, R.id.exchange_currency_rate, "field 'exchange_currency_rate'", TextView.class);
    target.exchange = Utils.findRequiredViewAsType(source, R.id.exchange, "field 'exchange'", LinearLayout.class);
    target.mCoinImg = Utils.findRequiredViewAsType(source, R.id.coin_img, "field 'mCoinImg'", ImageView.class);
    target.mCoinName = Utils.findRequiredViewAsType(source, R.id.coin_name, "field 'mCoinName'", TextView.class);
    target.mCoinImgTwo = Utils.findRequiredViewAsType(source, R.id.coin_img_two, "field 'mCoinImgTwo'", ImageView.class);
    target.mCoinNameTwo = Utils.findRequiredViewAsType(source, R.id.coin_name_two, "field 'mCoinNameTwo'", TextView.class);
    target.mChangeBalance = Utils.findRequiredViewAsType(source, R.id.change_balance, "field 'mChangeBalance'", TextView.class);
    target.change_balance_unit = Utils.findRequiredViewAsType(source, R.id.change_balance_unit, "field 'change_balance_unit'", TextView.class);
    target.mChangeOutNum = Utils.findRequiredViewAsType(source, R.id.change_out_num, "field 'mChangeOutNum'", EditText.class);
    target.mChangeIntNum = Utils.findRequiredViewAsType(source, R.id.change_int_num, "field 'mChangeIntNum'", TextView.class);
    target.rate_other = Utils.findRequiredViewAsType(source, R.id.rate_other, "field 'rate_other'", TextView.class);
    target.rate = Utils.findRequiredViewAsType(source, R.id.rate, "field 'rate'", TextView.class);
    target.exchange_in_img = Utils.findRequiredViewAsType(source, R.id.exchange_in_img, "field 'exchange_in_img'", ImageView.class);
    target.exchange_out_img = Utils.findRequiredViewAsType(source, R.id.exchange_out_img, "field 'exchange_out_img'", ImageView.class);
    target.key_main = Utils.findRequiredViewAsType(source, R.id.key_main, "field 'key_main'", RelativeLayout.class);
    target.key_scroll = Utils.findRequiredViewAsType(source, R.id.key_scroll, "field 'key_scroll'", LinearLayout.class);
    target.keyboardPlace = Utils.findRequiredViewAsType(source, R.id.keyboardPlace, "field 'keyboardPlace'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.exchange_more_two, "method 'onViewClicked'");
    view7f0a01f2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.exchange_more_one, "method 'onViewClicked'");
    view7f0a01f1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.exchange_all, "method 'onViewClicked'");
    view7f0a01ed = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.exchange_rate, "method 'onViewClicked'");
    view7f0a01f4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ExchangeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mTvRightText = null;
    target.mExchangeUnit = null;
    target.exchange_currency_rate = null;
    target.exchange = null;
    target.mCoinImg = null;
    target.mCoinName = null;
    target.mCoinImgTwo = null;
    target.mCoinNameTwo = null;
    target.mChangeBalance = null;
    target.change_balance_unit = null;
    target.mChangeOutNum = null;
    target.mChangeIntNum = null;
    target.rate_other = null;
    target.rate = null;
    target.exchange_in_img = null;
    target.exchange_out_img = null;
    target.key_main = null;
    target.key_scroll = null;
    target.keyboardPlace = null;

    view7f0a0552.setOnClickListener(null);
    view7f0a0552 = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a01f2.setOnClickListener(null);
    view7f0a01f2 = null;
    view7f0a01f1.setOnClickListener(null);
    view7f0a01f1 = null;
    view7f0a01ed.setOnClickListener(null);
    view7f0a01ed = null;
    view7f0a01f4.setOnClickListener(null);
    view7f0a01f4 = null;
  }
}
