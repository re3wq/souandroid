// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.goods;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MallMoreActivity_ViewBinding implements Unbinder {
  private MallMoreActivity target;

  private View view7f0a0256;

  private View view7f0a028e;

  @UiThread
  public MallMoreActivity_ViewBinding(MallMoreActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MallMoreActivity_ViewBinding(final MallMoreActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.img_car, "field 'mImgCar' and method 'onViewClicked'");
    target.mImgCar = Utils.castView(view, R.id.img_car, "field 'mImgCar'", ImageView.class);
    view7f0a0256 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mTabLayout = Utils.findRequiredViewAsType(source, R.id.tabLayout, "field 'mTabLayout'", SlidingTabLayout.class);
    target.mViewPager = Utils.findRequiredViewAsType(source, R.id.view_pager, "field 'mViewPager'", ViewPager.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MallMoreActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mImgCar = null;
    target.mTabLayout = null;
    target.mViewPager = null;

    view7f0a0256.setOnClickListener(null);
    view7f0a0256 = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
  }
}
