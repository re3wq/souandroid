// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BindSuccessActivity_ViewBinding implements Unbinder {
  private BindSuccessActivity target;

  private View view7f0a04f2;

  private View view7f0a009f;

  @UiThread
  public BindSuccessActivity_ViewBinding(BindSuccessActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BindSuccessActivity_ViewBinding(final BindSuccessActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.toLoginTv, "field 'toLoginTv' and method 'onViewClicked'");
    target.toLoginTv = Utils.castView(view, R.id.toLoginTv, "field 'toLoginTv'", TextView.class);
    view7f0a04f2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ivPhone = Utils.findRequiredViewAsType(source, R.id.iv_phone, "field 'ivPhone'", ImageView.class);
    target.ivEmail = Utils.findRequiredViewAsType(source, R.id.iv_email, "field 'ivEmail'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    BindSuccessActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.toLoginTv = null;
    target.ivPhone = null;
    target.ivEmail = null;

    view7f0a04f2.setOnClickListener(null);
    view7f0a04f2 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
