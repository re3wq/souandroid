// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.forget;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ForgetActivity_ViewBinding implements Unbinder {
  private ForgetActivity target;

  private View view7f0a028e;

  private View view7f0a0535;

  private View view7f0a02ef;

  private View view7f0a0553;

  private View view7f0a02f0;

  private View view7f0a02fa;

  private View view7f0a0537;

  @UiThread
  public ForgetActivity_ViewBinding(ForgetActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ForgetActivity_ViewBinding(final ForgetActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.iv_back, "field 'iv_back' and method 'onViewClicked'");
    target.iv_back = Utils.castView(view, R.id.iv_back, "field 'iv_back'", ImageView.class);
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_code, "field 'tvCode' and method 'onViewClicked'");
    target.tvCode = Utils.castView(view, R.id.tv_code, "field 'tvCode'", TextView.class);
    view7f0a0535 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.iv = Utils.findRequiredViewAsType(source, R.id.iv, "field 'iv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.ll_code, "field 'llCode' and method 'onViewClicked'");
    target.llCode = Utils.castView(view, R.id.ll_code, "field 'llCode'", LinearLayout.class);
    view7f0a02ef = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ivEmail = Utils.findRequiredViewAsType(source, R.id.iv_email, "field 'ivEmail'", ImageView.class);
    target.ivPhone = Utils.findRequiredViewAsType(source, R.id.iv_phone, "field 'ivPhone'", ImageView.class);
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'etEmail'", EditText.class);
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'etPassword'", EditText.class);
    target.et_sms_code = Utils.findRequiredViewAsType(source, R.id.et_sms_code, "field 'et_sms_code'", EditText.class);
    view = Utils.findRequiredView(source, R.id.tv_send_code, "field 'tv_send_code' and method 'onViewClicked'");
    target.tv_send_code = Utils.castView(view, R.id.tv_send_code, "field 'tv_send_code'", TextView.class);
    view7f0a0553 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.llPhone = Utils.findRequiredViewAsType(source, R.id.ll_phone, "field 'llPhone'", LinearLayout.class);
    target.llHide = Utils.findRequiredViewAsType(source, R.id.ll_hide, "field 'llHide'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.ll_email, "method 'onViewClicked'");
    view7f0a02f0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ll_phone1, "method 'onViewClicked'");
    view7f0a02fa = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_confirm, "method 'onViewClicked'");
    view7f0a0537 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ForgetActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.iv_back = null;
    target.tvCode = null;
    target.iv = null;
    target.llCode = null;
    target.ivEmail = null;
    target.ivPhone = null;
    target.etPhone = null;
    target.etEmail = null;
    target.etPassword = null;
    target.et_sms_code = null;
    target.tv_send_code = null;
    target.llPhone = null;
    target.llHide = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a0535.setOnClickListener(null);
    view7f0a0535 = null;
    view7f0a02ef.setOnClickListener(null);
    view7f0a02ef = null;
    view7f0a0553.setOnClickListener(null);
    view7f0a0553 = null;
    view7f0a02f0.setOnClickListener(null);
    view7f0a02f0 = null;
    view7f0a02fa.setOnClickListener(null);
    view7f0a02fa = null;
    view7f0a0537.setOnClickListener(null);
    view7f0a0537 = null;
  }
}
