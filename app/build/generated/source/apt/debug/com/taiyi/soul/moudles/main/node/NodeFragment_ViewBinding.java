// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.mikephil.charting.charts.LineChart;
import com.taiyi.soul.R;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;
import com.taiyi.soul.view.MyLineChartView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NodeFragment_ViewBinding implements Unbinder {
  private NodeFragment target;

  private View view7f0a00f6;

  private View view7f0a0324;

  @UiThread
  public NodeFragment_ViewBinding(final NodeFragment target, View source) {
    this.target = target;

    View view;
    target.cvNode = Utils.findRequiredViewAsType(source, R.id.cv_node, "field 'cvNode'", MyLineChartView.class);
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", LinearLayout.class);
    target.topIndTv = Utils.findRequiredViewAsType(source, R.id.topIndTv, "field 'topIndTv'", TextView.class);
    target.firstLevelTv = Utils.findRequiredViewAsType(source, R.id.firstClassTv, "field 'firstLevelTv'", TextView.class);
    target.secondLevelTv = Utils.findRequiredViewAsType(source, R.id.secondLevelTv, "field 'secondLevelTv'", TextView.class);
    target.level3Tv = Utils.findRequiredViewAsType(source, R.id.level3Tv, "field 'level3Tv'", TextView.class);
    target.level4Tv = Utils.findRequiredViewAsType(source, R.id.level4Tv, "field 'level4Tv'", TextView.class);
    target.computingPowerTv = Utils.findRequiredViewAsType(source, R.id.computingPowerTv, "field 'computingPowerTv'", NumberRollingView.class);
    target.computingPowerValueTv = Utils.findRequiredViewAsType(source, R.id.computingPowerValueTv, "field 'computingPowerValueTv'", NumberRollingView.class);
    target.myAssetsTv = Utils.findRequiredViewAsType(source, R.id.myAssetsTv, "field 'myAssetsTv'", NumberRollingView.class);
    target.assetsValueTv = Utils.findRequiredViewAsType(source, R.id.assetsValueTv, "field 'assetsValueTv'", NumberRollingView.class);
    target.computerPriceTv = Utils.findRequiredViewAsType(source, R.id.computerPriceTv, "field 'computerPriceTv'", TextView.class);
    target.currentProgressIv = Utils.findRequiredViewAsType(source, R.id.currentProgressIv, "field 'currentProgressIv'", ImageView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
    target.lineChart = Utils.findRequiredViewAsType(source, R.id.lineChart, "field 'lineChart'", LineChart.class);
    view = Utils.findRequiredView(source, R.id.buyTv, "method 'onViewClick'");
    view7f0a00f6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.marketTv, "method 'onViewClick'");
    view7f0a0324 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    NodeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cvNode = null;
    target.titleTv = null;
    target.topIndTv = null;
    target.firstLevelTv = null;
    target.secondLevelTv = null;
    target.level3Tv = null;
    target.level4Tv = null;
    target.computingPowerTv = null;
    target.computingPowerValueTv = null;
    target.myAssetsTv = null;
    target.assetsValueTv = null;
    target.computerPriceTv = null;
    target.currentProgressIv = null;
    target.recyclerView = null;
    target.progressBar = null;
    target.lineChart = null;

    view7f0a00f6.setOnClickListener(null);
    view7f0a00f6 = null;
    view7f0a0324.setOnClickListener(null);
    view7f0a0324 = null;
  }
}
