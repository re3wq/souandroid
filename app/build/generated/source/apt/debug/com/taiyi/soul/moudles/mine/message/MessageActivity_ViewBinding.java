// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.message;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MessageActivity_ViewBinding implements Unbinder {
  private MessageActivity target;

  private View view7f0a041b;

  private View view7f0a032d;

  private View view7f0a0367;

  private View view7f0a009f;

  @UiThread
  public MessageActivity_ViewBinding(MessageActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MessageActivity_ViewBinding(final MessageActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rightTitleTv, "field 'rightTitleTv' and method 'onViewClicked'");
    target.rightTitleTv = Utils.castView(view, R.id.rightTitleTv, "field 'rightTitleTv'", TextView.class);
    view7f0a041b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.messageTimeTv = Utils.findRequiredViewAsType(source, R.id.messageTimeTv, "field 'messageTimeTv'", TextView.class);
    target.messageContentTv = Utils.findRequiredViewAsType(source, R.id.contentTv, "field 'messageContentTv'", TextView.class);
    target.messageIsOpenIv = Utils.findRequiredViewAsType(source, R.id.isOpenIv, "field 'messageIsOpenIv'", ImageView.class);
    target.noticeTimeTv = Utils.findRequiredViewAsType(source, R.id.noticeTimeTv, "field 'noticeTimeTv'", TextView.class);
    target.noticeContentTv = Utils.findRequiredViewAsType(source, R.id.noticeContentTv, "field 'noticeContentTv'", TextView.class);
    target.noticeIsOpenIv = Utils.findRequiredViewAsType(source, R.id.noticeIsOpenIv, "field 'noticeIsOpenIv'", ImageView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.messageCl, "field 'messageCl' and method 'onViewClicked'");
    target.messageCl = Utils.castView(view, R.id.messageCl, "field 'messageCl'", ConstraintLayout.class);
    view7f0a032d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.noticeCl, "field 'noticeCl' and method 'onViewClicked'");
    target.noticeCl = Utils.castView(view, R.id.noticeCl, "field 'noticeCl'", ConstraintLayout.class);
    view7f0a0367 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MessageActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.rightTitleTv = null;
    target.messageTimeTv = null;
    target.messageContentTv = null;
    target.messageIsOpenIv = null;
    target.noticeTimeTv = null;
    target.noticeContentTv = null;
    target.noticeIsOpenIv = null;
    target.top = null;
    target.messageCl = null;
    target.noticeCl = null;

    view7f0a041b.setOnClickListener(null);
    view7f0a041b = null;
    view7f0a032d.setOnClickListener(null);
    view7f0a032d = null;
    view7f0a0367.setOnClickListener(null);
    view7f0a0367 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
