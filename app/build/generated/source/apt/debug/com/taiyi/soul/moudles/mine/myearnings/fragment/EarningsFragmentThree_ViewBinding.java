// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.myearnings.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.mikephil.charting.charts.LineChart;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import com.taiyi.soul.view.MyLineChartView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EarningsFragmentThree_ViewBinding implements Unbinder {
  private EarningsFragmentThree target;

  private View view7f0a017c;

  @UiThread
  public EarningsFragmentThree_ViewBinding(final EarningsFragmentThree target, View source) {
    this.target = target;

    View view;
    target.totalRevenueTv = Utils.findRequiredViewAsType(source, R.id.totalRevenueTv, "field 'totalRevenueTv'", TextView.class);
    target.thisMonthEarningsTv = Utils.findRequiredViewAsType(source, R.id.thisMonthEarningsTv, "field 'thisMonthEarningsTv'", TextView.class);
    target.dayGainTv = Utils.findRequiredViewAsType(source, R.id.dayGainTv, "field 'dayGainTv'", TextView.class);
    target.valueTv = Utils.findRequiredViewAsType(source, R.id.valueTv, "field 'valueTv'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.springView = Utils.findRequiredViewAsType(source, R.id.springView, "field 'springView'", SpringView.class);
    target.lineChart = Utils.findRequiredViewAsType(source, R.id.lineChart, "field 'lineChart'", LineChart.class);
    target.lineChartView = Utils.findRequiredViewAsType(source, R.id.lineChartView, "field 'lineChartView'", MyLineChartView.class);
    target.noDataLl = Utils.findRequiredViewAsType(source, R.id.noDataLl, "field 'noDataLl'", LinearLayout.class);
    target.noIncomeRecordTv = Utils.findRequiredViewAsType(source, R.id.noIncomeRecordTv, "field 'noIncomeRecordTv'", TextView.class);
    target.topIv = Utils.findRequiredViewAsType(source, R.id.topIv, "field 'topIv'", ImageView.class);
    target.bottomIv = Utils.findRequiredViewAsType(source, R.id.bottomIv, "field 'bottomIv'", ImageView.class);
    target.rlChartView = Utils.findRequiredViewAsType(source, R.id.rlChartView, "field 'rlChartView'", RelativeLayout.class);
    target.lineChart1 = Utils.findRequiredViewAsType(source, R.id.lineChart1, "field 'lineChart1'", LineChart.class);
    view = Utils.findRequiredView(source, R.id.dateIv, "method 'onClicked'");
    view7f0a017c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EarningsFragmentThree target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.totalRevenueTv = null;
    target.thisMonthEarningsTv = null;
    target.dayGainTv = null;
    target.valueTv = null;
    target.recyclerView = null;
    target.springView = null;
    target.lineChart = null;
    target.lineChartView = null;
    target.noDataLl = null;
    target.noIncomeRecordTv = null;
    target.topIv = null;
    target.bottomIv = null;
    target.rlChartView = null;
    target.lineChart1 = null;

    view7f0a017c.setOnClickListener(null);
    view7f0a017c = null;
  }
}
