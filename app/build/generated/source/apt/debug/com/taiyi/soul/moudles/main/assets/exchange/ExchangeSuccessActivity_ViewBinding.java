// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.exchange;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExchangeSuccessActivity_ViewBinding implements Unbinder {
  private ExchangeSuccessActivity target;

  private View view7f0a028e;

  private View view7f0a005f;

  @UiThread
  public ExchangeSuccessActivity_ViewBinding(ExchangeSuccessActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ExchangeSuccessActivity_ViewBinding(final ExchangeSuccessActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.accomplish, "method 'onViewClicked'");
    view7f0a005f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ExchangeSuccessActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a005f.setOnClickListener(null);
    view7f0a005f = null;
  }
}
