// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.accountmanagement.detail.importprivatekey;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ImportPrivateKeyActivity_ViewBinding implements Unbinder {
  private ImportPrivateKeyActivity target;

  private View view7f0a0151;

  private View view7f0a009f;

  @UiThread
  public ImportPrivateKeyActivity_ViewBinding(ImportPrivateKeyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ImportPrivateKeyActivity_ViewBinding(final ImportPrivateKeyActivity target, View source) {
    this.target = target;

    View view;
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.qrCodeIv = Utils.findRequiredViewAsType(source, R.id.qrCodeIv, "field 'qrCodeIv'", ImageView.class);
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.accountNameTv = Utils.findRequiredViewAsType(source, R.id.accountNameTv, "field 'accountNameTv'", TextView.class);
    target.privateKeyTv = Utils.findRequiredViewAsType(source, R.id.privateKeyTv, "field 'privateKeyTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.copyTv, "method 'onClick'");
    view7f0a0151 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onClick'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ImportPrivateKeyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.top = null;
    target.qrCodeIv = null;
    target.titleTv = null;
    target.accountNameTv = null;
    target.privateKeyTv = null;

    view7f0a0151.setOnClickListener(null);
    view7f0a0151 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
