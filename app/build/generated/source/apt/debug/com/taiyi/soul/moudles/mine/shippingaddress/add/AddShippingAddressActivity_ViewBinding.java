// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.shippingaddress.add;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddShippingAddressActivity_ViewBinding implements Unbinder {
  private AddShippingAddressActivity target;

  private View view7f0a041b;

  private View view7f0a049d;

  private View view7f0a0123;

  private View view7f0a0199;

  private View view7f0a009f;

  private View view7f0a0156;

  private View view7f0a0142;

  @UiThread
  public AddShippingAddressActivity_ViewBinding(AddShippingAddressActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddShippingAddressActivity_ViewBinding(final AddShippingAddressActivity target,
      View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rightTitleTv, "field 'rightTitleTv' and method 'onViewClicked'");
    target.rightTitleTv = Utils.castView(view, R.id.rightTitleTv, "field 'rightTitleTv'", TextView.class);
    view7f0a041b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.nameEt = Utils.findRequiredViewAsType(source, R.id.nameEt, "field 'nameEt'", EditText.class);
    target.surnameEt = Utils.findRequiredViewAsType(source, R.id.surnameEt, "field 'surnameEt'", EditText.class);
    target.phoneNumberEt = Utils.findRequiredViewAsType(source, R.id.phoneNumberEt, "field 'phoneNumberEt'", EditText.class);
    target.streetAddressEt = Utils.findRequiredViewAsType(source, R.id.streetAddressEt, "field 'streetAddressEt'", EditText.class);
    target.countryTv = Utils.findRequiredViewAsType(source, R.id.countryTv, "field 'countryTv'", TextView.class);
    target.countryIv = Utils.findRequiredViewAsType(source, R.id.countryIv, "field 'countryIv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.stateTv, "field 'stateTv' and method 'onViewClicked'");
    target.stateTv = Utils.castView(view, R.id.stateTv, "field 'stateTv'", TextView.class);
    view7f0a049d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.stateIv = Utils.findRequiredViewAsType(source, R.id.stateIv, "field 'stateIv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.cityTv, "field 'cityTv' and method 'onViewClicked'");
    target.cityTv = Utils.castView(view, R.id.cityTv, "field 'cityTv'", TextView.class);
    view7f0a0123 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.cityIv = Utils.findRequiredViewAsType(source, R.id.cityIv, "field 'cityIv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.defaultIv, "field 'defaultIv' and method 'onViewClicked'");
    target.defaultIv = Utils.castView(view, R.id.defaultIv, "field 'defaultIv'", ImageView.class);
    view7f0a0199 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.countryRl, "method 'onViewClicked'");
    view7f0a0156 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.confirmTv, "method 'onViewClicked'");
    view7f0a0142 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddShippingAddressActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.rightTitleTv = null;
    target.nameEt = null;
    target.surnameEt = null;
    target.phoneNumberEt = null;
    target.streetAddressEt = null;
    target.countryTv = null;
    target.countryIv = null;
    target.stateTv = null;
    target.stateIv = null;
    target.cityTv = null;
    target.cityIv = null;
    target.defaultIv = null;
    target.top = null;

    view7f0a041b.setOnClickListener(null);
    view7f0a041b = null;
    view7f0a049d.setOnClickListener(null);
    view7f0a049d = null;
    view7f0a0123.setOnClickListener(null);
    view7f0a0123 = null;
    view7f0a0199.setOnClickListener(null);
    view7f0a0199 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a0156.setOnClickListener(null);
    view7f0a0156 = null;
    view7f0a0142.setOnClickListener(null);
    view7f0a0142 = null;
  }
}
