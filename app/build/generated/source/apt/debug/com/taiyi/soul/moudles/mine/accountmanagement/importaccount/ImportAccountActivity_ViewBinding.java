// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.accountmanagement.importaccount;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ImportAccountActivity_ViewBinding implements Unbinder {
  private ImportAccountActivity target;

  private View view7f0a041a;

  private View view7f0a033e;

  private View view7f0a03d1;

  private View view7f0a009f;

  private View view7f0a0263;

  @UiThread
  public ImportAccountActivity_ViewBinding(ImportAccountActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ImportAccountActivity_ViewBinding(final ImportAccountActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rightIv, "field 'rightIv' and method 'onViewClicked'");
    target.rightIv = Utils.castView(view, R.id.rightIv, "field 'rightIv'", ImageView.class);
    view7f0a041a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.accountNameEt = Utils.findRequiredViewAsType(source, R.id.accountNameEt, "field 'accountNameEt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.mnemonicTv, "field 'mnemonicTv' and method 'onViewClicked'");
    target.mnemonicTv = Utils.castView(view, R.id.mnemonicTv, "field 'mnemonicTv'", TextView.class);
    view7f0a033e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.privateKeyTv, "field 'privateKeyTv' and method 'onViewClicked'");
    target.privateKeyTv = Utils.castView(view, R.id.privateKeyTv, "field 'privateKeyTv'", TextView.class);
    view7f0a03d1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.et_key = Utils.findRequiredViewAsType(source, R.id.et, "field 'et_key'", EditText.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.importTv, "method 'onViewClicked'");
    view7f0a0263 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ImportAccountActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.rightIv = null;
    target.accountNameEt = null;
    target.mnemonicTv = null;
    target.privateKeyTv = null;
    target.et_key = null;
    target.top = null;

    view7f0a041a.setOnClickListener(null);
    view7f0a041a = null;
    view7f0a033e.setOnClickListener(null);
    view7f0a033e = null;
    view7f0a03d1.setOnClickListener(null);
    view7f0a03d1 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a0263.setOnClickListener(null);
    view7f0a0263 = null;
  }
}
