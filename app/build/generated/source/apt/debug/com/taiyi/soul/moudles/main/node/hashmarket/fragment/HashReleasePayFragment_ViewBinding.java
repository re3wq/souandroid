// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node.hashmarket.fragment;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HashReleasePayFragment_ViewBinding implements Unbinder {
  private HashReleasePayFragment target;

  private View view7f0a0407;

  @UiThread
  public HashReleasePayFragment_ViewBinding(final HashReleasePayFragment target, View source) {
    this.target = target;

    View view;
    target.mPrice = Utils.findRequiredViewAsType(source, R.id.price, "field 'mPrice'", EditText.class);
    target.mNum = Utils.findRequiredViewAsType(source, R.id.num, "field 'mNum'", EditText.class);
    target.mAllPrice = Utils.findRequiredViewAsType(source, R.id.all_price, "field 'mAllPrice'", TextView.class);
    target.charge = Utils.findRequiredViewAsType(source, R.id.charge, "field 'charge'", TextView.class);
    target.charge_price = Utils.findRequiredViewAsType(source, R.id.charge_price, "field 'charge_price'", TextView.class);
    target.balanceTv = Utils.findRequiredViewAsType(source, R.id.balanceTv, "field 'balanceTv'", TextView.class);
    target.mMarketLowPriceTv = Utils.findRequiredViewAsType(source, R.id.marketLowPriceTv, "field 'mMarketLowPriceTv'", TextView.class);
    target.purchasedTv = Utils.findRequiredViewAsType(source, R.id.purchasedTv, "field 'purchasedTv'", TextView.class);
    target.key_main = Utils.findRequiredViewAsType(source, R.id.key_main, "field 'key_main'", RelativeLayout.class);
    target.key_scroll = Utils.findRequiredViewAsType(source, R.id.key_scroll, "field 'key_scroll'", LinearLayout.class);
    target.keyboardPlace = Utils.findRequiredViewAsType(source, R.id.keyboardPlace, "field 'keyboardPlace'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.releaseTv, "method 'onViewClicked'");
    view7f0a0407 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    HashReleasePayFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mPrice = null;
    target.mNum = null;
    target.mAllPrice = null;
    target.charge = null;
    target.charge_price = null;
    target.balanceTv = null;
    target.mMarketLowPriceTv = null;
    target.purchasedTv = null;
    target.key_main = null;
    target.key_scroll = null;
    target.keyboardPlace = null;

    view7f0a0407.setOnClickListener(null);
    view7f0a0407 = null;
  }
}
