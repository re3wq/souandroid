// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node.cpubandwidth;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CPUAndBandWidthActivity_ViewBinding implements Unbinder {
  private CPUAndBandWidthActivity target;

  private View view7f0a0345;

  private View view7f0a0401;

  private View view7f0a0344;

  private View view7f0a009f;

  @UiThread
  public CPUAndBandWidthActivity_ViewBinding(CPUAndBandWidthActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CPUAndBandWidthActivity_ViewBinding(final CPUAndBandWidthActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.priceTv = Utils.findRequiredViewAsType(source, R.id.priceTv, "field 'priceTv'", TextView.class);
    target.priceNetTv = Utils.findRequiredViewAsType(source, R.id.priceNetTv, "field 'priceNetTv'", TextView.class);
    target.remainingTv = Utils.findRequiredViewAsType(source, R.id.remainingTv, "field 'remainingTv'", TextView.class);
    target.cpuProgressBar = Utils.findRequiredViewAsType(source, R.id.cpuProgressBar, "field 'cpuProgressBar'", ProgressBar.class);
    target.cpuTotalMortgageAmountTv = Utils.findRequiredViewAsType(source, R.id.cpuTotalMortgageAmountTv, "field 'cpuTotalMortgageAmountTv'", TextView.class);
    target.cpuPersonMortgageAmountTv = Utils.findRequiredViewAsType(source, R.id.cpuPersonMortgageAmountTv, "field 'cpuPersonMortgageAmountTv'", TextView.class);
    target.cpuElseMortgageAmountTv = Utils.findRequiredViewAsType(source, R.id.cpuElseMortgageAmountTv, "field 'cpuElseMortgageAmountTv'", TextView.class);
    target.bandWidthRemainingTv = Utils.findRequiredViewAsType(source, R.id.bandWidthRemainingTv, "field 'bandWidthRemainingTv'", TextView.class);
    target.bandWidthProgressBar = Utils.findRequiredViewAsType(source, R.id.bandWidthProgressBar, "field 'bandWidthProgressBar'", ProgressBar.class);
    target.bandWidthTotalMortgageAmountTv = Utils.findRequiredViewAsType(source, R.id.bandWidthTotalMortgageAmountTv, "field 'bandWidthTotalMortgageAmountTv'", TextView.class);
    target.bandWidthPersonMortgageAmountTv = Utils.findRequiredViewAsType(source, R.id.bandWidthPersonMortgageAmountTv, "field 'bandWidthPersonMortgageAmountTv'", TextView.class);
    target.bandWidthElseMortgageAmountTv = Utils.findRequiredViewAsType(source, R.id.bandWidthElseMortgageAmountTv, "field 'bandWidthElseMortgageAmountTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.mortgageTv, "field 'mortgageTv' and method 'onViewClicked'");
    target.mortgageTv = Utils.castView(view, R.id.mortgageTv, "field 'mortgageTv'", TextView.class);
    view7f0a0345 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.redemptionTv, "field 'redemptionTv' and method 'onViewClicked'");
    target.redemptionTv = Utils.castView(view, R.id.redemptionTv, "field 'redemptionTv'", TextView.class);
    view7f0a0401 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.quantity = Utils.findRequiredViewAsType(source, R.id.quantity, "field 'quantity'", TextView.class);
    target.quantityEt = Utils.findRequiredViewAsType(source, R.id.quantityEt, "field 'quantityEt'", EditText.class);
    target.balanceTv = Utils.findRequiredViewAsType(source, R.id.balanceTv, "field 'balanceTv'", TextView.class);
    target.valueTv = Utils.findRequiredViewAsType(source, R.id.valueTv, "field 'valueTv'", TextView.class);
    target.valueNetTv = Utils.findRequiredViewAsType(source, R.id.valueNetTv, "field 'valueNetTv'", TextView.class);
    target.netMortgageQuantity = Utils.findRequiredViewAsType(source, R.id.netMortgageQuantity, "field 'netMortgageQuantity'", TextView.class);
    target.netMortgageQuantityEt = Utils.findRequiredViewAsType(source, R.id.netMortgageQuantityEt, "field 'netMortgageQuantityEt'", EditText.class);
    target.receiptAccount = Utils.findRequiredViewAsType(source, R.id.receiptAccount, "field 'receiptAccount'", TextView.class);
    target.receiptAccountNameEt = Utils.findRequiredViewAsType(source, R.id.receiptAccountNameEt, "field 'receiptAccountNameEt'", EditText.class);
    target.supportOtherAccountMortgageTv = Utils.findRequiredViewAsType(source, R.id.supportOtherAccountMortgageTv, "field 'supportOtherAccountMortgageTv'", TextView.class);
    target.arriveIn72HoursTv = Utils.findRequiredViewAsType(source, R.id.arriveIn72HoursTv, "field 'arriveIn72HoursTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.mortgageOrRedemptionTv, "field 'mortgageOrRedemptionTv' and method 'onViewClicked'");
    target.mortgageOrRedemptionTv = Utils.castView(view, R.id.mortgageOrRedemptionTv, "field 'mortgageOrRedemptionTv'", TextView.class);
    view7f0a0344 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CPUAndBandWidthActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.priceTv = null;
    target.priceNetTv = null;
    target.remainingTv = null;
    target.cpuProgressBar = null;
    target.cpuTotalMortgageAmountTv = null;
    target.cpuPersonMortgageAmountTv = null;
    target.cpuElseMortgageAmountTv = null;
    target.bandWidthRemainingTv = null;
    target.bandWidthProgressBar = null;
    target.bandWidthTotalMortgageAmountTv = null;
    target.bandWidthPersonMortgageAmountTv = null;
    target.bandWidthElseMortgageAmountTv = null;
    target.mortgageTv = null;
    target.redemptionTv = null;
    target.quantity = null;
    target.quantityEt = null;
    target.balanceTv = null;
    target.valueTv = null;
    target.valueNetTv = null;
    target.netMortgageQuantity = null;
    target.netMortgageQuantityEt = null;
    target.receiptAccount = null;
    target.receiptAccountNameEt = null;
    target.supportOtherAccountMortgageTv = null;
    target.arriveIn72HoursTv = null;
    target.mortgageOrRedemptionTv = null;
    target.top = null;

    view7f0a0345.setOnClickListener(null);
    view7f0a0345 = null;
    view7f0a0401.setOnClickListener(null);
    view7f0a0401 = null;
    view7f0a0344.setOnClickListener(null);
    view7f0a0344 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
