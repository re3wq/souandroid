// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.message.content;

import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MessageContentActivity_ViewBinding implements Unbinder {
  private MessageContentActivity target;

  private View view7f0a009f;

  @UiThread
  public MessageContentActivity_ViewBinding(MessageContentActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MessageContentActivity_ViewBinding(final MessageContentActivity target, View source) {
    this.target = target;

    View view;
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.dateTv = Utils.findRequiredViewAsType(source, R.id.dateTv, "field 'dateTv'", TextView.class);
    target.msgTitleTv = Utils.findRequiredViewAsType(source, R.id.msgTitleTv, "field 'msgTitleTv'", TextView.class);
    target.contentTv = Utils.findRequiredViewAsType(source, R.id.contentTv, "field 'contentTv'", TextView.class);
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.webView = Utils.findRequiredViewAsType(source, R.id.webView, "field 'webView'", WebView.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MessageContentActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.top = null;
    target.dateTv = null;
    target.msgTitleTv = null;
    target.contentTv = null;
    target.titleTv = null;
    target.webView = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
