// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.accountmanagement.create;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateAccountActivity_ViewBinding implements Unbinder {
  private CreateAccountActivity target;

  private View view7f0a009f;

  private View view7f0a043a;

  private View view7f0a03ec;

  private View view7f0a0402;

  private View view7f0a035c;

  @UiThread
  public CreateAccountActivity_ViewBinding(CreateAccountActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateAccountActivity_ViewBinding(final CreateAccountActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.inputInviteCodeEt = Utils.findRequiredViewAsType(source, R.id.inputInviteCodeEt, "field 'inputInviteCodeEt'", EditText.class);
    target.invitePeopleEt = Utils.findRequiredViewAsType(source, R.id.invitePeopleEt, "field 'invitePeopleEt'", TextView.class);
    target.accountNameEt = Utils.findRequiredViewAsType(source, R.id.accountNameEt, "field 'accountNameEt'", EditText.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.cl_title = Utils.findRequiredViewAsType(source, R.id.cl_title, "field 'cl_title'", ConstraintLayout.class);
    target.ll_register = Utils.findRequiredViewAsType(source, R.id.ll_register, "field 'll_register'", LinearLayout.class);
    target.view = Utils.findRequiredView(source, R.id.view, "field 'view'");
    target.tv_desc = Utils.findRequiredViewAsType(source, R.id.tv_desc, "field 'tv_desc'", TextView.class);
    target.view_desc = Utils.findRequiredView(source, R.id.view_desc, "field 'view_desc'");
    target.tv_type = Utils.findRequiredViewAsType(source, R.id.tv_type, "field 'tv_type'", TextView.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.scanIv, "method 'onViewClicked'");
    view7f0a043a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.randomTv, "method 'onViewClicked'");
    view7f0a03ec = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.refreshTv, "method 'onViewClicked'");
    view7f0a0402 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.nextStepTv, "method 'onViewClicked'");
    view7f0a035c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateAccountActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.inputInviteCodeEt = null;
    target.invitePeopleEt = null;
    target.accountNameEt = null;
    target.recyclerView = null;
    target.cl_title = null;
    target.ll_register = null;
    target.view = null;
    target.tv_desc = null;
    target.view_desc = null;
    target.tv_type = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a043a.setOnClickListener(null);
    view7f0a043a = null;
    view7f0a03ec.setOnClickListener(null);
    view7f0a03ec = null;
    view7f0a0402.setOnClickListener(null);
    view7f0a0402 = null;
    view7f0a035c.setOnClickListener(null);
    view7f0a035c = null;
  }
}
