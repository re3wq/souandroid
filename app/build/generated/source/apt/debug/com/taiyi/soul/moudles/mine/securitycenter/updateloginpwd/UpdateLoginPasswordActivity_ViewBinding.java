// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.securitycenter.updateloginpwd;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpdateLoginPasswordActivity_ViewBinding implements Unbinder {
  private UpdateLoginPasswordActivity target;

  private View view7f0a0217;

  private View view7f0a035c;

  private View view7f0a009f;

  @UiThread
  public UpdateLoginPasswordActivity_ViewBinding(UpdateLoginPasswordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public UpdateLoginPasswordActivity_ViewBinding(final UpdateLoginPasswordActivity target,
      View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.logoIv = Utils.findRequiredViewAsType(source, R.id.logoIv, "field 'logoIv'", ImageView.class);
    target.tv = Utils.findRequiredViewAsType(source, R.id.tv, "field 'tv'", TextView.class);
    target.emailAddressEt = Utils.findRequiredViewAsType(source, R.id.emailAddressEt, "field 'emailAddressEt'", TextView.class);
    target.verificationCodeEt = Utils.findRequiredViewAsType(source, R.id.verificationCodeEt, "field 'verificationCodeEt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.getVerificationCodeTv, "field 'getVerificationCodeTv' and method 'onViewClicked'");
    target.getVerificationCodeTv = Utils.castView(view, R.id.getVerificationCodeTv, "field 'getVerificationCodeTv'", TextView.class);
    view7f0a0217 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ll = Utils.findRequiredViewAsType(source, R.id.ll, "field 'll'", LinearLayout.class);
    target.newPwdEt = Utils.findRequiredViewAsType(source, R.id.newPwdEt, "field 'newPwdEt'", EditText.class);
    target.confirmPwdEt = Utils.findRequiredViewAsType(source, R.id.confirmPwdEt, "field 'confirmPwdEt'", EditText.class);
    target.llNext = Utils.findRequiredViewAsType(source, R.id.ll_next, "field 'llNext'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.nextStepTv, "field 'nextStepTv' and method 'onViewClicked'");
    target.nextStepTv = Utils.castView(view, R.id.nextStepTv, "field 'nextStepTv'", TextView.class);
    view7f0a035c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    UpdateLoginPasswordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.logoIv = null;
    target.tv = null;
    target.emailAddressEt = null;
    target.verificationCodeEt = null;
    target.getVerificationCodeTv = null;
    target.ll = null;
    target.newPwdEt = null;
    target.confirmPwdEt = null;
    target.llNext = null;
    target.nextStepTv = null;
    target.top = null;

    view7f0a0217.setOnClickListener(null);
    view7f0a0217 = null;
    view7f0a035c.setOnClickListener(null);
    view7f0a035c = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
