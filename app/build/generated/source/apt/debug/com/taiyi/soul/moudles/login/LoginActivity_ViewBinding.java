// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.login;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  private View view7f0a0535;

  private View view7f0a02ef;

  private View view7f0a053b;

  private View view7f0a0550;

  private View view7f0a02f0;

  private View view7f0a02fa;

  private View view7f0a0541;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(final LoginActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.tv_code, "field 'tvCode' and method 'onViewClicked'");
    target.tvCode = Utils.castView(view, R.id.tv_code, "field 'tvCode'", TextView.class);
    view7f0a0535 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.iv = Utils.findRequiredViewAsType(source, R.id.iv, "field 'iv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.ll_code, "field 'llCode' and method 'onViewClicked'");
    target.llCode = Utils.castView(view, R.id.ll_code, "field 'llCode'", LinearLayout.class);
    view7f0a02ef = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ivEmail = Utils.findRequiredViewAsType(source, R.id.iv_email, "field 'ivEmail'", ImageView.class);
    target.ivPhone = Utils.findRequiredViewAsType(source, R.id.iv_phone, "field 'ivPhone'", ImageView.class);
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'etEmail'", EditText.class);
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'etPassword'", EditText.class);
    view = Utils.findRequiredView(source, R.id.tv_forget, "field 'tvForget' and method 'onViewClicked'");
    target.tvForget = Utils.castView(view, R.id.tv_forget, "field 'tvForget'", TextView.class);
    view7f0a053b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_register, "field 'tvRegister' and method 'onViewClicked'");
    target.tvRegister = Utils.castView(view, R.id.tv_register, "field 'tvRegister'", TextView.class);
    view7f0a0550 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.llPhone = Utils.findRequiredViewAsType(source, R.id.ll_phone, "field 'llPhone'", LinearLayout.class);
    target.llHide = Utils.findRequiredViewAsType(source, R.id.ll_hide, "field 'llHide'", LinearLayout.class);
    target.key_main = Utils.findRequiredViewAsType(source, R.id.key_main, "field 'key_main'", RelativeLayout.class);
    target.key_scroll = Utils.findRequiredViewAsType(source, R.id.key_scroll, "field 'key_scroll'", LinearLayout.class);
    target.keyboardPlace = Utils.findRequiredViewAsType(source, R.id.keyboardPlace, "field 'keyboardPlace'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.ll_email, "method 'onViewClicked'");
    view7f0a02f0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ll_phone1, "method 'onViewClicked'");
    view7f0a02fa = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_login, "method 'onViewClicked'");
    view7f0a0541 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvCode = null;
    target.iv = null;
    target.llCode = null;
    target.ivEmail = null;
    target.ivPhone = null;
    target.etPhone = null;
    target.etEmail = null;
    target.etPassword = null;
    target.tvForget = null;
    target.tvRegister = null;
    target.llPhone = null;
    target.llHide = null;
    target.key_main = null;
    target.key_scroll = null;
    target.keyboardPlace = null;

    view7f0a0535.setOnClickListener(null);
    view7f0a0535 = null;
    view7f0a02ef.setOnClickListener(null);
    view7f0a02ef = null;
    view7f0a053b.setOnClickListener(null);
    view7f0a053b = null;
    view7f0a0550.setOnClickListener(null);
    view7f0a0550 = null;
    view7f0a02f0.setOnClickListener(null);
    view7f0a02f0 = null;
    view7f0a02fa.setOnClickListener(null);
    view7f0a02fa = null;
    view7f0a0541.setOnClickListener(null);
    view7f0a0541 = null;
  }
}
