// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.options;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.github.fujianlian.klinechart.KLineChartView;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OptionsDetailActivity_ViewBinding implements Unbinder {
  private OptionsDetailActivity target;

  private View view7f0a028e;

  private View view7f0a038c;

  private View view7f0a0391;

  private View view7f0a038a;

  private View view7f0a0390;

  private View view7f0a038d;

  private View view7f0a0388;

  private View view7f0a025e;

  private View view7f0a0389;

  private View view7f0a037b;

  private View view7f0a038b;

  private View view7f0a0387;

  private View view7f0a037c;

  @UiThread
  public OptionsDetailActivity_ViewBinding(OptionsDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OptionsDetailActivity_ViewBinding(final OptionsDetailActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.iv_back, "field 'mIvBack' and method 'onViewClicked'");
    target.mIvBack = Utils.castView(view, R.id.iv_back, "field 'mIvBack'", ImageView.class);
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    target.mKLineChartView = Utils.findRequiredViewAsType(source, R.id.kLineChartView, "field 'mKLineChartView'", KLineChartView.class);
    view = Utils.findRequiredView(source, R.id.options_minute, "field 'mOptionsMinute' and method 'onViewClicked'");
    target.mOptionsMinute = Utils.castView(view, R.id.options_minute, "field 'mOptionsMinute'", TextView.class);
    view7f0a038c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.options_one_hour, "field 'mOptionsOneHour' and method 'onViewClicked'");
    target.mOptionsOneHour = Utils.castView(view, R.id.options_one_hour, "field 'mOptionsOneHour'", TextView.class);
    view7f0a0391 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.options_four_hour, "field 'mOptionsFourHour' and method 'onViewClicked'");
    target.mOptionsFourHour = Utils.castView(view, R.id.options_four_hour, "field 'mOptionsFourHour'", TextView.class);
    view7f0a038a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.options_one_day, "field 'mOptionsOneDay' and method 'onViewClicked'");
    target.mOptionsOneDay = Utils.castView(view, R.id.options_one_day, "field 'mOptionsOneDay'", TextView.class);
    view7f0a0390 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.options_more, "field 'mOptionsMore' and method 'onViewClicked'");
    target.mOptionsMore = Utils.castView(view, R.id.options_more, "field 'mOptionsMore'", TextView.class);
    view7f0a038d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.options_depth_map, "field 'mOptionsDepthMap' and method 'onViewClicked'");
    target.mOptionsDepthMap = Utils.castView(view, R.id.options_depth_map, "field 'mOptionsDepthMap'", TextView.class);
    view7f0a0388 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mOptionsMoreImg = Utils.findRequiredViewAsType(source, R.id.options_more_img, "field 'mOptionsMoreImg'", ImageView.class);
    target.mOptionsFileImg = Utils.findRequiredViewAsType(source, R.id.option_file_img, "field 'mOptionsFileImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.img_right, "field 'img_right' and method 'onViewClicked'");
    target.img_right = Utils.castView(view, R.id.img_right, "field 'img_right'", ImageView.class);
    view7f0a025e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.options_file, "field 'mOptionsFile' and method 'onViewClicked'");
    target.mOptionsFile = Utils.castView(view, R.id.options_file, "field 'mOptionsFile'", RelativeLayout.class);
    view7f0a0389 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mOptionsTab = Utils.findRequiredViewAsType(source, R.id.options_tab, "field 'mOptionsTab'", SlidingTabLayout.class);
    target.mOptionsPager = Utils.findRequiredViewAsType(source, R.id.options_pager, "field 'mOptionsPager'", ViewPager.class);
    target.mOptionRealtimePrice = Utils.findRequiredViewAsType(source, R.id.option_realtime_price, "field 'mOptionRealtimePrice'", TextView.class);
    target.mOptionRate = Utils.findRequiredViewAsType(source, R.id.option_rate, "field 'mOptionRate'", TextView.class);
    target.mOptionFloat = Utils.findRequiredViewAsType(source, R.id.option_float, "field 'mOptionFloat'", TextView.class);
    target.mOptionHighPrice = Utils.findRequiredViewAsType(source, R.id.option_high_price, "field 'mOptionHighPrice'", TextView.class);
    target.mOptionLowPrice = Utils.findRequiredViewAsType(source, R.id.option_low_price, "field 'mOptionLowPrice'", TextView.class);
    target.mOptionHourPrice = Utils.findRequiredViewAsType(source, R.id.option_hour_price, "field 'mOptionHourPrice'", TextView.class);
    view = Utils.findRequiredView(source, R.id.option_button1, "field 'option_button1' and method 'onViewClicked'");
    target.option_button1 = Utils.castView(view, R.id.option_button1, "field 'option_button1'", TextView.class);
    view7f0a037b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rl_coin_img = Utils.findRequiredViewAsType(source, R.id.rl_coin_img, "field 'rl_coin_img'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.options_index, "method 'onViewClicked'");
    view7f0a038b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.options_bi_more, "method 'onViewClicked'");
    view7f0a0387 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.option_button2, "method 'onViewClicked'");
    view7f0a037c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    OptionsDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mIvBack = null;
    target.mTvTitle = null;
    target.mKLineChartView = null;
    target.mOptionsMinute = null;
    target.mOptionsOneHour = null;
    target.mOptionsFourHour = null;
    target.mOptionsOneDay = null;
    target.mOptionsMore = null;
    target.mOptionsDepthMap = null;
    target.mOptionsMoreImg = null;
    target.mOptionsFileImg = null;
    target.img_right = null;
    target.mOptionsFile = null;
    target.mOptionsTab = null;
    target.mOptionsPager = null;
    target.mOptionRealtimePrice = null;
    target.mOptionRate = null;
    target.mOptionFloat = null;
    target.mOptionHighPrice = null;
    target.mOptionLowPrice = null;
    target.mOptionHourPrice = null;
    target.option_button1 = null;
    target.rl_coin_img = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a038c.setOnClickListener(null);
    view7f0a038c = null;
    view7f0a0391.setOnClickListener(null);
    view7f0a0391 = null;
    view7f0a038a.setOnClickListener(null);
    view7f0a038a = null;
    view7f0a0390.setOnClickListener(null);
    view7f0a0390 = null;
    view7f0a038d.setOnClickListener(null);
    view7f0a038d = null;
    view7f0a0388.setOnClickListener(null);
    view7f0a0388 = null;
    view7f0a025e.setOnClickListener(null);
    view7f0a025e = null;
    view7f0a0389.setOnClickListener(null);
    view7f0a0389 = null;
    view7f0a037b.setOnClickListener(null);
    view7f0a037b = null;
    view7f0a038b.setOnClickListener(null);
    view7f0a038b = null;
    view7f0a0387.setOnClickListener(null);
    view7f0a0387 = null;
    view7f0a037c.setOnClickListener(null);
    view7f0a037c = null;
  }
}
