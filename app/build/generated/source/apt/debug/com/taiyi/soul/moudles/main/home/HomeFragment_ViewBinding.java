// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import com.zhouwei.mzbanner.MZBannerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeFragment_ViewBinding implements Unbinder {
  private HomeFragment target;

  private View view7f0a0269;

  private View view7f0a0139;

  private View view7f0a0515;

  private View view7f0a01ec;

  private View view7f0a0197;

  @UiThread
  public HomeFragment_ViewBinding(final HomeFragment target, View source) {
    this.target = target;

    View view;
    target.banner = Utils.findRequiredViewAsType(source, R.id.banner, "field 'banner'", MZBannerView.class);
    target.recyclerViewBrowser = Utils.findRequiredViewAsType(source, R.id.recyclerViewBrowser, "field 'recyclerViewBrowser'", RecyclerView.class);
    target.recycle = Utils.findRequiredViewAsType(source, R.id.recycle, "field 'recycle'", PullRecyclerView.class);
    view = Utils.findRequiredView(source, R.id.inv_img, "field 'inv_img' and method 'onViewClicked'");
    target.inv_img = Utils.castView(view, R.id.inv_img, "field 'inv_img'", ImageView.class);
    view7f0a0269 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.collectMoneyTv, "method 'onViewClicked'");
    view7f0a0139 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.transferTv, "method 'onViewClicked'");
    view7f0a0515 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.exchangeTv, "method 'onViewClicked'");
    view7f0a01ec = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.debtTv, "method 'onViewClicked'");
    view7f0a0197 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    HomeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.banner = null;
    target.recyclerViewBrowser = null;
    target.recycle = null;
    target.inv_img = null;

    view7f0a0269.setOnClickListener(null);
    view7f0a0269 = null;
    view7f0a0139.setOnClickListener(null);
    view7f0a0139 = null;
    view7f0a0515.setOnClickListener(null);
    view7f0a0515 = null;
    view7f0a01ec.setOnClickListener(null);
    view7f0a01ec = null;
    view7f0a0197.setOnClickListener(null);
    view7f0a0197 = null;
  }
}
