// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.accountmanagement.detail;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AccountDetailActivity_ViewBinding implements Unbinder {
  private AccountDetailActivity target;

  private View view7f0a026b;

  private View view7f0a009f;

  private View view7f0a0261;

  private View view7f0a0262;

  private View view7f0a01a4;

  @UiThread
  public AccountDetailActivity_ViewBinding(AccountDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AccountDetailActivity_ViewBinding(final AccountDetailActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.accountNameTv = Utils.findRequiredViewAsType(source, R.id.accountNameTv, "field 'accountNameTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.inviteCodeTv, "field 'inviteCodeTv' and method 'onViewClicked'");
    target.inviteCodeTv = Utils.castView(view, R.id.inviteCodeTv, "field 'inviteCodeTv'", TextView.class);
    view7f0a026b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.registerTimeTv = Utils.findRequiredViewAsType(source, R.id.registerTimeTv, "field 'registerTimeTv'", TextView.class);
    target.zwt = Utils.findRequiredViewAsType(source, R.id.zwt, "field 'zwt'", TextView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.importMnemonicTv, "method 'onViewClicked'");
    view7f0a0261 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.importPrivateKeyTv, "method 'onViewClicked'");
    view7f0a0262 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.deleteAccountTv, "method 'onViewClicked'");
    view7f0a01a4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AccountDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.accountNameTv = null;
    target.inviteCodeTv = null;
    target.registerTimeTv = null;
    target.zwt = null;
    target.top = null;

    view7f0a026b.setOnClickListener(null);
    view7f0a026b = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a0261.setOnClickListener(null);
    view7f0a0261 = null;
    view7f0a0262.setOnClickListener(null);
    view7f0a0262 = null;
    view7f0a01a4.setOnClickListener(null);
    view7f0a01a4 = null;
  }
}
