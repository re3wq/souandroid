// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.exchange;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BondExchangeActivity_ViewBinding implements Unbinder {
  private BondExchangeActivity target;

  private View view7f0a0552;

  private View view7f0a00df;

  private View view7f0a028e;

  @UiThread
  public BondExchangeActivity_ViewBinding(BondExchangeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BondExchangeActivity_ViewBinding(final BondExchangeActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.tv_right_text, "field 'mTvRightText' and method 'onViewClicked'");
    target.mTvRightText = Utils.castView(view, R.id.tv_right_text, "field 'mTvRightText'", TextView.class);
    view7f0a0552 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mBondExchange = Utils.findRequiredViewAsType(source, R.id.bond_exchange, "field 'mBondExchange'", TextView.class);
    target.mBondRecycle = Utils.findRequiredViewAsType(source, R.id.bond_recycle, "field 'mBondRecycle'", PullRecyclerView.class);
    target.mBondPrice = Utils.findRequiredViewAsType(source, R.id.bond_price, "field 'mBondPrice'", TextView.class);
    view = Utils.findRequiredView(source, R.id.bond_refresh, "field 'mBondRefresh' and method 'onViewClicked'");
    target.mBondRefresh = Utils.castView(view, R.id.bond_refresh, "field 'mBondRefresh'", ImageView.class);
    view7f0a00df = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mBondRedeemable = Utils.findRequiredViewAsType(source, R.id.bond_redeemable, "field 'mBondRedeemable'", TextView.class);
    target.bond_text = Utils.findRequiredViewAsType(source, R.id.bond_text, "field 'bond_text'", TextView.class);
    target.mBondExchangeRate = Utils.findRequiredViewAsType(source, R.id.bond_exchange_rate, "field 'mBondExchangeRate'", TextView.class);
    target.mBondNextTime = Utils.findRequiredViewAsType(source, R.id.bond_next_time, "field 'mBondNextTime'", TextView.class);
    target.mBondBalance = Utils.findRequiredViewAsType(source, R.id.bond_balance, "field 'mBondBalance'", TextView.class);
    target.bond_tit = Utils.findRequiredViewAsType(source, R.id.bond_tit, "field 'bond_tit'", TextView.class);
    target.mBondExisting = Utils.findRequiredViewAsType(source, R.id.bond_existing, "field 'mBondExisting'", TextView.class);
    target.mBondRedemption = Utils.findRequiredViewAsType(source, R.id.bond_redemption, "field 'mBondRedemption'", TextView.class);
    target.mBondEquityIndex = Utils.findRequiredViewAsType(source, R.id.bond_equity_index, "field 'mBondEquityIndex'", TextView.class);
    target.mBondReward = Utils.findRequiredViewAsType(source, R.id.bond_reward, "field 'mBondReward'", TextView.class);
    target.bond_reward1 = Utils.findRequiredViewAsType(source, R.id.bond_reward1, "field 'bond_reward1'", TextView.class);
    target.spring_view = Utils.findRequiredViewAsType(source, R.id.spring_view, "field 'spring_view'", SpringView.class);
    target.rl_rate = Utils.findRequiredViewAsType(source, R.id.rl_rate, "field 'rl_rate'", RelativeLayout.class);
    target.ll_time = Utils.findRequiredViewAsType(source, R.id.ll_time, "field 'll_time'", LinearLayout.class);
    target.empty_view = Utils.findRequiredViewAsType(source, R.id.empty_view, "field 'empty_view'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    BondExchangeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mTvRightText = null;
    target.mBondExchange = null;
    target.mBondRecycle = null;
    target.mBondPrice = null;
    target.mBondRefresh = null;
    target.mBondRedeemable = null;
    target.bond_text = null;
    target.mBondExchangeRate = null;
    target.mBondNextTime = null;
    target.mBondBalance = null;
    target.bond_tit = null;
    target.mBondExisting = null;
    target.mBondRedemption = null;
    target.mBondEquityIndex = null;
    target.mBondReward = null;
    target.bond_reward1 = null;
    target.spring_view = null;
    target.rl_rate = null;
    target.ll_time = null;
    target.empty_view = null;

    view7f0a0552.setOnClickListener(null);
    view7f0a0552 = null;
    view7f0a00df.setOnClickListener(null);
    view7f0a00df = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
  }
}
