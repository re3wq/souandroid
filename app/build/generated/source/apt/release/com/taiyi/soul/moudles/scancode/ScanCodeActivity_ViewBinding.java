// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.scancode;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mylhyl.zxing.scanner.ScannerView;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ScanCodeActivity_ViewBinding implements Unbinder {
  private ScanCodeActivity target;

  @UiThread
  public ScanCodeActivity_ViewBinding(ScanCodeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ScanCodeActivity_ViewBinding(ScanCodeActivity target, View source) {
    this.target = target;

    target.mScannerView = Utils.findRequiredViewAsType(source, R.id.scanner_view, "field 'mScannerView'", ScannerView.class);
    target.mIvBack = Utils.findRequiredViewAsType(source, R.id.iv_back, "field 'mIvBack'", ImageView.class);
    target.mLl = Utils.findRequiredViewAsType(source, R.id.ll, "field 'mLl'", RelativeLayout.class);
    target.mTvRight = Utils.findRequiredViewAsType(source, R.id.tv_right, "field 'mTvRight'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ScanCodeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mScannerView = null;
    target.mIvBack = null;
    target.mLl = null;
    target.mTvRight = null;
  }
}
