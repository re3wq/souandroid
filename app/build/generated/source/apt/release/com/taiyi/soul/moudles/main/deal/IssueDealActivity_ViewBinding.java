// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.deal;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class IssueDealActivity_ViewBinding implements Unbinder {
  private IssueDealActivity target;

  private View view7f0a0273;

  private View view7f0a028e;

  @UiThread
  public IssueDealActivity_ViewBinding(IssueDealActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public IssueDealActivity_ViewBinding(final IssueDealActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.issue_sure, "field 'mIssueSure' and method 'onViewClicked'");
    target.mIssueSure = Utils.castView(view, R.id.issue_sure, "field 'mIssueSure'", TextView.class);
    view7f0a0273 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mTvIssueNum = Utils.findRequiredViewAsType(source, R.id.tv_issue_num, "field 'mTvIssueNum'", TextView.class);
    target.mDealOncePrice = Utils.findRequiredViewAsType(source, R.id.deal_once_price, "field 'mDealOncePrice'", TextView.class);
    target.mDealPrice = Utils.findRequiredViewAsType(source, R.id.deal_price, "field 'mDealPrice'", TextView.class);
    target.mAccountBalance = Utils.findRequiredViewAsType(source, R.id.account_balance, "field 'mAccountBalance'", TextView.class);
    target.mServiceCharge = Utils.findRequiredViewAsType(source, R.id.service_charge, "field 'mServiceCharge'", TextView.class);
    target.mServiceChargePrice = Utils.findRequiredViewAsType(source, R.id.service_charge_price, "field 'mServiceChargePrice'", TextView.class);
    target.mTotalPrice = Utils.findRequiredViewAsType(source, R.id.total_price, "field 'mTotalPrice'", TextView.class);
    target.mPrice = Utils.findRequiredViewAsType(source, R.id.price, "field 'mPrice'", EditText.class);
    target.mNum = Utils.findRequiredViewAsType(source, R.id.num, "field 'mNum'", EditText.class);
    target.mAccountUnit = Utils.findRequiredViewAsType(source, R.id.account_unit, "field 'mAccountUnit'", TextView.class);
    target.mChargeUnit = Utils.findRequiredViewAsType(source, R.id.charge_unit, "field 'mChargeUnit'", TextView.class);
    target.mTotalUnit = Utils.findRequiredViewAsType(source, R.id.total_unit, "field 'mTotalUnit'", TextView.class);
    target.suggest_price = Utils.findRequiredViewAsType(source, R.id.suggest_price, "field 'suggest_price'", TextView.class);
    target.total_type = Utils.findRequiredViewAsType(source, R.id.total_type, "field 'total_type'", TextView.class);
    target.key_main = Utils.findRequiredViewAsType(source, R.id.key_main, "field 'key_main'", RelativeLayout.class);
    target.key_scroll = Utils.findRequiredViewAsType(source, R.id.key_scroll, "field 'key_scroll'", LinearLayout.class);
    target.keyboardPlace = Utils.findRequiredViewAsType(source, R.id.keyboardPlace, "field 'keyboardPlace'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    IssueDealActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mIssueSure = null;
    target.mTvIssueNum = null;
    target.mDealOncePrice = null;
    target.mDealPrice = null;
    target.mAccountBalance = null;
    target.mServiceCharge = null;
    target.mServiceChargePrice = null;
    target.mTotalPrice = null;
    target.mPrice = null;
    target.mNum = null;
    target.mAccountUnit = null;
    target.mChargeUnit = null;
    target.mTotalUnit = null;
    target.suggest_price = null;
    target.total_type = null;
    target.key_main = null;
    target.key_scroll = null;
    target.keyboardPlace = null;

    view7f0a0273.setOnClickListener(null);
    view7f0a0273 = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
  }
}
