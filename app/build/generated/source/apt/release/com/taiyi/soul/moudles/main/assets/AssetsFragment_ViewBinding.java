// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AssetsFragment_ViewBinding implements Unbinder {
  private AssetsFragment target;

  private View view7f0a0139;

  private View view7f0a041a;

  private View view7f0a0098;

  private View view7f0a0212;

  private View view7f0a0515;

  private View view7f0a01ec;

  private View view7f0a0197;

  @UiThread
  public AssetsFragment_ViewBinding(final AssetsFragment target, View source) {
    this.target = target;

    View view;
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", RelativeLayout.class);
    target.availableAssetsTv = Utils.findRequiredViewAsType(source, R.id.availableAssetsTv, "field 'availableAssetsTv'", NumberRollingView.class);
    target.accountTotalAssetsTv = Utils.findRequiredViewAsType(source, R.id.accountTotalAssetsTv, "field 'accountTotalAssetsTv'", NumberRollingView.class);
    target.freezeAssetsTv = Utils.findRequiredViewAsType(source, R.id.freezeAssetsTv, "field 'freezeAssetsTv'", NumberRollingView.class);
    view = Utils.findRequiredView(source, R.id.collectMoneyTv, "field 'collectMoneyTv' and method 'onViewClicked'");
    target.collectMoneyTv = Utils.castView(view, R.id.collectMoneyTv, "field 'collectMoneyTv'", TextView.class);
    view7f0a0139 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.rotationIv = Utils.findRequiredViewAsType(source, R.id.rotationIv, "field 'rotationIv'", ImageView.class);
    target.iivv = Utils.findRequiredViewAsType(source, R.id.iivv, "field 'iivv'", ImageView.class);
    target.transparentIv = Utils.findRequiredViewAsType(source, R.id.transparentIv, "field 'transparentIv'", ImageView.class);
    target.ll = Utils.findRequiredViewAsType(source, R.id.ll, "field 'll'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.rightIv, "method 'onViewClicked'");
    view7f0a041a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.availableAssetsCl, "method 'onViewClicked'");
    view7f0a0098 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.freezeAssetsCl, "method 'onViewClicked'");
    view7f0a0212 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.transferTv, "method 'onViewClicked'");
    view7f0a0515 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.exchangeTv, "method 'onViewClicked'");
    view7f0a01ec = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.debtTv, "method 'onViewClicked'");
    view7f0a0197 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AssetsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.top = null;
    target.availableAssetsTv = null;
    target.accountTotalAssetsTv = null;
    target.freezeAssetsTv = null;
    target.collectMoneyTv = null;
    target.recyclerView = null;
    target.rotationIv = null;
    target.iivv = null;
    target.transparentIv = null;
    target.ll = null;

    view7f0a0139.setOnClickListener(null);
    view7f0a0139 = null;
    view7f0a041a.setOnClickListener(null);
    view7f0a041a = null;
    view7f0a0098.setOnClickListener(null);
    view7f0a0098 = null;
    view7f0a0212.setOnClickListener(null);
    view7f0a0212 = null;
    view7f0a0515.setOnClickListener(null);
    view7f0a0515 = null;
    view7f0a01ec.setOnClickListener(null);
    view7f0a01ec = null;
    view7f0a0197.setOnClickListener(null);
    view7f0a0197 = null;
  }
}
