// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.account;

import android.view.View;
import android.widget.RelativeLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AccountManagerActivity_ViewBinding implements Unbinder {
  private AccountManagerActivity target;

  @UiThread
  public AccountManagerActivity_ViewBinding(AccountManagerActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AccountManagerActivity_ViewBinding(AccountManagerActivity target, View source) {
    this.target = target;

    target.mTitle = Utils.findRequiredViewAsType(source, R.id.title, "field 'mTitle'", RelativeLayout.class);
    target.recycleAccount = Utils.findRequiredViewAsType(source, R.id.recycle_account, "field 'recycleAccount'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AccountManagerActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTitle = null;
    target.recycleAccount = null;
  }
}
