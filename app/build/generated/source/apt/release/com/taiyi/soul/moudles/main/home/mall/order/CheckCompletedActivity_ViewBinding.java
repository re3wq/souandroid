// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.order;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckCompletedActivity_ViewBinding implements Unbinder {
  private CheckCompletedActivity target;

  private View view7f0a028e;

  private View view7f0a0117;

  @UiThread
  public CheckCompletedActivity_ViewBinding(CheckCompletedActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CheckCompletedActivity_ViewBinding(final CheckCompletedActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.check_order_find, "method 'onViewClicked'");
    view7f0a0117 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CheckCompletedActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a0117.setOnClickListener(null);
    view7f0a0117 = null;
  }
}
