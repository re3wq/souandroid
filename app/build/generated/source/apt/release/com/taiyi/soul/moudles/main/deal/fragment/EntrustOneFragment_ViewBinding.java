// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.deal.fragment;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EntrustOneFragment_ViewBinding implements Unbinder {
  private EntrustOneFragment target;

  @UiThread
  public EntrustOneFragment_ViewBinding(EntrustOneFragment target, View source) {
    this.target = target;

    target.mPayRecycle = Utils.findRequiredViewAsType(source, R.id.pull_recycle, "field 'mPayRecycle'", PullRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EntrustOneFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mPayRecycle = null;
  }
}
