// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.accountmanagement.create;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateAccountStep3Activity_ViewBinding implements Unbinder {
  private CreateAccountStep3Activity target;

  private View view7f0a01b0;

  private View view7f0a009f;

  @UiThread
  public CreateAccountStep3Activity_ViewBinding(CreateAccountStep3Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateAccountStep3Activity_ViewBinding(final CreateAccountStep3Activity target,
      View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.cl_title = Utils.findRequiredViewAsType(source, R.id.cl_title, "field 'cl_title'", ConstraintLayout.class);
    target.tv_type = Utils.findRequiredViewAsType(source, R.id.tv_type, "field 'tv_type'", TextView.class);
    view = Utils.findRequiredView(source, R.id.determineTv, "field 'determineTv' and method 'onViewClicked'");
    target.determineTv = Utils.castView(view, R.id.determineTv, "field 'determineTv'", TextView.class);
    view7f0a01b0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateAccountStep3Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.cl_title = null;
    target.tv_type = null;
    target.determineTv = null;

    view7f0a01b0.setOnClickListener(null);
    view7f0a01b0 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
