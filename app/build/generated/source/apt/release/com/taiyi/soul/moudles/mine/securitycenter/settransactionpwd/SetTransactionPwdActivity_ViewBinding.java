// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.securitycenter.settransactionpwd;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.maning.pswedittextlibrary.MNPasswordEditText;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SetTransactionPwdActivity_ViewBinding implements Unbinder {
  private SetTransactionPwdActivity target;

  private View view7f0a035c;

  private View view7f0a009f;

  @UiThread
  public SetTransactionPwdActivity_ViewBinding(SetTransactionPwdActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SetTransactionPwdActivity_ViewBinding(final SetTransactionPwdActivity target,
      View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.newPwdTv = Utils.findRequiredViewAsType(source, R.id.newPwdTv, "field 'newPwdTv'", TextView.class);
    target.pwdEditText = Utils.findRequiredViewAsType(source, R.id.pwdEditText, "field 'pwdEditText'", MNPasswordEditText.class);
    view = Utils.findRequiredView(source, R.id.nextStepTv, "field 'nextStepTv' and method 'onViewClicked'");
    target.nextStepTv = Utils.castView(view, R.id.nextStepTv, "field 'nextStepTv'", TextView.class);
    view7f0a035c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SetTransactionPwdActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.newPwdTv = null;
    target.pwdEditText = null;
    target.nextStepTv = null;
    target.top = null;

    view7f0a035c.setOnClickListener(null);
    view7f0a035c = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
