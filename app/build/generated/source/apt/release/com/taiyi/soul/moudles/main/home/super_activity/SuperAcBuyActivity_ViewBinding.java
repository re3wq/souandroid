// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.super_activity;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SuperAcBuyActivity_ViewBinding implements Unbinder {
  private SuperAcBuyActivity target;

  private View view7f0a00f6;

  private View view7f0a009f;

  private View view7f0a041a;

  @UiThread
  public SuperAcBuyActivity_ViewBinding(SuperAcBuyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SuperAcBuyActivity_ViewBinding(final SuperAcBuyActivity target, View source) {
    this.target = target;

    View view;
    target.mTitleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'mTitleTv'", TextView.class);
    target.mComputingPowerPriceTv = Utils.findRequiredViewAsType(source, R.id.computingPowerPriceTv, "field 'mComputingPowerPriceTv'", TextView.class);
    target.mQuantityHash = Utils.findRequiredViewAsType(source, R.id.quantityHash, "field 'mQuantityHash'", TextView.class);
    target.mAmountTv = Utils.findRequiredViewAsType(source, R.id.amountTv, "field 'mAmountTv'", NumberRollingView.class);
    target.mInputQuantityEt = Utils.findRequiredViewAsType(source, R.id.inputQuantityEt, "field 'mInputQuantityEt'", EditText.class);
    target.mT = Utils.findRequiredViewAsType(source, R.id.t, "field 'mT'", TextView.class);
    target.mUsdn = Utils.findRequiredViewAsType(source, R.id.usdn, "field 'mUsdn'", TextView.class);
    target.mBalanceTv = Utils.findRequiredViewAsType(source, R.id.balanceTv, "field 'mBalanceTv'", NumberRollingView.class);
    target.mTotalPaymentTv = Utils.findRequiredViewAsType(source, R.id.totalPaymentTv, "field 'mTotalPaymentTv'", TextView.class);
    target.mTt = Utils.findRequiredViewAsType(source, R.id.tt, "field 'mTt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.buyTv, "field 'mBuyTv' and method 'onViewClicked'");
    target.mBuyTv = Utils.castView(view, R.id.buyTv, "field 'mBuyTv'", TextView.class);
    view7f0a00f6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'mRecyclerView'", PullRecyclerView.class);
    target.empty_data = Utils.findRequiredViewAsType(source, R.id.empty_data, "field 'empty_data'", LinearLayout.class);
    target.keyScroll = Utils.findRequiredViewAsType(source, R.id.key_scroll, "field 'keyScroll'", LinearLayout.class);
    target.keyboardPlace = Utils.findRequiredViewAsType(source, R.id.keyboardPlace, "field 'keyboardPlace'", LinearLayout.class);
    target.keyMain = Utils.findRequiredViewAsType(source, R.id.key_main, "field 'keyMain'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rightIv, "method 'onViewClicked'");
    view7f0a041a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SuperAcBuyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTitleTv = null;
    target.mComputingPowerPriceTv = null;
    target.mQuantityHash = null;
    target.mAmountTv = null;
    target.mInputQuantityEt = null;
    target.mT = null;
    target.mUsdn = null;
    target.mBalanceTv = null;
    target.mTotalPaymentTv = null;
    target.mTt = null;
    target.mBuyTv = null;
    target.mRecyclerView = null;
    target.empty_data = null;
    target.keyScroll = null;
    target.keyboardPlace = null;
    target.keyMain = null;

    view7f0a00f6.setOnClickListener(null);
    view7f0a00f6 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a041a.setOnClickListener(null);
    view7f0a041a = null;
  }
}
