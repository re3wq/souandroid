// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.accountmanagement.detail.importmnemonic;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ImportMnemonicActivity_ViewBinding implements Unbinder {
  private ImportMnemonicActivity target;

  private View view7f0a009f;

  private View view7f0a0151;

  @UiThread
  public ImportMnemonicActivity_ViewBinding(ImportMnemonicActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ImportMnemonicActivity_ViewBinding(final ImportMnemonicActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.qrCodeIv = Utils.findRequiredViewAsType(source, R.id.qrCodeIv, "field 'qrCodeIv'", ImageView.class);
    target.accountNameTv = Utils.findRequiredViewAsType(source, R.id.accountNameTv, "field 'accountNameTv'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.copyTv, "method 'onViewClicked'");
    view7f0a0151 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ImportMnemonicActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.qrCodeIv = null;
    target.accountNameTv = null;
    target.recyclerView = null;
    target.top = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a0151.setOnClickListener(null);
    view7f0a0151 = null;
  }
}
