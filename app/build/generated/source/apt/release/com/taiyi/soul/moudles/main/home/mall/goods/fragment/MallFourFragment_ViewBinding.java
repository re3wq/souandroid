// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.goods.fragment;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MallFourFragment_ViewBinding implements Unbinder {
  private MallFourFragment target;

  @UiThread
  public MallFourFragment_ViewBinding(MallFourFragment target, View source) {
    this.target = target;

    target.recycle = Utils.findRequiredViewAsType(source, R.id.mall_more_recycle, "field 'recycle'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MallFourFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recycle = null;
  }
}
