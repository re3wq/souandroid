// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.currency.currencydetail.fragment;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CurrencyDetailsThreeFragment_ViewBinding implements Unbinder {
  private CurrencyDetailsThreeFragment target;

  @UiThread
  public CurrencyDetailsThreeFragment_ViewBinding(CurrencyDetailsThreeFragment target,
      View source) {
    this.target = target;

    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", PullRecyclerView.class);
    target.noDataLl = Utils.findRequiredViewAsType(source, R.id.noDataLl, "field 'noDataLl'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CurrencyDetailsThreeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.noDataLl = null;
  }
}
