// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node.hashmarket.fragment;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HashMarketSellFragment_ViewBinding implements Unbinder {
  private HashMarketSellFragment target;

  private View view7f0a0407;

  @UiThread
  public HashMarketSellFragment_ViewBinding(final HashMarketSellFragment target, View source) {
    this.target = target;

    View view;
    target.mPayRecycle = Utils.findRequiredViewAsType(source, R.id.pay_recycle, "field 'mPayRecycle'", PullRecyclerView.class);
    view = Utils.findRequiredView(source, R.id.releaseTv, "method 'onViewClicked'");
    view7f0a0407 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    HashMarketSellFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mPayRecycle = null;

    view7f0a0407.setOnClickListener(null);
    view7f0a0407 = null;
  }
}
