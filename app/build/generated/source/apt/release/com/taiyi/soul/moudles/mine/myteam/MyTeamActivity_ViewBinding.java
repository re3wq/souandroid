// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.myteam;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import com.taiyi.soul.view.MyListView;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyTeamActivity_ViewBinding implements Unbinder {
  private MyTeamActivity target;

  private View view7f0a009f;

  private View view7f0a04d3;

  private View view7f0a03bd;

  private View view7f0a041b;

  private View view7f0a046c;

  private View view7f0a0444;

  @UiThread
  public MyTeamActivity_ViewBinding(MyTeamActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MyTeamActivity_ViewBinding(final MyTeamActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.backIv, "field 'backIv' and method 'onViewClicked'");
    target.backIv = Utils.castView(view, R.id.backIv, "field 'backIv'", ImageView.class);
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.teamTv, "field 'teamTv' and method 'onViewClicked'");
    target.teamTv = Utils.castView(view, R.id.teamTv, "field 'teamTv'", TextView.class);
    view7f0a04d3 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.directPushNumTv = Utils.findRequiredViewAsType(source, R.id.directPushNumTv, "field 'directPushNumTv'", TextView.class);
    target.currentLevelTv = Utils.findRequiredViewAsType(source, R.id.currentLevelTv, "field 'currentLevelTv'", TextView.class);
    target.todayAddPeopleNumTv = Utils.findRequiredViewAsType(source, R.id.todayAddPeopleNumTv, "field 'todayAddPeopleNumTv'", TextView.class);
    target.todayAddPerformanceTv = Utils.findRequiredViewAsType(source, R.id.todayAddPerformanceTv, "field 'todayAddPerformanceTv'", TextView.class);
    target.teamTotalPeopleTv = Utils.findRequiredViewAsType(source, R.id.teamTotalPeopleTv, "field 'teamTotalPeopleTv'", TextView.class);
    target.totalPerformanceTv = Utils.findRequiredViewAsType(source, R.id.totalPerformanceTv, "field 'totalPerformanceTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.performanceTv, "field 'performanceTv' and method 'onViewClicked'");
    target.performanceTv = Utils.castView(view, R.id.performanceTv, "field 'performanceTv'", TextView.class);
    view7f0a03bd = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.accountTv = Utils.findRequiredViewAsType(source, R.id.accountTv, "field 'accountTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rightTitleTv, "field 'rightTitleTv' and method 'onViewClicked'");
    target.rightTitleTv = Utils.castView(view, R.id.rightTitleTv, "field 'rightTitleTv'", TextView.class);
    view7f0a041b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.search_text = Utils.findRequiredViewAsType(source, R.id.search_text, "field 'search_text'", TextView.class);
    view = Utils.findRequiredView(source, R.id.show_all, "field 'show_all' and method 'onViewClicked'");
    target.show_all = Utils.castView(view, R.id.show_all, "field 'show_all'", TextView.class);
    view7f0a046c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.nodata = Utils.findRequiredViewAsType(source, R.id.nodata, "field 'nodata'", LinearLayout.class);
    target.nodata_two = Utils.findRequiredViewAsType(source, R.id.nodata_two, "field 'nodata_two'", LinearLayout.class);
    target.teamLl = Utils.findRequiredViewAsType(source, R.id.teamLl, "field 'teamLl'", LinearLayout.class);
    target.performanceLl = Utils.findRequiredViewAsType(source, R.id.performanceLl, "field 'performanceLl'", LinearLayout.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", PullRecyclerView.class);
    target.springView = Utils.findRequiredViewAsType(source, R.id.springView, "field 'springView'", SpringView.class);
    target.lv = Utils.findRequiredViewAsType(source, R.id.lv, "field 'lv'", MyListView.class);
    target.avatarIv = Utils.findRequiredViewAsType(source, R.id.avatarIv, "field 'avatarIv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.search, "method 'onViewClicked'");
    view7f0a0444 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MyTeamActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backIv = null;
    target.titleTv = null;
    target.teamTv = null;
    target.directPushNumTv = null;
    target.currentLevelTv = null;
    target.todayAddPeopleNumTv = null;
    target.todayAddPerformanceTv = null;
    target.teamTotalPeopleTv = null;
    target.totalPerformanceTv = null;
    target.performanceTv = null;
    target.accountTv = null;
    target.rightTitleTv = null;
    target.search_text = null;
    target.show_all = null;
    target.nodata = null;
    target.nodata_two = null;
    target.teamLl = null;
    target.performanceLl = null;
    target.top = null;
    target.recyclerView = null;
    target.springView = null;
    target.lv = null;
    target.avatarIv = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a04d3.setOnClickListener(null);
    view7f0a04d3 = null;
    view7f0a03bd.setOnClickListener(null);
    view7f0a03bd = null;
    view7f0a041b.setOnClickListener(null);
    view7f0a041b = null;
    view7f0a046c.setOnClickListener(null);
    view7f0a046c = null;
    view7f0a0444.setOnClickListener(null);
    view7f0a0444 = null;
  }
}
