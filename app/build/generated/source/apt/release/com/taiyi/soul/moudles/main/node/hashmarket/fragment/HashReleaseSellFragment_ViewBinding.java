// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node.hashmarket.fragment;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HashReleaseSellFragment_ViewBinding implements Unbinder {
  private HashReleaseSellFragment target;

  private View view7f0a0407;

  @UiThread
  public HashReleaseSellFragment_ViewBinding(final HashReleaseSellFragment target, View source) {
    this.target = target;

    View view;
    target.mMarketMaxPriceTv = Utils.findRequiredViewAsType(source, R.id.marketMaxPriceTv, "field 'mMarketMaxPriceTv'", TextView.class);
    target.mPrice = Utils.findRequiredViewAsType(source, R.id.price, "field 'mPrice'", EditText.class);
    target.mNum = Utils.findRequiredViewAsType(source, R.id.num, "field 'mNum'", EditText.class);
    target.mSellBalanceTv = Utils.findRequiredViewAsType(source, R.id.sellBalanceTv, "field 'mSellBalanceTv'", TextView.class);
    target.mSellTotalTv = Utils.findRequiredViewAsType(source, R.id.sellTotalTv, "field 'mSellTotalTv'", TextView.class);
    target.mFeeTv = Utils.findRequiredViewAsType(source, R.id.feeTv, "field 'mFeeTv'", TextView.class);
    target.fee_price = Utils.findRequiredViewAsType(source, R.id.fee_price, "field 'fee_price'", TextView.class);
    target.mGetItTotalTv = Utils.findRequiredViewAsType(source, R.id.getItTotalTv, "field 'mGetItTotalTv'", TextView.class);
    target.availableTv = Utils.findRequiredViewAsType(source, R.id.availableTv, "field 'availableTv'", TextView.class);
    target.key_main = Utils.findRequiredViewAsType(source, R.id.key_main, "field 'key_main'", RelativeLayout.class);
    target.key_scroll = Utils.findRequiredViewAsType(source, R.id.key_scroll, "field 'key_scroll'", LinearLayout.class);
    target.keyboardPlace = Utils.findRequiredViewAsType(source, R.id.keyboardPlace, "field 'keyboardPlace'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.releaseTv, "method 'onViewClicked'");
    view7f0a0407 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    HashReleaseSellFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mMarketMaxPriceTv = null;
    target.mPrice = null;
    target.mNum = null;
    target.mSellBalanceTv = null;
    target.mSellTotalTv = null;
    target.mFeeTv = null;
    target.fee_price = null;
    target.mGetItTotalTv = null;
    target.availableTv = null;
    target.key_main = null;
    target.key_scroll = null;
    target.keyboardPlace = null;

    view7f0a0407.setOnClickListener(null);
    view7f0a0407 = null;
  }
}
