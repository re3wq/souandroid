// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.options.fragment;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OptionsListThreeFragment_ViewBinding implements Unbinder {
  private OptionsListThreeFragment target;

  @UiThread
  public OptionsListThreeFragment_ViewBinding(OptionsListThreeFragment target, View source) {
    this.target = target;

    target.recycle = Utils.findRequiredViewAsType(source, R.id.recycle, "field 'recycle'", PullRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OptionsListThreeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recycle = null;
  }
}
