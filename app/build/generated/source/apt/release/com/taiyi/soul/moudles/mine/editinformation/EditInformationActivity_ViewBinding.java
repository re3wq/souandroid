// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.editinformation;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditInformationActivity_ViewBinding implements Unbinder {
  private EditInformationActivity target;

  private View view7f0a0465;

  private View view7f0a00c6;

  private View view7f0a009f;

  private View view7f0a013c;

  @UiThread
  public EditInformationActivity_ViewBinding(EditInformationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EditInformationActivity_ViewBinding(final EditInformationActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.nameEt = Utils.findRequiredViewAsType(source, R.id.nameEt, "field 'nameEt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.sexTv, "field 'sexTv' and method 'onViewClicked'");
    target.sexTv = Utils.castView(view, R.id.sexTv, "field 'sexTv'", TextView.class);
    view7f0a0465 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.birthdayTv, "field 'birthdayTv' and method 'onViewClicked'");
    target.birthdayTv = Utils.castView(view, R.id.birthdayTv, "field 'birthdayTv'", TextView.class);
    view7f0a00c6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.addressTv = Utils.findRequiredViewAsType(source, R.id.addressTv, "field 'addressTv'", EditText.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.completeTv, "method 'onViewClicked'");
    view7f0a013c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EditInformationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.nameEt = null;
    target.sexTv = null;
    target.birthdayTv = null;
    target.addressTv = null;
    target.top = null;

    view7f0a0465.setOnClickListener(null);
    view7f0a0465 = null;
    view7f0a00c6.setOnClickListener(null);
    view7f0a00c6 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a013c.setOnClickListener(null);
    view7f0a013c = null;
  }
}
