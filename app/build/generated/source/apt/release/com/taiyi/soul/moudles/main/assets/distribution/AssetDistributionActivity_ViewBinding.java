// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.distribution;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.mikephil.charting.charts.PieChart;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AssetDistributionActivity_ViewBinding implements Unbinder {
  private AssetDistributionActivity target;

  private View view7f0a028e;

  @UiThread
  public AssetDistributionActivity_ViewBinding(AssetDistributionActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AssetDistributionActivity_ViewBinding(final AssetDistributionActivity target,
      View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    target.mPieChart = Utils.findRequiredViewAsType(source, R.id.pie_chart, "field 'mPieChart'", PieChart.class);
    target.mPieRecycle = Utils.findRequiredViewAsType(source, R.id.pie_recycle, "field 'mPieRecycle'", RecyclerView.class);
    target.mPercentage = Utils.findRequiredViewAsType(source, R.id.percentage, "field 'mPercentage'", TextView.class);
    target.mUnit = Utils.findRequiredViewAsType(source, R.id.unit, "field 'mUnit'", TextView.class);
    target.mAssetTit = Utils.findRequiredViewAsType(source, R.id.asset_tit, "field 'mAssetTit'", TextView.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AssetDistributionActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mPieChart = null;
    target.mPieRecycle = null;
    target.mPercentage = null;
    target.mUnit = null;
    target.mAssetTit = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
  }
}
