// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.transfer;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TransferActivity_ViewBinding implements Unbinder {
  private TransferActivity target;

  private View view7f0a0289;

  private View view7f0a012d;

  private View view7f0a0236;

  private View view7f0a009f;

  private View view7f0a043a;

  private View view7f0a0515;

  private View view7f0a0514;

  @UiThread
  public TransferActivity_ViewBinding(TransferActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public TransferActivity_ViewBinding(final TransferActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.coinIv = Utils.findRequiredViewAsType(source, R.id.coinIv, "field 'coinIv'", ImageView.class);
    target.coinNameTv = Utils.findRequiredViewAsType(source, R.id.coinNameTv, "field 'coinNameTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.iv, "field 'iv' and method 'onViewClicked'");
    target.iv = Utils.castView(view, R.id.iv, "field 'iv'", ImageView.class);
    view7f0a0289 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.balanceTv = Utils.findRequiredViewAsType(source, R.id.balanceTv, "field 'balanceTv'", TextView.class);
    target.transactionAmountEt = Utils.findRequiredViewAsType(source, R.id.transactionAmountEt, "field 'transactionAmountEt'", EditText.class);
    target.collectMoneyAddressEt = Utils.findRequiredViewAsType(source, R.id.collectMoneyAddressEt, "field 'collectMoneyAddressEt'", EditText.class);
    target.minerFee = Utils.findRequiredViewAsType(source, R.id.minerFee, "field 'minerFee'", TextView.class);
    target.downIv = Utils.findRequiredViewAsType(source, R.id.downIv, "field 'downIv'", ImageView.class);
    target.minerFeeTv = Utils.findRequiredViewAsType(source, R.id.minerFeeTv, "field 'minerFeeTv'", TextView.class);
    target.transfer_failed_hint = Utils.findRequiredViewAsType(source, R.id.transfer_failed_hint, "field 'transfer_failed_hint'", TextView.class);
    target.transfer_eos_hint = Utils.findRequiredViewAsType(source, R.id.transfer_eos_hint, "field 'transfer_eos_hint'", TextView.class);
    target.seekBar = Utils.findRequiredViewAsType(source, R.id.seekBar, "field 'seekBar'", SeekBar.class);
    view = Utils.findRequiredView(source, R.id.coinCl, "field 'coinCl' and method 'onViewClicked'");
    target.coinCl = Utils.castView(view, R.id.coinCl, "field 'coinCl'", ConstraintLayout.class);
    view7f0a012d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.group, "field 'group' and method 'onViewClicked'");
    target.group = Utils.castView(view, R.id.group, "field 'group'", LinearLayout.class);
    view7f0a0236 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.key_main = Utils.findRequiredViewAsType(source, R.id.key_main, "field 'key_main'", RelativeLayout.class);
    target.key_scroll = Utils.findRequiredViewAsType(source, R.id.key_scroll, "field 'key_scroll'", LinearLayout.class);
    target.keyboardPlace = Utils.findRequiredViewAsType(source, R.id.keyboardPlace, "field 'keyboardPlace'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.scanIv, "method 'onViewClicked'");
    view7f0a043a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.transferTv, "method 'onViewClicked'");
    view7f0a0515 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.transferOutAllTv, "method 'onViewClicked'");
    view7f0a0514 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    TransferActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.coinIv = null;
    target.coinNameTv = null;
    target.iv = null;
    target.balanceTv = null;
    target.transactionAmountEt = null;
    target.collectMoneyAddressEt = null;
    target.minerFee = null;
    target.downIv = null;
    target.minerFeeTv = null;
    target.transfer_failed_hint = null;
    target.transfer_eos_hint = null;
    target.seekBar = null;
    target.coinCl = null;
    target.top = null;
    target.group = null;
    target.key_main = null;
    target.key_scroll = null;
    target.keyboardPlace = null;

    view7f0a0289.setOnClickListener(null);
    view7f0a0289 = null;
    view7f0a012d.setOnClickListener(null);
    view7f0a012d = null;
    view7f0a0236.setOnClickListener(null);
    view7f0a0236 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a043a.setOnClickListener(null);
    view7f0a043a = null;
    view7f0a0515.setOnClickListener(null);
    view7f0a0515 = null;
    view7f0a0514.setOnClickListener(null);
    view7f0a0514 = null;
  }
}
