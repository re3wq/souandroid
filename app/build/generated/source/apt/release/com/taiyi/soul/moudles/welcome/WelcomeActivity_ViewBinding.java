// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.welcome;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.taiyi.soul.view.CustomVideoView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WelcomeActivity_ViewBinding implements Unbinder {
  private WelcomeActivity target;

  private View view7f0a058a;

  @UiThread
  public WelcomeActivity_ViewBinding(WelcomeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WelcomeActivity_ViewBinding(final WelcomeActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.videoview, "field 'videoview' and method 'onViewClicked'");
    target.videoview = Utils.castView(view, R.id.videoview, "field 'videoview'", CustomVideoView.class);
    view7f0a058a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.welcome_iv = Utils.findRequiredViewAsType(source, R.id.welcome_iv, "field 'welcome_iv'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WelcomeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.videoview = null;
    target.welcome_iv = null;

    view7f0a058a.setOnClickListener(null);
    view7f0a058a = null;
  }
}
