// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.setup.update;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NewVersionUpdateActivity_ViewBinding implements Unbinder {
  private NewVersionUpdateActivity target;

  private View view7f0a0174;

  private View view7f0a009f;

  @UiThread
  public NewVersionUpdateActivity_ViewBinding(NewVersionUpdateActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public NewVersionUpdateActivity_ViewBinding(final NewVersionUpdateActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.currentVersionTv, "field 'currentVersionTv' and method 'onViewClicked'");
    target.currentVersionTv = Utils.castView(view, R.id.currentVersionTv, "field 'currentVersionTv'", TextView.class);
    view7f0a0174 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    NewVersionUpdateActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.currentVersionTv = null;
    target.top = null;

    view7f0a0174.setOnClickListener(null);
    view7f0a0174 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
