// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.securitycenter.updatephonenumber;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BindPhoneNumberActivity_ViewBinding implements Unbinder {
  private BindPhoneNumberActivity target;

  private View view7f0a0154;

  private View view7f0a0289;

  private View view7f0a009f;

  private View view7f0a0217;

  private View view7f0a03c4;

  private View view7f0a035c;

  @UiThread
  public BindPhoneNumberActivity_ViewBinding(BindPhoneNumberActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BindPhoneNumberActivity_ViewBinding(final BindPhoneNumberActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.tv = Utils.findRequiredViewAsType(source, R.id.tv, "field 'tv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.countryCodeTv, "field 'countryCodeTv' and method 'onViewClicked'");
    target.countryCodeTv = Utils.castView(view, R.id.countryCodeTv, "field 'countryCodeTv'", TextView.class);
    view7f0a0154 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv, "field 'iv' and method 'onViewClicked'");
    target.iv = Utils.castView(view, R.id.iv, "field 'iv'", ImageView.class);
    view7f0a0289 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.backIv, "field 'backIv' and method 'onViewClicked'");
    target.backIv = Utils.castView(view, R.id.backIv, "field 'backIv'", ImageView.class);
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.phoneNumberEt = Utils.findRequiredViewAsType(source, R.id.phoneNumberEt, "field 'phoneNumberEt'", EditText.class);
    target.verificationCodeEt = Utils.findRequiredViewAsType(source, R.id.verificationCodeEt, "field 'verificationCodeEt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.getVerificationCodeTv, "field 'getVerificationCodeTv' and method 'onViewClicked'");
    target.getVerificationCodeTv = Utils.castView(view, R.id.getVerificationCodeTv, "field 'getVerificationCodeTv'", TextView.class);
    view7f0a0217 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ll = Utils.findRequiredViewAsType(source, R.id.ll, "field 'll'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.pop_ll, "field 'pop_ll' and method 'onViewClicked'");
    target.pop_ll = Utils.castView(view, R.id.pop_ll, "field 'pop_ll'", LinearLayout.class);
    view7f0a03c4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.nextStepTv, "method 'onViewClicked'");
    view7f0a035c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    BindPhoneNumberActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.tv = null;
    target.countryCodeTv = null;
    target.iv = null;
    target.backIv = null;
    target.phoneNumberEt = null;
    target.verificationCodeEt = null;
    target.getVerificationCodeTv = null;
    target.ll = null;
    target.pop_ll = null;
    target.top = null;

    view7f0a0154.setOnClickListener(null);
    view7f0a0154 = null;
    view7f0a0289.setOnClickListener(null);
    view7f0a0289 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a0217.setOnClickListener(null);
    view7f0a0217 = null;
    view7f0a03c4.setOnClickListener(null);
    view7f0a03c4 = null;
    view7f0a035c.setOnClickListener(null);
    view7f0a035c = null;
  }
}
