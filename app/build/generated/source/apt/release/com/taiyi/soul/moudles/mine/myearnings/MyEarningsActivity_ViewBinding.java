// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.myearnings;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import com.taiyi.soul.view.HorizontalCanScrollViewPager;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyEarningsActivity_ViewBinding implements Unbinder {
  private MyEarningsActivity target;

  private View view7f0a009f;

  @UiThread
  public MyEarningsActivity_ViewBinding(MyEarningsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MyEarningsActivity_ViewBinding(final MyEarningsActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.slidingTabLayout = Utils.findRequiredViewAsType(source, R.id.slidingTabLayout, "field 'slidingTabLayout'", SlidingTabLayout.class);
    target.viewPager = Utils.findRequiredViewAsType(source, R.id.viewPager, "field 'viewPager'", HorizontalCanScrollViewPager.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MyEarningsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.top = null;
    target.slidingTabLayout = null;
    target.viewPager = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
