// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.goods;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.yanzhenjie.recyclerview.SwipeRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ShoppingCartActivity_ViewBinding implements Unbinder {
  private ShoppingCartActivity target;

  private View view7f0a028e;

  private View view7f0a00ff;

  @UiThread
  public ShoppingCartActivity_ViewBinding(ShoppingCartActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ShoppingCartActivity_ViewBinding(final ShoppingCartActivity target, View source) {
    this.target = target;

    View view;
    target.mCartRecycle = Utils.findRequiredViewAsType(source, R.id.cart_recycle, "field 'mCartRecycle'", SwipeRecyclerView.class);
    target.cart_total = Utils.findRequiredViewAsType(source, R.id.cart_total, "field 'cart_total'", TextView.class);
    target.nodata = Utils.findRequiredViewAsType(source, R.id.nodata, "field 'nodata'", LinearLayout.class);
    target.mSwipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipe_layout, "field 'mSwipeRefreshLayout'", SwipeRefreshLayout.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.cart_next, "method 'onViewClicked'");
    view7f0a00ff = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ShoppingCartActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mCartRecycle = null;
    target.cart_total = null;
    target.nodata = null;
    target.mSwipeRefreshLayout = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a00ff.setOnClickListener(null);
    view7f0a00ff = null;
  }
}
