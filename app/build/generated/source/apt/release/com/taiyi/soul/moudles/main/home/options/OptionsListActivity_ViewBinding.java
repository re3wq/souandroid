// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.options;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OptionsListActivity_ViewBinding implements Unbinder {
  private OptionsListActivity target;

  private View view7f0a028e;

  private View view7f0a0256;

  @UiThread
  public OptionsListActivity_ViewBinding(OptionsListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OptionsListActivity_ViewBinding(final OptionsListActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    target.mTvRightText = Utils.findRequiredViewAsType(source, R.id.tv_right_text, "field 'mTvRightText'", TextView.class);
    target.mTabLayout = Utils.findRequiredViewAsType(source, R.id.tabLayout, "field 'mTabLayout'", SlidingTabLayout.class);
    target.mViewPager = Utils.findRequiredViewAsType(source, R.id.view_pager, "field 'mViewPager'", ViewPager.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_car, "method 'onViewClicked'");
    view7f0a0256 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    OptionsListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mTvRightText = null;
    target.mTabLayout = null;
    target.mViewPager = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a0256.setOnClickListener(null);
    view7f0a0256 = null;
  }
}
