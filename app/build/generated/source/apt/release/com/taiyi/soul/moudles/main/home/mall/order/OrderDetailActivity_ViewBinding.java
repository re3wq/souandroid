// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.order;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderDetailActivity_ViewBinding implements Unbinder {
  private OrderDetailActivity target;

  private View view7f0a0224;

  private View view7f0a0223;

  private View view7f0a022a;

  private View view7f0a022d;

  private View view7f0a0226;

  private View view7f0a0225;

  private View view7f0a028e;

  @UiThread
  public OrderDetailActivity_ViewBinding(OrderDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderDetailActivity_ViewBinding(final OrderDetailActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    target.mOrderState = Utils.findRequiredViewAsType(source, R.id.order_state, "field 'mOrderState'", TextView.class);
    target.mOrderName = Utils.findRequiredViewAsType(source, R.id.order_name, "field 'mOrderName'", TextView.class);
    target.mOrderPhone = Utils.findRequiredViewAsType(source, R.id.order_phone, "field 'mOrderPhone'", TextView.class);
    target.mOrderAddress = Utils.findRequiredViewAsType(source, R.id.order_address, "field 'mOrderAddress'", TextView.class);
    target.mGoodsRecycle = Utils.findRequiredViewAsType(source, R.id.goods_recycle, "field 'mGoodsRecycle'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.goods_change_address, "field 'mGoodsChangeAddress' and method 'onViewClicked'");
    target.mGoodsChangeAddress = Utils.castView(view, R.id.goods_change_address, "field 'mGoodsChangeAddress'", TextView.class);
    view7f0a0224 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.goods_cancel_order, "field 'mGoodsCancelOrder' and method 'onViewClicked'");
    target.mGoodsCancelOrder = Utils.castView(view, R.id.goods_cancel_order, "field 'mGoodsCancelOrder'", TextView.class);
    view7f0a0223 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.goods_pay, "field 'mGoodsPay' and method 'onViewClicked'");
    target.mGoodsPay = Utils.castView(view, R.id.goods_pay, "field 'mGoodsPay'", TextView.class);
    view7f0a022a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.goods_remind_shipment, "field 'mGoodsRemindShipment' and method 'onViewClicked'");
    target.mGoodsRemindShipment = Utils.castView(view, R.id.goods_remind_shipment, "field 'mGoodsRemindShipment'", TextView.class);
    view7f0a022d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.goods_confirm_receipt, "field 'mGoodsConfirmReceipt' and method 'onViewClicked'");
    target.mGoodsConfirmReceipt = Utils.castView(view, R.id.goods_confirm_receipt, "field 'mGoodsConfirmReceipt'", TextView.class);
    view7f0a0226 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.goods_confirm_finish, "field 'mGoodsConfirmFinish' and method 'onViewClicked'");
    target.mGoodsConfirmFinish = Utils.castView(view, R.id.goods_confirm_finish, "field 'mGoodsConfirmFinish'", TextView.class);
    view7f0a0225 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mOrderItem = Utils.findRequiredViewAsType(source, R.id.order_item, "field 'mOrderItem'", LinearLayout.class);
    target.mOrderPrice = Utils.findRequiredViewAsType(source, R.id.order_price, "field 'mOrderPrice'", TextView.class);
    target.mOrderFreight = Utils.findRequiredViewAsType(source, R.id.order_freight, "field 'mOrderFreight'", TextView.class);
    target.mOrderTotal = Utils.findRequiredViewAsType(source, R.id.order_total, "field 'mOrderTotal'", TextView.class);
    target.mOrderNum = Utils.findRequiredViewAsType(source, R.id.order_num, "field 'mOrderNum'", TextView.class);
    target.mOrderTime = Utils.findRequiredViewAsType(source, R.id.order_time, "field 'mOrderTime'", TextView.class);
    target.order_pay_type = Utils.findRequiredViewAsType(source, R.id.order_pay_type, "field 'order_pay_type'", TextView.class);
    target.ll_order_pay_time = Utils.findRequiredViewAsType(source, R.id.ll_order_pay_time, "field 'll_order_pay_time'", LinearLayout.class);
    target.ll_pay_type = Utils.findRequiredViewAsType(source, R.id.ll_pay_type, "field 'll_pay_type'", LinearLayout.class);
    target.mOrderPayTime = Utils.findRequiredViewAsType(source, R.id.order_pay_time, "field 'mOrderPayTime'", TextView.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mOrderState = null;
    target.mOrderName = null;
    target.mOrderPhone = null;
    target.mOrderAddress = null;
    target.mGoodsRecycle = null;
    target.mGoodsChangeAddress = null;
    target.mGoodsCancelOrder = null;
    target.mGoodsPay = null;
    target.mGoodsRemindShipment = null;
    target.mGoodsConfirmReceipt = null;
    target.mGoodsConfirmFinish = null;
    target.mOrderItem = null;
    target.mOrderPrice = null;
    target.mOrderFreight = null;
    target.mOrderTotal = null;
    target.mOrderNum = null;
    target.mOrderTime = null;
    target.order_pay_type = null;
    target.ll_order_pay_time = null;
    target.ll_pay_type = null;
    target.mOrderPayTime = null;

    view7f0a0224.setOnClickListener(null);
    view7f0a0224 = null;
    view7f0a0223.setOnClickListener(null);
    view7f0a0223 = null;
    view7f0a022a.setOnClickListener(null);
    view7f0a022a = null;
    view7f0a022d.setOnClickListener(null);
    view7f0a022d = null;
    view7f0a0226.setOnClickListener(null);
    view7f0a0226 = null;
    view7f0a0225.setOnClickListener(null);
    view7f0a0225 = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
  }
}
