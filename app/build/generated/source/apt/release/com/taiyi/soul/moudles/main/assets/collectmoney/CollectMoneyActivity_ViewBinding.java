// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.collectmoney;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CollectMoneyActivity_ViewBinding implements Unbinder {
  private CollectMoneyActivity target;

  private View view7f0a009f;

  private View view7f0a012d;

  private View view7f0a02bc;

  private View view7f0a0418;

  private View view7f0a014d;

  @UiThread
  public CollectMoneyActivity_ViewBinding(CollectMoneyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CollectMoneyActivity_ViewBinding(final CollectMoneyActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.coinIv = Utils.findRequiredViewAsType(source, R.id.coinIv, "field 'coinIv'", ImageView.class);
    target.coinNameTv = Utils.findRequiredViewAsType(source, R.id.coinNameTv, "field 'coinNameTv'", TextView.class);
    target.iv = Utils.findRequiredViewAsType(source, R.id.iv, "field 'iv'", ImageView.class);
    target.qrCodeIv = Utils.findRequiredViewAsType(source, R.id.qrCodeIv, "field 'qrCodeIv'", ImageView.class);
    target.selectCoinIv = Utils.findRequiredViewAsType(source, R.id.selectCoinIv, "field 'selectCoinIv'", ImageView.class);
    target.recyclerViewD = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerViewD'", RecyclerView.class);
    target.addressTv = Utils.findRequiredViewAsType(source, R.id.addressTv, "field 'addressTv'", TextView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.describeTv = Utils.findRequiredViewAsType(source, R.id.describeTv, "field 'describeTv'", TextView.class);
    target.dateTv = Utils.findRequiredViewAsType(source, R.id.dateTv, "field 'dateTv'", TextView.class);
    target.quantityTv = Utils.findRequiredViewAsType(source, R.id.quantityTv, "field 'quantityTv'", TextView.class);
    target.addressTv1 = Utils.findRequiredViewAsType(source, R.id.addressTv1, "field 'addressTv1'", TextView.class);
    target.whitePagerTv = Utils.findRequiredViewAsType(source, R.id.whitePagerTv, "field 'whitePagerTv'", TextView.class);
    target.queryTv = Utils.findRequiredViewAsType(source, R.id.queryTv, "field 'queryTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.coinCl, "method 'onViewClicked'");
    view7f0a012d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.leftIv, "method 'onViewClicked'");
    view7f0a02bc = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rightAddIv, "method 'onViewClicked'");
    view7f0a0418 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.copyAddressTv, "method 'onViewClicked'");
    view7f0a014d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CollectMoneyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.coinIv = null;
    target.coinNameTv = null;
    target.iv = null;
    target.qrCodeIv = null;
    target.selectCoinIv = null;
    target.recyclerViewD = null;
    target.addressTv = null;
    target.top = null;
    target.describeTv = null;
    target.dateTv = null;
    target.quantityTv = null;
    target.addressTv1 = null;
    target.whitePagerTv = null;
    target.queryTv = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a012d.setOnClickListener(null);
    view7f0a012d = null;
    view7f0a02bc.setOnClickListener(null);
    view7f0a02bc = null;
    view7f0a0418.setOnClickListener(null);
    view7f0a0418 = null;
    view7f0a014d.setOnClickListener(null);
    view7f0a014d = null;
  }
}
