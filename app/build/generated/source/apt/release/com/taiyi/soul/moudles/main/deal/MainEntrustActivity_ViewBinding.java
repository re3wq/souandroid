// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.deal;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainEntrustActivity_ViewBinding implements Unbinder {
  private MainEntrustActivity target;

  private View view7f0a028e;

  @UiThread
  public MainEntrustActivity_ViewBinding(MainEntrustActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainEntrustActivity_ViewBinding(final MainEntrustActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    target.mEntrustRecycle = Utils.findRequiredViewAsType(source, R.id.entrust_recycle, "field 'mEntrustRecycle'", RecyclerView.class);
    target.mEntrustTab = Utils.findRequiredViewAsType(source, R.id.entrust_tab, "field 'mEntrustTab'", SlidingTabLayout.class);
    target.mEntrustViewpager = Utils.findRequiredViewAsType(source, R.id.entrust_viewpager, "field 'mEntrustViewpager'", ViewPager.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MainEntrustActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mEntrustRecycle = null;
    target.mEntrustTab = null;
    target.mEntrustViewpager = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
  }
}
