// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.setup.signout;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignOutActivity_ViewBinding implements Unbinder {
  private SignOutActivity target;

  private View view7f0a009f;

  private View view7f0a00f9;

  private View view7f0a0142;

  @UiThread
  public SignOutActivity_ViewBinding(SignOutActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SignOutActivity_ViewBinding(final SignOutActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.cancelTv, "method 'onViewClicked'");
    view7f0a00f9 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.confirmTv, "method 'onViewClicked'");
    view7f0a0142 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SignOutActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.top = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a00f9.setOnClickListener(null);
    view7f0a00f9 = null;
    view7f0a0142.setOnClickListener(null);
    view7f0a0142 = null;
  }
}
