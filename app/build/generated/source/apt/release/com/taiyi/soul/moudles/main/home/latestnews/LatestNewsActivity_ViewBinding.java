// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.latestnews;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.liaoinstan.springview.widget.SpringView;
import com.taiyi.soul.R;
import com.zhouwei.mzbanner.MZBannerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LatestNewsActivity_ViewBinding implements Unbinder {
  private LatestNewsActivity target;

  private View view7f0a009f;

  @UiThread
  public LatestNewsActivity_ViewBinding(LatestNewsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LatestNewsActivity_ViewBinding(final LatestNewsActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.springView = Utils.findRequiredViewAsType(source, R.id.springView, "field 'springView'", SpringView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.banner = Utils.findRequiredViewAsType(source, R.id.banner, "field 'banner'", MZBannerView.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LatestNewsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.recyclerView = null;
    target.springView = null;
    target.top = null;
    target.banner = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
