// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main;

import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.mRgBottomMenu = Utils.findRequiredViewAsType(source, R.id.rg_bottom_menu, "field 'mRgBottomMenu'", RadioGroup.class);
    target.iv_home1 = Utils.findRequiredViewAsType(source, R.id.iv_home1, "field 'iv_home1'", RadioButton.class);
    target.iv_assets1 = Utils.findRequiredViewAsType(source, R.id.iv_assets1, "field 'iv_assets1'", RadioButton.class);
    target.iv_node1 = Utils.findRequiredViewAsType(source, R.id.iv_node1, "field 'iv_node1'", RadioButton.class);
    target.iv_transaction1 = Utils.findRequiredViewAsType(source, R.id.iv_transaction1, "field 'iv_transaction1'", RadioButton.class);
    target.iv_mine1 = Utils.findRequiredViewAsType(source, R.id.iv_mine1, "field 'iv_mine1'", RadioButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRgBottomMenu = null;
    target.iv_home1 = null;
    target.iv_assets1 = null;
    target.iv_node1 = null;
    target.iv_transaction1 = null;
    target.iv_mine1 = null;
  }
}
