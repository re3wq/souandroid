// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.order;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.yanzhenjie.recyclerview.SwipeRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckConfirmActivity_ViewBinding implements Unbinder {
  private CheckConfirmActivity target;

  private View view7f0a011a;

  private View view7f0a028e;

  private View view7f0a0116;

  @UiThread
  public CheckConfirmActivity_ViewBinding(CheckConfirmActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CheckConfirmActivity_ViewBinding(final CheckConfirmActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    target.mCheckOrderRecycle = Utils.findRequiredViewAsType(source, R.id.check_order_recycle, "field 'mCheckOrderRecycle'", SwipeRecyclerView.class);
    target.mCheckOrderFreight = Utils.findRequiredViewAsType(source, R.id.check_order_freight, "field 'mCheckOrderFreight'", TextView.class);
    target.mCheckOrderPrice = Utils.findRequiredViewAsType(source, R.id.check_order_price, "field 'mCheckOrderPrice'", TextView.class);
    target.mCheckOrderName = Utils.findRequiredViewAsType(source, R.id.check_order_name, "field 'mCheckOrderName'", TextView.class);
    target.mCheckOrderPhone = Utils.findRequiredViewAsType(source, R.id.check_order_phone, "field 'mCheckOrderPhone'", TextView.class);
    target.mCheckOrderAddress = Utils.findRequiredViewAsType(source, R.id.check_order_address, "field 'mCheckOrderAddress'", TextView.class);
    target.mCheckOrderTotal = Utils.findRequiredViewAsType(source, R.id.check_order_total, "field 'mCheckOrderTotal'", TextView.class);
    view = Utils.findRequiredView(source, R.id.check_order_next, "field 'mCheckOrderNext' and method 'onViewClicked'");
    target.mCheckOrderNext = Utils.castView(view, R.id.check_order_next, "field 'mCheckOrderNext'", TextView.class);
    view7f0a011a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.check_order_address_more, "method 'onViewClicked'");
    view7f0a0116 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CheckConfirmActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mCheckOrderRecycle = null;
    target.mCheckOrderFreight = null;
    target.mCheckOrderPrice = null;
    target.mCheckOrderName = null;
    target.mCheckOrderPhone = null;
    target.mCheckOrderAddress = null;
    target.mCheckOrderTotal = null;
    target.mCheckOrderNext = null;

    view7f0a011a.setOnClickListener(null);
    view7f0a011a = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a0116.setOnClickListener(null);
    view7f0a0116 = null;
  }
}
