// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.securitycenter.updateemail;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BindEmailActivity_ViewBinding implements Unbinder {
  private BindEmailActivity target;

  private View view7f0a009f;

  private View view7f0a0217;

  private View view7f0a035c;

  @UiThread
  public BindEmailActivity_ViewBinding(BindEmailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BindEmailActivity_ViewBinding(final BindEmailActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.logoIv = Utils.findRequiredViewAsType(source, R.id.logoIv, "field 'logoIv'", ImageView.class);
    target.tv = Utils.findRequiredViewAsType(source, R.id.tv, "field 'tv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.backIv, "field 'backIv' and method 'onViewClicked'");
    target.backIv = Utils.castView(view, R.id.backIv, "field 'backIv'", ImageView.class);
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.emailAddressEt = Utils.findRequiredViewAsType(source, R.id.emailAddressEt, "field 'emailAddressEt'", EditText.class);
    target.verificationCodeEt = Utils.findRequiredViewAsType(source, R.id.verificationCodeEt, "field 'verificationCodeEt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.getVerificationCodeTv, "field 'getVerificationCodeTv' and method 'onViewClicked'");
    target.getVerificationCodeTv = Utils.castView(view, R.id.getVerificationCodeTv, "field 'getVerificationCodeTv'", TextView.class);
    view7f0a0217 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ll = Utils.findRequiredViewAsType(source, R.id.ll, "field 'll'", LinearLayout.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.nextStepTv, "method 'onViewClicked'");
    view7f0a035c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    BindEmailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.logoIv = null;
    target.tv = null;
    target.backIv = null;
    target.emailAddressEt = null;
    target.verificationCodeEt = null;
    target.getVerificationCodeTv = null;
    target.ll = null;
    target.top = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a0217.setOnClickListener(null);
    view7f0a0217 = null;
    view7f0a035c.setOnClickListener(null);
    view7f0a035c = null;
  }
}
