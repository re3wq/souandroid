// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.securitycenter.fingerprint;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FingerprintEnterActivity_ViewBinding implements Unbinder {
  private FingerprintEnterActivity target;

  private View view7f0a03ef;

  private View view7f0a00a0;

  private View view7f0a009f;

  @UiThread
  public FingerprintEnterActivity_ViewBinding(FingerprintEnterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FingerprintEnterActivity_ViewBinding(final FingerprintEnterActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.iv = Utils.findRequiredViewAsType(source, R.id.iv, "field 'iv'", ImageView.class);
    target.resultTv = Utils.findRequiredViewAsType(source, R.id.resultTv, "field 'resultTv'", TextView.class);
    target.resultReasonTv = Utils.findRequiredViewAsType(source, R.id.resultReasonTv, "field 'resultReasonTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.reRegisterTv, "field 'reRegisterTv' and method 'onViewClicked'");
    target.reRegisterTv = Utils.castView(view, R.id.reRegisterTv, "field 'reRegisterTv'", TextView.class);
    view7f0a03ef = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.backTv, "field 'backTv' and method 'onViewClicked'");
    target.backTv = Utils.castView(view, R.id.backTv, "field 'backTv'", TextView.class);
    view7f0a00a0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    FingerprintEnterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.iv = null;
    target.resultTv = null;
    target.resultReasonTv = null;
    target.reRegisterTv = null;
    target.backTv = null;
    target.top = null;

    view7f0a03ef.setOnClickListener(null);
    view7f0a03ef = null;
    view7f0a00a0.setOnClickListener(null);
    view7f0a00a0 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
