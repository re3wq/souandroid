// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.register;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterActivity_ViewBinding implements Unbinder {
  private RegisterActivity target;

  private View view7f0a028e;

  private View view7f0a0535;

  private View view7f0a02ef;

  private View view7f0a0553;

  private View view7f0a052f;

  private View view7f0a028c;

  private View view7f0a02f0;

  private View view7f0a02fa;

  private View view7f0a035c;

  private View view7f0a043a;

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RegisterActivity_ViewBinding(final RegisterActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.iv_back, "field 'iv_back' and method 'onViewClicked'");
    target.iv_back = Utils.castView(view, R.id.iv_back, "field 'iv_back'", ImageView.class);
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_code, "field 'tvCode' and method 'onViewClicked'");
    target.tvCode = Utils.castView(view, R.id.tv_code, "field 'tvCode'", TextView.class);
    view7f0a0535 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.iv = Utils.findRequiredViewAsType(source, R.id.iv, "field 'iv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.ll_code, "field 'llCode' and method 'onViewClicked'");
    target.llCode = Utils.castView(view, R.id.ll_code, "field 'llCode'", LinearLayout.class);
    view7f0a02ef = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ivEmail = Utils.findRequiredViewAsType(source, R.id.iv_email, "field 'ivEmail'", ImageView.class);
    target.ivPhone = Utils.findRequiredViewAsType(source, R.id.iv_phone, "field 'ivPhone'", ImageView.class);
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'etEmail'", EditText.class);
    target.et_login_password = Utils.findRequiredViewAsType(source, R.id.et_login_password, "field 'et_login_password'", EditText.class);
    target.et_tx_password = Utils.findRequiredViewAsType(source, R.id.et_tx_password, "field 'et_tx_password'", EditText.class);
    target.et_sms_code = Utils.findRequiredViewAsType(source, R.id.et_sms_code, "field 'et_sms_code'", EditText.class);
    target.inputInviteCodeEt = Utils.findRequiredViewAsType(source, R.id.inputInviteCodeEt, "field 'inputInviteCodeEt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.tv_send_code, "field 'tv_send_code' and method 'onViewClicked'");
    target.tv_send_code = Utils.castView(view, R.id.tv_send_code, "field 'tv_send_code'", TextView.class);
    view7f0a0553 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.invitePeopleTv = Utils.findRequiredViewAsType(source, R.id.invitePeopleEt, "field 'invitePeopleTv'", TextView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", RelativeLayout.class);
    target.llPhone = Utils.findRequiredViewAsType(source, R.id.ll_phone, "field 'llPhone'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.tv_agreement, "field 'tv_agreement' and method 'onViewClicked'");
    target.tv_agreement = Utils.castView(view, R.id.tv_agreement, "field 'tv_agreement'", TextView.class);
    view7f0a052f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv_agreement, "field 'iv_agreement' and method 'onViewClicked'");
    target.iv_agreement = Utils.castView(view, R.id.iv_agreement, "field 'iv_agreement'", CheckBox.class);
    view7f0a028c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ll_email, "method 'onViewClicked'");
    view7f0a02f0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.ll_phone1, "method 'onViewClicked'");
    view7f0a02fa = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.nextStepTv, "method 'onViewClicked'");
    view7f0a035c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.scanIv, "method 'onViewClicked'");
    view7f0a043a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RegisterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.iv_back = null;
    target.tvCode = null;
    target.iv = null;
    target.llCode = null;
    target.ivEmail = null;
    target.ivPhone = null;
    target.etPhone = null;
    target.etEmail = null;
    target.et_login_password = null;
    target.et_tx_password = null;
    target.et_sms_code = null;
    target.inputInviteCodeEt = null;
    target.tv_send_code = null;
    target.invitePeopleTv = null;
    target.top = null;
    target.llPhone = null;
    target.tv_agreement = null;
    target.iv_agreement = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a0535.setOnClickListener(null);
    view7f0a0535 = null;
    view7f0a02ef.setOnClickListener(null);
    view7f0a02ef = null;
    view7f0a0553.setOnClickListener(null);
    view7f0a0553 = null;
    view7f0a052f.setOnClickListener(null);
    view7f0a052f = null;
    view7f0a028c.setOnClickListener(null);
    view7f0a028c = null;
    view7f0a02f0.setOnClickListener(null);
    view7f0a02f0 = null;
    view7f0a02fa.setOnClickListener(null);
    view7f0a02fa = null;
    view7f0a035c.setOnClickListener(null);
    view7f0a035c = null;
    view7f0a043a.setOnClickListener(null);
    view7f0a043a = null;
  }
}
