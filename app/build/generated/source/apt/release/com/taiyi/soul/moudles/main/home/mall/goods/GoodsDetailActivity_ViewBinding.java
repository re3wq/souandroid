// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.goods;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zk.banner.Banner;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GoodsDetailActivity_ViewBinding implements Unbinder {
  private GoodsDetailActivity target;

  private View view7f0a01ac;

  private View view7f0a01af;

  private View view7f0a01ad;

  private View view7f0a007e;

  private View view7f0a0222;

  private View view7f0a021c;

  @UiThread
  public GoodsDetailActivity_ViewBinding(GoodsDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public GoodsDetailActivity_ViewBinding(final GoodsDetailActivity target, View source) {
    this.target = target;

    View view;
    target.mScroll = Utils.findRequiredViewAsType(source, R.id.scroll, "field 'mScroll'", ScrollView.class);
    target.mGoodsBanner = Utils.findRequiredViewAsType(source, R.id.goods_banner_vs, "field 'mGoodsBanner'", Banner.class);
    target.mGoodsName = Utils.findRequiredViewAsType(source, R.id.goods_name, "field 'mGoodsName'", TextView.class);
    target.mGoodsPrice = Utils.findRequiredViewAsType(source, R.id.goods_price, "field 'mGoodsPrice'", TextView.class);
    target.brand = Utils.findRequiredViewAsType(source, R.id.brand, "field 'brand'", TextView.class);
    target.have_goods = Utils.findRequiredViewAsType(source, R.id.have_goods, "field 'have_goods'", TextView.class);
    target.have_goods_f = Utils.findRequiredViewAsType(source, R.id.have_goods_f, "field 'have_goods_f'", TextView.class);
    target.fee = Utils.findRequiredViewAsType(source, R.id.fee, "field 'fee'", TextView.class);
    target.goods_serial_number = Utils.findRequiredViewAsType(source, R.id.goods_serial_number, "field 'goods_serial_number'", TextView.class);
    target.price_two = Utils.findRequiredViewAsType(source, R.id.price_two, "field 'price_two'", TextView.class);
    target.name_two = Utils.findRequiredViewAsType(source, R.id.name_two, "field 'name_two'", TextView.class);
    target.mGoodsWeb = Utils.findRequiredViewAsType(source, R.id.goods_web, "field 'mGoodsWeb'", WebView.class);
    view = Utils.findRequiredView(source, R.id.details_back, "field 'mDetailsBack' and method 'onViewClicked'");
    target.mDetailsBack = Utils.castView(view, R.id.details_back, "field 'mDetailsBack'", RelativeLayout.class);
    view7f0a01ac = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mDetailsCart = Utils.findRequiredViewAsType(source, R.id.details_cart, "field 'mDetailsCart'", ImageView.class);
    target.mScrollTitle = Utils.findRequiredViewAsType(source, R.id.scroll_title, "field 'mScrollTitle'", LinearLayout.class);
    target.marks_goods = Utils.findRequiredViewAsType(source, R.id.marks_goods, "field 'marks_goods'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.details_cart_banner, "field 'details_cart_banner' and method 'onViewClicked'");
    target.details_cart_banner = Utils.castView(view, R.id.details_cart_banner, "field 'details_cart_banner'", ImageView.class);
    view7f0a01af = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.details_back_banner, "method 'onViewClicked'");
    view7f0a01ad = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_car, "method 'onViewClicked'");
    view7f0a007e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.goods_buy, "method 'onViewClicked'");
    view7f0a0222 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.go_top, "method 'onViewClicked'");
    view7f0a021c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    GoodsDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mScroll = null;
    target.mGoodsBanner = null;
    target.mGoodsName = null;
    target.mGoodsPrice = null;
    target.brand = null;
    target.have_goods = null;
    target.have_goods_f = null;
    target.fee = null;
    target.goods_serial_number = null;
    target.price_two = null;
    target.name_two = null;
    target.mGoodsWeb = null;
    target.mDetailsBack = null;
    target.mDetailsCart = null;
    target.mScrollTitle = null;
    target.marks_goods = null;
    target.details_cart_banner = null;

    view7f0a01ac.setOnClickListener(null);
    view7f0a01ac = null;
    view7f0a01af.setOnClickListener(null);
    view7f0a01af = null;
    view7f0a01ad.setOnClickListener(null);
    view7f0a01ad = null;
    view7f0a007e.setOnClickListener(null);
    view7f0a007e = null;
    view7f0a0222.setOnClickListener(null);
    view7f0a0222 = null;
    view7f0a021c.setOnClickListener(null);
    view7f0a021c = null;
  }
}
