// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.assets.currency.currencydetail;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.github.mikephil.charting.charts.LineChart;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CurrencyDetailsActivity_ViewBinding implements Unbinder {
  private CurrencyDetailsActivity target;

  private View view7f0a0084;

  private View view7f0a0139;

  private View view7f0a0515;

  private View view7f0a009f;

  private View view7f0a0138;

  private View view7f0a0513;

  @UiThread
  public CurrencyDetailsActivity_ViewBinding(CurrencyDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CurrencyDetailsActivity_ViewBinding(final CurrencyDetailsActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.amountTv = Utils.findRequiredViewAsType(source, R.id.amountTv, "field 'amountTv'", TextView.class);
    target.valueTv = Utils.findRequiredViewAsType(source, R.id.valueTv, "field 'valueTv'", TextView.class);
    target.select_tv = Utils.findRequiredViewAsType(source, R.id.select_tv, "field 'select_tv'", TextView.class);
    target.rightCurrencyTv = Utils.findRequiredViewAsType(source, R.id.rightCurrencyTv, "field 'rightCurrencyTv'", TextView.class);
    target.chartView = Utils.findRequiredViewAsType(source, R.id.chartView, "field 'chartView'", LineChart.class);
    view = Utils.findRequiredView(source, R.id.allTv, "field 'allTv' and method 'onViewClicked'");
    target.allTv = Utils.castView(view, R.id.allTv, "field 'allTv'", TextView.class);
    view7f0a0084 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.collectMoneyTv, "field 'collectMoneyTv' and method 'onViewClicked'");
    target.collectMoneyTv = Utils.castView(view, R.id.collectMoneyTv, "field 'collectMoneyTv'", TextView.class);
    view7f0a0139 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.transferTv, "field 'transferTv' and method 'onViewClicked'");
    target.transferTv = Utils.castView(view, R.id.transferTv, "field 'transferTv'", TextView.class);
    view7f0a0515 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.mTab = Utils.findRequiredViewAsType(source, R.id.tab, "field 'mTab'", SlidingTabLayout.class);
    target.mViewpager = Utils.findRequiredViewAsType(source, R.id.viewpager, "field 'mViewpager'", ViewPager.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.collectMoneyBtnTv, "method 'onViewClicked'");
    view7f0a0138 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.transferBtnTv, "method 'onViewClicked'");
    view7f0a0513 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CurrencyDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.amountTv = null;
    target.valueTv = null;
    target.select_tv = null;
    target.rightCurrencyTv = null;
    target.chartView = null;
    target.allTv = null;
    target.collectMoneyTv = null;
    target.transferTv = null;
    target.recyclerView = null;
    target.top = null;
    target.mTab = null;
    target.mViewpager = null;

    view7f0a0084.setOnClickListener(null);
    view7f0a0084 = null;
    view7f0a0139.setOnClickListener(null);
    view7f0a0139 = null;
    view7f0a0515.setOnClickListener(null);
    view7f0a0515 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a0138.setOnClickListener(null);
    view7f0a0138 = null;
    view7f0a0513.setOnClickListener(null);
    view7f0a0513 = null;
  }
}
