// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.goods;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.stx.xhb.pagemenulibrary.PageMenuLayout;
import com.taiyi.soul.R;
import com.zhouwei.mzbanner.MZBannerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MallActivity_ViewBinding implements Unbinder {
  private MallActivity target;

  private View view7f0a0256;

  private View view7f0a0398;

  private View view7f0a028e;

  private View view7f0a0319;

  private View view7f0a0314;

  @UiThread
  public MallActivity_ViewBinding(MallActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MallActivity_ViewBinding(final MallActivity target, View source) {
    this.target = target;

    View view;
    target.mClose = Utils.findRequiredViewAsType(source, R.id.close, "field 'mClose'", TextView.class);
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.img_car, "field 'mImgCar' and method 'onViewClicked'");
    target.mImgCar = Utils.castView(view, R.id.img_car, "field 'mImgCar'", ImageView.class);
    view7f0a0256 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.order_iv, "field 'orderIv' and method 'onViewClicked'");
    target.orderIv = Utils.castView(view, R.id.order_iv, "field 'orderIv'", ImageView.class);
    view7f0a0398 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.mMallBanner = Utils.findRequiredViewAsType(source, R.id.mall_banner, "field 'mMallBanner'", MZBannerView.class);
    target.mMallDiscount = Utils.findRequiredViewAsType(source, R.id.mall_discount, "field 'mMallDiscount'", RecyclerView.class);
    target.topicRecyclerView = Utils.findRequiredViewAsType(source, R.id.topicRecyclerView, "field 'topicRecyclerView'", RecyclerView.class);
    target.mPageMenuLayout = Utils.findRequiredViewAsType(source, R.id.pagemenu, "field 'mPageMenuLayout'", PageMenuLayout.class);
    target.ll_top_dot = Utils.findRequiredViewAsType(source, R.id.ll_top_dot, "field 'll_top_dot'", LinearLayout.class);
    target.mall_recycle = Utils.findRequiredViewAsType(source, R.id.mall_recycle, "field 'mall_recycle'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.mall_hot_more, "method 'onViewClicked'");
    view7f0a0319 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.mall_discount_more, "method 'onViewClicked'");
    view7f0a0314 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MallActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mClose = null;
    target.mTvTitle = null;
    target.mImgCar = null;
    target.orderIv = null;
    target.mMallBanner = null;
    target.mMallDiscount = null;
    target.topicRecyclerView = null;
    target.mPageMenuLayout = null;
    target.ll_top_dot = null;
    target.mall_recycle = null;

    view7f0a0256.setOnClickListener(null);
    view7f0a0256 = null;
    view7f0a0398.setOnClickListener(null);
    view7f0a0398 = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a0319.setOnClickListener(null);
    view7f0a0319 = null;
    view7f0a0314.setOnClickListener(null);
    view7f0a0314 = null;
  }
}
