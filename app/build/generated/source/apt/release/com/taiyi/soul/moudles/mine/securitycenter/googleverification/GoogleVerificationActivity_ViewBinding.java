// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.securitycenter.googleverification;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GoogleVerificationActivity_ViewBinding implements Unbinder {
  private GoogleVerificationActivity target;

  private View view7f0a045f;

  private View view7f0a056f;

  private View view7f0a009f;

  private View view7f0a0150;

  private View view7f0a03b2;

  @UiThread
  public GoogleVerificationActivity_ViewBinding(GoogleVerificationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public GoogleVerificationActivity_ViewBinding(final GoogleVerificationActivity target,
      View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.googleSecretTv = Utils.findRequiredViewAsType(source, R.id.googleSecretTv, "field 'googleSecretTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.sendCodeTv, "field 'sendCodeTv' and method 'onViewClicked'");
    target.sendCodeTv = Utils.castView(view, R.id.sendCodeTv, "field 'sendCodeTv'", TextView.class);
    view7f0a045f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.updateGoogleVerificationTv, "field 'updateGoogleVerificationTv' and method 'onViewClicked'");
    target.updateGoogleVerificationTv = Utils.castView(view, R.id.updateGoogleVerificationTv, "field 'updateGoogleVerificationTv'", TextView.class);
    view7f0a056f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.googleVerificationCodeEt = Utils.findRequiredViewAsType(source, R.id.googleVerificationCodeEt, "field 'googleVerificationCodeEt'", EditText.class);
    target.verificationCodeEt = Utils.findRequiredViewAsType(source, R.id.verificationCodeEt, "field 'verificationCodeEt'", EditText.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.copySecretTv, "method 'onViewClicked'");
    view7f0a0150 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.pasteGoogleVerificationCodeTv, "method 'onViewClicked'");
    view7f0a03b2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    GoogleVerificationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.googleSecretTv = null;
    target.sendCodeTv = null;
    target.updateGoogleVerificationTv = null;
    target.googleVerificationCodeEt = null;
    target.verificationCodeEt = null;
    target.top = null;

    view7f0a045f.setOnClickListener(null);
    view7f0a045f = null;
    view7f0a056f.setOnClickListener(null);
    view7f0a056f = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a0150.setOnClickListener(null);
    view7f0a0150 = null;
    view7f0a03b2.setOnClickListener(null);
    view7f0a03b2 = null;
  }
}
