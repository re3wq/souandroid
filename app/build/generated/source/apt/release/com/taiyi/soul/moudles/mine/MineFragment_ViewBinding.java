// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.taiyi.soul.utils.numberrolling.NumberRollingView;
import com.taiyi.soul.utils.numberrolling.NumberRollingViewT;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MineFragment_ViewBinding implements Unbinder {
  private MineFragment target;

  private View view7f0a032e;

  private View view7f0a0339;

  private View view7f0a03c0;

  private View view7f0a0463;

  @UiThread
  public MineFragment_ViewBinding(final MineFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.messageIv, "field 'messageIv' and method 'onClicked'");
    target.messageIv = Utils.castView(view, R.id.messageIv, "field 'messageIv'", ImageView.class);
    view7f0a032e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClicked(p0);
      }
    });
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
    target.iv = Utils.findRequiredViewAsType(source, R.id.iv, "field 'iv'", ImageView.class);
    target.levelIv = Utils.findRequiredViewAsType(source, R.id.levelIv, "field 'levelIv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.mine_avatars, "field 'avatarIv' and method 'onClicked'");
    target.avatarIv = Utils.castView(view, R.id.mine_avatars, "field 'avatarIv'", ImageView.class);
    view7f0a0339 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClicked(p0);
      }
    });
    target.topIndTv = Utils.findRequiredViewAsType(source, R.id.topIndTv, "field 'topIndTv'", TextView.class);
    target.V1 = Utils.findRequiredViewAsType(source, R.id.V1, "field 'V1'", TextView.class);
    target.V2 = Utils.findRequiredViewAsType(source, R.id.V2, "field 'V2'", TextView.class);
    target.totalAssetsTypeTv = Utils.findRequiredViewAsType(source, R.id.totalAssetsTypeTv, "field 'totalAssetsTypeTv'", TextView.class);
    target.usdnAssetsTv1 = Utils.findRequiredViewAsType(source, R.id.usdnAssetsTv1, "field 'usdnAssetsTv1'", NumberRollingView.class);
    target.ngkAssetsTv1 = Utils.findRequiredViewAsType(source, R.id.ngkAssetsTv1, "field 'ngkAssetsTv1'", NumberRollingView.class);
    target.totalAssetsTv1 = Utils.findRequiredViewAsType(source, R.id.totalAssetsTv1, "field 'totalAssetsTv1'", NumberRollingViewT.class);
    view = Utils.findRequiredView(source, R.id.phoneNumberTv, "field 'phoneNumberTv' and method 'onClicked'");
    target.phoneNumberTv = Utils.castView(view, R.id.phoneNumberTv, "field 'phoneNumberTv'", TextView.class);
    view7f0a03c0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClicked(p0);
      }
    });
    target.levelOneCl = Utils.findRequiredViewAsType(source, R.id.levelOneCl, "field 'levelOneCl'", ConstraintLayout.class);
    target.levelSixCl = Utils.findRequiredViewAsType(source, R.id.levelSixCl, "field 'levelSixCl'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.setupIv, "method 'onClicked'");
    view7f0a0463 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MineFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.messageIv = null;
    target.progressBar = null;
    target.iv = null;
    target.levelIv = null;
    target.avatarIv = null;
    target.topIndTv = null;
    target.V1 = null;
    target.V2 = null;
    target.totalAssetsTypeTv = null;
    target.usdnAssetsTv1 = null;
    target.ngkAssetsTv1 = null;
    target.totalAssetsTv1 = null;
    target.phoneNumberTv = null;
    target.levelOneCl = null;
    target.levelSixCl = null;

    view7f0a032e.setOnClickListener(null);
    view7f0a032e = null;
    view7f0a0339.setOnClickListener(null);
    view7f0a0339 = null;
    view7f0a03c0.setOnClickListener(null);
    view7f0a03c0 = null;
    view7f0a0463.setOnClickListener(null);
    view7f0a0463 = null;
  }
}
