// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node.hashmarket.transactionrecord;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TransactionRecordActivity_ViewBinding implements Unbinder {
  private TransactionRecordActivity target;

  private View view7f0a009f;

  @UiThread
  public TransactionRecordActivity_ViewBinding(TransactionRecordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public TransactionRecordActivity_ViewBinding(final TransactionRecordActivity target,
      View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'mTvTitle'", TextView.class);
    target.mEntrustTab = Utils.findRequiredViewAsType(source, R.id.entrust_tab, "field 'mEntrustTab'", SlidingTabLayout.class);
    target.mEntrustViewpager = Utils.findRequiredViewAsType(source, R.id.entrust_viewpager, "field 'mEntrustViewpager'", ViewPager.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    TransactionRecordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mEntrustTab = null;
    target.mEntrustViewpager = null;
    target.top = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
