// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.order;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckPayActivity_ViewBinding implements Unbinder {
  private CheckPayActivity target;

  private View view7f0a028e;

  private View view7f0a011a;

  @UiThread
  public CheckPayActivity_ViewBinding(CheckPayActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CheckPayActivity_ViewBinding(final CheckPayActivity target, View source) {
    this.target = target;

    View view;
    target.mTvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'mTvTitle'", TextView.class);
    target.mNgkChecked = Utils.findRequiredViewAsType(source, R.id.ngk_checked, "field 'mNgkChecked'", CheckBox.class);
    target.mUsdnChecked = Utils.findRequiredViewAsType(source, R.id.usdn_checked, "field 'mUsdnChecked'", CheckBox.class);
    target.mCheckOrderTotal = Utils.findRequiredViewAsType(source, R.id.check_order_total, "field 'mCheckOrderTotal'", TextView.class);
    target.balance_usdn = Utils.findRequiredViewAsType(source, R.id.balance_usdn, "field 'balance_usdn'", TextView.class);
    target.balance_ngk = Utils.findRequiredViewAsType(source, R.id.balance_ngk, "field 'balance_ngk'", TextView.class);
    target.iv_ngk = Utils.findRequiredViewAsType(source, R.id.iv_ngk, "field 'iv_ngk'", ImageView.class);
    target.iv_usdn = Utils.findRequiredViewAsType(source, R.id.iv_usdn, "field 'iv_usdn'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.check_order_next, "method 'onViewClicked'");
    view7f0a011a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CheckPayActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvTitle = null;
    target.mNgkChecked = null;
    target.mUsdnChecked = null;
    target.mCheckOrderTotal = null;
    target.balance_usdn = null;
    target.balance_ngk = null;
    target.iv_ngk = null;
    target.iv_usdn = null;

    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
    view7f0a011a.setOnClickListener(null);
    view7f0a011a = null;
  }
}
