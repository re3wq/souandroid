// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node.hashmarket.release;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flyco.tablayout.SlidingTabLayout;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReleaseOrderActivity_ViewBinding implements Unbinder {
  private ReleaseOrderActivity target;

  private View view7f0a009f;

  private View view7f0a041b;

  @UiThread
  public ReleaseOrderActivity_ViewBinding(ReleaseOrderActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ReleaseOrderActivity_ViewBinding(final ReleaseOrderActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    target.mDealTab = Utils.findRequiredViewAsType(source, R.id.deal_tab, "field 'mDealTab'", SlidingTabLayout.class);
    target.mDealViewpager = Utils.findRequiredViewAsType(source, R.id.deal_viewpager, "field 'mDealViewpager'", ViewPager.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rightTitleTv, "method 'onViewClicked'");
    view7f0a041b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ReleaseOrderActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.top = null;
    target.mDealTab = null;
    target.mDealViewpager = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a041b.setOnClickListener(null);
    view7f0a041b = null;
  }
}
