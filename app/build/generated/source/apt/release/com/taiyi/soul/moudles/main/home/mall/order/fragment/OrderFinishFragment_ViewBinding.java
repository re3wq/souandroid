// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.home.mall.order.fragment;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import com.zcolin.gui.pullrecyclerview.PullRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderFinishFragment_ViewBinding implements Unbinder {
  private OrderFinishFragment target;

  @UiThread
  public OrderFinishFragment_ViewBinding(OrderFinishFragment target, View source) {
    this.target = target;

    target.mOrderRecycle = Utils.findRequiredViewAsType(source, R.id.order_recycle, "field 'mOrderRecycle'", PullRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderFinishFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mOrderRecycle = null;
  }
}
