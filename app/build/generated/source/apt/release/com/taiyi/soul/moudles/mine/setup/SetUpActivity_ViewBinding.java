// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.setup;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SetUpActivity_ViewBinding implements Unbinder {
  private SetUpActivity target;

  private View view7f0a009f;

  @UiThread
  public SetUpActivity_ViewBinding(SetUpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SetUpActivity_ViewBinding(final SetUpActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.logoIv = Utils.findRequiredViewAsType(source, R.id.logoIv, "field 'logoIv'", ImageView.class);
    target.tv = Utils.findRequiredViewAsType(source, R.id.tv, "field 'tv'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SetUpActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.logoIv = null;
    target.tv = null;
    target.recyclerView = null;
    target.top = null;

    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
