// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.main.node.ram;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RamActivity_ViewBinding implements Unbinder {
  private RamActivity target;

  private View view7f0a00f5;

  private View view7f0a045e;

  private View view7f0a00f4;

  private View view7f0a009f;

  @UiThread
  public RamActivity_ViewBinding(RamActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RamActivity_ViewBinding(final RamActivity target, View source) {
    this.target = target;

    View view;
    target.titleTv = Utils.findRequiredViewAsType(source, R.id.titleTv, "field 'titleTv'", TextView.class);
    target.priceTv = Utils.findRequiredViewAsType(source, R.id.priceTv, "field 'priceTv'", TextView.class);
    target.speedTv = Utils.findRequiredViewAsType(source, R.id.speedTv, "field 'speedTv'", TextView.class);
    target.ramProgressBar = Utils.findRequiredViewAsType(source, R.id.ramProgressBar, "field 'ramProgressBar'", ProgressBar.class);
    view = Utils.findRequiredView(source, R.id.buyInTv, "field 'buyInTv' and method 'onViewClicked'");
    target.buyInTv = Utils.castView(view, R.id.buyInTv, "field 'buyInTv'", TextView.class);
    view7f0a00f5 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sellTv, "field 'sellTv' and method 'onViewClicked'");
    target.sellTv = Utils.castView(view, R.id.sellTv, "field 'sellTv'", TextView.class);
    view7f0a045e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.quantityEt = Utils.findRequiredViewAsType(source, R.id.quantityEt, "field 'quantityEt'", EditText.class);
    target.balanceTv = Utils.findRequiredViewAsType(source, R.id.balanceTv, "field 'balanceTv'", TextView.class);
    target.valueTv = Utils.findRequiredViewAsType(source, R.id.valueTv, "field 'valueTv'", TextView.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", SeekBar.class);
    target.receiptAccountNameEt = Utils.findRequiredViewAsType(source, R.id.receiptAccountNameEt, "field 'receiptAccountNameEt'", EditText.class);
    target.supportTv = Utils.findRequiredViewAsType(source, R.id.supportTv, "field 'supportTv'", TextView.class);
    view = Utils.findRequiredView(source, R.id.buyInOrSellTv, "field 'buyInOrSellTv' and method 'onViewClicked'");
    target.buyInOrSellTv = Utils.castView(view, R.id.buyInOrSellTv, "field 'buyInOrSellTv'", TextView.class);
    view7f0a00f4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.quantity = Utils.findRequiredViewAsType(source, R.id.quantity, "field 'quantity'", TextView.class);
    target.top = Utils.findRequiredViewAsType(source, R.id.top, "field 'top'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.backIv, "method 'onViewClicked'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RamActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.titleTv = null;
    target.priceTv = null;
    target.speedTv = null;
    target.ramProgressBar = null;
    target.buyInTv = null;
    target.sellTv = null;
    target.quantityEt = null;
    target.balanceTv = null;
    target.valueTv = null;
    target.progressBar = null;
    target.receiptAccountNameEt = null;
    target.supportTv = null;
    target.buyInOrSellTv = null;
    target.quantity = null;
    target.top = null;

    view7f0a00f5.setOnClickListener(null);
    view7f0a00f5 = null;
    view7f0a045e.setOnClickListener(null);
    view7f0a045e = null;
    view7f0a00f4.setOnClickListener(null);
    view7f0a00f4 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
  }
}
