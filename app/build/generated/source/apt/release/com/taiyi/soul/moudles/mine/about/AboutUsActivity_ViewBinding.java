// Generated code from Butter Knife. Do not modify!
package com.taiyi.soul.moudles.mine.about;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.taiyi.soul.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AboutUsActivity_ViewBinding implements Unbinder {
  private AboutUsActivity target;

  private View view7f0a0579;

  private View view7f0a0560;

  private View view7f0a023b;

  private View view7f0a028e;

  @UiThread
  public AboutUsActivity_ViewBinding(AboutUsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AboutUsActivity_ViewBinding(final AboutUsActivity target, View source) {
    this.target = target;

    View view;
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'tvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.user_xy, "field 'userXy' and method 'onViewClicked'");
    target.userXy = Utils.castView(view, R.id.user_xy, "field 'userXy'", RelativeLayout.class);
    view7f0a0579 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.twitter, "field 'twitter' and method 'onViewClicked'");
    target.twitter = Utils.castView(view, R.id.twitter, "field 'twitter'", TextView.class);
    view7f0a0560 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", TextView.class);
    view = Utils.findRequiredView(source, R.id.gw, "field 'gw' and method 'onViewClicked'");
    target.gw = Utils.castView(view, R.id.gw, "field 'gw'", TextView.class);
    view7f0a023b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.small_logo = Utils.findRequiredViewAsType(source, R.id.small_logo, "field 'small_logo'", ImageView.class);
    target.iv = Utils.findRequiredViewAsType(source, R.id.iv, "field 'iv'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "method 'onViewClicked'");
    view7f0a028e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AboutUsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvTitle = null;
    target.userXy = null;
    target.twitter = null;
    target.email = null;
    target.gw = null;
    target.small_logo = null;
    target.iv = null;

    view7f0a0579.setOnClickListener(null);
    view7f0a0579 = null;
    view7f0a0560.setOnClickListener(null);
    view7f0a0560 = null;
    view7f0a023b.setOnClickListener(null);
    view7f0a023b = null;
    view7f0a028e.setOnClickListener(null);
    view7f0a028e = null;
  }
}
