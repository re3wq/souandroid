package com.zcolin.gui.pullrecyclerview.progressindicator;

/**
 * Created by Jack on 2015/10/19.
 *
 * 常用：BallPulseIndicator  BallTrianglePathIndicator LineScaleIndicator  BallSpinFadeLoaderIndicator  LineSpinFadeLoaderIndicator
 */
public class ProgressStyle {
    public static final String SysProgress="SysProgress";
    public static final String BallBeatIndicator="BallBeatIndicator";
    public static final String BallClipRotateIndicator="BallClipRotateIndicator";
    public static final String BallClipRotateMultipleIndicator="BallClipRotateMultipleIndicator";
    public static final String BallClipRotatePulseIndicator="BallClipRotatePulseIndicator";
    public static final String BallGridBeatIndicator="BallGridBeatIndicator";
    public static final String BallGridPulseIndicator="BallGridPulseIndicator";
    public static final String BallPulseIndicator="BallPulseIndicator";//三个点
    public static final String BallPulseRiseIndicator="BallPulseRiseIndicator";
    public static final String BallPulseSyncIndicator="BallPulseSyncIndicator";
    public static final String BallRotateIndicator="BallRotateIndicator";
    public static final String BallScaleIndicator="BallScaleIndicator";
    public static final String BallScaleMultipleIndicator="BallScaleMultipleIndicator";
    public static final String BallScaleRippleIndicator="BallScaleRippleIndicator";
    public static final String BallScaleRippleMultipleIndicator="BallScaleRippleMultipleIndicator";
    public static final String BallSpinFadeLoaderIndicator="BallSpinFadeLoaderIndicator";//圆形圆点
    public static final String BallTrianglePathIndicator="BallTrianglePathIndicator";//三角形圆点
    public static final String BallZigZagDeflectIndicator="BallZigZagDeflectIndicator";
    public static final String BallZigZagIndicator="BallZigZagIndicator";
    public static final String CubeTransitionIndicator="CubeTransitionIndicator";
    public static final String LineScaleIndicator="LineScaleIndicator";//条形码
    public static final String LineScalePartyIndicator="LineScalePartyIndicator";
    public static final String LineScalePulseOutIndicator="LineScalePulseOutIndicator";
    public static final String LineScalePulseOutRapidIndicator="LineScalePulseOutRapidIndicator";
    public static final String LineSpinFadeLoaderIndicator="LineSpinFadeLoaderIndicator";//太阳
    public static final String PacmanIndicator="PacmanIndicator";
    public static final String SemiCircleSpinIndicator="SemiCircleSpinIndicator";
    public static final String SquareSpinIndicator="SquareSpinIndicator";
    public static final String TriangleSkewSpinIndicator="TriangleSkewSpinIndicator";
}
