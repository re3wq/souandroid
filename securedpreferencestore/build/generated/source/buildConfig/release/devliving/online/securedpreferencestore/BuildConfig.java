/**
 * Automatically generated file. DO NOT MODIFY
 */
package devliving.online.securedpreferencestore;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "devliving.online.securedpreferencestore";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 14;
  public static final String VERSION_NAME = "0.7.4";
}
